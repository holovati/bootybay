#ifndef MSR_H
#define MSR_H

#include <stddef.h>
#include <stdint.h>

#define  MSR_EFER_SCE    (1ULL << 0)   /* System Call Extensions R/W */
#define  MSR_EFER_LME    (1ULL << 8)   /* Long Mode Enable R/W */
#define  MSR_EFER_LMA    (1ULL << 10)   /* Long Mode Active R/W */
#define  MSR_EFER_NXE    (1ULL << 11)   /* No-Execute Enable R/W */
#define  MSR_EFER_SVME   (1ULL << 12)   /* Secure Virtual Machine Enable R/W */
#define  MSR_EFER_LMSLE  (1ULL << 13)   /* Long Mode Segment Limit Enable R/W */
#define  MSR_EFER_FFXSR  (1ULL << 14)   /* Fast FXSAVE/FXRSTOR R/W */
#define  MSR_EFER_TCE    (1ULL << 15)   /* Translation Cache Extension R/W */

#define  MSR_APIC_BASE       0x0000001B
#define  MSR_EFER            0xC0000080
#define  MSR_STAR            0xC0000081
#define  MSR_LSTAR           0xC0000082
#define  MSR_CSTAR           0xC0000083
#define  MSR_SF_MASK         0xC0000084
#define  MSR_FS_BASE         0xC0000100
#define  MSR_GS_BASE         0xC0000101
#define  MSR_KERNEL_GS_BASE  0xC0000102
#define  MSR_TSC_AUX         0xC0000103
#define  MSR_TSC_RATIO       0xC0000104
#define  MSR_SYSENTER_CS     0x00000174
#define  MSR_SYSENTER_ESP    0x00000175
#define  MSR_SYSENTER_EIP    0x00000176

static inline uint64_t msr_read(uintptr_t msr)
{
  uint32_t hi, lo;
  uint64_t res;

  asm volatile("rdmsr" : "=a"(lo), "=d"(hi) : "c"(msr) : );

  res = hi;

  res <<= 32;

  res |= lo;

  return res;
}

static inline void msr_write(uintptr_t msr, uint64_t val)
{
  uint32_t hi, lo;

  lo = (uint32_t)(val & 0xFFFFFFFF);
  hi = (uint32_t)(val >> 32);

  asm volatile("wrmsr" : : "a"(lo), "d"(hi), "c"(msr) : );
}


#endif /* MSR_H */
