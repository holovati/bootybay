#ifndef GDT_H
#define GDT_H
#include <stdint.h>
#include "assert.h"

#define  GDT_ACCESSED              (1ULL << (32 + 8))
#define  GDT_EXECUTABLE            (1ULL << (32 + 11))
#define  GDT_DESCRIPTOR_GDT        (1ULL << (32 + 12))
#define  GDT_PRESENT               (1ULL << (32 + 15))
#define  GDT_AVAILABLE             (1ULL << (32 + 20))
#define  GDT_LONG                  (1ULL << (32 + 21))
#define  GDT_DEFAULT_OPERAND_SIZE  (1ULL << (32 + 22))
#define  GDT_GRANULARITY           (1ULL << (32 + 23))

#define  GDT_CODE_READABLE         (1ULL << (32 + 9))
#define  GDT_CODE_CONFORMING       (1ULL << (32 + 10))

#define  GDT_DATA_WRITABLE         (1ULL << (32 + 9))
#define  GDT_DATA_EXPAND_DOWN      (1ULL << (32 + 10))

struct gdt_register
{
  uint16_t size;
  union
  {
    uint64_t uint;
    void* ptr;
  };
} __attribute__((packed));

static_assert(sizeof(struct gdt_register) == 10);

#endif /* GDT_H */
