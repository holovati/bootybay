#include "disk.h"
#include "bios.h"
#include "string.h"
#include "stdio.h"
#include "assert.h"

#define RESET_DISK_DRIVE 0x00
#define EXTENDED_READ 0x42

/* Disk Access Packet */
struct DAP
{
  uint8_t  size;
  uint8_t  unused;
  uint16_t n_sectors;
  uint16_t offset;
  uint16_t segment;
  uint64_t first_sector;
};

uint32_t disk_reset(uint32_t drv)
{
  struct bios_ioregs regs;

  memset(&regs, 0, sizeof(regs));

  regs.ah = RESET_DISK_DRIVE;
  regs.dl = (uint8_t)drv;

  if (bios_call(0x13, &regs))
  {
    /* bios_print_ioregs(&regs) */;
    return regs.ah;
  }

  return 0;
}

static uint32_t disk_read_sector(uint32_t drv, uint64_t sct, void *out)
{
  struct bios_ioregs regs;
  struct DAP dap;

  //printf("disk_read_sector: drv = %x\n", drv);

  assert(drv == 0x80);

  dap.size = sizeof(dap);
  dap.unused = 0;
  dap.n_sectors = 1;
  dap.offset = FLAT_TO_OFFSET(out);
  dap.segment = FLAT_TO_SEGMENT(out);

  dap.first_sector = sct;

  memset(&regs, 0, sizeof(regs));

  regs.ah = EXTENDED_READ;
  regs.dl = (uint8_t)drv;

  /* DS:SI segment:offset pointer to the DAP */
  /* DAP is on the stack*/
  regs.ds = FLAT_TO_SEGMENT(&dap);
  regs.si = FLAT_TO_OFFSET(&dap);

  if (bios_call(0x13, &regs))
  {
    /* bios_print_ioregs(&regs) */;
    return regs.ah;
  }

  return 0;
}

uint32_t disk_rd_sectors(uint32_t drv, uint64_t sct, size_t cnt, void *out, size_t out_sz)
{
  size_t idx;
  uint8_t sector[512];
  uint8_t (*out_buffer)[512];

  out_buffer = out;

  if ((cnt * 512) > out_sz)
  {
    printf("Disk: Reading sector into smaller buffer\n");
  }

  for (idx = 0; idx < cnt; ++idx)
  {
    if (disk_read_sector(drv, sct + idx, &sector[0]))
    {
      return 1;
    }

    memcpy(out_buffer[idx], &sector[0], (out_sz < sizeof(sector) ? out_sz : sizeof(sector)));
  }

  return 0;
}
