#include "stdio.h"
#include "fat32.h"
#include "malloc.h"

struct fat_file
{
  struct fat32 fat;
  struct fat32_dir fd;
  uint32_t offset_cluster;
  off_t offset_in_cluster;
};

FILE *fopen(const char *restrict pathname, const char *restrict mode)
{
  struct fat_file file, *p_file;
  struct fat32_dir *root_dir;

  (void)mode; /* UNUSED */

  file.offset_in_cluster = 0;
  file.offset_cluster = 0;

  if (fat32_init(&file.fat, 0x80))
  {
    return NULL;
  }

  root_dir = fat32_get_root_dir(&file.fat);

  if(root_dir == NULL)
  {
    return NULL;
  }

  if(fat32_find_file(&file.fat, pathname, root_dir, &file.fd))
  {
    fat32_free_dir(root_dir);
    return NULL;
  }

  file.offset_cluster = fat32_first_dir_cluster(&file.fd);

  p_file = malloc(sizeof(file));

  *p_file = file;

  fat32_free_dir(root_dir);

  return p_file;
}

/* int fat32_fread(struct fat32 *ctx, */
/*                 uint32_t begin_cluster, */
/*                 uint32_t offset_in_begin_cluster, */
/*                 void* buf, */
/*                 size_t buf_sz) */


size_t fread(void *restrict ptr, size_t size, size_t nitems, FILE *restrict stream)
{
  int remaining;

  remaining = fat32_fread(&stream->fat, stream->offset_cluster, (size_t)stream->offset_in_cluster, ptr, size * nitems);

  if (remaining == 0)
  {
    return nitems;
  }

  //printf("remaining %x\n", remaining);
  //printf("size %x\n", size);

  return (size_t)(remaining / (int)size);
}

int fseek(FILE *stream, off_t offset , int whence)
{
  switch (whence) {
    case SEEK_SET: {
      //printf("SEEK_SET %x\n", offset);
      stream->offset_in_cluster = offset;
      stream->offset_cluster = fat32_seek(&stream->fat, &stream->fd, (size_t*)&stream->offset_in_cluster);
      break;
    }

    case SEEK_CUR: {
      assert(0);
      //stream->offset += offset;
      //printf("SEEK_CUR %x\n", offset);
      break;
    }

    case SEEK_END:
    default:
      assert(0);
      break;
  }

  return 0;
}
