#include "cpuid.h"
#include <stdint.h>
#include "string.h"
#include "bios.h" /* for ioregs */

static void cpuid_call(struct bios_ioregs *regs)
{
  asm volatile ("cpuid \n"
      : "=a"(regs->eax), "=b"(regs->ebx), "=c"(regs->ecx), "=d"(regs->edx)
      : "a"(regs->eax)
      : /* Clobber */
      );
}

bool cpuid_is_present()
{
  uint32_t eflags, old_eflags;

  asm volatile ("pushfd \n"
       "pop eax \n"
       : "=a"(eflags)
       :
       :
       );

  old_eflags = eflags;

  eflags ^= 1 << 21;

  asm volatile ("push eax\n"
      "popfd \n"
      :
      : "a"(eflags)
      :
      );

  asm volatile ("pushfd \n"
       "pop eax \n"
       : "=a"(eflags)
       :
       :
       );

  asm volatile ("push eax\n"
      "popfd \n"
      :
      : "a"(old_eflags)
      :
      );

  return eflags != old_eflags;
}

bool cpuid_has_long_mode()
{
  struct bios_ioregs regs;

  memset(&regs, 0, sizeof regs);

  regs.eax = 0x80000000;

  cpuid_call(&regs);

  if (regs.eax < 0x80000000)
  {
    /* No extended function = no long mode */
    return false;
  }

  regs.eax = 0x80000001;

  cpuid_call(&regs);

  /* Is LM bit set? */

  if ((regs.edx & 1 << 29) == 0)
  {
    /* No :) */
    return false;
  }

  return true;
}
