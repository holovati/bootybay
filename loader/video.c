#include "video.h"
#include <stdint.h>

/* Some screen stuff. */
/* The number of columns. */
#define COLUMNS 80
/* The number of lines. */
#define LINES 25
/* The attribute of an character. */
#define ATTRIBUTE 7
/* The video memory address. */
#define VIDEO 0xB8000

/* Variables. */
/* Save the X position. */
static int xpos;
/* Save the Y position. */
static int ypos;
/* Point to the video memory. */
static volatile unsigned char *video;

void cls(void)
{
  int i;

  video = (unsigned char *)VIDEO;

  for (i = 0; i < COLUMNS * LINES * 2; i++)
  {
    *(video + i) = 0;
  }

  xpos = 0;
  ypos = 0;
}

static void scroll(void)
{
  // Get a space character with the default colour attributes.
  uint8_t attributeByte = (0 << 4) | (15 & 0x0F);
  uint16_t blank = (uint16_t)(0x20 | (attributeByte << 8));
  int i;

  uint16_t(*buf);
  buf = (uint16_t(*))VIDEO;

  if (ypos < LINES + 1)
  {
    return;
  }

  for (i = 0; i < (LINES * COLUMNS); i++)
  {
    buf[i] = buf[i + COLUMNS];
  }

  // The last line should now be blank. Do this by writing
  // 80 spaces to it.
  for (i = (LINES * COLUMNS); i < ((LINES + 1) * COLUMNS); i++)
  {
    buf[i] = blank;
  }

  ypos = LINES;
}

static void new_line(void)
{
  xpos = 0;
  ypos++;
  scroll();
}

void in_place_putchar(char character)
{
  *(video + (xpos + ypos * COLUMNS) * 2) = (uint8_t)(character & 0xFF);
  *(video + (xpos + ypos * COLUMNS) * 2 + 1) = ATTRIBUTE;
}

void _putchar(char character)
{
  if (character == '\n' || character == '\r')
  {
    new_line();
  }
  else
  {
    in_place_putchar(character);

    xpos++;

    if (xpos >= COLUMNS)
    {
      new_line();
    }
  }
}
