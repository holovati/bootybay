%define boot_addr 0x7c00
%define fat_cluster_buffer_ptr  (0x1000)
%define booty_load_addr (0x8000)

; DAP
%define dap_size 0
%define dap_null 1
%define dap_sector_count 2
%define dap_buffer_offset 4
%define dap_buffer_segment 6
%define dap_lba_hi 8
%define dap_lba_lo 12

%define dap_packet_size 16

; FAT 32 Byte Directory Entry Structure
%define DIR_Name			 0	; Size 11
%define DIR_Attr			 11	; Size 1
%define DIR_NTRes			 12	; Size 1
%define DIR_CrtTimeTenth		 13	; Size 1
%define DIR_CrtTime			 14	; Size 2
%define DIR_CrtDate                      16     ; Size 2
%define DIR_LstAccDate                   18     ; Size 2
%define DIR_FstClusHI			 20	; Size 2
%define DIR_WrtTime			 22	; Size 2
%define DIR_WrtDate			 24	; Size 2
%define DIR_FstClusLO			 26	; Size 2
%define DIR_FileSize			 28	; Size 4

%define DIR_Struct_Size 32

%macro ERROR 1
	mov si, %1
	jmp halt
%endmacro

bits 16
org boot_addr

BS_jmpBoot	: resb	 3
BS_OEMName	: resb	 8
BPB_BytsPerSec	: dw	 0
BPB_SecPerClus	: db	 0
BPB_RsvdSecCnt	: dw	 0
BPB_NumFATs	: db	 0
BPB_RootEntCnt	: dw	 0
BPB_TotSec16	: dw	 0
BPB_Media	: db	 0
BPB_FATSz16	: dw	 0
BPB_SecPerTrk	: dw	 0
BPB_NumHeads	: dw	 0
BPB_HiddSec	: dd	 0
BPB_TotSec32	: dd	 0
BPB_FATSz32	: dd	 0
BPB_ExtFlags	: dw	 0
BPB_FSVer	: dw	 0
BPB_RootClus	: dd	 0
BPB_FSInfo	: dw	 0
BPB_BkBootSec	: dw	 0
BPB_Reserved	: resb	 12
BS_DrvNum	: db	 0
BS_Reserved1	: db	 0
BS_BootSig	: db	 0
BS_VolID	: dd	 0
BS_VolLab	: resb	 11
BS_FilSysType	: resb	 8

	; Start
	cli
        mov     ax, 0
        mov     ds, ax
        mov     es, ax
        mov     fs, ax
        mov     gs, ax
	mov     ss, ax
        mov     ax, boot_addr
        mov     sp, ax
        sti

	; Find first cluster
	movzx   eax, BYTE [BPB_NumFATs]
        imul    eax, DWORD [BPB_FATSz32]
        mov     edx, eax
        movsx   eax, WORD [BPB_RsvdSecCnt]
        add     eax, DWORD [BPB_HiddSec]
        add     eax, edx
	mov dword[FirstCluster], eax

	; RootDirBuffer is placed after cluster buffer and is used for loading in the root dir
	xor eax, eax
	xor ecx, ecx
	mov ax, word [BPB_BytsPerSec] ; calc one cluster size in bytes
	mov cl, byte [BPB_SecPerClus]
	mul ecx
	mov cx, fat_cluster_buffer_ptr
	add ax, cx
	mov word [RootDirBuffer], ax

	; save the size of a cluster in bytes
	sub ax, fat_cluster_buffer_ptr
	mov word [ClusterSz], ax

	;set the Root directory is our begining
	mov eax, dword [BPB_RootClus]
	mov dword[cur_clst], eax

	mov bx, word [RootDirBuffer]
	call read_cluster_chain

	mov  di, bx
        mov  si, stage2fn
	mov  cx, stage2fnlen

.cmp_filename:

	push di
	push si
	push cx

        repe cmpsb

	pop cx
	pop si
	pop di

	jz .found

	;mov al, '-'
	;call print_char

	add di, DIR_Struct_Size ; increment di to next DIR

	jmp .cmp_filename
.found:
	;mov al, '+'
	;call print_char		;

	mov ax, word [di + DIR_FstClusHI]
	shl eax, 16
	mov ax, word [di + DIR_FstClusLO]

	mov dword[cur_clst], eax

	mov bx, booty_load_addr

	call read_cluster_chain
	jmp word 0:booty_load_addr
halt:
	mov al, 'E'
	call print_char
	cli ; clear interrupt flag
	hlt ; halt execution



; Returns ThisFATSecNum in eax, ThisFATEntOffset in edx for cur_clst
ent_for_cur_clst:
        push    bx
        mov     eax, DWORD[cur_clst]
        sal     eax, 2
        movsx   ecx, WORD [BPB_RsvdSecCnt]
        add     ecx, DWORD [BPB_HiddSec]
        movsx   ebx, WORD [BPB_BytsPerSec]
        mov     edx, 0
        div     ebx
        add     eax, ecx
        and     eax, 0x0FFFFFFF
        pop     bx
        ret

;Start sector for cur_clst, return in EAX, (GET FIRST SECTOR OF A CLUSTER
first_cur_clst_sector:
	mov     eax, DWORD [cur_clst]
        sub     eax, 2
        movzx   edx, BYTE [BPB_SecPerClus]
        imul    eax, edx
        add     eax, DWORD[FirstCluster]
        ret


; fetches the next cluster sector from the currently loaded fat
next_cluster:
	push di
	push bx
	; if booty is larger then 16k es will be > 0, and fat cluster buffer is at 0x1000 aka 0000:0x1000
	push es
	xor ax, ax
	mov es, ax
	;mov al, 'N'
	;call print_char

	call ent_for_cur_clst ;; eax holds the sector number for entry in cur_clst. dx holds offset

	mov dword [cur_clst], eax

	mov bx, fat_cluster_buffer_ptr

	call read_cluster

	mov di, dx

	mov eax, dword [bx + di]

	and eax, 0x0FFFFFFF
	pop es
	pop bx
	pop di
	ret

read_cluster_chain:
	pusha
	push es
.read_more:
	;mov al, ' '
	;call print_char

	;mov al, 'C'
	;call print_char

	push dword[cur_clst]

	call first_cur_clst_sector
	mov  dword[cur_clst], eax

	call read_cluster

	mov cx, word [ClusterSz]
	cmp bx, 0xF000
	jb  .cont

	;mov al, 'S'
  	;call print_char

	mov ax, es
	add ax, cx
	mov es, ax
.cont:
	add bx, cx

	pop dword[cur_clst]
	call next_cluster

	cmp eax, 0x0FFFFFF8	   ; we are eof or next cluster

	jae .exit

	mov dword[cur_clst], eax
	jmp .read_more
.exit:
	pop es
	popa
	ret


;--------------------------------------------------------------------------
; read_cluster - Read cluster from a partition using LBA addressing.
;
; Arguments:
;   ES:BX = pointer to where the sectors should be stored.
;
; Returns:
;   CF = 0 success
;        1 error
;
read_cluster:
	pusha                           ; save all registers
	mov     bp, sp                  ; save current SP

	;
	; Create the Disk Address Packet structure for the
	; INT13/F42 (Extended Read Sectors) on the stack.
	;

	; push    DWORD 0               ; offset 12, upper 32-bit LBA

	;mov al, 'R'
	;call print_char


	push    ds                      ; For sake of saving memory,
	push    ds                      ; push DS register, which is 0.
	push    dword[cur_clst]      ; offset 8, lower 32-bit LBA

	push    es                      ; offset 6, memory segment

	push    bx                      ; offset 4, memory offset

	xor     ax, ax                  ; offset 3, must be 0

	mov	al, byte [BPB_SecPerClus]

	push    ax                      ; offset 2, number of sectors

	push    WORD 16                 ; offset 0-1, packet size

	;
	; INT13 Func 42 - Extended Read Sectors
	;
	; Arguments:
	;   AH    = 0x42
	;   DL    = drive number (80h + drive unit)
	;   DS:SI = pointer to Disk Address Packet
	;
	; Returns:
	;   AH    = return status (sucess is 0)
	;   carry = 0 success
	;           1 error
	;
	; Packet offset 2 indicates the number of sectors read
	; successfully.
	;
	xor     ax, ax

	mov	dl, byte [BS_DrvNum]
	mov     si, sp
	mov     ah, 0x42
	int     0x13

	jnc .exit
	ERROR 2
.exit:
	mov     sp, bp                  ; restore SP
	popa
;	cmp bx, 0x8000
;	je halt


	ret

print_char:
	pusha
	mov     bx, 1                   ; BH=0, BL=1 (blue)
	mov     ah, 0x0e                ; bios INT 10, Function 0xE
	int     0x10                    ; display byte in tty mode
	popa
	ret

stage2fn         db      'BOOTYBAYBIN'
stage2fnlen      equ     $-stage2fn
ClusterSz        dw      0
RootDirBuffer    dw      0
FirstCluster     dd      0
cur_clst         dd      0


times 510 - ($-$$) db 0xFF ; pad remaining 510 bytes with zeroes
dw 0xaa55 ; magic bootloader magic - marks this 512 byte sector bootable!
