%define new_loc 0x7000
%define org_loc 0x7c00

%define first_pt_offset 0x01be

%define pt_boot 0
%define pt_start_head 1
%define pt_start_cy_sc 2
%define pt_system 4
%define pt_end_head 5
%define pt_end_cy_sc 6
%define pt_start_lba 8
%define pt_end_lba 12
%define pt_size 16

%define pt_boot_flag 0x80
%define pt_lba_flag 0x0c

bits 16
org 0x00

boot:
	cli

	mov     ax, 0
        mov     fs, ax
        mov     gs, ax
	mov     ss, ax
	mov     ds, ax
	mov	es, ax
	mov     sp, org_loc 	; stack below boot

	sti

	push dx 		; save boot drive

	mov bp, sp

	;check extended int 0x13
	mov ah, 0x41
	; dl is good
	mov bx, 0x55AA
	int 0x13
	mov si, no_int13_ex
	cmp bx, 0xAA55
	jne error

	; find bootable partition
	mov bx, org_loc + first_pt_offset ; begining of partition table
	mov si, no_bootable_part	  ; error msg
.loop:
	cmp bx, (pt_size * 4)
	je error

	mov dl, byte [bx]
	cmp dl, pt_boot_flag
	je .found_boot

 	add bx, pt_size
	;;  gdb -ex 'set architecture i8086' -ex 'set disassembly-flavor intel' -ex 'layout regs' -ex 'break *0x7000' -ex 'target remote localhost:1234' -ex 'continue'
.found_boot:
	; check if partition is using lba
	mov si, no_lba_part 	; error msg
	mov al, byte [bx + pt_system]
	cmp al, pt_lba_flag
	jne error


	mov ecx, dword [bx + pt_start_lba]
	push cx 		; push start lbaLO
	shr ecx, 16
	push cx 		; push start lbaHI

	; relocate and jump
	cld
	mov	si, org_loc	; move from ds:si
	mov   	di, new_loc	; move to es:di
	mov	cx, 0x0100 	; how many times to rep movsw
	rep	movsw           ; move boot code to the 0x7000

	jmp     new_loc >> 4:load_vbr
error:
	jmp     org_loc >> 4:error_exit

	; we are at 0x7000 here
load_vbr:
				; restore lba into ecx
	mov si, sp
	mov cx, word [si] 	; lbahi
	shl ecx, 16
	mov cx, word [si + 2]	; lbalo

	sub sp, 0x10		; make room for dap on the stack

	mov si, sp		; set DAP pointer

	; DAP is on the stack
	mov byte [si + 0], 0x10 ;packet size
	mov byte [si + 1], 0x00 ;reserved
	mov word [si + 2], 0x01 ; n sectors
	mov word [si + 4], org_loc ;buffer offset
	mov word [si + 6], 0  ;buffer segment
	mov dword [si + 8], ecx ; lbahi
	mov dword [si + 12], 0x00 ; lbalo

	mov ah, 0x42		; read sector
	mov dx, word [bp] 	; boot drive

	int 0x13

	jc error

	add sp, 0x10 		; clear dap from stack

	mov byte [org_loc + 64], dl ; ensure boot drive is correct in bpb

	jmp 0 : org_loc

read_error:
	mov si, read_failed
	jmp     org_loc >> 4:error_exit

error_exit:
	add si, org_loc
	mov ah,0x0e
.loop:
	lodsb
	or al,al
	jz halt
	int 0x10
	jmp .loop
halt:
	cli
	hlt

no_int13_ex:
	db "Extended Int13 not supported!", 0

no_bootable_part:
	db "No bootable partition found!", 0

no_lba_part:
	db "Partition is not formated with LBA!", 0

read_failed:
	db "Failed to read VBR!", 0


	times 510 - ($-$$) db 0
	dw 0xaa55
