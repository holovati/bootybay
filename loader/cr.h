#ifndef CR_H
#define CR_H
#include <stdint.h>

#define  CR0_PE          (1ULL << 0)    /* Protection Enabled                             R/W */
#define  CR0_MP          (1ULL << 1)    /* Monitor Coprocessor                            R/W */
#define  CR0_EM          (1ULL << 2)    /* Emulation                                      R/W */
#define  CR0_TS          (1ULL << 3)    /* Task Switched                                  R/W */
#define  CR0_ET          (1ULL << 4)    /* Extension Type                                 R   */
#define  CR0_NE          (1ULL << 5)    /* Numeric Error                                  R/W */
#define  CR0_WP          (1ULL << 16)    /* Write Protect                                  R/W */
#define  CR0_AM          (1ULL << 18)    /* Alignment Mask                                 R/W */
#define  CR0_NW          (1ULL << 29)    /* Not Writethrough                               R/W */
#define  CR0_CD          (1ULL << 30)    /* Cache Disable                                  R/W */
#define  CR0_PG          (1ULL << 31)    /* Paging                                         R/W */

#define  CR3_PWT         (1ULL << 3)   /* Page-Level Writethrough                         R/W */
#define  CR3_PCD         (1ULL << 4)   /* Page-Level Cache Disable                        R/W */

#define  CR4_VME         (1ULL << 0)   /* Virtual-8086 Mode Extensions                    R/W */
#define  CR4_PVI         (1ULL << 1)   /* Protected-Mode Virtual Interrupts               R/W */
#define  CR4_TSD         (1ULL << 2)   /* Time Stamp Disable                              R/W */
#define  CR4_DE          (1ULL << 3)   /* Debugging Extensions                            R/W */
#define  CR4_PSE         (1ULL << 4)   /* Page Size Extensions                            R/W */
#define  CR4_PAE         (1ULL << 5)   /* Physical-Address Extension                      R/W */
#define  CR4_MCE         (1ULL << 6)   /* Machine Check Enable                            R/W */
#define  CR4_PGE         (1ULL << 7)   /* Page-Global Enable                              R/W */
#define  CR4_PCE         (1ULL << 8)   /* Performance-Monitoring Counter Enable           R/W */
#define  CR4_OSFXSR      (1ULL << 9)   /* Operating System FXSAVE/FXRSTOR Support         R/W */
#define  CR4_OSXMMEXCPT  (1ULL << 10)   /* Operating System Unmasked Exception Support     R/W */
#define  CR4_FSGSBASE    (1ULL << 16)   /* RDFSBASE, RDGSBASE, WRFSBASE, WRGSBASE instr    R/W */
#define  CR4_OSXSAVE     (1ULL << 18)   /* XSAVE and Processor Extended States Enable Bit  R/W */

static inline uint32_t cr0_read(void)
{
  uint32_t res;

  asm volatile("mov %0, cr0" : "=r"(res) : : );

  return res;
}

static inline void cr0_write(uint32_t val)
{
  asm volatile("mov cr0, %0" : : "r"(val) : );
}

static inline uint32_t cr3_read(void)
{
  uint32_t res;

  asm volatile("mov %0, cr3" : "=r"(res) : : );

  return res;
}

static inline void cr3_write(uint32_t val)
{
  asm volatile("mov cr3, %0" : : "r"(val) : );
}

static inline uint32_t cr4_read(void)
{
  uint32_t res;

  asm volatile("mov %0, cr4" : "=r"(res) : : );

  return res;
}

static inline void cr4_write(uint32_t val)
{
  asm volatile("mov cr4, %0" : : "r"(val) : );
}

#endif /* CR_H */
