#ifndef CPU_H
#define CPU_H

#include "gdt.h"

#define CPU_EFLAGS_CF (1ULL << 0)

void cpu_halt(void);

int cpu_a20_active(void);

int cpu_long_mode_active(void);

void cpu_enable_long_mode(void);

void cpu_enable_pae(void);

void cpu_set_page_table(void *pt);

void cpu_enable_paging(void);

void cpu_load_gdt(void *address, uint16_t size);

#endif /* CPU_H */
