#ifndef DISK_H
#define DISK_H

#include <stddef.h>
#include <stdint.h>

uint32_t disk_rd_sectors(uint32_t drv, uint64_t sct, size_t cnt, void *out, size_t out_sz);

uint32_t disk_reset(uint32_t drv);

#endif /* DISK_H */
