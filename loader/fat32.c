#include "fat32.h"
#include "disk.h"
#include "stdio.h"
#include "string.h"
#include "flag.h"
#include "assert.h"
#include "malloc.h"

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

#define MBR_SIGNATURE 0xAA55

#define MBR_PARTITION_TYPE_FAT32_LBA 0x0C

struct mbr_partition
{
  u32 status    : 8;
  u32 chs_start : 24;
  u32 type      : 8;
  u32 chs_end   : 24;
  u32 lba_start;
  u32 n_sectors;
};

struct mbr
{
  u8 code[0x1BE];
  struct mbr_partition partition[4];
  u16 signature;
} __attribute__((packed));

static inline size_t cluster_size(struct fat32 *ctx)
{
  return (size_t)(ctx->BPB_BytsPerSec * ctx->BPB_SecPerClus);
}

static inline
uint32_t first_cluster_section(struct fat32 *ctx, uint32_t clst)
{
  uint32_t first_data_sector;

  first_data_sector = (ctx->BPB_NumFATs * ctx->BPB_FATSz32);

  first_data_sector += ctx->BPB_HiddSec;

  first_data_sector += ctx->BPB_RsvdSecCnt;

  return (((clst - 2) * ctx->BPB_SecPerClus) + first_data_sector);
}

static inline
uint32_t fat32_rd_clsts(struct fat32 *ctx, uint32_t clst, void* out, size_t out_sz)
{
  uint32_t first_data_sector;

  first_data_sector = first_cluster_section(ctx, clst);

  return disk_rd_sectors(ctx->BS_DrvNum,
                         first_data_sector,
                         ctx->BPB_SecPerClus,
                         out,
                         out_sz);
}

static uint32_t ent_for_clst(struct fat32 *ctx, uint32_t clst, uint32_t *offset)
{
  uint32_t FATOffset, ThisFATSecNum, ThisFATEntOffset;

  FATOffset = clst * 4;

  ThisFATSecNum = (ctx->BPB_HiddSec + ctx->BPB_RsvdSecCnt);
  ThisFATSecNum += (FATOffset / ctx->BPB_BytsPerSec);

  ThisFATEntOffset = FATOffset % ctx->BPB_BytsPerSec;

  if (offset != NULL)
  {
    *offset = ThisFATEntOffset;
  }

  return ThisFATSecNum & 0x0FFFFFFF;
}

static uint32_t next_cluster(struct fat32 *ctx, uint32_t clst)
{
  uint32_t fat[128];
  uint32_t fat_sector, fat_offset;
  uint32_t read_res;

  fat_sector = ent_for_clst(ctx, clst, &fat_offset);

  read_res = disk_rd_sectors(ctx->BS_DrvNum, fat_sector, 1, &fat[0], sizeof(fat));

  if (read_res != 0)
  {
    printf("Error while reading FAT for cluster %d\n", clst);
    return 0xFFFFFFFF;
  }

  if (fat_offset > 0)
  {
    fat_offset /= 4;
  }

  return (fat[fat_offset] & 0x0FFFFFFF);
}

static uint32_t get_cluster_count(struct fat32 *ctx, uint32_t cluster)
{
  uint32_t count;

  count = 0;

  while ((cluster = next_cluster(ctx, cluster)) < FAT32_EOC)
  {
    count++;
  }
  return count;
}

uint32_t fat32_init(struct fat32 *ctx, uint32_t drv)
{
  uint32_t res, idx;
  struct mbr boot_mbr;
  struct mbr_partition *boot_part;

  memset(ctx, 0, sizeof *ctx);

  assert(drv != 0);

  disk_reset(drv);

  /* What if its not 0x80 ? */
  disk_rd_sectors(drv, 0, 1, &boot_mbr, sizeof(boot_mbr));

  boot_part = NULL;
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Waddress-of-packed-member"
  for(idx = 0; idx < 4; ++idx)
  {
    if (boot_mbr.partition[idx].type == MBR_PARTITION_TYPE_FAT32_LBA &&
        boot_mbr.partition[idx].status == 0x80 &&
        boot_part == NULL)
    {
      boot_part = &boot_mbr.partition[idx];
    }
  }
#pragma GCC diagnostic pop
  assert(boot_part != NULL);

  res = disk_rd_sectors(drv, boot_part->lba_start, 1, ctx, sizeof *ctx);

  assert(res == 0);

  assert(ctx->signature == MBR_SIGNATURE);

  assert(ctx->BS_jmpBoot[0] != 0);

  return 0;
}

struct fat32_dir *fat32_get_root_dir(struct fat32 *ctx)
{
  uint32_t cur_cluster, cluster_count;
  struct fat32_dir *dir;

  cluster_count = get_cluster_count(ctx, ctx->BPB_RootClus);

  dir = malloc(cluster_size(ctx) * cluster_count);

  memset(dir, 0, cluster_size(ctx));

  cur_cluster = ctx->BPB_RootClus;

  while (cur_cluster < FAT32_EOC)
  {
    printf("Reading cluster(DIR) 0x%x\n", cur_cluster);

    if (fat32_rd_clsts(ctx, cur_cluster, dir, cluster_size(ctx)))
    {
      free(dir);
      return NULL;
    }

    printf("Reading cluster(DIR) 0x%x OK\n", cur_cluster);

    cur_cluster = next_cluster(ctx, cur_cluster);
  }

  return dir;
}

void fat32_free_dir(struct fat32_dir *dir)
{
  free(dir);
}

int fat32_find_file(struct fat32 *ctx, const char* fname, struct fat32_dir *dir, struct fat32_dir *file_out)
{
  uint8_t dir_nm;

  (void)ctx;

  for( ;(uint8_t)dir->DIR_Name[0] != 0x00; ++dir)
  {
    dir_nm = (uint8_t)dir->DIR_Name[0];

    if (dir_nm == 0x05)
    {
      printf("KANJI (0xE5). skip\n");
      continue;
    }
    else if(dir_nm == 0x20)
    {
      printf("DIR_Name[0] == 0x20. skip\n");
      continue;
    }
    else if(dir_nm ==  0xE5)
    {
      printf("There is no file or directory name in this entry. check next\n");
      continue;
    }

    /* Cannot parse lfn yet */
    if (HAS_FLAG(dir->DIR_Attr, FAT32_ATTR_LONG_NAME))
    {
      continue;
    }

    /* Root dir, not usable */
    if (HAS_FLAG(dir->DIR_Attr, FAT32_ATTR_VOLUME_ID))
    {
      continue;
    }

    if (memcmp(dir->DIR_Name, fname, 11) == 0)
    {
      *file_out = *dir;
      printf("Found %s\n", fname);
      return 0;
    }
  }

  return 1;
}

uint32_t fat32_first_dir_cluster(struct fat32_dir *file)
{
  uint32_t cur_cluster;

  cur_cluster = file->DIR_FstClusHI;
  cur_cluster <<= 16;
  cur_cluster |= file->DIR_FstClusLO;

  return cur_cluster;
}

uint32_t fat32_seek(struct fat32 *ctx, struct fat32_dir *file, size_t *offset)
{
  uint32_t cluster_cnt, cur_cluster;

  cluster_cnt = (*offset / cluster_size(ctx));

  for (cur_cluster = fat32_first_dir_cluster(file);
       cluster_cnt != 0;
       cur_cluster = next_cluster(ctx, cur_cluster), --cluster_cnt)
  {
    if (cur_cluster >= FAT32_EOC)
    {
      *offset = 0;
      printf("WARNING fat32_seek: reached EOF while seeking\n");
      return fat32_first_dir_cluster(file);
    }
    //printf("Seeking to %x\n", *offset);
  }

  *offset %= cluster_size(ctx);

  //printf("fat32_seek done: cur_clst %x, offset in cluster %x\n", cur_cluster, *offset);

  return cur_cluster;
}

int fat32_fread(struct fat32 *ctx, uint32_t data_cluster, uint32_t data_offset, void* buf, size_t buf_sz)
{
  size_t out_off, in_off;
  uint8_t *out_buf, *in_buf;

  size_t bytes_to_copy;

  uint32_t clst_size, cur_cluster, bytes_to_read;

  clst_size = cluster_size(ctx);

  bytes_to_read = buf_sz;

  cur_cluster = data_cluster;

  out_buf = buf;
  out_off = 0;

  in_buf = alloca(clst_size);
  in_off = data_offset;

  //printf("fat32 offset %x\n", offset);
  //printf("fat32_fread: begin cluster %x\n", begin_cluster);
  //printf("fat32_fread: custers to read %x\n", clusters_to_read);
  //printf("fat32_fread: begin offset %x\n", in_off);

  while(bytes_to_read != 0) /* Better to loop forever then do a wrong read */
  {
    //printf("fat32_fread: reading cluster %x\n", cur_cluster);

    assert(cur_cluster < FAT32_EOC);

    if (fat32_rd_clsts(ctx, cur_cluster, &in_buf[0], clst_size))
    {
      return 1;
    }

    bytes_to_copy = bytes_to_read / clst_size;

    if (bytes_to_copy == 0)
    {
      /* less then a full cluster, get remainder */
      bytes_to_copy = bytes_to_read % clst_size;
    }
    else
    {
      /* partial or full cluster, depending on in_off */
      bytes_to_copy = clst_size - in_off;
    }

    assert(bytes_to_copy != 0);

    memcpy(&out_buf[out_off], &in_buf[in_off], bytes_to_copy);

    out_off += bytes_to_copy;

    bytes_to_read -= bytes_to_copy;

    /* if there is more data after the first read, offset in the next cluster is always 0 */
    in_off = 0;

    cur_cluster = next_cluster(ctx, cur_cluster);

    //printf("fat32_fread: bytes to read %x\n", bytes_to_copy);
    //printf("fat32_fread: done reading cluster %x\n", clusters_to_read);
  }

  return (int)bytes_to_read;
}
