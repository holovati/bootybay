#include "bios.h"
#include "cpu.h"
#include "string.h"
#include "stdio.h"
#include "assert.h"
#include "flag.h"

extern uint8_t bios_call_int_patch;
extern uintptr_t real16;

BOOTYCALL void bios_call_internal(struct bios_ioregs *regs);

uint32_t bios_call(uint32_t int_n, struct bios_ioregs *regs)
{
  bios_call_int_patch = (uint8_t)int_n;
  bios_call_internal(regs);
  return regs->flags & 0x1;
}

void bios_print_ioregs(struct bios_ioregs *regs)
{
  printf("EAX=%x EBX=%x ECX=%x EDX=%x\n", regs->eax, regs->ebx, regs->ecx, regs->edx);
  printf("ESI=%x EDI=%x\n", regs->esi, regs->edi);
  printf("ES=%x\n", regs->es);
  printf("DS=%x\n", regs->ds);
  printf("FS=%x\n", regs->fs);
  printf("GS=%x\n", regs->gs);
}

void bios_set_video_mode(uint8_t mode)
{
  struct bios_ioregs regs;

  memset(&regs, 0, sizeof regs);

  regs.al = mode;

  assert(bios_call(0x10, &regs) == 0);
}

static_assert(sizeof(struct bios_address_range_descriptor) == 24);

#define SMAP 0x534D4150

int bios_query_address_map(struct bios_address_range_descriptor *ard)
{
  int count;
  struct bios_ioregs regs;
  struct bios_address_range_descriptor descriptor;

  memset(&regs, 0, sizeof regs);

  count = 0;

  do
  {
    regs.eax = 0xE820;
    regs.es = FLAT_TO_SEGMENT(&descriptor);
    regs.di = FLAT_TO_OFFSET(&descriptor);
    regs.ecx = sizeof(descriptor);
    regs.edx = SMAP;

    assert(bios_call(0x15, &regs) == 0);

    assert(regs.eax == SMAP);

    assert(!((regs.ecx < 0x14) || (regs.ecx > 0x18)));

    if (ard != NULL)
    {
      ard[count] = descriptor;
    }

    ++count;

  } while (regs.ebx != 0);

  return count;
}

__attribute__((noinline)) static uint16_t *vesa_get_mode_ptr(void)
{
  struct vbe_info vbei;
  memset(&vbei, 0, sizeof(vbei));

  vbei.signature[0] = 'V';
  vbei.signature[1] = 'B';
  vbei.signature[2] = 'E';
  vbei.signature[3] = '2';

  struct bios_ioregs ior;

  memset(&ior, 0, sizeof(ior));

  ior.ax = 0x4f00;
  ior.es = FLAT_TO_SEGMENT(&vbei);
  ior.di = FLAT_TO_OFFSET(&vbei);

  uint16_t *retval = NULL;

  bios_call(0x10, &ior);

  if (ior.al == 0x4f && ior.ah == 0)
  {
    retval = (uint16_t *)vbei.video_modes;
  }

  return retval;
}

int bios_enum_vesa_modes(vesa_mode_callback_t a_cb, void *a_user_data)
{
  int mode_count = 0;
  vesa_mode_info_t vmi;
  struct bios_ioregs ior;

  for (uint16_t *vmode = vesa_get_mode_ptr(); vmode != NULL && *vmode != 0xFFFF; vmode++)
  {
    memset(&vmi, 0, sizeof(vmi));
    memset(&ior, 0, sizeof(ior));

    ior.ax = 0x4f01;
    ior.cx = *vmode;
    ior.es = FLAT_TO_SEGMENT(&vmi);
    ior.di = FLAT_TO_OFFSET(&vmi);

    bios_call(0x10, &ior);

    if (ior.al != 0x4f /*|| ior.ah != 0*/)
    {
      printf("[FAIL] %s:%d\n",__FILE__, __LINE__);
      bios_print_ioregs(&ior);
      break;
    }

    mode_count++;

    int cb_ret = a_cb(*vmode, &vmi, a_user_data);

    if (cb_ret != 0)
    {
      if ((cb_ret & 0x1FF) == (((int)*vmode) & 0x1FF))
      {
        // Set the mode
        memset(&ior, 0, sizeof(ior));
        ior.ax = 0x4f02;
        ior.bx = (uint16_t)cb_ret;
        bios_call(0x10, &ior);
      }
    }
  }

  return mode_count;
}
