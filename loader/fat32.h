#ifndef FAT32_H
#define FAT32_H
#include <stddef.h>
#include <stdint.h>
#include "assert.h"

#define FAT32_EOC 0x0FFFFFF8

#define MBR_SIGNATURE 0xAA55
#define PARTITION_SYSYEM_FAT32_LBA 0xC
#define PARTITION_STATE_ACTIVE 0x80

struct fat32_pte
{
  uint8_t state;
  uint8_t start_head;  /* 6 bits for start sector, 2 for cylinder HI */
  uint8_t start_cyHI_sc;
  uint8_t start_cyLO;
  uint8_t system;
  uint8_t end_head;  /* 6 bits for end sector, 2 for cylinder HI */
  uint8_t end_cyHI_sc;
  uint8_t end_cyLO;
  uint32_t start_lba;
  uint32_t end_lba;
};

#define FAT32_ATTR_READ_ONLY 0x01
#define FAT32_ATTR_HIDDEN    0x02
#define FAT32_ATTR_SYSTEM    0x04
#define FAT32_ATTR_VOLUME_ID 0x08
#define FAT32_ATTR_DIRECTORY 0x10
#define FAT32_ATTR_ARCHIVE   0x20
#define FAT32_ATTR_LONG_NAME (FAT32_ATTR_READ_ONLY | FAT32_ATTR_HIDDEN | \
                              FAT32_ATTR_SYSTEM | FAT32_ATTR_VOLUME_ID)

struct fat32_dir
{
  char     DIR_Name[11];
  uint8_t  DIR_Attr;
  uint8_t  DIR_NTRes;
  uint8_t  DIR_CrtTimeTenth;
  uint16_t DIR_CrtTime;
  uint16_t DIR_CrtDate;
  uint16_t DIR_LstAccDate;
  uint16_t DIR_FstClusHI;
  uint16_t DIR_WrtTime;
  uint16_t DIR_WrtDate;
  uint16_t DIR_FstClusLO;
  uint32_t DIR_FileSize;
};

struct fat32
{
  uint8_t     BS_jmpBoot[3];
  char        BS_OEMName[8];
  uint16_t    BPB_BytsPerSec;
  uint8_t     BPB_SecPerClus;
  uint16_t    BPB_RsvdSecCnt;
  uint8_t     BPB_NumFATs;
  uint16_t    BPB_RootEntCnt;
  uint16_t    BPB_TotSec16;
  uint8_t     BPB_Media;
  uint16_t    BPB_FATSz16;
  uint16_t    BPB_SecPerTrk;
  uint16_t    BPB_NumHeads;
  uint32_t    BPB_HiddSec;
  uint32_t    BPB_TotSec32;
  uint32_t    BPB_FATSz32;
  uint16_t    BPB_ExtFlags;
  uint16_t    BPB_FSVer;
  uint32_t    BPB_RootClus;
  uint16_t    BPB_FSInfo;
  uint16_t    BPB_BkBootSec;
  uint8_t     BPB_Reserved[12];
  uint8_t     BS_DrvNum;
  uint8_t     BS_Reserved1;
  uint8_t     BS_BootSig;
  uint32_t    BS_VolID;
  char        BS_VolLab[11];
  char        BS_FilSysType[8];
  uint8_t     code[420];
  uint16_t    signature;
} __attribute__((packed));

static_assert(sizeof(struct fat32) == 512);

uint32_t fat32_init(struct fat32 *ctx, uint32_t drv);

struct fat32_dir *fat32_get_root_dir(struct fat32 *ctx);

void fat32_free_dir(struct fat32_dir *dir);

uint32_t fat32_first_dir_cluster(struct fat32_dir *file);

uint32_t fat32_seek(struct fat32 *ctx, struct fat32_dir *file, size_t *offset);

int fat32_find_file(struct fat32 *ctx, const char* fname, struct fat32_dir *dir, struct fat32_dir *file_out);

int fat32_fread(struct fat32 *ctx, uint32_t data_cluster, uint32_t data_offset, void* buf, size_t buf_sz);

#endif /* FAT32_H */
