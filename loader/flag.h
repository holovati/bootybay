#ifndef FLAG_H
#define FLAG_H

#define SET_FLAG(x, f) (x) |= (f)

#define CLR_FLAG(x, f) (x) &= ~(f)

#define HAS_FLAG(x, f) (((x) & (f)) != 0)

#endif /* FLAG_H */
