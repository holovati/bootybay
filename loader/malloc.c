#include "malloc.h"
#include <stddef.h>
#include <stdint.h>
#include "stdio.h"
#include "assert.h"

static void *align_ptr(void* p, uintptr_t align)
{
  uintptr_t ptr;
  ptr = (uintptr_t)(p);
  ptr += align - 1;
  return (void*)(ptr &~ (align - 1));
}

void *aligned_alloc(unsigned sz, unsigned align)
{
  static void* ptr_pos = (void*)0x100000;
  void* res;

  assert(ptr_pos < (void*)0x200000);

  res = align_ptr(ptr_pos, align);

  ptr_pos = res;

  ptr_pos = (void*)((uintptr_t)(ptr_pos) + sz);

  return res;
}
