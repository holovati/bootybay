#ifndef STDIO_H
#define STDIO_H
#include "printf.h"

typedef struct fat_file FILE;
typedef long off_t;
typedef void* fpos_t;

#define SEEK_SET 0
#define SEEK_CUR 1
#define SEEK_END 2

int      fclose(FILE *);
FILE    *fdopen(int, const char *);
int      feof(FILE *);
int      ferror(FILE *);
int      fflush(FILE *);
FILE    *fopen(const char *restrict pathname, const char *restrict mode);
size_t   fread(void *restrict ptr, size_t size, size_t nitems, FILE *restrict stream);
int      fseek(FILE *stream, off_t offset , int whence);
int      fseeko(FILE *, off_t, int);
int      fsetpos(FILE *, const fpos_t *);
long     ftell(FILE *);
off_t    ftello(FILE *);
size_t   fwrite(const void *restrict, size_t, size_t, FILE *restrict);

#endif /* STDIO_H */
