; gdt entry base(32 bit), limit(20 bit), access(8 bit), flags(4 bit)
%macro GDTE 4
	dw (%2 & 0xFFFF) 	; limit 0  : 15
	dw (%1 & 0xFFFF)	; base  0  : 15
	db ((%1 >> 16) & 0xFF)  ; base  16 : 23
	db (%3)			; access byte
	db (%4 << 4) | (((%2 >> 16) & 0xF))  ; limit 16 : 19 and flags
	db ((%1 >> 25) & 0xFF)		    ; base 24 : 31
%endmacro
