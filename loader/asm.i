%macro ENTRY 1
	global %1
	align 4
	%1
%endmacro

%macro  prologue 1
        push    ebp
        mov     ebp,esp
        sub     esp,%1
%endmacro

%macro  epilogue 1
	add     esp, %1
        pop     ebp
%endmacro

%macro  prologue 0
        push    ebp
        mov     ebp,esp
%endmacro

%macro  epilogue 0
        pop    ebp
%endmacro
