#ifndef MALLOC_H
#define MALLOC_H

void *aligned_alloc(unsigned sz, unsigned align);

#define malloc(x) aligned_alloc(x, sizeof(uintptr_t))

#define free(x) ((void)x)

#define alloca(x) __builtin_alloca((x))

#endif /* MALLOC_H */
