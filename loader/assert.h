#ifndef ASSERT_H
#define ASSERT_H

#define assert_stringify(x) #x
#define assert_tostring(x) assert_stringify(x)

#define static_assert_cc__(x, y) x ## y
#define static_assert_cc_(x, y) static_assert_cc__(x, y)
#define static_assert(exp) typedef int static_assert_cc_(static_assert, __LINE__) [((exp) * 2) - 1]

#define assert(exp)                                                     \
  do                                                                    \
  {                                                                     \
    if ((exp) == 0)                                                     \
    {                                                                   \
      printf("(%s) failed in (%s : %d)\n",                              \
             # exp,                                                     \
             __FILE__,                                                  \
             __LINE__);                                                 \
      asm volatile (".die" assert_tostring(__LINE__) ":    \n"          \
                    "cli                                   \n"          \
                    "hlt                                   \n"          \
                    "jmp .die" assert_tostring(__LINE__) " \n");        \
    }                                                                   \
  } while(0)

#endif /* ASSERT_H */
