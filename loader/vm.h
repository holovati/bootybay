#ifndef VM_H
#define VM_H
#include <stdint.h>
#include "assert.h"

typedef uint64_t u64;

#define VM_PAGE_PRESENT       (UINT64_C(1) << 0)
#define VM_PAGE_RW            (UINT64_C(1) << 1)
#define VM_PAGE_USER          (UINT64_C(1) << 2)
#define VM_PAGE_WRITETHROUGH  (UINT64_C(1) << 3)
#define VM_PAGE_CACHE_DISABLE (UINT64_C(1) << 4)
#define VM_PAGE_ACCESSED      (UINT64_C(1) << 5)
#define VM_PAGE_DIRTY         (UINT64_C(1) << 6)
#define VM_PAGE_SIZE          (UINT64_C(1) << 7)
#define VM_PAGE_GLOBAL        (UINT64_C(1) << 8)
#define VM_PAGE_NO_EXECUTE    (UINT64_C(1) << 63)

#define PAGE_OFFSET_MASK 0xfff
#define PAGE_TABLE_OFFSET_MASK 0x1ff

#define PT4_IDX(p)  (((p) >> 39) & 0x1FF) /*512 gb*/
#define PT3_IDX(p)   (((p) >> 30) & 0x1FF) /*1 gb*/
#define PT2_IDX(p)    (((p) >> 21) & 0x1FF) /*2 mb*/
#define PT1_IDX(p)    (((p) >> 12) & 0x1FF)

#define PT_IDX(i, p)    (((p) >> (12 + (((i) - 1) * 9)) & 0x1FF))


#define PG_OFFSET(p) (((p) >>  0) & 0xFFF)

union vm_pte
{
  struct
  {
    u64 present        : 1;  /* 0 - 1 */
    u64 rw             : 1;  /* 1 - 2 */
    u64 user           : 1;  /* 2 - 3 */
    u64 writethrough   : 1;  /* 3 - 4 */
    u64 cache_disable  : 1;  /* 4 - 5 */
    u64 accessed       : 1;  /* 5 - 6 */
    u64 dirty          : 1;  /* 6 - 7 */
    u64 size           : 1;  /* 7 - 8 */
    u64 global         : 1;  /* 8 - 9 */
    u64 avl3           : 3;  /* 9 - 12 */
    u64 frame          : 40; /* 12 - 52 */
    u64 avl11          : 11; /* 52 - 63 */
    u64 no_execute     : 1;  /* 63 - 64 */
  };
  u64 uint;
  void *ptr;
};

static_assert(sizeof(union vm_pte) == 0x08);

#define vm_pte_frame(p) ((void*)((uintptr_t)(pte[pt_idx].frame) << 12))

#endif /* VM_H */
