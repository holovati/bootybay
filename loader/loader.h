#ifndef ELF_LOADER_H
#define ELF_LOADER_H

typedef void (*elf_symbol_cb)(unsigned long addr, unsigned long size, const char *symnm, unsigned long symnmlen, void *ud);

void *elf_loader_loadfile(char *fname);

int elf_loader_ldsyms(elf_symbol_cb cb, void *ud);

#endif /* ELF_LOADER_H */
