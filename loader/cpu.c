#include "cpu.h"
#include "msr.h"
#include "cr.h"
#include "flag.h"
#include "string.h"
#include "malloc.h"
#include "stdio.h"

void cpu_halt(void)
{
  printf("CPU halted!\n");
  asm volatile(".loop: cli \n hlt \n jmp .loop");
}
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Warray-bounds"
int cpu_a20_active()
{
  uint32_t *cafebabe;

  cafebabe = (void*)0x500;

  *cafebabe = 0xCAFEBABE;

  cafebabe = (void*)(0xFFFFF + 1 + 0x500);

  return *cafebabe != 0xCAFEBABE;
}
#pragma GCC diagnostic pop

int cpu_long_mode_active()
{
  uint64_t efer;

  efer = msr_read(MSR_EFER);

  return HAS_FLAG(efer, MSR_EFER_LMA);
}

void cpu_enable_long_mode()
{
  uint64_t efer;

  efer = msr_read(MSR_EFER);

  SET_FLAG(efer, MSR_EFER_LME);

  msr_write(MSR_EFER, efer);
}

void cpu_enable_pae()
{
  uint32_t cr4;

  cr4 = cr4_read();

  SET_FLAG(cr4, CR4_PAE);

  cr4_write(cr4);
}

void cpu_set_page_table(void *pt)
{
  cr3_write((uintptr_t)pt);
}

void cpu_enable_paging()
{
  uint32_t cr0;;

  cr0 = cr0_read();

  SET_FLAG(cr0, CR0_PG);

  cr0_write(cr0);
}

void cpu_load_gdt(void *address, uint16_t size)
{
  static struct gdt_register gdtr;

  gdtr.size = (uint16_t)(size - 1);
  gdtr.ptr = address;

  asm volatile("lgdt [eax]" : : "a"(&gdtr));
}
