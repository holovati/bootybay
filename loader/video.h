#ifndef VIDEO_H
#define VIDEO_H
#include "printf.h"

void cls(void);

void in_place_putchar(char character);

#endif /* VIDEO_H */
