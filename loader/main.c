#include <stddef.h>
#include "string.h"
#include "bios.h"
#include "cpuid.h"
#include "fat32.h"
#include "stdio.h"
#include "cpu.h"
#include "vm.h"
#include "flag.h"
#include "assert.h"
#include "loader.h"
#include "malloc.h"
#include "video.h"
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpadded"
#include "multiboot2.h"
#pragma GCC diagnostic pop

struct system_memmap
{
  uint32_t count;
  uint32_t descriptor_ptr;
};

struct system_memmap g_sys_memmap;

struct multiboot_tag_framebuffer g_mbi __attribute__((aligned(MULTIBOOT_TAG_ALIGN)));

const uint64_t g_kernel_load_addr = 0xffffffff80200000;

char *g_symstart, *g_symend;

void print_error(int code);

void print_error(int code)
{
  printf("Error %d\n", code);
}

uint64_t g_kernel_entry_point;

static void init_gdt(u64 *gdt)
{
  gdt[0] = 0;
  gdt[1] = GDT_DESCRIPTOR_GDT | GDT_PRESENT | GDT_LONG | GDT_EXECUTABLE;
  gdt[2] = GDT_DESCRIPTOR_GDT | GDT_PRESENT | GDT_DATA_WRITABLE;
}

#define page_align(n) ((((uintptr_t)(n)) + 0x1000 - 1) & ((uintptr_t) ~(0x1000 - 1)))

static void vm_map(union vm_pte *pt4, void *phys, u64 virt, size_t sz)
{
  size_t page, n_pages, level, pt_idx;
  u64 cur_phys, cur_virt;
  union vm_pte *pte;

  n_pages = page_align(sz) >> 12; /* page_align(sz) / 0x1000 */

  cur_phys = (u64)((uintptr_t)phys);
  cur_virt = (u64)virt;

  for (page = 0; page < n_pages; ++page)
  {
    pte = pt4;

    for (level = 4; level > 0; --level)
    {
      pt_idx = PT_IDX(level, cur_virt);

      if (pte[pt_idx].present == 0)
      {
        if (level == 1)
        {
          // printf("setting for lvl %x, index %x\n", level, pt_idx);
          pte[pt_idx].uint = cur_phys;
        }
        else
        {
          // printf("allocating entry for lvl %x, index %x\n", level, pt_idx);
          pte[pt_idx].ptr = aligned_alloc(sizeof(union vm_pte) * 512, 0x1000);
          memset(pte[pt_idx].ptr, 0, sizeof(union vm_pte) * 512);
        }

        SET_FLAG(pte[pt_idx].uint, VM_PAGE_PRESENT | VM_PAGE_RW);
      }

      pte = vm_pte_frame(pte[pt_idx]);
    }

    cur_phys += 0x1000;
    cur_virt += 0x1000;
  }
}

static int set_vesa_mode_cb(int a_mode, vesa_mode_info_t *a_vmi, void *a_ud)
{
  (void)a_ud;
  (void)a_mode;

  if (a_mode < 0x100)
  {
    return 0; /* Ignore non-VBE modes             */
  }

  if (a_vmi->BitsPerPixel != 32)
  {
    return 0;
  }

  if (a_vmi->MemoryModel != memRGB)
  {
    return 0;
  }

  /* 1280 x 1024*/
  // printf("%dx%d - 0x%08x\n", a_vmi->XResolution, a_vmi->YResolution, a_vmi->MemoryModel);

  if ((a_vmi->XResolution > 1920) || (a_vmi->YResolution > 1080))
  {
    return 0;
  }

  if ((g_mbi.common.framebuffer_width >= a_vmi->XResolution) || (g_mbi.common.framebuffer_height >= a_vmi->YResolution))
  {
    return 0;
  }

  g_mbi.common.framebuffer_addr = a_vmi->PhysBasePtr;
  g_mbi.common.framebuffer_pitch = a_vmi->LinBytesPerScanLine;
  g_mbi.common.framebuffer_width = a_vmi->XResolution;
  g_mbi.common.framebuffer_height = a_vmi->YResolution;
  g_mbi.common.framebuffer_bpp = a_vmi->BitsPerPixel;
  g_mbi.common.framebuffer_type = MULTIBOOT_FRAMEBUFFER_TYPE_RGB;

  g_mbi.framebuffer_red_field_position = a_vmi->LinRedFieldPosition;
  g_mbi.framebuffer_red_mask_size = a_vmi->LinRedMaskSize;
  g_mbi.framebuffer_green_field_position = a_vmi->LinGreenFieldPosition;
  g_mbi.framebuffer_green_mask_size = a_vmi->GreenMaskSize;
  g_mbi.framebuffer_blue_field_position = a_vmi->BlueFieldPosition;
  g_mbi.framebuffer_blue_mask_size = a_vmi->BlueMaskSize;

  *((uintptr_t *)a_ud) = a_vmi->PhysBasePtr;

  return (a_mode | (1 << 14) /*Linear buffer*/) & ~(1 << 15) /* Clear display memory */;
}

static void symbol_cb(unsigned long addr, unsigned long size, const char *symnm, unsigned long symnmlen, void *ud)
{
  *((uint32_t *)g_symend) = ((uint32_t)addr);
  g_symend += sizeof(uint32_t);

  *((uint32_t *)g_symend) = ((uint32_t)size);
  g_symend += sizeof(uint32_t);

  memcpy(g_symend, symnm, symnmlen);
  g_symend[symnmlen] = 0;
  g_symend += (symnmlen + 1);

  (void)symnmlen;
  (void)ud;
  (void)addr;
  (void)size;
  (void)symnm;

  // printf("%p %08x %s\n", addr, size, symnm);
}

int main(void)
{
  union vm_pte *pt4;
  u64 *gdt;
  int n_addr_ranges;
  struct bios_address_range_descriptor *addr_ranges;
  cls();

  bios_set_video_mode(0x3);

  if (cpuid_is_present() == false)
  {
    return 1;
  }

  if (cpuid_has_long_mode() == false)
  {
    return 2;
  }

  asm volatile("in al, 0xEE\n");

  asm volatile("in al, 0x92 \n"
               "or al, 2\n"
               "out 0x92, al\n");

  if (cpu_a20_active() == false)
  {
    return 3;
  }

  /* First is always aligned on 1 MB */
  pt4 = malloc(sizeof(union vm_pte) * 512);

  memset(pt4, 0, sizeof(union vm_pte) * 512);

  n_addr_ranges = bios_query_address_map(NULL);

  addr_ranges = malloc((sizeof *addr_ranges) * (size_t)n_addr_ranges);

  assert(bios_query_address_map(addr_ranges) == n_addr_ranges);

  g_sys_memmap.count = (uint32_t)n_addr_ranges;

  g_sys_memmap.descriptor_ptr = (uint32_t)addr_ranges;

  g_symstart = g_symend = elf_loader_loadfile("KERNEL  ELF");

  printf("Loading symbols...\n");

  int nsyms = elf_loader_ldsyms(symbol_cb, NULL);

  if (nsyms)
  {
    printf("%d symbols loaded\n", nsyms);
  }
  else
  {
    printf("No symbols found\n");
  }

  vm_map(pt4, NULL, 0x0000000000000000, page_align(g_symend));

  vm_map(pt4, NULL, 0xffffffff80000000, page_align(g_symend));

  {
    memset(&g_mbi, 0, sizeof(g_mbi)); 

    /* Draw a test pattern */
    uint32_t *framebuffer;

    int modecount = bios_enum_vesa_modes(set_vesa_mode_cb, &framebuffer);

    if (modecount == 0)
    {
      printf("No vesa modes found");
      while (1)
      {
        ;
      }
    }

    {
      uint8_t *row = (uint8_t *)framebuffer;
      for (uint32_t y = 0; y < g_mbi.common.framebuffer_height; y++)
      {
        uint32_t *pixel = (uint32_t *)row;
        for (uint32_t x = 0; x < g_mbi.common.framebuffer_width; x++)
        {
          uint8_t blue = (uint8_t)(x);
          uint8_t green = (uint8_t)(y);
          *pixel++ = (uint32_t)((green << 8) | blue);
        }
        row += g_mbi.common.framebuffer_pitch;
      }
    }
  }

  gdt = aligned_alloc(sizeof(u64) * 3, 0x10);

  cpu_enable_long_mode();

  cpu_enable_pae();

  cpu_set_page_table(pt4);

  cpu_enable_paging();

  if (cpu_long_mode_active() == false)
  {
    return 5;
  }

  init_gdt(gdt);

  cpu_load_gdt(gdt, sizeof(u64) * 3);

  return 0;
}
