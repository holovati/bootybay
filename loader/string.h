#ifndef STRING_H
#define STRING_H
#include <stddef.h>

// #define memset(s, c, n) __builtin_memset(s, c, n);
#define memcpy(s, c, n) __builtin_memcpy(s, c, n);

static inline int strlen(const char *str)
{
  const char *p;

  p = str;

  while (*p++ != 0)
    ;

  return p - str - 1;
}

// #define memcmp __builtin_memcmp

// #define strlen __builtin_strlen

static inline int memcmp(const char *s0, const char *s1, size_t sz)
{
  const char *c1 = s0;
  const char *c2 = s1;

  if (!sz)
    return 0;

  while ((--sz > 0) && (*c1 == *c2))
  {
    c1++;
    c2++;
  }

  return *c1 - *c2;
}

static inline void *memset(void *dest, int src, size_t n)
{
  size_t i;
  for (i = 0; i < n; i++)
    ((unsigned char *)dest)[i] = (unsigned char)src;
  return dest;
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-align"
#pragma GCC diagnostic ignored "-Wcast-qual"
static inline int strncmp(const char *_l, const char *_r, size_t n)
{
  const unsigned char *l = (void *)_l, *r = (void *)_r;
  if (!n--)
    return 0;
  for (; *l && *r && n && *l == *r; l++, r++, n--)
    ;
  return *l - *r;
}
#pragma GCC diagnostic pop

#endif
