#ifndef CPUID_H
#define CPUID_H

#include <stdbool.h>

bool cpuid_is_present(void);

bool cpuid_has_long_mode(void);

#endif
