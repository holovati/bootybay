#ifndef BIOS_H
#define BIOS_H

#include "assert.h"
#include <stdint.h>

#define FLAT_TO_SEGMENT(x) ((((uintptr_t)x) & 0xF0000) >> 4)
#define FLAT_TO_OFFSET(x) (((uintptr_t)x) & 0xFFFF)
#define SEG_TO_FLAT(seg, off) ((((uintptr_t)seg) << 4) & (uint16_t)off))

#ifndef BOOTYCALL
#define BOOTYCALL __attribute__((cdecl)) extern
#endif

struct bios_ioregs {
  union {
    uint32_t eax;
    uint16_t ax;
    struct {
      uint8_t al;
      uint8_t ah;
    };
  };

  union {
    uint32_t ecx;
    uint16_t cx;
    struct {
      uint8_t cl;
      uint8_t ch;
    };
  };

  union {
    uint32_t edx;
    uint16_t dx;
    struct {
      uint8_t dl;
      uint8_t dh;
    };
  };

  union {
    uint32_t ebx;
    uint16_t bx;
    struct {
      uint8_t bl;
      uint8_t bh;
    };
  };

  union {
    uint32_t esi;
    uint16_t si;
  };

  union {
    uint32_t edi;
    uint16_t di;
  };

  uint16_t es;
  uint16_t fs;
  uint16_t gs;
  uint16_t ds;

  uint32_t flags;
};

static_assert(sizeof(struct bios_ioregs) == 36);

struct bios_address_range_descriptor {
  uint64_t base;
  uint64_t size;

  enum {
    address_range_memory = 1,
    address_range_reserved,
    address_range_acpi_reclaimable,
    address_range_acpi_nvs,
    address_range_bad,
  } type;

  uint32_t unused;
};

typedef struct vbe_info {
  char signature[4];     // must be "VESA" to indicate valid VBE support
  uint16_t version;      // VBE version; high byte is major version, low byte is
                         // minor version
  uint32_t oem;          // segment:offset pointer to OEM
  uint32_t capabilities; // bitfield that describes card capabilities
  uint32_t
      video_modes; // segment:offset pointer to list of supported video modes
  uint16_t video_memory; // amount of video memory in 64KB blocks
  uint16_t software_rev; // software revision
  uint32_t vendor;       // segment:offset to card vendor string
  uint32_t product_name; // segment:offset to card model name
  uint32_t product_rev;  // segment:offset pointer to product revision
  char reserved[222];    // reserved for future expansion
  char oem_data[256];    // OEM BIOSes store their strings in this area
} __attribute__((packed)) vbe_info_t;

static_assert(sizeof(struct vbe_info) == 512);

uint32_t bios_call(uint32_t int_n, struct bios_ioregs *regs);

void bios_print_ioregs(struct bios_ioregs *regs);

void bios_set_video_mode(uint8_t mode);

int bios_query_address_map(struct bios_address_range_descriptor *ard);

typedef enum {
  memPL = 3,  /* Planar memory model              */
  memPK = 4,  /* Packed pixel memory model        */
  memRGB = 6, /* Direct color RGB memory model    */
  memYUV = 7, /* Direct color YUV memory model    */
} VesaMemoryModels;

typedef struct vesa_mode_info_block {
  // Mandatory information for all VBE revisions
  uint16_t ModeAttributes;
  uint8_t WinAAttributes;
  uint8_t WinBAttributes;
  uint16_t WinGranularity;
  uint16_t WinSize;
  uint16_t WinASegment;
  uint16_t WinBSegment;
  uint32_t WinFuncPtr;
  uint16_t BytesPerScanLine;
  // Mandatory information for VBE 1.2 and above
  uint16_t XResolution;
  uint16_t YResolution;
  uint8_t XCharSize;
  uint8_t YCharSize;
  uint8_t NumberOfPlanes;
  uint8_t BitsPerPixel;
  uint8_t NumberOfBanks;
  uint8_t MemoryModel;
  uint8_t BankSize;
  uint8_t NumberOfImagePages;
  uint8_t Reserved_page;
  // Direct Color fields (required for direct/6 and YUV/7 memory models)
  uint8_t RedMaskSize;
  uint8_t RedFieldPosition;
  uint8_t GreenMaskSize;
  uint8_t GreenFieldPosition;
  uint8_t BlueMaskSize;
  uint8_t BlueFieldPosition;
  uint8_t RsvdMaskSize;
  uint8_t RsvdFieldPosition;
  uint8_t DirectColorModeInfo;
  // Mandatory information for VBE 2.0 and above
  uint32_t PhysBasePtr;
  uint32_t OffScreenMemOffset;
  uint16_t OffScreenMemSize;
  // Mandatory information for VBE 3.0 and above
  uint16_t LinBytesPerScanLine;
  uint8_t BnkNumberOfPages;
  uint8_t LinNumberOfPages;
  uint8_t LinRedMaskSize;
  uint8_t LinRedFieldPosition;
  uint8_t LinGreenMaskSize;
  uint8_t LinGreenFieldPosition;
  uint8_t LinBlueMaskSize;
  uint8_t LinBlueFieldPosition;
  uint8_t LinRsvdMaskSize;
  uint8_t LinRsvdFieldPosition;
  uint8_t Reserved[194];
} vesa_mode_info_t;

static_assert(sizeof(vesa_mode_info_t) == 256);

typedef int (*vesa_mode_callback_t)(int a_mode, vesa_mode_info_t *a_vmi,
                                    void *a_ud);

int bios_enum_vesa_modes(vesa_mode_callback_t a_cb, void *a_user_data);

#endif /* BIOS_H */
