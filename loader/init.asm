%include "asm.i"
%include "gdt.i"

extern _sbss
extern _ebss

extern start_addr

extern g_kernel_load_addr
extern g_kernel_entry_point
extern g_sys_memmap
extern g_mbi

extern g_symstart
extern g_symend

extern main
extern print_error

SECTION .init

[bits 16]
ENTRY start:
	cli

	sidt [idtr16]

	lgdt [gdtr32]    ; load GDT register with start address of Global Descriptor Table

	mov eax, cr0
	or al, 1       ; set PE (Protection Enable) bit in CR0 (Control Register 0)
	mov cr0, eax

	jmp dword 0x08:prot_mode

align 16

gdt32:
	GDTE 0x00000000, 0x00000, 0b00000000, 0b0000
	GDTE 0x00000000, 0xFFFFF, 0b10011010, 0b1100
	GDTE 0x00000000, 0xFFFFF, 0b10010010, 0b1100

ENTRY gdtr32:
	dw (gdtr32- gdt32) - 1
	dd gdt32

align 8
ENTRY idtr16:
	dw 0
	dd 0

align 16

[bits 32]
prot_mode:
	; setup new segments
	mov ax, 0x10
        mov ds, ax
        mov es, ax
        mov fs, ax
        mov gs, ax
	mov ss, ax

	;setup stack below us
	mov ebx, start_addr
	mov esp, ebx

	; Zero fill bss
	xor eax, eax
	mov edi, _sbss
	mov ecx, _ebss
	sub ecx, edi

	repe stosb
	call main

	test eax, eax

	jnz .error

	jmp dword 0x08:long_mode
.error:
	push eax
	call print_error
.loop:
	cli
	hlt
	jmp .loop

align 16
[bits 64]
long_mode:
	mov ax, 0x10
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	mov ss, ax

	mov rcx, [g_kernel_load_addr]
 	mov rsp, rcx

	xor rcx, rcx
	mov ecx, [g_symend]
	push rcx

	mov ecx, [g_symstart]
	push rcx

	lea rdi, [g_sys_memmap]
    lea rdx, [g_mbi]


	mov rcx, [g_kernel_entry_point]

	jmp rcx
align 16
