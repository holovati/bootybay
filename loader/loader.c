// clang-format off
#include "loader.h"
#include "elf.h"
#include "assert.h"
#include "stdio.h"
#include "string.h"
#include "malloc.h"
#include "cp-demangle.h"
#include "demangle.h"


static FILE *s_kernel_file;
static Elf64_Ehdr s_hdr;
extern uint64_t g_kernel_entry_point;

#define MAKEPTR(p, o) ((void*)(((uintptr_t)(p))+ ((uintptr_t)(o))))
#define LOAD_ADDR(x) (((uintptr_t)x) & 0x00FFFFFF)

#define ALIGN(x,w)      ((((uintptr_t)(x)) + (((uintptr_t)(w)) - 1) ) & ~((((uintptr_t)(w))) -1))

static void elf_phdr_load(Elf64_Phdr *phdr, FILE* fd)
{
  //printf("LOAD_ADDR(phdr->p_paddr) %x\n", LOAD_ADDR(phdr->p_paddr));
  //printf("phdr->p_filesz %x\n", phdr->p_filesz);
  //printf("phdr->p_offset %x\n", phdr->p_offset);

  fseek(fd, (off_t)phdr->p_offset, SEEK_SET);

  assert(fread((void*)LOAD_ADDR(phdr->p_paddr), (size_t)phdr->p_filesz, 1, fd) == 1);

  /* printf("p_type   %x\n", phdr->p_type); */
  /* printf("p_flags  %x\n", phdr->p_flags); */
  // printf("p_offset %x\n", phdr->p_offset);
  /* printf("p_vaddr  %x\n", phdr->p_vaddr); */
  /* printf("p_paddr  %x\n", phdr->p_paddr); */
  /* printf("p_filesz %x\n", phdr->p_filesz); */
  /* printf("p_memsz  %x\n", phdr->p_memsz); */
  /* printf("p_align  %x\n", phdr->p_align); */
}

static uintptr_t elf_do_phdrs(Elf64_Ehdr *hdr, FILE* fd)
{
  Elf64_Half idx;
  Elf64_Phdr *phdr;
  uintptr_t kend = 0;
  phdr = alloca((size_t)(hdr->e_phentsize * hdr->e_phnum));

  fseek(fd, (off_t)hdr->e_phoff, SEEK_SET);

  assert(fread(phdr, hdr->e_phentsize, hdr->e_phnum, fd) == hdr->e_phnum);

  for (idx = 0; idx < hdr->e_phnum; ++idx)
  {
    switch (phdr[idx].p_type) {
      case PT_NULL         : { continue; }
      case PT_LOAD         : { 
        elf_phdr_load(&phdr[idx], fd);
        uintptr_t e = ALIGN(LOAD_ADDR(phdr[idx].p_paddr) + phdr[idx].p_memsz, phdr[idx].p_align); 
        if (kend < e)
        {
          kend = e;
        } 
        continue;
      }
      case PT_DYNAMIC      : assert(0); break;
      case PT_INTERP       : assert(0); break;
      case PT_NOTE         : assert(0); break;
      case PT_SHLIB        : assert(0); break;
      case PT_PHDR         : assert(0); break;
      case PT_LOOS         : assert(0); break;
      case PT_TLS          : continue;
      case PT_SUNW_UNWIND  : assert(0); break;
      case PT_GNU_EH_FRAME : assert(0); break;
      case PT_GNU_STACK    : continue;
      case PT_GNU_RELRO    : assert(0); break;
      case PT_DUMP_DELTA   : assert(0); break;
      case PT_HIOS         : assert(0); break;
      case PT_LOPROC       : assert(0); break;
      case PT_HIPROC       : assert(0); break;
      default              : assert(0); break;
    }
  }

  return kend;
}

static int elf_get_shdr(FILE *f, const char *sname, Elf64_Ehdr *ehdr, Elf64_Shdr *shdr_out)
{
        Elf64_Shdr shstr_hdr;
        char tmpbuf[0x100];

        fseek(f, (off_t)(ehdr->e_shoff + (ehdr->e_shentsize * ehdr->e_shstrndx)), SEEK_SET);

        fread(&shstr_hdr, ehdr->e_shentsize, 1, f);

        for (Elf64_Half i = 0; i < ehdr->e_shnum; i++)
        {
                fseek(f,(off_t)(ehdr->e_shoff + (ehdr->e_shentsize * i)), SEEK_SET);

                fread(shdr_out, ehdr->e_shentsize, 1, f);

                fseek(f,(off_t)(shstr_hdr.sh_offset + shdr_out->sh_name), SEEK_SET);

                fread(tmpbuf, sizeof(tmpbuf), 1, f);

                if (strncmp(sname, tmpbuf, sizeof(tmpbuf)) == 0)
                {
                        return 0;
                }
        }

        return -1;
}

struct dmangle_data
{
        uintptr_t addr;
        size_t size;
        elf_symbol_cb cb;
        void *ud;
};

static void dmangle_cb(const char *str, size_t sz, void *op)
{
  struct dmangle_data *data = (struct dmangle_data *)op;
  data->cb(data->addr, data->size, str, sz, data->ud);
}

void* elf_loader_loadfile(char *fname)
{
  s_kernel_file = fopen(fname, "rb");

  assert(s_kernel_file != NULL);

  fread(&s_hdr, sizeof(s_hdr), 1, s_kernel_file);

  assert(IS_ELF(s_hdr));

  assert(s_hdr.e_ident[EI_VERSION] == EV_CURRENT);

  assert(s_hdr.e_ident[EI_CLASS] == ELFCLASS64);

  assert(s_hdr.e_ident[EI_DATA] == ELFDATA2LSB);

  assert(s_hdr.e_type == ET_EXEC);

  assert(s_hdr.e_machine == EM_X86_64);

  assert(s_hdr.e_phentsize == sizeof(Elf64_Phdr));

  assert(s_hdr.e_shentsize == sizeof(Elf64_Shdr));

  uintptr_t end_addr = elf_do_phdrs(&s_hdr, s_kernel_file);

  g_kernel_entry_point = s_hdr.e_entry;

  return (void*) end_addr;
}

int elf_loader_ldsyms(elf_symbol_cb cb, void *ud)
{
  if (cb == NULL)
  {
    return 0;
  }

  Elf64_Shdr symtab_shdr;
  if (elf_get_shdr(s_kernel_file, ".symtab", &s_hdr, &symtab_shdr) != 0)
  {
    return 0;
  }

  Elf64_Shdr strtab_shdr;
  if (elf_get_shdr(s_kernel_file, ".strtab", &s_hdr, &strtab_shdr) != 0)
  {
    return 0;
  }

  Elf64_Sym *symmem = (Elf64_Sym *)0x00A00000;
  fseek(s_kernel_file, (off_t)(symtab_shdr.sh_offset), SEEK_SET);
  fread(symmem, (size_t)(symtab_shdr.sh_size), 1, s_kernel_file);

  char* strmem = (char*)(0x00A00000 + ((uintptr_t)symtab_shdr.sh_size));
  fseek(s_kernel_file, (off_t)(strtab_shdr.sh_offset), SEEK_SET);
  fread(strmem, (size_t)strtab_shdr.sh_size, 1, s_kernel_file);

  size_t nsyms = (size_t)((int)symtab_shdr.sh_size/(int)symtab_shdr.sh_entsize);

  printf("Fread ok, syms %d\n", nsyms);

  size_t i;
  for (i = 0; i < nsyms; i++)
  {
     if (symmem[i].st_size == 0)
     {
        continue;
     }

    if (strlen(strmem + symmem[i].st_name))
    {
      struct dmangle_data cbd;
      cbd.addr = (uintptr_t)symmem[i].st_value;
      cbd.size = (size_t)symmem[i].st_size;
      cbd.cb = cb;
      cbd.ud = ud;

      int ret = cplus_demangle_v3_callback(strmem + symmem[i].st_name, DMGL_GNU, dmangle_cb, &cbd);

      if (ret == 0)
      {
        char *p = strmem + symmem[i].st_name;
        cb(cbd.addr, cbd.size, p, (size_t)strlen(p), ud);
      }
    }
  }
  return ((int)i);
}
