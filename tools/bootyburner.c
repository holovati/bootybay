#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>

extern int errno;

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

#define MBR_SIGNATURE 0xAA55

#define MBR_PARTITION_TYPE_FAT32_LBA 0x0C

struct mbr_partition
{
  u32 status    : 8;
  u32 chs_start : 24;
  u32 type      : 8;
  u32 chs_end   : 24;
  u32 lba_start;
  u32 n_sectors;
};

struct mbr
{
  u8 code[0x1BE];
  struct mbr_partition partition[4];
  u16 signature;
} __attribute__((packed));

struct fat32_bpb
{
  uint8_t     BS_jmpBoot[3];
  char        BS_OEMName[8];
  uint16_t    BPB_BytsPerSec;
  uint8_t     BPB_SecPerClus;
  uint16_t    BPB_RsvdSecCnt;
  uint8_t     BPB_NumFATs;
  uint16_t    BPB_RootEntCnt;
  uint16_t    BPB_TotSec16;
  uint8_t     BPB_Media;
  uint16_t    BPB_FATSz16;
  uint16_t    BPB_SecPerTrk;
  uint16_t    BPB_NumHeads;
  uint32_t    BPB_HiddSec;
  uint32_t    BPB_TotSec32;
  uint32_t    BPB_FATSz32;
  uint16_t    BPB_ExtFlags;
  uint16_t    BPB_FSVer;
  uint32_t    BPB_RootClus;
  uint16_t    BPB_FSInfo;
  uint16_t    BPB_BkBootSec;
  uint8_t     BPB_Reserved[12];
  uint8_t     BS_DrvNum;
  uint8_t     BS_Reserved1;
  uint8_t     BS_BootSig;
  uint32_t    BS_VolID;
  char        BS_VolLab[11];
  char        BS_FilSysType[8];
  uint8_t     code[420];
  uint16_t    signature;
} __attribute__((packed));

void error_exit()
{
  printf("Error: %s\n", strerror(errno));
  exit(errno);
}

void print_bytes(u8* arr, size_t elems)
{
  size_t idx;
  for(idx = 0; idx < elems; idx++)
  {
    printf("%02X ", arr[idx]);
  }

  printf("\n");
}

int main(int argc, char *argv[])
{
  FILE *disk_image_fd, *mbr_image_fd, *vbr_image_fd;
  struct mbr disk_mbr, booty_mbr;
  struct mbr_partition *boot_part;
  struct fat32_bpb fat32, booty_fat32;

  u32 idx;

  if (argc < 4)
  {
    printf("Missing args: image disk_mbr vbr\n");
    return 1;
  }

  disk_image_fd = fopen(argv[1], "r+b");

  if (disk_image_fd == NULL)
  {
    error_exit();
  }

  mbr_image_fd = fopen(argv[2], "rb");

  if (mbr_image_fd == NULL)
  {
    error_exit();
  }

  vbr_image_fd = fopen(argv[3], "rb");

  if (mbr_image_fd == NULL)
  {
    error_exit();
  }

  fread(&disk_mbr, sizeof(disk_mbr), 1, disk_image_fd);

  fread(&booty_mbr, sizeof(booty_mbr), 1, mbr_image_fd);

  if (disk_mbr.signature != MBR_SIGNATURE)
  {
    printf("Image mbr does not have a valid signature");
    return 2;
  }

  if (booty_mbr.signature != MBR_SIGNATURE)
  {
    printf("Image mbr does not have a valid signature");
    return 2;
  }

  memcpy(&disk_mbr.code[0], &booty_mbr.code[0], sizeof(disk_mbr.code));

  fseek(disk_image_fd, 0, SEEK_SET);

  if (fwrite(&disk_mbr, sizeof(disk_mbr), 1, disk_image_fd) != 1)
  {
    error_exit();
  }

  boot_part = NULL;

  for(idx = 0; idx < 4; ++idx)
  {
    printf("Partition %d\n", idx);
    printf("\tFlags       0x%08X\n", disk_mbr.partition[idx].status);
    printf("\tCHS Start   0x%08X\n", disk_mbr.partition[idx].chs_start);
    printf("\tType        0x%08X\n", disk_mbr.partition[idx].type);
    printf("\tCHS End     0x%08X\n", disk_mbr.partition[idx].chs_end);
    printf("\tLBA Start   0x%08X\n", disk_mbr.partition[idx].lba_start);
    printf("\tNum Sectors 0x%08X\n", disk_mbr.partition[idx].n_sectors);

    if (disk_mbr.partition[idx].type == MBR_PARTITION_TYPE_FAT32_LBA &&
        disk_mbr.partition[idx].status == 0x80 &&
        boot_part == NULL)
    {
      boot_part = &disk_mbr.partition[idx];
      printf("Selected partition %d\n", idx);
    }
  }

  if (boot_part == NULL)
  {
    printf("No bootable fat32 partition found\n");
    return 2;
  }

  fseek(disk_image_fd, boot_part->lba_start * 512, SEEK_SET);

  fread(&fat32, sizeof(fat32), 1, disk_image_fd);

  if (fat32.signature != MBR_SIGNATURE)
  {
    printf("Fat32 vbr wrong signature\n");
    return 3;
  }

  fseek(vbr_image_fd, 0, SEEK_SET);

  fread(&booty_fat32, sizeof(booty_fat32), 1, vbr_image_fd);

  if (booty_fat32.signature != MBR_SIGNATURE)
  {
    printf("Fat32 vbr wrong signature\n");
    return 3;
  }

  printf("\nVolume boot record at offset 0x%08X:\n", boot_part->lba_start * 512);

  printf("\tBS_jmpBoot     ");
  print_bytes(&fat32.BS_jmpBoot[0],sizeof(fat32.BS_jmpBoot));

  printf("\tBS_OEMName     ");
  print_bytes((u8*)&fat32.BS_OEMName[0],sizeof(fat32.BS_OEMName));

  printf("\tBPB_BytsPerSec 0x%08X\n", fat32.BPB_BytsPerSec);
  printf("\tBPB_SecPerClus 0x%08X\n", fat32.BPB_SecPerClus);
  printf("\tBPB_RsvdSecCnt 0x%08X\n", fat32.BPB_RsvdSecCnt);
  printf("\tBPB_NumFATs    0x%08X\n", fat32.BPB_NumFATs);
  printf("\tBPB_RootEntCnt 0x%08X\n", fat32.BPB_RootEntCnt);
  printf("\tBPB_TotSec16   0x%08X\n", fat32.BPB_TotSec16);
  printf("\tBPB_Media      0x%08X\n", fat32.BPB_Media);
  printf("\tBPB_FATSz16    0x%08X\n", fat32.BPB_FATSz16);
  printf("\tBPB_SecPerTrk  0x%08X\n", fat32.BPB_SecPerTrk);
  printf("\tBPB_NumHeads   0x%08X\n", fat32.BPB_NumHeads);
  printf("\tBPB_HiddSec    0x%08X\n", fat32.BPB_HiddSec);
  printf("\tBPB_TotSec32   0x%08X\n", fat32.BPB_TotSec32);
  printf("\tBPB_FATSz32    0x%08X\n", fat32.BPB_FATSz32);
  printf("\tBPB_ExtFlags   0x%08X\n", fat32.BPB_ExtFlags);
  printf("\tBPB_FSVer      0x%08X\n", fat32.BPB_FSVer);
  printf("\tBPB_RootClus   0x%08X\n", fat32.BPB_RootClus);
  printf("\tBPB_FSInfo     0x%08X\n", fat32.BPB_FSInfo);
  printf("\tBPB_BkBootSec  0x%08X\n", fat32.BPB_BkBootSec);

  printf("\tBPB_Reserved   ");
  print_bytes(&fat32.BPB_Reserved[0],sizeof(fat32.BPB_Reserved));

  printf("\tBS_DrvNum      0x%08X\n", fat32.BS_DrvNum);
  printf("\tBS_Reserved1   0x%08X\n", fat32.BS_Reserved1);
  printf("\tBS_BootSig     0x%08X\n", fat32.BS_BootSig);
  printf("\tBS_VolID       0x%08X\n", fat32.BS_VolID);

  printf("\tBS_VolLab      ");
  print_bytes((u8*)&fat32.BS_VolLab[0], sizeof(fat32.BS_VolLab));

  printf("\tBS_FilSysType  ");
  print_bytes((u8*)&fat32.BS_FilSysType[0], sizeof(fat32.BS_FilSysType));

  memcpy(&fat32.code[0], &booty_fat32.code[0], sizeof(fat32.code));

  fseek(disk_image_fd, boot_part->lba_start * 512, SEEK_SET);

  if (fwrite(&fat32, sizeof(fat32), 1, disk_image_fd) != 1)
  {
    error_exit();
  }

  fclose(disk_image_fd);
  fclose(mbr_image_fd);
  fclose(vbr_image_fd);

  return 0;
}
