#!/bin/bash

export PATH="/home/dholovati/root/bin:$PATH"

mkdir -p build

root_dir=$(pwd)

pushd build &>/dev/null

rm * &>/dev/null

dev="/dev/sde"

POSITIONAL=()
while [[ $# -gt 0 ]]
do
    key="$1"
    case $key in
	-m|--mbr)
	    EXTENSION="$2"
	    shift # past argument
	    shift # past value
	    ;;
	-v|--vbr)
	    SEARCHPATH="$2"
	    shift # past argument
	    shift # past value
	    ;;
	-b|--booty)
	    mkdir -p loader

	    pushd loader &>/dev/null

	    rm *.o &>/dev/null

	    pushd $root_dir/loader

	    for file in *.{c,asm}; do

		[ -e "$file" ] || continue

		extension="${file##*.}"

		file=${file%.*} # remove suffix

		if [ "$extension" = "asm" ]; then
		    nasm -f elf32			\
			 -o "$file.$extension.o"	\
			 "$file.asm"

		else
		    $BOOTYCC     -std=gnu11     			\
				 -pedantic				\
				 -Werror				\
				 -Wall					\
				 -Wextra				\
				 -Wimplicit-fallthrough			\
				 -Wmisleading-indentation		\
				 -Wparentheses				\
				 -Wsequence-point			\
				 -Wshift-overflow			\
				 -Wswitch				\
				 -Wswitch-default			\
				 -Wswitch-enum				\
				 -Wswitch-bool				\
				 -Wswitch-unreachable			\
				 -Wunused				\
				 -Wuninitialized			\
				 -Wmaybe-uninitialized			\
				 -Wstrict-aliasing			\
				 -Wstrict-overflow			\
				 -Wbool-compare				\
				 -Wduplicated-branches			\
				 -Wduplicated-cond			\
				 -Wshadow				\
				 -Wtype-limits				\
				 -Wbad-function-cast			\
				 -Wcast-qual				\
				 -Wconversion				\
				 -Wdangling-else			\
				 -Wempty-body				\
				 -Wsign-conversion			\
				 -Wconversion				\
				 -Wsizeof-array-argument		\
				 -Wlogical-op				\
				 -Wmissing-prototypes			\
				 -Wmissing-declarations			\
				 -Wmissing-field-initializers		\
				 -Wpacked				\
				 -Wpadded				\
				 -Wredundant-decls			\
				 -Wvla					\
				 -fsigned-char				\
				 -Wstrict-prototypes			\
				 -mno-sse -mno-mmx -mno-sse2 -mno-3dnow \
				 -fno-asynchronous-unwind-tables        \
				 -fno-pic -fno-pie                      \
				 -masm=intel				\
				 -ffreestanding				\
				 -march=i686				\
				 -O2 -nostdlib 				\
				 --freestanding				\
				 -c "$file.c"				\
				 -o "$file.$extension.o"

		fi

		check=$?

		if [ "$check" -eq 0 ]; then
		    echo "$file Ok"
		else
		    echo "$file Error"
		    exit 1
		fi

	    done

	    popd

	    mv $root_dir/loader/*.o .

	    $BOOTYLD -M -static --strip-all --nmagic -nostdlib -z max-page-size=4096 -z common-page-size=4096 -M -T $root_dir/loader/link.ld *.o

	    check=$?

	    if [ "$check" -eq 0 ]; then
		echo "Linking Ok"
	    else
		echo "Linking Error"
		exit 1
	    fi

	    popd #loader

	    #shift # past argument
	    shift # past value
	    ;;
	-k|--kernel)
	    mkdir -p kernel

	    #cmake $root_dir/kernel -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DCMAKE_TOOLCHAIN_FILE=$root_dir/kernel/tools/cmake/toolchain.cmake

	    pushd kernel &>/dev/null

	    #rm -rf *

	    #cmake $root_dir/kernel -DCMAKE_TOOLCHAIN_FILE=$root_dir/kernel/tools/cmake/toolchain.cmake

	    #make clean
	    #make VERBOSE=1
	    make

	    check=$?

	    if [ "$check" -eq 0 ]; then
		echo "$file Ok"
	    else
		echo "$file Error"
		exit 1
	    fi
	    shift # past value
	    ;;
	-cb|--copybooty)
	    #dev=$(sudo losetup --partscan --show --find $root_dir/fat32.img)
	    mkdir $root_dir/mnt
	    sudo mount "$dev"1 $root_dir/mnt
	    sudo cp $root_dir/build/loader/bootybay.bin $root_dir/mnt/bootybay.bin
	    sudo umount $root_dir/mnt
	    #sudo losetup -d $dev
	    rm -rf $root_dir/mnt
	    echo "Copy booty ok"
	    shift # past value
	    ;;

	-ck|--copykernel)
	    #dev=$(sudo losetup --partscan --show --find $root_dir/fat32.img)
	    mkdir $root_dir/mnt
	    echo 'damir' | sudo -kS mount "$dev"1 $root_dir/mnt
	    echo 'damir' | sudo -kS cp $root_dir/kernel/build/kernel.elf $root_dir/mnt/kernel.elf
		objdump -M intel -d $root_dir/kernel/build/kernel.elf > $root_dir/disasm.txt
	    echo 'damir' | sudo -kS umount $root_dir/mnt
	    #sudo losetup -d $dev
	    rm -rf $root_dir/mnt
	    echo "Copy kernel ok"
	    shift # past value
	    ;;

	-q|--qemu)
	    sync
	    #dev=$(sudo losetup --partscan --show --find $root_dir/fat32.img)
		echo 'damir' | sudo -kS qemu-system-x86_64 -s -S -net nic,macaddr=52:54:00:12:34:56,model=e1000,name=net0 -net bridge,id=bridge0,br=virbr0 -machine q35 -smp sockets=1,cores=2 \
	    -drive id=disk,file="$dev",format=raw,if=none -device ahci,id=ahci -device ide-drive,drive=disk,bus=ahci.0 -m 128 -no-reboot -monitor telnet:127.0.0.1:55555,server,nowait
		#-curses
#-drive format=raw,file="$dev" -monitor stdio -no-reboot -m 128
	    #sudo losetup -d $dev
	    shift # past value
	    ;;

	--default)
	    DEFAULT=YES
	    shift # past argument
	    ;;
	*)    # unknown option
	    POSITIONAL+=("$1") # save it in an array for later
	    shift # past argument
	    ;;
    esac
done

popd # build

# rm -rf build

# gdb -ex 'set disassembly-flavor intel' -ex 'target remote localhost:1234' -ex 'continue' symbol-file ~/bootybay/build/kernel/kernel.elf
