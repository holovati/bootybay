#!/bin/bash

gcc tools/bootyburner.c -o bootyburner
nasm loader/mbr/mbr.asm -f bin -o mbr
nasm loader/mbr/vbr.asm -f bin -o vbr

sudo ./bootyburner $1 mbr vbr

rm bootyburner
rm mbr
rm vbr

sync

echo "$1"1
