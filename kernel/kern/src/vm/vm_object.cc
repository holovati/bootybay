#include <aux/init.h>

#include <aux/defer.hh>

#include <kernel/vm/vm_object.h>
#include <kernel/vm/vm_page.h>
#include <kernel/vm/vm_page_map.h>
#include <kernel/vm/vm_prot.h>
#include <kernel/vm/vm_param.h>
#include <kernel/vm/pmap.h>

#include <kernel/zalloc.h>

// ********************************************************************************************************************************
void vm_object_init(vm_object_t a_vm_obj)
// ********************************************************************************************************************************
{
        a_vm_obj->m_ops      = nullptr;
        a_vm_obj->m_page_map = nullptr;
        a_vm_obj->m_usecnt   = 1;
        a_vm_obj->m_size     = 0;
        lock_init(&a_vm_obj->m_lock, true);
}

// ********************************************************************************************************************************
kern_return_t vm_object_get_page(vm_object_t a_vm_obj, vm_offset_t a_vmo, vm_prot_t a_vm_prot, vm_page_t *a_vmp_out)
// ********************************************************************************************************************************
{
        kern_return_t retval = a_vm_obj->m_ops->get_page(a_vm_obj, a_vmo, a_vm_prot, a_vmp_out);
        return retval;
}

// ********************************************************************************************************************************
boolean_t vm_object_collapse(vm_object_t a_vm_obj,
                             vm_object_t a_destination,
                             vm_object_t *a_backing_object,
                             vm_offset_t *a_backing_object_offset)
// ********************************************************************************************************************************
{
        boolean_t has_collapsed = false;

        if (a_vm_obj->m_ops->collapse != nullptr)
        {
                has_collapsed = a_vm_obj->m_ops->collapse(a_vm_obj, a_destination, a_backing_object, a_backing_object_offset);
        }

        return has_collapsed;
}

// ********************************************************************************************************************************
kern_return_t vm_object_set_size(vm_object_t a_vm_obj, natural_t a_new_size)
// ********************************************************************************************************************************
{
        natural_t osize  = a_vm_obj->m_size;
        a_vm_obj->m_size = a_new_size; // Not a multiple of PAGE_SIZE, must be rounded up if needed.
        if (a_vm_obj->m_ops != nullptr && a_vm_obj->m_ops->set_size != nullptr && a_new_size != osize)
        {
                return a_vm_obj->m_ops->set_size(a_vm_obj, a_new_size);
        }
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
void vm_object_prot_change(vm_object_t a_vm_obj, vm_prot_t a_prot)
// ********************************************************************************************************************************
{
        if (a_vm_obj->m_page_map != nullptr)
        {
                for (vm_page_t vmp = vm_page_map_first(a_vm_obj->m_page_map); vmp != nullptr; vmp = vm_page_map_next(vmp))
                {
                        pmap_page_protect(vmp->phys_addr, a_prot);
                }
        }
}

// ********************************************************************************************************************************
#if defined(VMO_LOCK_DEBUG)
void __vm_object_lock(vm_object_t a_vm_obj, char const *a_file, integer_t a_line)
#else
void vm_object_lock(vm_object_t a_vm_obj)
#endif
// ********************************************************************************************************************************
{
#if defined(VMO_LOCK_DEBUG)
        if (a_vm_obj->m_locked_in_file == nullptr)
        {
                a_vm_obj->m_locked_in_file = a_file;
                a_vm_obj->m_locked_on_line = a_line;
        }
#endif
        lock_write(&a_vm_obj->m_lock);
}

// *******************************************************************************************************************************
void vm_object_unlock(vm_object_t a_vm_obj)
// ********************************************************************************************************************************
{
#if defined(VMO_LOCK_DEBUG)
        if (vm_object_is_locked(a_vm_obj) != true)
        {
                panic("Object not locked, but trying to unlock");
        }
        a_vm_obj->m_locked_in_file = nullptr;
        a_vm_obj->m_locked_on_line = 0;
#endif

        lock_done(&a_vm_obj->m_lock);
}

// *******************************************************************************************************************************
boolean_t vm_object_is_locked(vm_object_t a_vm_obj)
// *******************************************************************************************************************************
{
        if (lock_try_write(&a_vm_obj->m_lock) == true)
        {
                lock_done(&a_vm_obj->m_lock);
                return false;
        }
        else
        {
                return true;
        }
}

// ********************************************************************************************************************************
void vm_object_ref(vm_object_t a_vm_obj)
// ********************************************************************************************************************************
{
        a_vm_obj->m_usecnt++;
}

// ********************************************************************************************************************************
void vm_object_rel(vm_object_t a_vm_obj)
// ********************************************************************************************************************************
{
        KASSERT((a_vm_obj->m_usecnt) > 0);

        if ((--(a_vm_obj->m_usecnt)) > 0)
        {
                return;
        }

        a_vm_obj->m_ops->inactive(a_vm_obj);
}

// ********************************************************************************************************************************
void vm_object_put(vm_object_t a_vm_obj)
// ********************************************************************************************************************************
{
        lock_done(&a_vm_obj->m_lock);
        vm_object_rel(a_vm_obj);
}

// ZERO Object implementation
extern vm_page_t vm_page_zero; // Allocated and initialized during pmap initialization.

static struct vm_object_data s_zero_vm_obj;

// ********************************************************************************************************************************
static kern_return_t zero_vm_obj_get_page(vm_object_t a_vm_obj, vm_offset_t a_vmo, vm_prot_t a_vm_prot, vm_page_t *a_vmp_out)
// ********************************************************************************************************************************
{
        *a_vmp_out = vm_page_zero;

        KASSERT((*a_vmp_out)->flags & PG_COPYONWRITE);

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t zero_vm_obj_inactive(vm_object_t a_vm_obj)
// ********************************************************************************************************************************
{
        panic("Zero vm_object lost all refrences.");
        return KERN_FAILURE;
}

static struct vm_object_operations_data zero_vm_obj_ops = {

        .get_page = zero_vm_obj_get_page,
        .inactive = zero_vm_obj_inactive
};

// ********************************************************************************************************************************
kern_return_t vm_object_create_unamed(natural_t a_len, vm_object_t *a_vm_obj_out)
// ********************************************************************************************************************************
{
        kern_return_t retval = vm_object_create_shadow(&s_zero_vm_obj, 0, a_len, a_vm_obj_out);

        return retval;
}

// SHADOW Object implementation
typedef struct shadow_vm_object_data
{
        struct vm_object_data m_vm_obj;
        struct vm_page_map_data m_vmp_map;
        vm_object_t m_shadow;
        vm_offset_t m_shadow_offset;
} *shadow_vm_object_t;

ZONE_DEFINE(shadow_vm_object_data, s_vm_object_shadow_zone, 0x1000, 0x80, ZONE_FIXED, 32);
static int nshadow_objs;

// ********************************************************************************************************************************
static kern_return_t shadow_vm_obj_get_page(vm_object_t a_vm_obj, vm_offset_t a_offset, vm_prot_t a_prot, vm_page_t *a_vmp_out)
// ********************************************************************************************************************************
{
        shadow_vm_object_t shdw_vm_obj = (shadow_vm_object_t)(a_vm_obj);

        // Check if we have the page at the given offset
        *a_vmp_out = vm_page_map_lookup(&shdw_vm_obj->m_vmp_map, a_offset);

        // If we do, return it.
        if (*a_vmp_out)
        {
                // We don't set COW on our own pages
                (*a_vmp_out)->flags &= ~PG_COPYONWRITE;
                return KERN_SUCCESS;
        }

        // We don't have the requested page, so must have a object to forward the request to.
        KASSERT(shdw_vm_obj->m_shadow != nullptr);

        vm_object_lock(shdw_vm_obj->m_shadow);
        defer(vm_object_unlock(shdw_vm_obj->m_shadow));

        // We don't, request it from the object we are shadowing
        kern_return_t result
                = vm_object_get_page(shdw_vm_obj->m_shadow, shdw_vm_obj->m_shadow_offset + a_offset, a_prot, a_vmp_out);

        // If the shadowed object does'nt have the page, bail!
        if (result != KERN_SUCCESS)
        {
                return result;
        }

        if ((a_prot & VM_PROT_WRITE) == VM_PROT_NONE)
        {
                // Since the page we are returning does not belong to us, ensure COW is set
                (*a_vmp_out)->flags |= PG_COPYONWRITE;
                return KERN_SUCCESS;
        }

        vm_page_t shadow_page = *a_vmp_out;

        // Don't try to copy fictitious pages.
        if (shadow_page->flags & PG_FICTITIOUS)
        {
                (*a_vmp_out)->flags |= PG_COPYONWRITE;
                return KERN_SUCCESS;
        }

        *a_vmp_out = vm_page_alloc(&shdw_vm_obj->m_vmp_map, a_offset);

        KASSERT(*a_vmp_out != shadow_page);

        (*a_vmp_out)->flags &= ~(PG_BUSY | PG_FAKE);

        vm_page_copy(*a_vmp_out, shadow_page, 0, PAGE_SIZE);

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t shadow_vm_obj_inactive(vm_object_t a_vm_obj)
// ********************************************************************************************************************************
{
        shadow_vm_object_t shdw_vm_obj = (shadow_vm_object_t)(a_vm_obj);

        // KASSERT(islocked());

        // Are we shadowing anyone, if so drop the refrence
        if (shdw_vm_obj->m_shadow)
        {
                vm_object_rel(shdw_vm_obj->m_shadow);
        }

        vm_page_map_clear(&shdw_vm_obj->m_vmp_map);

        s_vm_object_shadow_zone.free_delete(shdw_vm_obj);

        nshadow_objs--;

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static boolean_t shadow_vm_obj_collapse(vm_object_t a_vm_obj,
                                        vm_object_t a_destination,
                                        vm_object_t *a_backing_object,
                                        vm_offset_t *a_backing_object_offset)
// ********************************************************************************************************************************
{
        shadow_vm_object_t shdw_vm_obj = (shadow_vm_object_t)(a_vm_obj);

        // First lets check if the our shadow can be collapsed into us
        if (shdw_vm_obj->m_shadow != nullptr)
        {
                vm_object_lock(shdw_vm_obj->m_shadow);
                vm_object_t new_shadow = nullptr;

                boolean_t collapsed = vm_object_collapse(shdw_vm_obj->m_shadow,
                                                         &shdw_vm_obj->m_vm_obj,
                                                         &new_shadow,
                                                         &shdw_vm_obj->m_shadow_offset);
                vm_object_unlock(shdw_vm_obj->m_shadow);

                if (collapsed)
                {
                        // We now have all the pages we need from the object we were shadowing, loose it.
                        vm_object_rel(shdw_vm_obj->m_shadow);
                        shdw_vm_obj->m_shadow = new_shadow;
                }
        }

        // Did we get a destination object object for our pages? And can we be collapsed?
        bool can_collapse = a_destination != nullptr && a_destination->m_page_map != nullptr && shdw_vm_obj->m_vm_obj.m_usecnt == 1
                         && a_destination->m_size == shdw_vm_obj->m_vm_obj.m_size;

        if (can_collapse)
        {
                // The pages we do not transfer to the destination object will be freed when er use_count drops to zero.
                // The caller has the responsibilty to call unreference on us.
                vm_page_t vmp_next = nullptr;
                for (vm_page_t vmp = vm_page_map_first(&shdw_vm_obj->m_vmp_map); vmp != nullptr; vmp = vmp_next)
                {
                        vmp_next = vm_page_map_next(vmp);
                        vm_page_rename(&shdw_vm_obj->m_vmp_map, vmp, a_destination->m_page_map, vmp->offset);
                }

                if (a_backing_object)
                {
                        *a_backing_object          = shdw_vm_obj->m_shadow;
                        shdw_vm_obj->m_shadow      = nullptr;
                        (*a_backing_object_offset) = shdw_vm_obj->m_shadow_offset;
                }
        }

        return can_collapse;
}

static struct vm_object_operations_data shadow_vm_obj_ops = {
        .get_page = shadow_vm_obj_get_page,
        .inactive = shadow_vm_obj_inactive,
        .collapse = shadow_vm_obj_collapse,
};

// ********************************************************************************************************************************
kern_return_t vm_object_create_shadow(vm_object_t a_vm_obj_src,
                                      vm_offset_t a_src_offset,
                                      natural_t a_len,
                                      vm_object_t *a_vm_obj_shdw_out)
// ********************************************************************************************************************************
{
        shadow_vm_object_t shwd_obj = s_vm_object_shadow_zone.alloc();

        nshadow_objs++;

        vm_object_init(&shwd_obj->m_vm_obj);

        shwd_obj->m_vm_obj.m_ops = &shadow_vm_obj_ops;
        vm_page_map_init((shwd_obj->m_vm_obj.m_page_map = &shwd_obj->m_vmp_map));
        shwd_obj->m_vm_obj.m_size = a_len;

        vm_object_ref((shwd_obj->m_shadow = a_vm_obj_src));
        shwd_obj->m_shadow_offset = a_src_offset;

        *a_vm_obj_shdw_out = &shwd_obj->m_vm_obj;

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t vm_object_op_get_page_fail(vm_object_t a_vm_obj, vm_offset_t a_vmo, vm_prot_t a_prot, vm_page_t *a_vmp_out)
// ********************************************************************************************************************************
{
        return KERN_FAILURE;
}

// ********************************************************************************************************************************
boolean_t vm_object_op_collapse_fail(vm_object_t a_vm_obj,
                                     vm_object_t a_destination,
                                     vm_object_t *a_backing_object,
                                     vm_offset_t *a_backing_object_offset)
// ********************************************************************************************************************************
{
        return KERN_FAILURE;
}

// ********************************************************************************************************************************
kern_return_t vm_object_op_set_size_noop(vm_object_t a_vm_obj, natural_t a_new_size)
// ********************************************************************************************************************************
{
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t vm_object_early_init()
// ********************************************************************************************************************************
{
        vm_object_init(&s_zero_vm_obj);
        s_zero_vm_obj.m_ops  = &zero_vm_obj_ops;
        s_zero_vm_obj.m_size = UINTPTR_MAX;

        nshadow_objs = 0;

        return KERN_SUCCESS;
}

early_initcall(vm_object_early_init);
