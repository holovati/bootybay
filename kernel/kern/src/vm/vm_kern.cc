/*
 * Mach Operating System
 * Copyright (c) 1991,1990,1989,1988,1987 Carnegie Mellon University.
 * Copyright (c) 1993,1994 The University of Utah and
 * the Computer Systems Laboratory (CSL).
 * All rights reserved.
 *
 * Permission to use, copy, modify and distribute this software and its
 * documentation is hereby granted, provided that both the copyright
 * notice and this permission notice appear in all copies of the
 * software, derivative works or modified versions, and any portions
 * thereof, and that both notices appear in supporting documentation.
 *
 * CARNEGIE MELLON, THE UNIVERSITY OF UTAH AND CSL ALLOW FREE USE OF
 * THIS SOFTWARE IN ITS "AS IS" CONDITION, AND DISCLAIM ANY LIABILITY
 * OF ANY KIND FOR ANY DAMAGES WHATSOEVER RESULTING FROM THE USE OF
 * THIS SOFTWARE.
 *
 * Carnegie Mellon requests users of this software to return to
 *
 *  Software Distribution Coordinator  or  Software.Distribution@CS.CMU.EDU
 *  School of Computer Science
 *  Carnegie Mellon University
 *  Pittsburgh PA 15213-3890
 *
 * any improvements or extensions that they make and grant Carnegie Mellon
 * the rights to redistribute these changes.
 */
/*
 *	File:	vm/vm_kern.c
 *	Author:	Avadis Tevanian, Jr., Michael Wayne Young
 *	Date:	1985
 *
 *	Kernel memory management.
 */
#include <kernel/lock.h>
#include <kernel/vm/vm_fault.h>
#include <kernel/vm/vm_param.h>
#include <kernel/vm/vm_object.h>
#include <kernel/thread_data.h>
#include <mach/machine/vm_param.h>

#include "vm_pageout.h"
#include <kernel/vm/vm_kern.h>
#include <kernel/vm/vm_map.h>
#include <kernel/vm/vm_page.h>
#include <string.h>

/*
 *	Variables exported by this module.
 */

vm_map_t kernel_map;
vm_map_t kernel_pageable_map;

static vm_object_t s_kernel_object;

/*
 *	Allocate new wired pages in an object.
 *	The object is assumed to be mapped into the kernel map or
 *	a submap.
 */
static void kmem_alloc_pages(vm_object_t object, vm_offset_t offset, vm_offset_t start, vm_offset_t end, vm_prot_t protection);

/*
 *	Remap wired pages in an object into a new region.
 *	The object is assumed to be mapped into the kernel map or
 *	a submap.
 */
static void kmem_remap_pages(vm_object_t object, vm_offset_t offset, vm_offset_t start, vm_offset_t end, vm_prot_t protection);

/*
 *	kmem_alloc:
 *
 *	Allocate wired-down memory in the kernel's address map
 *	or a submap.  The memory is not zero-filled.
 */
kern_return_t kmem_alloc(vm_map_t map, vm_offset_t *addrp, vm_size_t size)
{
        /*
         * Allocate a new object.  We must do this before locking
         * the map, lest we risk deadlock with the default pager:
         * device_read_alloc uses kmem_alloc,
         * which tries to allocate an object,
         * which uses kmem_alloc_wired to get memory,
         * which blocks for pages.
         * then the default pager needs to read a block
         * to process a memory_object_data_write,
         * and device_read_alloc calls kmem_alloc
         * and deadlocks on the map lock.
         */
        vm_map_lock(map);

        vm_offset_t addr     = 0;
        vm_map_entry_t entry = nullptr;
        kern_return_t kr     = vm_map_find_entry(map, &addr, size, (vm_offset_t)0, NULL, &entry);

        if (kr != KERN_SUCCESS)
        {
                vm_map_unlock(map);
                panic("old shit below");
                // vm_object_deallocate(object);
                return kr;
        }
        size          = round_page(size);
        kr            = vm_object_create_unamed(size, &entry->object.vm_object);
        entry->offset = 0;

        /*
         *	Since we have not given out this address yet,
         *	it is safe to unlock the map.
         */
        vm_map_unlock(map);

        /*
         *	Allocate wired-down memory in the kernel_object,
         *	for this entry, and enter it in the kernel pmap.
         */
        kmem_alloc_pages(entry->object.vm_object, 0, addr, addr + size, VM_PROT_DEFAULT);

        /*
         *	Return the memory, not zeroed.
         */
        *addrp = addr;
        return KERN_SUCCESS;
}

/*
 *	kmem_realloc:
 *
 *	Reallocate wired-down memory in the kernel's address map
 *	or a submap.  Newly allocated pages are not zeroed.
 *	This can only be used on regions allocated with kmem_alloc.
 *
 *	If successful, the pages in the old region are mapped twice.
 *	The old region is unchanged.  Use kmem_free to get rid of it.
 */
kern_return_t kmem_realloc(vm_map_t map, vm_offset_t oldaddr, vm_size_t oldsize, vm_offset_t *newaddrp, vm_size_t newsize)
{
        vm_offset_t oldmin, oldmax;
        vm_offset_t newaddr;
        vm_map_entry_t oldentry, newentry;
        kern_return_t kr;

        oldmin  = trunc_page(oldaddr);
        oldmax  = round_page(oldaddr + oldsize);
        oldsize = oldmax - oldmin;
        newsize = round_page(newsize);

        /*
         *	Find space for the new region.
         */

        vm_map_lock(map);
        kr = vm_map_find_entry(map, &newaddr, newsize, (vm_offset_t)0, NULL, &newentry);
        if (kr != KERN_SUCCESS)
        {
                vm_map_unlock(map);
                return kr;
        }

        /*
         *	Find the VM object backing the old region.
         */

        if (!vm_map_lookup_entry(map, oldmin, &oldentry))
        {
                panic("kmem_realloc");
        }

        vm_object_t object = oldentry->object.vm_object;

        /*
         *	Increase the size of the object and
         *	fill in the new region.
         */

        vm_object_ref(object);
        vm_object_lock(object);

        if (object->m_size != oldsize)
        {
                panic("kmem_realloc");
        }

        object->m_size = newsize;
        vm_object_unlock(object);

        newentry->object.vm_object = object;
        newentry->offset           = 0;

        /*
         *	Since we have not given out this address yet,
         *	it is safe to unlock the map.  We are trusting
         *	that nobody will play with either region.
         */

        vm_map_unlock(map);

        /*
         *	Remap the pages in the old region and
         *	allocate more pages for the new region.
         */

        kmem_remap_pages(object, 0, newaddr, newaddr + oldsize, VM_PROT_DEFAULT);
        kmem_alloc_pages(object, oldsize, newaddr + oldsize, newaddr + newsize, VM_PROT_DEFAULT);

        *newaddrp = newaddr;
        return KERN_SUCCESS;
}

/*
 *	kmem_alloc_wired:
 *
 *	Allocate wired-down memory in the kernel's address map
 *	or a submap.  The memory is not zero-filled.
 *
 *	The memory is allocated in the kernel_object.
 *	It may not be copied with vm_map_copy, and
 *	it may not be reallocated with kmem_realloc.
 */

kern_return_t kmem_alloc_wired(vm_map_t map, vm_offset_t *addrp, vm_size_t size)
{
        /*
         *	Use the kernel object for wired-down kernel pages.
         *	Assume that no region of the kernel object is
         *	referenced more than once.  We want vm_map_find_entry
         *	to extend an existing entry if possible.
         */

        size = round_page(size);

        vm_map_lock(map);

        vm_object_t vmo_kern = s_kernel_object;

        vm_map_entry_t entry = nullptr;
        vm_offset_t addr     = 0;
        kern_return_t kr     = vm_map_find_entry(map, &addr, size, (vm_offset_t)0, vmo_kern, &entry);

        if (kr != KERN_SUCCESS)
        {
                vm_map_unlock(map);
                return kr;
        }

        /*
         *	Since we didn't know where the new region would
         *	start, we couldn't supply the correct offset into
         *	the kernel object.  We only initialize the entry
         *	if we aren't extending an existing entry.
         */

        extern vm_offset_t kernel_virtual_start;
        vm_offset_t offset = addr - kernel_virtual_start;

        if (entry->object.vm_object == nullptr)
        {

                vm_object_ref(vmo_kern);
                entry->object.vm_object = vmo_kern;
                entry->offset           = offset;
        }

        /*
         *	Since we have not given out this address yet,
         *	it is safe to unlock the map.
         */
        vm_map_unlock(map);

        /*
         *	Allocate wired-down memory in the kernel_object,
         *	for this entry, and enter it in the kernel pmap.
         */
        kmem_alloc_pages(vmo_kern, offset, addr, addr + size, VM_PROT_DEFAULT);

        /*
         *	Return the memory, not zeroed.
         */
        *addrp = addr;
        return KERN_SUCCESS;
}

/*
 *	kmem_alloc_aligned:
 *
 *	Like kmem_alloc_wired, except that the memory is aligned.
 *	The size should be a power-of-2.
 */

kern_return_t kmem_alloc_aligned(vm_map_t map, vm_offset_t *addrp, vm_size_t size)
{
        vm_map_entry_t entry;
        vm_offset_t offset;
        vm_offset_t addr;
        kern_return_t kr;

        if ((size & (size - 1)) != 0)
        {
                panic("kmem_alloc_aligned");
        }

        /*
         *	Use the kernel object for wired-down kernel pages.
         *	Assume that no region of the kernel object is
         *	referenced more than once.  We want vm_map_find_entry
         *	to extend an existing entry if possible.
         */

        size = round_page(size);

        vm_map_lock(map);

        vm_object_t vmo_kern = s_kernel_object;

        kr = vm_map_find_entry(map, &addr, size, size + PAGE_SIZE - 1, vmo_kern, &entry);
        if (kr != KERN_SUCCESS)
        {
                vm_map_unlock(map);
                return kr;
        }

        /*
         *	Since we didn't know where the new region would
         *	start, we couldn't supply the correct offset into
         *	the kernel object.  We only initialize the entry
         *	if we aren't extending an existing entry.
         */

        offset = addr - VM_MIN_KERNEL_ADDRESS;

        if (entry->object.vm_object == nullptr)
        {
                vm_object_ref(vmo_kern);

                entry->object.vm_object = vmo_kern;
                entry->offset           = offset;
        }

        /*
         *	Since we have not given out this address yet,
         *	it is safe to unlock the map.
         */
        vm_map_unlock(map);

        /*
         *	Allocate wired-down memory in the kernel_object,
         *	for this entry, and enter it in the kernel pmap.
         */
        kmem_alloc_pages(vmo_kern, offset, addr, addr + size, VM_PROT_DEFAULT);

#ifdef ENABLE_MEM_POISON
        memset((void *)addr, 0xEC, size);
#endif
        /*
         *	Return the memory, not zeroed.
         */
        *addrp = addr;
        return KERN_SUCCESS;
}

/*
 *	kmem_alloc_pageable:
 *
 *	Allocate pageable memory in the kernel's address map.
 */

kern_return_t kmem_alloc_pageable(vm_map_t map, vm_offset_t *addrp, vm_size_t size)
{
        vm_offset_t addr;
        kern_return_t kr;

        if (map != kernel_map)
        {
                panic("kmem_alloc_pageable: not called with kernel_map");
        }

        size = round_page(size);

        addr = map->min_offset();
        kr   = vm_map_find(map, NULL, (vm_offset_t)0, &addr, size, TRUE, nullptr);

        if (kr != KERN_SUCCESS)
        {
                return kr;
        }

        *addrp = addr;
        return KERN_SUCCESS;
}

/*
 *	kmem_free:
 *
 *	Release a region of kernel virtual memory allocated
 *	with kmem_alloc, kmem_alloc_wired, or kmem_alloc_pageable,
 *	and return the physical pages associated with that region.
 */

void kmem_free(vm_map_t map, vm_offset_t addr, vm_size_t size)
{
        kern_return_t kr = vm_map_remove(map, trunc_page(addr), round_page(addr + size));
        if (kr != KERN_SUCCESS)
        {
                panic("kmem_free");
        }
}

/*
 *	Allocate new wired pages in an object.
 *	The object is assumed to be mapped into the kernel map or
 *	a submap.
 */
void kmem_alloc_pages(vm_object_t object, vm_offset_t offset, vm_offset_t start, vm_offset_t end, vm_prot_t protection)
{
        /*
         *	Mark the pmap region as not pageable.
         */
        pmap_pageable(kernel_pmap, start, end, FALSE);

        while (start < end)
        {
                vm_object_lock(object);

                /*
                 *	Allocate a page
                 */
                vm_page_t mem = nullptr; // vm_page_alloc(&object->m_page_map, offset);

                vm_object_get_page(object, offset, protection | VM_PROT_WRITE, &mem);

#if 0
                while (mem == nullptr)
                {
                        object->unlock();
                        pmap_wait_free_page();
                        object->lock();
                }
#endif
                /*
                 *	Wire it down
                 */
                mem->wire();

                vm_object_unlock(object);

                /*
                 *	Enter it in the kernel pmap
                 */

                pmap_enter(kernel_pmap, start, mem, protection, TRUE);

#if 0
                object->lock();
                mem->wakeup_done();
                object->unlock();
#endif
                start += PAGE_SIZE;
                offset += PAGE_SIZE;
        }
}

/*
 *	Remap wired pages in an object into a new region.
 *	The object is assumed to be mapped into the kernel map or
 *	a submap.
 */
void kmem_remap_pages(vm_object_t object, vm_offset_t offset, vm_offset_t start, vm_offset_t end, vm_prot_t protection)
{
        /*
         *	Mark the pmap region as not pageable.
         */
        pmap_pageable(kernel_pmap, start, end, FALSE);

        while (start < end)
        {
                vm_object_lock(object);

                /*
                 *	Find a page
                 */
                // vm_page_t mem = vm_page_map_lookup(&object->m_page_map, offset);

                vm_page_t mem = nullptr;
                vm_object_get_page(object, offset, protection | VM_PROT_WRITE, &mem);

                if (mem == nullptr)
                {
                        panic("kmem_remap_pages");
                }

                /*
                 *	Wire it down (again)
                 */
                mem->wire();
                vm_object_unlock(object);

                /*
                 *	Enter it in the kernel pmap.  The page isn't busy,
                 *	but this shouldn't be a problem because it is wired.
                 */
                pmap_enter(kernel_pmap, start, mem, protection, TRUE);

                start += PAGE_SIZE;
                offset += PAGE_SIZE;
        }
}

boolean_t kmem_is_kernel_object(vm_object_t a_obj)
{
        return a_obj == s_kernel_object;
}

/*
 *	kmem_suballoc:
 *
 *	Allocates a map to manage a subrange
 *	of the kernel virtual address space.
 *
 *	Arguments are as follows:
 *
 *	parent		Map to take range from
 *	size		Size of range to find
 *	min, max	Returned endpoints of map
 *	pageable	Can the region be paged
 */

vm_map_t kmem_suballoc(vm_map_t parent, vm_offset_t *min, vm_offset_t *max, vm_size_t size, boolean_t pageable)
{
        size = round_page(size);

        *min = parent->min_offset();

        kern_return_t ret = vm_map_find(parent, NULL, (vm_offset_t)0, min, size, TRUE, nullptr);

        if (ret != KERN_SUCCESS)
        {
                panic("kmem_suballoc: bad status return of %d.", ret);
        }

        *max = *min + size;

        pmap_reference(vm_map_pmap(parent));

        vm_map_t result = vm_map_create(vm_map_pmap(parent), *min, *max, pageable);

        if (result == NULL)
        {
                panic("kmem_suballoc: cannot create submap");
        }

        if ((ret = vm_map_submap(parent, *min, *max, result)) != KERN_SUCCESS)
        {
                panic("kmem_suballoc: unable to change range to submap");
        }

        return result;

#if 0
  vm_map_t map;
  vm_offset_t addr;
  kern_return_t kr;

  size = round_page(size);

  /*
   *	Need reference on submap object because it is internal
   *	to the vm_system.  vm_object_enter will never be called
   *	on it (usual source of reference for vm_map_enter).
   */
  vm_object_reference(vm_submap_object);

  addr = (vm_offset_t)vm_map_min(parent);
  kr   = vm_map_enter(parent,
                    &addr,
                    size,
                    (vm_offset_t)0,
                    TRUE,
                    vm_submap_object,
                    (vm_offset_t)0,
                    FALSE,
                    VM_PROT_DEFAULT,
                    VM_PROT_ALL,
                    VM_INHERIT_DEFAULT);
  if (kr != KERN_SUCCESS)
    panic("kmem_suballoc");

  pmap_reference(vm_map_pmap(parent));
  map = vm_map_create(vm_map_pmap(parent), addr, addr + size, pageable);
  if (map == VM_MAP_NULL)
    panic("kmem_suballoc");

  kr = vm_map_submap(parent, addr, addr + size, map);
  if (kr != KERN_SUCCESS)
    panic("kmem_suballoc");

  *min = addr;
  *max = addr + size;
  return map;
#endif
}

/*
 *	kmem_init:
 *
 *	Initialize the kernel's virtual memory map, taking
 *	into account all memory allocated up to this time.
 */
void kmem_init(vm_offset_t start, vm_offset_t end)
{
        extern vm_offset_t kernel_virtual_start;
        extern vm_offset_t kernel_virtual_end;

        natural_t kernel_obj_size = kernel_virtual_end - kernel_virtual_start;

        vm_object_create_unamed(kernel_obj_size, &s_kernel_object);

        kernel_map = vm_map_create(pmap_kernel(), kernel_virtual_start, kernel_virtual_end, FALSE);
#if 0
  /*
   *	Reserve virtual memory allocated up to this time.
   */

  if (start != kernel_virtual_start) {
    kern_return_t rc;
    vm_offset_t addr = kernel_virtual_start;

    rc = vm_map_find(
        kernel_map, NULL, 0, &addr, start - kernel_virtual_start, FALSE);
    /*
    rc = vm_map_enter(kernel_map,
                      &addr,
                      start - kernel_virtual_start,
                      (vm_offset_t)0,
                      TRUE,
                      NULL,
                      (vm_offset_t)0,
                      FALSE,
                      VM_PROT_DEFAULT,
                      VM_PROT_ALL,
                      VM_INHERIT_DEFAULT);
    */
    if (rc)
      panic("%s:%d: vm_map_enter failed (%d)\n", rc);
  }
#endif
}
