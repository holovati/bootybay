/*
 * Mach Operating System
 * Copyright (c) 1991,1990,1989,1988,1987 Carnegie Mellon University.
 * Copyright (c) 1993,1994 The University of Utah and
 * the Computer Systems Laboratory (CSL).
 * All rights reserved.
 *
 * Permission to use, copy, modify and distribute this software and its
 * documentation is hereby granted, provided that both the copyright
 * notice and this permission notice appear in all copies of the
 * software, derivative works or modified versions, and any portions
 * thereof, and that both notices appear in supporting documentation.
 *
 * CARNEGIE MELLON, THE UNIVERSITY OF UTAH AND CSL ALLOW FREE USE OF
 * THIS SOFTWARE IN ITS "AS IS" CONDITION, AND DISCLAIM ANY LIABILITY
 * OF ANY KIND FOR ANY DAMAGES WHATSOEVER RESULTING FROM THE USE OF
 * THIS SOFTWARE.
 *
 * Carnegie Mellon requests users of this software to return to
 *
 *  Software Distribution Coordinator  or  Software.Distribution@CS.CMU.EDU
 *  School of Computer Science
 *  Carnegie Mellon University
 *  Pittsburgh PA 15213-3890
 *
 * any improvements or extensions that they make and grant Carnegie Mellon
 * the rights to redistribute these changes.
 */
/*
 *	File:	vm/vm_page.c
 *	Author:	Avadis Tevanian, Jr., Michael Wayne Young
 *
 *	Resident memory management module.
 */
#define RBTNODE_FUNCTION_DEFINITIONS
#include <kernel/vm/vm_page.h>
#include <kernel/vm/vm_page_map.h>

#include <kernel/sched_prim.h>
#include <kernel/vm/pmap.h>
#include <kernel/vm/vm_meter.h>
#include <kernel/vm/vm_param.h>
#include <kernel/zalloc.h>
#include <kernel/thread_data.h>

#include <mach/machine/spl.h>
#include <mach/machine/vm_param.h>

/*
 *	Occasionally, the virtual memory system uses
 *	resident page structures that do not refer to
 *	real pages, for example to leave a page with
 *	important state information in the VP table.
 *
 *	These page structures are allocated the way
 *	most other kernel structures are.
 */
ZONE_DEFINE(vm_page, vm_page_zone, PAGE_SIZE / sizeof(vm_page), 0, ZONE_FIXED, PAGE_SIZE / sizeof(vm_page));

/*
 *	Resident page structures are also chained on
 *	queues that are used by the page replacement
 *	system (pageout daemon).  These queues are
 *	defined here, but are shared by the pageout
 *	module.
 */

static simple_lock_data_t vm_page_queue_lock;
static queue_head_t s_active_list;
static queue_head_t s_inactive_list;

/*
 *	The VM system has a couple of heuristics for deciding
 *	that pages are "uninteresting" and should be placed
 *	on the inactive queue as likely candidates for replacement.
 *	These variables let the heuristics be controlled at run-time
 *	to make experimentation easier.
 */

boolean_t vm_page_deactivate_behind = TRUE;
boolean_t vm_page_deactivate_hint   = TRUE;

template <>
// ********************************************************************************************************************************
vm_page_t rbtnode_to_type<vm_page_t>(rbtnode_t a_node)
// ********************************************************************************************************************************
{
        return rbtnode_type(a_node, vm_page, m_map_link);
}

template <>
// ********************************************************************************************************************************
rbtnode_t type_to_rbtnode<vm_page_t>(vm_page_t a_vmp)
// ********************************************************************************************************************************
{
        return &a_vmp->m_map_link;
}

template <>
// ********************************************************************************************************************************
bool rbtnode_is_less<vm_offset_t, vm_page_t>(vm_offset_t a_offset, vm_page_t a_vmp)
// ********************************************************************************************************************************
{
        return a_offset < a_vmp->offset;
}

template <>
// ********************************************************************************************************************************
bool rbtnode_is_less<vm_page_t, vm_offset_t>(vm_page_t a_vmp, vm_offset_t a_offset)
// ********************************************************************************************************************************
{
        return a_vmp->offset < a_offset;
}

template <>
// ********************************************************************************************************************************
bool rbtnode_is_less<vm_page_t, vm_page_t>(vm_page_t a_lhs, vm_page_t a_rhs)
// ********************************************************************************************************************************
{
        return a_lhs->offset < a_rhs->offset;
}

// ********************************************************************************************************************************
void vm_page_map_init(vm_page_map_t a_vm_page_map)
// ********************************************************************************************************************************
{
        a_vm_page_map->m_root = rbtnode_init_sentinel(&a_vm_page_map->m_sentinel);
}

/*
 *	vm_page_map_lookup:
 *
 *	Returns the page associated with the object/offset
 *	pair specified; if none is found, VM_PAGE_NULL is returned.
 *
 *	The object must be locked.  No side effects.
 */
// ********************************************************************************************************************************
vm_page_t vm_page_map_lookup(vm_page_map_t a_map, vm_offset_t a_offset)
// ********************************************************************************************************************************
{
        vm_page_t vmp = rbtnode_find<vm_offset_t, vm_page_t>(a_offset, a_map->m_root);
        return vmp;
}

// ********************************************************************************************************************************
vm_page_t vm_page_map_first(vm_page_map_t a_vm_page_map)
// ********************************************************************************************************************************
{
        vm_page_t vmp = rbt_first<vm_page_t>(a_vm_page_map->m_root);
        return vmp;
}

// ********************************************************************************************************************************
vm_page_t vm_page_map_next(vm_page_t a_vm_page)
// ********************************************************************************************************************************
{
        vm_page_t vmp = rbt_next(a_vm_page);
        return vmp;
}

// ********************************************************************************************************************************
vm_page_t vm_page_map_remove(vm_page_map_t a_vm_page_map, vm_page_t a_vm_page)
// ********************************************************************************************************************************
{
        vm_page_t vmp_next = rbt_next(a_vm_page);
        rbtnode_delete(&a_vm_page->m_map_link, &a_vm_page_map->m_root);
        return vmp_next;
}

// ********************************************************************************************************************************
void vm_page_map_insert(vm_page_map_t a_map, vm_page_t a_vmp, vm_offset_t offset)
// ********************************************************************************************************************************
{
        a_vmp->offset = offset;
        KASSERT(rbtnode_insert(a_vmp, &a_map->m_root, &a_map->m_sentinel) == a_vmp);
}

// ********************************************************************************************************************************
static void vm_page_do_free(vm_page_t a_vmp)
// ********************************************************************************************************************************
{
        if (a_vmp->flags & PG_ACTIVE)
        {
                remqueue(&a_vmp->m_qlink);
                a_vmp->flags &= ~PG_ACTIVE;
                cnt.v_active_count--;
        }
        else if (a_vmp->flags & PG_INACTIVE)
        {
                remqueue(&a_vmp->m_qlink);
                a_vmp->flags &= ~PG_INACTIVE;
                cnt.v_inactive_count--;
        }

        pmap_free_page(a_vmp);
}

// ********************************************************************************************************************************
void vm_page_map_clear(vm_page_map_t a_vm_page_map)
// ********************************************************************************************************************************
{
        while (rbtnode_is_sentinel(a_vm_page_map->m_root) == false)
        {
                vm_page_t vmp = rbtnode_to_type<vm_page_t>(a_vm_page_map->m_root);

                rbtnode_delete(a_vm_page_map->m_root, &a_vm_page_map->m_root);

                vm_page_do_free(vmp);
        }
}

// ********************************************************************************************************************************
boolean_t vm_page_map_empty(vm_page_map_t a_vm_page_map)
// ********************************************************************************************************************************
{
        return rbtnode_is_sentinel(a_vm_page_map->m_root);
}

// ********************************************************************************************************************************
kern_return_t vm_page_rename(vm_page_map_t a_src_vmp_map, vm_page_t a_vmp, vm_page_map_t a_dst_vmp_map, vm_offset_t a_dst_offset)
// ********************************************************************************************************************************
{
        pmap_page_protect(a_vmp->phys_addr, VM_PROT_NONE);

        vm_offset_t old_off = a_vmp->offset;

        rbtnode_delete(&a_vmp->m_map_link, &a_src_vmp_map->m_root);

        a_vmp->offset = a_dst_offset;

        if (rbtnode_insert<vm_page_t>(a_vmp, &a_dst_vmp_map->m_root, &a_dst_vmp_map->m_sentinel) != a_vmp)
        {
                // Dst already has a page at this offset, put the vmp back in the old map
                a_vmp->offset = old_off;
                rbtnode_insert<vm_page_t>(a_vmp, &a_src_vmp_map->m_root, &a_src_vmp_map->m_sentinel);
                return KERN_MEMORY_PRESENT;
        }

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
void vm_page_map_remove_range(vm_page_map_t a_vmp_map, vm_offset_t a_start, vm_offset_t a_end)
// ********************************************************************************************************************************
{
        bool key_is_less;

        vm_page_t vmp = nullptr;
        for (rbtnode_t x = a_vmp_map->m_root; !rbtnode_is_sentinel(x); x = x->fields[!key_is_less])
        {
                vmp = rbtnode_to_type<vm_page_t>(x);

                key_is_less = a_start < vmp->offset;

                if ((vmp->offset >= a_start) && (vmp->offset < a_end))
                {
                        break;
                }

                vmp = nullptr;
        }

        while ((vmp != nullptr) && (vmp->offset < a_end))
        {
                vm_page_t vmp_next = rbt_next<vm_page_t>(vmp);

                rbtnode_delete(&vmp->m_map_link, &a_vmp_map->m_root);

                vm_page_do_free(vmp);

                vmp = vmp_next;
        }
}

// ********************************************************************************************************************************
void vm_page_wait(vm_page_t a_vmp)
// ********************************************************************************************************************************
{
        assert_wait(a_vmp, false);
        thread_block(nullptr);
}

// ********************************************************************************************************************************
void vm_page_wake(vm_page_t a_vmp)
// ********************************************************************************************************************************
{
        thread_wakeup(a_vmp);
}

// ********************************************************************************************************************************
vm_page_t vm_page_get_page(vm_address_t phys_addr)
// ********************************************************************************************************************************
{
        return pmap_get_managed_page(trunc_page(phys_addr));
}

// ********************************************************************************************************************************
static kern_return_t vm_page_bootstrap()
// ********************************************************************************************************************************
{
        /*
         *	Initialize the page queues.
         */
        simple_lock_init(&vm_page_queue_lock);

        queue_init(&s_active_list);
        queue_init(&s_inactive_list);

        return KERN_SUCCESS;
}

early_initcall(vm_page_bootstrap);

/*
 *	vm_page_alloc:
 *
 *	Allocate and return a memory cell associated
 *	with this VM object/offset pair.
 *
 *	Object must be locked.
 */
vm_page_t vm_page_alloc(vm_page_map_t a_vmp_map, vm_offset_t offset)
{
        vm_page_t mem = pmap_alloc_page();

        if (mem != nullptr)
        {
                (mem)->flags       = PG_BUSY | PG_CLEAN | PG_FAKE;
                mem->offset        = offset;
                *(mem->m_io_descr) = {};
                KASSERT(rbtnode_insert(mem, &a_vmp_map->m_root, &a_vmp_map->m_sentinel) == mem);
                (mem)->wire_count = 0;
        }

        return (mem);
}

vm_page_t vm_page_alloc_private(vm_page_map_t a_vmp_map, vm_offset_t offset, vm_address_t a_phys_address)
{
        vm_page_t mem = pmap_alloc_private_page(a_phys_address);
        (mem)->flags |= PG_BUSY;
        mem->offset = offset;
        KASSERT(rbtnode_insert(mem, &a_vmp_map->m_root, &a_vmp_map->m_sentinel) == mem);
        (mem)->wire_count = 0;
        return (mem);
}

/*
 *	vm_page_free:
 *
 *	Returns the given page to the free list,
 *	disassociating it with any VM object.
 *
 *	Object and page queues must be locked prior to entry.
 */
void vm_page_free(vm_page_map_t a_map, vm_page_t mem)
{
        vm_page_map_remove(a_map, mem);
        vm_page_do_free(mem);
}

/*
 *	vm_page_wire:
 *
 *	Mark this page as wired down by yet
 *	another map, removing it from paging queues
 *	as necessary.
 *
 *	The page's object and the page queues must be locked.
 */

void vm_page::wire()
{
        if (wire_count == 0)
        {
                simple_lock(&vm_page_queue_lock);

                if (flags & PG_ACTIVE)
                {
                        // TAILQ_REMOVE(&vm_page_queue_active, mem, pageq);
                        // vm_page::active_queue.remove(this);
                        remqueue(&m_qlink);
                        cnt.v_active_count--;
                        flags &= ~PG_ACTIVE;
                }
                else if (flags & PG_INACTIVE)
                {
                        // TAILQ_REMOVE(&vm_page::inactive_queue, mem, pageq);
                        // vm_page::inactive_queue.remove(this);
                        remqueue(&m_qlink);
                        cnt.v_inactive_count--;
                        flags &= ~PG_INACTIVE;
                }
                cnt.v_wire_count++;
                simple_unlock(&vm_page_queue_lock);
        }
        wire_count++;
}

/*
 *	vm_page_unwire:
 *
 *	Release one wiring of this page, potentially
 *	enabling it to be paged again.
 *
 *	The page's object and the page queues must be locked.
 */
void vm_page::unwire()
{
        wire_count--;
        if (wire_count == 0)
        {
                simple_lock(&vm_page_queue_lock);
                // TAILQ_INSERT_TAIL(&vm_page_queue_active, mem, pageq);
                // vm_page::active_queue.insert_tail(this);
                enqueue_tail(&s_active_list, &m_qlink);
                cnt.v_active_count++;
                flags |= PG_ACTIVE;
                cnt.v_wire_count--;
                simple_unlock(&vm_page_queue_lock);
        }
}

/*
 *	vm_page_deactivate:
 *
 *	Returns the given page to the inactive list,
 *	indicating that no physical maps have access
 *	to this page.  [Used by the physical mapping system.]
 *
 *	The page queues must be locked.
 */
void vm_page::deactivate()
{
        /*
         *	Only move active pages -- ignore locked or already
         *	inactive ones.
         */

        if (flags & PG_ACTIVE)
        {
                simple_lock(&vm_page_queue_lock);
                pmap_clear_reference(phys_addr);
                // TAILQ_REMOVE(&vm_page_queue_active, m, pageq);
                // TAILQ_INSERT_TAIL(&vm_page::inactive_queue, m, pageq);

                remqueue(&m_qlink);
                enqueue_tail(&s_inactive_list, &m_qlink);
                simple_unlock(&vm_page_queue_lock);

                flags &= ~PG_ACTIVE;
                flags |= PG_INACTIVE;
                cnt.v_active_count--;
                cnt.v_inactive_count++;
                if (pmap_is_modified(phys_addr))
                {
                        flags &= ~PG_CLEAN;
                }
                if (flags & PG_CLEAN)
                {
                        flags &= ~PG_LAUNDRY;
                }
                else
                {
                        flags |= PG_LAUNDRY;
                }
        }
}

/*
 *	vm_page_activate:
 *
 *	Put the specified page on the active list (if appropriate).
 *
 *	The page queues must be locked.
 */

void vm_page::activate()
{
        simple_lock(&vm_page_queue_lock);
        if (flags & PG_INACTIVE)
        {
                // TAILQ_REMOVE(&vm_page::inactive_queue, m, pageq);
                //               vm_page::inactive_queue.remove(this);
                remqueue(&m_qlink);
                cnt.v_inactive_count--;
                flags &= ~PG_INACTIVE;
        }
        if (wire_count == 0)
        {
                if (flags & PG_ACTIVE)
                {
                        panic("vm_page_activate: already active");
                }

                // TAILQ_INSERT_TAIL(&vm_page_queue_active, m, pageq);
                //             vm_page::active_queue.insert_tail(this);
                enqueue_tail(&s_active_list, &m_qlink);
                flags |= PG_ACTIVE;
                cnt.v_active_count++;
        }
        simple_unlock(&vm_page_queue_lock);
}

/*
 *	vm_page_zero_fill:
 *
 *	Zero-fill the specified page.
 */
int vm_page::zero_fill()
{
        flags &= ~PG_CLEAN;
        pmap_zero_page(phys_addr);
        return (TRUE);
}

/*
 *	vm_page_copy:
 *
 *	Copy one page to another
 */

void vm_page_copy(vm_page_t a_dest, vm_page_t a_source, vm_offset_t a_offset, size_t a_src_size)
{
        a_dest->flags &= ~PG_CLEAN;
        pmap_copy_page(a_dest->phys_addr, a_source->phys_addr, a_offset, a_src_size);
}

boolean_t vm_page_modified(vm_page_t a_vmp)
{
        return pmap_vm_page_modified(a_vmp->phys_addr);
}
