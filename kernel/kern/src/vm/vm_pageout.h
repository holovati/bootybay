#pragma once

#if !defined(__NEED_vm_page)
#define __NEED_vm_page
#endif

#if !defined(__NEED_vm_object)
#define __NEED_vm_object
#endif

#include <kernel/vm/vm_types.h>
#include <kernel/lock.h>


/*
 *	Header file for pageout daemon.
 */

/*
 *	Exported data structures.
 */

extern int vm_pages_needed; /* should be some "event" structure */
extern simple_lock_data_t vm_pages_needed_lock;

/*
 *	Exported routines.
 */

/*
 *	Signal pageout-daemon and wait for it.
 */

#define VM_WAIT                                                                                                                    \
        {                                                                                                                          \
                simple_lock(&vm_pages_needed_lock);                                                                                \
                thread_wakeup(&vm_pages_needed);                                                                                   \
                thread_sleep(&cnt.v_free_count, &vm_pages_needed_lock, FALSE);                                                     \
        }

void vm_pageout_init();

void vm_pageout(void);
void vm_pageout_scan(void);
void vm_pageout_page(vm_page_t a_pgp, vm_object_t a_op);
void vm_pageout_cluster(vm_page_t a_pgp, vm_object_t a_op);
