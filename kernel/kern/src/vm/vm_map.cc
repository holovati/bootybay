// clang-format off
/*
 * Mach Operating System
 * Copyright (c) 1991,1990,1989,1988,1987 Carnegie Mellon University.
 * Copyright (c) 1993,1994 The University of Utah and
 * the Computer Systems Laboratory (CSL).
 * All rights reserved.
 *
 * Permission to use, copy, modify and distribute this software and its
 * documentation is hereby granted, provided that both the copyright
 * notice and this permission notice appear in all copies of the
 * software, derivative works or modified versions, and any portions
 * thereof, and that both notices appear in supporting documentation.
 *
 * CARNEGIE MELLON, THE UNIVERSITY OF UTAH AND CSL ALLOW FREE USE OF
 * THIS SOFTWARE IN ITS "AS IS" CONDITION, AND DISCLAIM ANY LIABILITY
 * OF ANY KIND FOR ANY DAMAGES WHATSOEVER RESULTING FROM THE USE OF
 * THIS SOFTWARE.
 *
 * Carnegie Mellon requests users of this software to return to
 *
 *  Software Distribution Coordinator  or  Software.Distribution@CS.CMU.EDU
 *  School of Computer Science
 *  Carnegie Mellon University
 *  Pittsburgh PA 15213-3890
 *
 * any improvements or extensions that they make and grant Carnegie Mellon
 * the rights to redistribute these changes.
 */
/*
 *	File:	vm/vm_map.c
 *	Author:	Avadis Tevanian, Jr., Michael Wayne Young
 *	Date:	1985
 *
 *	Virtual memory mapping module.
 */

#include <etl/algorithm.hh>

#include <kernel/vm/vm_fault.h>
#include <kernel/vm/vm_param.h>

#include <kernel/vm/vm_kern.h>
#include <kernel/vm/vm_map.h>
#include <kernel/vm/vm_object.h>
#include <kernel/vm/vm_page.h>
#include <kernel/vm/vm_prot.h>

#include <kernel/zalloc.h>

/*
 * Macros to copy a vm_map_entry. We must be careful to correctly
 * manage the wired page count. vm_map_entry_copy() creates a new
 * map entry to the same memory - the wired count in the new entry
 * must be set to zero. vm_map_entry_copy_full() creates a new
 * entry that is identical to the old entry.  This preserves the
 * wire count; it's used for map splitting and zone changing in
 * vm_map_copyout.
 */
#define vm_map_entry_copy(NEW, OLD)                                                                                                \
        MACRO_BEGIN                                                                                                                \
        *(NEW)                  = *(OLD);                                                                                          \
        (NEW)->is_shared        = FALSE;                                                                                           \
        (NEW)->needs_wakeup     = FALSE;                                                                                           \
        (NEW)->in_transition    = FALSE;                                                                                           \
        (NEW)->wired_count      = 0;                                                                                               \
        (NEW)->user_wired_count = 0;                                                                                               \
        MACRO_END

#define vm_map_entry_copy_full(NEW, OLD) (*(NEW) = *(OLD))

/*
 *	Virtual memory maps provide for the mapping, protection,
 *	and sharing of virtual memory objects.  In addition,
 *	this module provides for an efficient virtual copy of
 *	memory from one map to another.
 *
 *	Synchronization is required prior to most operations.
 *
 *	Maps consist of an ordered doubly-linked list of simple
 *	entries; a single hint is used to speed up lookups.
 *
 *	Sharing maps have been deleted from this version of Mach.
 *	All shared objects are now mapped directly into the respective
 *	maps.  This requires a change in the copy on write strategy;
 *	the asymmetric (delayed) strategy is used for shared temporary
 *	objects instead of the symmetric (shadow) strategy.  This is
 *	selected by the (new) use_shared_copy bit in the object.  See
 *	vm_object_copy_temporary in vm_object.c for details.  All maps
 *	are now "top level" maps (either task map, kernel map or submap
 *	of the kernel map).
 *
 *	Since portions of maps are specified by start/end addreses,
 *	which may not align with existing map entries, all
 *	routines merely "clip" entries to these start/end values.
 *	[That is, an entry is split into two, bordering at a
 *	start or end value.]  Note that these clippings may not
 *	always be necessary (as the two resulting entries are then
 *	not changed); however, the clipping is done for convenience.
 *	No attempt is currently made to "glue back together" two
 *	abutting entries.
 *
 *	The symmetric (shadow) copy strategy implements virtual copy
 *	by copying VM object references from one map to
 *	another, and then marking both regions as copy-on-write.
 *	It is important to note that only one writeable reference
 *	to a VM object region exists in any map when this strategy
 *	is used -- this means that shadow object creation can be
 *	delayed until a write operation occurs.  The asymmetric (delayed)
 *	strategy allows multiple maps to have writeable references to
 *	the same region of a vm object, and hence cannot delay creating
 *	its copy objects.  See vm_object_copy_temporary() in vm_object.c.
 *	Copying of permanent objects is completely different; see
 *	vm_object_copy_strategically() in vm_object.c.
 */

/*
 *	Macros:		vm_map_lock, etc. [internal use only]
 *	Description:
 *		Perform locking on the data portion of a map.
 */

FORCEINLINE static void vm_map_lock_init(vm_map_t a_map)
{
        lock_init(&(a_map)->lock, TRUE);
        (a_map)->timestamp = 0;
}

static FORCEINLINE void vm_map_lock_read(vm_map_t a_map) { lock_read(&(a_map)->lock); }

static FORCEINLINE void vm_map_unlock_read(vm_map_t a_map) { lock_read_done(&(a_map)->lock); }

// static inline void vm_map_lock_write_to_read(vm_map_t a_map) { lock_write_to_read(&(a_map)->lock); }

static inline void vm_map_lock_read_to_write(vm_map_t a_map)
{
        (lock_read_to_write(&(a_map)->lock) || (((a_map)->timestamp++), 0));
}

// zone_t vm_map_zone;        /* zone for vm_map structures */
// zone_t vm_map_entry_zone;  /* zone for vm_map_entry structures */
// zone_t vm_map_kentry_zone; /* zone for kernel entry structures */
// zone_t vm_map_copy_zone;   /* zone for vm_map_copy structures */

#define INIT_ENTRY_COUNT 512

using vm_map_zone_t       = zone<vm_map, 40, PAGE_SIZE / sizeof(vm_map)>;
using vm_map_entry_zone_t = zone<vm_map_entry, INIT_ENTRY_COUNT * 1000, INIT_ENTRY_COUNT * 2>;

static vm_map_zone_t vm_map_zone;
static vm_map_entry_zone_t vm_map_entry_zone;
static vm_map_entry_zone_t vm_map_kentry_zone;

#if DAMIR
static kern_return_t vm_map_copyout_page_list(vm_map_t dst_map, vm_offset_t *dst_addr, vm_map_copy_t copy);
#endif
void vm_map_entry_unwire(vm_map_t map, vm_map_entry_t entry);

/*
 *	Placeholder object for submap operations.  This object is dropped
 *	into the range by a call to vm_map_find, and removed when
 *	vm_map_submap creates the submap.
 */

// vm_object_t vm_submap_object;

/*
 *	vm_map_init:
 *
 *	Initialize the vm_map module.  Must be called before
 *	any other vm_map routines.
 *
 *	Map and entry structures are allocated from zones -- we must
 *	initialize those zones.
 *
 *	There are three zones of interest:
 *
 *	vm_map_zone:		used to allocate maps.
 *	vm_map_entry_zone:	used to allocate map entries.
 *	vm_map_kentry_zone:	used to allocate map entries for the kernel.
 *
 *	The kernel allocates map entries from a special zone that is initially
 *	"crammed" with memory.  It would be difficult (perhaps impossible) for
 *	the kernel to allocate more memory to a entry zone when it became
 *	empty since the very act of allocating memory implies the creation
 *	of a new entry.
 */

// void *kentry_data;
// vm_size_t kentry_data_size;

void vm_map_init()
{
        // kentry_data_size = INIT_ENTRY_COUNT * sizeof(struct vm_map_entry);
        // kentry_data      = pmap_vmbrk(kentry_data_size);
#if DAMIR
        vm_map_zone        = zinit((vm_size_t)sizeof(struct vm_map), 40 * 1024, PAGE_SIZE, 0, "maps");
        vm_map_entry_zone  = zinit((vm_size_t)sizeof(struct vm_map_entry), 1024 * 1024, PAGE_SIZE * 5, 0, "non-kernel map entries");
        vm_map_kentry_zone = zinit(
            (vm_size_t)sizeof(struct vm_map_entry), kentry_data_size, kentry_data_size, ZONE_FIXED /* XXX */, "kernel map entries");

        vm_map_copy_zone = zinit((vm_size_t)sizeof(struct vm_map_copy), 16 * 1024, PAGE_SIZE, 0, "map copies");
#endif

        vm_map_zone_t::init(&vm_map_zone, "maps");
        vm_map_entry_zone_t::init(&vm_map_entry_zone, "non-kernel map entries");
        vm_map_entry_zone_t::init(&vm_map_kentry_zone, "non-kernel map entries");

        /*
         *	Cram the kentry zone with initial data.
         */
        // zcram(vm_map_kentry_zone, kentry_data, kentry_data_size);
        // vm_map_kentry_zone.cram(kentry_data, kentry_data_size);
        /*
         *	Submap object is initialized by vm_object_init.
         */
}

/*
 *	vm_map_create:
 *
 *	Creates and returns a new empty VM map with
 *	the given physical map structure, and having
 *	the given lower and upper address bounds.
 */
vm_map_t vm_map_create(pmap_t pmap, vm_offset_t min, vm_offset_t max, boolean_t pageable)
{
        vm_map_t result;

        result = vm_map_zone.alloc();

        if (result == VM_MAP_NULL)
        {
                panic("vm_map_create");
        }

        vm_map_first_entry(result)   = vm_map_to_entry(result);
        vm_map_last_entry(result)    = vm_map_to_entry(result);
        result->hdr.nentries         = 0;
        result->hdr.entries_pageable = pageable;

        result->size            = 0;
        result->ref_count       = 1;
        result->is_main_map     = TRUE;
        result->pmap            = pmap;
        result->hdr.links.start = min;
        result->hdr.links.end   = max;
        // result->wiring_required = FALSE;
        // result->wait_for_space  = FALSE;
        result->first_free = vm_map_to_entry(result);
        result->hint       = vm_map_to_entry(result);
        vm_map_lock_init(result);
        simple_lock_init(&result->ref_lock);
        simple_lock_init(&result->hint_lock);

        return (result);
}

/*
 *	vm_map_entry_create:	[ internal use only ]
 *
 *	Allocates a VM map entry for insertion in the
 *	given map (or map copy).  No fields are filled.
 */

// #define vm_map_copy_entry_create(copy) _vm_map_entry_create(&(copy)->cpy_hdr)

static vm_map_entry_t vm_map_entry_create(struct vm_map_header *map_header)
{
        // zone_t zone;
        vm_map_entry_zone_t *zone;
        vm_map_entry_t entry;

        if (map_header->entries_pageable)
        {
                zone = &vm_map_entry_zone;
        }
        else
        {
                zone = &vm_map_kentry_zone;
        }

        entry = zone->alloc();

        if (entry == VM_MAP_ENTRY_NULL)
        {
                panic("vm_map_entry_create");
        }

        return (entry);
}

static vm_map_entry_t vm_map_entry_create(vm_map_t a_map) { return vm_map_entry_create(&a_map->hdr); }

/*
 *	vm_map_entry_dispose:	[ internal use only ]
 *
 *	Inverse of vm_map_entry_create.
 */

// #define vm_map_copy_entry_dispose(map, entry) _vm_map_entry_dispose(&(copy)->cpy_hdr, (entry))

static void vm_map_entry_dispose(struct vm_map_header *map_header, vm_map_entry_t entry)
{
        if ((entry->flags & VM_MAP_ENTRY_NO_FREE) == 0)
        {
                vm_map_entry_zone_t *z;

                if (map_header->entries_pageable)
                {
                        z = &vm_map_entry_zone;
                }
                else
                {
                        z = &vm_map_kentry_zone;
                }
                // zfree(zone, (vm_offset_t)entry);
                z->free(entry);
        }
}

static void vm_map_entry_dispose(vm_map_t a_map, vm_map_entry_t a_entry) { vm_map_entry_dispose(&(a_map->hdr), a_entry); }

/*
 *	vm_map_entry_{un,}link:
 *
 *	Insert/remove entries from maps (or map copies).
 */

static void vm_map_entry_link(vm_map_header *a_hdr, vm_map_entry_t a_prev, vm_map_entry_t a_new_entry)
{
        a_hdr->nentries++;
        a_new_entry->links.prev         = a_prev;
        a_new_entry->links.next         = a_prev->links.next;
        a_new_entry->prev()->links.next = a_new_entry->next()->links.prev = a_new_entry;
}

static void vm_map_entry_link(vm_map_t a_map, vm_map_entry_t a_prev, vm_map_entry_t a_new_entry)
{
        vm_map_entry_link(&(a_map->hdr), a_prev, a_new_entry);
}

static void vm_map_entry_unlink(vm_map_header *a_hdr, vm_map_entry_t a_entry)
{
        a_hdr->nentries--;

        a_entry->next()->links.prev = a_entry->prev();
        a_entry->prev()->links.next = a_entry->next();
}

static void vm_map_entry_unlink(vm_map_t a_map, vm_map_entry_t a_entry) { vm_map_entry_unlink(&(a_map->hdr), a_entry); }

// #define vm_map_copy_entry_link(copy, after_where, entry) _vm_map_entry_link(&(copy)->cpy_hdr, after_where, entry)
// #define vm_map_copy_entry_unlink(copy, entry) _vm_map_entry_unlink(&(copy)->cpy_hdr, entry)

/*
 *	vm_map_reference:
 *
 *	Creates another valid reference to the given map.
 *
 */
void vm_map_reference(vm_map_t map)
{
        if (map == VM_MAP_NULL)
        {
                return;
        }

        simple_lock(&map->ref_lock);
        map->ref_count++;
        simple_unlock(&map->ref_lock);
}

/*
 *	vm_map_deallocate:
 *
 *	Removes a reference from the specified map,
 *	destroying it if no references remain.
 *	The map should not be locked.
 */
void vm_map_deallocate(vm_map_t map)
{
        if (map == NULL)
                return;

        simple_lock(&map->ref_lock);
        if (--map->ref_count > 0)
        {
                simple_unlock(&map->ref_lock);
                return;
        }

        /*
         *	Lock the map, to wait out all other references
         *	to it.
         */

        vm_map_lock_drain_interlock(map);

        vm_map_delete(map, map->min_offset(), map->max_offset());

        pmap_destroy(map->pmap);

        vm_map_unlock(map);

        // zfree(vm_map_zone, (vm_offset_t)map);
        vm_map_zone.free(map);
}

/*
 *	SAVE_HINT:
 *
 *	Saves the specified entry as the hint for
 *	future lookups.  Performs necessary interlocks.
 */
#define SAVE_HINT(map, value)                                                                                                      \
        simple_lock(&(map)->hint_lock);                                                                                            \
        (map)->hint = (value);                                                                                                     \
        simple_unlock(&(map)->hint_lock);

/*
 *	vm_map_lookup_entry:
 *
 *	Finds the map entry containing (or
 *	immediately preceding) the specified address
 *	in the given map; the entry is returned
 *	in the "entry" parameter.  The boolean
 *	result indicates whether the address is
 *	actually contained in the map.
 */
boolean_t vm_map_lookup_entry(vm_map_t map, vm_offset_t address, vm_map_entry_t *entry) /* OUT */
{
        vm_map_entry_t cur;
        vm_map_entry_t last;

        /*
         *	Start looking either from the head of the
         *	list, or from the hint.
         */

        simple_lock(&map->hint_lock);
        cur = map->hint;
        simple_unlock(&map->hint_lock);

        if (cur == vm_map_to_entry(map))
        {
                cur = cur->next();
        }

        if (address >= cur->start())
        {
                /*
                 *	Go from hint to end of list.
                 *
                 *	But first, make a quick check to see if
                 *	we are already looking at the entry we
                 *	want (which is usually the case).
                 *	Note also that we don't need to save the hint
                 *	here... it is the same hint (unless we are
                 *	at the header, in which case the hint didn't
                 *	buy us anything anyway).
                 */
                last = vm_map_to_entry(map);
                if ((cur != last) && (cur->end() > address))
                {
                        *entry = cur;
                        return (TRUE);
                }
        }
        else
        {
                /*
                 *	Go from start to hint, *inclusively*
                 */
                last = cur->next();
                cur  = vm_map_first_entry(map);
        }

        /*
         *	Search linearly
         */

        while (cur != last)
        {
                if (cur->end() > address)
                {
                        if (address >= cur->start())
                        {
                                /*
                                 *	Save this lookup for future
                                 *	hints, and return
                                 */
                                *entry = cur;
                                SAVE_HINT(map, cur);
                                return (TRUE);
                        }
                        break;
                }
                cur = cur->next();
        }

        *entry = cur->prev();
        SAVE_HINT(map, *entry);
        return (FALSE);
        
}

/*
 *	Routine:	vm_map_find_entry
 *	Purpose:
 *		Allocate a range in the specified virtual address map,
 *		returning the entry allocated for that range.
 *		Used by kmem_alloc, etc.  Returns wired entries.
 *
 *		The map must be locked.
 *
 *		If an entry is allocated, the object/offset fields
 *		are initialized to zero.  If an object is supplied,
 *		then an existing entry may be extended.
 */
kern_return_t vm_map_find_entry(vm_map_t map,
                                vm_offset_t *address, /* OUT */
                                vm_size_t size,
                                vm_offset_t mask,
                                vm_object_t object,
                                vm_map_entry_t *o_entry /* OUT */)
{
        vm_map_entry_t entry, new_entry;
        vm_offset_t start;
        vm_offset_t end;

        /*
         *	Look for the first possible address;
         *	if there's already something at this
         *	address, we have to start after it.
         */

        if ((entry = map->first_free) == vm_map_to_entry(map))
        {
                start = map->min_offset();
        }
        else
        {
                start = entry->end();
        }

        /*
         *	In any case, the "entry" always precedes
         *	the proposed new region throughout the loop:
         */
        while (TRUE)
        {
                vm_map_entry_t next;

                /*
                 *	Find the end of the proposed new region.
                 *	Be sure we didn't go beyond the end, or
                 *	wrap around the address.
                 */

                if (((start + mask) & ~mask) < start)
                {
                        return (KERN_NO_SPACE);
                }

                start = ((start + mask) & ~mask);
                end   = start + size;

                if ((end > map->max_offset()) || (end < start))
                {
                        return (KERN_NO_SPACE);
                }

                /*
                 *	If there are no more entries, we must win.
                 */

                next = entry->next();
                if (next == vm_map_to_entry(map))
                {
                        break;
                }

                /*
                 *	If there is another entry, it must be
                 *	after the end of the potential new region.
                 */

                if (next->start() >= end)
                {
                        break;
                }

                /*
                 *	Didn't fit -- move to the next entry.
                 */

                entry = next;
                start = entry->end();
        }

        /*
         *	At this point,
         *		"start" and "end" should define the endpoints of the
         *			available new range, and
         *		"entry" should refer to the region before the new
         *			range, and
         *
         *		the map should be locked.
         */

        *address = start;

        /*
         *	See whether we can avoid creating a new entry by
         *	extending one of our neighbors.  [So far, we only attempt to
         *	extend from below.]
         */

        if ((object != NULL) && (entry != vm_map_to_entry(map)) && (entry->end() == start)
            && (!(entry->flags & VM_MAP_ENTRY_IS_SHARED)) && (!(entry->flags & VM_MAP_ENTRY_IS_A_SUBMAP))
            && (entry->object.vm_object == object) && (!(entry->flags & VM_MAP_ENTRY_NEEDS_COPY))
            && (entry->inheritance == VM_INHERIT_DEFAULT) && (entry->protection == VM_PROT_DEFAULT)
            && (entry->max_protection == VM_PROT_ALL) && (entry->wired_count == 1) && (entry->user_wired_count == 0)
            && (entry->projected_on == 0))
        {
                /*
                 *	Because this is a special case,
                 *	we don't need to use vm_object_coalesce.
                 */
                entry->set_end(end);
                new_entry = entry;
        }
        else
        {
                new_entry = vm_map_entry_create(map);

                new_entry->set_start(start);
                new_entry->set_end(end);

                // new_entry->is_shared        = FALSE;
                // new_entry->is_sub_map       = FALSE;
                new_entry->object.vm_object = object;
                
                if (new_entry->object.vm_object != nullptr)
                {
                        vm_object_ref(new_entry->object.vm_object);
                }

                new_entry->offset           = 0;

                // new_entry->needs_copy = FALSE;

                new_entry->inheritance      = VM_INHERIT_DEFAULT;
                new_entry->protection       = VM_PROT_DEFAULT;
                new_entry->max_protection   = VM_PROT_ALL;
                new_entry->wired_count      = 1;
                new_entry->user_wired_count = 0;

                // new_entry->in_transition = FALSE;
                // new_entry->needs_wakeup  = FALSE;
                new_entry->flags        = 0;
                new_entry->projected_on = nullptr;

                /*
                 *	Insert the new entry into the list
                 */

                vm_map_entry_link(map, entry, new_entry);
        }

        map->size += size;

        /*
         *	Update the free space hint and the lookup hint
         */

        map->first_free = new_entry;
        SAVE_HINT(map, new_entry);

        *o_entry = new_entry;
        return (KERN_SUCCESS);
}

/*
 *	vm_map_clip_start:	[ internal use only ]
 *
 *	Asserts that the given entry begins at or after
 *	the specified address; if necessary,
 *	it splits the entry into two.
 */

#define vm_map_clip_start(map, entry, startaddr)                                                                                   \
        do                                                                                                                         \
        {                                                                                                                          \
                if ((startaddr) > (entry)->start())                                                                                \
                        _vm_map_clip_start(&(map)->hdr, (entry), (startaddr));                                                     \
        } while (0)

#define vm_map_copy_clip_start(copy, entry, startaddr)                                                                             \
        do                                                                                                                         \
        {                                                                                                                          \
                if ((startaddr) > (entry)->start())                                                                                \
                        _vm_map_clip_start(&(copy)->cpy_hdr, (entry), (startaddr));                                                \
        } while (0)

/*
 *	This routine is called only when it is known that
 *	the entry must be split.
 */
static void _vm_map_clip_start(struct vm_map_header *map_header, vm_map_entry_t entry, vm_offset_t start)
{
        vm_map_entry_t new_entry;

        /*
         *	Split off the front portion --
         *	note that we must insert the new
         *	entry BEFORE this one, so that
         *	this entry has the specified starting
         *	address.
         */

        new_entry = vm_map_entry_create(map_header);
        vm_map_entry_copy_full(new_entry, entry);

        new_entry->set_end(start);
        entry->offset += (start - entry->start());
        entry->set_start(start);

        vm_map_entry_link(map_header, entry->prev(), new_entry);

        if (entry->flags & VM_MAP_ENTRY_IS_A_SUBMAP)
        {
                vm_map_reference(new_entry->object.sub_map);
        }
        else
        {
                if (new_entry->object.vm_object)
                {
                        // If anonymous memory was allocated but not accessed the object will be allocated
                        vm_object_ref(new_entry->object.vm_object);
                }
        }
}

/*
 *	vm_map_clip_end:	[ internal use only ]
 *
 *	Asserts that the given entry ends at or before
 *	the specified address; if necessary,
 *	it splits the entry into two.
 */
#define vm_map_clip_end(map, entry, endaddr)                                                                                       \
        do                                                                                                                         \
        {                                                                                                                          \
                if ((endaddr) < (entry)->end())                                                                                    \
                        _vm_map_clip_end(&(map)->hdr, (entry), (endaddr));                                                         \
        } while (0)

#define vm_map_copy_clip_end(copy, entry, endaddr)                                                                                 \
        do                                                                                                                         \
        {                                                                                                                          \
                if ((endaddr) < (entry)->end())                                                                                    \
                        _vm_map_clip_end(&(copy)->cpy_hdr, (entry), (endaddr));                                                    \
        } while (0)

/*
 *	This routine is called only when it is known that
 *	the entry must be split.
 */
static void _vm_map_clip_end(struct vm_map_header *map_header, vm_map_entry_t entry, vm_offset_t end)
{
        vm_map_entry_t new_entry;

        /*
         *	Create a new entry and insert it
         *	AFTER the specified entry
         */

        new_entry = vm_map_entry_create(map_header);
        vm_map_entry_copy_full(new_entry, entry);

        entry->set_end(end);
        new_entry->set_start(end);

        new_entry->offset += (end - entry->start());

        vm_map_entry_link(map_header, entry, new_entry);

        if (entry->flags & VM_MAP_ENTRY_IS_A_SUBMAP)
        {
                vm_map_reference(new_entry->object.sub_map);
        }
        else
        {
                if (new_entry->object.vm_object)
                {
                        // If anonymous memory was allocated but not accessed the object will be allocated
                        vm_object_ref(new_entry->object.vm_object);
                }
        }
}

/*
 *	VM_MAP_RANGE_CHECK:	[ internal use only ]
 *
 *	Asserts that the starting and ending region
 *	addresses fall within the valid range of the map.
 */
#define VM_MAP_RANGE_CHECK(map, start, end)                                                                                        \
        {                                                                                                                          \
                if (start < map->min_offset())                                                                                     \
                        start = map->min_offset();                                                                                 \
                if (end > map->max_offset())                                                                                       \
                        end = map->max_offset();                                                                                   \
                if (start > end)                                                                                                   \
                        start = end;                                                                                               \
        }

/*
 *	vm_map_submap:		[ kernel use only ]
 *
 *	Mark the given range as handled by a subordinate map.
 *
 *	This range must have been created with vm_map_find using
 *	the vm_submap_object, and no other operations may have been
 *	performed on this range prior to calling vm_map_submap.
 *
 *	Only a limited number of operations can be performed
 *	within this rage after calling vm_map_submap:
 *		vm_fault
 *	[Don't try vm_map_copyin!]
 *
 *	To remove a submapping, one must first remove the
 *	range from the superior map, and then destroy the
 *	submap (if desired).  [Better yet, don't try it.]
 */
kern_return_t vm_map_submap(vm_map_t map, vm_offset_t start, vm_offset_t end, vm_map_t submap)
{
        vm_map_entry_t entry;
        int result = KERN_INVALID_ARGUMENT;

        vm_map_lock(map);

        VM_MAP_RANGE_CHECK(map, start, end);

        if (vm_map_lookup_entry(map, start, &entry))
        {
                vm_map_clip_start(map, entry, start);
        }
        else
        {
                entry = entry->next();
        }

        vm_map_clip_end(map, entry, end);

        if ((entry->start() == start) && (entry->end() == end) && (!(entry->flags & (VM_MAP_ENTRY_IS_A_MAP | VM_MAP_ENTRY_COW)))
            && (entry->object.vm_object == NULL))
        {
                entry->flags &= ~VM_MAP_ENTRY_IS_A_MAP;
                entry->flags |= VM_MAP_ENTRY_IS_A_SUBMAP;
                vm_map_reference(entry->object.sub_map = submap);
                result = KERN_SUCCESS;
        }
        vm_map_unlock(map);

        return (result);
}

/*
 *	vm_map_inherit:
 *
 *	Sets the inheritance of the specified address
 *	range in the target map.  Inheritance
 *	affects how the map will be shared with
 *	child maps at the time of vm_map_fork.
 */
int vm_map_inherit(vm_map_t map, vm_offset_t start, vm_offset_t end, vm_inherit_t new_inheritance)
{
        vm_map_entry_t entry;
        vm_map_entry_t temp_entry;

        switch (new_inheritance)
        {
        case VM_INHERIT_NONE:
        case VM_INHERIT_COPY:
        case VM_INHERIT_SHARE:
                break;
        default:
                return (KERN_INVALID_ARGUMENT);
        }

        vm_map_lock(map);

        VM_MAP_RANGE_CHECK(map, start, end);

        if (vm_map_lookup_entry(map, start, &temp_entry))
        {
                entry = temp_entry;
                vm_map_clip_start(map, entry, start);
        }
        else
        {
                entry = temp_entry->next();
        }

        while ((entry != vm_map_to_entry(map)) && (entry->start() < end))
        {
                vm_map_clip_end(map, entry, end);

                entry->inheritance = new_inheritance;

                entry = entry->next();
        }

        vm_map_unlock(map);
        return (KERN_SUCCESS);
}

/*
 *	vm_map_protect:
 *
 *	Sets the protection of the specified address
 *	region in the target map.  If "set_max" is
 *	specified, the maximum protection is to be set;
 *	otherwise, only the current protection is affected.
 */
kern_return_t vm_map_protect(vm_map_t map, vm_offset_t start, vm_offset_t end, vm_prot_t new_prot, boolean_t set_max)
{
        vm_map_entry_t current;
        vm_map_entry_t entry;

        vm_map_lock(map);

        VM_MAP_RANGE_CHECK(map, start, end);

        if (vm_map_lookup_entry(map, start, &entry))
        {
                vm_map_clip_start(map, entry, start);
        }
        else
        {
                entry = entry->next();
        }
        /*
         *	Make a first pass to check for protection
         *	violations.
         */

        current = entry;
        while ((current != vm_map_to_entry(map)) && (current->start() < end))
        {
                if (current->flags & VM_MAP_ENTRY_IS_A_SUBMAP)
                {
                        return (KERN_INVALID_ARGUMENT);
                }

                if ((new_prot & current->max_protection) != new_prot)
                {
                        vm_map_unlock(map);
                        return (KERN_PROTECTION_FAILURE);
                }

                current = current->next();
        }

        /*
         *	Go back and fix up protections.
         *	[Note that clipping is not necessary the second time.]
         */

        current = entry;

        while ((current != vm_map_to_entry(map)) && (current->start() < end))
        {
                vm_prot_t old_prot;

                vm_map_clip_end(map, current, end);

                old_prot = current->protection;
                if (set_max)
                {
                        current->protection = (current->max_protection = new_prot) & old_prot;
                }
                else
                {
                        current->protection = new_prot;
                }

                /*
                 *	Update physical map if necessary.
                 *	Worry about copy-on-write here -- CHECK THIS XXX
                 */

                if (current->protection != old_prot)
                {
#define MASK(entry) (((entry)->flags & VM_MAP_ENTRY_COW) ? ~VM_PROT_WRITE : VM_PROT_ALL)

                        if (current->flags & VM_MAP_ENTRY_IS_A_MAP)
                        {
                                vm_map_entry_t share_entry;
                                vm_offset_t share_end;

                                vm_map_lock(current->object.share_map);
                                vm_map_lookup_entry(current->object.share_map, current->offset, &share_entry);

                                share_end = current->offset + (current->end() - current->start());

                                while ((share_entry != vm_map_to_entry(current->object.share_map))
                                       && (share_entry->start() < share_end))
                                {
#if 0
                                       // pmap_protect(
                                            map->pmap,
                                            (etl::max(share_entry->start(), current->offset) - current->offset + current->start()),
                                            etl::min(share_entry->end(), share_end) - current->offset + current->start(),
                                            current->protection & MASK(share_entry));
#endif
                                        panic("Removed");
                                        share_entry = share_entry->next();
                                }
                                vm_map_unlock(current->object.share_map);
                        }
                        else
                        {
                                if (current->object.vm_object != nullptr)
                                        vm_object_prot_change(current->object.vm_object, current->protection & MASK(entry));
                                // pmap_protect(map->pmap, current->start(), current->end(), current->protection & MASK(entry));
                        }
#undef MASK
                }
                current = current->next();
        }

        vm_map_unlock(map);
        return (KERN_SUCCESS);
}

/*
 *	vm_map_entry_delete:	[ internal use only ]
 *
 *	Deallocate the given entry from the target map.
 */
static void vm_map_entry_delete(vm_map_t map, vm_map_entry_t entry)
{
        if (entry->wired_count != 0)
        {
                vm_map_entry_unwire(map, entry);
        }

        vm_map_entry_unlink(map, entry);

        map->size -= entry->end() - entry->start();

        if (entry->flags & (VM_MAP_ENTRY_IS_A_MAP | VM_MAP_ENTRY_IS_A_SUBMAP))
        {
                vm_map_deallocate(entry->object.share_map);
        }
        else
        {
                // vm_object_deallocate(entry->object.vm_object);
                if (entry->object.vm_object)
                {
                        vm_object_rel(entry->object.vm_object);
                }
        }

        vm_map_entry_dispose(map, entry);
}

/*
 *
 *	Deallocates the given address range from the target
 *	map.
 *
 *	When called with a sharing map, removes pages from
 *	that region from all physical maps.
 */
int vm_map_delete(vm_map_t map, vm_offset_t start, vm_offset_t end)
{
        vm_map_entry_t entry;
        vm_map_entry_t first_entry;

        /*
         *	Find the start of the region, and clip it
         */

        if (!vm_map_lookup_entry(map, start, &first_entry))
        {
                entry = first_entry->next();
        }
        else
        {
                entry = first_entry;
                vm_map_clip_start(map, entry, start);

                /*
                 *	Fix the lookup hint now, rather than each
                 *	time though the loop.
                 */

                SAVE_HINT(map, entry->prev());
        }

        /*
         *	Save the free space hint
         */

        if (map->first_free->start() >= start)
        {
                map->first_free = entry->prev();
        }
        /*
         *	Step through all entries in this region
         */

        while ((entry != vm_map_to_entry(map)) && (entry->start() < end))
        {
                vm_map_entry_t next;
                vm_offset_t s, e;
                vm_object_t object;

                vm_map_clip_end(map, entry, end);

                next = entry->next();
                s    = entry->start();
                e    = entry->end();

                /*
                 *	Unwire before removing addresses from the pmap;
                 *	otherwise, unwiring will put the entries back in
                 *	the pmap.
                 */

                object = entry->object.vm_object;
                if (entry->wired_count != 0)
                {
                        vm_map_entry_unwire(map, entry);
                }
                /*
                 *	If this is a sharing map, we must remove
                 *	*all* references to this data, since we can't
                 *	find all of the physical maps which are sharing
                 *	it.
                 */

                if (kmem_is_kernel_object(object) == true /*|| object == kmem_object*/)
                {
                        pmap_remove(map->pmap, entry->offset, entry->offset + (e - s));
                        
                        if (object->m_page_map != nullptr)
                        {
                                vm_page_map_remove_range(object->m_page_map, entry->offset, entry->offset + (e - s));
                        }
                }
                else if (!map->is_main_map)
                {
                        panic("VM_OBJECT");
                        // object->opmap_remove(entry->offset, entry->offset + (e - s));
                }
                else
                {
                        pmap_remove(map->pmap, s, e);
                }
                /*
                 *	Delete the entry (which may delete the object)
                 *	only after removing all pmap entries pointing
                 *	to its pages.  (Otherwise, its page frames may
                 *	be reallocated, and any modify bits will be
                 *	set in the wrong object!)
                 */

                vm_map_entry_delete(map, entry);
                entry = next;
        }
        return (KERN_SUCCESS);
}

/*
 *	vm_map_remove:
 *
 *	Remove the given address range from the target map.
 *	This is the exported form of vm_map_delete.
 */
kern_return_t vm_map_remove(vm_map_t map, vm_offset_t start, vm_offset_t end)
{
        vm_map_lock(map);
        VM_MAP_RANGE_CHECK(map, start, end);
        kern_return_t result = vm_map_delete(map, start, end);
        vm_map_unlock(map);
        return (result);
}

/*
 *	Macro:		vm_map_copy_insert
 *
 *	Description:
 *		Link a copy chain ("copy") into a map at the
 *		specified location (after "where").
 *	Side effects:
 *		The copy chain is destroyed.
 *	Warning:
 *		The arguments are evaluated multiple times.
 */
#define vm_map_copy_insert(map, where, copy)                                                                                       \
        MACRO_BEGIN(((where)->next())->prev() = vm_map_copy_last_entry(copy))->next() = ((where)->next());                         \
        ((where)->next() = vm_map_copy_first_entry(copy))->prev()                     = (where);                                   \
        (map)->hdr.nentries += (copy)->cpy_hdr.nentries;                                                                           \
        zfree(vm_map_copy_zone, (vm_offset_t)copy);                                                                                \
        MACRO_END

#if 0
/*
 *	vm_map_lookup:
 *
 *	Finds the VM object, offset, and
 *	protection for a given virtual address in the
 *	specified map, assuming a page fault of the
 *	type specified.
 *
 *	Leaves the map in question locked for read; return
 *	values are guaranteed until a vm_map_lookup_done
 *	call is performed.  Note that the map argument
 *	is in/out; the returned map must be used in
 *	the call to vm_map_lookup_done.
 *
 *	A handle (out_entry) is returned for use in
 *	vm_map_lookup_done, to make that fast.
 *
 *	If a lookup is requested with "write protection"
 *	specified, the map may be changed to perform virtual
 *	copying operations, although the data referenced will
 *	remain the same.
 */
kern_return_t vm_map_lookup(vm_map_t *var_map, /* IN/OUT */
                            vm_offset_t vaddr,
                            vm_prot_t fault_type,
                            vm_map_entry_t *out_entry, /* OUT */
                            vm_object_t *object,       /* OUT */
                            vm_offset_t *offset,       /* OUT */
                            vm_prot_t *out_prot,       /* OUT */
                            boolean_t *wired,          /* OUT */
                            boolean_t *single_use /* OUT */)
{
        vm_map_t share_map;
        vm_offset_t share_offset;
        vm_map_entry_t entry;
        vm_map_t map = *var_map;
        vm_prot_t prot;
        boolean_t su;

RetryLookup:;

        /*
         *	Lookup the faulting address.
         */

        vm_map_lock_read(map);

#define RETURN(why)                                                                                                                \
        {                                                                                                                          \
                vm_map_unlock_read(map);                                                                                           \
                return (why);                                                                                                      \
        }

        /*
         *	If the map has an interesting hint, try it before calling
         *	full blown lookup routine.
         */

        simple_lock(&map->hint_lock);
        entry = map->hint;
        simple_unlock(&map->hint_lock);

        *out_entry = entry;

        if ((entry == vm_map_to_entry(map)) || (vaddr < entry->start()) || (vaddr >= entry->end()))
        {
                vm_map_entry_t tmp_entry;

                /*
                 *	Entry was either not a valid hint, or the vaddr
                 *	was not contained in the entry, so do a full lookup.
                 */
                if (!vm_map_lookup_entry(map, vaddr, &tmp_entry))
                        RETURN(KERN_INVALID_ADDRESS);

                entry      = tmp_entry;
                *out_entry = entry;
        }

        /*
         *	Handle submaps.
         */

        if (entry->flags & VM_MAP_ENTRY_IS_A_SUBMAP)
        {
                vm_map_t old_map = map;

                *var_map = map = entry->object.sub_map;
                vm_map_unlock_read(old_map);
                goto RetryLookup;
        }

        /*
         *	Check whether this task is allowed to have
         *	this page.
         */

        prot = entry->protection;
        if ((fault_type & (prot)) != fault_type)
                RETURN(KERN_PROTECTION_FAILURE);

        /*
         *	If this page is not pageable, we have to get
         *	it for all possible accesses.
         */

        if (*wired = (entry->wired_count != 0))
                prot = fault_type = entry->protection;

        /*
         *	If we don't already have a VM object, track
         *	it down.
         */

        if (su = !(entry->flags & VM_MAP_ENTRY_IS_A_MAP))
        {
                share_map    = map;
                share_offset = vaddr;
        }
        else
        {
                vm_map_entry_t share_entry;

                /*
                 *	Compute the sharing map, and offset into it.
                 */

                share_map    = entry->object.share_map;
                share_offset = (vaddr - entry->start()) + entry->offset;

                /*
                 *	Look for the backing store object and offset
                 */

                vm_map_lock_read(share_map);

                if (!vm_map_lookup_entry(share_map, share_offset, &share_entry))
                {
                        vm_map_unlock_read(share_map);
                        RETURN(KERN_INVALID_ADDRESS);
                }
                entry = share_entry;
        }

        /*
         *	If the entry was copy-on-write, we either ...
         */

        if (entry->flags & VM_MAP_ENTRY_NEEDS_COPY)
        {
                /*
                 *	If we want to write the page, we may as well
                 *	handle that now since we've got the sharing
                 *	map locked.
                 *
                 *	If we don't need to write the page, we just
                 *	demote the permissions allowed.
                 */

                if (fault_type & VM_PROT_WRITE)
                {
                        // panic("STAP");
#if DAMIR
                        /*
                         *	Make a new object, and place it in the
                         *	object chain.  Note that no new references
                         *	have appeared -- one just moved from the
                         *	share map to the new object.
                         */

                        if (lockmgr(&share_map->lock, LK_EXCLUPGRADE, (void *)0, curproc))
                        {
                                if (share_map != map)
                                        vm_map_unlock_read(map);
                                goto RetryLookup;
                        }
#endif
                        vm_object_t vmo = entry->object.vm_object;

                        entry->object.vm_object = vmo->oshadow(&entry->offset, (vm_size_t)(entry->end() - entry->start()));

                        entry->flags &= ~VM_MAP_ENTRY_NEEDS_COPY;
#if DAMIR
                        lockmgr(&share_map->lock, LK_DOWNGRADE, (void *)0, curproc);
#endif
                }
                else
                {
                        /*
                         *	We're attempting to read a copy-on-write
                         *	page -- don't allow writes.
                         */

                        prot &= (~VM_PROT_WRITE);
                }
        }

        /*
         *	Create an object if necessary.
         */
        if (entry->object.vm_object == NULL)
        {
                // panic("STAP");
                if (/*lockmgr(&share_map->lock, LK_EXCLUPGRADE, (void *)0, curproc)*/ 0)
                {
                        if (share_map != map)
                                vm_map_unlock_read(map);
                        goto RetryLookup;
                }

                entry->object.vm_object = vm_object_allocate((vm_size_t)(entry->end() - entry->start()));
                entry->offset           = 0;
                /*lockmgr(&share_map->lock, LK_DOWNGRADE, (void *)0, curproc);*/
        }
        /*
         *	Return the object/offset from this entry.  If the entry
         *	was copy-on-write or empty, it has been fixed up.
         */

        *offset = (share_offset - entry->start()) + entry->offset;
        *object = entry->object.vm_object;

        /*
         *	Return whether this is the only map sharing this data.
         */

        if (!su)
        {
                simple_lock(&share_map->ref_lock);
                su = (share_map->ref_count == 1);
                simple_unlock(&share_map->ref_lock);
        }

        *out_prot   = prot;
        *single_use = su;

        return (KERN_SUCCESS);

#undef RETURN
}

#endif
/*
 *	Routine:	vm_map_simplify
 *
 *	Description:
 *		Attempt to simplify the map representation in
 *		the vicinity of the given starting address.
 *	Note:
 *		This routine is intended primarily to keep the
 *		kernel maps more compact -- they generally don't
 *		benefit from the "expand a map entry" technology
 *		at allocation time because the adjacent entry
 *		is often wired down.
 */
static void vm_map_simplify(vm_map_t map, vm_offset_t start)
{
        vm_map_entry_t this_entry;
        vm_map_entry_t prev_entry;

        vm_map_lock(map);
        if ((vm_map_lookup_entry(map, start, &this_entry)) && ((prev_entry = this_entry->prev()) != vm_map_to_entry(map)) &&

            (prev_entry->end() == start) &&

            !(prev_entry->flags & (VM_MAP_ENTRY_IS_SHARED | VM_MAP_ENTRY_IS_A_SUBMAP))

            &&

            !(this_entry->flags & (VM_MAP_ENTRY_IS_SHARED | VM_MAP_ENTRY_IS_A_SUBMAP))

            &&

            (prev_entry->inheritance == this_entry->inheritance) && (prev_entry->protection == this_entry->protection)
            && (prev_entry->max_protection == this_entry->max_protection) && (prev_entry->wired_count == this_entry->wired_count)
            && (prev_entry->user_wired_count == this_entry->user_wired_count) &&

            ((prev_entry->flags & VM_MAP_ENTRY_NEEDS_COPY) == (this_entry->flags & VM_MAP_ENTRY_NEEDS_COPY)) &&

            (prev_entry->object.vm_object == this_entry->object.vm_object)
            && ((prev_entry->offset + (prev_entry->end() - prev_entry->start())) == this_entry->offset)
            && (prev_entry->projected_on == 0) && (this_entry->projected_on == 0))
        {
                if (map->first_free == this_entry)
                {
                        map->first_free = prev_entry;
                }

                SAVE_HINT(map, prev_entry);
                vm_map_entry_unlink(map, this_entry);
                prev_entry->set_end(this_entry->end());

                panic("old shit below");
                // vm_object_deallocate(this_entry->object.vm_object);
                vm_map_entry_dispose(map, this_entry);
        }
        vm_map_unlock(map);
}

/*
 * Find sufficient space for `length' bytes in the given map, starting at
 * `start'.  The map must be locked.  Returns 0 on success, 1 on no space.
 */
int vm_map_findspace(vm_map_t map, vm_offset_t start, vm_size_t length, vm_offset_t *addr)
{
        vm_map_entry_t entry, next;
        vm_offset_t end;

        if (start < map->min_offset())
        {
                start = map->min_offset();
        }

        if (start > map->max_offset())
        {
                return (1);
        }

        /*
         * Look for the first possible address; if there's already
         * something at this address, we have to start after it.
         */
        if (start == map->min_offset())
        {
                if ((entry = map->first_free) != vm_map_to_entry(map))
                {
                        start = entry->end();
                }
        }
        else
        {
                vm_map_entry_t tmp;
                if (vm_map_lookup_entry(map, start, &tmp))
                {
                        start = tmp->end();
                }
                entry = tmp;
        }

        /*
         * Look through the rest of the map, trying to fit a new region in
         * the gap between existing regions, or after the very last region.
         */
        for (;; start = (entry = next)->end())
        {
                /*
                 * Find the end of the proposed new region.  Be sure we didn't
                 * go beyond the end of the map, or wrap around the address;
                 * if so, we lose.  Otherwise, if this is the last entry, or
                 * if the proposed new region fits before the next entry, we
                 * win.
                 */
                end = start + length;
                if (end > map->max_offset() || end < start)
                {
                        return (1);
                }

                next = entry->next();

                if (next == vm_map_to_entry(map) || next->start() >= end)
                {
                        break;
                }
        }
        SAVE_HINT(map, entry);
        *addr = start;
        return (0);
}
/*
 *	vm_map_insert:
 *
 *	Inserts the given whole VM object into the target
 *	map at the specified address range.  The object's
 *	size should match that of the address range.
 *
 *	Requires that the map be locked, and leaves it so.
 */
kern_return_t vm_map_insert(
    vm_map_t map, vm_object_t object, vm_offset_t offset, vm_offset_t start, vm_offset_t end, vm_map_entry_t a_new_entry)
{
        vm_map_entry_t new_entry;
        vm_map_entry_t prev_entry;
        vm_map_entry_t temp_entry;

        /*
         *	Check that the start and end points are not bogus.
         */

        if ((start < map->min_offset()) || (end > map->max_offset()) || (start >= end))
        {
                return (KERN_INVALID_ADDRESS);
        }

        /*
         *	Find the entry prior to the proposed
         *	starting address; if it's part of an
         *	existing entry, this range is bogus.
         */

        if (vm_map_lookup_entry(map, start, &temp_entry))
        {
                return (KERN_NO_SPACE);
        }

        prev_entry = temp_entry;

        /*
         *	Assert that the next entry doesn't overlap the
         *	end point.
         */

        if ((prev_entry->next() != vm_map_to_entry(map)) && (prev_entry->next()->start() < end))
                return (KERN_NO_SPACE);

                /*
                 *	See if we can avoid creating a new entry by
                 *	extending one of our neighbors.
                 */
#if 0
        if (object == NULL && 0) // Does not work
        {
                if ((prev_entry != vm_map_to_entry(map)) && (prev_entry->end() == start) && (map->is_main_map)
                    && !(prev_entry->flags & VM_MAP_ENTRY_IS_A_MAP) && !(prev_entry->flags & VM_MAP_ENTRY_IS_A_SUBMAP)
                    && (prev_entry->inheritance == VM_INHERIT_DEFAULT) && (prev_entry->protection == VM_PROT_DEFAULT)
                    && (prev_entry->max_protection == /*VM_PROT_DEFAULT*/ VM_PROT_ALL) && (prev_entry->wired_count == 0))
                {
                        if (prev_entry->object.vm_object->coalesce(NULL,
                                                                   prev_entry->offset,
                                                                   (vm_offset_t)0,
                                                                   (vm_size_t)(prev_entry->end() - prev_entry->start()),
                                                                   (vm_size_t)(end - prev_entry->end())))
                        {
                                /*
                                 *	Coalesced the two objects - can extend
                                 *	the previous map entry to include the
                                 *	new range.
                                 */
                                map->size += (end - prev_entry->end());
                                prev_entry->set_end(end);
                                return (KERN_SUCCESS);
                        }
                }
        }
#endif
        /*
         *	Create a new entry
         */

        if (a_new_entry != nullptr)
        {
                new_entry = a_new_entry;
        }
        else
        {
                new_entry = vm_map_entry_create(map);
        }

        new_entry->set_start(start);
        new_entry->set_end(end);

        // new_entry->is_a_map         = FALSE;
        // new_entry->is_sub_map       = FALSE;
        new_entry->object.vm_object = object;
        if (object != nullptr)
        {
                vm_object_ref(object);
        }

        new_entry->offset           = offset;

        // new_entry->copy_on_write = FALSE;
        // new_entry->needs_copy    = FALSE;

        new_entry->flags = 0;

        // Used sometimes when debugging
        new_entry->__pad = 0;

        if (a_new_entry)
        {
                new_entry->flags |= VM_MAP_ENTRY_NO_FREE;
        }

        if (map->is_main_map)
        {
                new_entry->inheritance    = VM_INHERIT_DEFAULT;
                new_entry->protection     = VM_PROT_DEFAULT;
                new_entry->max_protection = /*VM_PROT_DEFAULT*/ VM_PROT_ALL;
                new_entry->wired_count    = 0;
        }
        /*
         *	Insert the new entry into the list
         */

        vm_map_entry_link(map, prev_entry, new_entry);
        map->size += new_entry->end() - new_entry->start();

        /*
         *	Update the free space hint
         */

        if ((map->first_free == prev_entry) && (prev_entry->end() >= new_entry->start()))
        {
                map->first_free = new_entry;
        }

        return (KERN_SUCCESS);
}

/*
 * Returns TRUE if the range [start - end) is allocated in either
 * a single entry (single_entry == TRUE) or multiple contiguous
 * entries (single_entry == FALSE).
 *
 * start and end should be page aligned.
 */
static boolean_t vm_map_is_allocated(vm_map_t map, vm_offset_t start, vm_offset_t end, boolean_t single_entry)
{
        vm_map_entry_t mapent;
        vm_offset_t nend;

        vm_map_lock_read(map);

        /*
         * Start address not in any entry
         */
        if (!vm_map_lookup_entry(map, start, &mapent))
        {
                vm_map_unlock_read(map);
                return (FALSE);
        }
        /*
         * Find the maximum stretch of contiguously allocated space
         */
        nend = mapent->end();
        if (!single_entry)
        {
                mapent = mapent->next();
                while (mapent != vm_map_to_entry(map) && mapent->start() == nend)
                {
                        nend   = mapent->end();
                        mapent = mapent->next();
                }
        }

        vm_map_unlock_read(map);
        return (end <= nend);
}

/*
 *	vm_map_find finds an unallocated region in the target address
 *	map with the given length.  The search is defined to be
 *	first-fit from the specified address; the region found is
 *	returned in the same parameter.
 *
 */
kern_return_t vm_map_find(vm_map_t map,
                          vm_object_t object,
                          vm_offset_t offset,
                          vm_offset_t *addr, /* IN/OUT */
                          vm_size_t length,
                          boolean_t find_space,
                          vm_map_entry_t a_new_entry)
{
        vm_offset_t start;
        kern_return_t result;

        start = *addr;
        vm_map_lock(map);
        if (find_space)
        {
                if (vm_map_findspace(map, start, length, addr))
                {
                        vm_map_unlock(map);
                        return (KERN_NO_SPACE);
                }
                start = *addr;
        }
        result = vm_map_insert(map, object, offset, start, start + length, a_new_entry);
        vm_map_unlock(map);
        return (result);
}

/*
 *	vm_map_entry_unwire:	[ internal use only ]
 *
 *	Make the region specified by this entry pageable.
 *
 *	The map in question should be locked.
 *	[This is the reason for this routine's existence.]
 */
void vm_map_entry_unwire(vm_map_t a_map, vm_map_entry_t entry)
{
        // vm_fault_unwire(map, entry->start(), entry->end());
        /////// from vm_fault_unwire

        if (entry->object.vm_object == nullptr || entry->object.vm_object->m_page_map == nullptr)
        {
                return;
        }

        for (vm_offset_t off = 0;
             ((entry->offset + off) < entry->object.vm_object->m_size) && ((off + entry->start()) < entry->end());
             off += PAGE_SIZE)
        {
                pmap_change_wiring(a_map->pmap, off + entry->start(), FALSE);

                vm_page_t p = vm_page_map_lookup(entry->object.vm_object->m_page_map, entry->offset + off);
                if (p)
                {
                        p->unwire();
                }
        }

        ///////

        entry->wired_count = 0;
}

/*
 *	vm_map_check_protection:
 *
 *	Assert that the target map allows the specified
 *	privilege on the entire address region given.
 *	The entire region must be allocated.
 */
static boolean_t vm_map_check_protection(vm_map_t map, vm_offset_t start, vm_offset_t end, vm_prot_t protection)
{
        vm_map_entry_t entry;
        vm_map_entry_t tmp_entry;

        if (!vm_map_lookup_entry(map, start, &tmp_entry))
        {
                return (FALSE);
        }

        entry = tmp_entry;

        while (start < end)
        {
                if (entry == vm_map_to_entry(map))
                {
                        return (FALSE);
                }

                /*
                 *	No holes allowed!
                 */

                if (start < entry->start())
                {
                        return (FALSE);
                }

                /*
                 * Check protection associated with entry.
                 */

                if ((entry->protection & protection) != protection)
                {
                        return (FALSE);
                }

                /* go to next entry */

                start = entry->end();
                entry = entry->next();
        }
        return (TRUE);
}

/*
 *	vm_map_copy_entry:
 *
 *	Copies the contents of the source entry to the destination
 *	entry.  The entries *must* be aligned properly.
 */
static void vm_map_copy_entry(vm_map_t src_map, vm_map_t dst_map, vm_map_entry_t src_entry, vm_map_entry_t dst_entry)
{
        if (src_entry->flags & VM_MAP_ENTRY_IS_A_SUBMAP || dst_entry->flags & VM_MAP_ENTRY_IS_A_SUBMAP)
        {
                return;
        }

        if (dst_entry->object.vm_object != nullptr /*(dst_entry->object.vm_object->flags & VM_OBJ_INTERNAL) == 0*/)
        {
                panic("vm_map_copy_entry: copying over permanent data!\n");
        }

        /*
         *	If our destination map was wired down,
         *	unwire it now.
         */
        if (dst_entry->wired_count != 0)
        {
                vm_map_entry_unwire(dst_map, dst_entry);
        }

        /*
         *	If we're dealing with a sharing map, we
         *	must remove the destination pages from
         *	all maps (since we cannot know which maps
         *	this sharing map belongs in).
         */

        if (dst_map->is_main_map)
        {
                pmap_remove(dst_map->pmap, dst_entry->start(), dst_entry->end());
        }
        else
        {
                panic("VM_OBJECT");
                // dst_entry->object.vm_object->opmap_remove(dst_entry->offset,
                //                                          dst_entry->offset + (dst_entry->end() - dst_entry->start()));
        }

        if (src_entry->wired_count == 0)
        {
                boolean_t src_needs_copy;

                /*
                 *	If the source entry is marked needs_copy,
                 *	it is already write-protected.
                 */
                if (!(src_entry->flags & VM_MAP_ENTRY_NEEDS_COPY))
                {
                        boolean_t su;

                        /*
                         *	If the source entry has only one mapping,
                         *	we can just protect the virtual address
                         *	range.
                         */
                        if (!(su = src_map->is_main_map))
                        {
                                simple_lock(&src_map->ref_lock);
                                su = (src_map->ref_count == 1);
                                simple_unlock(&src_map->ref_lock);
                        }

                        if (su)
                        {
                                // pmap_protect(src_map->pmap,
                                //              src_entry->start(),
                                //              src_entry->end(),
                                //              src_entry->protection & ~VM_PROT_WRITE);
                        }
                        else
                        {
                                panic("VM_OBJECT");
                                // src_entry->object.vm_object->opmap_copy(
                                //     src_entry->offset, src_entry->offset + (src_entry->end() - src_entry->start()));
                        }
                }
                /*
                 *	Make a copy of the object.
                 */

#if DAMIR
                panic("VM_OBJECT");
                vm_object_t temp_object = dst_entry->object.vm_object;
                // src_entry->object.vm_object->ocopy(src_entry->offset,
                //                                   (vm_size_t)(src_entry->end() - src_entry->start()),
                //                                   &dst_entry->object.vm_object,
                //                                   &dst_entry->offset,
                //                                   &src_needs_copy);

                /*
                 *	If we didn't get a copy-object now, mark the
                 *	source map entry so that a shadow will be created
                 *	to hold its changed pages.
                 */
                if (src_needs_copy)
                {
                        src_entry->flags |= VM_MAP_ENTRY_NEEDS_COPY | VM_MAP_ENTRY_COW;
                }
#endif

                KASSERT(src_map != kernel_map);
                KASSERT(dst_map != kernel_map);

                KASSERT(dst_entry->object.vm_object == nullptr);

                if (src_entry->object.vm_object != nullptr)
                {
                        vm_object_ref((dst_entry->object.vm_object = src_entry->object.vm_object));

                        if ((src_entry->flags & VM_MAP_ENTRY_IS_SHARED) == 0)
                        {
                        
                                if (src_entry->protection & VM_PROT_WRITE)
                                {
                                        vm_object_lock(dst_entry->object.vm_object);
                                        vm_object_prot_change(dst_entry->object.vm_object, src_entry->protection & ~VM_PROT_WRITE);
                                        // pmap_protect(
                                        //     src_map->pmap, src_entry->start(), src_entry->end(), src_entry->protection & ~VM_PROT_WRITE);
                                        vm_object_unlock(dst_entry->object.vm_object);
                                }

                                src_entry->flags |= VM_MAP_ENTRY_NEEDS_COPY | VM_MAP_ENTRY_COW;

                                /*
                                        *	The destination always needs to have a shadow
                                        *	created.
                                        */
                                dst_entry->flags |= VM_MAP_ENTRY_NEEDS_COPY | VM_MAP_ENTRY_COW;
                        }

                /*
                 *	Mark the entries copy-on-write, so that write-enabling
                 *	the entry won't make copy-on-write pages writable.
                 */
                // src_entry->flags |= VM_MAP_ENTRY_COW;
                // dst_entry->flags |= VM_MAP_ENTRY_COW;
                /*
                 *	Get rid of the old object.
                 */
                // vm_object_deallocate(temp_object);

                        pmap_copy(dst_map->pmap, src_map->pmap, dst_entry->start, dst_entry->end - dst_entry->start, src_entry->start);
                }



        }
        else
        {
                /*
                 *	Of course, wired down pages can't be set copy-on-write.
                 *	Cause wired pages to be copied into the new
                 *	map by simulating faults (the new pages are
                 *	pageable)
                 */
                panic("don't");
                // vm_fault_copy_entry(dst_map, src_map, dst_entry, src_entry);
        }
}

/*
 *	vm_map_copy:
 *
 *	Perform a virtual memory copy from the source
 *	address map/range to the destination map/range.
 *
 *	If src_destroy or dst_alloc is requested,
 *	the source and destination regions should be
 *	disjoint, not only in the top-level map, but
 *	in the sharing maps as well.  [The best way
 *	to guarantee this is to use a new intermediate
 *	map to make copies.  This also reduces map
 *	fragmentation.]
 */
#define vm_map_set_recursive(map)                                                                                                  \
        {                                                                                                                          \
                /* simple_lock(&(map)->lk_interlock);*/                                                                            \
                /*(map)->lk_flags |= LK_CANRECURSE;*/                                                                              \
                /* simple_unlock(&(map)->lk_interlock); */                                                                         \
        }
#define vm_map_clear_recursive(map)                                                                                                \
        {                                                                                                                          \
                /*simple_lock(&(map)->lk_interlock);*/                                                                             \
                /*(map)->lk_flags &= ~LK_CANRECURSE;*/                                                                             \
                /*simple_unlock(&(map)->lk_interlock);*/                                                                           \
        }

kern_return_t vm_map_copy(vm_map_t dst_map,
                          vm_map_t src_map,
                          vm_offset_t dst_addr,
                          vm_size_t len,
                          vm_offset_t src_addr,
                          boolean_t dst_alloc,
                          boolean_t src_destroy)
{
        vm_map_entry_t src_entry;
        vm_map_entry_t dst_entry;
        vm_map_entry_t tmp_entry;
        vm_offset_t src_start;
        vm_offset_t src_end;
        vm_offset_t dst_start;
        vm_offset_t dst_end;
        vm_offset_t src_clip;
        vm_offset_t dst_clip;
        kern_return_t result;
        boolean_t old_src_destroy;

        /*
         *	XXX While we figure out why src_destroy screws up,
         *	we'll do it by explicitly vm_map_delete'ing at the end.
         */

        old_src_destroy = src_destroy;
        src_destroy     = FALSE;

        /*
         *	Compute start and end of region in both maps
         */

        src_start = src_addr;
        src_end   = src_start + len;
        dst_start = dst_addr;
        dst_end   = dst_start + len;

        /*
         *	Check that the region can exist in both source
         *	and destination.
         */

        if ((dst_end < dst_start) || (src_end < src_start))
        {
                return KERN_NO_SPACE;
        }

        /*
         *	Lock the maps in question -- we avoid deadlock
         *	by ordering lock acquisition by map value
         */

        if (src_map == dst_map)
        {
                vm_map_lock(src_map);
        }
        else if ((long)src_map < (long)dst_map)
        {
                vm_map_lock(src_map);
                vm_map_lock(dst_map);
        }
        else
        {
                vm_map_lock(dst_map);
                vm_map_lock(src_map);
        }

        result = KERN_SUCCESS;

        /*
         *	Check protections... source must be completely readable and
         *	destination must be completely writable.  [Note that if we're
         *	allocating the destination region, we don't have to worry
         *	about protection, but instead about whether the region
         *	exists.]
         */

        if (src_map->is_main_map && dst_map->is_main_map)
        {
                if (!vm_map_check_protection(src_map, src_start, src_end, VM_PROT_READ))
                {
                        result = KERN_PROTECTION_FAILURE;
                        goto Return;
                }

                if (dst_alloc)
                {
                        /* XXX Consider making this a vm_map_find instead */
                        if ((result = vm_map_insert(dst_map, NULL, (vm_offset_t)0, dst_start, dst_end, nullptr)) != KERN_SUCCESS)
                        {
                                goto Return;
                        }
                }
                else if (!vm_map_check_protection(dst_map, dst_start, dst_end, VM_PROT_WRITE))
                {
                        result = KERN_PROTECTION_FAILURE;
                        goto Return;
                }
        }

        /*
         *	Find the start entries and clip.
         *
         *	Note that checking protection asserts that the
         *	lookup cannot fail.
         *
         *	Also note that we wait to do the second lookup
         *	until we have done the first clip, as the clip
         *	may affect which entry we get!
         */

        vm_map_lookup_entry(src_map, src_addr, &tmp_entry);
        src_entry = tmp_entry;
        vm_map_clip_start(src_map, src_entry, src_start);

        vm_map_lookup_entry(dst_map, dst_addr, &tmp_entry);
        dst_entry = tmp_entry;
        vm_map_clip_start(dst_map, dst_entry, dst_start);

        /*
         *	If both source and destination entries are the same,
         *	retry the first lookup, as it may have changed.
         */

        if (src_entry == dst_entry)
        {
                vm_map_lookup_entry(src_map, src_addr, &tmp_entry);
                src_entry = tmp_entry;
        }

        /*
         *	If source and destination entries are still the same,
         *	a null copy is being performed.
         */

        if (src_entry == dst_entry)
        {
                goto Return;
        }

        /*
         *	Go through entries until we get to the end of the
         *	region.
         */

        while (src_start < src_end)
        {
                /*
                 *	Clip the entries to the endpoint of the entire region.
                 */

                vm_map_clip_end(src_map, src_entry, src_end);
                vm_map_clip_end(dst_map, dst_entry, dst_end);

                /*
                 *	Clip each entry to the endpoint of the other entry.
                 */

                src_clip = src_entry->start() + (dst_entry->end() - dst_entry->start());
                vm_map_clip_end(src_map, src_entry, src_clip);

                dst_clip = dst_entry->start() + (src_entry->end() - src_entry->start());
                vm_map_clip_end(dst_map, dst_entry, dst_clip);

                /*
                 *	Both entries now match in size and relative endpoints.
                 *
                 *	If both entries refer to a VM object, we can
                 *	deal with them now.
                 */

                if (!(src_entry->flags & VM_MAP_ENTRY_IS_A_MAP) && !(dst_entry->flags & VM_MAP_ENTRY_IS_A_MAP))
                {
                        vm_map_copy_entry(src_map, dst_map, src_entry, dst_entry);
                }
                else
                {
                        vm_map_t new_dst_map;
                        vm_offset_t new_dst_start;
                        vm_size_t new_size;
                        vm_map_t new_src_map;
                        vm_offset_t new_src_start;

                        /*
                         *	We have to follow at least one sharing map.
                         */

                        new_size = (dst_entry->end() - dst_entry->start());

                        if (src_entry->flags & VM_MAP_ENTRY_IS_A_MAP)
                        {
                                new_src_map   = src_entry->object.share_map;
                                new_src_start = src_entry->offset;
                        }
                        else
                        {
                                new_src_map   = src_map;
                                new_src_start = src_entry->start();
                                vm_map_set_recursive(&src_map->lock);
                        }

                        if (dst_entry->flags & VM_MAP_ENTRY_IS_A_MAP)
                        {
                                vm_offset_t new_dst_end;

                                new_dst_map   = dst_entry->object.share_map;
                                new_dst_start = dst_entry->offset;

                                /*
                                 *	Since the destination sharing entries
                                 *	will be merely deallocated, we can
                                 *	do that now, and replace the region
                                 *	with a null object.  [This prevents
                                 *	splitting the source map to match
                                 *	the form of the destination map.]
                                 *	Note that we can only do so if the
                                 *	source and destination do not overlap.
                                 */

                                new_dst_end = new_dst_start + new_size;

                                if (new_dst_map != new_src_map)
                                {
                                        vm_map_lock(new_dst_map);
                                        vm_map_delete(new_dst_map, new_dst_start, new_dst_end);
                                        vm_map_insert(new_dst_map, nullptr, 0, new_dst_start, new_dst_end, nullptr);
                                        vm_map_unlock(new_dst_map);
                                }
                        }
                        else
                        {
                                new_dst_map   = dst_map;
                                new_dst_start = dst_entry->start();
                                vm_map_set_recursive(&dst_map->lock);
                        }

                        /*
                         *	Recursively copy the sharing map.
                         */

                        vm_map_copy(new_dst_map, new_src_map, new_dst_start, new_size, new_src_start, FALSE, FALSE);

                        if (dst_map == new_dst_map)
                        {
                                vm_map_clear_recursive(&dst_map->lock);
                        }

                        if (src_map == new_src_map)
                        {
                                vm_map_clear_recursive(&src_map->lock);
                        }
                }

                /*
                 *	Update variables for next pass through the loop.
                 */

                src_start = src_entry->end();
                src_entry = src_entry->next();
                dst_start = dst_entry->end();
                dst_entry = dst_entry->next();

                /*
                 *	If the source is to be destroyed, here is the
                 *	place to do it.
                 */

                if (src_destroy && src_map->is_main_map && dst_map->is_main_map)
                {
                        vm_map_entry_delete(src_map, src_entry->prev());
                }
        }

        /*
         *	Update the physical maps as appropriate
         */

        if (src_map->is_main_map && dst_map->is_main_map)
        {
                if (src_destroy)
                {
                        pmap_remove(src_map->pmap, src_addr, src_addr + len);
                }
        }

        /*
         *	Unlock the maps
         */

Return:;
        if (old_src_destroy)
        {
                vm_map_delete(src_map, src_addr, src_addr + len);
        }

        vm_map_unlock(src_map);

        if (src_map != dst_map)
        {
                vm_map_unlock(dst_map);
        }
        return result;
}
//
void vm_map_lookup_done(vm_map_t map, vm_map_entry_t entry)
{
        /*
         *	If this entry references a map, unlock it first.
         */

        if (entry->flags & VM_MAP_ENTRY_IS_A_MAP)
        {
                vm_map_unlock_read(entry->object.share_map);
        }
        /*
         *	Unlock the main-level map
         */

        vm_map_unlock_read(map);
}

/*
 * vm_map_fork:
 * Create a new map based on an existing pmap.
 * The new map is based on the old map, according to the inheritance
 * values on the regions in that map.
 *
 * The source map must not be locked.
 */
vm_map_t vm_map_fork(vm_map_t old_map)
{
        vm_map_entry_t old_entry;
        vm_map_entry_t new_entry;
        pmap_t new_pmap = pmap_create(0);

        vm_map_t new_map = vm_map_create(new_pmap, old_map->min_offset(), old_map->max_offset(), old_map->hdr.entries_pageable);
        vm_map_lock(old_map);

#if DAMIR
        vm2 = vmspace_alloc(old_map->min_offset, old_map->max_offset, old_map->entries_pageable);
        bcopy(&vm1->vm_startcopy, &vm2->vm_startcopy, (caddr_t)(vm1 + 1) - (caddr_t)&vm1->vm_startcopy);
        new_pmap = &vm2->vm_pmap; /* XXX */
        new_map  = &vm2->vm_map;  /* XXX */
#endif

        old_entry = vm_map_to_entry(old_map)->next();

        while (old_entry != vm_map_to_entry(old_map))
        {
                if (old_entry->flags & VM_MAP_ENTRY_IS_A_SUBMAP)
                {
                        panic("vm_map_fork: encountered a submap");
                }

                switch (old_entry->inheritance)
                {
                case VM_INHERIT_NONE:
                        break;

                case VM_INHERIT_SHARE:
                        panic("Should not run yet");
                        /*
                         *	If we don't already have a sharing map:
                         */

                        if (!(old_entry->flags & VM_MAP_ENTRY_IS_A_MAP))
                        {
                                vm_map_t new_share_map;
                                vm_map_entry_t new_share_entry;

                                /*
                                 *	Create a new sharing map
                                 */

                                new_share_map              = vm_map_create(NULL, old_entry->start(), old_entry->end(), TRUE);
                                new_share_map->is_main_map = FALSE;

                                /*
                                 *	Create the only sharing entry from the
                                 *	old task map entry.
                                 */

                                new_share_entry              = vm_map_entry_create(new_share_map);
                                *new_share_entry             = *old_entry;
                                new_share_entry->wired_count = 0;

                                /*
                                 *	Insert the entry into the new sharing
                                 *	map
                                 */

                                vm_map_entry_link(new_share_map, vm_map_to_entry(new_share_map)->prev(), new_share_entry);

                                /*
                                 *	Fix up the task map entry to refer
                                 *	to the sharing map now.
                                 */

                                old_entry->flags |= VM_MAP_ENTRY_IS_A_MAP;
                                old_entry->object.share_map = new_share_map;
                                old_entry->offset           = old_entry->start();
                        }

                        /*
                         *	Clone the entry, referencing the sharing map.
                         */

                        new_entry              = vm_map_entry_create(new_map);
                        *new_entry             = *old_entry;
                        new_entry->wired_count = 0;
                        vm_map_reference(new_entry->object.share_map);

                        /*
                         *	Insert the entry into the new map -- we
                         *	know we're inserting at the end of the new
                         *	map.
                         */

                        vm_map_entry_link(new_map, vm_map_to_entry(new_map)->prev(), new_entry);

                        /*
                         *	Update the physical map
                         */
                        pmap_copy(
                            new_map->pmap, old_map->pmap, new_entry->start, (old_entry->end - old_entry->start), old_entry->start);
                        break;

                case VM_INHERIT_COPY:
                        /*
                         *	Clone the entry and link into the map.
                         */

                        new_entry                   = vm_map_entry_create(new_map);
                        *new_entry                  = *old_entry;
                        new_entry->wired_count      = 0;
                        new_entry->object.vm_object = NULL;
                        new_entry->flags &= ~VM_MAP_ENTRY_IS_A_MAP;
                        vm_map_entry_link(new_map, vm_map_to_entry(new_map)->prev(), new_entry);
                        if (old_entry->flags & VM_MAP_ENTRY_IS_A_MAP)
                        {
                                panic("should not run yet");
                                int check;

                                check = vm_map_copy(new_map,
                                                    old_entry->object.share_map,
                                                    new_entry->start(),
                                                    (vm_size_t)(new_entry->end() - new_entry->start()),
                                                    old_entry->offset,
                                                    FALSE,
                                                    FALSE);
                                if (check != KERN_SUCCESS)
                                        log_warn("vm_map_fork: copy in share_map region failed");
                        }
                        else
                        {
                                vm_map_copy_entry(old_map, new_map, old_entry, new_entry);
                        }
                        break;
                default:
                        panic("Unknown inheritance mode");
                }
                old_entry = old_entry->next();
        }

        new_map->size = old_map->size;
        vm_map_unlock(old_map);

        return new_map;
}
