/*
 * Mach Operating System
 * Copyright (c) 1991,1990,1989,1988 Carnegie Mellon University
 * All Rights Reserved.
 *
 * Permission to use, copy, modify and distribute this software and its
 * documentation is hereby granted, provided that both the copyright
 * notice and this permission notice appear in all copies of the
 * software, derivative works or modified versions, and any portions
 * thereof, and that both notices appear in supporting documentation.
 *
 * CARNEGIE MELLON ALLOWS FREE USE OF THIS SOFTWARE IN ITS "AS IS"
 * CONDITION.  CARNEGIE MELLON DISCLAIMS ANY LIABILITY OF ANY KIND FOR
 * ANY DAMAGES WHATSOEVER RESULTING FROM THE USE OF THIS SOFTWARE.
 *
 * Carnegie Mellon requests users of this software to return to
 *
 *  Software Distribution Coordinator  or  Software.Distribution@CS.CMU.EDU
 *  School of Computer Science
 *  Carnegie Mellon University
 *  Pittsburgh PA 15213-3890
 *
 * any improvements or extensions that they make and grant Carnegie Mellon
 * the rights to redistribute these changes.
 */
/*
 *	Mach kernel startup.
 */

#include "../../arch/amd64/src/proc_reg.h"
#include "mach_factor.h"
#include <kernel/mach_clock.h>
#include <kernel/machine.h>
#include <kernel/processor.h>
#include <kernel/sched_prim.h>
#include <kernel/timer.h>
#include <kernel/vm/vm_kern.h>
#include <kernel/vm/vm_map.h>
#include <kernel/vm/vm_param.h>
#include <kernel/zalloc.h>
#include <kernel/thread_data.h>
#include <mach/machine/model_dep.h>
#include <mach/machine/pmap.h>
#include <mach/machine/spl.h>

#include "thread_swap.h"
#include "vm/vm_pageout.h"

extern void vm_mem_init();
extern "C" void machine_init();

extern void bootstrap_create();
extern void device_service_create();
extern "C" void do_initcalls(int lvl);

void cpu_launch_first_thread(thread_t th); /* forward */
void start_kernel_threads();               /* forward */

extern void start_other_cpus();
extern "C" void startrtclock(); // modeldep

/*
 *	Running in virtual memory, on the interrupt stack.
 *	Does not return.  Dispatches initial thread.
 *
 *	Assumes that master_cpu is set.
 */
extern "C" void setup_main(struct modeldep_bootdata *mbd)
{
        thread_t startup_thread;

#ifdef DAMIR
        panic_init();
        printf_init();
#endif
        // sched_init(); done in initcall
        do_initcalls(INITCALL_POSTCORE);
        vm_offset_t start, end;

        /*
         *	Initializes resident memory structures.
         *	From here on, all physical memory is accounted for,
         *	and we use only virtual addresses.
         */
        // vm_page_bootstrap(&start, &end);

        /*
         *	Initialize other VM packages
         */

        zone_bootstrap();
        // vm_object_bootstrap();
        vm_map_init();
        kmem_init(start, end);
        pmap_init(mbd);
        vm_pageout_init();
        zone_init();
        // kalloc_init();
        // vm_fault_init();
        // vm_page_module_init();

#ifdef DAMIR
        ipc_bootstrap();
        vm_mem_init();
        ipc_init();
#endif
        /*
         * As soon as the virtual memory system is up, we record
         * that this CPU is using the kernel pmap.
         */
        PMAP_ACTIVATE_KERNEL(master_cpu);

        init_timers();
        init_timeout();

#if XPR_DEBUG && DAMIR
        xprbootstrap();
#endif

#ifdef DAMIR
        timestamp_init();
#endif
        mapable_time_init();

        machine_init();

        machine_info.max_cpus    = NCPUS;
        machine_info.memory_size = mbd->phys_last_addr - mbd->phys_first_addr; /* XXX mem_size */
        machine_info.avail_cpus  = 0;
        // machine_info.major_version = KERNEL_MAJOR_VERSION;
        // machine_info.minor_version = KERNEL_MINOR_VERSION;

        /*
         *	Initialize the IPC, task, and thread subsystems.
         */
        task_init();
        thread_init();
        swapper_init();
#if MACH_HOST
        pset_sys_init();
#endif

        /*
         *	Kick off the time-out driven routines by calling
         *	them the first time.
         */
        recompute_priorities(NULL);
        compute_mach_factor();

        /*
         *	Create a kernel thread to start the other kernel
         *	threads.  Thread_resume (from kernel_thread) calls
         *	thread_setrun, which may look at current thread;
         *	we must avoid this, since there is no current thread.
         */

        /*
         * Create the thread, and point it at the routine.
         */
        thread_create(kernel_task, &startup_thread);
        thread_start(startup_thread, start_kernel_threads);

        /*
         * Give it a kernel stack.
         */
        thread_doswapin(startup_thread);

        /*
         * Pretend it is already running, and resume it.
         * Since it looks as if it is running, thread_resume
         * will not try to put it on the run queues.
         *
         * We can do all of this without locking, because nothing
         * else is running yet.
         */
        startup_thread->state |= TH_RUN;
        thread_resume(startup_thread);

        /*
         * Start the thread.
         */
        cpu_launch_first_thread(startup_thread);
        /*NOTREACHED*/
        UNREACHABLE();
}

/*
 * Now running in a thread.  Create the rest of the kernel threads
 * and the bootstrap task.
 */

void start_kernel_threads()
{
        log_set_quiet(TRUE); // does not work this early, FIX this.

        int i;

        /*
         *	Create the idle threads and the other
         *	service threads.
         */
        for (i = 0; i < NCPUS; i++)
        {
                if (machine_slot[i].is_cpu)
                {
                        thread_t th;

                        thread_create(kernel_task, &th);
                        thread_bind(th, cpu_to_processor(i));
                        thread_start(th, idle_thread);
                        thread_doswapin(th);
                        thread_resume(th);
                }
        }

        kernel_thread(kernel_task, reaper_thread, NULL);
        kernel_thread(kernel_task, swapin_thread, NULL);
        kernel_thread(kernel_task, sched_thread, NULL);

        /*
         *	Create the shutdown thread.
         */
        kernel_thread(kernel_task, action_thread, (char *)0);

        /*
         *	Allow other CPUs to run.
         */
        /*start_other_cpus();*/
#ifdef DAMIR
        /*
         *	Create the device service.
         */
        device_service_create();
#endif

        /*
         *	Start the user bootstrap.
         */
        {
                task_t bootstrap_task;
                thread_t bootstrap_thread;

                /*
                 * Create the bootstrap task.
                 */

                task_create(TASK_NULL, TASK_CREATE_NEW_VMMAP, &bootstrap_task);
                thread_create(bootstrap_task, &bootstrap_thread);

                /*
                 * Start the bootstrap thread.
                 */

                thread_start(bootstrap_thread, locore_init_user);
                thread_resume(bootstrap_thread);
        }

#if XPR_DEBUG && DAMIR
        xprinit(); /* XXX */
#endif

        /*
         *	Become the pageout daemon.
         */
        spl0();
        vm_pageout();
        /*NOTREACHED*/
}

void slave_main()
{
        cpu_launch_first_thread(THREAD_NULL);
}

/*
 *	Start up the first thread on a CPU.
 *	First thread is specified for the master CPU.
 */
void cpu_launch_first_thread(thread_t th)
{
        int mycpu;

        mycpu = cpu_number();

        cpu_up(mycpu);

        start_timer(&kernel_timer[mycpu]);

        /*
         * Block all interrupts for choose_thread.
         */
        splhigh();

        if (th == THREAD_NULL)
        {
                th = choose_thread(cpu_to_processor(mycpu));
        }

        if (th == THREAD_NULL)
        {
                panic("cpu_launch_first_thread");
        }

        startrtclock(); /* needs an active thread */
        PMAP_ACTIVATE_KERNEL(mycpu);

        active_threads[mycpu] = th;
        active_stacks[mycpu]  = th->kernel_stack;
        thread_lock(th);
        th->state &= ~TH_UNINT;
        thread_unlock(th);
        timer_switch(&th->system_timer);

        PMAP_ACTIVATE_USER(vm_map_pmap(th->task->map), th, mycpu);

        /*printf("Initialization end reached!\n");*/

        load_context(th);
        /*NOTREACHED*/
}
