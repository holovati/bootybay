/*
 * Mach Operating System
 * Copyright (c) 1992,1991,1990,1989,1988,1987 Carnegie Mellon University
 * All Rights Reserved.
 *
 * Permission to use, copy, modify and distribute this software and its
 * documentation is hereby granted, provided that both the copyright
 * notice and this permission notice appear in all copies of the
 * software, derivative works or modified versions, and any portions
 * thereof, and that both notices appear in supporting documentation.
 *
 * CARNEGIE MELLON ALLOWS FREE USE OF THIS SOFTWARE IN ITS "AS IS"
 * CONDITION.  CARNEGIE MELLON DISCLAIMS ANY LIABILITY OF ANY KIND FOR
 * ANY DAMAGES WHATSOEVER RESULTING FROM THE USE OF THIS SOFTWARE.
 *
 * Carnegie Mellon requests users of this software to return to
 *
 *  Software Distribution Coordinator  or  Software.Distribution@CS.CMU.EDU
 *  School of Computer Science
 *  Carnegie Mellon University
 *  Pittsburgh PA 15213-3890
 *
 * any improvements or extensions that they make and grant Carnegie Mellon
 * the rights to redistribute these changes.
 */
/*
 *	File:	sched_prim.h
 *	Author:	David Golub
 *
 *	Scheduling primitive definitions file
 *
 */

#ifndef _KERN_SCHED_PRIM_H_
#define _KERN_SCHED_PRIM_H_
#include <kernel/kern_types.h> /* for thread_t */
#include <kernel/lock.h>
#include <limits.h>
#include <mach/machine/locore.h>
/*
 *	Possible results of assert_wait - returned in
 *	current_thread()->wait_result.
 */
typedef enum thread_wait_result
{
        THREAD_AWAKENED,    /* normal wakeup */
        THREAD_TIMED_OUT,   /* timeout expired */
        THREAD_INTERRUPTED, /* interrupted by clear_wait */
        THREAD_RESTART,     /* restart operation entirely */
        THREAD_MSG_PENDING,
        THREAD_MSG_ACK,
} thread_wait_result_t;

typedef void *event_t; /* wait event */

/*
 *	Exported interface to sched_prim.c.
 */

void assert_wait(event_t event, boolean_t interruptible);
void clear_wait(thread_t thread, thread_wait_result_t result, boolean_t interrupt_only);
void thread_sleep(event_t event, simple_lock_t lock, boolean_t interruptible);
void thread_wakeup(); /* for function pointers */
kern_return_t thread_wakeup_prim(event_t event, int wakeup_count, thread_wait_result_t result, boolean_t interrupt_only);
boolean_t thread_invoke(thread_t old_thread, continuation_t continuation, thread_t new_thread);
// void thread_block(continuation_t continuation);
void thread_run(continuation_t continuation, thread_t new_thread);
void thread_set_timeout(int t);
void thread_setrun(thread_t thread, boolean_t may_preempt);
void thread_dispatch(thread_t thread);
void thread_continue(thread_t old_thread);
// void thread_go(thread_t thread);
// void thread_will_wait(thread_t thread);
/// void thread_will_wait_with_timeout(thread_t thread, mach_msg_timeout_t msecs);
// boolean_t thread_handoff(thread_t old_thread, continuation_t continuation, thread_t new_thread);
int recompute_priorities(void *param);
/*
 *	Routines defined as macros
 */

#define thread_wakeup(x)                    thread_wakeup_prim((x), INT_MAX, THREAD_AWAKENED, false)
#define thread_wakeup_with_result(x, z)     thread_wakeup_prim((x), INT_MAX, (z), false)
#define thread_wakeup_one(x)                thread_wakeup_prim((x), 1, THREAD_AWAKENED, false)
#define thread_wakeup_interruptible_only(x) thread_wakeup_prim((x), INT_MAX, THREAD_AWAKENED, true)

/*
 *	Machine-dependent code must define these functions.
 */
#ifdef __cplusplus
extern "C" {
#endif
void thread_bootstrap_return(void);
thread_wait_result_t thread_block(continuation_t continuation);
#ifdef __cplusplus
}
#endif
thread_t switch_context(thread_t old_thread, continuation_t continuation, thread_t new_thread);
void stack_handoff(thread_t old_thread, thread_t new_thread);

/*
 *	These functions are either defined in kern/thread.c
 *	via machine-dependent stack_attach and stack_detach functions,
 *	or are defined directly by machine-dependent code.
 */

void stack_alloc(thread_t thread, void (*resume)(thread_t));
boolean_t stack_alloc_try(thread_t thread, void (*resume)(thread_t));
void stack_free(thread_t thread);

vm_offset_t stack_detach(thread_t thread);

/*
 *	choose_pset_thread:  choose a thread from processor_set runq or
 *		set processor idle and choose its idle thread.
 *
 *	Caller must be at splsched and have a lock on the runq.  This
 *	lock is released by this routine.  myprocessor is always the current
 *	processor, and pset must be its processor set.
 *	This routine chooses and removes a thread from the runq if there
 *	is one (and returns it), else it sets the processor idle and
 *	returns its idle thread.
 */

thread_t choose_pset_thread(processor_t myprocessor, processor_set_t pset);

/*
 *	Convert a timeout in milliseconds (mach_msg_timeout_t)
 *	to a timeout in ticks (for use by set_timeout).
 *	This conversion rounds UP so that small timeouts
 *	at least wait for one tick instead of not waiting at all.
 */

/*
 *	compute_my_priority:
 *
 *	Version of compute priority for current thread or thread
 *	being manipulated by scheduler (going on or off a runq).
 *	Only used for priority updates.  Policy or priority changes
 *	must call compute_priority above.  Caller must have thread
 *	locked and know it is timesharing and not depressed.
 */

void compute_my_priority(thread_t thread);

void idle_thread();

void sched_thread();

#define convert_ipc_timeout_to_ticks(millis) (((millis)*hz + 999) / 1000)

#endif
