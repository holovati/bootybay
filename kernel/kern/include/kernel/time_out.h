#pragma once
/*
 * Mach Operating System
 * Copyright (c) 1991,1990,1989,1988,1987 Carnegie Mellon University
 * All Rights Reserved.
 *
 * Permission to use, copy, modify and distribute this software and its
 * documentation is hereby granted, provided that both the copyright
 * notice and this permission notice appear in all copies of the
 * software, derivative works or modified versions, and any portions
 * thereof, and that both notices appear in supporting documentation.
 *
 * CARNEGIE MELLON ALLOWS FREE USE OF THIS SOFTWARE IN ITS "AS IS"
 * CONDITION.  CARNEGIE MELLON DISCLAIMS ANY LIABILITY OF ANY KIND FOR
 * ANY DAMAGES WHATSOEVER RESULTING FROM THE USE OF THIS SOFTWARE.
 *
 * Carnegie Mellon requests users of this software to return to
 *
 *  Software Distribution Coordinator  or  Software.Distribution@CS.CMU.EDU
 *  School of Computer Science
 *  Carnegie Mellon University
 *  Pittsburgh PA 15213-3890
 *
 * any improvements or extensions that they make and grant Carnegie Mellon
 * the rights to redistribute these changes.
 */

/*
 * Mach time-out and time-of-day facility.
 */

#include <etl/queue.h>
#include <kernel/lock.h>

/*
 *	Timers in kernel:
 */
extern natural_t elapsed_ticks; /* number of ticks elapsed since bootup */
extern int hz;                  /* number of ticks per second */
extern int tick;                /* number of usec per tick */

/*
 *	Time-out element.
 */
struct timer_elt
{
        queue_chain_t chain;          /* chain in order of expiration */
        kern_return_t (*fcn)(void *); /* function to call */
        void *param;                  /* with this parameter */
        natural_t ticks;              /* expiration time, in ticks */
        integer_t set;                /* unset | set | allocated */
};
#define TELT_UNSET 0 /* timer not set */
#define TELT_SET   1 /* timer set */
#define TELT_ALLOC 2 /* timer allocated from pool */

typedef struct timer_elt timer_elt_data_t;
typedef struct timer_elt *timer_elt_t;

#ifdef __cplusplus
extern "C" {
#endif

/* for 'private' timer elements */
void set_timeout(timer_elt_t telt, natural_t interval);
boolean_t reset_timeout(timer_elt_t telt);

/* for public timer elements */
void timeout(kern_return_t (*fcn)(void *param), void *param, int interval);
boolean_t untimeout(kern_return_t (*fcn)(void *), void *param);

#ifdef __cplusplus
}
#endif

#define set_timeout_setup(telt, fcn, param, interval)                                                                              \
        ((telt)->fcn = (fcn), (telt)->param = (param), (telt)->private = TRUE, set_timeout((telt), (interval)))

#define reset_timeout_check(t)                                                                                                     \
        MACRO_BEGIN                                                                                                                \
        if ((t)->set)                                                                                                              \
                reset_timeout((t));                                                                                                \
        MACRO_END
