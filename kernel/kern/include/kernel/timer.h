/*
 * Mach Operating System
 * Copyright (c) 1991,1990,1989,1988,1987 Carnegie Mellon University
 * All Rights Reserved.
 *
 * Permission to use, copy, modify and distribute this software and its
 * documentation is hereby granted, provided that both the copyright
 * notice and this permission notice appear in all copies of the
 * software, derivative works or modified versions, and any portions
 * thereof, and that both notices appear in supporting documentation.
 *
 * CARNEGIE MELLON ALLOWS FREE USE OF THIS SOFTWARE IN ITS "AS IS"
 * CONDITION.  CARNEGIE MELLON DISCLAIMS ANY LIABILITY OF ANY KIND FOR
 * ANY DAMAGES WHATSOEVER RESULTING FROM THE USE OF THIS SOFTWARE.
 *
 * Carnegie Mellon requests users of this software to return to
 *
 *  Software Distribution Coordinator  or  Software.Distribution@CS.CMU.EDU
 *  School of Computer Science
 *  Carnegie Mellon University
 *  Pittsburgh PA 15213-3890
 *
 * any improvements or extensions that they make and grant Carnegie Mellon
 * the rights to redistribute these changes.
 */

#ifndef _KERN_TIMER_H_
#define _KERN_TIMER_H_

// clang-format on
/*
 *	Statistical timer definitions - use microseconds in timer, seconds
 *	in high unit field.  No adjustment needed to convert to time_value_t
 *	as a result.  Service timers once an hour.
 */

#define TIMER_RATE      1000000
#define TIMER_HIGH_UNIT TIMER_RATE
#undef TIMER_ADJUST

/*
 *	Definitions for accurate timers.  high_bits_check is a copy of
 *	high_bits that allows reader to verify that values read are ok.
 */

struct mach_timer
{
        unsigned low_bits;
        unsigned high_bits;
        unsigned high_bits_check;
        unsigned tstamp;
};

typedef struct mach_timer mach_timer_data_t;
typedef struct mach_timer *mach_timer_t;

/*
 *	Mask to check if low_bits is in danger of overflowing
 */

#define TIMER_LOW_FULL 0x80000000U

/*
 *	Kernel timers and current timer array.  [Exported]
 */

extern mach_timer_t current_timer[NCPUS];
extern mach_timer_data_t kernel_timer[NCPUS];

/*
 *	save structure for timer readings.  This is used to save timer
 *	readings for elapsed time computations.
 */

struct mach_timer_save
{
        unsigned low;
        unsigned high;
};

typedef struct mach_timer_save mach_timer_save_data_t, *mach_timer_save_t;

/*
 *	Exported kernel interface to timers
 */

#define start_timer(timer)
#define timer_switch(timer)

extern void timer_read();

/*
 *	init_timers initializes all non-thread timers and puts the
 *	service routine on the callout queue.  All timers must be
 *	serviced by the callout routine once an hour.
 */
void init_timers();

unsigned timer_delta(mach_timer_t timer, mach_timer_save_t save);

void timer_init(mach_timer_t this_timer);

void timer_normalize(mach_timer_t timer);

/*
 *	Macro to bump timer values.
 */
#define timer_bump(timer, usec)                                                                                                    \
        MACRO_BEGIN(timer)->low_bits += usec;                                                                                      \
        if ((timer)->low_bits & TIMER_LOW_FULL)                                                                                    \
        {                                                                                                                          \
                timer_normalize(timer);                                                                                            \
        }                                                                                                                          \
        MACRO_END

/*
 *	TIMER_DELTA finds the difference between a timer and a saved value,
 *	and updates the saved value.  Look at high_bits check field after
 *	reading low because that's the first written by a normalize
 *	operation; this isn't necessary for current usage because
 *	this macro is only used when the timer can't be normalized:
 *	thread is not running, or running thread calls it on itself at
 *	splsched().
 */

#define TIMER_DELTA(timer, save, result)                                                                                           \
        MACRO_BEGIN                                                                                                                \
        unsigned temp;                                                                                                             \
                                                                                                                                   \
        temp = (timer).low_bits;                                                                                                   \
        if ((save).high != (timer).high_bits_check)                                                                                \
        {                                                                                                                          \
                result += timer_delta(&(timer), &(save));                                                                          \
        }                                                                                                                          \
        else                                                                                                                       \
        {                                                                                                                          \
                result += temp - (save).low;                                                                                       \
                (save).low = temp;                                                                                                 \
        }                                                                                                                          \
        MACRO_END

#endif