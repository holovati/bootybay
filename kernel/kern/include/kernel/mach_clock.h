#pragma once

/*
 *	Handle clock interrupts.
 *
 *	The clock interrupt is assumed to be called at a (more or less)
 *	constant rate.  The rate must be identical on all CPUS (XXX - fix).
 *
 *	Usec is the number of microseconds that have elapsed since the
 *	last clock tick.  It may be constant or computed, depending on
 *	the accuracy of the hardware clock.
 *
 */
void clock_interrupt(int usec,           /* microseconds per tick */
                     boolean_t usermode, /* executing user code */
                     boolean_t basepri /* at base priority */);

void softclock();

void init_timeout();

void mapable_time_init();

#ifdef __cplusplus
extern "C"
{
#endif

        void mach_get_timespec(struct timespec *a_timespec);

#ifdef __cplusplus
}
#endif