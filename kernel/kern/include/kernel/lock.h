#pragma once
/*
 * Mach Operating System
 * Copyright (c) 1993-1987 Carnegie Mellon University
 * All Rights Reserved.
 *
 * Permission to use, copy, modify and distribute this software and its
 * documentation is hereby granted, provided that both the copyright
 * notice and this permission notice appear in all copies of the
 * software, derivative works or modified versions, and any portions
 * thereof, and that both notices appear in supporting documentation.
 *
 * CARNEGIE MELLON ALLOWS FREE USE OF THIS SOFTWARE IN ITS "AS IS"
 * CONDITION.  CARNEGIE MELLON DISCLAIMS ANY LIABILITY OF ANY KIND FOR
 * ANY DAMAGES WHATSOEVER RESULTING FROM THE USE OF THIS SOFTWARE.
 *
 * Carnegie Mellon requests users of this software to return to
 *
 *  Software Distribution Coordinator  or  Software.Distribution@CS.CMU.EDU
 *  School of Computer Science
 *  Carnegie Mellon University
 *  Pittsburgh PA 15213-3890
 *
 * any improvements or extensions that they make and grant Carnegie Mellon
 * the rights to redistribute these changes.
 */
/*
 *	File:	kern/lock.h
 *	Author:	Avadis Tevanian, Jr., Michael Wayne Young
 *	Date:	1985
 *
 *	Locking primitives definitions
 */

/*
 *	A simple spin lock.
 */
struct slock
{
        volatile u32 lock_data; /* in general 1 bit is sufficient */
        int pad;
};

typedef struct slock simple_lock_data_t;
typedef struct slock *simple_lock_t;

/*
 *	The general lock structure.  Provides for multiple readers,
 *	upgrading from read to write, and sleeping until the lock
 *	can be gained.
 *
 *	On some architectures, assembly language code in the 'inline'
 *	program fiddles the lock structures.  It must be changed in
 *	concert with the structure layout.
 *
 *	Only the "interlock" field is used for hardware exclusion;
 *	other fields are modified with normal instructions after
 *	acquiring the interlock bit.
 */
struct lock_data
{
        struct thread *thread;                    /* Thread that has lock, if recursive locking allowed */
        natural_t read_count    : 16,             /* Number of accepted readers */
                want_upgrade    : 1,              /* Read-to-write upgrade waiting */
                want_write      : 1,              /* Writer is waiting, or locked for write */
                waiting         : 1,              /* Someone is sleeping on lock */
                can_sleep       : 1,              /* Can attempts to lock go to sleep? */
                recursion_depth : 12,             /* Depth of recursion */
                : ((sizeof(natural_t) * 8) - 32); /* Free bits*/
        simple_lock_data_t interlock;
        /* Hardware interlock field.
           Last in the structure so that
           field offsets are the same whether
           or not it is present. */
};

typedef struct lock_data lock_data_t, *lock_t;

/* Sleep locks must work even if no multiprocessing */
#ifdef __cplusplus
extern "C"
{
#endif

void lock_init(lock_t, boolean_t);
void lock_sleepable(lock_t, boolean_t);
void lock_write(lock_t);
void lock_read(lock_t);
void lock_done(lock_t);
boolean_t lock_read_to_write(lock_t);
void lock_write_to_read(lock_t);
boolean_t lock_try_write(lock_t);
boolean_t lock_try_read(lock_t);
boolean_t lock_try_read_to_write(lock_t);

#define lock_read_done(l)  lock_done(l)
#define lock_write_done(l) lock_done(l)

void lock_set_recursive(lock_t);
void lock_clear_recursive(lock_t);

#ifdef __cplusplus
}
#endif

#include <mach/machine/lock.h>

#ifdef __cplusplus
#define LOCK_WRITE_SCOPE(l)                                                                                                        \
        lock_write(l);                                                                                                             \
        defer(lock_write_done(l))

#define LOCK_READ_SCOPE(l)                                                                                                         \
        lock_read(l);                                                                                                              \
        defer(lock_read_done(l))
#endif
