#pragma once

#ifndef KASSERT
#define KASSERT(cond)                                                          \
  do {                                                                         \
    if (!(cond)) {                                                             \
      panic("Assertion failed: %s", #cond);                                    \
    }                                                                          \
  } while (0)
#endif
