#pragma once

#include <arch/env.h>

extern char __percpu_start[];
extern char __percpu_end[];

#define __PER_CPU_ATTRIB(section_name) __attribute__((unused, section(".percpu_" #section_name)))

#ifdef __cplusplus
#define PER_CPU_VAR(nm)                                                                                                            \
        __percpu_##nm##_data __PER_CPU_ATTRIB(data_##nm);                                                                          \
        decltype(__percpu_##nm##_data) __percpu_##nm##_data_boot __PER_CPU_ATTRIB(data_boot_##nm)
#else
#define PER_CPU_VAR(nm)                                                                                                            \
        __percpu_##nm##_data __PER_CPU_ATTRIB(data_##nm);                                                                          \
        typeof(__percpu_##nm##_data) __percpu_##nm##_data_boot __PER_CPU_ATTRIB(data_boot_##nm)
#endif

#define PER_CPU_OFF(nm) ((natural_t)(&__percpu_##nm##_data))

#define PER_CPU_PTR(nm, out) __PER_CPU_PTR(PER_CPU_OFF(nm), out)
