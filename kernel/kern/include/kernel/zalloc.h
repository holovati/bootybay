// clang-format off
/*
 *	File:	zalloc.h
 *	Author:	Avadis Tevanian, Jr.
 *	Date:	 1985
 *
 */

#pragma once
#include <aux/init.h>
#include <kernel/lock.h>
#include <kernel/vm/vm_param.h>
#include <mach/machine/vm_types.h>
#include <mach/param.h>

/*
 *	A zone is a collection of fixed size blocks for which there
 *	is fast allocation/deallocation access.  Kernel routines can
 *	use zones to manage data structures dynamically, creating a zone
 *	for each type of data structure to be managed.
 *
 */

// typedef struct zone *zone_t;

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"

#define ZONE_NULL nullptr

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
/* Exported to everyone */
zone_t zinit(vm_size_t size,
             vm_size_t max,
             vm_size_t alloc,
             unsigned int memtype,
             char const *name);
vm_offset_t zalloc(zone_t zone);
vm_offset_t zget(zone_t zone);
void zfree(zone_t zone, vm_offset_t elem);
void zcram(zone_t zone, vm_offset_t newmem, vm_size_t size);

/* Exported only to vm/vm_pageout.c */
void consider_zone_gc();
#endif
#ifdef __cplusplus
} // extern "C"
#endif

/* Exported only to vm/vm_init.c */
void zone_bootstrap();
void zone_init();

/* Memory type bits for zones */
#define ZONE_PAGEABLE    0x00000001
#define ZONE_COLLECTABLE 0x00000002 /* Garbage-collect this zone when memory runs low */
#define ZONE_EXHAUSTIBLE 0x00000004 /* zalloc() on this zone is allowed to fail */
#define ZONE_FIXED       0x00000008 /* Panic if zone is exhausted (XXX) */

/* Machine-dependent code can provide additional memory types.  */
#define ZONE_MACHINE_TYPES 0xffff0000

#ifdef __cplusplus
struct zone_base
{
      protected:
        template <typename T>
        union element
        {
                using list_t = union element *;
                using data_t = T;
                list_t linknext;
                T data;

#ifdef ENABLE_MEM_POISON
                static element *alloc(zone_base *a_zone)
                {
#else
                static element *alloc()
                {

#endif
                        element *e = static_cast<element *>(get_space(sizeof(element), ROUNDUP(alignof(T), sizeof(natural_t))));

#ifdef ENABLE_MEM_POISON
                        zone_base::poison(a_zone, e, sizeof(element));
#endif

                        alloc_register(e, sizeof(element));
                        return e;
                }

                static element *from_ptr(T *a_ptr)
                {
                        return static_cast<element *>(static_cast<void *>(a_ptr));
                }
        };

        template <typename T, size_t INIT_COUNT>
        using init_data_t = char[sizeof(element<T>) * INIT_COUNT] alignas(alignof(element<T>));

        template <typename T>
        void cram_common(void *a_ptr, size_t a_size, typename element<T>::list_t &a_list, simple_lock_data_t *a_lock)
        {
                KASSERT(a_ptr);
                KASSERT(IS_ALIGNED(a_ptr, alignof(T)));
                KASSERT(a_size % sizeof(T) == 0);

                element<T> *new_elements = element<T>::from_ptr(static_cast<T *>(a_ptr));
                size_t new_count         = a_size / sizeof(element<T>);
                simple_lock(a_lock);
                for (size_t i = new_count; i; --i)
                {
#ifdef ENABLE_MEM_POISON
                        poison(this, &new_elements[i - 1], sizeof(element<T>));
#endif
                        element<T> *e = &new_elements[i - 1];

                        e->linknext = a_list;
                        a_list      = e;

                        alloc_register(a_list, sizeof(element<T>));
                }

#ifdef ENABLE_MEM_POISON
                for (element<T> *e : a_list)
                {
                        check_poison(this, e, sizeof(element<T>), sizeof(typename element<T>::link_t));
                }
#endif
                simple_unlock(a_lock);
                m_free_count += new_count;
        }

        template <typename T>
        T *get_common(typename element<T>::list_t &a_list, simple_lock_data_t *a_lock)
        {
                T *result = nullptr;
                simple_lock(a_lock);
                if (!a_list.empty())
                {
#ifdef ENABLE_MEM_POISON
                        for (element<T> *e : a_list)
                        {
                                check_poison(this, e, sizeof(element<T>), sizeof(typename element<T>::link_t));
                        }
#endif

                        element<T> *e = a_list.remove_head();

#ifdef ENABLE_MEM_POISON
                        poison(this, e, sizeof(typename element<T>::link_t));
#endif

                        removed_element();
                        result = &(e->data);
                }
                simple_unlock(a_lock);
                return result;
        }

        template <typename T>
        T *alloc_common(typename element<T>::list_t &a_list, simple_lock_data_t *a_lock)
        {
                element<T> *e;
                simple_lock(a_lock);

                if (a_list != nullptr)
                {
                        --m_free_count;

#ifdef ENABLE_MEM_POISON
                        for (element<T> *ee : a_list)
                        {
                                check_poison(this, ee, sizeof(element<T>), sizeof(typename element<T>::link_t));
                        }
#endif
                        e = a_list;

                        a_list = a_list->linknext;

#ifdef ENABLE_MEM_POISON
                        poison(this, e, sizeof(typename element<T>::link_t));
#endif
                }
                else
                {
#ifdef ENABLE_MEM_POISON
                        e = element<T>::alloc(this);
#else
                        e = element<T>::alloc();
#endif
                        if (e == nullptr)
                        {
                                panic("zone(%s) is empty, called alloc got a nullptr, HOW?!", m_zone_name);
                        }
                }

                m_used_count++;
                simple_unlock(a_lock);

                return &(e->data);
        }

        template <typename T>
        void free_common(T *a_ptr, typename element<T>::list_t &a_list, simple_lock_data_t *a_lock)
        {
                KASSERT(a_ptr);

#ifdef ENABLE_MEM_POISON
                poison(this, a_ptr, sizeof(element<T>));
#endif

                simple_lock(a_lock);

#ifdef ENABLE_MEM_POISON
                for (element<T> *ee : a_list)
                {
                        check_poison(this, ee, sizeof(element<T>), sizeof(typename element<T>::link_t));
                }
#endif
                element<T> *e = element<T>::from_ptr(a_ptr);

                e->linknext = a_list;
                a_list      = e;
                added_element();
                simple_unlock(a_lock);
        }

        template <typename T>
        void *operator new(size_t, T *a_type)
        {
                return a_type;
        }

      public:
        bool is_exhausted()
        {
                return m_used_count == m_max_elements;
        }

      protected:
        void removed_element()
        {
                ++m_used_count;
                --m_free_count;
        }

        void added_element()
        {
                --m_used_count;
                ++m_free_count;
        }

        char const *m_zone_name; /* a name for the zone */
        struct zone_base *m_next;

        i64 m_doing_alloc;   /* is zone expanding now? */
        size_t m_used_count; /* Number of elements used now */
        size_t m_free_count; /* Number of elements free now */

      public:
        size_t m_max_elements; /* how large can this zone grow */

      protected:
        // vm_size_t elem_size;      /* size of an element */
        // vm_size_t alloc_size;     /* size used for more memory */
        // lock_data_t m_complex_lock; /* Lock for pageable zones */
        // i32 m_mem_type;          /* type of memory */
        // size_t cur_size;       /* current memory utilization */

        static void *get_space(size_t a_size, size_t a_alignment);

        static void alloc_register(void *a_ptr, size_t a_element_size);

#ifdef ENABLE_MEM_POISON
        static void poison(zone_base *, void *, size_t);
        static void check_poison(zone_base *, void *, size_t, size_t);
#endif

        zone_base() = default;

        zone_base(char const *a_zone_name, size_t a_max_elemets);

        zone_base(char const *a_zone_name, size_t a_max_elemets, lock_data_t *a_lock);

        zone_base(char const *a_zone_name, size_t a_max_elemets, simple_lock_data_t *a_lock);

        template <typename E, typename L>
        zone_base(char const *a_zone_name, size_t a_max_elemets, L &a_lock, E &a_elements_list) :
                zone_base(a_zone_name, a_max_elemets, &a_lock)
        {
                a_elements_list = nullptr;
        }

      public:
        template <typename T>
        static void init(T *a_zone, char const *a_name)
        {
                new (a_zone) T{ a_name };

                if (sizeof(T::__init_data))
                {
                        a_zone->cram(a_zone->__init_data, sizeof(a_zone->__init_data));
                }
        }
};

// General zone template
template <typename T, size_t MAX_ELEMENTS, size_t GROWTH_CHUNK, i32 MEM_TYPE = ZONE_FIXED, size_t INIT_COUNT = 0>
struct zone
{
};

// Specialization for ZONE_FIXED
template <typename T, size_t MAX_ELEMENTS, size_t GROWTH_CHUNK, size_t INIT_COUNT>
struct zone<T, MAX_ELEMENTS, GROWTH_CHUNK, ZONE_FIXED, INIT_COUNT> : zone_base
{
        using type           = zone<T, MAX_ELEMENTS, GROWTH_CHUNK>;
        using element_t      = zone_base::element<T>;
        using element_list_t = element_t *;

        T *get()
        {
                return get_common<T>(m_free_elements, &m_lock);
        }

        T *alloc()
        {
                if (is_exhausted())
                {
                        panic("zone (%s) is exhausted", m_zone_name);
                }

                return alloc_common<T>(m_free_elements, &m_lock);
        }

        template <typename... Args>
        T *alloc_new(Args... args)
        {
                return new (alloc()) T{ args... };
        }

        template <typename... Args>
        T *get_new(Args... args)
        {
                T *result = get();
                if (result)
                {
                        result = new (result) T{ args... };
                }
                return result;
        }

        void free(T *a_ptr)
        {
                free_common<T>(a_ptr, m_free_elements, &m_lock);
        }

        void free_delete(T *a_ptr)
        {
                KASSERT(a_ptr);
                a_ptr->~T();
                free(a_ptr);
        }

        void cram(void *a_ptr, size_t a_size)
        {
                cram_common<T>(a_ptr, a_size, m_free_elements, &m_lock);
        }

        zone(const char *a_name) :
                zone_base(a_name, MAX_ELEMENTS, m_lock, m_free_elements)
        {
        }

        zone() = default;

        element_list_t m_free_elements;

        simple_lock_data_t m_lock; /* generic lock */

        init_data_t<T, INIT_COUNT> __init_data;
};

#define ZONE_DEFINE(type, name, max_elements, growth_chunk, zone_type, init_count)                                                 \
        namespace                                                                                                                  \
        {                                                                                                                          \
        using __##type##_zone_t = zone<type, max_elements, growth_chunk, zone_type, init_count>;                                   \
        static __##type##_zone_t name;                                                                                             \
        static int __##type##_zone_initcall()                                                                                      \
        {                                                                                                                          \
                __##type##_zone_t::init(&(name), #name);                                                                           \
                return 0;                                                                                                          \
        }                                                                                                                          \
        early_initcall(__##type##_zone_initcall);                                                                                  \
        }                                                                                                                          \
        inline void *operator new(size_t, type *a_type)                                                                            \
        {                                                                                                                          \
                return a_type;                                                                                                     \
        }                                                                                                                          \
        static_assert(1)

// Specialization for ZONE_EXHAUSTIBLE
template <typename T, size_t MAX_ELEMENTS, size_t GROWTH_CHUNK, size_t INIT_COUNT>
struct zone<T, MAX_ELEMENTS, GROWTH_CHUNK, ZONE_EXHAUSTIBLE, INIT_COUNT> : zone_base
{
        using type           = zone<T, MAX_ELEMENTS, GROWTH_CHUNK, ZONE_EXHAUSTIBLE>;
        using element_t      = zone_base::element<T>;
        using element_list_t = typename element_t::list_t;

        T *get()
        {
                return get_common<T>(m_free_elements, &m_lock);
        }

        T *alloc()
        {
                T *result = nullptr;

                if (!is_exhausted())
                {
                        return alloc_common<T>(m_free_elements, &m_lock);
                }

                return result;
        }

        T *calloc()
        {
                return &(*alloc() = {});
        }

        void free(T *a_ptr)
        {
                free_common<T>(a_ptr, m_free_elements, &m_lock);
        }

        void cram(void *a_ptr, size_t a_size)
        {
                cram_common<T>(a_ptr, a_size, m_free_elements, &m_lock);
        }

        zone(const char *a_name) :
                zone_base(a_name, MAX_ELEMENTS, m_lock, m_free_elements)
        {
        }

        zone() = default;

        element_list_t m_free_elements;

        simple_lock_data_t m_lock; /* generic lock */

        init_data_t<T, INIT_COUNT> __init_data;
};

#endif

#if 0

#define zone_count_up(zone)   ((zone)->count++)
#define zone_count_down(zone) ((zone)->count--)

  /* These quick inline versions only work for small, nonpageable zones
   * (currently).  */

  static inline vm_offset_t ZALLOC(zone_t zone) {
    simple_lock(&zone->lock);
    if (zone->free_elements == 0) {
      simple_unlock(&zone->lock);
      return zalloc(zone);
    } else {
      vm_offset_t element = zone->free_elements;
      zone->free_elements = *((vm_offset_t *)(element));
      zone_count_up(zone);
      simple_unlock(&zone->lock);
      return element;
    }
  }

  static inline void ZFREE(zone_t zone, vm_offset_t element) {
    *((vm_offset_t *)(element)) = zone->free_elements;
    zone->free_elements         = (vm_offset_t)(element);
    zone_count_down(zone);
  }

#endif
#pragma GCC diagnostic pop
