/*
 * Mach Operating System
 * Copyright (c) 1993-1987 Carnegie Mellon University
 * All Rights Reserved.
 *
 * Permission to use, copy, modify and distribute this software and its
 * documentation is hereby granted, provided that both the copyright
 * notice and this permission notice appear in all copies of the
 * software, derivative works or modified versions, and any portions
 * thereof, and that both notices appear in supporting documentation.
 *
 * CARNEGIE MELLON ALLOWS FREE USE OF THIS SOFTWARE IN ITS "AS IS"
 * CONDITION.  CARNEGIE MELLON DISCLAIMS ANY LIABILITY OF ANY KIND FOR
 * ANY DAMAGES WHATSOEVER RESULTING FROM THE USE OF THIS SOFTWARE.
 *
 * Carnegie Mellon requests users of this software to return to
 *
 *  Software Distribution Coordinator  or  Software.Distribution@CS.CMU.EDU
 *  School of Computer Science
 *  Carnegie Mellon University
 *  Pittsburgh PA 15213-3890
 *
 * any improvements or extensions that they make and grant Carnegie Mellon
 * the rights to redistribute these changes.
 */
/*
 *	File:	thread.h
 *	Author:	Avadis Tevanian, Jr.
 *
 *	This file contains the structure definitions for threads.
 *
 */

#ifndef _KERN_THREAD_H_
#define _KERN_THREAD_H_

#define THREAD_MAX   1024 /* Max number of threads */
#define THREAD_CHUNK 64   /* Allocation chunk */

#include <kernel/ast.h>
#include <kernel/lock.h>
#include <kernel/processor.h>
#include <kernel/sched.h>
#include <kernel/sched_prim.h> /* event_t, continuation_t */
#include <kernel/thread.h>
#include <kernel/time_out.h>
#include <kernel/timer.h>
#include <kernel/task_data.h> /* for current_space(), current_map() */
#include <kernel/vm/vm_prot.h>

#include <mach/machine/thread.h>
#include <mach/machine/vm_types.h>
#include <kernel/thread_info.h>
#include <kernel/thread_status.h>

/*
 *	Thread states [bits or'ed]
 */
#define TH_WAIT   0x01 /* thread is queued for waiting */
#define TH_SUSP   0x02 /* thread has been asked to stop */
#define TH_RUN    0x04 /* thread is running or on runq */
#define TH_UNINT  0x08 /* thread is waiting uninteruptibly */
#define TH_HALTED 0x10 /* thread is halted at clean point ? */

#define TH_IDLE 0x80 /* thread is an idle thread */

#define TH_SCHED_STATE (TH_WAIT | TH_SUSP | TH_RUN | TH_UNINT)

#define TH_SWAPPED      0x0100 /* thread has no kernel stack */
#define TH_SW_COMING_IN 0x0200 /* thread is waiting for kernel stack */

#define TH_SWAP_STATE (TH_SWAPPED | TH_SW_COMING_IN)

struct thread
{
        /* Run queues */
        queue_chain_t links; /* current run queue links */
        run_queue_t runq;    /* run queue p is on SEE BELOW */
                             /*
                              *	NOTE:	The runq field in the thread structure has an unusual
                              *	locking protocol.  If its value is RUN_QUEUE_NULL, then it is
                              *	locked by the thread_lock, but if its value is something else
                              *	(i.e. a run_queue) then it is locked by that run_queue's lock.
                              */

        /* Task information */
        task_t task;               /* Task to which I belong */
        queue_chain_t thread_list; /* list of threads in task */

        /* Thread bookkeeping */
        queue_chain_t pset_threads; /* list of all threads in proc set*/

        /* Self-preservation */
        simple_lock_data_t lock;
        integer_t ref_count; /* number of references to me */

        /* Hardware state */
        pcb_t pcb;                   /* hardware pcb & machine state */
        vm_offset_t kernel_stack;    /* accurate only if the thread is not swapped and not executing */
        vm_offset_t stack_privilege; /* reserved kernel stack */

        /* Swapping information */
        void (*swap_func)(); /* start here after swapin */

        /* Blocking information */
        event_t wait_event;               /* event we are waiting on */
        integer_t suspend_count;          /* internal use only */
        thread_wait_result_t wait_result; /* outcome of wait - may be examined by this thread WITHOUT locking */
        char pad_bytes[sizeof(natural_t) - sizeof(thread_wait_result_t)];
        boolean_t wake_active; /* someone is waiting for this thread to become suspended */
        integer_t state;       /* Thread state: TH_* [bits or'ed] */

        /* Scheduling information */
        integer_t priority;     /* thread's priority */
        integer_t max_priority; /* maximum priority */
        integer_t sched_pri;    /* scheduled (computed) priority */
#if MACH_FIXPRI
        integer_t sched_data; /* for use by policy */
        integer_t policy;     /* scheduling policy */
#endif
        integer_t depress_priority; /* depressed from this priority */
        natural_t cpu_usage;        /* exp. decaying cpu usage [%cpu] */
        natural_t sched_usage;      /* load-weighted cpu usage [sched] */
        natural_t sched_stamp;      /* last time priority was updated */

        /* VM global variables */
        vm_offset_t recover;    /* page fault recovery (copyin/out) */
        boolean_t vm_privilege; /* Can use reserved memory? */

        /* User-visible scheduling state */
        integer_t user_stop_count; /* outstanding stops */

        /* Timing data structures */
        mach_timer_data_t user_timer;             /* user mode timer */
        mach_timer_data_t system_timer;           /* system mode timer */
        mach_timer_save_data_t user_timer_save;   /* saved user timer value */
        mach_timer_save_data_t system_timer_save; /* saved sys timer val. */
        natural_t cpu_delta;                      /* cpu usage since last update */
        natural_t sched_delta;                    /* weighted cpu usage since update */

        /* Time-outs */
        timer_elt_data_t timer;         /* timer for thread */
        timer_elt_data_t depress_timer; /* timer for priority depression */

        /* Ast/Halt data structures */
        boolean_t active; /* how alive is the thread */
        integer_t ast;    /* ast's needed.  See ast.h */

        /* Processor data structures */
        processor_set_t processor_set; /* assigned processor set */
        processor_t bound_processor;   /* bound to processor ?*/

        // sample_control_t pc_sample;

#if MACH_HOST
        boolean_t may_assign;    /* may assignment change? */
        boolean_t assign_active; /* someone waiting for may_assign */
#endif
        processor_t last_processor; /* processor this last ran on */

        natural_t tls; /* Pointer to thread local storage */

        pthread_t pthread; /* POSIX thread running on this thread */
};

typedef struct thread *thread_t;

#define THREAD_NULL NULL

extern thread_t active_threads[NCPUS];   /* active threads */
extern vm_offset_t active_stacks[NCPUS]; /* active kernel stacks */

/*
 *	Kernel-only routines
 */

void thread_init(void);
void thread_reference(thread_t);
void thread_deallocate(thread_t);
void thread_hold(thread_t);
kern_return_t thread_dowait(thread_t thread, boolean_t must_halt);
void thread_release(thread_t);
kern_return_t thread_halt(thread_t thread, boolean_t must_halt);
void thread_halt_self(void);
void thread_force_terminate(thread_t);
void thread_set_own_priority(int priority);
thread_t kernel_thread(task_t task, void (*start)(void), void *arg);
void thread_timeout_setup(thread_t thread);

/*
 *	thread_quantum_update:
 *
 *	Recalculate the quantum and priority for a thread.
 *	The number of ticks that has elapsed since we were last called
 *	is passed as "nticks."
 *
 *	Called only from clock_interrupt().
 */

void thread_quantum_update(int mycpu, thread_t thread, int nticks, int state);

/*
 *	thread_max_priority:
 *
 *	Reset the max priority for a thread.
 */
kern_return_t thread_max_priority(thread_t thread, processor_set_t pset, int max_priority);

/*
 *	thread_policy:
 *
 *	Set scheduling policy for thread.
 */
kern_return_t thread_policy(thread_t thread, int policy, int data);

/*
 *	thread_start:
 *
 *	Start a thread at the specified routine.
 *	The thread must	be in a swapped state.
 */

void thread_start(thread_t thread, continuation_t start);

/*
 *	thread_priority:
 *
 *	Set priority (and possibly max priority) for thread.
 */
kern_return_t thread_priority(thread_t thread, int priority, boolean_t set_max);

/*
 * Switch to the first thread on a CPU.
 */
void load_context(thread_t new_thread);

kern_return_t thread_getstatus(thread_t thread,
                               enum THREAD_STATE_FLAVOR flavor,
                               thread_state_t tstate, /* pointer to OUT array */
                               natural_t *count /* IN/OUT */);

kern_return_t thread_setstatus(thread_t thread, enum THREAD_STATE_FLAVOR flavor, thread_state_t tstate, natural_t count);

#ifdef __cplusplus
extern "C"
{
#endif

vm_offset_t thread_set_user_regs(thread_t a_thread,
                                 vm_offset_t stack_base,
                                 vm_offset_t stack_size,
                                 vm_offset_t entry,
                                 vm_size_t arg_size);

#ifdef __cplusplus
}
#endif

void thread_clear_pcb(thread_t a_thread);

void thread_bind(thread_t thread, processor_t processor);

void stack_privilege(thread_t thread);

void stack_attach(thread_t thread, vm_offset_t stack, void *continuation);

extern void reaper_thread(void);

void pcb_module_init();
void pcb_init(thread_t thread);
void pcb_terminate(thread_t thread);

void compute_priority(thread_t thread, boolean_t resched);
void update_priority(thread_t thread);

#if MACH_HOST
extern void thread_freeze(thread_t thread);
extern void thread_doassign(thread_t thread, processor_set_t new_pset, boolean_t release_freeze);
extern void thread_unfreeze(thread_t thread);
#endif

/*
 *	Macro-defined routines
 */
#define thread_pcb(th)             ((th)->pcb)
#define thread_lock(th)            simple_lock(&(th)->lock)
#define thread_unlock(th)          simple_unlock(&(th)->lock)
#define thread_should_halt(thread) ((thread)->ast & (AST_HALT | AST_TERMINATE))

/*
 *	Machine specific implementations of the current thread macro
 *	designate this by defining CURRENT_THREAD.
 */
#ifndef CURRENT_THREAD
#define current_thread() (active_threads[/*cpu_number()*/ 0])
#endif

#define current_stack() (active_stacks[/*cpu_number()*/ 0])

#define current_task()  (current_thread()->task)
#define current_space() (current_task()->itk_space)
#define current_map()   (current_task()->map)

#endif
