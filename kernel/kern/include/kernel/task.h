#pragma once

#include <kernel/kern_types.h>

/*
 *	Exported routines/macros
 */

typedef enum
{
        TASK_CREATE_NEW_VMMAP,
        TASK_CREATE_INHERIT_VMMAP,
        TASK_CREATE_SHARE_VMMAP
} task_create_vm_ops_t;

typedef void (*task_enum_threads_callback)(thread_t, void *);

kern_return_t task_create(task_t parent_task, task_create_vm_ops_t a_vm_ops, task_t *child_task);
kern_return_t task_terminate(task_t task);
kern_return_t task_suspend(task_t task);
kern_return_t task_resume(task_t task);
kern_return_t task_threads(task_t task, thread_array_t *thread_list, natural_t *count);
kern_return_t task_info(task_t task, int flavor, task_info_t task_info_out, natural_t *task_info_count);
void task_enum_threads(task_t a_task, task_enum_threads_callback a_cb, void *a_user_data);

// kern_return_t task_get_special_port(task_t task, int which, struct ipc_port **portp);
// kern_return_t task_set_special_port(task_t task, int which, struct ipc_port *port);
kern_return_t task_assign(task_t task, processor_set_t new_pset, boolean_t assign_threads);
kern_return_t task_assign_default(task_t task, boolean_t assign_threads);
