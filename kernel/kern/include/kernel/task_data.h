#pragma once
/*
 * Mach Operating System
 * Copyright (c) 1993-1988 Carnegie Mellon University
 * All Rights Reserved.
 *
 * Permission to use, copy, modify and distribute this software and its
 * documentation is hereby granted, provided that both the copyright
 * notice and this permission notice appear in all copies of the
 * software, derivative works or modified versions, and any portions
 * thereof, and that both notices appear in supporting documentation.
 *
 * CARNEGIE MELLON ALLOWS FREE USE OF THIS SOFTWARE IN ITS "AS IS"
 * CONDITION.  CARNEGIE MELLON DISCLAIMS ANY LIABILITY OF ANY KIND FOR
 * ANY DAMAGES WHATSOEVER RESULTING FROM THE USE OF THIS SOFTWARE.
 *
 * Carnegie Mellon requests users of this software to return to
 *
 *  Software Distribution Coordinator  or  Software.Distribution@CS.CMU.EDU
 *  School of Computer Science
 *  Carnegie Mellon University
 *  Pittsburgh PA 15213-3890
 *
 * any improvements or extensions that they make and grant Carnegie Mellon
 * the rights to redistribute these changes.
 */
/*
 *	File:	task.h
 *	Author:	Avadis Tevanian, Jr.
 *
 *	This file contains the structure definitions for tasks.
 *
 */

#if !defined(__NEED_vm_map)
#define __NEED_vm_map
#endif

#include <kernel/task.h>

#define TASK_MAX   1024 /* Max number of tasks */
#define TASK_CHUNK 64   /* Allocation chunk */

#include <kernel/env.h>
#include <kernel/lock.h>
#include <kernel/processor.h>
#include <kernel/task_info.h>
#include <kernel/time_value.h>
#include <kernel/vm/vm_types.h>

extern char __pertask_start[];
extern char __pertask_end[];

#define __PER_TASK_ATTRIB(section_name) __attribute__((unused, section(".pertask_" #section_name)))
#define __PER_TASK_NAME(nm)             __pertask_##nm##_data

#define PER_TASK_VAR(nm)         __PER_TASK_NAME(nm) __PER_TASK_ATTRIB(data_##nm)
#define PER_TASK_OFF(nm)         ((natural_t)(&__PER_TASK_NAME(nm)))
#define PER_TASK_PTR(nm)         (PER_TASK_PTR_EX(current_task(), nm))
#define PER_TASK_PTR_EX(tsk, nm) ((decltype(&__PER_TASK_NAME(nm)))(&((tsk)->env_area)[PER_TASK_OFF(nm)]))

struct task
{
        /* Synchronization/destruction information */
        simple_lock_data_t lock; /* Task's lock */
        integer_t ref_count;     /* Number of references to me */
        int32_t active;          /* Task has not been terminated */
        int32_t suspend_count;   /* Internal scheduling only */

        /* Miscellaneous */
        vm_map_t map;             /* Address space description */
        queue_chain_t pset_tasks; /* list of tasks assigned to pset */

        /* Thread information */
        queue_head_t thread_list; /* list of threads */
        natural_t thread_count;   /* number of threads */
        // size_t prog_brk;               /* End of process's data segment */
        processor_set_t processor_set; /* processor set for new threads */
        boolean_t may_assign;          /* can assigned pset be changed? */
        boolean_t assign_active;       /* waiting for may_assign */

        /* User-visible scheduling information */
        int user_stop_count; /* outstanding stops */
        int priority;        /* for new threads */

        /* Statistics */
        time_value_t total_user_time;
        /* total user time for dead threads */
        time_value_t total_system_time;
        /* total system time for dead threads */
        // sample_control_t pc_sample;

        char *env_area; /* PER_TASK_VAR(...) data */
};

#define task_lock(task)   simple_lock(&(task)->lock)
#define task_unlock(task) simple_unlock(&(task)->lock)

/*
 *	Internal only routines
 */

void task_init();
void task_reference(task_t task);
void task_deallocate(task_t task);
kern_return_t task_hold(task_t task);
kern_return_t task_dowait(task_t task, boolean_t must_wait);
kern_return_t task_release();
kern_return_t task_halt(task_t task, boolean_t must_wait);

kern_return_t task_suspend_nowait();
task_t kernel_task_create();

extern task_t kernel_task;
