#pragma once
/*
 *	Definitions for cpu identification in multi-processors.
 */

extern int master_cpu; /* 'master' processor - keeps time */

#ifdef __cplusplus
extern "C"
{
#endif

        int cpu_number(); // Defined in model_dep.c

#ifdef __cplusplus
}
#endif
