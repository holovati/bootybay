#pragma once
#include <kernel/thread_status.h>
typedef struct task *task_t;
typedef struct thread *thread_t;
typedef struct processor_set *processor_set_t;
typedef struct time_value time_value_t;

/*
 *	User routines
 */

kern_return_t thread_create(task_t parent_task, thread_t *child_thread);
kern_return_t thread_terminate(thread_t thread);
kern_return_t thread_suspend(thread_t thread);
kern_return_t thread_resume(thread_t thread);

void thread_dup(thread_t parent, thread_t child, natural_t **a_child_tls, natural_t **a_child_sp, natural_t **a_child_retval);

kern_return_t thread_get_state(thread_t thread,
                               enum THREAD_STATE_FLAVOR flavor,
                               thread_state_t old_state,
                               natural_t *old_state_count);
kern_return_t thread_set_state(thread_t thread,
                               enum THREAD_STATE_FLAVOR flavor,
                               thread_state_t new_state,
                               natural_t new_state_count);
// kern_return_t thread_set_special_port(thread_t thread, int which, struct ipc_port *port);
// kern_return_t thread_info(thread_t thread, int flavor, thread_info_t thread_info_out, natural_t *thread_info_count);
kern_return_t thread_assign(thread_t thread, processor_set_t new_pset);
kern_return_t thread_assign_default(thread_t thread);
void thread_read_times(thread_t thread, time_value_t *user_time_p, time_value_t *system_time_p);
