/*
 * Mach Operating System
 * Copyright (c) 1991,1990,1989,1988,1987 Carnegie Mellon University
 * All Rights Reserved.
 *
 * Permission to use, copy, modify and distribute this software and its
 * documentation is hereby granted, provided that both the copyright
 * notice and this permission notice appear in all copies of the
 * software, derivative works or modified versions, and any portions
 * thereof, and that both notices appear in supporting documentation.
 *
 * CARNEGIE MELLON ALLOWS FREE USE OF THIS SOFTWARE IN ITS "AS IS"
 * CONDITION.  CARNEGIE MELLON DISCLAIMS ANY LIABILITY OF ANY KIND FOR
 * ANY DAMAGES WHATSOEVER RESULTING FROM THE USE OF THIS SOFTWARE.
 *
 * Carnegie Mellon requests users of this software to return to
 *
 *  Software Distribution Coordinator  or  Software.Distribution@CS.CMU.EDU
 *  School of Computer Science
 *  Carnegie Mellon University
 *  Pittsburgh PA 15213-3890
 *
 * any improvements or extensions that they make and grant Carnegie Mellon
 * the rights to redistribute these changes.
 */
/*
 *	File:	vm/vm_fault.h
 *
 *	Page fault handling module declarations.
 */

#pragma once

#if !defined(__NEED_vm_map)
#define __NEED_vm_map
#endif

#if !defined(__NEED_vm_prot)
#define __NEED_vm_prot
#endif

#include <kernel/vm/vm_types.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Forward declarations */
typedef struct vm_map_entry *vm_map_entry_t;

/*
 *	Page fault handling based on vm_object only.
 */

typedef kern_return_t vm_fault_return_t;
#define VM_FAULT_SUCCESS             0
#define VM_FAULT_RETRY               1
#define VM_FAULT_INTERRUPTED         2
#define VM_FAULT_MEMORY_SHORTAGE     3
#define VM_FAULT_FICTITIOUS_SHORTAGE 4
#define VM_FAULT_MEMORY_ERROR        5

extern void vm_fault_init();
extern vm_fault_return_t vm_fault_page();

extern void vm_fault_cleanup();
/*
 *	Page fault handling based on vm_map (or entries therein)
 */

kern_return_t vm_fault(vm_map_t map, vm_offset_t vaddr, vm_prot_t fault_type, boolean_t change_wiring);
kern_return_t vm_fault_wire(vm_map_t map, vm_offset_t start, vm_offset_t end);
kern_return_t vm_fault_unwire(vm_map_t map, vm_offset_t start, vm_offset_t end);

void vm_fault_copy_entry(vm_map_t dst_map, vm_map_t src_map, vm_map_entry_t dst_entry, vm_map_entry_t src_entry);

kern_return_t vm_fault_copy(); /* Copy pages from
                                * one object to another
                                */
#ifdef __cplusplus
}
#endif
