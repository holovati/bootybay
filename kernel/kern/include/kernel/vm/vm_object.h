#pragma once
#if !defined(__NEED_vm_object)
#define __NEED_vm_object
#endif

#if !defined(__NEED_vm_object_operations)
#define __NEED_vm_object_operations
#endif

#if !defined(__NEED_vm_prot)
#define __NEED_vm_prot
#endif

#if !defined(__NEED_vm_page_map)
#define __NEED_vm_page_map
#endif

#if !defined(__NEED_vm_page)
#define __NEED_vm_page
#endif

#include <kernel/lock.h>
#include <kernel/vm/vm_types.h>

struct vm_object_operations_data
{
        kern_return_t (*get_page)(vm_object_t a_vm_obj, vm_offset_t a_vmo, vm_prot_t a_prot, vm_page_t *a_vmp_out);
        kern_return_t (*inactive)(vm_object_t a_vm_obj);
        boolean_t (*collapse)(vm_object_t a_vm_obj,
                              vm_object_t a_destination,
                              vm_object_t *a_backing_object,
                              vm_offset_t *a_backing_object_offset);
        kern_return_t (*set_size)(vm_object_t a_vm_obj, natural_t a_new_size);
};

#define VMO_LOCK_DEBUG

struct vm_object_data
{
        vm_object_operations_t m_ops;
        vm_page_map_t m_page_map;
        integer_t m_usecnt;
        natural_t m_size;
        struct lock_data m_lock;
#if defined(VMO_LOCK_DEBUG)
        char const *m_locked_in_file;
        integer_t m_locked_on_line;
#endif
};

void vm_object_init(vm_object_t a_vm_obj);

kern_return_t vm_object_create_unamed(natural_t a_len, vm_object_t *a_vm_obj_out);

kern_return_t vm_object_create_shadow(vm_object_t a_vm_obj_src,
                                      vm_offset_t a_src_offset,
                                      natural_t a_len,
                                      vm_object_t *a_vm_obj_shdw_out);

kern_return_t vm_object_get_page(vm_object_t a_vm_obj, vm_offset_t a_vmo, vm_prot_t a_vm_prot, vm_page_t *a_vmp_out);

boolean_t vm_object_collapse(vm_object_t a_vm_obj,
                             vm_object_t a_destination,
                             vm_object_t *a_backing_object,
                             vm_offset_t *a_backing_object_offset);

kern_return_t vm_object_set_size(vm_object_t a_vm_obj, natural_t a_new_size);

void vm_object_prot_change(vm_object_t a_vm_obj, vm_prot_t a_new_prot);

#if defined(VMO_LOCK_DEBUG)
void __vm_object_lock(vm_object_t a_vm_obj, char const *a_file, integer_t a_line);
#define vm_object_lock(vmo) (__vm_object_lock((vmo), __FILE__, __LINE__))
#else
void vm_object_lock(vm_object_t a_vm_obj);
#endif

void vm_object_unlock(vm_object_t a_vm_obj);

boolean_t vm_object_is_locked(vm_object_t a_vm_obj);

void vm_object_ref(vm_object_t a_vm_obj);

void vm_object_rel(vm_object_t a_vm_obj);

void vm_object_put(vm_object_t a_vm_obj);

kern_return_t vm_object_op_get_page_fail(vm_object_t a_vm_obj, vm_offset_t a_vmo, vm_prot_t a_prot, vm_page_t *a_vmp_out);

boolean_t vm_object_op_collapse_fail(vm_object_t a_vm_obj,
                                     vm_object_t a_destination,
                                     vm_object_t *a_backing_object,
                                     vm_offset_t *a_backing_object_offset);

kern_return_t vm_object_op_set_size_noop(vm_object_t a_vm_obj, natural_t a_new_size);
