#pragma once

#include <etl/rbtnode.h>

struct vm_page_map_data
{
        struct rbtnode_data m_sentinel;
        struct rbtnode_data *m_root;
};


