#if defined(__NEED_vm_object) && !defined(__DEFINED_vm_object)
#define __DEFINED_vm_object
typedef struct vm_object_data *vm_object_t;
#endif

#if defined(__NEED_vm_object_operations) && !defined(__DEFINED_vm_object_operations)
#define __DEFINED_vm_object_operations
typedef struct vm_object_operations_data *vm_object_operations_t;
#endif

#if defined(__NEED_vm_page) && !defined(__DEFINED_vm_page)
#define __DEFINED_vm_page
typedef struct vm_page *vm_page_t;
#endif

#if defined(__NEED_vm_page_map) && !defined(__DEFINED_vm_page_map)
#define __DEFINED_vm_page_map
typedef struct vm_page_map_data *vm_page_map_t;
#endif

#if defined(__NEED_vm_map) && !defined(__DEFINED_vm_map)
#define __DEFINED_vm_map
typedef struct vm_map *vm_map_t;
#endif

#if defined(__NEED_vm_prot) && !defined(__DEFINED_vm_prot)
#define __DEFINED_vm_prot
typedef int vm_prot_t;
#endif
