#pragma once

#if !defined(__NEED_vm_page_map)
#define __NEED_vm_page_map
#endif

#if !defined(__NEED_vm_page)
#define __NEED_vm_page
#endif

#include <etl/queue.h>
#include <etl/rbtnode.h>

#include <kernel/vm/vm_types.h>

typedef struct vm_page_io_descr
{
        vm_page_t m_next;
        uint64_t m_write       : 1;
        uint64_t m_block_sel__ : 3;
        uint64_t m_unused_bits : 60;
        uint64_t m_lba[8];
} *vm_page_io_descr_t;

#ifdef __cplusplus

static_assert((sizeof(vm_page_io_descr)) == (8 * 8) + 8 + 8);

/*
 * These are the flags defined for vm_page.
 *
 * Note: PG_FILLED and PG_DIRTY are added for the filesystems.
 */
#define PG_INACTIVE    0x00001 /* page is in inactive list (P) */
#define PG_ACTIVE      0x00002 /* page is in active list (P) */
#define PG_LAUNDRY     0x00004 /* page is being cleaned now (P)*/
#define PG_CLEAN       0x00008 /* page has not been modified */
#define PG_BUSY        0x00010 /* page is in transit (O) */
#define PG_WANTED      0x00020 /* someone is waiting for page (O) */
#define PG_TABLED      0x00040 /* page is in VP table (O) */
#define PG_COPYONWRITE 0x00080 /* must copy page before changing (O) */
#define PG_FICTITIOUS  0x00100 /* physical page doesn't exist (O) */
#define PG_FAKE        0x00200 /* page is placeholder for pagein (O) */
#define PG_FILLED      0x00400 /* client flag to set when filled */
#define PG_DIRTY       0x00800 /* client flag to set when dirty */
#define PG_PAGEROWNED  0x04000 /* DEBUG: async paging op in progress */
#define PG_PTPAGE      0x08000 /* DEBUG: is a user page table page */
#define PG_PRIVATE     0x10000 /* Page is not owned by the pmap system */

struct vm_page
{
        /**
         * @brief
         * Each pageable resident page falls into one of the following three lists.
         * Active, inactive and free
         */
        queue_chain_t m_qlink;

        /* pages in same object (O)*/
        rbtnode_data m_map_link;

        /**
         * @brief
         * Offset into that object (O,P)
         */
        vm_offset_t offset;

        /**
         * @brief
         * wired down maps refs (P)
         */
        natural_t wire_count;

        /**
         * @brief
         *
         */
        natural_t flags;

        /**
         * @brief
         * Physical address of page
         */
        natural_t phys_addr;

        /**
         * @brief
         * When a page is used for buffered IO it has a block cluster describing the IO operation
         */
        vm_page_io_descr_t m_io_descr;
        /**
         * @brief
         * Put the specified page on the active list (if appropriate).
         * The page queues must be locked.
         */
        void activate();

        /**
         * @brief
         * Returns the given page to the inactive list,
         * indicating that no physical maps have access
         * to this page. [Used by the physical mapping system.]
         */
        void deactivate();

        /**
         * @brief
         * Zero-fill the specified page.
         * @return int
         */
        int zero_fill();

        void wire();

        void unwire();
};
#endif // __cplusplus

#ifdef __cplusplus
extern "C"
{
#endif
vm_page_t vm_page_get_page(vm_address_t phys_addr);

void vm_page_wait(vm_page_t a_vmp);

void vm_page_wake(vm_page_t a_vmp);

vm_page_t vm_page_alloc(vm_page_map_t a_vmp_map, vm_offset_t offset);

vm_page_t vm_page_alloc_private(vm_page_map_t a_vmp_map, vm_offset_t offset, vm_address_t a_phys_address);

void vm_page_free(vm_page_map_t a_map, vm_page_t);

void vm_page_copy(vm_page_t src_m, vm_page_t dest_m, vm_offset_t a_offset, size_t a_src_size);

boolean_t vm_page_modified(vm_page_t a_vmp);

kern_return_t vm_page_rename(vm_page_map_t a_src_vmp_map, vm_page_t a_vmp, vm_page_map_t a_dst_vmp_map, vm_offset_t a_dst_offset);

void vm_page_map_remove_range(vm_page_map_t a_vmp_map, vm_offset_t a_start, vm_offset_t a_end);
void vm_page_map_init(vm_page_map_t a_vm_page_map);
vm_page_t vm_page_map_first(vm_page_map_t a_vm_page_map);
vm_page_t vm_page_map_next(vm_page_t a_vm_page);
vm_page_t vm_page_map_remove(vm_page_map_t a_vm_page_map, vm_page_t a_vm_page);
vm_page_t vm_page_map_lookup(vm_page_map_t a_map, vm_offset_t a_offset);
void vm_page_map_insert(vm_page_map_t a_map, vm_page_t a_vmp, vm_offset_t offset);
void vm_page_map_clear(vm_page_map_t a_vm_page_map);
boolean_t vm_page_map_empty(vm_page_map_t a_vm_page_map);

#ifdef __cplusplus
}
#endif
