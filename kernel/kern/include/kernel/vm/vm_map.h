/*
 * Mach Operating System
 * Copyright (c) 1991,1990,1989,1988,1987 Carnegie Mellon University.
 * Copyright (c) 1993,1994 The University of Utah and
 * the Computer Systems Laboratory (CSL).
 * All rights reserved.
 *
 * Permission to use, copy, modify and distribute this software and its
 * documentation is hereby granted, provided that both the copyright
 * notice and this permission notice appear in all copies of the
 * software, derivative works or modified versions, and any portions
 * thereof, and that both notices appear in supporting documentation.
 *
 * CARNEGIE MELLON, THE UNIVERSITY OF UTAH AND CSL ALLOW FREE USE OF
 * THIS SOFTWARE IN ITS "AS IS" CONDITION, AND DISCLAIM ANY LIABILITY
 * OF ANY KIND FOR ANY DAMAGES WHATSOEVER RESULTING FROM THE USE OF
 * THIS SOFTWARE.
 *
 * Carnegie Mellon requests users of this software to return to
 *
 *  Software Distribution Coordinator  or  Software.Distribution@CS.CMU.EDU
 *  School of Computer Science
 *  Carnegie Mellon University
 *  Pittsburgh PA 15213-3890
 *
 * any improvements or extensions that they make and grant Carnegie Mellon
 * the rights to redistribute these changes.
 */
/*
 *	File:	vm/vm_map.h
 *	Author:	Avadis Tevanian, Jr., Michael Wayne Young
 *	Date:	1985
 *
 *	Virtual memory map module definitions.
 *
 * Contributors:
 *	avie, dlb, mwyoung
 */

#ifndef _VM_VM_MAP_H_
#define _VM_VM_MAP_H_

#if !defined(__NEED_vm_object)
#define __NEED_vm_object
#endif

#if !defined(__NEED_vm_map)
#define __NEED_vm_map
#endif

#include <kernel/lock.h>
#include <kernel/vm/pmap.h>
#include <kernel/vm/vm_inherit.h>
#include <kernel/vm/vm_types.h>
#include <etl/bit.hh>

/*
 *	Types defined:
 *
 *	vm_map_t		the high-level address map data structure.
 *	vm_map_entry_t		an entry in an address map.
 *	vm_map_version_t	a timestamp of a map, for use with vm_map_lookup
 *	vm_map_copy_t		represents memory copied from an address map,
 *				 used for inter-map copy operations
 */

/*
 *	Type:		vm_map_object_t [internal use only]
 *
 *	Description:
 *		The target of an address mapping, either a virtual
 *		memory object or a sub map (of the kernel map).
 */
typedef union vm_map_object
{
        vm_object_t vm_object;    /* object object */
        struct vm_map *share_map; /* share map */
        struct vm_map *sub_map;   /* belongs to another map */
} vm_map_object_t;

/*
 *	Type:		vm_map_entry_t [internal use only]
 *
 *	Description:
 *		A single mapping within an address map.
 *
 *	Implementation:
 *		Address map entries consist of start and end addresses,
 *		a VM object (or sub map) and offset into that object,
 *		and user-exported inheritance and protection information.
 *		Control information for virtual copy operations is also
 *		stored in the address map entry.
 */
struct vm_map_links
{
        struct vm_map_entry *prev; /* previous entry */
        struct vm_map_entry *next; /* next entry */
        vm_offset_t start;         /* start address */
        vm_offset_t end;           /* end address */
};

#if __cplusplus
#define VM_MAP_ENTRY_IS_SHARED     etl::bit<decltype(vm_map_entry::flags), 0>::value /* region is shared */
#define VM_MAP_ENTRY_IS_A_MAP      etl::bit<decltype(vm_map_entry::flags), 1>::value /* Is "object" a map? */
#define VM_MAP_ENTRY_IS_A_SUBMAP   etl::bit<decltype(vm_map_entry::flags), 2>::value /* Is "object" a submap? */
#define VM_MAP_ENTRY_COW           etl::bit<decltype(vm_map_entry::flags), 6>::value /* Is data copy-on-write */
#define VM_MAP_ENTRY_IN_TRANSITION etl::bit<decltype(vm_map_entry::flags), 4>::value /* Entry being changed */
#define VM_MAP_ENTRY_NEEDS_WAKEUP  etl::bit<decltype(vm_map_entry::flags), 5>::value /* Waiters on in_transition */
#define VM_MAP_ENTRY_NEEDS_COPY    etl::bit<decltype(vm_map_entry::flags), 6>::value /* does object need to be copied */
#define VM_MAP_ENTRY_NO_FREE       etl::bit<decltype(vm_map_entry::flags), 7>::value /* The entry does not belong to the vm subsystem */
#define VM_MAP_ENTRY_MIRROR_OBJECT etl::bit<decltype(vm_map_entry::flags), 8>::value /* If the object is to small for the entry (object_offset % object_size) */

#define VM_MAP_ENTRY_NULL nullptr
#endif

typedef struct vm_map_entry *vm_map_entry_t;

struct vm_map_entry
{
        struct vm_map_links links;  /* links to other entries */
        union vm_map_object object; /* object I point to */
        vm_offset_t offset;         /* offset into object */
        u32 flags;

        /* Only in task maps: */
        vm_prot_t protection;     /* protection code */
        vm_prot_t max_protection; /* maximum protection */
        vm_inherit_t inheritance; /* inheritance */
        i32 __pad;
        i16 wired_count;      /* can be paged if = 0 */
        i16 user_wired_count; /* for vm_wire */
        /*
        0 for normal map entry
        or persistent kernel map projected buffer entry;
        -1 for non-persistent kernel map projected buffer entry;
        pointer to corresponding kernel map entry for user map
        projected buffer entry
        */
        struct vm_map_entry *projected_on;

#if __cplusplus
#define FORCE_INLINE __attribute__((always_inline))

        FORCE_INLINE vm_map_entry_t prev()
        {
                return links.prev;
        }
        FORCE_INLINE vm_map_entry_t next()
        {
                return links.next;
        }

        FORCE_INLINE vm_offset_t start()
        {
                return links.start;
        }
        FORCE_INLINE vm_offset_t end()
        {
                return links.end;
        }

        FORCE_INLINE void set_start(vm_offset_t a_start)
        {
                links.start = a_start;
        }
        FORCE_INLINE void set_end(vm_offset_t a_end)
        {
                links.end = a_end;
        }

#endif
};

/*
 *	Type:		struct vm_map_header
 *
 *	Description:
 *		Header for a vm_map and a vm_map_copy.
 */
struct vm_map_header
{
        struct vm_map_links links; /* first, last, min, max */
        integer_t nentries;        /* Number of entries */
        boolean_t entries_pageable;
        /* are map entries pageable? */
};

/*
 *	Type:		vm_map_t [exported; contents invisible]
 *
 *	Description:
 *		An address map -- a directory relating valid
 *		regions of a task's address space to the corresponding
 *		virtual memory objects.
 *
 *	Implementation:
 *		Maps are doubly-linked lists of map entries, sorted
 *		by address.  One hint is used to start
 *		searches again from the last successful search,
 *		insertion, or removal.  Another hint is used to
 *		quickly find free space.
 */
struct vm_map
{
        lock_data_t lock;             /* Lock for map data */
        struct vm_map_header hdr;     /* Map entry header */
        pmap_t pmap;                  /* Physical map */
        vm_map_entry_t hint;          /* hint for quick lookups */
        vm_size_t size;               /* virtual size */
        natural_t timestamp;          /* Version number */
        natural_t ref_count;          /* Reference count */
        vm_map_entry_t first_free;    /* First free space hint */
        natural_t is_main_map;        /* Am I a main map? */
        simple_lock_data_t ref_lock;  /* Lock for ref_count field */
        simple_lock_data_t hint_lock; /* lock for hint storage */
#if __cplusplus
        /* Lowest valid address in a map */
        vm_offset_t min_offset()
        {
                return hdr.links.start;
        }
        /* Highest valid address */
        vm_offset_t max_offset()
        {
                return hdr.links.end;
        }
#endif
};

#define VM_MAP_NULL nullptr

#define vm_map_to_entry(map)    ((struct vm_map_entry *)&(map)->hdr.links)
#define vm_map_first_entry(map) ((map)->hdr.links.next)
#define vm_map_last_entry(map)  ((map)->hdr.links.prev)

/*
 *	Type:		vm_map_version_t [exported; contents invisible]
 *
 *	Description:
 *		Map versions may be used to quickly validate a previous
 *		lookup operation.
 *
 *	Usage note:
 *		Because they are bulky objects, map versions are usually
 *		passed by reference.
 *
 *	Implementation:
 *		Just a timestamp for the main map.
 */
typedef struct vm_map_version
{
        unsigned int main_timestamp;
} vm_map_version_t;

#define vm_map_lock_drain_interlock(map)                                                                                           \
        {                                                                                                                          \
                /*lockmgr(&(map)->lock, LK_DRAIN | LK_INTERLOCK, &(map)->ref_lock,                                                 \
                 * curproc);*/                                                                                                     \
                (map)->timestamp++;                                                                                                \
        }

#if DAMIR
/*
 *	Type:		vm_map_copy_t [exported; contents invisible]
 *
 *	Description:
 *		A map copy object represents a region of virtual memory
 *		that has been copied from an address map but is still
 *		in transit.
 *
 *		A map copy object may only be used by a single thread
 *		at a time.
 *
 *	Implementation:
 * 		There are three formats for map copy objects.
 *		The first is very similar to the main
 *		address map in structure, and as a result, some
 *		of the internal maintenance functions/macros can
 *		be used with either address maps or map copy objects.
 *
 *		The map copy object contains a header links
 *		entry onto which the other entries that represent
 *		the region are chained.
 *
 *		The second format is a single vm object.  This is used
 *		primarily in the pageout path.  The third format is a
 *		list of vm pages.  An optional continuation provides
 *		a hook to be called to obtain more of the memory,
 *		or perform other operations.  The continuation takes 3
 *		arguments, a saved arg buffer, a pointer to a new vm_map_copy
 *		(returned) and an abort flag (abort if TRUE).
 */

#define VM_MAP_COPY_PAGE_LIST_MAX 64

typedef struct vm_map_copy
{
        int type;
        int pad;
#define VM_MAP_COPY_ENTRY_LIST 1
#define VM_MAP_COPY_OBJECT     2
#define VM_MAP_COPY_PAGE_LIST  3
        vm_offset_t offset;
        vm_size_t size;
        union
        {
                struct vm_map_header hdr; /* ENTRY_LIST */
                struct
                { /* OBJECT */
                        vm_object_t object;
                } c_o;
                struct
                { /* PAGE_LIST */
                        vm_page_t page_list[VM_MAP_COPY_PAGE_LIST_MAX];
                        size_t npages;
                        kern_return_t (*cont)();
                        char *cont_args;
                } c_p;
        } c_u;
} *vm_map_copy_t;

#define cpy_hdr c_u.hdr

#define cpy_object c_u.c_o.object

#define cpy_page_list c_u.c_p.page_list
#define cpy_npages    c_u.c_p.npages
#define cpy_cont      c_u.c_p.cont
#define cpy_cont_args c_u.c_p.cont_args

#define VM_MAP_COPY_NULL ((vm_map_copy_t)0)
#endif
/*
 *	Useful macros for entry list copy objects
 */

#define vm_map_copy_first_entry(copy) ((copy)->cpy_hdr.links.next)
#define vm_map_copy_last_entry(copy)  ((copy)->cpy_hdr.links.prev)

#define vm_map_lock(a_map)                                                                                                         \
        do                                                                                                                         \
        {                                                                                                                          \
                lock_write(&(a_map->lock));                                                                                        \
                a_map->timestamp++;                                                                                                \
        }                                                                                                                          \
        while (0)

#define vm_map_unlock(a_map) lock_write_done(&(a_map->lock))

// static inline void vm_map_lock_set_recursive(vm_map_t a_map) { lock_set_recursive(&(a_map)->lock); }

// static inline void vm_map_lock_clear_recursive(vm_map_t a_map) { lock_clear_recursive(&(a_map)->lock); }

/*
 *	Exported procedures that operate on vm_map_t.
 */

extern void *kentry_data;
extern vm_size_t kentry_data_size;
extern int kentry_count;
extern void vm_map_init(); /* Initialize the module */

#ifdef __cplusplus
extern "C"
{
#endif
/*
 *	vm_map_create:
 *
 *	Creates and returns a new empty VM map with
 *	the given physical map structure, and having
 *	the given lower and upper address bounds.
 */
vm_map_t vm_map_create(pmap_t pmap, vm_offset_t min, vm_offset_t max, boolean_t pageable);

/*
 * vm_map_fork:
 * Create a new map based on an existing pmap.
 * The new map is based on the old map, according to the inheritance
 * values on the regions in that map.
 *
 * The source map must not be locked.
 */
vm_map_t vm_map_fork(vm_map_t old_map);

void vm_map_reference(vm_map_t a_vmmap); /* Gain a reference to an existing map */

void vm_map_deallocate(vm_map_t map); /* Lose a reference */

/* Enter a mapping primitive */
kern_return_t vm_map_find_entry(vm_map_t map,
                                vm_offset_t *address, /* OUT */
                                vm_size_t size,
                                vm_offset_t mask,
                                vm_object_t object,
                                vm_map_entry_t *o_entry /* OUT */);

/*
 *	vm_map_lookup_entry:
 *
 *	Finds the map entry containing (or
 *	immediately preceding) the specified address
 *	in the given map; the entry is returned
 *	in the "entry" parameter.  The boolean
 *	result indicates whether the address is
 *	actually contained in the map.
 */
boolean_t vm_map_lookup_entry(vm_map_t map, vm_offset_t address, vm_map_entry_t *entry /* OUT */);

kern_return_t vm_map_remove(vm_map_t map, vm_offset_t start, vm_offset_t end);

kern_return_t vm_map_protect(vm_map_t map,
                             vm_offset_t start,
                             vm_offset_t end,
                             vm_prot_t new_prot,
                             boolean_t set_max); /* Change protection */

/* Change inheritance */
kern_return_t vm_map_inherit(vm_map_t map, vm_offset_t start, vm_offset_t end, vm_inherit_t new_inheritance);

extern void vm_map_print(); /* Debugging: print a map */

/* Look up an address */
kern_return_t vm_map_lookup(vm_map_t *var_map, /* IN/OUT */
                            vm_offset_t vaddr,
                            vm_prot_t fault_type,
                            vm_map_entry_t *out_entry, /* OUT */
                            vm_object_t *object,       /* OUT */
                            vm_offset_t *offset,       /* OUT */
                            vm_prot_t *out_prot,       /* OUT */
                            boolean_t *wired,          /* OUT */
                            boolean_t *single_use /* OUT */);

void vm_map_lookup_done(vm_map_t map, vm_map_entry_t entry);

/*
 *	vm_map_copy:
 *
 *	Perform a virtual memory copy from the source
 *	address map/range to the destination map/range.
 *
 *	If src_destroy or dst_alloc is requested,
 *	the source and destination regions should be
 *	disjoint, not only in the top-level map, but
 *	in the sharing maps as well.  [The best way
 *	to guarantee this is to use a new intermediate
 *	map to make copies.  This also reduces map
 *	fragmentation.]
 */
kern_return_t vm_map_copy(vm_map_t dst_map,
                          vm_map_t src_map,
                          vm_offset_t dst_addr,
                          vm_size_t len,
                          vm_offset_t src_addr,
                          boolean_t dst_alloc,
                          boolean_t src_destroy);

/*
 * Find sufficient space for `length' bytes in the given map, starting at
 * `start'.  The map must be locked.  Returns 0 on success, 1 on no space.
 */
kern_return_t vm_map_findspace(vm_map_t map, vm_offset_t start, vm_size_t length, vm_offset_t *addr);

/*
 *	vm_map_insert:
 *
 *	Inserts the given whole VM object into the target
 *	map at the specified address range.  The object's
 *	size should match that of the address range.
 *
 *	Requires that the map be locked, and leaves it so.
 */

kern_return_t vm_map_insert(vm_map_t map,
                            vm_object_t object,
                            vm_offset_t offset,
                            vm_offset_t start,
                            vm_offset_t end,
                            vm_map_entry_t a_new_entry);

/*
 *	vm_map_find finds an unallocated region in the target address
 *	map with the given length.  The search is defined to be
 *	first-fit from the specified address; the region found is
 *	returned in the same parameter.
 *
 */
kern_return_t vm_map_find(vm_map_t map,
                          vm_object_t object,
                          vm_offset_t offset,
                          vm_offset_t *addr, /* IN/OUT */
                          vm_size_t length,
                          boolean_t find_space,
                          vm_map_entry_t a_new_entry);

/*
 *	vm_map_submap:		[ kernel use only ]
 *
 *	Mark the given range as handled by a subordinate map.
 *
 *	This range must have been created with vm_map_find using
 *	the vm_submap_object, and no other operations may have been
 *	performed on this range prior to calling vm_map_submap.
 *
 *	Only a limited number of operations can be performed
 *	within this rage after calling vm_map_submap:
 *		vm_fault
 *	[Don't try vm_map_copyin!]
 *
 *	To remove a submapping, one must first remove the
 *	range from the superior map, and then destroy the
 *	submap (if desired).  [Better yet, don't try it.]
 */
kern_return_t vm_map_submap(vm_map_t map, vm_offset_t start, vm_offset_t end, vm_map_t submap);

/*
 *	vm_map_delete:
 *
 *	Deallocates the given address range from the target
 *	map.
 *
 *	When called with a sharing map, removes pages from
 *	that region from all physical maps.
 */
kern_return_t vm_map_delete(vm_map_t map, vm_offset_t start, vm_offset_t end);
#ifdef __cplusplus
}
#endif

#define vm_map_pmap(map) ((map)->pmap)
/* Physical map associated
 * with this address map */

/*
 *	Submap object.  Must be used to create memory to be put
 *	in a submap by vm_map_submap.
 */
// extern vm_object_t vm_submap_object;

/*
 *	Wait and wakeup macros for in_transition map entries.
 */
#define vm_map_entry_wait(map, interruptible)                                                                                      \
        MACRO_BEGIN                                                                                                                \
        assert_wait((event_t) & (map)->hdr, interruptible);                                                                        \
        vm_map_unlock(map);                                                                                                        \
        thread_block((void (*)())0);                                                                                               \
        MACRO_END

#define vm_map_entry_wakeup(map) thread_wakeup((event_t) & (map)->hdr)

#endif
