#pragma once
/*-
 * Copyright (c) 1982, 1986, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)vmmeter.h	8.2 (Berkeley) 7/10/94
 */

/*
 * System wide statistics counters.
 */
struct vmmeter {
  /*
   * General system activity.
   */
  natural_t v_swtch;   /* context switches */
  natural_t v_trap;    /* calls to trap */
  natural_t v_syscall; /* calls to syscall() */
  natural_t v_intr;    /* device interrupts */
  natural_t v_soft;    /* software interrupts */
  natural_t v_faults;  /* total faults taken */
  /*
   * Virtual memory activity.
   */
  natural_t v_lookups;     /* object cache lookups */
  natural_t v_hits;        /* object cache hits */
  natural_t v_vm_faults;   /* number of address memory faults */
  natural_t v_cow_faults;  /* number of copy-on-writes */
  natural_t v_swpin;       /* swapins */
  natural_t v_swpout;      /* swapouts */
  natural_t v_pswpin;      /* pages swapped in */
  natural_t v_pswpout;     /* pages swapped out */
  natural_t v_pageins;     /* number of pageins */
  natural_t v_pageouts;    /* number of pageouts */
  natural_t v_pgpgin;      /* pages paged in */
  natural_t v_pgpgout;     /* pages paged out */
  natural_t v_intrans;     /* intransit blocking page faults */
  natural_t v_reactivated; /* number of pages reactivated from free list */
  natural_t v_rev;         /* revolutions of the hand */
  natural_t v_scan;        /* scans in page out daemon */
  natural_t v_dfree;       /* pages freed by daemon */
  natural_t v_pfree;       /* pages freed by exiting processes */
  natural_t v_zfod;        /* pages zero filled on demand */
  natural_t v_nzfod;       /* number of zfod's created */
  /*
   * Distribution of page usages.
   */
  natural_t v_page_size;       /* page size in bytes */
  natural_t v_kernel_pages;    /* number of pages in use by kernel */
  natural_t v_free_target;     /* number of pages desired free */
  natural_t v_free_min;        /* minimum number of pages desired free */
  natural_t v_free_count;      /* number of pages free */
  natural_t v_wire_count;      /* number of pages wired down */
  natural_t v_active_count;    /* number of pages active */
  natural_t v_inactive_target; /* number of pages desired inactive */
  natural_t v_inactive_count;  /* number of pages inactive */
};

extern struct vmmeter cnt;
