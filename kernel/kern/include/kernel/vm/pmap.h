/*
 * Mach Operating System
 * Copyright (c) 1991,1990,1989,1988,1987 Carnegie Mellon University.
 * Copyright (c) 1993,1994 The University of Utah and
 * the Computer Systems Laboratory (CSL).
 * All rights reserved.
 *
 * Permission to use, copy, modify and distribute this software and its
 * documentation is hereby granted, provided that both the copyright
 * notice and this permission notice appear in all copies of the
 * software, derivative works or modified versions, and any portions
 * thereof, and that both notices appear in supporting documentation.
 *
 * CARNEGIE MELLON, THE UNIVERSITY OF UTAH AND CSL ALLOW FREE USE OF
 * THIS SOFTWARE IN ITS "AS IS" CONDITION, AND DISCLAIM ANY LIABILITY
 * OF ANY KIND FOR ANY DAMAGES WHATSOEVER RESULTING FROM THE USE OF
 * THIS SOFTWARE.
 *
 * Carnegie Mellon requests users of this software to return to
 *
 *  Software Distribution Coordinator  or  Software.Distribution@CS.CMU.EDU
 *  School of Computer Science
 *  Carnegie Mellon University
 *  Pittsburgh PA 15213-3890
 *
 * any improvements or extensions that they make and grant Carnegie Mellon
 * the rights to redistribute these changes.
 */
/*
 *	File:	vm/pmap.h
 *	Author:	Avadis Tevanian, Jr.
 *	Date:	1985
 *
 *	Machine address mapping definitions -- machine-independent
 *	section.  [For machine-dependent section, see "machine/pmap.h".]
 */

#ifndef _VM_PMAP_H_
#define _VM_PMAP_H_

#if !defined(__NEED_vm_page)
#define __NEED_vm_page
#endif

#if !defined(__NEED_vm_prot)
#define __NEED_vm_prot
#endif

#include <mach/machine/pmap.h>
#include <mach/machine/vm_types.h>
#include <kernel/vm/vm_types.h>
#ifdef __cplusplus
extern "C"
{
#endif

struct modeldep_bootdata;

/*
 *	The following is a description of the interface to the
 *	machine-dependent "physical map" data structure.  The module
 *	must provide a "pmap_t" data type that represents the
 *	set of valid virtual-to-physical addresses for one user
 *	address space.  [The kernel address space is represented
 *	by a distinguished "pmap_t".]  The routines described manage
 *	this type, install and update virtual-to-physical mappings,
 *	and perform operations on physical addresses common to
 *	many address spaces.
 */

/*
 *	Routines used for initialization.
 *	There is traditionally also a pmap_bootstrap,
 *	used very early by machine-dependent code,
 *	but it is not part of the interface.
 */

vm_size_t pmap_free_pages(); /* During VM initialization,
                              * report total available/usable
                              * physical pages on the machine.
                              */

/* During VM initialization,
 * use remaining physical pages
 * to allocate page frames.
 */
void pmap_startup(vm_offset_t *startp, vm_offset_t *endp);

vm_address_t pmap_get_phys_addr(vm_page_t a_vm_page);

vm_page_t pmap_get_managed_page(vm_address_t a_physical_address);

void pmap_init(struct modeldep_bootdata *bd); /* Initialization,
                                               * after kernel runs
                                               * in virtual memory.
                                               */

#ifndef MACHINE_PAGES
/*
 *	If machine/pmap.h defines MACHINE_PAGES, it must implement
 *	the above functions.  The pmap module has complete control.
 *	Otherwise, it must implement
 *		pmap_free_pages
 *		pmap_virtual_space
 *		pmap_next_page
 *		pmap_init
 *	and vm/vm_resident.c implements pmap_steal_memory and pmap_startup
 *	using pmap_free_pages, pmap_next_page, pmap_virtual_space,
 *	and pmap_enter.  pmap_free_pages may over-estimate the number
 *	of unused physical pages, and pmap_next_page may return FALSE
 *	to indicate that there are no more unused pages to return.
 *	However, for best performance pmap_free_pages should be accurate.
 */

/* During VM initialization,
 * return the next unused
 * physical page.
 */
boolean_t pmap_next_page(vm_offset_t *addrp);

void pmap_zero_page(vm_offset_t p);

/*
 *	pmap_copy_page copies the specified (machine independent) pages.
 */
void pmap_copy_page(vm_offset_t dst, vm_offset_t src, vm_offset_t a_offset, size_t a_size);

boolean_t pmap_is_valid_paddr(vm_address_t paddr);

boolean_t pmap_vm_page_modified(vm_address_t paddr);

#endif /*MACHINE_PAGES*/

/*
 *	Routines to manage the physical map data structure.
 */

/* Map a set of physical memory pages into the direct map
 * address space. Return a pointer to where it is mapped. This
 * routine is intended to be used for mapping device memory,
 * NOT real memory.
 */
void *pmap_mapdev_uncacheable(vm_offset_t pa, size_t size);

/* Create a pmap_t. */
pmap_t pmap_create(vm_size_t size);

/* Return the kernel's pmap_t. */
#ifndef pmap_kernel
extern pmap_t pmap_kernel(void);
#endif

/* Gain and release a reference. */
void pmap_reference(pmap_t pmap);
void pmap_destroy(pmap_t pmap);

/* Enter a mapping */
void pmap_enter(pmap_t pmap, vm_offset_t va, vm_page_t a_vmp, vm_prot_t prot, boolean_t wired);

/*
 *	Routines that operate on ranges of virtual addresses.
 */

/* Remove mappings. */
void pmap_remove(pmap_t pmap, vm_offset_t sva, vm_offset_t eva);

/*
 *	Routines to set up hardware state for physical maps to be used.
 */
void pmap_activate();   /* Prepare pmap_t to run
                         * on a given processor.
                         */
void pmap_deactivate(); /* Release pmap_t from
                         * use on processor.
                         */

/*
 *	Routines that operate on physical addresses.
 */

/* Restrict access to page. */
void pmap_page_protect(vm_offset_t pa, vm_prot_t prot);

/*
 *	Routines to manage reference/modify bits based on
 *	physical addresses, simulating them if not provided
 *	by the hardware.
 */

/* Clear reference bit */
void pmap_clear_reference(vm_offset_t pa);

/* Return reference bit */
#ifndef pmap_is_referenced
boolean_t pmap_is_referenced(vm_offset_t pa);
#endif

/* Clear modify bit */
void pmap_clear_modify(vm_offset_t pa);

/* Return modify bit */
boolean_t pmap_is_modified(vm_offset_t pa);

/*
 *	Statistics routines
 */
void pmap_get_statistics(); /* Return statistics */

#ifndef pmap_resident_count
extern int pmap_resident_count();
#endif

/*
 *	Sundry required routines
 */

/* Return a virtual-to-physical
 * mapping, if possible.
 */
vm_offset_t pmap_extract(pmap_t pmap, vm_offset_t va);

boolean_t pmap_access(); /* Is virtual address valid? */

/* Perform garbage
 * collection, if any
 */
void pmap_collect(pmap_t p);

/* Specify pageability */
void pmap_change_wiring(pmap_t map, vm_offset_t v, boolean_t wired);

void pmap_free_page(vm_page_t a_vmp);

vm_page_t pmap_alloc_private_page(vm_address_t a_phys_address);
vm_page_t pmap_alloc_page();
void      pmap_wait_free_page();

#ifndef pmap_phys_address
extern vm_offset_t pmap_phys_address(); /* Transform address
                                         * returned by device
                                         * driver mapping function
                                         * to physical address
                                         * known to this module.
                                         */
#endif                                  /*pmap_phys_address*/
#ifndef pmap_phys_to_frame
extern int pmap_phys_to_frame(); /* Inverse of
                                  * pmap_phys_address,
                                  * for use by device driver
                                  * mapping function in
                                  * machine-independent
                                  * pseudo-devices.
                                  */
#endif                           /*pmap_phys_to_frame*/

/*
 *	Optional routines
 */
#ifndef pmap_copy
extern void pmap_copy(); /* Copy range of
                          * mappings, if desired.
                          */
#endif                   /*pmap_copy*/
#ifndef pmap_attribute
extern kern_return_t pmap_attribute(); /* Get/Set special
                                        * memory attributes
                                        */
#endif                                 /*pmap_attribute*/

/*
 * Routines defined as macros.
 */
#ifndef PMAP_ACTIVATE_USER
#define PMAP_ACTIVATE_USER(pmap, thread, cpu)                                                                                      \
        {                                                                                                                          \
                if ((pmap) != kernel_pmap)                                                                                         \
                        PMAP_ACTIVATE(pmap, thread, cpu);                                                                          \
        }
#endif

#ifndef PMAP_DEACTIVATE_USER
#define PMAP_DEACTIVATE_USER(pmap, thread, cpu)                                                                                    \
        {                                                                                                                          \
                if ((pmap) != kernel_pmap)                                                                                         \
                        PMAP_DEACTIVATE(pmap, thread, cpu);                                                                        \
        }
#endif

#ifndef PMAP_ACTIVATE_KERNEL
#define PMAP_ACTIVATE_KERNEL(cpu) PMAP_ACTIVATE(kernel_pmap, THREAD_NULL, cpu)
#endif

#ifndef PMAP_DEACTIVATE_KERNEL
#define PMAP_DEACTIVATE_KERNEL(cpu) PMAP_DEACTIVATE(kernel_pmap, THREAD_NULL, cpu)
#endif

#ifdef __cplusplus
}
#endif

#endif
