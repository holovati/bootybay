#include <fcntl.h>

#include <string.h>
#include <sys/sysmacros.h>

#include <aux/init.h>

#include <etl/algorithm.hh>

#include <emerixx/poll.h>
#include <emerixx/sleep.h>
#include <emerixx/memory_rw.h>
#include <emerixx/chrdev.h>
#include <emerixx/vfs/vfs_file_data.h>

#include <mach/machine/ps2.h>
#include <mach/machine/spl.h>

#include <kernel/thread_data.h>
#include <mach/conf.h>

static chrdev_data s_psaux_device;
static integer_t s_psaux_open_cnt;
static poll_device_context_t s_psaux_pollfds;

// ********************************************************************************************************************************
static kern_return_t psaux_open(vfs_node_t, vfs_file_t a_file)
// ********************************************************************************************************************************
{
        if (s_psaux_open_cnt == 0)
        {
                int s = spltty();
                ps2_disable_auxilary();
                ps2_enable_auxilary();

                ps2_aux_cmd(PS2_MOUSE_COMMAND_RESET);
                ps2_device_output(); // ACK 0xfa
                ps2_device_output(); // self test AA
                ps2_device_output(); // mouse id 00

                ps2_aux_cmd(PS2_MOUSE_COMMAND_ENABLE_DATA_REPORTING);
                ps2_device_output(); // ACK
                splx(s);

                s_psaux_open_cnt++;
        }
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t psaux_read(vfs_file_t a_vfsf, memory_rdwr_t a_memrw)
// ********************************************************************************************************************************
{
        kern_return_t retval = KERN_SUCCESS;
        uint8_t mousebytes[0x10];
        int s = spltty();
        while (KERN_STATUS_SUCCESS(retval))
        {
                size_t nbytes_read = 0;
                size_t read_count  = etl::min(a_memrw->uio_resid, sizeof(mousebytes));

                for (int spin = 1000; spin && nbytes_read < read_count; spin--)
                {
                        if (ps2_aux_output_full())
                        {
                                mousebytes[nbytes_read++] = ps2_device_output();

                                spin = 1000;
                        }
                }

                if ((a_vfsf->m_oflags & O_NONBLOCK) && nbytes_read == 0)
                {
                        retval = KERN_FAIL(EWOULDBLOCK);
                        continue;
                }

                if (nbytes_read == 0)
                {
                        retval = uthread_sleep((event_t)psaux_read);
                        continue;
                }

                retval = uiomove(mousebytes, nbytes_read, a_memrw);
                break;
        }
        splx(s);
        return retval;
}

// ********************************************************************************************************************************
static kern_return_t psaux_write(vfs_file_t a_vfsf, memory_rdwr_t a_memrw)
// ********************************************************************************************************************************
{
        panic("Not ready");
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t psaux_close(vfs_file_t a_file)
// ********************************************************************************************************************************
{
        s_psaux_open_cnt--;
        if (s_psaux_open_cnt == 0)
        {
                int s = spltty();
                ps2_disable_auxilary();
                // Drain, or else keyboard wont work
                for (int spin = 1000; spin; spin--)
                {
                        if (ps2_aux_output_full())
                        {
                                ps2_device_output();
                                spin = 1000;
                        }
                }
                splx(s);
        }
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t psaux_poll(vfs_file_t a_file, struct pollfd **a_pfd)
// ********************************************************************************************************************************
{
        if ((*a_pfd)->events & (POLLIN | POLLRDNORM))
        {
                if (ps2_aux_output_full())
                {
                        (*a_pfd)->revents |= (short)((*a_pfd)->events & (POLLIN | POLLRDNORM));
                }
        }

        if ((*a_pfd)->events & (POLLOUT | POLLWRNORM))
        {
                if (ps2_ctrl_input_full() == 0)
                {
                        (*a_pfd)->revents |= (short)((*a_pfd)->events & (POLLOUT | POLLWRNORM));
                }
        }
        return poll_record(&s_psaux_pollfds, a_pfd);
}

static struct vfs_file_operations_data s_psaux_fops = {
        .fopen  = psaux_open,               //
        .fread  = psaux_read,               //
        .fwrite = psaux_write,              //
        .fioctl = vfs_file_op_ioctl_enotty, //
        .fclose = psaux_close,              //
        .fpoll  = psaux_poll,               //
};

// ********************************************************************************************************************************
static void ps2_mouse_intr()
// ********************************************************************************************************************************
{
        int s = spltty();
        if (ps2_aux_output_full())
        {
                uthread_wakeup((event_t)psaux_read);
                poll_wakeup(&s_psaux_pollfds, POLLIN | POLLRDNORM);
        }

        assert_wait((event_t)ps2_aux_output_full, true);
        splx(s);
        thread_block(ps2_mouse_intr);
}

// ********************************************************************************************************************************
static kern_return_t psaux_init()
// ********************************************************************************************************************************
{
        poll_device_ctx_init(&s_psaux_pollfds);

        kernel_thread(kernel_task, ps2_mouse_intr, nullptr);

        s_psaux_open_cnt     = 0;
        kern_return_t retval = driver_chrdev_alloc(&s_psaux_device, "psaux", &s_psaux_fops, makedev(16, 0), 1);
        return retval;
}

device_initcall(psaux_init);
