#pragma once

/* Lifted from OpenBSD - Public domain */

#if defined(__amd64__)
#define __iomem volatile __attribute__((space(iomem)))
#define __membar(_f)                                                                                                               \
        do                                                                                                                         \
        {                                                                                                                          \
                __asm__ volatile(_f ::: "memory");                                                                                 \
        } while (0)

#define rmb() __membar("lfence")
#define wmb() __membar("sfence")
#define mb()  __membar("mfence")

#define lemtoh16(x) (*((uint16_t const volatile *)(x)))
#define lemtoh32(x) (*((uint32_t const volatile *)(x)))
#define lemtoh64(x) (*((uint64_t const volatile *)(x)))

#define htolem16(x, v)                                                                                                             \
        do                                                                                                                         \
        {                                                                                                                          \
                (*((uint16_t volatile *)(x))) = ((uint16_t)(v));                                                                   \
        } while (0)

#define htolem32(x, v)                                                                                                             \
        do                                                                                                                         \
        {                                                                                                                          \
                (*((uint32_t volatile *)(x))) = ((uint32_t)(v));                                                                   \
        } while (0)

#define htolem64(x, v)                                                                                                             \
        do                                                                                                                         \
        {                                                                                                                          \
                (*((uint64_t volatile *)(x))) = ((uint64_t)(v));                                                                   \
        } while (0)

#else
#error Missing
#endif

#define iobarrier() __asm__ volatile("" : : : "memory")

typedef void __iomem *ioaddr_t;

static inline uint8_t ioread8(const void __iomem *addr)
{
        uint8_t val;

        iobarrier();
        val = *(volatile uint8_t const *)addr;
        rmb();
        return val;
}

static inline void iowrite8(u8 val, void __iomem *addr)
{
        wmb();
        *(volatile uint8_t *)addr = val;
}

static inline uint16_t ioread16(const void __iomem *addr)
{
        uint16_t val;

        iobarrier();
        val = lemtoh16(addr);
        rmb();
        return val;
}

static inline uint32_t ioread32(const void __iomem *addr)
{
        uint32_t val;

        iobarrier();
        val = lemtoh32(addr);
        rmb();
        return val;
}

static inline uint64_t ioread64(const void __iomem *addr)
{
        uint64_t val;

        iobarrier();
        val = lemtoh64(addr);
        rmb();
        return val;
}

static inline void iowrite16(uint16_t val, void __iomem *addr)
{
        wmb();
        htolem16(addr, val);
}

static inline void iowrite32(uint32_t val, void __iomem *addr)
{
        wmb();
        htolem32(addr, val);
}

static inline void iowrite64(uint64_t val, void __iomem *addr)
{
        wmb();
        htolem64(addr, val);
}

#define readb(p)     ioread8(p)
#define writeb(v, p) iowrite8(v, p)
#define readw(p)     ioread16(p)
#define writew(v, p) iowrite16(v, p)
#define readl(p)     ioread32(p)
#define writel(v, p) iowrite32(v, p)
#define readq(p)     ioread64(p)
#define writeq(v, p) iowrite64(v, p)