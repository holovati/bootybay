#pragma once
/* Public domain. */

typedef int irqreturn_t;

#if defined(__cplusplus)
enum irqreturn : int
#else
enum irqreturn
#endif
{
        IRQ_NONE    = 0,
        IRQ_HANDLED = 1
};
