#pragma once

#include <emerixx/irqreturn.h>

#define IRQF_SHARED 0

typedef irqreturn_t (*irq_handler_t)(int, void *);

#if defined(__cplusplus)
extern "C" {
#endif

int request_irq(int irq, irq_handler_t handler, unsigned long flags, const char *name, void *dev);

#if defined(__cplusplus)
}
#endif