#include <sys/sysmacros.h>

#include <aux/init.h>

#include <emerixx/ioctl_req.h>

#include <emerixx/disklabel.h>
#include <emerixx/pci_regs.h>

#include <etl/bit.hh>
#include <etl/algorithm.hh>

#include <kernel/sched_prim.h>
#include <kernel/vm/pmap.h>
#include <kernel/vm/vm_kern.h>
#include <kernel/vm/vm_page.h>
#include <kernel/vm/vm_param.h>
#include <mach/conf.h>

#include <mach/machine/bus/pci/pci_cfgreg.h>
#include <mach/machine/spl.h>
#include <mach/param.h>

#include <string.h>

#include "atapi.h"

template <unsigned... i>
using u32bit = etl::bit<u32, i...>;

#define ABAR_INDEX 0x05

#define AHCI_FIS_REGISTER_H2D        0x27 // Register FIS - Host to Device
#define AHCI_FIS_REGISTER_H2D_LENGTH 20
#define AHCI_FIS_REGISTER_D2H        0x34 // Register FIS - Device to Host
#define AHCI_FIS_PIO_SETUP           0x5F // PIO Setup FIS - Device to Host

#define AHCI_D2H_FIS_OFFSET 0x40
#define AHCI_PIO_FIS_OFFSET 0x20
#define AHCI_FIS_TYPE_MASK  0xFF

#define AHCI_CAPABILITY_OFFSET 0x0000
#define AHCI_S64A              u32bit<31>::value
#define AHCI_CAP_SAM           u32bit<18>::value
#define AHCI_CAP_SSS           u32bit<27>::value
#define AHCI_CAP_SIS           u32bit<27>::value
#define AHCI_CAP_NCS(capreg)   ((((capreg)&0x0f00) >> 8) + 1)
#define AHCI_CAP_NP(capreg)    ((((capreg)&0x0f)) + 1)

#define AHCI_GHC_OFFSET 0x0004
#define AHCI_GHC_HR     u32bit<0>::value
#define AHCI_GHC_IE     u32bit<1>::value
#define AHCI_GHC_AE     u32bit<31>::value

#define AHCI_IS_OFFSET 0x0008

#define AHCI_PI_OFFSET 0x000C

#define AHCI_VS_OFFSET 0x0010

#define AHCI_BOHC_OFFSET 0x0028
#define AHCI_BOHC_BOS    u32bit<0>::value /* BIOS Owned Semaphore */
#define AHCI_BOHC_OOS    u32bit<1>::value /* OS Owned Semaphore */
#define AHCI_BOHC_SOOE   u32bit<2>::value /* SMI on OS Ownership Change Enable */
#define AHCI_BOHC_OOC    u32bit<3>::value /* OS Ownership Change */
#define AHCI_BOHC_BB     u32bit<4>::value /* BIOS Busy */

#define AHCI_MAX_PORTS     32
#define AHCI_MAX_CMD_SLOTS 32

#define AHCI_PORT_RFIS_SIZE     0x100
#define AHCI_PORT_CMD_HDR_SIZE  0x20
#define AHCI_PORT_CMD_TBL_SIZE  0x80
#define AHCI_PORT_CMD_TBL_PRDTL 0x08
#define AHCI_PORT_PRD_ITEM_SIZE 0x10

#define AHCI_PxCLB  0x00 /* Port x Command List Base Address */
#define AHCI_PxCLBU 0x04 /* Port x Command List Base Address Upper 32-Bits */

#define AHCI_PxFB 0x08 /* Port x FIS Base Address */

#define AHCI_PxFBU 0x0C /* Port x FIS Base Address Upper 32-Bits */

#define AHCI_PxIS      0x10              /* Port x Interrupt Status */
#define AHCI_PxIS_DHRS u32bit<0>::value  /* Device to Host Register FIS Interrupt */
#define AHCI_PxIS_IFS  u32bit<27>::value /* Host Bus Data Error Status (HBDS) */
#define AHCI_PxIS_HBDS u32bit<28>::value /* Interface Fatal Error Status (IFS) */
#define AHCI_PxIS_HBFS u32bit<29>::value /* Host Bus Fatal Error Status (HBFS) */
#define AHCI_PxIS_TFES u32bit<30>::value /* Task File Error Status (TFES) */
#define AHCI_PxIS_CPDS u32bit<31>::value /* Cold Port Detect Status (CPDS)*/

#define AHCI_PxIE     0x14 /* Port x Interrupt Enable */
#define AHCI_PxIE_DPE (u32bit<5>::value)

#define AHCI_PxCMD     0x18              /* Port x Command and Status */
#define AHCI_PxCMD_SUD u32bit<1>::value  /* Spin-Up Device */
#define AHCI_PxCMD_POD u32bit<2>::value  /* Power On Device */
#define AHCI_PxCMD_CPD u32bit<20>::value /* Cold Presence Detection */
#define AHCI_PxCMD_FRE u32bit<4>::value  /* FIS Receive Enable (FRE) */
#define AHCI_PxCMD_ST  u32bit<0>::value  /* Start (ST) */

#define AHCI_PxTFD      0x20 /* Port x Task File Data */
#define AHCI_PxTFD_ERR  u32bit<0>::value
#define AHCI_PxTFD_DRQ  u32bit<3>::value
#define AHCI_PxTFD_BSY  u32bit<7>::value
#define AHCI_PxTFD_MASK (AHCI_PxTFD_BSY | AHCI_PxTFD_DRQ | AHCI_PxTFD_ERR)

#define AHCI_ATAPI_SIG_MASK 0xFFFF0000
#define AHCI_ATA_DEVICE_SIG 0x00000000

#define AHCI_PxSIG 0x24 /* Port x Signature */

#define AHCI_PxSSTS          0x28 /* Port x Serial ATA Status (SCR0: SStatus) */
#define AHCI_PxSSTS_DET_MASK (u32bit<3, 2, 1, 0>::value)
#define AHCI_PxSSTS_DET_PNCE 0x01
#define AHCI_PxSSTS_DET_PCE  0x03

#define AHCI_PxSCTL             0x2C /* Port x Serial ATA Control (SCR2: SControl) */
#define AHCI_PxSCTL_IPM_DISABLE 0x00000300

#define AHCI_PxSERR 0x30 /* Port x Serial ATA Error (SCR1: SError) */

#define AHCI_PxSACT 0x34 /* Port x Serial ATA Active (SCR3: SActive) */

#define AHCI_PxCI 0x38 /* Port x Command Issue */

#define AHCI_PxSNTF 0x3C /* Port x Serial ATA Notification (SCR4: SNotifi) */

#define AHCI_PxFBS 0x40 /* Port x FIS-based Switching Control */

#define AHCI_PxVS 0x70 /* Port x Vendor Specific */

/* Serial ata ahci spec 1.3 chapter 3.3 */
#define AHCI_PORT_REG_OFFSET(port_idx) (0x100u + (port_idx * 0x80))

#define AHCI_ADD_PTR(type, start, offset) ((type)(((vm_offset_t)(start)) + ((vm_offset_t)(offset))))

#define AHCI_RFIS_SIZE(max_ports) ((size_t)(round_page(sizeof(struct ahci_rfis) * (max_ports))))

#define AHCI_CMD_LIST_SIZE(max_ports) ((size_t)(sizeof(struct ahci_cmd_list_ent) * (max_ports) * (AHCI_MAX_CMD_SLOTS)))

#define AHCI_CMD_TBL_SIZE(max_ports, max_cmds) ((size_t)(sizeof(struct ahci_cmd_table) * (max_ports) * (max_cmds)))

#define AHCI_PARTMASK  0x7
#define AHCI_PARTSHIFT 6

#define AHCI_GETPORT(dev) ((minor(dev)) >> AHCI_PARTSHIFT)
#define AHCI_GETPART(d)   ((minor(d) & AHCI_PARTMASK) & 0x7)

typedef struct ahci_rfis *ahci_rfis_t;
typedef struct ahci_cmd_list_ent *ahci_cmd_list_t;
typedef struct ahci_hba *ahci_hba_t;
typedef struct ahci_port *ahci_port_t;
typedef struct ahci_cfis *ahci_cfis_t;
typedef struct ahci_atapi_cmd *ahci_atapi_cmd_t;
typedef struct ahci_cmd_prdt *ahci_prdt_t;
typedef struct ahci_cmd_table *ahci_cmd_table_t;

struct ahci_port
{
        ahci_rfis_t rfis;
        ahci_cmd_list_t cmdlst;
        ahci_cmd_table_t cmdtbl;
        u32 volatile cmd_bitmap;
        u32 pad;
        ata_identify_data_t idata;
        vm_page_t vm_pages[AHCI_MAX_CMD_SLOTS];
        struct disklabel label;
};

struct ahci_hba
{
        vm_offset_t bar;
        pci_vendor_id_t ven;
        pci_product_id_t dev;
        u32 ncs;
        u32 pi;
        u32 np;
        ahci_rfis_t rfis;
        ahci_cmd_list_t cmd_list;
        ahci_cmd_table_t cmd_table;
        ata_identify_data_t idata;
        struct ahci_port ports[AHCI_MAX_PORTS];
};

/* structs lifted from intel edk2 */
#pragma pack(1)
// clang-format off
//
// Received FIS structure
//
struct ahci_rfis {
  u8 dma_setup_fis[0x1c];	// Dma Setup Fis: offset 0x00
  u8 dma_setupfis_rsvd[0x04];
  u8 pio_setup_fis[0x14];	// Pio Setup Fis: offset 0x20
  u8 pio_setup_fis_rsvd[0x0c];
  u8 d2h_register_fis[0x14];	// D2H Register Fis: offset 0x40
  u8 d2h_register_fis_rsvd[0x04];
  u64 set_device_bits_fis;	// Set Device Bits Fix: offset 0x58
  u8 unknown_fis[0x40];		// Unknown Fis: offset 0x60
  u8 unknown_fis_rsvd[0x60];
};

//
// Command List structure includes total 32 entries.
// The entry Data structure is listed at the following.
//
struct ahci_cmd_list_ent {
  u32    cmd_cfl:5;      //Command FIS Length
  u32    cmd_a:1;        //ATAPI
  u32    cmd_w:1;        //Write
  u32    cmd_p:1;        //Prefetchable
  u32    cmd_r:1;        //Reset
  u32    cmd_b:1;        //BIST
  u32    cmd_c:1;        //Clear Busy upon R_OK
  u32    cmd_rsvd:1;
  u32    cmd_pmp:4;      //Port Multiplier Port
  u32    cmd_prdtl:16;   //Physical Region Descriptor Table Length
  u32    cmd_prdbc;      //Physical Region Descriptor Byte Count
  u32    cmd_ctba;       //Command Table Descriptor Base Address
  u32    cmd_ctbau;      //Command Table Descriptor Base Address Upper 32-BITs
  u32    cmd_rsvd1[4];
};

//
// This is a software constructed FIS.
// For Data transfer operations, this is the H2D Register FIS format as
// specified in the Serial ATA Revision 2.6 specification.
struct ahci_cfis {
  // DWORD 0
  u8     type;
  u8     pmnum:4;
  u8     rsvd:1;
  u8     rsvd1:1;
  u8     rsvd2:1;
  u8     cmdind:1;
  u8     cmd;
  u8     feature;
  // DWORD 1
  u8     lba_lo;
  u8     lba_mid;
  u8     lba_hi;
  u8     devhead;
  // DWORD 2
  u8     lba_exp_lo;
  u8     lba_exp_mid;
  u8     lba_exp_hi;
  u8     featureexp;
  // DWORD 3
  u8     seccount;
  u8     seccountexp;
  u8     rsvd3;
  u8     control;
  // DWORD 4
  u8     rsvd4[4];
  // DWORD 5 +
  u8     rsvd5[44];
};

//
// ACMD: ATAPI command (12 or 16 bytes)
//
struct ahci_atapi_cmd {
  u8    cmd[0x10];
};

//
// Physical Region Descriptor Table includes up to 65535 entries
// The entry data structure is listed at the following.
// the actual entry number comes from the PRDTL field in the command
// list entry for this command slot.
//
struct ahci_cmd_prdt {
  u32    dba;       //Data Base Address
  u32    dbau;      //Data Base Address Upper 32-BITs
  u32    rsvd;
  u32    dbc:22;    //Data Byte Count
  u32    rsvd1:9;
  u32    ioc:1;     //Interrupt on Completion
};

//
// Command table Data structure which is pointed to by the entry in the command list
//
struct ahci_cmd_table {
  struct ahci_cfis      cfis;       // A software constructed FIS.
  struct ahci_atapi_cmd atapi_cmd;         // 12 or 16 bytes ATAPI cmd.
  u8                     rsvd[0x30];
  //
  // The scatter/gather list for Data transfer.
  //
  struct ahci_cmd_prdt prdtl[AHCI_PORT_CMD_TBL_PRDTL];
};
// clang-format on
#pragma pack()

static inline u32 ahci_hba_rd_reg(ahci_hba_t hba, size_t offset)
{
        return (*((u32 volatile *)(hba->bar + offset)));
}

static inline void ahci_hba_wr_reg(ahci_hba_t hba, size_t offset, u32 value)
{
        (*((u32 volatile *)(hba->bar + offset))) = value;
}

static inline void ahci_hba_or_reg(ahci_hba_t hba, size_t offset, u32 value)
{
        (*((u32 volatile *)(hba->bar + offset))) |= value;
}

static inline void ahci_hba_and_reg(ahci_hba_t hba, size_t offset, u32 value)
{
        (*((u32 volatile *)(hba->bar + offset))) &= value;
}

static u32 ahci_port_rd_reg(ahci_hba_t hba, size_t port_idx, size_t offset)
{
        return ahci_hba_rd_reg(hba, AHCI_PORT_REG_OFFSET(port_idx) + offset);
}

static void ahci_port_wr_reg(ahci_hba_t hba, size_t port_idx, size_t offset, u32 value)
{
        ahci_hba_wr_reg(hba, AHCI_PORT_REG_OFFSET(port_idx) + offset, value);
}

static void ahci_port_or_reg(ahci_hba_t hba, size_t port_idx, size_t offset, u32 value)
{
        ahci_hba_or_reg(hba, AHCI_PORT_REG_OFFSET(port_idx) + offset, value);
}

static void ahci_port_and_reg(ahci_hba_t hba, size_t port_idx, size_t offset, u32 value)
{
        ahci_hba_and_reg(hba, AHCI_PORT_REG_OFFSET(port_idx) + offset, value);
}

static vm_offset_t ahci_extract_phys_addr(vm_offset_t va)
{
        vm_offset_t cmd_table_page     = pmap_extract(kernel_pmap, trunc_page(va));
        vm_offset_t cmd_table_page_off = va & PAGE_MASK;
        return (cmd_table_page + cmd_table_page_off);
}

extern int hz;
static void ahci_delay(u64 usec)
{
        assert_wait(nullptr, false);
        thread_set_timeout(((int)usec * hz) / 1000);
        thread_block(nullptr);
}
#if 0 // Unused
static void ahci_hba_mmio_wait_set(ahci_hba_t hba, size_t offset, u32 value)
{
        while ((ahci_hba_rd_reg(hba, offset) & value) == 0)
                ;
}
#endif
static int ahci_port_mmio_wait(ahci_hba_t hba, size_t port_idx, size_t offset, u32 mask, u32 value, int timeout)
{
        while (timeout-- && ((ahci_port_rd_reg(hba, port_idx, offset) & mask) != value))
        {
                ahci_delay(1);
        }
        return timeout > 0;
}

static boolean_t ahci_hba_mmio_wait(ahci_hba_t hba, size_t offset, u32 mask, u32 value, int timeout)
{
        while (timeout-- && ((ahci_hba_rd_reg(hba, offset) & mask) != value))
        {
                ahci_delay(1);
        }

        return timeout > 0;
}

static size_t atapi_capacity(ata_identify_data_t idata)
{
        if ((idata->command_set_supported_83 & etl::bit<decltype(idata->command_set_supported_83), 10>::value) == 0)
        {
                //
                // The device doesn't support 48 bit addressing
                //
                return 0;
        }

        //
        // 48 bit address feature set is supported, get maximum capacity
        //
        size_t cap    = 0;
        size_t tmplba = 0;
        for (size_t idx = 0; idx < 4; idx++)
        {
                //
                // Lower byte goes first: word[100] is the lowest word, word[103] is highest
                //
                tmplba = idata->maximum_lba_for_48bit_addressing[idx];
                cap |= (tmplba << (16 * idx));
        }

        return cap;
}

// Buffers that did not get a command slot at strategy. To be picked up in the
// interrupt handler.
static vm_page_t s_next_vmp;
static vm_page_t *s_next_vmp_last_ptr;

// Find a command slot for a given port. Must be called at splbio
static boolean_t ahci_port_find_cmd_slot(ahci_hba_t a_hba, u32 a_portno, u32 *a_cmdnum_out)
{
        boolean_t found = FALSE;

        for (u32 cmd_idx = 0; cmd_idx < a_hba->ncs; cmd_idx++)
        {
                u32 cmd_bit = (1u << cmd_idx);
                if ((a_hba->ports[a_portno].cmd_bitmap & cmd_bit) == 0)
                {
                        *a_cmdnum_out = cmd_idx;
                        a_hba->ports[a_portno].cmd_bitmap |= cmd_bit;
                        found = TRUE;
                        break;
                }
        }

        return found;
}

static int vmp_get_seccount(vm_page_t a_vmp)
{

        natural_t i;
        for (i = a_vmp->m_io_descr->m_block_sel__; i < ARRAY_SIZE(a_vmp->m_io_descr->m_lba); ++i)
        {
                if (a_vmp->m_io_descr->m_lba[i] != 0xffffffffffffffff)
                {
                        break;
                }
                a_vmp->m_io_descr->m_block_sel__ = (i + 1) & 0x7;
        }

        if (i == ARRAY_SIZE(a_vmp->m_io_descr->m_lba))
        {
                // Reached end, everything from blocksel to end is -1
                return 0;
        }

        if (i == (ARRAY_SIZE(a_vmp->m_io_descr->m_lba) - 1))
        {
                // This last element is not -1
                return 1;
        }

        int count = 1;

        for (; i < (ARRAY_SIZE(a_vmp->m_io_descr->m_lba) - 1); ++i)
        {
                if (a_vmp->m_io_descr->m_lba[i + 1] != (a_vmp->m_io_descr->m_lba[i] + 1))
                {
                        break;
                }

                count++;
        }

        return count;
}

static int ahci_port_post_buffer3(ahci_hba_t a_hba, int a_minor, vm_page_t a_vmp, u32 a_portno, u32 a_cmdno, u32 a_seccount)
{
        // u32 partno       = minor(a_io_desc->get_device_id()) - 1;
        ahci_port_t port = &(a_hba->ports[a_portno]);

        KASSERT(port->vm_pages[a_cmdno] == NULL);

        partitionp_t part       = &port->label.part[a_minor];
        ahci_cmd_list_t cmdlst  = port->cmdlst;
        ahci_cmd_table_t cmdtbl = port->cmdtbl;
        ahci_cfis_t cfis        = &cmdtbl[a_cmdno].cfis;

        // vm_offset_t b_paddr = a_io_desc->get_phys_address();
        // size_t block_size   = a_io_desc->get_block_size();

        // b_paddr             = pmap_get_phys_addr(buf_get_vm_page(bp));
        // int rd = (a_io_desc->m_flags & IO_DESCRIPTOR_READ);

        /* Read or write */
        cmdlst[a_cmdno].cmd_w = !!a_vmp->m_io_descr->m_write;

        cfis->cmdind = 1;
        cfis->type   = AHCI_FIS_REGISTER_H2D;
        cfis->cmd    = cmdlst[a_cmdno].cmd_w ? ATA_CMD_WRITE_DMA_EXT : ATA_CMD_READ_DMA_EXT;

        /* Always 1 sector, maybe set during init and don't touch? */
        /* No, bio decides, just remember to save how much data the hba can transfer
         * and check it */
        // KASSERT((a_bh->m_block_size & DEV_BMASK) == 0);
        // cfis->seccount = (u8)(block_size >> DEV_BSHIFT);
        // cfis->seccount = a_vmp->m_io_descr->m_dev_blocks[a_vmp->m_io_descr->m_block_sel].nsect;
        cfis->seccount = a_seccount & 0xFF; // a_vmp->m_io_descr->m_dev_blocks[port->vm_page_blocksel[a_cmdno]].nsect;

        // ACS PG 178.
        cfis->devhead = 1 << 6;

        /* Add startlba as an offset for the partition */
        // size_t lba = a_io_desc->m_lba_offset;
        // lba += (a_io_desc->m_phys_block_address >> DEV_BSHIFT);
        natural_t lba = part->p_offset + a_vmp->m_io_descr->m_lba[a_vmp->m_io_descr->m_block_sel__];

        cfis->lba_mid     = (u8)(lba >> 0x08 & 0xFF);
        cfis->lba_hi      = (u8)(lba >> 0x10 & 0xFF);
        cfis->lba_lo      = (u8)(lba >> 0x00 & 0xFF);
        cfis->lba_exp_lo  = (u8)(lba >> 0x18 & 0xFF);
        cfis->lba_exp_mid = (u8)(lba >> 0x20 & 0xFF);
        cfis->lba_exp_hi  = (u8)(lba >> 0x28 & 0xFF);

        vm_address_t phyaddr          = a_vmp->phys_addr + (a_vmp->m_io_descr->m_block_sel__ << DEV_BSHIFT);
        cmdtbl[a_cmdno].prdtl[0].dba  = (u32)(phyaddr & 0xFFFFFFFFu);
        cmdtbl[a_cmdno].prdtl[0].dbau = (u32)(phyaddr >> 32);
        cmdtbl[a_cmdno].prdtl[0].dbc  = (u32)((cfis->seccount << DEV_BSHIFT) - 1) & 0x1fffff;
        cmdtbl[a_cmdno].prdtl[0].ioc  = 1;

        /* Always clear the amount transfered before clearing IS */
        port->cmdlst[a_cmdno].cmd_prdbc = 0;

        port->vm_pages[a_cmdno] = a_vmp;

        // ahci_port_or_reg(&tmp_hba, portno, AHCI_PxSACT, 1u << cmdno);
        ahci_port_or_reg(a_hba, a_portno, AHCI_PxCI, 1u << a_cmdno);

        return 0; // bp->b_error;
}

static int ata_open(dev_t dev, int rw, int type);
static int ata_close(dev_t dev);
// static int ata_strategy(struct buf *bp);

// static kern_return_t ata_strategy(dev_t a_dev, buffer_cache::io_descriptor *);
static kern_return_t ata_strategy3(dev_t a_dev, vm_page_t a_vmp);

static int ata_ioctl(dev_t dev, ioctl_req_t cmd, int fflag);
// static struct buf ata_buf;

struct bdevsw atasw = {
        .d_open      = ata_open,
        .d_close     = ata_close,
        .d_strategy  = nullptr,
        .d_strategy2 = nullptr,
        .d_strategy3 = ata_strategy3,
        .d_ioctl     = ata_ioctl,
        //.d_tab      = &ata_buf
        /* END */
};

static struct ahci_hba tmp_hba;

static int ata_fatal_err(ahci_hba_t hbap, u32 p)
{
        // ahci_port_t pp = &hbap->ports[p];
        u32 psact = ahci_port_rd_reg(hbap, p, AHCI_PxSACT);
        u32 pserr = ahci_port_rd_reg(hbap, p, AHCI_PxSERR);
        u32 cmd   = ahci_port_rd_reg(hbap, p, AHCI_PxCMD);
        panic("AHCI FATAL ERROR\n");

        return (int)(psact + pserr + cmd);
}

static int ata_intr3()
{
        for (u32 p = 0; p < tmp_hba.np; ++p)
        {
                /*
                 * If set, indicates that the corresponding port has an
                 * interrupt pending. Software can use this information to determine which
                 * ports require service after an interrupt.
                 */
                u32 ips = ahci_hba_rd_reg(&tmp_hba, AHCI_IS_OFFSET);

                u32 pb = (ips & (1u << p));

                if (!pb)
                {
                        continue;
                }

                /* Port p Interrupt Status */
                u32 pxis = ahci_port_rd_reg(&tmp_hba, p, AHCI_PxIS);
                ahci_port_wr_reg(&tmp_hba, p, AHCI_PxIS, pxis);

                /* Clear the interrupt bit in IS.IPS for port p (RWC) */
                ahci_hba_wr_reg(&tmp_hba, AHCI_IS_OFFSET, pb);

                if (pxis & (AHCI_PxIS_HBFS | AHCI_PxIS_HBDS | AHCI_PxIS_IFS | AHCI_PxIS_TFES))
                {
                        ata_fatal_err(&tmp_hba, p);
                }

#define AHCI_PxIS_UFS u32bit<4>::value
                if (pxis & AHCI_PxIS_UFS)
                {
                        u32 pserr = ahci_port_rd_reg(&tmp_hba, p, AHCI_PxSERR);
                        if (pserr)
                        {
                                panic("Read pg 23 on how to handle this");
                        }
                        else
                        {
                                pxis &= (u32)(~AHCI_PxIS_UFS);
                        }
                }

#define AHCI_PxIS_DPS u32bit<5>::value
#define AHCI_PxIS_DSS u32bit<2>::value
#define AHCI_PxIS_PSS u32bit<1>::value
                if (pxis & ~(AHCI_PxIS_DHRS | AHCI_PxIS_PSS | AHCI_PxIS_DSS | AHCI_PxIS_DPS))
                {
                        // log_info("PxIE %08X", ahci_port_rd_reg(&tmp_hba, p, AHCI_PxIE));

                        // log_info("Transferred bytes %d", tmp_hba.ports[p].cmdlst->cmd_prdbc);
                        panic("Unhandled interrupt status on port %d PxIS %08X, PxSERR %08X",
                              p,
                              pxis,
                              ahci_port_rd_reg(&tmp_hba, p, AHCI_PxSERR));
                }
                if (pxis & AHCI_PxIS_DHRS)
                {
                        /*
                         * Clear appropriate bits in the PxIS register corresponding to the
                         * cause of the interrupt. (RWC)
                         */
                        u32 ci_act = ahci_port_rd_reg(&tmp_hba, p, AHCI_PxCI) | ahci_port_rd_reg(&tmp_hba, p, AHCI_PxSACT);
                        for (u32 cmd_idx = 0; cmd_idx < tmp_hba.ncs; ++cmd_idx)
                        {
                                u32 cmd_bit = (1u << cmd_idx);
                                if ((tmp_hba.ports[p].cmd_bitmap & cmd_bit) && ((ci_act & cmd_bit) == 0))
                                {
                                        // buffer_cache::io_descriptor *io_desc = tmp_hba.ports[p].block_headers[cmd_idx];
                                        vm_page_t vmp = tmp_hba.ports[p].vm_pages[cmd_idx];
                                        KASSERT(vmp);
                                        vm_page_io_descr_t vmpiod = vmp->m_io_descr;

                                        size_t block_size = (tmp_hba.cmd_table[cmd_idx].cfis.seccount << DEV_BSHIFT);

                                        if (block_size != tmp_hba.ports[p].cmdlst[cmd_idx].cmd_prdbc)
                                        {
                                                // log_info("Unhandled interrupt status on port %d PxIS %08X, PxSERR
                                                // %08X",
                                                //         p,
                                                //        pxis,
                                                //         ahci_port_rd_reg(&tmp_hba, p, AHCI_PxSERR));
                                                // log_info("%08X cmd_prdbc %08X b_count %08X",
                                                //         p,
                                                //         tmp_hba.ports[p].cmdlst->cmd_prdbc,
                                                //         buf_get_bcount(tmp_hba.ports[p].bufs[0]));
                                                size_t b1 = (block_size);
                                                u32 b2    = tmp_hba.ports[p].cmdlst[cmd_idx].cmd_prdbc;
                                                log_error("cmd_prdbc != b_count %08x, %08x", b2, b1);
                                                u32 sact = ahci_port_rd_reg(&tmp_hba, p, AHCI_PxSACT);
                                                u32 serr = ahci_port_rd_reg(&tmp_hba, p, AHCI_PxSERR);
                                                u32 cmd  = ahci_port_rd_reg(&tmp_hba, p, AHCI_PxCMD);

                                                log_error("SACT 0x%08X", sact);
                                                log_error("SERR 0x%08X", serr);
                                                // log_error("CI 0x%08X", ci);
                                                log_error("CI_bmp 0x%08X 0x%08X", tmp_hba.ports[p].cmd_bitmap, cmd_bit);
                                                log_error("CMD 0x%08X", cmd);

                                                while (1)
                                                        ;
                                        }

                                        // Remove the bit from our list
                                        tmp_hba.ports[p].cmd_bitmap &= ~cmd_bit;
                                        tmp_hba.ports[p].vm_pages[cmd_idx] = nullptr;

                                        // io_desc->io_done(KERN_SUCCESS);

                                        vmp->m_io_descr->m_block_sel__
                                                = (vmp->m_io_descr->m_block_sel__ + tmp_hba.cmd_table[cmd_idx].cfis.seccount) & 0x7;

                                        if (vmp->m_io_descr->m_block_sel__ && vmp_get_seccount(vmp))
                                        {
                                                // We have more work todo in order to fill the whole page.
                                                // The vmpiod->m_block_sel counter has to wrap to zero when
                                                // vmpiod->m_dev_blocks[vmpiod->m_block_sel].nsect is added to
                                                // vmpiod->m_block_sel.

                                                // Requeue the page for more IO
                                                // Put it infron of the queue
                                                if (&s_next_vmp == s_next_vmp_last_ptr)
                                                {
                                                        // the queue is empty
                                                        *s_next_vmp_last_ptr = vmp;
                                                        s_next_vmp_last_ptr  = &vmp->m_io_descr->m_next;
                                                }
                                                else
                                                {
                                                        // enq in front
                                                        vmpiod->m_next = s_next_vmp;
                                                        s_next_vmp     = vmp;
                                                }
                                        }
                                        else
                                        {
                                                // The page is filled, vmpiod->m_block_sel +=
                                                // vmpiod->m_dev_blocks[vmpiod->m_block_sel].nsect has wrapped to zero
                                                vmp->m_io_descr->m_block_sel__ = 0;
                                                vm_page_wake(vmp);
                                        }

                                        tmp_hba.ports[p].cmdlst[cmd_idx].cmd_prdbc = 0xDEAD;

                                        if (!tmp_hba.ports[p].cmd_bitmap)
                                        {
                                                break;
                                        }
                                }
                        }

                        u32 cmdno;

                        while (((s_next_vmp_last_ptr != &s_next_vmp)) && ahci_port_find_cmd_slot(&tmp_hba, p, &cmdno))
                        {
                                // buffer_cache::io_descriptor *bh = s_wait_list.remove_head();
                                // ahci_port_post_buffer(&tmp_hba, bh, p, cmdno);
                                vm_page_t vmp = s_next_vmp;
                                s_next_vmp    = vmp->m_io_descr->m_next;

                                if (s_next_vmp_last_ptr == &vmp->m_io_descr->m_next)
                                {
                                        // This wast the last entry
                                        s_next_vmp_last_ptr = &s_next_vmp;
                                }

                                vmp->m_io_descr->m_next = nullptr;

                                natural_t seccount = vmp_get_seccount(vmp);

                                if (seccount == 0)
                                {
                                        panic("Why");
                                }

                                ahci_port_post_buffer3(&tmp_hba, 2, vmp, p, cmdno, (u32)seccount);
                        }
                }
        }

        return 0;
}

int ata_ioctl(dev_t dev, ioctl_req_t cmd, int fflag)
{
        kern_return_t retval = KERN_SUCCESS;

        dev_t portno = AHCI_GETPORT(dev);
        // dev_t partno = AHCI_GETPART(dev);
        /* Check values above */

        ahci_port_t portp = &tmp_hba.ports[portno];

        switch (ioctl_req_cmdno(cmd))
        {
#if 0
    case DIOCGPART: {
      struct partinfo *dpart = (struct partinfo *)data;
      memset(dpart, 0, sizeof(partinfo));

      if (portp->label.d_npartitions == 0) {  // Try a scan if no partitions
        retval = partition_scan(dev,
                                portp->label.d_nsectors,
                                &portp->label.part[0],
                                MAXPARTITIONS,
                                &portp->label.d_npartitions,
                                nullptr);
      }

      partitionp_t partp = &portp->label.part[partno];

      dpart->disklab = &portp->label;
      dpart->part    = partp;

      break;
    }
#endif
                case (DIOCGDINFO):
                {
                        // disklabel *di_out = (disklabel *)data;
                        //*di_out           = portp->label;
                        retval = ioctl_req_write(cmd, &portp->label, sizeof(portp->label));
                        break;
                }
                case DIOCSDINFO:
                {
                        // disklabel *di_in = (disklabel *)data;
                        // portp->label     = *di_in;
                        retval = ioctl_req_read(cmd, &portp->label, sizeof(portp->label));
                        break;
                }

                default:
                {
                        retval = KERN_FAIL(EINVAL);
                }
        }

        return retval;
}

static struct sup_dev
{
        int ven;
        int dev;
        int bus;
        int devn;
        int pad;
        int func;
        const char *nm;
} devs[] = {
        {0x8086,  0x2922, -1, 0, 0, 0, "QEMU AHCI"                                                               },
        { 0x8086, 0x3b2f, -1, 0, 0, 0, "5 Series/3400 Series Chipset 6 port SATA AHCI Controller (rev 06)"       },
        { 0x8086, 0x3b22, -1, 0, 0, 0, "5 Series/3400 Series Chipset 6 port SATA AHCI Controller (rev 06)"       },
        { 0x8086, 0x2929, -1, 0, 0, 0, "82801IBM/IEM (ICH9M/ICH9M-E) 4 port SATA controller"                     },
        { 0x8086, 0x2829, -1, 0, 0, 0, "82801HM/HEM (ICH9M/ICH9M-E) 4 port SATA controller"                      },
        { 0x15ad, 0x07e0, -1, 0, 0, 0, "VMWare SATA AHCI controller"                                             },
        { 0x8086, 0x8c02, -1, 0, 0, 0, "8 Series/C220 Series Chipset Family 6 Port SATA Controller 1 [AHCI mode]"}
};

static int ahci_init_hba(ahci_hba_t a_hba)
{
        log_info("Initializeing HBA");

        u32 version = ahci_hba_rd_reg(a_hba, AHCI_VS_OFFSET);

        log_info("Version %x.%x\n", version >> 16, version & 0xFFFF);

        u32 ports_implemented = ahci_hba_rd_reg(a_hba, AHCI_PI_OFFSET);

        log_info("Ports implmented 0x%08X\n", ports_implemented);

        /* BIOS initialized capabilities */
        u32 host_capabilities = ahci_hba_rd_reg(a_hba, AHCI_CAPABILITY_OFFSET);

        /* We are only interrested in staggered spinup and hotplug support */
        host_capabilities &= AHCI_CAP_SSS | AHCI_CAP_SIS;

        log_info("Host Capabilities 0x%08X\n", host_capabilities);

        /* Wait if there is a reset in progress */
        if (ahci_hba_mmio_wait(a_hba, AHCI_GHC_OFFSET, AHCI_GHC_HR, 0, 500) == FALSE)
        {
                panic("HBA stuck in reset, bailing!");
                return -1;
        }

        // Tell the HBA we want to use AHCI mechanisms for comunication */
        ahci_hba_or_reg(a_hba, AHCI_GHC_OFFSET, AHCI_GHC_AE);

        ahci_delay(250);

        /* Flush */
        ahci_hba_rd_reg(a_hba, AHCI_GHC_OFFSET);

        /* Issue the reset command */
        ahci_hba_or_reg(a_hba, AHCI_GHC_OFFSET, AHCI_GHC_HR);

        /* Wait if there is a reset in progress */
        if (ahci_hba_mmio_wait(a_hba, AHCI_GHC_OFFSET, AHCI_GHC_HR, 0, 500) == FALSE)
        {
                log_error("HBA did not clear the bit");
                return -1;
        }

        // The AHCI_GHC_AE(The one we set before) should be cleared after reset
        if (ahci_hba_rd_reg(a_hba, AHCI_GHC_OFFSET) & AHCI_GHC_AE)
        {
                log_warn("AHCI_GHC_AE not cleared after HBA reset");
                // Let's clear it manually
                ahci_hba_and_reg(a_hba, AHCI_GHC_OFFSET, ~AHCI_GHC_AE);

                // Give it some time
                ahci_delay(250);
        }

        ahci_delay(10);

        // Tell the HBA again that we wish AHCI mechanisms for comunication */
        ahci_hba_or_reg(a_hba, AHCI_GHC_OFFSET, AHCI_GHC_AE);

        ahci_delay(10);

        /* Flush */
        ahci_hba_rd_reg(a_hba, AHCI_GHC_OFFSET);

        // Writeback the old capabilities(set by bios)
        ahci_hba_or_reg(a_hba, AHCI_CAPABILITY_OFFSET, ahci_hba_rd_reg(a_hba, AHCI_CAPABILITY_OFFSET) | host_capabilities);

        // Writeback the number of ports implmented(set by bios)
        ahci_hba_wr_reg(a_hba, AHCI_PI_OFFSET, ports_implemented);

        /* Flush */
        ahci_hba_rd_reg(a_hba, AHCI_GHC_OFFSET);

        return 0;
}

extern "C" void form_pic_mask(void);

int ata_open(dev_t dev, int rw, int type)
{
        return 0;
}

int ata_close(dev_t dev)
{
        int drive = minor(dev) & 0x1f;

        return 1 + drive;
}

static kern_return_t ata_strategy3(dev_t a_dev, vm_page_t a_vmp)
{
        KASSERT(a_vmp->m_io_descr->m_block_sel__ == 0);

        natural_t seccount = vmp_get_seccount(a_vmp);

        if (seccount == 0)
        {
                return 0;
        }

        spl_t s = splbio();

        u32 portno = AHCI_GETPORT(a_dev);
        u32 cmdno;

        boolean_t cmd_found = ahci_port_find_cmd_slot(&tmp_hba, portno, &cmdno);

        if (cmd_found)
        {
                ahci_port_post_buffer3(&tmp_hba, minor(a_dev), a_vmp, portno, cmdno, (u32)seccount);
        }
        else
        {

                *s_next_vmp_last_ptr = a_vmp;
                s_next_vmp_last_ptr  = &a_vmp->m_io_descr->m_next;
        }

        vm_page_wait(a_vmp);

        splx(s);

        return KERN_SUCCESS;
}

static kern_return_t ata_init()
{

        // s_wait_list.init();

        // dev_t drive = minor(dev);
        ahci_hba_t hba = &tmp_hba;
        *hba           = {};
        struct sup_dev *__supdev;
        for (int b = 0; b < 0xFF; b++)
        {
                for (int p = 0; p < 0x20; ++p)
                {
                        for (int f = 0; f < 8; ++f)
                        {
                                for (int d = 0; d < (int)(sizeof(devs) / sizeof(struct sup_dev)); ++d)
                                {
                                        __supdev = &devs[d];

                                        if ((u32)__supdev->ven == (pci_cfgregread(b, p, f, PCIR_VENDOR, sizeof(hba->ven)) & 0xFFFF)
                                            && (u32)__supdev->dev
                                                       == (pci_cfgregread(b, p, f, PCIR_DEVICE, sizeof(hba->dev)) & 0xFFFF))
                                        {
                                                __supdev->bus  = b;
                                                __supdev->devn = p;
                                                __supdev->func = f;
                                                goto out;
                                        }
                                }
                        }
                }
        }

out:
        if (__supdev->bus == -1)
        {
                panic("No AHCI controller detected");
                return -/*ENODEV*/ 1;
        }
        else
        {
                log_info("%s [%04x:%04x] at %02x:%02x:%x",
                         __supdev->nm,
                         __supdev->ven,
                         __supdev->dev,
                         __supdev->bus,
                         __supdev->devn,
                         __supdev->func);
        }

        hba->ven = pci_cfgregread(__supdev->bus, __supdev->devn, __supdev->func, PCIR_VENDOR, sizeof(hba->ven)) & 0xFFFF;
        hba->dev = pci_cfgregread(__supdev->bus, __supdev->devn, __supdev->func, PCIR_DEVICE, sizeof(hba->dev)) & 0xFFFF;

        hba->bar = pci_cfgregread(__supdev->bus, __supdev->devn, __supdev->func, PCIR_BAR(ABAR_INDEX), 4);

        if (PCI_BAR_MEM(hba->bar) == FALSE)
        {
                /* Should be MMIO BAR */
                panic("Should be MMIO BAR");
                return 1;
        }

        if ((hba->bar & PCIM_BAR_MEM_TYPE) == PCIM_BAR_MEM_64)
        {
                /* We only support 32 bit devices for now */
                panic("Only support 32 bit devices");
                return 1;
        }

        /* Get the size of the MMIO region */
        size_t bar_sz = 0xFFFFFFFF;
        pci_cfgregwrite(__supdev->bus, __supdev->devn, __supdev->func, PCIR_BAR(ABAR_INDEX), (uint32_t)bar_sz, 4);

        bar_sz = ~pci_cfgregread(__supdev->bus, __supdev->devn, __supdev->func, PCIR_BAR(ABAR_INDEX), 4) + 1;

        /* Write back the original BAR */
        pci_cfgregwrite(__supdev->bus, __supdev->devn, __supdev->func, PCIR_BAR(ABAR_INDEX), (uint32_t)hba->bar, 4);

        // Taken from DragonflyBSD
        /*
         * Intel hocus pocus in case the BIOS has not set the chip up
         * properly for AHCI operation.
         */
        if (__supdev->ven == 0x8086)
        {
                if ((pci_cfgregread(__supdev->bus, __supdev->devn, __supdev->func, 0x92, 2) & 0x0F) != 0x0F)
                {
                        log_warn("Intel hocus pocus\n");
                }

                pci_cfgregwrite(__supdev->bus,
                                __supdev->devn,
                                __supdev->func,
                                0x92,
                                pci_cfgregread(__supdev->bus, __supdev->devn, __supdev->func, 0x92, 2) | 0x0F,
                                2);
        }

        /* Ensure the bar is mapped in the direct map region as uncacheable */
        hba->bar = (vm_offset_t)pmap_mapdev_uncacheable(hba->bar, bar_sz);

        int err = 0;
        if ((err = ahci_init_hba(hba)))
        {
                panic("Init hba failed");
                return err;
        }

        KASSERT(ahci_hba_rd_reg(hba, AHCI_GHC_OFFSET) & AHCI_GHC_AE);

        /* Read the HBA capabilty register */
        u32 cap = ahci_hba_rd_reg(hba, AHCI_CAPABILITY_OFFSET);

        /* Bitmap of the implemented ports */
        hba->pi = ahci_hba_rd_reg(hba, AHCI_PI_OFFSET);
        if (hba->pi == 0)
        {
                /* No implemented ports? */
                panic("No ports on HBA");
                return 1;
        }

        /* Number of ports supported by this HBA */
        hba->np = AHCI_CAP_NP(cap);

        /* Number of implemented ports on this HBA */
        // u32 npi = popcount32(hba->pi);

        /* Highest port number/index on this HBA */
        u32 max_pi = AHCI_MAX_PORTS - (u32)etl::countl_zero(hba->pi);

        /* Number of command slots pr port */
        size_t ncs = hba->ncs = AHCI_CAP_NCS(cap);

        /* Calculate the how much memory is needed for all implemented ports */

        /* rfis'es must be 256 byte aligned */
        hba->rfis = NULL;

        /* After a rfis for every implemented port we page align the memory for a
         * command list for every implemented port. Command lists must be 1k
         * aligned, for that reason we allocate 32 command headers even if the hba
         * supports less */

        hba->cmd_list = AHCI_ADD_PTR(ahci_cmd_list_t, hba->rfis, AHCI_RFIS_SIZE(max_pi));

        /* After command headers we place number of implemented ports * number of
        commands per port. command tables must be 128 bytes aligned. With 8 prdt
        entries for every command table the total size is 256 bytes.*/

        hba->cmd_table = AHCI_ADD_PTR(ahci_cmd_table_t, hba->cmd_list, AHCI_CMD_LIST_SIZE(max_pi));

        hba->idata = AHCI_ADD_PTR(ata_identify_data_t, hba->cmd_table, AHCI_CMD_TBL_SIZE(max_pi, ncs));

        vm_size_t memblksz = AHCI_ADD_PTR(vm_size_t, hba->idata, sizeof(struct ata_identify_data) * max_pi);

        memblksz = (vm_size_t)round_page(memblksz);

        if (kmem_alloc_wired(kernel_map, (vm_offset_t *)&hba->rfis, memblksz))
        {
                panic("Could not allocated rfis memory");
                return 1 /*ENOMEM*/;
        }

        bzero(hba->rfis, (size_t)memblksz);

        /* Offset the pointers with the actual address */
        hba->cmd_list  = AHCI_ADD_PTR(ahci_cmd_list_t, hba->cmd_list, hba->rfis);
        hba->cmd_table = AHCI_ADD_PTR(ahci_cmd_table_t, hba->cmd_table, hba->rfis);
        hba->idata     = AHCI_ADD_PTR(ata_identify_data_t, hba->idata, hba->rfis);

        /* Temporary storage for phys addresses */
        vm_offset_t phys_addr;
        for (size_t port_idx = 0; port_idx < AHCI_MAX_PORTS; ++port_idx)
        {
                if ((hba->pi & (1 << port_idx)) == 0)
                {
                        continue;
                }

                ahci_port_t port = &hba->ports[port_idx];
                port->rfis       = &hba->rfis[port_idx];
                port->idata      = &hba->idata[port_idx];
                port->cmdlst     = &hba->cmd_list[port_idx * ncs];
                port->cmdtbl     = &hba->cmd_table[port_idx * ncs];

                // Reset the port

                // COMRESET. Clear PxCMD.ST, and wait for PxCMD.CR to
                // clear to ‘0’ before re-initializing communication. However, if PxCMD.CR
                // does not clear within a reasonable time (500 milliseconds), it may assume
                // the interface is in a hung condition and may continue with issuing the
                // port reset.
                ahci_port_and_reg(hba, port_idx, AHCI_PxCMD, ~AHCI_PxCMD_ST);

#define PxCMD_CR u32bit<15>::value

                if (!ahci_port_mmio_wait(hba, port_idx, AHCI_PxCMD, PxCMD_CR, 0, 500))
                {
                        log_warn("Interface hung during port(%d) reset", port_idx);
                }

                // Software causes a port reset (COMRESET) by writing 1h to the PxSCTL.DET
                // field to invoke a COMRESET on the interface and start a re-establishment
                // of Phy layer communications. Software shall wait at least 1 millisecond
                // before clearing PxSCTL.DET to 0h; this ensures that at least one COMRESET
                // signal is sent over the interface. After clearing PxSCTL.DET to 0h,
                // software should wait for communication to be re-established as indicated
                // by bit 0 of PxSSTS.DET being set to ‘1’. Then software should write all
                // 1s to the PxSERR register to clear any bits that were set as part of the
                // port reset.

#define AHCI_PxSCTL_DET u32bit<0>::value
                ahci_port_or_reg(hba, port_idx, AHCI_PxSCTL, AHCI_PxSCTL_DET);
                ahci_delay(10);
                ahci_port_and_reg(hba, port_idx, AHCI_PxSCTL, ~AHCI_PxSCTL_DET);
                ahci_delay(10);

#define AHCI_PxSSTS_DET u32bit<0>::value
                if (!ahci_port_mmio_wait(hba, port_idx, AHCI_PxSSTS, AHCI_PxSSTS_DET_PCE, 3, 5))
                {
                        if (!ahci_port_mmio_wait(hba, port_idx, AHCI_PxSSTS, AHCI_PxSSTS_DET_PCE, 1, 5))
                        {
                                log_debug("Interface failed to re-establish commounication on port(%d) after "
                                          "reset",
                                          port_idx);
                                continue;
                        }
                }

                ahci_port_wr_reg(hba, port_idx, AHCI_PxSERR, 0xFFFFFFFF);

                // Initialize the port

                // 2. Ensure that PxCMD.ST = ‘0’, PxCMD.CR = ‘0’, PxCMD.FRE = ‘0’, PxCMD.FR
                // = ‘0’, and PxSCTL.DET = ‘0h’.

                if (ahci_port_rd_reg(hba, port_idx, AHCI_PxCMD) & AHCI_PxCMD_ST)
                {
                        log_error("PxCMD.ST set to 1 after reset");
                        continue;
                }
#define AHCI_PxCMD_CR u32bit<15>::value
                if (ahci_port_rd_reg(hba, port_idx, AHCI_PxCMD) & AHCI_PxCMD_CR)
                {
                        log_error("PxCMD.CR set to 1 after reset");
                        continue;
                }

                if (ahci_port_rd_reg(hba, port_idx, AHCI_PxCMD) & AHCI_PxCMD_FRE)
                {
                        log_error("PxCMD.FRE set to 1 after reset");
                        continue;
                }

#define AHCI_PxCMD_FR u32bit<14>::value
                if (ahci_port_rd_reg(hba, port_idx, AHCI_PxCMD) & AHCI_PxCMD_FR)
                {
                        log_error("PxCMD.FR set to 1 after reset");
                        continue;
                }

                if (ahci_port_rd_reg(hba, port_idx, AHCI_PxSCTL) & AHCI_PxSCTL_DET)
                {
                        log_error("PxSCTL set to 1 after reset");
                        continue;
                }

                // 3. Allocate memory for the command list and the FIS receive area. Set
                // PxCLB and PxCLBU to the physical address of the allocated command list.
                // Set PxFB and PxFBU to the physical address of the allocated FIS receive
                // area. Then set PxCMD.FRE to ‘1’.

                /* Initialize the command list for port_idx */
                for (size_t cmd_idx = 0; cmd_idx < ncs; ++cmd_idx)
                {
                        /* We don't use the scatter/gather, blocks are never larger than 4kbytes
                         * and expected to be contiguous, so we always use 1 PRD even if we
                         * AHCI_PORT_CMD_TBL_PRDTL of them per command table */
                        port->cmdlst[cmd_idx].cmd_prdtl = 1;

                        /* Clear Busy upon R_OK (C): When set, the HBA shall clear PxTFD.STS.BSY
                         * and PxCI.CI(pIssueSlot) after transmitting this FIS and receiving
                         * R_OK. When cleared, the HBA shall not clear PxTFD.STS.BSY nor
                         * PxCI.CI(pIssueSlot) after transmitting this FIS and receiving R_OK.
                         */
                        // port->cmdlst[cmd_idx].cmd_c = 1;

                        /* Length of the Command FIS. A ‘0’ represents 0 DW, ‘4’ represents 4
                         * DW. A length of ‘0’ or ‘1’ is illegal. The maximum value allowed is
                         * 10h, or 16 DW. The HBA uses this field to know the length of the FIS
                         * it shall send to the device. */
                        port->cmdlst[cmd_idx].cmd_cfl = offsetof(struct ahci_cfis, rsvd5) / sizeof(u32);

                        // Why is this needed?
                        port->cmdlst[cmd_idx].cmd_p = 1;

                        /* Physical address of the command table, which contains the command
                         * FIS, ATAPI Command, and PRD table. This address must be aligned to a
                         * 128-byte cache line, indicated by bits 06:00 being reserved. */
                        phys_addr = ahci_extract_phys_addr((vm_offset_t)&port->cmdtbl[cmd_idx]);

                        port->cmdlst[cmd_idx].cmd_ctba  = (u32)(phys_addr & 0xFFFFFFFFu);
                        port->cmdlst[cmd_idx].cmd_ctbau = (u32)(phys_addr >> 0x20u);
                }

                /* Initialize Port registers */
                /* Associate command list with this port */
                phys_addr = ahci_extract_phys_addr((vm_offset_t)port->cmdlst);
                ahci_port_wr_reg(hba, port_idx, AHCI_PxCLB, (u32)(phys_addr & 0xFFFFFFFFu));
                ahci_port_wr_reg(hba, port_idx, AHCI_PxCLBU, (u32)(phys_addr >> 0x20u));

                /* Associate rfis with this port */
                phys_addr = ahci_extract_phys_addr((vm_offset_t)port->rfis);
                ahci_port_wr_reg(hba, port_idx, AHCI_PxFB, (u32)(phys_addr & 0xFFFFFFFFu));
                ahci_port_wr_reg(hba, port_idx, AHCI_PxFBU, (u32)(phys_addr >> 0x20u));

                /* Enable FIS Recieve */
                ahci_port_or_reg(hba, port_idx, AHCI_PxCMD, AHCI_PxCMD_FRE);

                // 4. Initiate a spin up of the SATA drive attached to the port; i.e. set
                // PxCMD.SUD to ‘1’.
                ahci_port_or_reg(hba, port_idx, AHCI_PxCMD, AHCI_PxCMD_POD | AHCI_PxCMD_SUD);

                // 5. Wait for a positive indication that a device is attached to the port
                // (the maximum amount of time to wait for presence indication is specified
                // in the Serial ATA 1.0a specification). This is done by polling
                // PxSSTS.DET. If PxSSTS.DET returns a value of 1h or 3h when read, then
                // system software shall continue to the next step, otherwise if the polling
                // process times out system software moves to the next implemented port and
                // returns to step 1.
                /* Detect the presence of a device */
                size_t waitc;
                for (waitc = 0; waitc < 15; ++waitc)
                {
                        u32 ssts_det = ahci_port_rd_reg(hba, port_idx, AHCI_PxSSTS);
                        ssts_det &= AHCI_PxSSTS_DET_MASK;

                        /* Check if a device is present(We don't care if communication is
                         * established or not) */
                        if ((ssts_det == AHCI_PxSSTS_DET_PCE) || (ssts_det == AHCI_PxSSTS_DET_PNCE))
                        {
                                log_info("Device present on port %d", port_idx);

                                break;
                        }
                        /* wait 1ms */
                        ahci_delay(10);
                }

                if (waitc == 15)
                {
                        continue;
                }

                // 6. Clear the PxSERR register, by writing ‘1s’ to each implemented bit
                // location.
                ahci_port_wr_reg(hba, port_idx, AHCI_PxSERR, 0xFFFFFFFF);

                // 7. Wait for indication that SATA drive is ready. This is determined via
                // an examination of PxTFD.STS. If PxTFD.STS.BSY, PxTFD.STS.DRQ, and
                // PxTFD.STS.ERR are all ‘0’, prior to the maximum allowed time as specified
                // in the ATA/ATAPI-6 specification, the device is ready.

                /* Wait for deviec to become ready
                  According to SATA1.0a spec section 5.2, we need to wait for PxTFD.BSY
                  and PxTFD.DRQ and PxTFD.ERR to be zero. The maximum wait time is 16s
                  which is defined at ATA spec.*/
                for (waitc = 0; waitc < 16; ++waitc)
                {
                        u32 serr = ahci_port_rd_reg(hba, port_idx, AHCI_PxSERR);

                        if (serr)
                        {
                                /* Clear any errors */
                                ahci_port_wr_reg(hba, port_idx, AHCI_PxSERR, serr);
                                log_debug("SERR %x Clearing errors on port %d, clearing", serr, port_idx);
                        }

                        u32 tfd = ahci_port_rd_reg(hba, port_idx, AHCI_PxTFD);
                        tfd &= AHCI_PxTFD_MASK;

                        if (tfd == 0)
                        {
                                break;
                        }

                        /* wait 1 second */
                        ahci_delay(1000);
                }

                if (waitc == 16)
                {
                        log_debug("PxCI  0x%x", ahci_port_rd_reg(hba, port_idx, AHCI_PxCI));
                        log_debug("PxCMD 0x%x", ahci_port_rd_reg(hba, port_idx, AHCI_PxCMD));
                        log_debug("SERR  0x%x ", ahci_port_rd_reg(hba, port_idx, AHCI_PxSERR));
                        log_debug("PxIS  0x%x ", ahci_port_rd_reg(hba, port_idx, AHCI_PxIS));
                        log_debug("PxTFD  0x%x ", ahci_port_rd_reg(hba, port_idx, AHCI_PxTFD));

                        panic("Device on port %d timed out AHCI_PxTFD", port_idx);
                        continue;
                }

                log_info("Device on port %d ready", port_idx);

                // log_debug("PxCI  0x%x", ahci_port_rd_reg(hba, port_idx, AHCI_PxCI));

                boolean_t sucess = ahci_port_mmio_wait(hba, port_idx, AHCI_PxSIG, 0x0000FFFF, 0x00000101, 16 * 1000 * 1000);

                if (!sucess)
                {
                        log_debug("Failed wait for PxSIG on port %d", port_idx);
                        continue;
                }

                u32 port_sig = ahci_port_rd_reg(hba, port_idx, AHCI_PxSIG);

                if ((port_sig & AHCI_ATAPI_SIG_MASK) != AHCI_ATA_DEVICE_SIG)
                {
                        log_debug("(port_sig & AHCI_ATAPI_SIG_MASK) != AHCI_ATA_DEVICE_SIG) on %d", port_idx);

                        /* ATAPI (TODO: Handle this) */
                        continue;
                }

#define AHCI_PxCMD_CLO u32bit<3>::value
                ahci_port_or_reg(hba, port_idx, AHCI_PxCMD, AHCI_PxCMD_CLO);
                if (!ahci_port_mmio_wait(hba, port_idx, AHCI_PxCMD, AHCI_PxCMD_CLO, 0, 10))
                {
                        log_warn("AHCI_PxCMD_CLO did not clear on port(%d) after 10 seconds reset", port_idx);

                        ahci_port_or_reg(hba, port_idx, AHCI_PxCMD, AHCI_PxCMD_CLO);
                }

                // Start processing commands
                ahci_port_or_reg(hba, port_idx, AHCI_PxCMD, AHCI_PxCMD_ST);

                ahci_delay(10);

                // Send identify command
                phys_addr = ahci_extract_phys_addr((vm_offset_t)port->idata);

                port->cmdtbl[0].cfis.type     = AHCI_FIS_REGISTER_H2D;
                port->cmdtbl[0].cfis.cmdind   = 0x1;
                port->cmdtbl[0].cfis.cmd      = ATA_CMD_IDENTIFY_DRIVE;
                port->cmdtbl[0].prdtl[0].dba  = (u32)(phys_addr & 0xFFFFFFFF);
                port->cmdtbl[0].prdtl[0].dbau = (u32)(phys_addr >> 32);
                port->cmdtbl[0].prdtl[0].dbc  = sizeof(struct ata_identify_data);
                // port->cmdtbl[0].prdtl[0].ioc = 1;

                ahci_port_or_reg(hba, port_idx, AHCI_PxCI, 1);

                sucess = ahci_port_mmio_wait(hba, port_idx, AHCI_PxCI, 0x1, 0, 16 * 1000 * 1000);

                if (!sucess)
                {
                        log_warn("Identify command to port %d failed", port_idx);
                        continue;
                }

                // Wait for the command to finish
                sucess = ahci_port_mmio_wait(hba,
                                             port_idx,
                                             AHCI_PxTFD,
                                             AHCI_PxTFD_MASK | AHCI_PxTFD_BSY | AHCI_PxTFD_DRQ,
                                             0x0,
                                             16 * 1000 * 1000);

                if (!sucess)
                {
                        log_debug("Identify command timed out on port %d", port_idx);
                        continue;
                }

                /* Always clear the amount transfered before clearing IS */
                port->cmdlst->cmd_prdbc = 0;

                /* Clear any interrupt status */
                u32 portis = ahci_port_rd_reg(hba, port_idx, AHCI_PxIS);
                ahci_port_wr_reg(hba, port_idx, AHCI_PxIS, portis);

                /* Descriptor Processed Interrupt Enable */
                ahci_port_wr_reg(hba, port_idx, AHCI_PxIE, AHCI_PxIE_DPE | u32bit<0>::value);

                // port_mem += port_mem_sz;
                // log_info("Port %d initialized", port_idx);

                // log_debug("PxCI  0x%x", ahci_port_rd_reg(hba, port_idx, AHCI_PxCI));
                // log_debug("PxCMD 0x%x", ahci_port_rd_reg(hba, port_idx, AHCI_PxCMD));
                // log_debug("SERR  0x%x ", ahci_port_rd_reg(hba, port_idx, AHCI_PxSERR));
                // log_debug("PxIS  0x%x ", ahci_port_rd_reg(hba, port_idx, AHCI_PxIS));
                // log_debug("PxTFD  0x%x ", ahci_port_rd_reg(hba, port_idx, AHCI_PxTFD));
        }

        u32 iline = pci_cfgregread(__supdev->bus, __supdev->devn, __supdev->func, PCIR_INTLINE, sizeof(u8));
        // log_info("%d", iline);

        extern int (*ivect[])();
        extern int intpri[];

        /* Install interrupt handler */
        ivect[iline]  = ata_intr3;
        intpri[iline] = SPLBIO;
        form_pic_mask();

        /* Clear any pending interrupts */
        u32 is = ahci_hba_rd_reg(hba, AHCI_IS_OFFSET);
        ahci_hba_wr_reg(hba, AHCI_IS_OFFSET, is);
        // log_info("PxIS %08X", is);
        /* This global bit enables interrupts from the HBA */
        ahci_hba_or_reg(hba, AHCI_GHC_OFFSET, AHCI_GHC_IE);

        /* Scan the parition table of every implemented port */
        for (size_t port_idx = 0; port_idx < 1; ++port_idx)
        {
                ahci_port_t port = &hba->ports[port_idx];
                // Fixup ModelName
                for (size_t c_idx = 0; c_idx < sizeof(port->idata->ModelName) / 2; ++c_idx)
                {
                        char *first_byte  = (char *)(&port->idata->ModelName.double_byte[c_idx][0]);
                        char *second_byte = (char *)(&port->idata->ModelName.double_byte[c_idx][1]);

                        char tmp_char = *first_byte;
                        *first_byte   = *second_byte;
                        *second_byte  = tmp_char;
                }

                log_info("%d:%s", port_idx, port->idata->ModelName.single_byte);
                size_t dev_cap = atapi_capacity(port->idata);
                log_info("Devcap %08X", dev_cap);
                if (dev_cap == 0)
                {
                        /* Either 48 bit is not supported or the device is not present */
                        /* TODO(Damir) Find a better way to handle not present devices */
                        continue;
                }
                port->label.d_nsectors = dev_cap;
                port->label.d_secsize  = DEV_BSIZE;

                // KASSERT(specfs_register_block_device(makedev(13, 0), &atasw) == KERN_SUCCESS);

#if 0
    log_debug("Scaning partition table attached to port %d", port_idx);

    int error = partition_scan(0,
                               dev_cap,
                               &port->label.part[0],
                               MAXPARTITIONS,
                               &port->label.d_npartitions,
                               nullptr);
    if (error) {
      panic("Partscan failed");
    }
    // log_debug("PxIE  0x%x ", ahci_port_rd_reg(hba, port_idx, AHCI_PxIE));

    port->label.d_secsize = DEV_BSIZE;
#endif
        }

        // log_debug("HBA init done", drive, rw, cap + ncs, max_pi + npi);

        bdevsw[13] = atasw;

        s_next_vmp          = nullptr;
        s_next_vmp_last_ptr = &s_next_vmp;

        return KERN_SUCCESS;
}

device_initcall(ata_init);
