/*
 * Error log buffer for kernel printf's.
 */
#include <emerixx/poll.h>
#include <fcntl.h>
#include <mach/conf.h>
#include <mach/param.h>
#include <mach/syslog/syslog.h>
#include <kernel/thread_data.h>
#include <emerixx/ioctl_req.h>
#include <emerixx/memory_rw.h>

#include <stdarg.h>
#include <stdio.h>
#include <sys/ioctl.h>

#define LOG_RDPRI (PZERO + 1)

#define LOG_ASYNC  0x04
#define LOG_RDWAIT 0x08

#define TOCONS 0x01
#define TOTTY  0x02
#define TOLOG  0x04

#if DAMIR
struct logsoftc
{
        int sc_state;           /* see above for possibilities */
        struct selinfo sc_selp; /* process waiting on select call */
        int sc_pgid;            /* process/group for async I/O */
} logsoftc;
#endif
static int log_open;

static int logopen(dev_t dev, int flags, mode_t mode, pthread_t p)
{
#if DAMIR
        struct msgbuf *mbp = msgbufp;

        if (log_open)
        {
                return -EBUSY;
        }
        log_open         = 1;
        logsoftc.sc_pgid = p->p_pid; /* signal process only */
        /*
         * Potential race here with putchar() but since putchar should be
         * called by autoconf, msg_magic should be initialized by the time
         * we get here.
         */
        if (mbp->msg_magic != MSG_MAGIC)
        {
                register int i;

                mbp->msg_magic = MSG_MAGIC;
                mbp->msg_bufx = mbp->msg_bufr = 0;
                for (i = 0; i < MSG_BSIZE; i++)
                {
                        mbp->msg_bufc[i] = 0;
                }
        }
#endif
        return 0;
}

static int logclose(dev_t dev, int flag, mode_t mode, pthread_t p)
{
        log_open = 0;
        // logsoftc.sc_state = 0;
        return (0);
}

static int logread(dev_t dev, struct uio *uio, int flag)
{
#if DAMIR
        struct msgbuf *mbp = msgbufp;
        long l;
        int s;
        int error = 0;

        s = splhigh();
        while (mbp->msg_bufr == mbp->msg_bufx)
        {
                if (flag & IO_NDELAY)
                {
                        splx(s);
                        return -EWOULDBLOCK;
                }
                logsoftc.sc_state |= LOG_RDWAIT;
                if (error = tsleep((caddr_t)mbp, LOG_RDPRI | PCATCH, "klog", 0))
                {
                        splx(s);
                        return (error);
                }
        }
        splx(s);
        logsoftc.sc_state &= ~LOG_RDWAIT;

        while (uio->uio_resid > 0)
        {
                l = mbp->msg_bufx - mbp->msg_bufr;
                if (l < 0)
                {
                        l = MSG_BSIZE - mbp->msg_bufr;
                }
                l = min(l, uio->uio_resid);
                if (l == 0)
                {
                        break;
                }
                error = uiomove(&mbp->msg_bufc[mbp->msg_bufr], (int)l, uio);
                if (error)
                {
                        break;
                }
                mbp->msg_bufr += l;
                if (mbp->msg_bufr < 0 || mbp->msg_bufr >= MSG_BSIZE)
                {
                        mbp->msg_bufr = 0;
                }
        }
        return (error);
#endif
        return 0;
}

int logselect(dev_t dev, int rw, pthread_t p)
{
        int s = splhigh();
#if DAMIR
        switch (rw)
        {
                case FREAD:
                        if (msgbufp->msg_bufr != msgbufp->msg_bufx)
                        {
                                splx(s);
                                return (1);
                        }
                        selrecord(p, &logsoftc.sc_selp);
                        break;
        }
#endif
        splx(s);
        return (0);
}

void logwakeup(pthread_t p)
{
#if 0
  if (!log_open)
    return;
  selwakeup(&logsoftc.sc_selp);
  if (logsoftc.sc_state & LOG_ASYNC) {
    if (logsoftc.sc_pgid < 0)
      gsignal(-logsoftc.sc_pgid, SIGIO);
    else if (p = pfind(logsoftc.sc_pgid))
      psignal(p, SIGIO);
  }
  if (logsoftc.sc_state & LOG_RDWAIT) {
    thread_wakeup(msgbufp);
    logsoftc.sc_state &= ~LOG_RDWAIT;
  }
#endif
}

int logioctl(dev_t dev, ioctl_req_t com, void *data, int flag, pthread_t p)
{
        long l;
        int s;

        switch (ioctl_req_cmdno(com))
        {
                /* return number of characters immediately available */
                case FIONREAD:
#if DAMIR
                        s = splhigh();
                        l = msgbufp->msg_bufx - msgbufp->msg_bufr;
                        splx(s);
                        if (l < 0)
                        {
                                l += MSG_BSIZE;
                        }
                        *(int *)data = l;
#endif
                        break;

                case FIONBIO:
                        break;

                case FIOASYNC:
#if DAMIR
                        if (*(int *)data)
                        {
                                logsoftc.sc_state |= LOG_ASYNC;
                        }
                        else
                        {
                                logsoftc.sc_state &= ~LOG_ASYNC;
                        }
#endif
                        break;

                case TIOCSPGRP:
#if DAMIR
                        logsoftc.sc_pgid = *(int *)data;
#endif
                        break;

                case TIOCGPGRP:
#if DAMIR
                        *(int *)data = logsoftc.sc_pgid;
#endif
                        break;

                default:
                        return (-1);
        }
        return (0);
}

static simple_lock_data_t syslog_add_lock;

/* TODO(Unmap this after successfull boot) */
/* BOOTDATA */
static char temp_buf[0x100];

static char *syslog_buf;
static size_t syslog_buf_sz;
static size_t buf_head, buf_tail;

static void syslog_buf_putc(char c)
{
        buf_head &= syslog_buf_sz - 1;
        syslog_buf[buf_head] = c;
        buf_head++;
}

void syslog_setconstty(struct tty *aconsttyp)
{
        // constty = aconsttyp;
}

static void syslog_lock(boolean_t lock, void *sl)
{
        if (lock)
        {
                simple_lock((simple_lock_t)sl);
        }
        else
        {
                simple_unlock((simple_lock_t)sl);
        }
}

extern "C" void syslog_init()
{
        simple_lock_init(&syslog_add_lock);

        buf_head = buf_tail = 0;

        syslog_buf = &temp_buf[0];

        syslog_buf_sz = sizeof(temp_buf);

        // constty = nullptr;

        log_init();

        log_set_lock(syslog_lock, &syslog_add_lock);

        log_set_level(LOG_TRACE);
}

// Should only be called from rxi's logger as it takes care of locking
extern "C" void syslog_vadd(const char *fmt, va_list ap)
{
        char buffer[0x100];
        int len = vsnprintf(&buffer[0], sizeof(buffer), fmt, ap);

        for (int i = 0; i < len; ++i)
        {
                syslog_buf_putc(buffer[i]);
        }

        if (!log_open && len > 0)
        {
                struct uio conuio;
                struct iovec iov  = { .iov_base = &buffer[0], .iov_len = (size_t)len };
                conuio.uio_iov    = &iov;
                conuio.uio_iovcnt = 1;
                conuio.uio_resid  = iov.iov_len;
                conuio.uio_offset = 0;
                conuio.uio_segflg = UIO_SYSSPACE;
                conuio.uio_rw     = UIO_WRITE;

                //(linesw[constty->t_line].l_write)(constty, &conuio, 0);
        }

        logwakeup(nullptr);
}

extern "C" void syslog_add(const char *fmt, ...)
{
        va_list ap;
        va_start(ap, fmt);
        syslog_vadd(fmt, ap);
        va_end(ap);
}
