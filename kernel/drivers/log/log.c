/*
 * Copyright (c) 2020 rxi
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <mach/syslog/syslog.h>
#include <stdbool.h>
#include <stdio.h>
#include <strings.h>

#define MAX_CALLBACKS 32

typedef struct
{
        log_LogFn fn;
        void *udata;
        integer_t level;
} Callback;

static struct
{
        void *udata;
        log_LockFn lock;
        integer_t level;
        boolean_t quiet;
        Callback callbacks[MAX_CALLBACKS];
} L;

extern unsigned long elapsed_ticks; /* number of ticks elapsed since bootup */

/* Output sink, will either go to the ctty or a process reading from /dev/syslog
 */
void syslog_add(const char *fmt, ...);
void syslog_vadd(const char *fmt, va_list ap);

static const char *level_strings[] = {"TRACE", "DEBUG", "INFO", "WARN", "ERROR", "PANIC"};

#ifdef LOG_USE_COLOR
static const char *level_colors[] = {"\x1b[94m", "\x1b[36m", "\x1b[32m", "\x1b[33m", "\x1b[31m", "\x1b[35m"};
#endif
static void stdout_callback(log_Event *ev)
{
        char buf[16];
        buf[snprintf(buf, sizeof(buf), "[%08d]", elapsed_ticks)] = '\0';

#ifdef LOG_USE_COLOR
        syslog_add(
            "%s %s%-5s\x1b[0m \x1b[90m%s:%d:\x1b[0m ", buf, level_colors[ev->level], level_strings[ev->level], ev->file, ev->line);
#else
        syslog_add("%s %-5s %s:%d: ", buf, level_strings[ev->level], ev->file, ev->line);
#endif
#ifndef KERNEL
        vfprintf(ev->udata, ev->fmt, ev->ap);
        fprintf(ev->udata, "\n");
        fflush(ev->udata);
#endif
        syslog_vadd(ev->fmt, ev->ap);
        syslog_add("\n");
}

#ifndef KERNEL
static void file_callback(log_Event *ev)
{
        char buf[64];
        buf[strftime(buf, sizeof(buf), "%Y-%m-%d %H:%M:%S", ev->time)] = '\0';
        fprintf(ev->udata, "%s %-5s %s:%d: ", buf, level_strings[ev->level], ev->file, ev->line);
        vfprintf(ev->udata, ev->fmt, ev->ap);
        fprintf(ev->udata, "\n");
        fflush(ev->udata);
}
#endif

static void lock(void)
{
        if (L.lock)
        {
                L.lock(true, L.udata);
        }
}

static void unlock(void)
{
        if (L.lock)
        {
                L.lock(false, L.udata);
        }
}

const char *log_level_string(int level) { return level_strings[level]; }

void log_set_lock(log_LockFn fn, void *udata)
{
        L.lock  = fn;
        L.udata = udata;
}

void log_set_level(int level) { L.level = level; }

void log_set_quiet(boolean_t enable) { L.quiet = (bool)enable; }

int log_add_callback(log_LogFn fn, void *udata, int level)
{
        for (int i = 0; i < MAX_CALLBACKS; i++)
        {
                if (!L.callbacks[i].fn)
                {
                        L.callbacks[i] = (Callback){fn, udata, level};
                        return 0;
                }
        }
        return -1;
}

#ifndef KERNEL
int log_add_fp(vfs_file_t tp, int level) { return log_add_callback(file_callback, fp, level); }
#endif

static void init_event(log_Event *ev, void *udata)
{
#ifndef KERNEL
        if (!ev->time)
        {
                time_t t = time(NULL);
                ev->time = localtime(&t);
        }
#endif
        ev->udata = udata;
}

void log_log(int level, const char *file, int line, const char *fmt, ...)
{
        log_Event ev = {
            .fmt   = fmt,
            .file  = file,
            .line  = line,
            .level = level,
        };

        lock();

        if (!L.quiet && level >= L.level)
        {
                init_event(&ev, /*stderr*/ NULL);
                va_start(ev.ap, fmt);
                stdout_callback(&ev);
                va_end(ev.ap);
        }
#ifndef KERNEL
        for (int i = 0; i < MAX_CALLBACKS && L.callbacks[i].fn; i++)
        {
                Callback *cb = &L.callbacks[i];
                if (level >= cb->level)
                {
                        init_event(&ev, cb->udata);
                        va_start(ev.ap, fmt);
                        cb->fn(&ev);
                        va_end(ev.ap);
                }
        }
#endif

        unlock();
}

void log_init() { bzero(&L, sizeof(L)); }
