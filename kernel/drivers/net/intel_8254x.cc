#include <aux/init.h>
#include <emerixx/interrupt.h>
#include <emerixx/module.h>
#include <emerixx/net/ethernet.h>
#include <emerixx/net/net_core.h>
#include <emerixx/net/iface_buffer.h>
#include <emerixx/net/iface_buffer_data.h>
#include <emerixx/net/iface.h>
#include <emerixx/net/iface_data.h>
#include <emerixx/pci.h>

#include <kernel/vm/pmap.h>
#if 0
#include <linux/kernel.h>
#endif

#define BIT(...) (etl::bit<uint32_t, __VA_ARGS__>::value)

// Should not be here
#if defined(__amd64__)
#define cpu_to_le16(x) (x)
#define cpu_to_le32(x) (x)
#define cpu_to_le64(x) (x)

#define le16_to_cpu(x) (x)
#define le32_to_cpu(x) (x)
#define le64_to_cpu(x) (x)
#endif

// Veneer
#define iowaitset32(mask, addr) while ((ioread32(addr) & (mask)) != (mask))
#define ioand32(mask, addr)     iowrite32(ioread32(addr) & (mask), addr)
#define ioor32(mask, addr)      iowrite32(ioread32(addr) | (mask), addr)

// Device Control
#define CTRL         0x00000
#define CTRL_FD      BIT(0)  // Full-Duplex
#define CTRL_LRST    BIT(3)  // Link Reset
#define CTRL_ASDE    BIT(5)  // Auto-Speed Detection Enable
#define CTRL_SLU     BIT(6)  // Set Link up
#define CTRL_ILOS    BIT(7)  // Invert Loss-of-Signal (LOS)
#define CTRL_RST     BIT(26) // Device Reset
#define CTRL_PHY_RST BIT(31) // PHY Reset

#define rdctrl(addr)        ioread32((addr) + CTRL)
#define wrctrl(val, addr)   iowrite32((val), (addr) + CTRL)
#define orctrl(mask, addr)  ioor32((mask), (addr + CTRL))
#define andctrl(mask, addr) ioand32((mask), (addr + CTRL))

// Device Status
#define STATUS       0x00008
#define STATUS_FD    BIT(0) // Link Full Duplex configuration Indication
#define STATUS_LU    BIT(1) // Link Up Indication
#define STATUS_TXOFF BIT(4) // Transmission Paused

#define rdstatus(addr) ioread32((addr) + STATUS)

// EEPROM/Flash Control/Data
#define EECD         0x00010
#define EECD_SK      BIT(0)
#define EECD_CS      BIT(1)
#define EECD_DI      BIT(2)
#define EECD_EE_REQ  BIT(6)
#define EECD_EE_GNT  BIT(7)
#define EECD_EE_PRES BIT(8)

// EEPROM Read
#define EERD         0x00014
#define EERD_START   BIT(0)
#define EERD_DONE    BIT(4)
#define EERD_ADDR(x) ((((uint32_t)(x & 0xFF)) << 8))
#define EERD_DATA(x) (((x) >> 16) & 0xFFFF)

// Extended Device Control
#define CTRL_EXT        0x00018
#define CTRL_EXT_PHYINT BIT5 // PHY Interrupt Value

// Interrupt Cause Read
#define ICR        0x000C0
#define ICR_TXDW   BIT(0)  // Transmit Descriptor Written Back
#define ICR_TXQE   BIT(1)  // Transmit Queue Empty
#define ICR_LSC    BIT(2)  // Link Status Change
#define ICR_RXSEQ  BIT(3)  // Receive Sequence Error
#define ICR_RXDMT0 BIT(4)  // Receive Descriptor Minimum Threshold Reached
#define ICR_RXO    BIT(6)  // Receiver Overrun
#define ICR_RXT0   BIT(7)  // Receiver Timer Interrupt
#define ICR_MDAC   BIT(8)  // MDI/O Access Complete
#define ICR_PHYINT BIT(12) // PHY Interrupt
#define ICR_SRPD   BIT(16) // Small Receive Packet Detected

// Interrupt Cause Set
#define ICS         0x000C8
#define ICS_TXDW    BIT(0)  // Sets Transmit Descriptor Written Back Interrupt
#define ICS_TXQE    BIT(1)  // Sets Transmit Queue Empty Interrupt
#define ICS_LCS     BIT(2)  // Sets Link Status Change Interrupt
#define ICS_RXSEQ   BIT(3)  // Sets Receive Sequence Error Interrupt
#define ICS_RXDMT0  BIT(4)  // Sets Receive Descriptor Minimum Threshold Reached Interrupt
#define ICS_RXO     BIT(6)  // Sets on Receiver FIFO Overrun Interrupt
#define ICS_RXT0    BIT(7)  // Sets Receiver Timer Interrupt
#define ICS_MDAC    BIT(9)  // Sets MDI/O Access Complete Interrupt
#define ICS_TXD_LOW BIT(15) // Transmit Descriptor Low Threshold Hit
#define ICS_SRPD    BIT(16) // Small Receive Packet Detected and Transferred

// Interrupt Mask Set/Read
#define IMS         0x000D0
#define IMS_TXDW    BIT(0)  // Sets mask for Transmit Descriptor Written Back
#define IMS_TXQE    BIT(1)  // Sets mask for Transmit Queue Empty
#define IMS_LSC     BIT(2)  // Sets mask for Link Status Change
#define IMS_RXSEQ   BIT(3)  // Sets mask for Receive Sequence Error
#define IMS_RXDMT0  BIT(4)  // Sets mask for Receive Descriptor Minimum Threshold hit
#define IMS_RXO     BIT(6)  // Sets mask for on Receiver FIFO Overrun
#define IMS_RXT0    BIT(7)  // Sets mask for Receiver Timer Interrupt
#define IMS_MDAC    BIT(9)  // Sets mask for MDI/O Access Complete Interrupt
#define IMS_RXCFG   BIT(10) // Sets mask for Receiving /C/ ordered sets
#define IMS_PHYINT  BIT(12) // Sets mask for PHY Interrupt
#define IMS_TXD_LOW BIT(15) // Sets the mask for Transmit Descriptor Low Threshold hit
#define IMS_SRPD    BIT(16) // Sets mask for Small Receive Packet Detection

// Interrupt Mask Clear
#define IMC         0x000D8
#define IMC_TXDW    BIT(0)  // Clears mask for Transmit Descriptor Written Back
#define IMC_TXQE    BIT(1)  // Clears mask for Transmit Queue Empty
#define IMC_LSC     BIT(2)  // Clears mask for Link Status Change
#define IMC_RXSEQ   BIT(3)  // Clears mask for Receive Sequence Error
#define IMC_RXDMT0  BIT(4)  // Clears mask for Receive Descriptor Minimum Threshold hit
#define IMC_RXO     BIT(6)  // Clears mask for on Receiver FIFO Overrun
#define IMC_RXT0    BIT(7)  // Clears mask for Receiver Timer Interrupt
#define IMC_MDAC    BIT(9)  // Clears mask for MDI/O Access Complete Interrupt
#define IMC_RXCFG   BIT(10) // Clears mask for Receiving /C/ ordered sets
#define IMC_PHYINT  BIT(12) // Clears PHY Interrupts
#define IMC_TXD_LOW BIT(15) // Clears the mask for Transmit Descriptor Low Threshold hit
#define IMC_SRPD    BIT(16) // Clears mask for Small Receive Packet Detect Interrupt

// Receive Control
#define RCTL       0x00100
#define RCTL_EN    BIT(1)      // Receiver Enable
#define RCTL_SBP   BIT(2)      // Store Bad Packets
#define RCTL_UPE   BIT(3)      // Unicast Promiscuous Enabled
#define RCTL_MPE   BIT(4)      // Multicast Promiscuous Enabled
#define RCTL_LPE   BIT(5)      // Long Packet Reception Enable
#define RCTL_BAM   BIT(15)     // Broadcast Accept Mode
#define RCTL_BSIZE BIT(16, 17) // Receive Buffer Size
#define RCTL_VFE   BIT(18)     // VLAN Filter Enable
#define RCTL_CFIEN BIT(19)     // Canonical Form Indicator Enable
#define RCTL_CFI   BIT(20)     // Canonical Form Indicator bit value
#define RCTL_DPF   BIT(22)     // Discard Pause Frames
#define RCTL_PMCF  BIT(23)     // Pass MAC Control Frames
#define RCTL_BSEX  BIT(25)     // Buffer Size Extension
#define RCTL_SECRC BIT(26)     // Strip Ethernet CRC from incoming packet

// Flow Control Receive Threshold Low
#define FCRTL 0x02160

// Flow Control Receive Threshold High
#define FCRTH 0x02168

// Receive Descriptor Base Address Low
#define RDBAL 0x02800

// Receive Descriptor Base Address High
#define RDBAH 0x02804

// Receive Descriptor Length
#define RDLEN 0x02808

// Receive Descriptor Head
#define RDH 0x02810

// Receive Descriptor Tail
#define RDT 0x02818

// Multicast Table Array (n)
#define MTA   0x05200
#define MTA_N ((0x053FC - MTA) / sizeof(uint32_t))

// Receive Address Low
#define RAL 0x05400

// Receive Address High
#define RAH    0x05404
#define RAH_AV BIT(31) // Address Valid

// Transmit Control
#define TCTL           0x00400
#define TCTL_EN        BIT(1)                // Transmit Enable
#define TCTL_PSP       BIT(3)                // Pad Short Packets
#define TCTL_CT(val)   (((val)&0xFF) << 4)   // Collision Threshold
#define TCTL_COLD(val) (((val)&0x3FF) << 12) // Collision Distance
#define TCTL_SWXOFF    BIT(22)               // Software XOFF Transmission
#define TCTL_RTLC      BIT(24)               // Re-transmit on Late Collision

// Transmit IPG
#define TIPG            0x00410
#define TIPG_IPGT(val)  (((val)&0x3FF) << 00) // IPG Transmit Time
#define TIPG_IPGR1(val) (((val)&0x3FF) << 10) // IPG Receive Time 1
#define TIPG_IPGR2(val) (((val)&0x3FF) << 20) // IPG Receive Time 2

// Transmit Descriptor Base Address Low
#define TDBAL 0x03800

// Transmit Descriptor Base Address High
#define TDBAH 0x03804

// Transmit Descriptor Length
#define TDLEN 0x03808

// Transmit Descriptor Head
#define TDH 0x03810

// Transmit Descriptor Tail
#define TDT 0x03818

// Transmit Interrupt Delay Value
#define TIDV 0x03820

struct alignas(16) i8254x_rdesc
{
        uint64_t buffer_address;
        uint64_t packet_info;
};

struct alignas(16) i8254x_tdesc
{
        uint64_t buffer_address;
        // uint64_t packet_info;
        uint16_t lenght;
        uint8_t cso;
        uint8_t cmd;
        uint8_t sta;
        uint8_t css;
        uint16_t spec;
};
static_assert(sizeof(i8254x_tdesc) == 16);

#define N_RX_DESC (128 / sizeof(i8254x_rdesc)) // Gives us 8 descriptors
#define N_TX_DESC (128 / sizeof(i8254x_tdesc)) // Gives us 8 descriptors

#define SZ_RX_DESC (N_RX_DESC * sizeof(i8254x_rdesc))
#define SZ_TX_DESC (N_RX_DESC * sizeof(i8254x_tdesc))

struct i8254x_private
{
        ioaddr_t ioaddr;
        natural_t iolen;
        pci_device_t pdev;
        natural_t pad;
        i8254x_rdesc rdesc[N_RX_DESC];   // should not be here
        iface_buffer_t rbufs[N_RX_DESC]; // likewise
        i8254x_tdesc tdesc[N_TX_DESC];   // should not be here
        iface_buffer_t tbufs[N_TX_DESC]; // likewise
};

#define NETDEV_PRIV(nd) ((i8254x_private *)iface_private(nd))

static struct pci_device_id ids[] = {
        {
                PCI_DEVICE(PCI_VENDOR_ID_INTEL, 0x100e) // 82540EM Gigabit Ethernet Controller
        },
        { 0 } //
};

MODULE_DEVICE_TABLE(pci, ids);

// ********************************************************************************************************************************
static int waitreg(ioaddr_t a_ioaddr, uint32_t a_reg, uint32_t a_mask, uint32_t value, int32_t timeout)
// ********************************************************************************************************************************
{
        while (timeout-- && ((ioread32(a_ioaddr + a_reg) & a_mask) != value))
        {
                // ahci_delay(1);
        }

        return timeout > 0;
}

// ********************************************************************************************************************************
static uint16_t eeprom_read(uint8_t a_wordno, ioaddr_t a_ioaddr)
// ********************************************************************************************************************************
{
        ioor32(EECD_EE_REQ, a_ioaddr + EECD);

        iowaitset32(EECD_EE_GNT, a_ioaddr + EECD);

        iowrite32(EERD_START | EERD_ADDR(a_wordno), a_ioaddr + EERD);

        iowaitset32(EERD_DONE, a_ioaddr + EERD);

        uint16_t retval = EERD_DATA(ioread32(a_ioaddr + EERD));

        ioand32(~EECD_EE_REQ, a_ioaddr + EECD);

        return le16_to_cpu(retval);
}

// ********************************************************************************************************************************
static void i8254x_poll(iface_t a_netdev)
// ********************************************************************************************************************************
{
        i8254x_private *pd = NETDEV_PRIV(a_netdev);

        uint32_t tdh = ioread32(pd->ioaddr + TDH);
        for (uint32_t tdt = (ioread32(pd->ioaddr + TDT) + 1) & 7; tdt != tdh; tdt = (tdt + 1) & 7)
        {
                if (pd->tbufs[tdt])
                {
                        // log_debug("intel e100e free: %p tdt=%u sta=%u", pd->tbufs[tdt], tdt, pd->tdesc->sta);
                        iface_buffer_free(pd->tbufs[tdt]);
                        pd->tbufs[tdt] = nullptr;
                        pd->tdesc[tdt] = { .buffer_address = 0 };
                }
        }

        uint32_t rdh = ioread32(pd->ioaddr + RDH);

        for (uint32_t rdt = (ioread32(pd->ioaddr + RDT) + 1) & 7; rdt != rdh; rdt = (rdt + 1) & 7)
        {
                i8254x_rdesc *rdesc      = &pd->rdesc[rdt];
                iface_buffer_t old_ndbuf = pd->rbufs[rdt];

                iface_buffer_t new_ndbuf;
                kern_return_t retval = iface_buffer_alloc(a_netdev, &new_ndbuf);

                if (KERN_STATUS_FAILURE(retval))
                {
                        panic("netdevice_alloc_buffer failed: Add sane handling");
                }

                uint32_t len    = (pd->rdesc[rdt].packet_info >> 00) & 0xFFFF;
                uint32_t status = (pd->rdesc[rdt].packet_info >> 32) & 0xFF;
                uint32_t err    = (pd->rdesc[rdt].packet_info >> 40) & 0xFF;
                uint32_t spec   = (pd->rdesc[rdt].packet_info >> 48) & 0xFF;

                if (err != 0)
                {
                        panic("packet error set: Add sane handling");
                }

                pd->rbufs[rdt] = new_ndbuf;
                *rdesc         = { .buffer_address = cpu_to_le64(new_ndbuf->m_phys_addr), .packet_info = 0 };

                iowrite32(rdt, pd->ioaddr + RDT);

                if (status != 7 || spec != 0)
                {
                        // log_warn("Packet(%d, %d) len=%d, status=%d, err=%d, spec=%d", rdh, rdt, len, status, err, spec);
                }

                old_ndbuf->m_packet_size = len - 4; // Remove the CRC checksum
                net_core_receive(old_ndbuf);
        }

        // Reenable interrupts
        iowrite32(IMS_RXT0 | IMS_RXO | IMS_RXDMT0 | IMS_RXSEQ | IMS_LSC | IMS_TXDW, pd->ioaddr + IMS);
}

// ********************************************************************************************************************************
static void i8254x_tx(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        i8254x_private *pd = NETDEV_PRIV(a_iface);
        uint32_t tdh       = ioread32(pd->ioaddr + TDH);
        uint32_t tdt       = ioread32(pd->ioaddr + TDT);

        if (((tdt + 1) & 7) == tdh)
        {
                panic("tx buffer full: Add sane handling");
        }

        // log_debug("e100e %p tdt=%u", a_iface_buffer, tdt);

        if (pd->tbufs[tdt] != nullptr)
        {
                iface_buffer_free(pd->tbufs[tdt]);
                pd->tbufs[tdt] = nullptr;
                pd->tdesc[tdt] = { .buffer_address = 0 };
        }

        pd->tbufs[tdt] = a_iface_buffer;
        pd->tdesc[tdt] = {
                .buffer_address = pd->tbufs[tdt]->m_phys_addr,
                .lenght         = (uint16_t)pd->tbufs[tdt]->m_packet_size,
                .cmd            = BIT(0, 1, 3),
                .css            = (uint8_t)pd->tbufs[tdt]->m_packet_size,
        };

        iowrite32((tdt + 1) & 7, pd->ioaddr + TDT);
}

// ********************************************************************************************************************************
static irqreturn_t i8254x_interrupt(int a_irq, void *a_dev_id)
// ********************************************************************************************************************************
{
        pci_device_t pdev  = (pci_device_t)(a_dev_id);
        iface_t dev        = pdev->m_device.netdev;
        i8254x_private *pd = NETDEV_PRIV(dev);

        uint32_t icr = ioread32(pd->ioaddr + ICR);

        if (icr == 0)
        {
                return IRQ_NONE;
        }
#if 0
        if (icr & ICR_TXDW)
        {
                uint32_t tdh = ioread32(pd->ioaddr + TDH);
                for (uint32_t tdt = (ioread32(pd->ioaddr + TDT) + 1) & 7; tdt != tdh; tdt = (tdt + 1) & 7)
                {
                        if (pd->tbufs[tdt])
                        {
                                iface_buffer_free(dev, pd->tbufs[tdt]);
                                pd->tbufs[tdt] = nullptr;
                                pd->tdesc[tdt] = {.buffer_address = 0};
                        }
                }

                if ((icr & (~ICR_TXDW)) == 0)
                {
                        return IRQ_HANDLED;
                }
        }
#endif
        if (icr & ICR_LSC)
        {
                panic("LINK status changed, why");
        }

        if (icr & ICR_RXDMT0)
        {
                // log_warn("RX threshold reached, are we to slow?");
        }

        if (icr & IMS_RXO)
        {
                // log_error("RX FIFO overrun, packet dropped");
        }

        if (icr & IMS_RXSEQ)
        {
                panic("RX sequence error");
        }

        iowrite32(0xFFFFFFFF, pd->ioaddr + IMC);

        net_core_rx_sched(dev);

        return IRQ_HANDLED;
}

// ********************************************************************************************************************************
static int probe(struct pci_dev *dev, const struct pci_device_id *id)
// ********************************************************************************************************************************
{
        dev->m_device.netdev = iface_alloc(sizeof(i8254x_private), alignof(i8254x_private));
        i8254x_private *pd   = NETDEV_PRIV(dev->m_device.netdev);

        KASSERT((((vm_address_t)pd->rdesc) & 15) == 0);
        KASSERT((((vm_address_t)pd->tdesc) & 15) == 0);

        pd->pdev = dev;

        pci_enable_device(pd->pdev);

        pd->iolen  = pci_resource_len(pd->pdev, 0);
        pd->ioaddr = pci_iomap(pd->pdev, 0, pd->iolen);

        // Reset the controller
        wrctrl(CTRL_RST, pd->ioaddr);

        if (waitreg(pd->ioaddr, CTRL, CTRL_RST, 0, 1000) == 0)
        {
                panic("Stuck on reset");
        }

        // Enable EEPROM access
        if ((ioread32(pd->ioaddr + EECD) & EECD_EE_PRES) == 0)
        {
                panic("EEPROM not present");
        }

        ioor32(EECD_SK | EECD_CS | EECD_DI, pd->ioaddr + EECD);
        iowaitset32(EECD_SK | EECD_CS | EECD_DI, pd->ioaddr + EECD);

        *((uint16_t *)&dev->m_device.netdev->m_mac_addr[0]) = eeprom_read(0, pd->ioaddr);
        *((uint16_t *)&dev->m_device.netdev->m_mac_addr[2]) = eeprom_read(1, pd->ioaddr);
        *((uint16_t *)&dev->m_device.netdev->m_mac_addr[4]) = eeprom_read(2, pd->ioaddr);

        // Initialization testing

        // Auto detect speed and set link up
        wrctrl(CTRL_ASDE | CTRL_SLU, pd->ioaddr);

        if (waitreg(pd->ioaddr, STATUS, STATUS_LU, STATUS_LU, 5000) == 0)
        {
                panic("Link not up");
        }

        orctrl(CTRL_PHY_RST, pd->ioaddr);

        andctrl(~CTRL_PHY_RST, pd->ioaddr);

        // Enable the device to become a master on the bus (to do DMA)
        pci_set_master(pd->pdev);

        // Should be done at open
        request_irq(pd->pdev->irq, i8254x_interrupt, IRQF_SHARED, /*dev->name*/ "nic", dev);

        pd->pdev->m_device.netdev->rx = i8254x_poll;
        pd->pdev->m_device.netdev->tx = i8254x_tx;

        pd->pdev->m_device.netdev->input = ethernet_input;
        // pd->pdev->m_device.netdev->output = ehternet_output;

        // RX init

        // Initialize RX Buffers
        for (uint32_t i = 0; i < N_RX_DESC; ++i)
        {
                kern_return_t retval = iface_buffer_alloc(dev->m_device.netdev, &pd->rbufs[i]);

                if (KERN_STATUS_FAILURE(retval))
                {
                        panic("Add sane handling here");
                }

                pd->rdesc[i] = { .buffer_address = cpu_to_le64(pd->rbufs[i]->m_phys_addr) };
        }

        // Program the Receive Address Register(s) (RAL/RAH) with the desired Ethernet addresses.
        uint16_t *mac = (uint16_t *)dev->m_device.netdev->m_mac_addr;
        iowrite32((((uint32_t)cpu_to_le16(mac[1])) << 16) | cpu_to_le16(mac[0]), pd->ioaddr + RAL);
        iowrite32(((uint32_t)cpu_to_le16(mac[2])) | RAH_AV, pd->ioaddr + RAH);

        // Initialize the MTA (Multicast Table Array) to 0b. Entries can be added to this table as desired.
        for (uint32_t i = 0; i < MTA_N; i++)
        {
                iowrite32(0, pd->ioaddr + (MTA + (i << 2)));
        }

        // Program the Interrupt Mask Set/Read (IMS) register to enable any interrupt the software driver wants to be notified of
        // when the event occurs. Suggested bits include RXT, RXO, RXDMT, RXSEQ, and LSC. There is no immediate reason to enable the
        // transmit interrupts.
        iowrite32(IMS_RXT0 | IMS_RXO | IMS_RXDMT0 | IMS_RXSEQ | IMS_LSC | IMS_TXDW, pd->ioaddr + IMS);

        // If software uses the Receive Descriptor Minimum Threshold Interrupt, the Receive Delay Timer (RDTR) register should be
        // initialized with the desired delay time.

        // TODO: !!!!!

        // Allocate a region of memory for the receive descriptor list. Software should insure this memory is aligned on a paragraph
        // (16-byte) boundary. Program the Receive Descriptor Base Address (RDBAL/RDBAH) register(s) with the address of the region.
        // RDBAL is used for 32-bit addresses and both RDBAL and RDBAH are used for 64-bit addresses.
        uint64_t rb = pmap_extract(kernel_pmap, (vm_address_t)pd->rdesc);
        // TODO(rb + sizeof(s_rdesc) might cross a physical page boundry);
        iowrite32((uint32_t)(rb & 0xFFFFFFFF), pd->ioaddr + RDBAL);
        iowrite32((uint32_t)((rb >> 32) & 0xFFFFFFFF), pd->ioaddr + RDBAH);

        // Set the Receive Descriptor Length (RDLEN) register to the size (in bytes) of the descriptor ring. This register must be
        // 128-byte aligned.
        static_assert((SZ_RX_DESC & 0x7F) == 0);
        iowrite32(SZ_RX_DESC, pd->ioaddr + RDLEN);

        // The Receive Descriptor Head and Tail registers are initialized (by hardware) to 0b after a power-on or a
        // software-initiated Ethernet controller reset. Receive buffers of appropriate size should be allocated and pointers to
        // these buffers should be stored in the receive descriptor ring. Software initializes the Receive Descriptor Head (RDH)
        // register and Receive Descriptor Tail (RDT) with the appropriate head and tail addresses. Head should point to the first
        // valid receive descriptor in the descriptor ring and tail should point to one descriptor beyond the last valid descriptor
        // in the descriptor ring.
        iowrite32(0, pd->ioaddr + RDH);
        iowrite32(N_RX_DESC - 1, pd->ioaddr + RDT);

        // Set the receiver Enable (RCTL.EN) bit to 1b for normal operation. However, it is best to leave the Ethernet controller
        // receive logic disabled (RCTL.EN = 0b) until after the receive descriptor ring has been initialized and software is ready
        // to process received packets.
        uint32_t rctl = RCTL_EN;

        // Set the Long Packet Enable (RCTL.LPE) bit to 1b when processing packets greater than the standard Ethernet packet size.
        // For example, this bit would be set to 1b when processing Jumbo Frames.

        // Loopback Mode (RCTL.LBM) should be set to 00b for normal operation.

        // Configure the Receive Descriptor Minimum Threshold Size (RCTL.RDMTS) bits to thedesired value.
        // 00b = Free Buffer threshold is set to 1/2 of RDLEN.
        // 01b = Free Buffer threshold is set to 1/4 of RDLEN.
        // 10b = Free Buffer threshold is set to 1/8 of RDLEN.
        rctl |= (0b10 << 8);

        // Configure the Multicast Offset (RCTL.MO) bits to the desired value.
        // 0 I guess.

        // Set the Broadcast Accept Mode (RCTL.BAM) bit to 1b allowing the hardware to accept broadcast packets.
        rctl |= RCTL_BAM;

        // Configure the Receive Buffer Size (RCTL.BSIZE) bits to reflect the size of the receive buffers software provides to
        // hardware. Also configure the Buffer Extension Size (RCTL.BSEX) bits if receive buffer needs to be larger than 2048 bytes.
        // RCTL.BSEX = 0b:
        // 00b = 2048 Bytes.
        // 01b = 1024 Bytes.
        // 10b = 512 Bytes.
        // 1b1 = 256 Bytes.
        // RCTL.BSEX = 1b:
        // 00b = Reserved; software should not program this value.
        // 01b = 16384 Bytes.
        // 10b = 8192 Bytes.
        // 11b = 4096 Bytes.
        rctl |= RCTL_BSEX | RCTL_BSIZE;

        // Set the Strip Ethernet CRC (RCTL.SECRC) bit if the desire is for hardware to strip the CRC prior to DMA-ing the receive
        // packet to host memory.
        // rctl |= RCTL_SECRC;

        // TX init

        // Initialize TX Buffers
        for (uint32_t i = 0; i < N_TX_DESC; ++i)
        {
                pd->tbufs[i] = nullptr;
                pd->tdesc[i] = { .buffer_address = 0 };
        }

        // Allocate a region of memory for the transmit descriptor list. Software should insure this memory is aligned on a
        // paragraph (16-byte) boundary. Program the Transmit Descriptor Base Address (TDBAL/TDBAH) register(s) with the address of
        // the region. TDBAL is used for 32-bit addresses and both TDBAL and TDBAH are used for 64-bit addresses.
        uint64_t tb = pmap_extract(kernel_pmap, (vm_address_t)pd->tdesc);
        iowrite32((uint32_t)(tb & 0xFFFFFFFF), pd->ioaddr + TDBAL);
        iowrite32((uint32_t)((tb >> 32) & 0xFFFFFFFF), pd->ioaddr + TDBAH);

        // Set the Transmit Descriptor Length (TDLEN) register to the size (in bytes) of the descriptor ring. This register must be
        // 128-byte aligned.
        static_assert((SZ_TX_DESC & 0x7F) == 0);
        iowrite32(SZ_TX_DESC, pd->ioaddr + TDLEN);

        // The Transmit Descriptor Head and Tail (TDH/TDT) registers are initialized (by hardware) to 0b after a power-on or a
        // software initiated Ethernet controller reset.
        // Software should write 0b to both these registers to ensure this.
        iowrite32(0, pd->ioaddr + TDH);
        iowrite32(0, pd->ioaddr + TDT);

        // Initialize the Transmit Control Register (TCTL) for desired operation to include the following:

        // Set the Enable (TCTL.EN) bit to 1b for normal operation
        uint32_t tctl = TCTL_EN;

        // Set the Pad Short Packets (TCTL.PSP) bit to 1b.
        tctl |= TCTL_PSP;

        // Configure the Collision Threshold (TCTL.CT) to the desired value. Ethernet standard is 10h. This setting only has meaning
        // in half duplex mode.
        tctl |= TCTL_CT(0x10);

        // Configure the Collision Distance (TCTL.COLD) to its expected value. For full duplex operation, this value should be set
        // to 40h. For gigabit half duplex, this value should be set to 200h. For 10/100 half duplex, this value should be set to
        // 40h.
        tctl |= TCTL_COLD(0x40);

        // Program the Transmit IPG (TIPG) register with the following decimal values to get the minimum legal Inter Packet Gap:
        iowrite32(TIPG_IPGT(10) | TIPG_IPGR1(10) | TIPG_IPGR2(10), pd->ioaddr + TIPG);

        iface_add(dev->m_device.netdev);

        // ENABLE RX AND TX
        iowrite32(rctl, pd->ioaddr + RCTL);
        iowrite32(tctl, pd->ioaddr + TCTL);

        return 0;
}

// ********************************************************************************************************************************
static void remove(struct pci_dev *dev)
// ********************************************************************************************************************************
{
}

static struct pci_driver pci_driver = {
        .name     = "intel_8254x",
        .id_table = ids,
        .probe    = probe,
        .remove   = remove,
};

// ********************************************************************************************************************************
static int pci_skel_init(void)
// ********************************************************************************************************************************
{
        return pci_register_driver(&pci_driver);
}

// ********************************************************************************************************************************
static void pci_skel_exit(void)
// ********************************************************************************************************************************
{
        pci_unregister_driver(&pci_driver);
}

module_init(pci_skel_init);

module_exit(pci_skel_exit);
