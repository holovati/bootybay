#include <fcntl.h>
#include <sys/ttydefaults.h>
#include <sys/sysmacros.h>

#include <aux/init.h>

#include <etl/algorithm.hh>

#include <kernel/zalloc.h>

#include <emerixx/poll.h>
#include <emerixx/chrdev.h>
#include <emerixx/memory_rw.h>
#include <emerixx/ioctl_req.h>
#include <emerixx/vfs/vfs_file_data.h>

#include <emerixx/drivers/tty/tty_device_data.h>

#define MAX_PTY 0x20

#define PTS_MAJOR 19

#define PTM(f) ((ptm_t)((f)->m_private))

typedef struct ptm_data *ptm_t;

struct ptm_data
{
        tty_device_t m_tdev;
        integer_t m_slave_minor;
        poll_device_context_t m_pfd;
};

static chrdev_data s_ptmxdev;

ZONE_DEFINE(ptm_data, s_ptm_zone, MAX_PTY, MAX_PTY / 2, ZONE_EXHAUSTIBLE, MAX_PTY / 8);

// ********************************************************************************************************************************
static kern_return_t ptm_open(vfs_node_t, vfs_file_t a_file) // Master open
// ********************************************************************************************************************************
{
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t ptm_read(vfs_file_t a_file, memory_rdwr_t a_mrw) // Master read
// ********************************************************************************************************************************
{
        ptm_t ptm = PTM(a_file);

        kern_return_t retval = KERN_SUCCESS;

        char buf[0x400];

        natural_t oresid = a_mrw->uio_resid;

        tty_device_lock(ptm->m_tdev);

        while (KERN_STATUS_SUCCESS(retval) && (a_mrw->uio_resid > 0))
        {
                if (tty_device_output_possible(ptm->m_tdev))
                {
                        struct iovec iov = { .iov_base = buf, .iov_len = ARRAY_SIZE(buf) };

                        memory_rdwr_data memrw = { .uio_iov    = &iov,
                                                   .uio_iovcnt = 1,
                                                   .uio_offset = 0,
                                                   .uio_resid  = iov.iov_len,
                                                   .uio_segflg = UIO_SYSSPACE,
                                                   .uio_rw     = UIO_READ,
                                                   .aux        = {} };

                        retval = tty_device_output_start(ptm->m_tdev, &memrw);

                        if (KERN_STATUS_SUCCESS(retval))
                        {
                                retval = uiomove(buf, memrw.uio_offset, a_mrw);
                        }
                }
                else
                {

                        if (ptm->m_tdev->m_flags & TTY_DEVICE_FLAG_USER_CLOSED)
                        {
                                break;
                        }

                        if (a_file->m_oflags & O_NONBLOCK)
                        {
                                if ((oresid - a_mrw->uio_resid) == 0)
                                {
                                        retval = KERN_FAIL(EWOULDBLOCK);
                                }
                                else
                                {
                                        break;
                                }
                        }
                        else
                        {
                                retval = tty_device_wait(ptm->m_tdev, (void *)ptm_read);
                        }
                }
        }

        tty_device_unlock(ptm->m_tdev);

        return retval;
}

// ********************************************************************************************************************************
static kern_return_t ptm_write(vfs_file_t a_file, memory_rdwr_t a_mrw) // Master write
// ********************************************************************************************************************************
{
        ptm_t ptm = PTM(a_file);

        char buf[0x100];

        kern_return_t retval = KERN_SUCCESS;

        natural_t oresid = a_mrw->uio_resid;

        tty_device_lock(ptm->m_tdev);

        while (KERN_STATUS_SUCCESS(retval) && (oresid > 0))
        {
                if (tty_device_input_possible(ptm->m_tdev))
                {
                        natural_t xfercnt = a_mrw->uio_resid;

                        retval = uiomove(buf, ARRAY_SIZE(buf), a_mrw);

                        xfercnt -= a_mrw->uio_resid;

                        if (KERN_STATUS_SUCCESS(retval))
                        {
                                for (natural_t idx = 0; idx < xfercnt; ++idx, --oresid)
                                {
                                        tty_device_input(ptm->m_tdev, &buf[idx], 1);

                                        if (tty_device_input_possible(ptm->m_tdev) == false)
                                        {
                                                break;
                                        }
                                }
                        }
                }
                else
                {
                        if (ptm->m_tdev->m_flags & TTY_DEVICE_FLAG_USER_CLOSED)
                        {
                                break;
                        }

                        if (a_file->m_oflags & O_NONBLOCK)
                        {
                                if (oresid == a_mrw->uio_resid)
                                {
                                        retval = KERN_FAIL(EWOULDBLOCK);
                                }
                                else
                                {
                                        break;
                                }
                        }
                        else
                        {
                                retval = tty_device_wait(ptm->m_tdev, (void *)ptm_write);
                        }
                }
        }

        a_mrw->uio_resid = oresid;

        tty_device_unlock(ptm->m_tdev);

        return retval;
}

// ********************************************************************************************************************************
static kern_return_t ptm_ioctl(vfs_file_t a_file, ioctl_req_t a_ioctl_req) // Master ioctl
// ********************************************************************************************************************************
{
        ptm_t ptm = PTM(a_file);

        kern_return_t retval = KERN_SUCCESS;

        integer_t cmd = ioctl_req_cmdno(a_ioctl_req);

        tty_device_lock(ptm->m_tdev);

        switch (cmd)
        {
                case TIOCGWINSZ:
                case TIOCSWINSZ:
                {
                        // Get/Set window size.
                        // Argument: struct winsize *argp
                        retval = tty_device_forward_ioctl(ptm->m_tdev, a_ioctl_req);
                }
                break;

                case TIOCGPTN:
                {
                        // Get device unit number, which can be used to generate the filename of the pseudo terminal slave device.
                        // Argument: unsigned int *argp
                        unsigned int arg = (unsigned int)ptm->m_slave_minor;

                        retval = ioctl_req_copyout(a_ioctl_req, &arg);
                }
                break;

                case TIOCSPTLCK:
                {
                        // Set (if *argp is nonzero) or remove (if *argp is zero) the lock on the pseudo terminal slave device.
                        // Argument: int *argp

                        int argp;

                        retval = ioctl_req_copyin(a_ioctl_req, &argp);

                        if (KERN_STATUS_SUCCESS(retval))
                        {
                                if (argp != 0)
                                {
                                        log_debug("Lock slave");
                                }
                                else
                                {
                                        log_debug("Unlock slave");
                                }
                        }
                }
                break;

                default:
                        panic("Unhandled ptm ioctl %08X", cmd);
                        break;
        }

        tty_device_unlock(ptm->m_tdev);

        return retval;
}

// ********************************************************************************************************************************
static kern_return_t ptm_close(vfs_file_t a_file) // Master close
// ********************************************************************************************************************************
{
        ptm_t ptm = PTM(a_file);

        KASSERT(ptm->m_tdev->m_flags & TTY_DEVICE_FLAG_USER_CLOSED); // We kinda expect the slave to initiate the closing.

        KASSERT(KERN_STATUS_SUCCESS(tty_device_unregister(ptm->m_tdev)));

        s_ptm_zone.free(ptm);

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t ptm_poll(vfs_file_t a_file, struct pollfd **a_pfd) // Master poll
// ********************************************************************************************************************************
{
        ptm_t ptm = PTM(a_file);

        tty_device_lock(ptm->m_tdev);

        if ((*a_pfd)->events & (POLLIN | POLLRDNORM))
        {
                if (tty_device_output_possible(ptm->m_tdev)) // Is there data on the output queue
                {
                        (*a_pfd)->revents |= (short)((*a_pfd)->events & (POLLIN | POLLRDNORM));
                }
        }

        /* NOTE: POLLHUP and POLLOUT/POLLWRNORM are mutually exclusive */
        if (/*!ISSET(tp->t_cflag, CLOCAL) && !ISSET(tp->t_state, TS_CARR_ON)*/ ptm->m_tdev->m_flags & TTY_DEVICE_FLAG_USER_CLOSED)
        {
                (*a_pfd)->revents |= POLLHUP;
        }
        else if ((*a_pfd)->events & (POLLOUT | POLLWRNORM))
        {
                if (tty_device_input_possible(ptm->m_tdev)) // Is there space on the input queue
                {
                        (*a_pfd)->revents |= (short)((*a_pfd)->events & (POLLOUT | POLLWRNORM));
                }
        }

        tty_device_unlock(ptm->m_tdev);

        return poll_record(&ptm->m_pfd, a_pfd);
}

static struct vfs_file_operations_data s_ptm_ops = {
        .fopen  = ptm_open,                 //
        .fread  = ptm_read,                 //
        .fwrite = ptm_write,                //
        .flseek = vfs_file_op_lseek_espipe, //
        .fioctl = ptm_ioctl,                //
        .fclose = ptm_close,                //
        .fpoll  = ptm_poll                  //
};

// ********************************************************************************************************************************
static kern_return_t ptmdev_initialize(tty_device_t a_tdev)
// ********************************************************************************************************************************
{
        ptm_t ptm = PTM(a_tdev);

        ptm->m_tdev = a_tdev;

        // Input modes
        a_tdev->m_tio.c_iflag = TTYDEF_IFLAG;

        // Output modes
        a_tdev->m_tio.c_oflag = TTYDEF_OFLAG;

        // Control modes
        a_tdev->m_tio.c_cflag = TTYDEF_CFLAG;

        // Local modes
        a_tdev->m_tio.c_lflag = TTYDEF_LFLAG;

        // Line discipline
        a_tdev->m_tio.c_line = N_TTY;

        // Control characters
        a_tdev->m_tio.c_cc[VINTR]    = CINTR;
        a_tdev->m_tio.c_cc[VQUIT]    = CQUIT;
        a_tdev->m_tio.c_cc[VERASE]   = CERASE;
        a_tdev->m_tio.c_cc[VKILL]    = CKILL;
        a_tdev->m_tio.c_cc[VEOF]     = CEOF;
        a_tdev->m_tio.c_cc[VTIME]    = CTIME;
        a_tdev->m_tio.c_cc[VMIN]     = CMIN;
        a_tdev->m_tio.c_cc[VSWTC]    = 0;
        a_tdev->m_tio.c_cc[VSTART]   = CSTART;
        a_tdev->m_tio.c_cc[VSTOP]    = CSTOP;
        a_tdev->m_tio.c_cc[VSUSP]    = CSUSP;
        a_tdev->m_tio.c_cc[VEOL]     = CEOL;
        a_tdev->m_tio.c_cc[VREPRINT] = CREPRINT;
        a_tdev->m_tio.c_cc[VDISCARD] = CDISCARD;
        a_tdev->m_tio.c_cc[VWERASE]  = CWERASE;
        a_tdev->m_tio.c_cc[VLNEXT]   = CLNEXT;
        a_tdev->m_tio.c_cc[VEOL2]    = 0;

        a_tdev->m_tio.__c_ispeed = TTYDEF_SPEED; // 9600 baud
        a_tdev->m_tio.__c_ospeed = TTYDEF_SPEED; // 9600 baud

        a_tdev->m_ws = {};

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static void ptmdev_user_closed(tty_device_t a_tdev)
// ********************************************************************************************************************************
{
        ptm_t ptm = PTM(a_tdev);

        ptm->m_tdev->m_flags |= TTY_DEVICE_FLAG_USER_CLOSED;

        poll_wakeup(&ptm->m_pfd, POLLHUP);

        tty_device_notify(a_tdev, (void *)ptm_read);

        tty_device_notify(a_tdev, (void *)ptm_write);
}

// ********************************************************************************************************************************
static kern_return_t ptmdev_output_rdy(tty_device_t a_tdev)
// ********************************************************************************************************************************
{
        ptm_t ptm = PTM(a_tdev);

        tty_device_notify(a_tdev, (void *)ptm_read);

        poll_wakeup(&ptm->m_pfd, POLLIN | POLLRDNORM);

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t ptmdev_input_rdy(tty_device_t a_tdev)
// ********************************************************************************************************************************
{
        ptm_t ptm = PTM(a_tdev);

        tty_device_notify(a_tdev, (void *)ptm_write);

        poll_wakeup(&ptm->m_pfd, POLLOUT | POLLWRNORM);

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t ptmdev_output_rcv(tty_device_t a_tdev,
                                       void *a_data,
                                       natural_t a_datalen,
                                       natural_t *a_rdcnt,
                                       void *a_user_data)
// ********************************************************************************************************************************
{
        tty_device_unlock(a_tdev);
        memory_rdwr_t mrw = (memory_rdwr_t)(a_user_data);

        natural_t oresid = mrw->uio_resid;

        if (oresid == 0)
        {
                *a_rdcnt = 0;
                return KERN_SUCCESS;
        }

        kern_return_t retval = uiomove(a_data, a_datalen, mrw);

        if (KERN_STATUS_SUCCESS(retval))
        {
                *a_rdcnt = (oresid - mrw->uio_resid);
        }
        else
        {
                *a_rdcnt = 0;
        }
        tty_device_lock(a_tdev);
        return retval;
}

struct tty_device_ops_data s_ptmdev_ops = {
        .initialize      = ptmdev_initialize,  //
        .user_closed     = ptmdev_user_closed, //
        .input_processed = ptmdev_input_rdy,   //
        .output_pending  = ptmdev_output_rdy,  //
        .output_receive  = ptmdev_output_rcv   //
};

// ********************************************************************************************************************************
static kern_return_t ptmx_open(vfs_node_t, vfs_file_t a_file) // Master open
// ********************************************************************************************************************************
{
        ptm_t ptm = s_ptm_zone.alloc();

        if (ptm == nullptr)
        {
                return KERN_FAIL(ENOMEM);
        }

        // Allocate a slave, and set the ptm struct as device private data

        kern_return_t retval;
        for (ptm->m_slave_minor = 0; ptm->m_slave_minor < MAX_PTY; ptm->m_slave_minor += 1)
        {
                retval = tty_device_register("pts", makedev(PTS_MAJOR, ptm->m_slave_minor), &s_ptmdev_ops, ptm);

                if (retval != KERN_FAIL(EINVAL))
                {
                        break;
                }
        }

        if (KERN_STATUS_FAILURE(retval))
        {
                s_ptm_zone.free(ptm);
                return KERN_FAIL(ENOMEM);
        }

        poll_device_ctx_init(&ptm->m_pfd);

        a_file->m_private = ptm;
        a_file->m_fops    = &s_ptm_ops;

        return KERN_SUCCESS;
}

static struct vfs_file_operations_data s_ptmxdev_ops = {
        .fopen = ptmx_open //
};

// ********************************************************************************************************************************
static kern_return_t pt_init()
// ********************************************************************************************************************************
{
        kern_return_t retval = driver_chrdev_alloc(&s_ptmxdev, "ptmx", &s_ptmxdev_ops, makedev(18, 0), 1);
        return retval;
}

device_initcall(pt_init);
