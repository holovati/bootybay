#include <kernel/vm/vm_map.h>
#include <kernel/vm/vm_kern.h>

#include <etl/algorithm.hh>

#include <emerixx/drivers/tty/ldisc.h>

#include "ttyq.h"

#define TTYQ_MASK (TTYQ_SIZE - 1)

static_assert(etl::is_power_of_two(TTYQ_SIZE));

// ********************************************************************************************************************************
kern_return_t ttyq_initialize(ttyq_t a_ttyq)
// ********************************************************************************************************************************
{
        vm_map_lock(kernel_map);

        vm_map_entry_t vment = nullptr;

        kern_return_t retval = vm_map_find_entry(kernel_map, (vm_address_t *)&a_ttyq->m_buffer, TTYQ_SIZE << 1, 0, nullptr, &vment);

        a_ttyq->m_rpos = a_ttyq->m_wpos = 0;

        if (retval != KERN_SUCCESS)
        {
                retval = KERN_FAIL(ENOMEM);
                goto out;
        }

        vment->flags |= VM_MAP_ENTRY_MIRROR_OBJECT;
        vment->wired_count = 0;

out:
        vm_map_unlock(kernel_map);
        return retval;
}

// ********************************************************************************************************************************
void ttyq_destroy(ttyq_t a_ttyq)
// ********************************************************************************************************************************
{
        vm_address_t vma = (vm_address_t)&a_ttyq->m_buffer;
        vm_map_remove(kernel_map, vma, vma + (TTYQ_SIZE << 1));
}

// ********************************************************************************************************************************
tty_char *ttyq_read_buffer(ttyq_t a_ttyq, natural_t *a_rd_cnt_out)
// ********************************************************************************************************************************
{
        *a_rd_cnt_out = (a_ttyq->m_wpos - a_ttyq->m_rpos) & TTYQ_MASK;
        return &a_ttyq->m_buffer[a_ttyq->m_rpos & TTYQ_MASK];
}

// ********************************************************************************************************************************
tty_char *ttyq_write_buffer(ttyq_t a_ttyq, natural_t *a_wr_cnt_out)
// ********************************************************************************************************************************
{
        *a_wr_cnt_out = TTYQ_MASK - ((a_ttyq->m_wpos - a_ttyq->m_rpos) & TTYQ_MASK);
        return &a_ttyq->m_buffer[a_ttyq->m_wpos & TTYQ_MASK];
}

// ********************************************************************************************************************************
void ttyq_read_advance(ttyq_t a_ttyq, natural_t a_cnt)
// ********************************************************************************************************************************
{
        natural_t readcnt = (a_ttyq->m_wpos - a_ttyq->m_rpos) & TTYQ_MASK;
        a_ttyq->m_rpos += etl::min(readcnt, a_cnt);

        if (a_ttyq->m_rpos == a_ttyq->m_wpos)
        {
                a_ttyq->m_rpos = a_ttyq->m_wpos = 0;
        }
}

// ********************************************************************************************************************************
void ttyq_write_advance(ttyq_t a_ttyq, natural_t a_cnt)
// ********************************************************************************************************************************
{
        natural_t writecnt = (TTYQ_MASK /* naming :() */ - ((a_ttyq->m_wpos - a_ttyq->m_rpos) & TTYQ_MASK));
        a_ttyq->m_wpos += etl::min(writecnt, a_cnt);
}
