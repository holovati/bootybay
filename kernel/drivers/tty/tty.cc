#include <etl/algorithm.hh>

#include <kernel/zalloc.h>

#include <emerixx/vfs/vfs_file_data.h>

#include <emerixx/sleep.h>
#include <emerixx/process.h>
#include <emerixx/memory_rw.h>
#include <emerixx/poll.h>

#include <emerixx/chrdev.h>
#include <emerixx/drivers/tty/ldisc.h>
#include <emerixx/drivers/tty/tty_device_data.h>

#define FIL_TTY_DRV(f)  ((((struct tty_driver_data *)(f)->m_private)))
#define DEV_TTY_DRV(d)  (queue_containing_record(d, struct tty_driver_data, m_dev))
#define LDSC_TTY_DRV(d) (d->m_drv)

static tty_ldisc_ops_t s_ldisc_ops[0x20];

struct tty_driver_data
{
        struct chrdev_data m_chrdev;
        struct tty_device_data m_dev;
        poll_device_context_t m_pfd;
        struct lock_data m_lock;
        integer_t m_open_count;
        tty_ldisc_t m_tty;
};

ZONE_DEFINE(tty_driver_data, s_tty_driver_zone, 0x40, 0x10, ZONE_EXHAUSTIBLE, 0x10);

// ********************************************************************************************************************************
static kern_return_t tty_driver_open(vfs_node_t a_vfsn, vfs_file_t a_file)
// ********************************************************************************************************************************
{
        tty_driver_t ttyd = FIL_TTY_DRV(a_file);

        tty_ldisc_t tty = ttyd->m_tty;

        lock_write(&ttyd->m_lock);

        ttyd->m_open_count++;

        kern_return_t retval = s_ldisc_ops[ttyd->m_dev.m_tio.c_line]->open(tty, a_file->m_oflags, ttyd->m_chrdev.m_beg);

        lock_done(&ttyd->m_lock);

        return retval;
}

// ********************************************************************************************************************************
static int tty_driver_close(vfs_file_t a_file)
// ********************************************************************************************************************************
{
        tty_driver_t ttyd = FIL_TTY_DRV(a_file);

        tty_ldisc_t tty = ttyd->m_tty;

        if ((--(ttyd->m_open_count)) == 0)
        {
                s_ldisc_ops[ttyd->m_dev.m_tio.c_line]->close(tty);
                ttyd->m_dev.m_device_ops->user_closed(&ttyd->m_dev);
        }

        return 0;
}

// ********************************************************************************************************************************
static int tty_driver_rw(vfs_file_t a_file, memory_rdwr_t a_memrw)
// ********************************************************************************************************************************
{
        tty_driver_t ttyd = FIL_TTY_DRV(a_file);

        tty_ldisc_t tty = ttyd->m_tty;

        lock_write(&ttyd->m_lock);

        kern_return_t retval;

        if (a_memrw->uio_rw == UIO_READ)
        {
                retval = s_ldisc_ops[ttyd->m_dev.m_tio.c_line]->read(tty, &ttyd->m_dev, a_file->m_oflags, a_memrw);
        }
        else
        {
                retval = s_ldisc_ops[ttyd->m_dev.m_tio.c_line]->write(tty, &ttyd->m_dev, a_file->m_oflags, a_memrw);
        }

        lock_done(&ttyd->m_lock);

        return retval;
}

// ********************************************************************************************************************************
static int tty_driver_ioctl(vfs_file_t a_file, ioctl_req_t a_ioctl_req)
// ********************************************************************************************************************************
{
        tty_driver_t ttyd = FIL_TTY_DRV(a_file);

        tty_ldisc_t tty = ttyd->m_tty;

        lock_write(&ttyd->m_lock);

        kern_return_t retval = s_ldisc_ops[ttyd->m_dev.m_tio.c_line]->ioctl(tty, &ttyd->m_dev, ttyd->m_chrdev.m_beg, a_ioctl_req);

        lock_done(&ttyd->m_lock);

        return retval;
}

// ********************************************************************************************************************************
static int tty_driver_poll(vfs_file_t a_file, pollfd **a_pfd)
// ********************************************************************************************************************************
{
        tty_driver_t ttyd = FIL_TTY_DRV(a_file);

        tty_ldisc_t tty = ttyd->m_tty;

        lock_write(&ttyd->m_lock);

        kern_return_t retval = s_ldisc_ops[ttyd->m_dev.m_tio.c_line]->poll(tty, &ttyd->m_dev, (*a_pfd)->events);

        lock_done(&ttyd->m_lock);

        if (retval > 0)
        {
                (*a_pfd)->revents = (short)retval;
        }

        return poll_record(&ttyd->m_pfd, a_pfd);
}

static struct vfs_file_operations_data s_condev_ops = {
        .fopen  = tty_driver_open,          //
        .fread  = tty_driver_rw,            //
        .fwrite = tty_driver_rw,            //
        .flseek = vfs_file_op_lseek_espipe, //
        .fioctl = tty_driver_ioctl,         //
        .fclose = tty_driver_close,         //
        .fpoll  = tty_driver_poll           //
};

// ********************************************************************************************************************************
boolean_t tty_device_input_possible(tty_device_t a_tty_dev)
// ********************************************************************************************************************************
{
        tty_driver_t ttyd = DEV_TTY_DRV(a_tty_dev);

        tty_ldisc_t tty = ttyd->m_tty;

        return s_ldisc_ops[ttyd->m_dev.m_tio.c_line]->input_possible(tty);
}

// ********************************************************************************************************************************
kern_return_t tty_device_input(tty_device_t a_tty_dev, char const *a_char, natural_t a_char_len)
// ********************************************************************************************************************************
{
        tty_driver_t ttyd = DEV_TTY_DRV(a_tty_dev);

        tty_ldisc_t tty = ttyd->m_tty;

        kern_return_t retval = s_ldisc_ops[ttyd->m_dev.m_tio.c_line]->dev_input(tty, &ttyd->m_dev, a_char, a_char_len);

        return retval;
}

// ********************************************************************************************************************************
boolean_t tty_device_output_possible(tty_device_t a_tty_dev)
// ********************************************************************************************************************************
{
        tty_driver_t ttyd = DEV_TTY_DRV(a_tty_dev);

        tty_ldisc_t tty = ttyd->m_tty;

        return s_ldisc_ops[ttyd->m_dev.m_tio.c_line]->output_possible(tty);
}

// ********************************************************************************************************************************
kern_return_t tty_device_output_start(tty_device_t a_tty_dev, void *a_user_data)
// ********************************************************************************************************************************
{
        tty_driver_t ttyd = DEV_TTY_DRV(a_tty_dev);

        tty_ldisc_t tty = ttyd->m_tty;

        kern_return_t retval = s_ldisc_ops[ttyd->m_dev.m_tio.c_line]->dev_output_start(tty, &ttyd->m_dev, a_user_data);

        return retval;
}

// ********************************************************************************************************************************
kern_return_t tty_device_forward_ioctl(tty_device_t a_tty_dev, ioctl_req_t a_ioctl_req)
// ********************************************************************************************************************************
{
        tty_driver_t ttyd = DEV_TTY_DRV(a_tty_dev);

        tty_ldisc_t tty = ttyd->m_tty;

        kern_return_t retval = s_ldisc_ops[ttyd->m_dev.m_tio.c_line]->ioctl(tty, &ttyd->m_dev, ttyd->m_chrdev.m_beg, a_ioctl_req);

        return retval;
}

// ********************************************************************************************************************************
kern_return_t tty_device_wait(tty_device_t a_tty_dev, void *a_condvar)
// ********************************************************************************************************************************
{
        tty_driver_t ttyd = DEV_TTY_DRV(a_tty_dev);
        return uthread_sleep_timeout(a_condvar, 0, &ttyd->m_lock);
}

// ********************************************************************************************************************************
void tty_device_notify(tty_device_t a_tty_dev, void *a_condvar)
// ********************************************************************************************************************************
{
        uthread_wakeup(a_condvar);
}

// ********************************************************************************************************************************
boolean_t tty_device_lock_try(tty_device_t a_tty_dev)
// ********************************************************************************************************************************
{
        tty_driver_t ttyd = DEV_TTY_DRV(a_tty_dev);
        return lock_try_write(&ttyd->m_lock);
}

// ********************************************************************************************************************************
void tty_device_lock(tty_device_t a_tty_dev)
// ********************************************************************************************************************************
{
        tty_driver_t ttyd = DEV_TTY_DRV(a_tty_dev);
        lock_write(&ttyd->m_lock);
}

// ********************************************************************************************************************************
void tty_device_unlock(tty_device_t a_tty_dev)
// ********************************************************************************************************************************
{
        tty_driver_t ttyd = DEV_TTY_DRV(a_tty_dev);
        lock_done(&ttyd->m_lock);
}

// ********************************************************************************************************************************
kern_return_t tty_device_register(const char *a_name, dev_t a_dev, tty_device_ops_t a_devops, void *a_dev_priv_data)
// ********************************************************************************************************************************
{
        tty_driver_t ttyd = s_tty_driver_zone.alloc();

        if (ttyd == nullptr)
        {
                return KERN_FAIL(ENOMEM);
        }

        kern_return_t retval = driver_chrdev_alloc(&ttyd->m_chrdev, a_name, &s_condev_ops, a_dev, 1);

        if (KERN_STATUS_FAILURE(retval))
        {
                s_tty_driver_zone.free(ttyd);
                return retval;
        }

        retval = s_ldisc_ops[N_TTY]->create(&ttyd->m_tty);

        if (KERN_STATUS_FAILURE(retval))
        {
                driver_chrdev_free(&ttyd->m_chrdev);
                s_tty_driver_zone.free(ttyd);
        }

        ttyd->m_tty->m_drv = ttyd;

        poll_device_ctx_init(&ttyd->m_pfd);

        lock_init(&ttyd->m_lock, true);

        ttyd->m_open_count = 0;

        ttyd->m_dev.m_device_ops = a_devops;

        ttyd->m_dev.m_private = a_dev_priv_data;

        a_devops->initialize(&ttyd->m_dev);

        if (KERN_STATUS_FAILURE(retval))
        {
                s_ldisc_ops[N_TTY]->destroy(ttyd->m_tty);
                driver_chrdev_free(&ttyd->m_chrdev);
                s_tty_driver_zone.free(ttyd);
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t tty_device_unregister(tty_device_t a_tty_dev)
// ********************************************************************************************************************************
{
        tty_driver_t ttyd = DEV_TTY_DRV(a_tty_dev);

        KASSERT(ttyd->m_open_count == 0);

        kern_return_t retval = driver_chrdev_free(&ttyd->m_chrdev);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        s_ldisc_ops[N_TTY]->destroy(ttyd->m_tty);

        *ttyd = {};

        s_tty_driver_zone.free(ttyd);

        return retval;
}

// ********************************************************************************************************************************
kern_return_t tty_ldisc_register(natural_t a_line_no, tty_ldisc_ops_t a_line_ops)
// ********************************************************************************************************************************
{
        if (a_line_no > ARRAY_SIZE(s_ldisc_ops))
        {
                return KERN_FAIL(EINVAL);
        }

        if (s_ldisc_ops[a_line_no] != nullptr)
        {
                return KERN_FAIL(EEXIST);
        }

        s_ldisc_ops[a_line_no] = a_line_ops;

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t tty_ldisc_wait(tty_ldisc_t a_tty, void *a_condvar)
// ********************************************************************************************************************************
{
        return uthread_sleep_timeout(a_condvar, 0, &LDSC_TTY_DRV(a_tty)->m_lock);
}

// ********************************************************************************************************************************
kern_return_t tty_ldisc_notify(tty_ldisc_t, void *a_condvar)
// ********************************************************************************************************************************
{
        uthread_wakeup(a_condvar);
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t tty_ldisc_lock(tty_ldisc_t a_tty)
// ********************************************************************************************************************************
{
        lock_write(&LDSC_TTY_DRV(a_tty)->m_lock);
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t tty_ldisc_unlock(tty_ldisc_t a_tty)
// ********************************************************************************************************************************
{
        lock_done(&LDSC_TTY_DRV(a_tty)->m_lock);
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t tty_ldisc_poll_event(tty_ldisc_t a_ldisc, integer_t a_event)
// ********************************************************************************************************************************
{
        poll_wakeup(&a_ldisc->m_drv->m_pfd, (short)a_event);
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t tty_driver_early_init()
// ********************************************************************************************************************************
{
        for (tty_ldisc_ops_t &e : s_ldisc_ops)
        {
                e = nullptr;
        }

        return KERN_SUCCESS;
}

early_initcall(tty_driver_early_init);
