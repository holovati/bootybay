#pragma once

#define TTYQ_SIZE 0x80000

typedef struct ttyq_data *ttyq_t;

struct ttyq_data
{
        tty_char *m_buffer;
        natural_t m_rpos;
        natural_t m_wpos;
};

kern_return_t ttyq_initialize(ttyq_t a_ttyq);

void ttyq_destroy(ttyq_t a_ttyq);

tty_char *ttyq_read_buffer(ttyq_t a_ttyq, natural_t *a_rd_cnt_out);

tty_char *ttyq_write_buffer(ttyq_t a_ttyq, natural_t *a_rd_cnt_out);

void ttyq_read_advance(ttyq_t a_ttyq, natural_t a_cnt);

void ttyq_write_advance(ttyq_t a_ttyq, natural_t a_cnt);

static inline boolean_t ttyq_is_empty(ttyq_t a_ttyq)
{
        natural_t rdcnt = 0;
        ttyq_read_buffer(a_ttyq, &rdcnt);
        return rdcnt == 0;
}

static inline boolean_t ttyq_is_full(ttyq_t a_ttyq)
{
        natural_t wrcnt = 0;
        ttyq_write_buffer(a_ttyq, &wrcnt);
        return wrcnt == 0;
}
