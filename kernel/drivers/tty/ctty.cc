#include <sys/ioctl.h>
#include <sys/sysmacros.h>

#include <aux/init.h>

#include <kernel/lock.h>

#include <emerixx/ioctl_req.h>
#include <emerixx/vfs/vfs_file.h>
#include <emerixx/vfs/vfs_file_data.h>
#include <emerixx/process.h>

#include <emerixx/chrdev.h>

static chrdev_data s_cttydev;

// ********************************************************************************************************************************
static kern_return_t ctty_open(vfs_node_t a_vfsn, vfs_file_t a_vfsf)
// ********************************************************************************************************************************
{
        kern_return_t retval = KERN_FAIL(ENXIO);

        chrdev_t cttydev;
        retval = driver_chrdev_lookup(utask_self()->m_ctty, &cttydev);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        a_vfsf->m_private = cttydev;

        cttydev->m_fops->fopen(a_vfsn, a_vfsf);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        a_vfsf->m_fops = cttydev->m_fops;

        return retval;
}

static struct vfs_file_operations_data s_cttydev_ops = {
        .fopen = ctty_open //
};

static kern_return_t ctty_device_init()
{
        kern_return_t retval = driver_chrdev_alloc(&s_cttydev, "tty", &s_cttydev_ops, makedev(17, 0), 1);
        return retval;
}

device_initcall(ctty_device_init);
