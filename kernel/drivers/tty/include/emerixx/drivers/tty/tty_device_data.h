#pragma once

#include <termios.h>
#include <sys/ioctl.h>

#if !defined(__NEED_ioctl_req)
#define __NEED_ioctl_req
#endif

#include <emerixx/vfs/vfs_types.h>
#include <etl/bit.hh>

#include <emerixx/drivers/tty/tty.h>

#define TTY_DEVICE_FLAG_INPUT_SUSPENDED  etl::bit<decltype(tty_device_data::m_flags), 0>::value
#define TTY_DEVICE_FLAG_OUTPUT_SUSPENDED etl::bit<decltype(tty_device_data::m_flags), 1>::value
#define TTY_DEVICE_FLAG_USER_CLOSED      etl::bit<decltype(tty_device_data::m_flags), 2>::value

struct tty_device_ops_data
{
        kern_return_t (*initialize)(tty_device_t);
        void (*user_closed)(tty_device_t);
        kern_return_t (*input_processed)(tty_device_t);
        kern_return_t (*output_pending)(tty_device_t);
        kern_return_t (*output_receive)(tty_device_t, void *, natural_t, natural_t *, void *);
};

struct tty_device_data
{
        tty_device_ops_t m_device_ops;
        natural_t m_flags;
        void *m_private;
        struct termios m_tio;
        struct winsize m_ws;
        int pad;
};

boolean_t tty_device_input_possible(tty_device_t a_tty_dev);

kern_return_t tty_device_input(tty_device_t a_tty_dev, char const *a_char, natural_t a_char_len);

boolean_t tty_device_output_possible(tty_device_t a_tty_dev);

kern_return_t tty_device_output_start(tty_device_t a_tty_dev, void *a_user_data);

kern_return_t tty_device_forward_ioctl(tty_device_t a_tty_dev, ioctl_req_t a_req);

kern_return_t tty_device_wait(tty_device_t a_tty_dev, void *a_condvar);

void tty_device_notify(tty_device_t a_tty_dev, void *a_condvar);

boolean_t tty_device_lock_try(tty_device_t a_tty_dev);

void tty_device_lock(tty_device_t a_tty_dev);

void tty_device_unlock(tty_device_t a_tty_dev);

kern_return_t tty_device_register(const char *a_name, dev_t a_dev, tty_device_ops_t a_devops, void *a_dev_priv_data);

kern_return_t tty_device_unregister(tty_device_t a_tty_dev);
