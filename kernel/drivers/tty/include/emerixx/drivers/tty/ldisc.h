#pragma once

#if !defined(__NEED_ioctl_req)
#define __NEED_ioctl_req
#endif

#include <emerixx/vfs/vfs_types.h>

#include <emerixx/drivers/tty/tty.h>

struct tty_ldisc_base
{
        tty_driver_t m_drv;
};

struct tty_ldisc_ops_data
{
        kern_return_t (*create)(tty_ldisc_t *);
        kern_return_t (*destroy)(tty_ldisc_t);
        kern_return_t (*open)(tty_ldisc_t, integer_t, dev_t);
        kern_return_t (*close)(tty_ldisc_t);
        kern_return_t (*read)(tty_ldisc_t, tty_device_t, integer_t, struct uio *uio);
        kern_return_t (*write)(tty_ldisc_t, tty_device_t, integer_t, struct uio *uio);
        kern_return_t (*ioctl)(tty_ldisc_t, tty_device_t, dev_t, ioctl_req_t cmd);
        kern_return_t (*poll)(tty_ldisc_t, tty_device_t, integer_t a_events);
        kern_return_t (*dev_input)(tty_ldisc_t, tty_device_t, char const *, natural_t);
        kern_return_t (*dev_output_start)(tty_ldisc_t, tty_device_t, void *);
        boolean_t (*input_possible)(tty_ldisc_t);
        boolean_t (*output_possible)(tty_ldisc_t);
};

kern_return_t tty_ldisc_register(natural_t a_line_no, tty_ldisc_ops_t a_line_ops);

kern_return_t tty_ldisc_wait(tty_ldisc_t, void *a_condvar);

kern_return_t tty_ldisc_notify(tty_ldisc_t, void *a_condvar);

kern_return_t tty_ldisc_lock(tty_ldisc_t);

kern_return_t tty_ldisc_unlock(tty_ldisc_t);

kern_return_t tty_ldisc_poll_event(tty_ldisc_t, integer_t event);
