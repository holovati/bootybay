#pragma once

typedef /*int32_t*/ char tty_char;

typedef struct tty_device_ops_data *tty_device_ops_t;

typedef struct tty_device_data *tty_device_t;

typedef struct tty_ldisc_base *tty_ldisc_t;

typedef struct tty_driver_data *tty_driver_t;

typedef struct tty_ldisc_ops_data *tty_ldisc_ops_t;
