#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <poll.h>
#include <ctype.h>
#include <sys/sysmacros.h>

#include <etl/algorithm.hh>
#include <etl/cstring.hh>

#include <kernel/zalloc.h>

#include <emerixx/ioctl_req.h>
#include <emerixx/process.h>

#include <emerixx/drivers/tty/ldisc.h>
#include <emerixx/drivers/tty/tty_device_data.h>

#include "ttyq.h"

#ifndef OXTABS
#define OXTABS XTABS /* Makes more sense since its a output thing*/
#endif

#define TERMIOS_IFLAG(dev, flg) (((dev)->m_tio.c_iflag & (flg)) == (flg))
#define TERMIOS_OFLAG(dev, flg) (((dev)->m_tio.c_oflag & (flg)) == (flg))
#define TERMIOS_LFLAG(dev, flg) (((dev)->m_tio.c_lflag & (flg)) == (flg))
#define TERMIOS_CCHAR(dev, i)   (((dev)->m_tio.c_cc[(i)]))

#define CHAR_CR  '\r'
#define CHAR_TAB '\t'
#define CHAR_NL  '\n'
#define CHAR_DEL '\177'

#define TAB_SIZE 8
#define TAB_MASK (TAB_SIZE - 1)

#define NTTY(t) ((ntty_t)(t))

#define NTTY_NONE 0

#define NTTY_DISCARD etl::bit<decltype(ntty_data::m_flags), 0>::value
#define NTTY_LNEXT   etl::bit<decltype(ntty_data::m_flags), 1>::value

/* Internal globals */
typedef struct ntty_data : tty_ldisc_base
{
        struct ttyq_data m_iputq;
        struct ttyq_data m_oputq;

        natural_t m_iput_pos;
        natural_t m_oput_pos;

        int32_t m_canon_buffer[_POSIX_MAX_CANON];
        int32_t m_canon_buflen;

        integer_t m_flags;

        pid_t m_sid;
        pid_t m_fg_pgid;
} *ntty_t;

ZONE_DEFINE(ntty_data, s_ntty_zone, 0x40, 0x10, ZONE_EXHAUSTIBLE, 0x10);

// ********************************************************************************************************************************
static kern_return_t ntty_create(tty_ldisc_t *a_tty_out)
// ********************************************************************************************************************************
{
        ntty_t ntty = s_ntty_zone.alloc();

        if (ntty == nullptr)
        {
                return KERN_FAIL(ENOMEM);
        }

        kern_return_t retval = ttyq_initialize(&ntty->m_iputq);

        if (KERN_STATUS_FAILURE(retval))
        {
                s_ntty_zone.free(ntty);
                goto out;
        }

        retval = ttyq_initialize(&ntty->m_oputq);

        if (KERN_STATUS_FAILURE(retval))
        {
                ttyq_destroy(&ntty->m_iputq);
                s_ntty_zone.free(ntty);
                goto out;
        }

        ntty->m_iput_pos = ntty->m_oput_pos = 0;

        etl::clear(ntty->m_canon_buffer);
        ntty->m_canon_buflen = 0;

        ntty->m_flags = NTTY_NONE;

        ntty->m_fg_pgid = ntty->m_sid = 0;

        *a_tty_out = ntty;

out:
        return retval;
}

// ********************************************************************************************************************************
static kern_return_t ntty_open(tty_ldisc_t a_tty, integer_t a_oflags, dev_t a_devno)
// ********************************************************************************************************************************
{
        ntty_t tty = NTTY(a_tty);

        pid_t pid = getpid();
        pid_t sid = getsid(pid);
        pid_t gid = getpgid(pid);

        dev_t ctty = getctty();

        boolean_t is_session_leader = sid == pid;

        if (is_session_leader && ((a_oflags & O_NOCTTY) == 0) && (ctty == makedev(0xFF, 0xFF)))
        {
                ctty = a_devno;
                if (tty->m_sid == 0)
                {
                        tty->m_sid     = sid;
                        tty->m_fg_pgid = gid;
                        setctty(ctty);
                }
        }

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t ntty_close(tty_ldisc_t)
// ********************************************************************************************************************************
{
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t ntty_destroy(tty_ldisc_t a_tty)
// ********************************************************************************************************************************
{
        ntty_t tty = NTTY(a_tty);

        ttyq_destroy(&tty->m_iputq);
        ttyq_destroy(&tty->m_oputq);

        *tty = {};

        s_ntty_zone.free(tty);

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t ntty_ioctl(tty_ldisc_t a_tty, tty_device_t tdev, dev_t a_devno, ioctl_req_t a_ioctl_req)
// ********************************************************************************************************************************
{
        ntty_t tty = NTTY(a_tty);

        kern_return_t retval = KERN_SUCCESS;

        integer_t cmd = ioctl_req_cmdno(a_ioctl_req);

        switch (cmd)
        {
                        //////////////////////////////////////////////////////////////////////////////////////
                        //                        Get and set terminal attributes                           //
                        //////////////////////////////////////////////////////////////////////////////////////
                case TCGETS:
                {
                        // Get the current serial port settings.
                        // Argument: struct termios *argp
                        retval = ioctl_req_copyout(a_ioctl_req, &tdev->m_tio);
                }
                break;

                case TCSETS:
                {
                        // Set the current serial port settings.
                        // Argument: struct termios *argp
                        struct termios newtio;
                        retval = ioctl_req_copyin(a_ioctl_req, &newtio);

                        if (KERN_STATUS_SUCCESS(retval))
                        {
                                tdev->m_tio = newtio;
                        }
                }
                break;

                case TCSETSW:
                {
                        // Allow the output buffer to drain, and set the current serial port settings.
                        // Argument: struct termios *argp
                        struct termios newtio;
                        retval = ioctl_req_copyin(a_ioctl_req, &newtio);

                        while (KERN_STATUS_SUCCESS(retval) && (ttyq_is_empty(&tty->m_oputq) == false))
                        {
                                tdev->m_device_ops->output_pending(tdev);

                                retval = tty_ldisc_wait(a_tty, &tty->m_oputq);
                        }

                        if (KERN_STATUS_SUCCESS(retval))
                        {
                                tdev->m_tio = newtio;
                        }
                }
                break;

                case TCSETSF:
                {
                        // Allow the output buffer to drain, discard pending input, and set the current serial port settings.
                        // Argument: const struct termios *argp

                        struct termios newtio;
                        retval = ioctl_req_copyin(a_ioctl_req, &newtio);

                        while (KERN_STATUS_SUCCESS(retval) && (ttyq_is_empty(&tty->m_oputq) == false))
                        {
                                tdev->m_device_ops->output_pending(tdev);

                                retval = tty_ldisc_wait(a_tty, &tty->m_oputq);
                        }

                        if (KERN_STATUS_SUCCESS(retval))
                        {
                                tty->m_iput_pos = 0;

                                natural_t rdcnt = 0;
                                ttyq_read_buffer(&tty->m_iputq, &rdcnt);
                                ttyq_read_advance(&tty->m_iputq, rdcnt);

                                tdev->m_device_ops->input_processed(tdev);

                                tdev->m_tio = newtio;
                        }
                }
                break;

                        //////////////////////////////////////////////////////////////////////////////////////
                        //                              Software flow control                               //
                        //////////////////////////////////////////////////////////////////////////////////////
                case TCXONC:
                {
                        // Suspends transmission or reception of data, depending on the value of action.
                        // Argument: int arg
                        switch ((int)ioctl_req_arg(a_ioctl_req))
                        {
                                case TCOOFF:
                                {
                                        // Suspends output.
                                        tdev->m_flags |= TTY_DEVICE_FLAG_OUTPUT_SUSPENDED;
                                }
                                break;
                                case TCOON:
                                {
                                        // Restarts suspended output.
                                        tdev->m_flags &= ~TTY_DEVICE_FLAG_OUTPUT_SUSPENDED;
                                        tdev->m_device_ops->output_pending(tdev);
                                }
                                break;
                                case TCIOFF:
                                {
                                        // Transmits a STOP char, which stops the terminal dev from transmitting data to the system.
                                        panic("Implement TCIOFF");
                                }
                                break;
                                case TCION:
                                {
                                        // Transmits a START char, which starts the terminal dev transmitting data to the system.
                                        panic("Implement TCION");
                                }
                                break;

                                default:
                                        retval = KERN_FAIL(EINVAL);
                                        break;
                        }
                }
                break;
                        //////////////////////////////////////////////////////////////////////////////////////
                        //                              Controlling terminal                                //
                        //////////////////////////////////////////////////////////////////////////////////////
                case TIOCSCTTY:
                {
                        // Make the given terminal the controlling terminal of the calling process.
                        // The calling process must be a session leader and not have a controlling terminal already.
                        // Argument: int arg ( For this case, arg should be specified as zero )
                        pid_t pid = getpid();
                        pid_t sid = getsid(pid);
                        pid_t gid = getpgid(pid);

                        dev_t ctty = getctty();

                        boolean_t is_session_leader = sid == pid;

                        if (is_session_leader && (ctty == makedev(0xFF, 0xFF)))
                        {
                                ctty = a_devno;
                                if (tty->m_sid == 0)
                                {
                                        tty->m_sid     = sid;
                                        tty->m_fg_pgid = gid;
                                        setctty(ctty);
                                }
                        }
                }
                break;

                        //////////////////////////////////////////////////////////////////////////////////////
                        //                           Process group and session ID                           //
                        //////////////////////////////////////////////////////////////////////////////////////
                case TIOCGPGRP:
                {
                        // Get the process group ID of the foreground process group on this terminal.
                        // Argument: pid_t *argp
                        retval = ioctl_req_copyout(a_ioctl_req, &tty->m_fg_pgid);
                }
                break;

                case TIOCSPGRP:
                {
                        // Set the foreground process group ID of this terminal.
                        // Argument: const pid_t *argp
                        pid_t new_pgid;
                        retval = ioctl_req_copyin(a_ioctl_req, &new_pgid);

                        if (KERN_STATUS_SUCCESS(retval))
                        {
                                tty->m_fg_pgid = new_pgid;
                        }
                }
                break;

                        //////////////////////////////////////////////////////////////////////////////////////
                        //                           Get and set window size                                //
                        //////////////////////////////////////////////////////////////////////////////////////
                case TIOCGWINSZ:
                {
                        // Get window size.
                        // Argument: struct winsize *argp
                        retval = ioctl_req_copyout(a_ioctl_req, &tdev->m_ws);
                }
                break;

                case TIOCSWINSZ:
                {
                        // Get window size.
                        // Argument: struct winsize *argp
                        struct winsize newws;
                        retval = ioctl_req_copyin(a_ioctl_req, &newws);
                        if (KERN_STATUS_SUCCESS(retval))
                        {
                                if ((tdev->m_ws.ws_col != newws.ws_col)           //
                                    || (tdev->m_ws.ws_row != newws.ws_row)        //
                                    || (tdev->m_ws.ws_xpixel != newws.ws_xpixel)  //
                                    || (tdev->m_ws.ws_ypixel != newws.ws_ypixel)) //
                                {
                                        tdev->m_ws = newws;
                                        killpg(tty->m_fg_pgid, SIGWINCH);
                                }
                        }
                }
                break;

                default:
                        panic("Unhandled tty ioctl %04X", cmd);
                        break;
        }

        return retval;
}

// ********************************************************************************************************************************
static kern_return_t ntty_read(tty_ldisc_t a_tty, tty_device_t tdev, integer_t a_oflags, struct uio *a_memrw)
// ********************************************************************************************************************************
{
        ntty_t tty = NTTY(a_tty);

        kern_return_t retval = KERN_SUCCESS;

        natural_t rdcnt = 0;
again:
        tty_char *cbuf = ttyq_read_buffer(&tty->m_iputq, &rdcnt);

        if (rdcnt == 0)
        {
                if (a_oflags & O_NONBLOCK)
                {
                        retval = KERN_FAIL(EWOULDBLOCK);
                        goto out;
                }
                else
                {
                        retval = tty_ldisc_wait(a_tty, &tty->m_iputq);

                        if (KERN_STATUS_FAILURE(retval))
                        {
                                goto out;
                        }
                        else
                        {
                                goto again;
                        }
                }
        }
        else
        {
                natural_t xfercnt = a_memrw->uio_resid;

                retval = uiomove(cbuf, rdcnt, a_memrw);

                if (KERN_STATUS_SUCCESS(retval))
                {
                        xfercnt -= a_memrw->uio_resid;
                        ttyq_read_advance(&tty->m_iputq, xfercnt);
                        tdev->m_device_ops->input_processed(tdev);
                }
        }

out:

        return retval;
}

// ********************************************************************************************************************************
static kern_return_t ntty_write(tty_ldisc_t a_tty, tty_device_t tdev, integer_t a_oflags, struct uio *a_memrw)
// ********************************************************************************************************************************
{
        ntty_t tty = NTTY(a_tty);

        if (tty->m_flags & NTTY_DISCARD)
        {
                a_memrw->uio_offset += a_memrw->uio_resid;
                a_memrw->uio_resid = 0;
                return KERN_SUCCESS;
        }

        kern_return_t retval = KERN_SUCCESS;

        natural_t oresid = a_memrw->uio_resid;

        natural_t wrcnt = 0;
again:
        tty_char *obuf = ttyq_write_buffer(&tty->m_oputq, &wrcnt);

        wrcnt = (wrcnt * 3) / 4; // 75 % is the high water mark

        if (wrcnt == 0)
        {
                tdev->m_device_ops->output_pending(tdev);

                if (a_oflags & O_NONBLOCK)
                {
                        if ((oresid == a_memrw->uio_resid))
                        {
                                retval = KERN_FAIL(EWOULDBLOCK);
                        }
                        goto out;
                }
                else
                {
                        retval = tty_ldisc_wait(a_tty, &tty->m_oputq);

                        if (KERN_STATUS_FAILURE(retval))
                        {
                                goto out;
                        }
                        else
                        {
                                goto again;
                        }
                }
        }
        else
        {

                natural_t xfercnt = a_memrw->uio_resid;

                retval = uiomove(obuf, wrcnt, a_memrw);

                xfercnt = xfercnt - a_memrw->uio_resid;

                if (KERN_STATUS_FAILURE(retval))
                {
                        goto out;
                }
                else
                {
                        ttyq_write_advance(&tty->m_oputq, xfercnt);
                }
        }

out:

        tdev->m_device_ops->output_pending(tdev);

        return retval;
}

// ********************************************************************************************************************************
static kern_return_t ntty_poll(tty_ldisc_t a_tty, tty_device_t tdev, integer_t a_events)
// ********************************************************************************************************************************
{
        ntty_t tty = NTTY(a_tty);

        integer_t retval = 0;

        if (a_events & (POLLIN | POLLRDNORM))
        {
                if ((ttyq_is_empty(&tty->m_iputq) == false) /*|| (!ISSET(tp->t_cflag, CLOCAL) && !ISSET(tp->t_state, TS_CARR_ON*/)
                {
                        retval |= (a_events & (POLLIN | POLLRDNORM));
                }
        }

        /* NOTE: POLLHUP and POLLOUT/POLLWRNORM are mutually exclusive */
        if (/*!ISSET(tp->t_cflag, CLOCAL) && !ISSET(tp->t_state, TS_CARR_ON)*/ 0)
        {
                retval |= POLLHUP;
        }
        else if (a_events & (POLLOUT | POLLWRNORM))
        {
                natural_t wrcnt = 0;
                ttyq_write_buffer(&tty->m_oputq, &wrcnt);

                if (wrcnt > 8)
                {
                        retval |= a_events & (POLLOUT | POLLWRNORM);
                }
                else
                {
                        tdev->m_device_ops->output_pending(tdev);
                }
        }

        return (kern_return_t)retval;
}

// ********************************************************************************************************************************
static kern_return_t ntty_output_start_post_process(tty_device_t a_dev,
                                                    void *a_buf,
                                                    natural_t a_cnt,
                                                    natural_t *a_cnt_out,
                                                    void *a_user_data)
// ********************************************************************************************************************************
{
        kern_return_t retval = KERN_SUCCESS;

        tty_char *obuf = (tty_char *)a_buf;

        natural_t i;
        for (i = 0; i < a_cnt; ++i)
        {
                tty_char pbuf[TAB_SIZE];
                natural_t plen = 0;

                tty_char ch = obuf[i];

                if ((ch == CHAR_CR) && TERMIOS_OFLAG(a_dev, OCRNL))
                {
                        // Map CR to NL on output.
                        ch = CHAR_NL;
                }

                if ((ch == CHAR_NL) && TERMIOS_OFLAG(a_dev, ONLCR))
                {
                        // Map NL to CR-NL on output.
                        pbuf[0] = CHAR_CR;
                        pbuf[1] = CHAR_NL;
                        plen    = 2;
                }
                else if ((ch == CHAR_TAB) && TERMIOS_OFLAG(a_dev, OXTABS))
                {
                        // Convert tab characters into the appropriate number of spaces to emulate a tab stop every 8 cols.
                        // TODO
                        etl::memset8(pbuf, ' ', TAB_SIZE);
                        plen = TAB_SIZE;
                }
                else
                {
                        pbuf[0] = ch;
                        plen    = 1;
                }

                natural_t cnt = 0;

                retval = a_dev->m_device_ops->output_receive(a_dev, pbuf, plen, &cnt, a_user_data);

                if (KERN_STATUS_FAILURE(retval))
                {
                        break;
                }

                if (cnt < plen)
                {
                        break;
                }
        }

        (*a_cnt_out) = i;

        return retval;
}

// ********************************************************************************************************************************
static kern_return_t ntty_output_start(tty_ldisc_t a_tty, tty_device_t tdev, void *a_user_data)
// ********************************************************************************************************************************
{
        ntty_t tty = NTTY(a_tty);

        natural_t rdcnt = 0;
        tty_char *buf   = ttyq_read_buffer(&tty->m_oputq, &rdcnt);

        kern_return_t retval = KERN_SUCCESS;

        if (rdcnt > 0)
        {
                if (TERMIOS_OFLAG(tdev, OPOST))
                {
                        retval = ntty_output_start_post_process(tdev, buf, rdcnt, &rdcnt, a_user_data);
                }
                else
                {
                        retval = tdev->m_device_ops->output_receive(tdev, buf, rdcnt, &rdcnt, a_user_data);
                }

                if (rdcnt > 0)
                {
                        ttyq_read_advance(&tty->m_oputq, rdcnt);
                        tty_ldisc_notify(a_tty, &tty->m_oputq);
                        tty_ldisc_poll_event(a_tty, POLLOUT | POLLWRNORM);
                }
        }

        return retval;
}

/* Fields and flags on characters in the input queue. */
#define IN_CHAR   0x00FF /* low 8 bits are the character itself */
#define IN_LEN    0x0F00 /* length of char if it has been echoed */
#define IN_LSHIFT 8      /* length = (c & IN_LEN) >> IN_LSHIFT */
#define IN_EOL    0x1000 /* char is a line break (^D, LF) */
#define IN_EOF    0x2000 /* char is EOF (^D), do not return to user */
#define IN_ESC    0x4000 /* escaped by LNEXT (^V), no interpretation */

// ********************************************************************************************************************************
static int ntty_echo(ntty_t a_ntty, tty_device_t a_tdev, int a_char)
// ********************************************************************************************************************************
{
        /* Echo the character if echoing is on.  Some control characters are echoed
         * with their normal effect, other control characters are echoed as "^X",
         * normal characters are echoed normally.  EOF (^D) is echoed, but immediately
         * backspaced over.  Return the character with the echoed length added to its
         * attributes.
         */

        natural_t wrcnt = 0;
        tty_char *obuf  = ttyq_write_buffer(&a_ntty->m_oputq, &wrcnt);

        KASSERT(wrcnt > a_ntty->m_oput_pos);

        wrcnt -= a_ntty->m_oput_pos;

        if (wrcnt < (1 * TAB_SIZE))
        {
                // Output is full, no echo!
                return a_char;
        }

        a_char &= ~IN_LEN;

        int len = 0;

        if (iscntrl(a_char & IN_CHAR))
        {
                switch (a_char & (IN_ESC | IN_EOF | IN_EOL | IN_CHAR))
                {
                        case '\t':
                        {
                                do
                                {
                                        obuf[a_ntty->m_oput_pos++] = ' ';
                                        len++;
                                }
                                while ((len < TAB_SIZE) && ((a_ntty->m_oput_pos & TAB_MASK) != 0));
                                break;
                        }
                        case '\r' | IN_EOL:
                        case '\n' | IN_EOL:
                        {
                                obuf[a_ntty->m_oput_pos++] = (tty_char)(a_char & IN_CHAR);
                                break;
                        }
                        default:
                        {
                                obuf[a_ntty->m_oput_pos++] = '^';
                                obuf[a_ntty->m_oput_pos++] = (tty_char)(0x40 + (a_char & IN_CHAR));

                                len = 2;
                                break;
                        }
                }
        }
        else if ((a_char & IN_CHAR) == CHAR_DEL)
        {
                /* A DEL prints as "^?". */
                obuf[a_ntty->m_oput_pos++] = '^';
                obuf[a_ntty->m_oput_pos++] = '?';

                len = 2;
        }
        else
        {
                obuf[a_ntty->m_oput_pos++] = (tty_char)(a_char & IN_CHAR);

                len = 1;
        }

        if ((a_char & IN_EOF) || (a_ntty->m_flags & NTTY_LNEXT))
        {
                while (len > 0)
                {
                        obuf[a_ntty->m_oput_pos++] = TERMIOS_CCHAR(a_tdev, VERASE);
                        len--;
                }
        }

        return (a_char | (len << IN_LSHIFT));
}

// ********************************************************************************************************************************
static void ntty_erase(ntty_t a_ntty, tty_device_t a_tdev)
// ********************************************************************************************************************************
{
        if (a_ntty->m_canon_buflen > 0)
        {
                int ch = a_ntty->m_canon_buffer[--a_ntty->m_canon_buflen];

                if (TERMIOS_LFLAG(a_tdev, ECHOE))
                {
                        natural_t wrcnt = 0;
                        tty_char *obuf  = ttyq_write_buffer(&a_ntty->m_oputq, &wrcnt);

                        KASSERT(wrcnt > a_ntty->m_oput_pos);

                        wrcnt -= a_ntty->m_oput_pos;

                        for (int len = (ch & IN_LEN) >> IN_LSHIFT; len > 0; len--)
                        {
                                obuf[a_ntty->m_oput_pos++] = TERMIOS_CCHAR(a_tdev, VERASE);
                                obuf[a_ntty->m_oput_pos++] = ' ';
                                obuf[a_ntty->m_oput_pos++] = TERMIOS_CCHAR(a_tdev, VERASE);
                        }
                }
        }
}

// ********************************************************************************************************************************
static void ntty_canon_input(ntty_t a_ntty, tty_device_t a_tdev, int a_ch)
// ********************************************************************************************************************************
{
        if (a_ch == TERMIOS_CCHAR(a_tdev, VERASE))
        {
                ntty_erase(a_ntty, a_tdev);
        }
        else if (a_ch == TERMIOS_CCHAR(a_tdev, VWERASE))
        {
                if (TERMIOS_LFLAG(a_tdev, IEXTEN))
                {
                        while ((a_ntty->m_canon_buflen > 0)
                               && ((a_ntty->m_canon_buffer[a_ntty->m_canon_buflen - 1] & IN_CHAR) == ' '))
                        {
                                ntty_erase(a_ntty, a_tdev);
                        }

                        while ((a_ntty->m_canon_buflen > 0)
                               && ((a_ntty->m_canon_buffer[a_ntty->m_canon_buflen - 1] & IN_CHAR) != ' '))
                        {
                                ntty_erase(a_ntty, a_tdev);
                        }
                }
        }
        else if (a_ch == TERMIOS_CCHAR(a_tdev, VKILL))
        {
                while (a_ntty->m_canon_buflen > 0)
                {
                        ntty_erase(a_ntty, a_tdev);
                }
        }
        else if (a_ch == TERMIOS_CCHAR(a_tdev, VREPRINT))
        {
                if (TERMIOS_LFLAG(a_tdev, IEXTEN))
                {
                        if (TERMIOS_LFLAG(a_tdev, ECHO))
                        {
                                ntty_echo(a_ntty, a_tdev, a_ch);
                        }

                        ntty_echo(a_ntty, a_tdev, CHAR_NL | IN_EOL);

                        for (int32_t idx = 0; idx < a_ntty->m_canon_buflen; ++idx)
                        {
                                ntty_echo(a_ntty, a_tdev, a_ntty->m_canon_buffer[idx]);
                        }
                }
        }
        else
        {
                /* EOF (^D) means end-of-file, an invisible "line break". */
                if (a_ch == TERMIOS_CCHAR(a_tdev, VEOF))
                {
                        a_ch |= IN_EOL | IN_EOF;
                }

                /* Same thing with EOL, whatever it may be. */
                if ((a_ch == TERMIOS_CCHAR(a_tdev, VEOL)) || (a_ch == CHAR_NL))
                {
                        a_ch |= IN_EOL;
                }

                if (TERMIOS_LFLAG(a_tdev, ECHO) || (((a_ch & IN_CHAR) == CHAR_NL) && TERMIOS_LFLAG(a_tdev, ECHONL)))
                {
                        a_ch = ntty_echo(a_ntty, a_tdev, a_ch);
                }

                if (((a_ntty->m_flags & NTTY_LNEXT) == 0))
                {
                        KASSERT(a_ntty->m_canon_buflen < (int)ARRAY_SIZE(a_ntty->m_canon_buffer));
                        a_ntty->m_canon_buffer[a_ntty->m_canon_buflen++] = a_ch;
                }

                if ((a_ntty->m_canon_buflen + 2) == (int)ARRAY_SIZE(a_ntty->m_canon_buffer))
                {
                        a_ch = a_ntty->m_canon_buffer[a_ntty->m_canon_buflen++] = CHAR_NL | IN_EOL;
                }
        }

        if (a_ch & IN_EOL)
        {
                natural_t wrcnt = 0;
                tty_char *ibuf  = ttyq_write_buffer(&a_ntty->m_iputq, &wrcnt);

                for (int32_t idx = 0; idx < a_ntty->m_canon_buflen; ++idx)
                {
                        int ch = a_ntty->m_canon_buffer[idx];

                        if (iscntrl(ch & IN_CHAR))
                        {
                                if (ch & IN_EOF)
                                {
                                        break;
                                }

                                if (ch & IN_EOL)
                                {
                                        if ((ch & IN_CHAR) != CHAR_NL)
                                        {
                                                continue;
                                        }
                                }
                                else
                                {
                                        continue;
                                }
                        }

                        ibuf[a_ntty->m_iput_pos++] = (tty_char)(ch & IN_CHAR);

                        KASSERT(wrcnt > a_ntty->m_iput_pos);
                }

                a_ntty->m_canon_buflen = 0;

                if (a_ntty->m_iput_pos > 0)
                {
                        ttyq_write_advance(&a_ntty->m_iputq, a_ntty->m_iput_pos);
                        a_ntty->m_iput_pos = 0;
                }
        }
}

// ********************************************************************************************************************************
static int ntty_signal(ntty_t a_ntty, tty_device_t a_tdev, int a_sig, int a_ch)
// ********************************************************************************************************************************
{
        killpg(a_ntty->m_fg_pgid, a_sig);

        if (TERMIOS_LFLAG(a_tdev, NOFLSH) == false)
        {
                natural_t rdcnt = 0;
                ttyq_read_buffer(&a_ntty->m_iputq, &rdcnt);
                ttyq_read_advance(&a_ntty->m_iputq, rdcnt);
                a_ntty->m_iput_pos     = 0;
                a_ntty->m_canon_buflen = 0;
        }

        return a_ch | IN_EOL;
}

// ********************************************************************************************************************************
static void ntty_raw_input(ntty_t a_ntty, tty_device_t a_tdev, int a_ch)
// ********************************************************************************************************************************
{
        if (TERMIOS_LFLAG(a_tdev, ECHO))
        {
                a_ch = ntty_echo(a_ntty, a_tdev, a_ch);
        }

        natural_t wrcnt = 0;
        tty_char *ibuf  = ttyq_write_buffer(&a_ntty->m_iputq, &wrcnt);

        KASSERT(wrcnt > a_ntty->m_iput_pos);

        ibuf[a_ntty->m_iput_pos++] = (tty_char)(a_ch & IN_CHAR);

        ttyq_write_advance(&a_ntty->m_iputq, a_ntty->m_iput_pos);
        a_ntty->m_iput_pos = 0;
}

// ********************************************************************************************************************************
static void ntty_input_char(ntty_t a_ntty, tty_device_t tdev, int a_ch) // Push a single character from the device to user
// ********************************************************************************************************************************
{
        if (TERMIOS_IFLAG(tdev, ISTRIP))
        {
                a_ch &= 0x7F;
        }

        if (TERMIOS_LFLAG(tdev, IEXTEN))
        {
                // IEXTEN implementation-defined meaning.
                // On BSD systems and GNU/Linux and GNU/Hurd systems, it enables the LNEXT and DISCARD characters

                if (a_ntty->m_flags & NTTY_LNEXT)
                {
                        // ch = (tty_char)('@' + ch);
                        a_ch |= IN_ESC;
                        a_ntty->m_flags &= ~NTTY_LNEXT;
                }

                if (a_ntty->m_flags & NTTY_DISCARD)
                {
                        a_ntty->m_flags &= ~NTTY_DISCARD;
                }

                if (a_ch == TERMIOS_CCHAR(tdev, VLNEXT))
                {
                        // The LNEXT character is recognized only when IEXTEN is set, but in both canonical and
                        // noncanonical mode.

                        // It disables any special significance of the next character the user types.

                        // Even if the character would normally perform some editing function or generate a
                        // signal, it is read as a plain character. This is the analogue of the C-q command in
                        // Emacs.

                        // “LNEXT” stands for “literal next.”
                        // The LNEXT character is usually C-v.
                        // This character is available on BSD systems and GNU/Linux and GNU/Hurd systems.

                        a_ntty->m_flags |= NTTY_LNEXT;
                        a_ch = '^';
                }
                else if (a_ch == TERMIOS_CCHAR(tdev, VDISCARD))
                {
                        // The DISCARD character is recognized only when IEXTEN is set, but in both canonical and
                        // noncanonical mode.

                        // Its effect is to toggle the discard-output flag.
                        // When this flag is set, all program output is discarded.
                        // Setting the flag also discards all output currently in the output buffer.
                        // Typing any other character resets the flag.
                        // This character is available on BSD systems and GNU/Linux and GNU/Hurd systems.

                        a_ntty->m_flags |= NTTY_DISCARD;
                        a_ntty->m_oput_pos = 0;
                }
        }

        if (a_ch == _POSIX_VDISABLE)
        {
                a_ch |= IN_ESC;
        }

        if (a_ch == CHAR_CR)
        {
                if (TERMIOS_IFLAG(tdev, IGNCR))
                {
                        return;
                }

                if (TERMIOS_IFLAG(tdev, ICRNL))
                {
                        a_ch = CHAR_NL;
                }
        }
        else if (a_ch == CHAR_NL)
        {
                if (TERMIOS_IFLAG(tdev, INLCR))
                {
                        a_ch = CHAR_CR;
                }
        }

        if (TERMIOS_LFLAG(tdev, ISIG))
        {
                if (a_ch == TERMIOS_CCHAR(tdev, VINTR))
                {
                        a_ch = ntty_signal(a_ntty, tdev, SIGINT, a_ch);
                }
                else if (a_ch == TERMIOS_CCHAR(tdev, VQUIT))
                {
                        a_ch = ntty_signal(a_ntty, tdev, SIGQUIT, a_ch);
                }
                else if (a_ch == TERMIOS_CCHAR(tdev, VSUSP))
                {
                        // TODO: Job control
                        // killpg(tty->m_fg_pgid, SIGSTOP);
                        a_ch |= IN_EOL;
                }
        }

        if (TERMIOS_LFLAG(tdev, ICANON))
        {
                ntty_canon_input(a_ntty, tdev, a_ch);
        }
        else
        {
                if ((a_ch & IN_EOL) == 0)
                {
                        ntty_raw_input(a_ntty, tdev, a_ch);
                }
        }
}

// ********************************************************************************************************************************
static kern_return_t ntty_input(tty_ldisc_t a_tty, tty_device_t tdev, char const *a_char, natural_t a_char_len)
// ********************************************************************************************************************************
{
        ntty_t tty = NTTY(a_tty);

        kern_return_t retval = KERN_SUCCESS;

        for (natural_t i = 0; i < a_char_len; ++i)
        {
                ntty_input_char(tty, tdev, a_char[i]);
        }

        if (tty->m_oput_pos > 0)
        {
                ttyq_write_advance(&tty->m_oputq, tty->m_oput_pos);
                tty->m_oput_pos = 0;
        }

        natural_t rdcnt = 0;
        ttyq_read_buffer(&tty->m_oputq, &rdcnt);
        if (rdcnt > 0)
        {
                tdev->m_device_ops->output_pending(tdev);
        }

        ttyq_read_buffer(&tty->m_iputq, &rdcnt);
        if (rdcnt > 0)
        {
                tty_ldisc_notify(tty, &tty->m_iputq);
                tty_ldisc_poll_event(tty, POLLIN | POLLRDNORM);
        }

        return retval;
}

// ********************************************************************************************************************************
static boolean_t ntty_input_possible(tty_ldisc_t a_tty)
// ********************************************************************************************************************************
{
        ntty_t ntty = NTTY(a_tty);

        natural_t cnt = 0;

        ttyq_write_buffer(&ntty->m_iputq, &cnt);

        return (cnt - ntty->m_iput_pos) > 0;
}

// ********************************************************************************************************************************
static boolean_t ntty_output_possible(tty_ldisc_t a_tty)
// ********************************************************************************************************************************
{
        ntty_t ntty = NTTY(a_tty);

        natural_t cnt = 0;

        ttyq_read_buffer(&ntty->m_oputq, &cnt);

        return cnt > 0;
}

static struct tty_ldisc_ops_data s_ntty_ops = {
        .create           = ntty_create,         //
        .destroy          = ntty_destroy,        //
        .open             = ntty_open,           //
        .close            = ntty_close,          //
        .read             = ntty_read,           //
        .write            = ntty_write,          //
        .ioctl            = ntty_ioctl,          //
        .poll             = ntty_poll,           //
        .dev_input        = ntty_input,          //
        .dev_output_start = ntty_output_start,   //
        .input_possible   = ntty_input_possible, //
        .output_possible  = ntty_output_possible //
};

// ********************************************************************************************************************************
static kern_return_t ntty_reg_ldisc()
// ********************************************************************************************************************************
{
        tty_ldisc_register(N_TTY, &s_ntty_ops);

        return KERN_SUCCESS;
}

core_initcall(ntty_reg_ldisc);
