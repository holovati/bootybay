#include <sys/sysmacros.h>

#include <fcntl.h>

#include <aux/init.h>

#include <emerixx/memory_rw.h>
#include <emerixx/chrdev.h>
#include <emerixx/poll.h>
#include <emerixx/ioctl_req.h>
#include <emerixx/vfs/vfs_node.h>
#include <emerixx/vfs/vfs_file.h>
#include <emerixx/vfs/vfs_file_data.h>

#include <kernel/vm/vm_page_map.h>
#include <kernel/vm/vm_page.h>
#include <kernel/vm/vm_param.h>
#include <kernel/vm/vm_object.h>

#include <mach/framebuffer.h>

#include <emerixx/fb.h>

static struct chrdev_data s_fbdev;
static struct vm_page_map_data s_fb_page_map;
static struct vm_object_data s_fb_vm_object;

// ********************************************************************************************************************************
static kern_return_t fbdev_open(vfs_node_t a_vfsn, vfs_file_t a_file)
// ********************************************************************************************************************************
{
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t fbdev_close(vfs_file_t a_file)
// ********************************************************************************************************************************
{
        return KERN_SUCCESS;
}

#define FBDEV_OFF(f) ((f)->m_offset)

// ********************************************************************************************************************************
static kern_return_t fbdev_rw(vfs_file_t a_file, struct uio *a_uio)
// ********************************************************************************************************************************
{
        fb_var_screeninfo vsi;
        fb_get_variable_screeninfo(&vsi);

        fb_fix_screeninfo fsi;
        fb_get_fixed_screeninfo(&fsi);

        off_t fblen = round_page(vsi.height * fsi.line_length);

        kern_return_t retval = KERN_SUCCESS;

        a_uio->uio_offset = FBDEV_OFF(a_file);

        off_t oresid = (off_t)a_uio->uio_resid;

        if (a_uio->uio_offset < fblen)
        {
                off_t xfercount = fblen - a_uio->uio_offset;

                char *vidbuf = ((char *)(phystokv(fsi.smem_start))) + a_uio->uio_offset;

                retval = uiomove(vidbuf, xfercount, a_uio);
        }

        FBDEV_OFF(a_file) += (oresid - a_uio->uio_resid);

        return retval;
}

// ********************************************************************************************************************************
static kern_return_t fbdev_lseek(vfs_file_t a_file, off_t a_offset, int a_whence)
// ********************************************************************************************************************************
{
        switch (a_whence)
        {
                case SEEK_CUR:
                        FBDEV_OFF(a_file) += a_offset;
                        break;
                case SEEK_END:
                        FBDEV_OFF(a_file) = a_offset + s_fb_vm_object.m_size;
                        break;
                case SEEK_SET:
                        FBDEV_OFF(a_file) = a_offset;
                        break;
                default:
                        return KERN_FAIL(EINVAL);
        }

        return (kern_return_t)FBDEV_OFF(a_file);
}

// ********************************************************************************************************************************
static kern_return_t fbdev_ioctl(vfs_file_t a_file, ioctl_req_t a_ioctl_req)
// ********************************************************************************************************************************
{
        // Just enough to get microwindows running
        kern_return_t retval = KERN_FAIL(ENOTTY);

        integer_t cmdno = ioctl_req_cmdno(a_ioctl_req);

        switch (cmdno)
        {
                case FBIOGET_VSCREENINFO:
                {
                        fb_var_screeninfo vsi;

                        retval = fb_get_variable_screeninfo(&vsi);

                        if (KERN_STATUS_SUCCESS(retval))
                        {
                                retval = ioctl_req_copyout(a_ioctl_req, &vsi);
                        }
                }
                break;

                case FBIOGET_FSCREENINFO:
                {
                        fb_fix_screeninfo fsi;

                        retval = fb_get_fixed_screeninfo(&fsi);

                        if (KERN_STATUS_SUCCESS(retval))
                        {
                                retval = ioctl_req_copyout(a_ioctl_req, &fsi);
                        }
                }
                break;

                default:
                        panic("Framebuffer: missing ioctl %d", cmdno);
                        break;
        }

        return retval;
}

// ********************************************************************************************************************************
static kern_return_t fbdev_mmap(vfs_file_t, vm_object_t *a_object_out)
// ********************************************************************************************************************************
{
        vm_object_ref((*a_object_out = &s_fb_vm_object));
        return KERN_SUCCESS;
}

static struct vfs_file_operations_data s_fbdev_ops = {
        //
        .fopen  = fbdev_open,  //
        .fread  = fbdev_rw,    //
        .fwrite = fbdev_rw,    //
        .flseek = fbdev_lseek, //
        .fioctl = fbdev_ioctl, //
        .fclose = fbdev_close, //
        .fmmap  = fbdev_mmap
        //
};

// ********************************************************************************************************************************
static kern_return_t fbdev_vmo_get_page(vm_object_t a_vm_obj, vm_offset_t a_vmo, vm_prot_t a_vm_prot, vm_page_t *a_vmp_out)
// ********************************************************************************************************************************
{

        vm_page_t vmp = vm_page_map_lookup(&s_fb_page_map, a_vmo);

        if (vmp == nullptr)
        {
                panic("what is the correct return value");
                return 0;
        }

        *a_vmp_out = vmp;

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static boolean_t fbdev_vmo_collapse(vm_object_t a_vm_obj,
                                    vm_object_t a_destination,
                                    vm_object_t *a_backing_object,
                                    vm_offset_t *a_backing_object_offset)
// ********************************************************************************************************************************
{
        return false;
}

// ********************************************************************************************************************************
static kern_return_t fbdev_vmo_set_size(vm_object_t a_vm_obj, natural_t a_new_size)
// ********************************************************************************************************************************
{
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t fbdev_vmo_inactive(vm_object_t a_vmo)
// ********************************************************************************************************************************
{
        panic("How?, we added a extra ref");
        return KERN_SUCCESS;
}
static struct vm_object_operations_data fbdev_vm_obj_oops = {
        //
        .get_page = fbdev_vmo_get_page,
        .inactive = fbdev_vmo_inactive,
        .collapse = fbdev_vmo_collapse,
        .set_size = fbdev_vmo_set_size
        //
};

static kern_return_t fb_dev_init()
{
        kern_return_t retval = driver_chrdev_alloc(&s_fbdev, "fb", &s_fbdev_ops, makedev(23, 0), 1);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        fb_var_screeninfo vsi;
        fb_get_variable_screeninfo(&vsi);

        fb_fix_screeninfo fsi;
        fb_get_fixed_screeninfo(&fsi);

        vm_page_map_init(&s_fb_page_map);

        vm_object_init(&s_fb_vm_object);

        vm_object_ref(&s_fb_vm_object);

        s_fb_vm_object.m_size = round_page(vsi.yres * fsi.line_length);

        s_fb_vm_object.m_ops = &fbdev_vm_obj_oops;

        for (vm_offset_t off = 0; off < s_fb_vm_object.m_size; off += PAGE_SIZE)
        {
                vm_page_alloc_private(&s_fb_page_map, off, fsi.smem_start + off);
        }

        return retval;
}

device_initcall(fb_dev_init);
