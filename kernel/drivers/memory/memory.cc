#include <sys/sysmacros.h> // makedev

#include <aux/init.h>

#include <etl/algorithm.hh>

#include <kernel/vm/vm_param.h>

#include <emerixx/memory_rw.h>
#include <emerixx/chrdev.h>
#include <emerixx/poll.h>
#include <emerixx/vfs/vfs_file.h>
#include <emerixx/vfs/vfs_file_data.h>

static natural_t s_zero_buf[PAGE_SIZE / sizeof(natural_t)];

static poll_device_context_t s_pollfds;
static chrdev_data s_null_dev;
static chrdev_data s_zero_dev;

// ********************************************************************************************************************************
static kern_return_t mem_poll(vfs_file_t a_file, pollfd **a_pfd)
// ********************************************************************************************************************************
{
        // Can always read and write. Same for both null and zero
        if ((*a_pfd)->events & (POLLIN | POLLRDNORM))
        {
                (*a_pfd)->revents |= (short)((*a_pfd)->events & (POLLIN | POLLRDNORM));
        }

        if ((*a_pfd)->events & (POLLOUT | POLLWRNORM))
        {
                (*a_pfd)->revents |= (short)((*a_pfd)->events & (POLLOUT | POLLWRNORM));
        }
        return poll_record(&s_pollfds, a_pfd);
}

// ********************************************************************************************************************************
static kern_return_t mem_null_read(vfs_file_t a_file, memory_rdwr_t a_memrw)
// ********************************************************************************************************************************
{
        // Will return EOF
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t mem_zero_read(vfs_file_t a_file, memory_rdwr_t a_memrw)
// ********************************************************************************************************************************
{
        KASSERT(a_memrw->uio_rw == UIO_READ);
        // Zero fill the buffer
        kern_return_t retval = KERN_SUCCESS;

        while (a_memrw->uio_resid > 0 && KERN_STATUS_SUCCESS(retval))
        {
                retval = uiomove(s_zero_buf, sizeof(s_zero_buf), a_memrw);
        }

        return retval;
}

// ********************************************************************************************************************************
static kern_return_t mem_write(vfs_file_t a_file, memory_rdwr_t a_memrw)
// ********************************************************************************************************************************
{
        // Just pretend to write the whole uio. Same for both null and zero
        a_memrw->uio_offset += (off_t)(a_memrw->uio_resid);
        a_memrw->uio_resid = 0;
        a_memrw->uio_iov += (a_memrw->uio_iovcnt - 1);
        return KERN_SUCCESS;
}

static struct vfs_file_operations_data s_null_dev_ops = {
        .fopen  = vfs_file_op_open_noop,    //
        .fread  = mem_null_read,            //
        .fwrite = mem_write,                //
        .flseek = vfs_file_op_lseek_espipe, //
        .fioctl = vfs_file_op_ioctl_enotty, //
        .fclose = vfs_file_op_close_noop,   //
        .fpoll  = mem_poll,                 //
};

static struct vfs_file_operations_data s_zero_dev_ops = {
        .fopen  = vfs_file_op_open_noop,    //
        .fread  = mem_zero_read,            //
        .fwrite = mem_write,                //
        .flseek = vfs_file_op_lseek_espipe, //
        .fioctl = vfs_file_op_ioctl_enotty, //
        .fclose = vfs_file_op_close_noop,   //
        .fpoll  = mem_poll,
};

static kern_return_t memory_device_init()
{
        etl::clear(s_zero_buf);

        poll_device_ctx_init(&s_pollfds);

        kern_return_t retval = driver_chrdev_alloc(&s_null_dev, "null", &s_null_dev_ops, makedev(2, 2), 1);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        retval = driver_chrdev_alloc(&s_zero_dev, "zero", &s_zero_dev_ops, makedev(2, 12), 1);

        return retval;
}

device_initcall(memory_device_init);
