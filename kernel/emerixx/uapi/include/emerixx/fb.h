#ifndef EMERIXX_FB_H
#define EMERIXX_FB_H
#include <stdint.h>

#define FBIOGET_VSCREENINFO 0x4600
#define FBIOPUT_VSCREENINFO 0x4601
#define FBIOGET_FSCREENINFO 0x4602

/*
Screen information are queried by applications using the FBIOGET_FSCREENINFO
and FBIOGET_VSCREENINFO ioctls. Those ioctls take a pointer to a
fb_fix_screeninfo and fb_var_screeninfo structure respectively.

struct fb_fix_screeninfo stores device independent unchangeable information
about the frame buffer device and the current format. Those information can't
be directly modified by applications, but can be changed by the driver when an
application modifies the format.
*/

#define FB_TYPE_PACKED_PIXELS 0 /* Packed Pixels	*/

struct fb_fix_screeninfo
{
        char id[16];          /* identification string eg "TT Builtin" */
        uintptr_t smem_start; /* Start of frame buffer mem */
                              /* (physical address) */
        uint32_t smem_len;    /* Length of frame buffer mem */
        uint32_t type;        /* see FB_TYPE_*		*/
        uint32_t type_aux;    /* Interleave for interleaved Planes */
        uint32_t visual;      /* see FB_VISUAL_*		*/
        uint16_t xpanstep;    /* zero if no hardware panning  */
        uint16_t ypanstep;    /* zero if no hardware panning  */
        uint16_t ywrapstep;   /* zero if no hardware ywrap    */
        uint16_t __pad;
        uint32_t line_length;  /* length of a line in bytes    */
        uint32_t accel;        /* Indicate to driver which	*/
                               /*  specific chip/card we have	*/
        uint16_t capabilities; /* see FB_CAP_*			*/
        uint16_t reserved[3];  /* Reserved for future compatibility */
};

/*
struct fb_var_screeninfo stores device independent changeable information
about a frame buffer device, its current format and video mode, as well as
other miscellaneous parameters.

To modify variable information, applications call the FBIOPUT_VSCREENINFO
ioctl with a pointer to a fb_var_screeninfo structure. If the call is
successful, the driver will update the fixed screen information accordingly.

Instead of filling the complete fb_var_screeninfo structure manually,
applications should call the FBIOGET_VSCREENINFO ioctl and modify only the
fields they care about.
*/

#define FB_ACTIVATE_NOW 0 /* set values immediately (or vbl)*/

#define FB_ACCEL_NONE 0 /* no hardware accelerator	*/

#define FB_VISUAL_MONO01    0 /* Monochr. 1=Black 0=White */
#define FB_VISUAL_MONO10    1 /* Monochr. 1=White 0=Black */
#define FB_VISUAL_TRUECOLOR 2 /* True color	*/

struct fb_bitfield
{
        uint32_t offset;    /* beginning of bitfield	*/
        uint32_t length;    /* length of bitfield		*/
        uint32_t msb_right; /* != 0 : Most significant bit is right */
};

struct fb_var_screeninfo
{
        uint32_t xres; /* visible resolution		*/
        uint32_t yres;
        uint32_t xres_virtual; /* virtual resolution		*/
        uint32_t yres_virtual;
        uint32_t xoffset; /* offset from virtual to visible */
        uint32_t yoffset; /* resolution			*/

        uint32_t bits_per_pixel;  /* guess what			*/
        uint32_t grayscale;       /* 0 = color, 1 = grayscale,	*/
                                  /* >1 = FOURCC			*/
        struct fb_bitfield red;   /* bitfield in fb mem if true color, */
        struct fb_bitfield green; /* else only length is significant */
        struct fb_bitfield blue;
        struct fb_bitfield transp; /* transparency			*/

        uint32_t nonstd;      /* != 0 Non standard pixel format */
        uint32_t activate;    /* see FB_ACTIVATE_*		*/
        uint32_t height;      /* height of picture in mm    */
        uint32_t width;       /* width of picture in mm     */
        uint32_t accel_flags; /* (OBSOLETE) see fb_info.flags */

        /* Timing: All values in pixclocks, except pixclock (of course) */
        uint32_t pixclock;     /* pixel clock in ps (pico seconds) */
        uint32_t left_margin;  /* time from sync to picture	*/
        uint32_t right_margin; /* time from picture to sync	*/
        uint32_t upper_margin; /* time from sync to picture	*/
        uint32_t lower_margin;
        uint32_t hsync_len;   /* length of horizontal sync	*/
        uint32_t vsync_len;   /* length of vertical sync	*/
        uint32_t sync;        /* see FB_SYNC_*		*/
        uint32_t vmode;       /* see FB_VMODE_*		*/
        uint32_t rotate;      /* angle we rotate counter clockwise */
        uint32_t colorspace;  /* colorspace for FOURCC-based modes */
        uint32_t reserved[4]; /* Reserved for future compatibility */
};

#endif // EMERIXX_FB_H