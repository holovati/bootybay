#include <emerixx/ioctl_req.h>

// ********************************************************************************************************************************
kern_return_t ioctl_req_create(integer_t a_cmdno, void *a_arg, boolean_t a_is_user_req, ioctl_req_t a_ioctl_req)
// ********************************************************************************************************************************
{
        a_ioctl_req->m_iov = { .iov_base = a_arg, .iov_len = 0 };

        a_ioctl_req->m_internal.uio_iov    = &a_ioctl_req->m_iov;
        a_ioctl_req->m_internal.uio_iovcnt = 1;
        a_ioctl_req->m_internal.uio_offset = 0;
        a_ioctl_req->m_internal.uio_resid  = 0;
        a_ioctl_req->m_internal.uio_segflg = a_is_user_req ? UIO_USERSPACE : UIO_SYSSPACE;
        a_ioctl_req->m_internal.uio_rw     = UIO_READ;
        a_ioctl_req->m_internal.aux.s      = a_cmdno;

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t ioctl_req_read(ioctl_req_t a_ioctl_req, void *a_data, natural_t a_data_len)
// ********************************************************************************************************************************
{
        a_ioctl_req->m_internal.uio_offset = 0;

        a_ioctl_req->m_iov.iov_len = a_ioctl_req->m_internal.uio_resid = a_data_len;

        a_ioctl_req->m_internal.uio_rw = UIO_WRITE;

        return uiomove(a_data, a_data_len, &a_ioctl_req->m_internal);
}

// ********************************************************************************************************************************
kern_return_t ioctl_req_write(ioctl_req_t a_ioctl_req, void *a_data, natural_t a_data_len)
// ********************************************************************************************************************************
{
        a_ioctl_req->m_internal.uio_offset = 0;

        a_ioctl_req->m_iov.iov_len = a_ioctl_req->m_internal.uio_resid = a_data_len;

        a_ioctl_req->m_internal.uio_rw = UIO_READ;

        return uiomove(a_data, a_data_len, &a_ioctl_req->m_internal);
}

// ********************************************************************************************************************************
integer_t ioctl_req_cmdno(ioctl_req_t a_ioctl_req)
// ********************************************************************************************************************************
{
        return a_ioctl_req->m_internal.aux.s;
}

// ********************************************************************************************************************************
integer_t ioctl_req_arg(ioctl_req_t a_ioctl_req)
// ********************************************************************************************************************************
{
        return (integer_t)a_ioctl_req->m_iov.iov_base;
}
