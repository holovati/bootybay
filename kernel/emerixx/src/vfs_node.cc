#include <limits.h>
#include <fcntl.h>
#include <sys/stat.h>

#include <aux/init.h>
#include <aux/defer.hh>

#include <etl/queue.h>
#include <etl/bit.hh>

#include <emerixx/fsdata.hh>
#include <emerixx/process.h>
#include <emerixx/vfs/vfs_node.h>
#include <emerixx/vfs/vfs_pathbuf.h>
#include <emerixx/vfs/vfs_node_data.h>
#include <emerixx/vfs/vfs_mount.h>
#include <emerixx/memory_rw.h>

#include "vfs_cache.h"

#define IS_DOT(p, pl)     (((pl) == 1) && (((p)[0]) == '.'))
#define IS_DOT_DOT(p, pl) (((pl) == 2) && ((((p)[0]) == '.') && (((p)[1]) == '.')))
#define IS_ROOT(p, pl)    (((pl) == 1) && ((p)[0] == '/'))

static struct lock_data s_rename_lock;

static struct dircache_entry_data
{
        struct vfs_cache_link_data m_link;
        queue_chain_t m_lrulink;
        vfs_node_t m_vfsn;
} s_vfs_cache_storage[0x100];

static queue_head_t s_cache_lru;
static vfs_cache_data s_vfsc;

// ********************************************************************************************************************************
static char const *_path_get_component(char const *path, char const *path_end, natural_t *len)
// ********************************************************************************************************************************
#define first_component(path, path_end, len) (_path_get_component((path), (path_end), (len)))
#define next_component(path, path_end, len)  (_path_get_component((path) + *(len), (path_end), (len)))
{
        *len = 0;

        while (*path == '/')
        {
                path++;
        }

        while (((path + (*len)) < path_end) && path[*len] != '/')
        {
                (*len)++;
        }

        return *len ? path : nullptr;
}

// ********************************************************************************************************************************
static kern_return_t start_lookup(vfs_node_t a_vfsn,
                                  char const *a_path,
                                  natural_t a_path_len,
                                  vfs_node_lookup_type_t a_lookup_kind,
                                  vfs_node_t *a_prnt_vfsn_out,
                                  vfs_node_t *a_chld_vfsn_out,
                                  char const **a_cnam_out,
                                  natural_t *a_cnamlen_out,
                                  boolean_t a_symlink_follow,
                                  int a_flags)
// ********************************************************************************************************************************
#define TRAILING_SLASH etl::bit<int, 0>::value
#define LAST_COMPONENT etl::bit<int, 1>::value
#define IN_LINK_LOOP   etl::bit<int, 2>::value
#define LOOKUP_SUCCESS etl::bit<int, 3>::value
#define LOOKUP_FAILURE 0

#define cnam         ((depth_ctx[idx])._cname)
#define cnamlen      ((depth_ctx[idx])._cnamlen)
#define cnbeg        ((depth_ctx[idx])._cnbeg)
#define cnend        ((depth_ctx[idx])._cnend)
#define cnam_next    ((depth_ctx[idx])._cname_next)
#define cnamlen_next ((depth_ctx[idx])._cnamlen_next)
#define vfsn         ((depth_ctx[idx])._vfsn)
{
        struct
        {
                char const *_cname;
                natural_t _cnamlen;
                char const *_cname_next;
                natural_t _cnamlen_next;
                char const *_cnbeg;
                char const *_cnend;
                vfs_node_t _vfsn;
        } depth_ctx[_POSIX_SYMLOOP_MAX] = {
  //
                {a_path, a_path_len, nullptr, 0, a_path, a_path + a_path_len, a_vfsn}
  //
        };

        vfs_node_ref(a_vfsn);

        kern_return_t retval = KERN_SUCCESS;
        integer_t idx        = 0;
        bool trailing_slash  = cnam[cnamlen - 1] == '/';
        int flags            = 0;
depth_inc:
        for (cnam = first_component(cnam, cnend, &cnamlen); //
             cnam != nullptr && KERN_STATUS_SUCCESS(retval);
             cnam = cnam_next, cnamlen = cnamlen_next)
        {
                cnamlen_next = cnamlen;
                cnam_next    = next_component(cnam, cnend, &cnamlen_next);

                vfs_node_t prnt_vfsn = vfsn, chld_vfsn = nullptr;

                vfsn = nullptr;

                if (IS_DOT(cnam, cnamlen) == true) [[unlikeley]]
                {
                        if (((idx == 0) && (cnam_next == nullptr)))
                        {
                                // Lock it if last component
                                vm_object_lock(&prnt_vfsn->m_vmo);
                        }

                        // '.' as a component means the parent we already have is also the child
                        vfs_node_ref(chld_vfsn = prnt_vfsn);
                }
                else
                {
                        flags = a_flags;

                        if (cnam_next == nullptr)
                        {
                                flags |= VFS_NODE_LOOKUP_FLAG_ISLASTCN;
                        }

                        if (IS_DOT_DOT(cnam, cnamlen) == true)
                        {
                                flags |= VFS_NODE_LOOKUP_FLAG_ISDOTDOT;
                        }

                relookup:
                        vm_object_lock(&prnt_vfsn->m_vmo);
                        retval = prnt_vfsn->m_ops->lookup(prnt_vfsn, cnam, cnamlen, a_lookup_kind, flags, &chld_vfsn);

                        if (KERN_STATUS_SUCCESS(retval))
                        {
                                if (S_ISDIR(chld_vfsn->m_mode) && chld_vfsn->m_mounted_here != nullptr) // Fs mounted on the node
                                {
                                        vfs_node_t mount_root_vfsn = nullptr;

                                        retval = vfs_mount_rootnode(chld_vfsn->m_mounted_here, &mount_root_vfsn);

                                        if (KERN_STATUS_SUCCESS(retval))
                                        {
                                                vfs_node_rel(chld_vfsn);
                                                chld_vfsn = mount_root_vfsn;
                                        }
                                }
                                else if ((flags & VFS_NODE_LOOKUP_FLAG_ISDOTDOT) && (prnt_vfsn == chld_vfsn))
                                {
                                        // child is parent, maybe root of a mounted Fs
                                        vfs_node_t mount_root_vfsn = nullptr;

                                        retval = vfs_mount_rootnode(chld_vfsn->m_mount, &mount_root_vfsn);

                                        if (KERN_STATUS_SUCCESS(retval))
                                        {
                                                if ((chld_vfsn == mount_root_vfsn)
                                                    && (vfs_mount_covered_vfsn(chld_vfsn->m_mount) != nullptr))
                                                {
                                                        vfs_node_rel(mount_root_vfsn);

                                                        vfs_node_rel(chld_vfsn);
                                                        vfs_node_put(prnt_vfsn);

                                                        vfs_node_ref(prnt_vfsn = vfs_mount_covered_vfsn(chld_vfsn->m_mount));
                                                        chld_vfsn = nullptr;
                                                        goto relookup;
                                                }
                                                else
                                                {
                                                        vfs_node_rel(mount_root_vfsn);
                                                }
                                        }
                                }
                        }
                        if (((idx == 0) && (cnam_next == nullptr)) == false)
                        {
                                // Not last component yet, unlock the parent
                                vm_object_unlock(&prnt_vfsn->m_vmo);
                        }

                link_resolve_finish:
                        // Check if we are at the last component and if we should follow the symlink if so.
                        boolean_t resolve_link = ((idx == 0 && cnam_next == nullptr && a_symlink_follow == false) == false)
                                              || ((idx == 0 && cnam_next == nullptr && trailing_slash == false) == false);

                        // If not dealing with a link, we are done with this component.
                        if (KERN_STATUS_SUCCESS(retval) && S_ISLNK(chld_vfsn->m_mode) && (resolve_link == true))
                        {

                                if ((idx + 1) < _POSIX_SYMLOOP_MAX)
                                {
                                        // Our locking protocol says when finished resolving the last protocol, we leave the parent
                                        // locked Except when the last component is a link and we need to resolve it.
                                        if (((idx == 0) && (cnam_next == nullptr)) == true)
                                        {
                                                // Not last component yet, unlock the parent
                                                vm_object_unlock(&prnt_vfsn->m_vmo);
                                        }

                                        // We need to know where to return to in case we fail resolving the link
                                        vfs_node_ref(vfsn = prnt_vfsn);

                                        idx++;

                                        char *lnkbuf = vfs_pathbuf_alloc(&cnamlen);

                                        vm_object_lock(&chld_vfsn->m_vmo);
                                        retval = chld_vfsn->m_ops->readlink(chld_vfsn, lnkbuf, &cnamlen);
                                        vm_object_unlock(&chld_vfsn->m_vmo);

                                        cnbeg = cnam = lnkbuf;

                                        cnend = cnam + cnamlen;

                                        if (KERN_STATUS_SUCCESS(retval))
                                        {
                                                if (cnam[0] == '/')
                                                {
                                                        // root vfsn gained a ref
                                                        vfsn = fs_context_getroot(uthread_self()->m_fsctx);
                                                }
                                                else
                                                {
                                                        // parent vfsn gained a ref
                                                        vfs_node_ref((vfsn = prnt_vfsn));
                                                }

                                                vfs_node_rel(chld_vfsn);
                                                chld_vfsn = nullptr;
                                                vfs_node_rel(prnt_vfsn);
                                                prnt_vfsn = nullptr;

                                                goto depth_inc;
                                        }
                                        else
                                        {
                                                vfs_pathbuf_free(lnkbuf);
                                                idx--;
                                        }
                                }
                                else
                                {
                                        vfs_node_rel(chld_vfsn);
                                        chld_vfsn = nullptr;
                                        retval    = KERN_FAIL(ELOOP);
                                }
                        }
                }

                switch (trailing_slash | ((cnam_next == nullptr) << 1) | ((idx > 0) << 2) | ((retval == KERN_SUCCESS) << 3))
                {
                        case LOOKUP_SUCCESS | TRAILING_SLASH:
                                [[fallthrough]];
                        case LOOKUP_SUCCESS:
                        { /*OK*/
                                // We are somewhere(not the last cn) in resolving the given path
                                // We expect the vnode to be a dir
                                if ((S_ISDIR(chld_vfsn->m_mode) == false))
                                {
                                        retval = KERN_FAIL(ENOTDIR);
                                        vfs_node_rel(chld_vfsn);
                                        vfs_node_rel(prnt_vfsn);
                                }
                                else
                                {
                                        vfsn      = chld_vfsn;
                                        chld_vfsn = nullptr;
                                        vfs_node_rel(prnt_vfsn);
                                        prnt_vfsn = nullptr;
                                }
                                break;
                        }

                        case LOOKUP_SUCCESS | LAST_COMPONENT | TRAILING_SLASH:
                        {
                                if ((S_ISDIR(chld_vfsn->m_mode) == false)
                                    || ((a_lookup_kind == VFS_NODE_LOOKUP_RENAME)
                                        && ((a_flags & VFS_NODE_LOOKUP_FLAG_CREATEDIR) == 0)))
                                {
                                        retval = KERN_FAIL(ENOTDIR);

                                        vfs_node_rel(chld_vfsn);

                                        vm_object_unlock(&prnt_vfsn->m_vmo);
                                        vfs_node_rel(prnt_vfsn);
                                        break;
                                }
                                [[fallthrough]];
                        }

                        case LOOKUP_SUCCESS | LAST_COMPONENT:
                        {
                                if ((a_chld_vfsn_out == nullptr) && (a_lookup_kind == VFS_NODE_LOOKUP_CREATE)
                                    && (chld_vfsn != nullptr))
                                {
                                        // A case for mkdir, symlink, link, mknod. they only want the parent which they signal by
                                        // passing nullptr for child. open wants the child if it exists.
                                        retval = KERN_FAIL(EEXIST);

                                        vfs_node_rel(chld_vfsn);

                                        vm_object_unlock(&prnt_vfsn->m_vmo);
                                        vfs_node_rel(prnt_vfsn);
                                }
                                else
                                {
                                        *a_chld_vfsn_out = chld_vfsn;
                                        chld_vfsn        = nullptr;
                                        *a_prnt_vfsn_out = prnt_vfsn;
                                        prnt_vfsn        = nullptr;
                                        *a_cnam_out      = cnam;
                                        *a_cnamlen_out   = cnamlen;
                                }
                                break;
                        }

                        case LOOKUP_FAILURE | TRAILING_SLASH | LAST_COMPONENT:
                                if ((retval == KERN_FAIL(ENOENT))
                                    && ((a_lookup_kind == VFS_NODE_LOOKUP_CREATE) || (a_lookup_kind == VFS_NODE_LOOKUP_RENAME))
                                    && (a_flags & VFS_NODE_LOOKUP_FLAG_CREATEDIR))
                                {
                                        goto can_create;
                                }
                                vm_object_unlock(&prnt_vfsn->m_vmo);

                                [[fallthrough]];
                        case LOOKUP_FAILURE | TRAILING_SLASH:
                                [[fallthrough]];
                        case LOOKUP_FAILURE:
                        { /*OK*/
                                KASSERT(chld_vfsn == nullptr);
                                vfs_node_rel(prnt_vfsn);
                                break;
                        }

                        case LOOKUP_FAILURE | LAST_COMPONENT:
                        {
                                KASSERT(chld_vfsn == nullptr);
                                if (((a_lookup_kind == VFS_NODE_LOOKUP_CREATE) || (a_lookup_kind == VFS_NODE_LOOKUP_RENAME))
                                    && (retval == KERN_FAIL(ENOENT)))
                                {
                                can_create:
                                        *a_prnt_vfsn_out = prnt_vfsn;
                                        prnt_vfsn        = nullptr;
                                        *a_cnam_out      = cnam;
                                        *a_cnamlen_out   = cnamlen;

                                        retval = KERN_SUCCESS;
                                }
                                else
                                {
                                        vm_object_unlock(&prnt_vfsn->m_vmo);
                                        vfs_node_rel(prnt_vfsn);
                                }
                                break;
                        }

                        case IN_LINK_LOOP | LOOKUP_SUCCESS | TRAILING_SLASH:
                                [[fallthrough]];
                        case IN_LINK_LOOP | LOOKUP_SUCCESS:
                        { /*OK*/
                                // We are resolving a link but not its last component, we expect a dir.
                                if ((S_ISDIR(chld_vfsn->m_mode) == true))
                                {
                                        vfsn      = chld_vfsn;
                                        chld_vfsn = nullptr;
                                        vfs_node_rel(prnt_vfsn);
                                        prnt_vfsn = nullptr;
                                        break; // goto link_resolve_continue;
                                }
                                else
                                {
                                        retval = KERN_FAIL(ENOTDIR);
                                        vfs_node_rel(chld_vfsn);
                                        chld_vfsn = nullptr;
                                }
                        }
                                [[fallthrough]];
                        case IN_LINK_LOOP | LOOKUP_FAILURE | TRAILING_SLASH | LAST_COMPONENT:
                                [[fallthrough]];
                        case IN_LINK_LOOP | LOOKUP_FAILURE | TRAILING_SLASH:
                                [[fallthrough]];
                        case IN_LINK_LOOP | LOOKUP_FAILURE | LAST_COMPONENT:
                                [[fallthrough]];
                        case IN_LINK_LOOP | LOOKUP_FAILURE:
                        { /*OK*/
                                // We are somewhere(not the last cn) in resolving a link
                                // And we failed, we don't have a child
                                KASSERT(chld_vfsn == nullptr);
                        }
                                [[fallthrough]];
                        case IN_LINK_LOOP | LOOKUP_SUCCESS | LAST_COMPONENT | TRAILING_SLASH:
                                [[fallthrough]];
                        case IN_LINK_LOOP | LOOKUP_SUCCESS | LAST_COMPONENT:
                        { /*OK*/
                                // We have successfully resolved a link
                                vfs_pathbuf_free(cnbeg);
                                depth_ctx[idx] = {};
                                idx--;

                                // Reset the sate as it was before we started resolving the link
                                vfs_node_rel(prnt_vfsn);
                                prnt_vfsn = nullptr; // we don't need the link parent
                                prnt_vfsn = vfsn;
                                vfsn      = nullptr; // restore the old parent

                                if ((idx == 0) && (cnam_next == nullptr))
                                {
                                        // The last component was a link and we just resolved it.
                                        // Re lock the parent.
                                        vm_object_lock(&prnt_vfsn->m_vmo);
                                }

                                goto link_resolve_finish;
                        }

                        default:
                        {
                                panic("Unknown state");
                                break;
                        }
                }
        }

        KASSERT(idx == 0);

        /* Paths like "/////////........n""  */
        if (KERN_STATUS_SUCCESS(retval) && (*a_prnt_vfsn_out == nullptr) && (*a_chld_vfsn_out == nullptr))
        {
                *a_prnt_vfsn_out = a_vfsn;
                // vfs_node_ref(*a_prnt_vfsn_out = a_vfsn); We already refed it once at the beginning but did not enter the loop
                vfs_node_ref(*a_chld_vfsn_out = a_vfsn);
                vm_object_lock(&a_vfsn->m_vmo);
        }
        return retval;
}
#undef cnam
#undef cnamlen
#undef cnam_end
#undef cnam_next
#undef cnamlen_next
#undef vfsn

// ********************************************************************************************************************************
kern_return_t vfs_node_lookup(vfs_node_t a_vfsn,
                              const char *a_path,
                              natural_t a_path_len,
                              vfs_node_t *a_vfsn_out,
                              boolean_t a_symlink_follow)
// ********************************************************************************************************************************
{

        if (IS_ROOT(a_path, a_path_len))
        {
                // We already have it
                vfs_node_ref(*a_vfsn_out = a_vfsn);
                vm_object_lock(&(*a_vfsn_out)->m_vmo);

                return KERN_SUCCESS;
        }

        char const *lastcn   = nullptr;
        natural_t lastcn_len = 0;
        vfs_node_t prnt_vfsn = nullptr;

        kern_return_t retval = start_lookup(a_vfsn,
                                            a_path,
                                            a_path_len,
                                            VFS_NODE_LOOKUP_GLANCE,
                                            &prnt_vfsn,
                                            a_vfsn_out,
                                            &lastcn,
                                            &lastcn_len,
                                            a_symlink_follow,
                                            0);

        if (KERN_STATUS_SUCCESS(retval))
        {
                if (prnt_vfsn != *a_vfsn_out)
                {
                        vm_object_lock(&(*a_vfsn_out)->m_vmo);
                        vm_object_unlock(&prnt_vfsn->m_vmo);
                }
                vfs_node_rel(prnt_vfsn);
        }

        return retval;
        // char banja[] = "./usr/src/microwindows/src/demoscripts/fonts/em-fonts/Accidental Presidency.ttf";
        // a_path = banja;
        // a_path_len = sizeof(banja) - 1;
        // char banja[] = "/usr/include/././arpa/.//../john/inet.h";
        // char banja[] = "/bin/bash";
        // a_path = banja;
        // a_path_len = sizeof(banja) - 1;
}

// ********************************************************************************************************************************
kern_return_t vfs_node_lookup_create(vfs_node_t a_vfsn,
                                     const char *a_path,
                                     natural_t a_path_len,
                                     vfs_node_t *a_prnt_vfsn_out,
                                     const char **a_cnpnam_out,
                                     natural_t *a_cnpnamlen_out,
                                     vfs_node_t *a_chld_vfsn_out,
                                     boolean_t a_follow_symlink,
                                     boolean_t a_create_directory)
// ********************************************************************************************************************************
{
        if (IS_ROOT(a_path, a_path_len))
        {
                return KERN_FAIL(EEXIST);
        }

        char const *lastcn   = nullptr;
        natural_t lastcn_len = 0;
        vfs_node_t prnt_vfsn = nullptr;
        vfs_node_t chld_vfsn = nullptr;

        int flags = 0;

        if (a_create_directory == true)
        {
                flags |= VFS_NODE_LOOKUP_FLAG_CREATEDIR;
        }

        kern_return_t retval = start_lookup(a_vfsn,
                                            a_path,
                                            a_path_len,
                                            VFS_NODE_LOOKUP_CREATE,
                                            &prnt_vfsn,
                                            a_chld_vfsn_out ? &chld_vfsn : nullptr,
                                            &lastcn,
                                            &lastcn_len,
                                            a_follow_symlink,
                                            flags);

        if (KERN_STATUS_SUCCESS(retval))
        {
                *a_cnpnam_out    = lastcn;
                *a_cnpnamlen_out = lastcn_len;
                *a_prnt_vfsn_out = prnt_vfsn;

                if ((a_chld_vfsn_out != nullptr) && (chld_vfsn != nullptr))
                {
                        if (chld_vfsn != prnt_vfsn)
                        {
                                vm_object_lock(&chld_vfsn->m_vmo);
                                vm_object_unlock(&prnt_vfsn->m_vmo);
                        }

                        vfs_node_rel(prnt_vfsn);

                        *a_chld_vfsn_out = chld_vfsn;

                        *a_prnt_vfsn_out = nullptr;
                }
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t vfs_node_lookup_delete(vfs_node_t a_vfsn,
                                     const char *a_kpath,
                                     natural_t a_path_len,
                                     boolean_t a_follow_symlink,
                                     vfs_node_t *a_parent_vfsn_out,
                                     vfs_node_t *a_child_vfsn_out,
                                     const char **a_child_cnp,
                                     natural_t *a_child_cnplen)
// ********************************************************************************************************************************
{
        if (IS_ROOT(a_kpath, a_path_len))
        {
                // Can't delete root
                return KERN_FAIL(EBUSY);
        }

        char const *lastcn   = nullptr;
        natural_t lastcn_len = 0;
        vfs_node_t prnt_vfsn = nullptr;
        vfs_node_t chld_vfsn = nullptr;

        kern_return_t retval = start_lookup(a_vfsn,
                                            a_kpath,
                                            a_path_len,
                                            VFS_NODE_LOOKUP_DELETE,
                                            &prnt_vfsn,
                                            &chld_vfsn,
                                            &lastcn,
                                            &lastcn_len,
                                            a_follow_symlink,
                                            0);

        if (KERN_STATUS_SUCCESS(retval))
        {
                *a_child_cnp       = lastcn;
                *a_child_cnplen    = lastcn_len;
                *a_parent_vfsn_out = prnt_vfsn;
                *a_child_vfsn_out  = chld_vfsn;
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t vfs_node_lookup_rename(vfs_node_t a_src_start_vfsn,
                                     const char *a_src_kpath,
                                     natural_t a_src_path_len,
                                     vfs_node_t a_dst_start_vfsn,
                                     const char *a_dst_kpath,
                                     natural_t a_dst_path_len,
                                     boolean_t a_follow_symlink,
                                     vfs_node_t *a_src_vfsn_out,
                                     const char **a_src_cnpnam_out,
                                     natural_t *a_src_cnpnamlen_out,
                                     vfs_node_t *a_dst_vfsn_out,
                                     const char **a_dst_cnpnam_out,
                                     natural_t *a_dst_cnpnamlen_out)
// ********************************************************************************************************************************
{
        if (IS_ROOT(a_src_kpath, a_src_path_len) || IS_ROOT(a_dst_kpath, a_dst_path_len))
        {
                // Can't rename root
                return KERN_FAIL(EBUSY);
        }

        char const *src_lastcn   = nullptr;
        natural_t src_lastcn_len = 0;
        vfs_node_t src_prnt_vfsn = nullptr;
        vfs_node_t src_chld_vfsn = nullptr;

        kern_return_t retval = start_lookup(a_src_start_vfsn,
                                            a_src_kpath,
                                            a_src_path_len,
                                            VFS_NODE_LOOKUP_DELETE,
                                            &src_prnt_vfsn,
                                            &src_chld_vfsn,
                                            &src_lastcn,
                                            &src_lastcn_len,
                                            a_follow_symlink,
                                            0);
        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        vm_object_unlock(&src_prnt_vfsn->m_vmo);

        defer(vfs_node_rel(src_chld_vfsn));
        defer(vfs_node_rel(src_prnt_vfsn));

        if (IS_DOT(src_lastcn, src_lastcn_len) || IS_DOT_DOT(src_lastcn, src_lastcn_len))
        {
                return KERN_FAIL(EBUSY);
        }

        // TODO: We must unlock src parent and child

        char const *dst_lastcn   = nullptr;
        natural_t dst_lastcn_len = 0;
        vfs_node_t dst_prnt_vfsn = nullptr;
        vfs_node_t dst_chld_vfsn = nullptr;

        retval = start_lookup(a_dst_start_vfsn,
                              a_dst_kpath,
                              a_dst_path_len,
                              VFS_NODE_LOOKUP_RENAME,
                              &dst_prnt_vfsn,
                              &dst_chld_vfsn,
                              &dst_lastcn,
                              &dst_lastcn_len,
                              a_follow_symlink,
                              S_ISDIR(src_chld_vfsn->m_mode) ? VFS_NODE_LOOKUP_FLAG_CREATEDIR : 0);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        if (src_prnt_vfsn != dst_prnt_vfsn)
        {
                vm_object_lock(&src_prnt_vfsn->m_vmo);
        }

        if (dst_chld_vfsn != nullptr)
        {
                defer(vfs_node_rel(dst_chld_vfsn));
                defer(vfs_node_rel(dst_prnt_vfsn));

                if (IS_DOT(dst_lastcn, dst_lastcn_len) || IS_DOT_DOT(dst_lastcn, dst_lastcn_len))
                {
                        return KERN_FAIL(EBUSY);
                }

                if (((S_ISDIR(dst_chld_vfsn->m_mode) == true) && (S_ISDIR(src_chld_vfsn->m_mode) == false)))
                {
                        return KERN_FAIL(EISDIR);
                }

                if ((S_ISDIR(src_chld_vfsn->m_mode) == true) && (S_ISDIR(dst_chld_vfsn->m_mode) == false))
                {
                        return KERN_FAIL(ENOTDIR);
                }

                if (src_prnt_vfsn->m_mount != dst_prnt_vfsn->m_mount)
                {
                        return KERN_FAIL(EXDEV);
                }

                vfs_node_ref(dst_prnt_vfsn); // For the return value
        }
        else
        {
                defer(vfs_node_rel(dst_prnt_vfsn));

                if (src_prnt_vfsn->m_mount != dst_prnt_vfsn->m_mount)
                {
                        return KERN_FAIL(EXDEV);
                }

                vfs_node_ref(dst_prnt_vfsn); // For the return value
        }

        vfs_node_ref(src_prnt_vfsn); // For the return value

        KASSERT(vm_object_is_locked(&src_prnt_vfsn->m_vmo));
        KASSERT(vm_object_is_locked(&dst_prnt_vfsn->m_vmo));

        *a_src_vfsn_out      = src_prnt_vfsn;
        *a_src_cnpnam_out    = src_lastcn;
        *a_src_cnpnamlen_out = src_lastcn_len;

        *a_dst_vfsn_out      = dst_prnt_vfsn;
        *a_dst_cnpnam_out    = dst_lastcn;
        *a_dst_cnpnamlen_out = dst_lastcn_len;

        return retval;
}

// ********************************************************************************************************************************
void vfs_node_init(vfs_node_t a_vfsn)
// ********************************************************************************************************************************
{
        vm_object_init(&a_vfsn->m_vmo);
        a_vfsn->m_fops         = nullptr;
        a_vfsn->m_ops          = nullptr;
        a_vfsn->m_mounted_here = nullptr;
        a_vfsn->m_mode         = 0;
        a_vfsn->m_rdev         = 0;
}

// ********************************************************************************************************************************
kern_return_t vfs_node_read(vfs_node_t a_vfsn, natural_t a_offset, void *a_buf_out, natural_t a_buf_len)
// ********************************************************************************************************************************
{
        KASSERT(vm_object_is_locked(&a_vfsn->m_vmo));

        struct iovec iov = { .iov_base = a_buf_out, .iov_len = a_buf_len };
        memory_rdwr_data mrw;
        mrw.uio_iov    = &iov;
        mrw.uio_iovcnt = 1;
        mrw.uio_rw     = UIO_READ;
        mrw.uio_segflg = UIO_SYSSPACE;
        mrw.uio_offset = a_offset;
        mrw.uio_resid  = iov.iov_len;

        kern_return_t retval = a_vfsn->m_ops->rdwr(a_vfsn, O_RDWR, &mrw);

        return retval;
}

// ********************************************************************************************************************************
kern_return_t vfs_node_stat(vfs_node_t a_vfsn, vfs_node_stat_t a_vfsn_stat)
// ********************************************************************************************************************************
{
        KASSERT(vm_object_is_locked(&a_vfsn->m_vmo));

        kern_return_t retval = a_vfsn->m_ops->stat(a_vfsn, a_vfsn_stat);

        return retval;
}

// ********************************************************************************************************************************
kern_return_t vfs_node_readlink(vfs_node_t a_vfsn, char *a_buf, natural_t *a_buflen)
// ********************************************************************************************************************************
{
        KASSERT(vm_object_is_locked(&a_vfsn->m_vmo));

        if (S_ISLNK(a_vfsn->m_mode) == false)
        {
                return KERN_FAIL(EINVAL);
        }

        kern_return_t retval = a_vfsn->m_ops->readlink(a_vfsn, a_buf, a_buflen);

        return retval;
}

// ********************************************************************************************************************************
kern_return_t vfs_node_utimens(vfs_node_t a_vfsn, struct timespec *a_timens)
// ********************************************************************************************************************************
{
        KASSERT(vm_object_is_locked(&a_vfsn->m_vmo));

        // TODO: Handle UTIME_NOW and friends here
        kern_return_t retval = a_vfsn->m_ops->utimens(a_vfsn, a_timens);
        return retval;
}

// ********************************************************************************************************************************
kern_return_t vfs_node_chmod(vfs_node_t a_vfsn, mode_t a_mode)
// ********************************************************************************************************************************
{
        KASSERT(vm_object_is_locked(&a_vfsn->m_vmo));

        kern_return_t retval = a_vfsn->m_ops->chmod(a_vfsn, a_mode);
        return retval;
}

// ********************************************************************************************************************************
kern_return_t vfs_node_chown(vfs_node_t a_vfsn, uid_t a_uid, gid_t a_gid)
// ********************************************************************************************************************************
{
        KASSERT(vm_object_is_locked(&a_vfsn->m_vmo));

        // We don't have users or groups atm, we only call the fs layer if it really wants it
        kern_return_t retval = KERN_SUCCESS;

        if (a_vfsn->m_ops->chown)
        {
                retval = a_vfsn->m_ops->chown(a_vfsn, a_uid, a_gid);
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t vfs_node_fsync(vfs_node_t a_vfsn)
// ********************************************************************************************************************************
{
        KASSERT(vm_object_is_locked(&a_vfsn->m_vmo));

        kern_return_t retval = a_vfsn->m_ops->fsync(a_vfsn);

        return retval;
}

// ********************************************************************************************************************************
kern_return_t vfs_node_create(vfs_node_t a_vfsn, char const *a_cname, natural_t a_cname_len, mode_t a_mode, vfs_node_t *a_vfsn_out)
// ********************************************************************************************************************************
{
        KASSERT(vm_object_is_locked(&a_vfsn->m_vmo));

        a_mode = fs_context_cmode(uthread_self()->m_fsctx, a_mode);

        kern_return_t retval = a_vfsn->m_ops->create(a_vfsn, a_cname, a_cname_len, a_mode, a_vfsn_out);

        return retval;
}

// ********************************************************************************************************************************
kern_return_t vfs_node_mknod(vfs_node_t a_vfsn, char const *a_cname, natural_t a_cname_len, mode_t a_mode, dev_t a_dev)
// ********************************************************************************************************************************
{
        KASSERT(vm_object_is_locked(&a_vfsn->m_vmo));

        a_mode = (a_mode & S_IFMT) | fs_context_cmode(uthread_self()->m_fsctx, a_mode);

        kern_return_t retval = a_vfsn->m_ops->mknod(a_vfsn, a_cname, a_cname_len, a_mode, a_dev);

        return retval;
}

// ********************************************************************************************************************************
kern_return_t vfs_node_mkdir(vfs_node_t a_vfsn, char const *a_cname, natural_t a_cname_len, mode_t a_mode)
// ********************************************************************************************************************************
{
        KASSERT(vm_object_is_locked(&a_vfsn->m_vmo));

        a_mode = fs_context_cmode(uthread_self()->m_fsctx, a_mode);

        kern_return_t retval = a_vfsn->m_ops->mkdir(a_vfsn, a_cname, a_cname_len, a_mode);

        return retval;
}

// ********************************************************************************************************************************
kern_return_t vfs_node_symlink(vfs_node_t a_vfsn,
                               char const *a_cname,
                               natural_t a_cname_len,
                               char const *a_tname,
                               natural_t a_tname_len)
// ********************************************************************************************************************************
{
        KASSERT(vm_object_is_locked(&a_vfsn->m_vmo));

        mode_t mode = fs_context_cmode(uthread_self()->m_fsctx, S_IRWXU | S_IRWXG | S_IRWXO);

        kern_return_t retval = a_vfsn->m_ops->symlink(a_vfsn, a_cname, a_cname_len, a_tname, a_tname_len, mode);

        return retval;
}

// ********************************************************************************************************************************
kern_return_t vfs_node_link(vfs_node_t a_vfsn, vfs_node_t a_target_vfsn, char const *a_tcname, natural_t a_tcname_len)
// ********************************************************************************************************************************
{
        KASSERT(vm_object_is_locked(&a_vfsn->m_vmo));

        // TODO: Check that both a_vfsn a_target_vfsn belong to the same mount
        kern_return_t retval = a_vfsn->m_ops->link(a_vfsn, a_target_vfsn, a_tcname, a_tcname_len);

        return retval;
}

// ********************************************************************************************************************************
kern_return_t vfs_node_unlink(vfs_node_t a_vfsn,
                              vfs_node_t a_target_vfsn,
                              char const *a_tcname,
                              natural_t a_tcname_len,
                              boolean_t a_remove_dir)
// ********************************************************************************************************************************
{
        KASSERT(vm_object_is_locked(&a_vfsn->m_vmo));

        kern_return_t retval;

        if (a_remove_dir == true)
        {
                if (IS_ROOT(a_tcname, a_tcname_len))
                {
                        retval = KERN_FAIL(EBUSY);
                        goto out;
                }

                if (IS_DOT(a_tcname, a_tcname_len))
                {
                        retval = KERN_FAIL(EINVAL);
                        goto out;
                }

                if (IS_DOT_DOT(a_tcname, a_tcname_len))
                {
                        retval = KERN_FAIL(ENOTEMPTY);
                        goto out;
                }

                retval = a_vfsn->m_ops->rmdir(a_vfsn, a_target_vfsn, a_tcname, a_tcname_len);
        }
        else
        {
                if (S_ISDIR(a_target_vfsn->m_mode))
                {
                        retval = KERN_FAIL(EISDIR);
                        goto out;
                }

                retval = a_vfsn->m_ops->remove(a_vfsn, a_target_vfsn, a_tcname, a_tcname_len);
        }

out:

        return retval;
}

// ********************************************************************************************************************************
kern_return_t vfs_node_truncate(vfs_node_t a_vfsn, off_t a_length)
// ********************************************************************************************************************************
{
        KASSERT(vm_object_is_locked(&a_vfsn->m_vmo));

        kern_return_t retval = a_vfsn->m_ops->truncate(a_vfsn, a_length);

        return retval;
}

// ********************************************************************************************************************************
kern_return_t vfs_node_rename(vfs_node_t a_src_prnt_vfsn,
                              char const *a_src_chld_nam,
                              natural_t a_src_chld_nam_len,
                              vfs_node_t a_trgt_prnt_vfsn,
                              char const *a_trgt_chld_nam,
                              natural_t a_trgt_chld_nam_len)
// ********************************************************************************************************************************
{
        // Do some checks here.
        KASSERT(vm_object_is_locked(&a_src_prnt_vfsn->m_vmo));
        KASSERT(vm_object_is_locked(&a_trgt_prnt_vfsn->m_vmo));

        kern_return_t retval = a_src_prnt_vfsn->m_ops->rename(a_src_prnt_vfsn,
                                                              a_src_chld_nam,
                                                              a_src_chld_nam_len,
                                                              a_trgt_prnt_vfsn,
                                                              a_trgt_chld_nam,
                                                              a_trgt_chld_nam_len);

        return retval;
}

// ********************************************************************************************************************************
dev_t vfs_node_device(vfs_node_t a_vfsn)
// ********************************************************************************************************************************
{
        return a_vfsn->m_rdev;
}

// ********************************************************************************************************************************
kern_return_t vfs_node_mount_here(vfs_node_t a_vfsn, vfs_mount_t a_mount)
// ********************************************************************************************************************************
{
        KASSERT(a_vfsn->m_mounted_here == nullptr);
        a_vfsn->m_mounted_here = a_mount;
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t vfs_node_socket_id_set(vfs_node_t a_vfsn, natural_t a_socket_id)
// ********************************************************************************************************************************
{
        KASSERT(vm_object_is_locked(&a_vfsn->m_vmo));

        if (S_ISSOCK(a_vfsn->m_mode) == false)
        {
                return KERN_FAIL(ENOTSOCK);
        }

        if (a_vfsn->m_socket_id != 0)
        {
                return KERN_FAIL(EADDRINUSE);
        }

        a_vfsn->m_socket_id = a_socket_id;

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t vfs_node_socket_id_get(vfs_node_t a_vfsn, natural_t *a_socket_id_out)
// ********************************************************************************************************************************
{
        KASSERT(vm_object_is_locked(&a_vfsn->m_vmo));

        if (S_ISSOCK(a_vfsn->m_mode) == false)
        {
                return KERN_FAIL(ENOTSOCK);
        }

        if (a_vfsn->m_socket_id == 0)
        {
                return KERN_FAIL(ECONNREFUSED);
        }

        *a_socket_id_out = a_vfsn->m_socket_id;

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
void vfs_node_ref(vfs_node_t a_vfsn)
// ********************************************************************************************************************************
{
        vm_object_ref(&a_vfsn->m_vmo);
}

// ********************************************************************************************************************************
void vfs_node_rel(vfs_node_t a_vfsn)
// ********************************************************************************************************************************
{
        vm_object_rel(&a_vfsn->m_vmo);
}

// ********************************************************************************************************************************
void vfs_node_lock(vfs_node_t a_vfsn)
// ********************************************************************************************************************************
{
        vm_object_lock(&a_vfsn->m_vmo);
}

// ********************************************************************************************************************************
void vfs_node_unlock(vfs_node_t a_vfsn)
// ********************************************************************************************************************************
{
        vm_object_unlock(&a_vfsn->m_vmo);
}

// ********************************************************************************************************************************
void vfs_node_put(vfs_node_t a_vfsn)
// ********************************************************************************************************************************
{
        vm_object_unlock(&a_vfsn->m_vmo);
        vm_object_rel(&a_vfsn->m_vmo);
}

// ********************************************************************************************************************************
void vfs_rename_lock_shared()
// ********************************************************************************************************************************
{
        lock_read(&s_rename_lock);
}

// ********************************************************************************************************************************
void vfs_rename_lock_shared_done()
// ********************************************************************************************************************************
{
        lock_read_done(&s_rename_lock);
}

// ********************************************************************************************************************************
void vfs_rename_lock_exclusive()
// ********************************************************************************************************************************
{
        lock_write(&s_rename_lock);
}

// ********************************************************************************************************************************
void vfs_rename_lock_exclusive_done()
// ********************************************************************************************************************************
{
        lock_write_done(&s_rename_lock);
}

// ********************************************************************************************************************************
static kern_return_t vfs_node_early_init()
// ********************************************************************************************************************************
{
        lock_init(&s_rename_lock, true);
        return KERN_SUCCESS;
}

early_initcall(vfs_node_early_init);

int vfs_cache_hits;
int vfs_cache_miss;

// ********************************************************************************************************************************
static kern_return_t vfs_cache_init()
// ********************************************************************************************************************************
{
        queue_init(&s_cache_lru);

        vfs_cache_init(&s_vfsc);

        for (struct dircache_entry_data &node : s_vfs_cache_storage)
        {
                node.m_link    = {};
                node.m_lrulink = {};
                node.m_vfsn    = nullptr;
                enqueue_head(&s_cache_lru, &node.m_lrulink);
        }

        vfs_cache_hits = 0;
        vfs_cache_miss = 0;

        return KERN_SUCCESS;
}

early_initcall(vfs_cache_init);
