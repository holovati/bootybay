#include <aux/defer.hh>
#include <aux/time.h>

#include <kernel/mach_clock.h>
#include <kernel/zalloc.h>
#include <kernel/vm/vm_kern.h>
#include <kernel/thread_data.h>

#include <emerixx/vfs.h>
#include <emerixx/poll.h>
#include <emerixx/fdesc.hh>
#include <emerixx/process.h>
#include <emerixx/sleep.h>

#include <signal.h>

/*
 * compute third argument to timeout() from an absolute time.
 */
static int timespec_to_ticks(timespec const *a_ts)
{
        /*
         * If number of milliseconds will fit in 32 bit arithmetic,
         * then compute number of milliseconds to time and scale to
         * ticks.  Otherwise just compute number of hz in time, rounding
         * times greater than representible to maximum value.
         *
         * Delta times less than 25 days can be computed ``exactly''.
         * Maximum value for any timeout in 10ms ticks is 250 days.
         */
        // s = splhigh();

        timespec ts, sleepspec = *a_ts;
        mach_get_timespec(&ts);

        timespec_add_msec(&sleepspec, &sleepspec, timespec_to_msec(&ts));

        timeval tmp, tv;
        TIMESPEC_TO_TIMEVAL(&tv, &sleepspec);
        TIMESPEC_TO_TIMEVAL(&tmp, &ts);

        // tmp.tv_sec  = g_time.seconds;
        // tmp.tv_usec = g_time.microseconds;

        long ticks = 0;
        long sec   = tv.tv_sec - tmp.tv_sec;
        if (sec <= 0x7fffffff / 1000 - 1000)
        {
                ticks = ((tv.tv_sec - tmp.tv_sec) * 1000 + (tv.tv_usec - tmp.tv_usec) / 1000) / (tick / 1000);
        }
        else if (sec <= 0x7fffffff / hz)
        {
                ticks = sec * hz;
        }
        else
        {
                ticks = 0x7fffffff;
        }
        // splx(s);
        return (ticks);
}

struct poll_entry
{
        queue_chain_t m_link;
        pollfd *m_pfd;
        poll_device_context_t *m_dev_record;
        void *m_sleep_addr;
};

static inline size_t poll_exit_internal(poll_entry *a_pe, nfds_t a_nfds)
{
        size_t completed_polls = 0;
        pthread_t thread       = uthread_self();
        for (nfds_t i = 0; i < a_nfds; ++i)
        {
                vfs_file_t f         = nullptr;
                kern_return_t retval = fd_context_get_file(thread->m_fdctx, a_pe[i].m_pfd->fd, &f);

                if (KERN_STATUS_SUCCESS(retval))
                {
                        // One for now and one for when we installed the poll
                        vfs_file_rel(f);
                        vfs_file_rel(f);
                }

                if (a_pe[i].m_dev_record != nullptr)
                {
                        remqueue(&a_pe[i].m_link);
                        a_pe[i].m_dev_record = nullptr;
                }

                if (a_pe[i].m_pfd->revents != 0)
                {
                        completed_polls++;
                }
        }
        return completed_polls;
}

kern_return_t poll_record(poll_device_context_t *a_pdc, pollfd **a_pfd)
{
        if ((*a_pfd)->revents == 0)
        {
                poll_entry *pe   = (poll_entry *)(((natural_t)a_pfd) - offsetof(poll_entry, m_pfd));
                pe->m_dev_record = a_pdc;
                enqueue_tail(pe->m_dev_record, &pe->m_link);
        }

        return KERN_SUCCESS;
}

kern_return_t poll_wakeup(poll_device_context_t *a_pdc, poll_event_t a_events)
{
        for (queue_entry_t qe = queue_first(a_pdc); queue_end(a_pdc, qe) == false;)
        {
                poll_entry *pe = (poll_entry *)qe;
                qe             = queue_next(qe);

                if (pe->m_pfd->events & a_events)
                {
                        if (a_pdc != pe->m_dev_record)
                        {
                                panic("pdc(%p) != dev_record(%p)", a_pdc, pe->m_dev_record);
                        }
                        remqueue(&pe->m_link);

                        pe->m_dev_record = nullptr;

                        pe->m_pfd->revents |= (pe->m_pfd->events & a_events);

                        uthread_wakeup(pe->m_sleep_addr);
                }
        }

        return KERN_SUCCESS;
}

poll_event_t poll_enum_wanted_events(poll_device_context_t *a_pdc)
{
        poll_event_t events = 0;

        for (queue_entry_t qe = queue_first(a_pdc); queue_end(a_pdc, qe) == false;)
        {
                poll_entry *pe = (poll_entry *)qe;
                qe             = queue_next(qe);

                events |= pe->m_pfd->events;
        }

        return events;
}

struct pollentry_storage_data
{
        poll_entry pe_data[0x100];
};

ZONE_DEFINE(pollentry_storage_data, s_pollentry_storage, 0x100, 0x100 / 2, ZONE_EXHAUSTIBLE, 0x20);

kern_return_t ppoll(pollfd *a_fds, nfds_t a_nfds, const timespec *a_ts, const sigset_t *a_mask)
{
        if (a_nfds > 0x100)
        {
                return KERN_FAIL(ENOMEM);
        }

        poll_entry *pe = (poll_entry *)s_pollentry_storage.alloc();
        defer(s_pollentry_storage.free((pollentry_storage_data *)pe));

        pthread_t thread = uthread_self();

        sigset_t osset;
        if (a_mask != nullptr)
        {
                kern_return_t retval = uthread_sigprocmask(thread, SIG_SETMASK, a_mask, &osset);

                if (KERN_STATUS_FAILURE(retval))
                {
                        return retval;
                }
        }

        // Reset the sigmask if needed
        defer(if (a_mask != nullptr) { uthread_sigprocmask(thread, SIG_SETMASK, &osset, nullptr); });

        spl_t s = splhigh();
        defer(splx(s));

        // Lock thread files

        size_t completed_polls = 0;

        for (nfds_t i = 0; i < a_nfds; ++i)
        {
                a_fds[i].revents = 0;

                // If the value of fd is less than 0, events shall be ignored, and revents shall be set to 0 in that entry
                // on return from poll().
                if (a_fds[i].fd < 0)
                {
                        continue;
                }

                pe[i] = { {}, &a_fds[i], nullptr, pe };

                vfs_file_t f         = nullptr;
                kern_return_t retval = fd_context_get_file(thread->m_fdctx, pe[i].m_pfd->fd, &f);

                if (KERN_STATUS_FAILURE(retval))
                {
                        pe[i].m_pfd->revents = POLLNVAL;
                        completed_polls++;
                        continue;
                }

                if (KERN_STATUS_FAILURE(vfs_file_poll(f, &pe[i].m_pfd)))
                {
                        pe[i].m_pfd->revents = POLLERR;
                }
                else
                {
                        if (pe[i].m_pfd->revents)
                        {
                                completed_polls++;
                        }
                }
        }

        // If none of the events requested (and no error) has occurred for any of the file descriptors, then poll() blocks
        // until one of the events occurs. The reason we check here is because we might have recorded errors above;
        if (completed_polls > 0)
        {
                return poll_exit_internal(pe, a_nfds);
        }

        kern_return_t retval = 0;
        if (a_ts == nullptr)
        {
                // If timeout_ts is specified as NULL, then ppoll() can block indefinitely.
                // retval = tsleep((event_t)&complete_polls, PUSER | PCATCH, "poll", 0);
                retval = uthread_sleep(pe);
        }
        else
        {
                // Specifying a timeout of zero causes poll() to return immediately, even if no file descriptors are ready
                if (timespec_is_zero(a_ts))
                {
                        return poll_exit_internal(pe, a_nfds);
                }

                int sleep_ticks = timespec_to_ticks(a_ts);

                if (sleep_ticks == 0)
                {
                        sleep_ticks = 1;
                }
                retval = uthread_sleep_timeout(pe /*, PUSER | PCATCH, "poll"*/, sleep_ticks, nullptr);
        }

        completed_polls = poll_exit_internal(pe, a_nfds);

        extern int curr_ipl;
        KASSERT(curr_ipl == SPLHI);

        if (retval == KERN_FAIL(EINTR))
        {
                // See proc::on_signal_return. A signal aborted the sleep, a restart might block forver.
                // ERESTART_RESTARTBLOCK will return EINTR to userspace.
                if (completed_polls == 0)
                {
                        // Interrupted and nothing completed, return EINTR to userspace
                        retval = KERN_FAIL(ERESTART_RESTARTBLOCK);
                }
                else
                {
                        retval = KERN_SUCCESS;
                }
        }
        else if (retval == KERN_FAIL(EWOULDBLOCK))
        {
                // Timeout
                // A poll might have completed after we timed out but before we were scheduled to run
                // Check complete count and modify and modify return value.
                // if (completed_polls > 0)
                //{
                retval = completed_polls;
                //}
        }

        if (completed_polls > 0)
        {
                retval = completed_polls;
        }

        return retval;
}

kern_return_t poll(struct pollfd *a_fds, nfds_t a_nfds, int a_timeout)
{
        if (a_timeout < 0)
        {
                //  Specifying a negative value in timeout means an infinite timeout.
                return ppoll(a_fds, a_nfds, nullptr, nullptr);
        }

        // Specifying a timeout of zero causes poll() to return immediately, even if no file descriptors are ready
        timespec ts;
        timespec_from_msec(&ts, a_timeout);

        return ppoll(a_fds, a_nfds, &ts, nullptr);
}
