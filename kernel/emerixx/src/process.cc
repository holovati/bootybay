#define RBTREE_IMPLEMENTATION
#include <etl/rbtree.hh>
#include <sys/stat.h>

#define NONO 1

#include "../../arch/amd64/src/proc_reg.h"
#include <aux/init.h>
#include <aux/time.h>
#include <kernel/vm/vm_kern.h>
#include <kernel/vm/vm_map.h>
#include <kernel/zalloc.h>
#include <kernel/env.h>

#include <emerixx/memory_rw.h>
#include <mach/conf.h>
#include <aux/defer.hh>
#include <mach/machine/fpu.h>
// #include <mach/task.h>
#include <kernel/thread_data.h>

#include <unistd.h>

#include <emerixx/fdesc.hh>
#include <emerixx/vfs/vfs_file.h>
#include <emerixx/fsdata.hh>
#include <emerixx/vfs/vfs_node.h>
#include <emerixx/vfs/vfs_mount.h>
#include <emerixx/vfs/vfs_file_system.h>

#include <emerixx/process.h>
#include <emerixx/sleep.h>

#include <fcntl.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <sys/wait.h>
#include <sys/sysmacros.h>

#include "tls.h"

#define PTHREAD_MAX (PAGE_SIZE * CHAR_BIT)

#define PROCESS_ZONE_GROWTH_SIZE (PAGE_SIZE / sizeof(process_internal))
#define PTHREAD_ZONE_GROWTH_SIZE (PAGE_SIZE / sizeof(pthread_internal))

#define SIGMASK(s) (((sigset_t)1) << (((sigset_t)(s)) - 1))

#define SIGMASK_CLEAN(m) (m & (~(SIGMASK(SIGKILL) | SIGMASK(SIGSTOP))))

/* Macros for constructing status values.  */
#define __W_EXITCODE(ret, sig) ((ret) << 8 | (sig))
#define __W_STOPCODE(sig)      ((sig) << 8 | 0x7f)
#define __W_CONTINUED          0xffff
#define __WCOREFLAG            0x80

#define GLOBALS_READ_SCOPE_LOCK(spllvl)                                                                                            \
        spl_t s##__LINE__ = spllvl();                                                                                              \
        defer(splx(s##__LINE__));                                                                                                  \
        LOCK_READ_SCOPE(&s_globals_lock)

#define GLOBALS_WRITE_SCOPE_LOCK(spllvl)                                                                                           \
        spl_t s##__LINE__ = spllvl();                                                                                              \
        defer(splx(s##__LINE__));                                                                                                  \
        LOCK_WRITE_SCOPE(&s_globals_lock)

enum SIGNAL_ACTION : uint8_t
{
        SIGNAL_TERMINATE = 0,
        SIGNAL_DUMP,
        SIGNAL_IGNORE,
        SIGNAL_CONTINUE,
        SIGNAL_STOP,
        SIGNAL_CATCH
};

struct process_internal : process
{
        rbtnode_data m_sibling_link;
        rbtnode_data m_allprocess_link;
        rbtnode_data m_group_link;

        RBTREE_UNIQUE_KEY(process_internal, m_sibling_link) m_children;
        RBTREE_UNIQUE_KEY(process_internal, m_sibling_link) m_children_zombie;

        task_t m_kernel_task;
        lock_data_t m_lock;

        timer_elt_data_t m_itimer_elt[3]; /* for getitimer/setitimer */
        itimerspec m_itimer[3];           /*part of the above */

        sigset_t m_pending_signals;
        ksigaction m_user_signal_action[NSIG];
        SIGNAL_ACTION m_kernel_signal_action[NSIG];

        uint8_t freebytes[ROUNDUP(NSIG, alignof(natural_t)) - NSIG - sizeof(int)];

        int m_exit_code;

        int32_t m_refcount;
        int32_t m_wstatus;
};

struct process_internal *PER_TASK_VAR(__s_curproc);
#define s_curproc (*PER_TASK_PTR(__s_curproc))

typedef RBTREE_UNIQUE_KEY(process_internal, m_allprocess_link) process_map;
typedef RBTREE_UNIQUE_KEY(process_internal, m_sibling_link) process_child_map;
typedef RBTREE_UNIQUE_KEY(process_internal, m_group_link) process_group_map;

struct pthread_internal : pthread
{
        rbtnode_data m_allthread_link;
        thread_t m_kernel_thread;
        lock_data_t m_lock;
        sigset_t m_pending_signals;
        sigset_t m_blocked_signals;
        stack_t m_signal_stack;
        int32_t m_refcount;
        int pad;
};

typedef RBTREE_UNIQUE_KEY(pthread_internal, m_allthread_link) pthread_map;

struct utask_group_internal : utask_group
{
        rbtnode_data m_allgroup_link;
        process_group_map m_members;
        lock_data_t m_lock;
        int32_t m_refcount;
        int pad;
};

typedef RBTREE_UNIQUE_KEY(utask_group_internal, m_allgroup_link) utask_group_map;

typedef kern_return_t (*ksighandler_t)(process_internal *, pthread_internal *, int a_signum);

ZONE_DEFINE(process_internal, s_process_zone, PTHREAD_MAX, PROCESS_ZONE_GROWTH_SIZE, ZONE_FIXED, PROCESS_ZONE_GROWTH_SIZE);
ZONE_DEFINE(pthread_internal, s_pthread_zone, PTHREAD_MAX, PTHREAD_ZONE_GROWTH_SIZE, ZONE_FIXED, PTHREAD_ZONE_GROWTH_SIZE);
ZONE_DEFINE(utask_group_internal, s_utask_group_zone, PTHREAD_MAX, PTHREAD_ZONE_GROWTH_SIZE, ZONE_FIXED, PTHREAD_ZONE_GROWTH_SIZE);

static lock_data_t s_globals_lock;
static process_map s_all_process_map;
static pthread_map s_all_pthread_map;
static utask_group_map s_all_utask_group_map;
static size_t s_nprocs;
static size_t s_nthreads;
static size_t s_n_utask_groups;
static pid_t s_last_pid;

static SIGNAL_ACTION const s_default_sigactions[NSIG] = {
        /* Unused           */ SIGNAL_TERMINATE, // Normal termination, used only in kernel
        /* SIGHUP           */ SIGNAL_TERMINATE, // Hang up controlling terminal or process
        /* SIGINT           */ SIGNAL_TERMINATE, // Interrupt from keyboard
        /* SIGQUIT          */ SIGNAL_DUMP,      // Quit from keyboard
        /* SIGILL           */ SIGNAL_DUMP,      // Illegal instruction
        /* SIGTRAP          */ SIGNAL_DUMP,      // Breakpoint for debugging
        /* SIGABRT / SIGIOT */ SIGNAL_DUMP,      // Abnormal termination / Equivalent to SIGABRT
        /* SIGBUS           */ SIGNAL_DUMP,      // Bus error
        /* SIGFPE           */ SIGNAL_DUMP,      // Floating-point exception
        /* SIGKILL          */ SIGNAL_TERMINATE, // Forced-process termination
        /* SIGUSR1          */ SIGNAL_TERMINATE, // Available to processes
        /* SIGSEGV          */ SIGNAL_DUMP,      // Invalid memory reference
        /* SIGUSR2          */ SIGNAL_TERMINATE, // Available to processes
        /* SIGPIPE          */ SIGNAL_TERMINATE, // Write to pipe with no readers
        /* SIGALARM         */ SIGNAL_TERMINATE, // Real-timerclock
        /* SIGTERM          */ SIGNAL_TERMINATE, // Process termination
        /* SIGSTKFLT        */ SIGNAL_TERMINATE, // Coprocessor stack error
        /* SIGCHLD          */ SIGNAL_IGNORE,    // Child process stopped or terminated, or got signal if traced
        /* SIGCONT          */ SIGNAL_CONTINUE,  // Resume execution, if stopped
        /* SIGSTOP          */ SIGNAL_STOP,      // Stop process execution
        /* SIGTSTP          */ SIGNAL_STOP,      // Stop process issued from tty
        /* SIGTTIN          */ SIGNAL_STOP,      // Background process requires input
        /* SIGTTOU          */ SIGNAL_STOP,      // Background process requires output
        /* SIGURG           */ SIGNAL_IGNORE,    // Urgent condition on socket
        /* SIGXCPU          */ SIGNAL_DUMP,      // CPU time limit exceeded
        /* SIGXFSZ          */ SIGNAL_DUMP,      // File size limit exceeded
        /* SIGVTALRM        */ SIGNAL_TERMINATE, // Virtual timer clock
        /* SIGPROF          */ SIGNAL_TERMINATE, // Profile timer clock
        /* SIGWINCH         */ SIGNAL_IGNORE,    // Window resizing
        /* SIGIO            */ SIGNAL_TERMINATE, // I/O now possible
        /* SIGPOLL          */ SIGNAL_TERMINATE, // Equivalent to SIGIO
        /* SIGPWR           */ SIGNAL_TERMINATE, // Power supply failure
        /* SIGSYS           */ SIGNAL_DUMP,      // Bad system call
        /* SIGUNUSED        */ SIGNAL_DUMP,      // Equivalent to SIGSYS
        /* SIGUNUSED        */ SIGNAL_DUMP,      // Equivalent to SIGSYS
        /* SIGUNUSED        */ SIGNAL_DUMP,      // Equivalent to SIGSYS
        /* SIGUNUSED        */ SIGNAL_DUMP,      // Equivalent to SIGSYS
        /* SIGUNUSED        */ SIGNAL_DUMP,      // Equivalent to SIGSYS
        /* SIGUNUSED        */ SIGNAL_DUMP,      // Equivalent to SIGSYS
        /* SIGUNUSED        */ SIGNAL_DUMP,      // Equivalent to SIGSYS
        /* SIGUNUSED        */ SIGNAL_DUMP,      // Equivalent to SIGSYS
        /* SIGUNUSED        */ SIGNAL_DUMP,      // Equivalent to SIGSYS
        /* SIGUNUSED        */ SIGNAL_DUMP,      // Equivalent to SIGSYS
        /* SIGUNUSED        */ SIGNAL_DUMP,      // Equivalent to SIGSYS
        /* SIGUNUSED        */ SIGNAL_DUMP,      // Equivalent to SIGSYS
        /* SIGUNUSED        */ SIGNAL_DUMP,      // Equivalent to SIGSYS
        /* SIGUNUSED        */ SIGNAL_DUMP,      // Equivalent to SIGSYS
        /* SIGUNUSED        */ SIGNAL_DUMP,      // Equivalent to SIGSYS
        /* SIGUNUSED        */ SIGNAL_DUMP,      // Equivalent to SIGSYS
        /* SIGUNUSED        */ SIGNAL_DUMP,      // Equivalent to SIGSYS
        /* SIGUNUSED        */ SIGNAL_DUMP,      // Equivalent to SIGSYS
        /* SIGUNUSED        */ SIGNAL_DUMP,      // Equivalent to SIGSYS
        /* SIGUNUSED        */ SIGNAL_DUMP,      // Equivalent to SIGSYS
        /* SIGUNUSED        */ SIGNAL_DUMP,      // Equivalent to SIGSYS
        /* SIGUNUSED        */ SIGNAL_DUMP,      // Equivalent to SIGSYS
        /* SIGUNUSED        */ SIGNAL_DUMP,      // Equivalent to SIGSYS
        /* SIGUNUSED        */ SIGNAL_DUMP,      // Equivalent to SIGSYS
        /* SIGUNUSED        */ SIGNAL_DUMP,      // Equivalent to SIGSYS
        /* SIGUNUSED        */ SIGNAL_DUMP,      // Equivalent to SIGSYS
        /* SIGUNUSED        */ SIGNAL_DUMP,      // Equivalent to SIGSYS
        /* SIGUNUSED        */ SIGNAL_DUMP,      // Equivalent to SIGSYS
        /* SIGUNUSED        */ SIGNAL_DUMP,      // Equivalent to SIGSYS
        /* SIGUNUSED        */ SIGNAL_DUMP,      // Equivalent to SIGSYS
        /* SIGUNUSED        */ SIGNAL_DUMP,      // Equivalent to SIGSYS
        /* SIGUNUSED        */ SIGNAL_DUMP,      // Equivalent to SIGSYS
};

// Forward declarations
static kern_return_t signal_generate_terminate(process_internal *, pthread_internal *, int); // SIGNAL_TERMINATE
static kern_return_t signal_generate_dump(process_internal *, pthread_internal *, int);      // SIGNAL_DUMP
static kern_return_t signal_generate_ignore(process_internal *, pthread_internal *, int);    // SIGNAL_IGNORE
static kern_return_t signal_generate_continue(process_internal *, pthread_internal *, int);  // SIGNAL_CONTINUE
static kern_return_t signal_generate_stop(process_internal *, pthread_internal *, int);      // SIGNAL_STOP
static kern_return_t signal_generate_catch(process_internal *, pthread_internal *, int);     // SIGNAL_CATCH

static ksighandler_t const s_sig_utask_generation_actions[] = {
        //
        signal_generate_terminate, //
        signal_generate_dump,      //
        signal_generate_ignore,    //
        signal_generate_continue,  //
        signal_generate_stop,      //
        signal_generate_catch,     //
};

static kern_return_t signal_generate_ignore_thread(process_internal *, pthread_internal *, int); // SIGNAL_IGNORE
static ksighandler_t const s_sig_uthread_generation_actions[] = {
        //
        signal_generate_terminate,     //
        signal_generate_dump,          //
        signal_generate_ignore_thread, //
        signal_generate_continue,      //
        signal_generate_stop,          //
        signal_generate_catch,         //
};

static kern_return_t signal_deliver_terminate(process_internal *, pthread_internal *, int); // SIGNAL_TERMINATE
static kern_return_t signal_deliver_dump(process_internal *, pthread_internal *, int);      // SIGNAL_DUMP
static kern_return_t signal_deliver_ignore(process_internal *, pthread_internal *, int);    // SIGNAL_IGNORE
static kern_return_t signal_deliver_continue(process_internal *, pthread_internal *, int);  // SIGNAL_CONTINUE
static kern_return_t signal_deliver_stop(process_internal *, pthread_internal *, int);      // SIGNAL_STOP
static kern_return_t signal_deliver_catch(process_internal *, pthread_internal *, int);     // SIGNAL_CATCH

static ksighandler_t const s_sig_delivery_actions[] = {
        //
        signal_deliver_terminate, //
        signal_deliver_dump,      //
        signal_deliver_ignore,    //
        signal_deliver_continue,  //
        signal_deliver_stop,      //
        signal_deliver_catch,     //
};

// ********************************************************************************************************************************
RBTREE_INSERT_LESS(process_child_map, process_internal)(process_internal &lhs, process_internal &rhs)
// ********************************************************************************************************************************
{
        return lhs.m_pid < rhs.m_pid;
}

// ********************************************************************************************************************************
RBTREE_FIND_LESS(process_child_map, pid_t const &)(pid_t const &lhs, process_internal &rhs)
// ********************************************************************************************************************************
{
        return (lhs > 0) && (lhs < rhs.m_pid); // pid zero match the first child
}

// ********************************************************************************************************************************
RBTREE_FIND_LESS(process_child_map, pid_t const &)(process_internal &lhs, pid_t const &rhs)
// ********************************************************************************************************************************
{
        return (rhs > 0) && (lhs.m_pid < rhs); // pid zero match the first child
}

// ********************************************************************************************************************************
RBTREE_FIND_LESS(process_child_map, process_internal *const &)(process_internal *const &lhs, process_internal &rhs)
// ********************************************************************************************************************************
{
        return lhs->m_pid < rhs.m_pid;
}

// ********************************************************************************************************************************
RBTREE_FIND_LESS(process_child_map, process_internal *const &)(process_internal &lhs, process_internal *const &rhs)
// ********************************************************************************************************************************
{
        return lhs.m_pid < rhs->m_pid;
}

// ********************************************************************************************************************************
RBTREE_FIND_LESS(process_group_map, process_internal *const &)(process_internal *const &lhs, process_internal &rhs)
// ********************************************************************************************************************************
{
        return lhs->m_pid < rhs.m_pid;
}

// ********************************************************************************************************************************
RBTREE_FIND_LESS(process_group_map, process_internal *const &)(process_internal &lhs, process_internal *const &rhs)
// ********************************************************************************************************************************
{
        return lhs.m_pid < rhs->m_pid;
}

// ********************************************************************************************************************************
RBTREE_INSERT_LESS(process_group_map, process_internal)(process_internal &lhs, process_internal &rhs)
// ********************************************************************************************************************************
{
        return lhs.m_pid < rhs.m_pid;
}

// ********************************************************************************************************************************
RBTREE_INSERT_LESS(process_map, process_internal)(process_internal &lhs, process_internal &rhs)
// ********************************************************************************************************************************
{
        return lhs.m_pid < rhs.m_pid;
}

// ********************************************************************************************************************************
RBTREE_FIND_LESS(process_map, pid_t const &)(pid_t const &lhs, process_internal &rhs)
// ********************************************************************************************************************************
{
        return lhs < rhs.m_pid;
}

// ********************************************************************************************************************************
RBTREE_FIND_LESS(process_map, pid_t const &)(process_internal &lhs, pid_t const &rhs)
// ********************************************************************************************************************************
{
        return lhs.m_pid < rhs;
}

// ********************************************************************************************************************************
RBTREE_INSERT_LESS(pthread_map, pthread_internal)(pthread_internal &lhs, pthread_internal &rhs)
// ********************************************************************************************************************************
{
        return lhs.m_tid < rhs.m_tid;
}

// ********************************************************************************************************************************
RBTREE_FIND_LESS(pthread_map, pid_t const &)(pid_t const &lhs, pthread_internal &rhs)
// ********************************************************************************************************************************
{
        return lhs < rhs.m_tid;
}

// ********************************************************************************************************************************
RBTREE_FIND_LESS(pthread_map, pid_t const &)(pthread_internal &lhs, pid_t const &rhs)
// ********************************************************************************************************************************
{
        return lhs.m_tid < rhs;
}

// ********************************************************************************************************************************
RBTREE_FIND_LESS(utask_group_map, pid_t const &)(utask_group_internal &lhs, pid_t const &rhs)
// ********************************************************************************************************************************
{
        return lhs.m_pgid < rhs;
}

// ********************************************************************************************************************************
RBTREE_FIND_LESS(utask_group_map, pid_t const &)(pid_t const &lhs, utask_group_internal &rhs)
// ********************************************************************************************************************************
{
        return lhs < rhs.m_pgid;
}

// ********************************************************************************************************************************
RBTREE_FIND_LESS(utask_group_map, process_internal *const &)(utask_group_internal &lhs, process_internal *const &rhs)
// ********************************************************************************************************************************
{
        return lhs.m_pgid < rhs->m_pgid;
}

// ********************************************************************************************************************************
RBTREE_FIND_LESS(utask_group_map, process_internal *const &)(process_internal *const &lhs, utask_group_internal &rhs)
// ********************************************************************************************************************************
{
        return lhs->m_pgid < rhs.m_pgid;
}

// ********************************************************************************************************************************
RBTREE_INSERT_LESS(utask_group_map, utask_group_internal)(utask_group_internal &lhs, utask_group_internal &rhs)
// ********************************************************************************************************************************
{
        return lhs.m_pgid < rhs.m_pgid;
}

// ********************************************************************************************************************************
static pid_t pid_find_free()
// ********************************************************************************************************************************
{
        for (s_last_pid++; //
             s_last_pid == 0 || s_all_pthread_map.find(s_last_pid) || s_all_process_map.find(s_last_pid)
             || s_all_utask_group_map.find(s_last_pid);
             s_last_pid++)
        {
        }

        return s_last_pid;
}

// ********************************************************************************************************************************
static process_internal *process_alloc(pid_t a_pid)
// ********************************************************************************************************************************
{
        process_internal *proc = s_process_zone.alloc_new();
        proc->m_ppid           = 0;
        proc->m_children.init();
        proc->m_children_zombie.init();
        proc->m_kernel_task = nullptr;
        lock_init(&proc->m_lock, true);
        proc->m_pending_signals = 0;
        proc->m_pid             = a_pid;
        proc->m_refcount        = 1;
        proc->m_wstatus         = 0;
        proc->m_exit_code       = 0;
        KASSERT(s_all_process_map.insert(proc) == proc);
        s_nprocs++;
        return proc;
}

// ********************************************************************************************************************************
static void process_free(process_internal *a_proc)
// ********************************************************************************************************************************
{
        KASSERT(a_proc->m_refcount == 0);
        s_all_process_map.remove(a_proc);
        s_nprocs--;
        s_process_zone.free_delete(a_proc);
}

// ********************************************************************************************************************************
static pthread_internal *pthread_alloc(pid_t a_tid)
// ********************************************************************************************************************************
{
        pthread_internal *thrd = s_pthread_zone.alloc_new();
        thrd->m_tid            = a_tid;
        thrd->m_kernel_thread  = nullptr;
        lock_init(&thrd->m_lock, true);
        thrd->m_pending_signals = 0;
        thrd->m_blocked_signals = 0;
        thrd->m_refcount        = 1;
        thrd->m_pad             = 0xDEADBEEF;
        KASSERT(s_all_pthread_map.insert(thrd) == thrd);
        s_nthreads++;
        return thrd;
}

// ********************************************************************************************************************************
static void pthread_free(pthread_internal *a_uthread)
// ********************************************************************************************************************************
{
        KASSERT(a_uthread->m_refcount == 0);
        s_all_pthread_map.remove(a_uthread);
        s_nthreads--;
        s_pthread_zone.free_delete(a_uthread);
}

// ********************************************************************************************************************************
static utask_group_internal *utask_group_alloc(pid_t a_pgid)
// ********************************************************************************************************************************
{
        utask_group_internal *utsk_grp = s_utask_group_zone.alloc_new();
        utsk_grp->m_pgid               = a_pgid;
        utsk_grp->m_sid                = 0;
        utsk_grp->m_members.init();
        lock_init(&utsk_grp->m_lock, true);
        utsk_grp->m_refcount = 1;
        KASSERT(s_all_utask_group_map.insert(utsk_grp) == utsk_grp);
        s_n_utask_groups++;
        return utsk_grp;
}

// ********************************************************************************************************************************
static void utask_group_free(utask_group_internal *a_utask_grp)
// ********************************************************************************************************************************
{
        KASSERT(a_utask_grp->m_refcount == 0);
        s_all_utask_group_map.remove(a_utask_grp);
        s_n_utask_groups--;
        s_utask_group_zone.free_delete(a_utask_grp);
}

// ********************************************************************************************************************************
kern_return_t signal_generate_terminate(process_internal *a_utask, pthread_internal *a_uthread, int a_signo)
// ********************************************************************************************************************************
{
        a_utask->m_wstatus = __W_EXITCODE(a_utask->m_exit_code, a_signo);
        // Kick wake up a single thread to make the process kill itself
        clear_wait(a_uthread->m_kernel_thread, THREAD_INTERRUPTED, true);
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t signal_deliver_terminate(process_internal *a_utask, pthread_internal *a_uthread, int a_signo)
// ********************************************************************************************************************************
{
        KASSERT(a_utask->m_kernel_task->active);

        KASSERT(KERN_SUCCESS == task_terminate(a_utask->m_kernel_task));

        size_t n_zombies             = 0;
        process_internal *init_utask = s_all_process_map.find(1); // Save a global ptr to init
        for (process_internal *cld_utask = a_utask->m_children.first(); cld_utask;)
        {
                LOCK_WRITE_SCOPE(&init_utask->m_lock);

                process_internal *tmp = process_child_map::next(cld_utask);

                a_utask->m_children.remove(cld_utask);
                init_utask->m_children.insert(cld_utask);
                cld_utask->m_ppid = init_utask->m_pid;
                cld_utask         = tmp;
        }
        for (process_internal *cld_utask = a_utask->m_children_zombie.first(); cld_utask;)
        {
                LOCK_WRITE_SCOPE(&init_utask->m_lock);
                process_internal *tmp = process_child_map::next(cld_utask);

                a_utask->m_children_zombie.remove(cld_utask);
                init_utask->m_children_zombie.insert(cld_utask);
                cld_utask->m_ppid = init_utask->m_pid;
                cld_utask         = tmp;
                n_zombies++;
        }

        if (n_zombies > 0)
        {
                init_utask->m_pending_signals |= SIGMASK(SIGCHLD);
                s_sig_utask_generation_actions[init_utask->m_kernel_signal_action[SIGCHLD]](
                        init_utask,
                        static_cast<pthread_internal *>(((thread_t)init_utask->m_kernel_task->thread_list.next)->pthread),
                        SIGCHLD);
                thread_wakeup_interruptible_only(init_utask);
        }

        for (timer_elt &te : a_utask->m_itimer_elt)
        {
                if (te.set == TELT_SET)
                {
                        reset_timeout(&te);
                }
        }

        fd_context_exit(a_uthread->m_fdctx);
        a_uthread->m_fdctx = nullptr;

        fs_context_deallocate(a_uthread->m_fsctx);
        a_uthread->m_fsctx = nullptr;

        // sighand_exit(a_pthread->pt_sighand);
        // a_pthread->pt_sighand = nullptr;
        process_internal *parent_utask = s_all_process_map.find(a_utask->m_ppid);

        utask_group_internal *utask_grp = s_all_utask_group_map.find(a_utask);
        KASSERT(utask_grp != nullptr);

        lock_write(&utask_grp->m_lock);
        utask_grp->m_members.remove(a_utask);
        if (utask_grp->m_members.empty() && (--(utask_grp->m_refcount)) == 0)
        {
                lock_write_done(&utask_grp->m_lock);
                utask_group_free(utask_grp);
        }
        else
        {
                if (a_utask->m_pid == a_utask->m_pgid
                    && a_utask->m_ctty != makedev(0xFF, 0xFF) /*group leader with associated control terminal*/)
                {
                        // log_debug("SIGHUP if contolling terminal");
                        // Send hangup signal to all members of processgroup
                        // reset process group for all members to 0
                        for (process_internal *grputask = utask_grp->m_members.first(); //
                             grputask != nullptr;                                       //
                             grputask = process_group_map::next(grputask))
                        {
                                pthread_internal *grputhrd = static_cast<pthread_internal *>(
                                        ((thread_t)(grputask->m_kernel_task->thread_list.next))->pthread);

                                if ((grputhrd->m_pending_signals & SIGMASK(SIGHUP)) == 0)
                                {
                                        // grputask->m_pending_signals |= SIGMASK(SIGHUP);
                                        // KASSERT(s_sig_utask_generation_actions[grputask->m_kernel_signal_action[SIGHUP]](
                                        //            grputask, grputhrd, SIGHUP)
                                        //        == KERN_SUCCESS);
                                }

                                //                                clear_wait(grputhrd->m_kernel_thread, THREAD_INTERRUPTED, true);
                        }
                }
        }

        lock_write_done(&utask_grp->m_lock);

        tls_exit();

        thread_deallocate(a_uthread->m_kernel_thread);

        a_uthread->m_kernel_thread = nullptr;
        --a_uthread->m_refcount;

        if (a_uthread->m_tid_address != nullptr)
        {
                emerixx::uio::copyout(0, a_uthread->m_tid_address);
        }

        KASSERT(parent_utask != nullptr);

        LOCK_WRITE_SCOPE(&parent_utask->m_lock);
        parent_utask->m_children.remove(a_utask);
        parent_utask->m_children_zombie.insert(a_utask);

        parent_utask->m_pending_signals |= SIGMASK(SIGCHLD);
        s_sig_utask_generation_actions[parent_utask->m_kernel_signal_action[SIGCHLD]](
                parent_utask,
                static_cast<pthread_internal *>(((thread_t)parent_utask->m_kernel_task->thread_list.next)->pthread),
                SIGCHLD);

        if (a_utask->m_clone_flags & CLONE_VFORK)
        {
                thread_wakeup_interruptible_only(&a_utask->m_clone_flags);
                a_utask->m_clone_flags = 0;
        }

        thread_wakeup_interruptible_only(parent_utask);
        // thread_block(nullptr);

        // For all_uthread_map
        a_uthread->m_refcount--;

        // for (process_internal *pik = s_all_process_map.first(); pik; pik = process_map::next(pik))
        //{
        //        log_debug("Pid %d, ppid %d", pik->m_pid, pik->m_ppid);
        //}

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t signal_generate_dump(process_internal *a_utask, pthread_internal *a_uthread, int a_signo)
// ********************************************************************************************************************************
{
        a_utask->m_wstatus = __W_EXITCODE(a_utask->m_exit_code, a_signo) | __WCOREFLAG;
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t signal_deliver_dump(process_internal *a_utask, pthread_internal *a_uthread, int a_signo)
// ********************************************************************************************************************************
{
        // TODO; Generate dump here
        return signal_deliver_terminate(a_utask, a_uthread, a_signo);
}

// ********************************************************************************************************************************
kern_return_t signal_generate_ignore_thread(process_internal *a_utask, pthread_internal *a_uthread, int a_signo)
// ********************************************************************************************************************************
{
        if ((a_signo == SIGCHLD) && (((a_utask->m_user_signal_action[a_signo].flags & SA_NOCLDWAIT) != 0) || (a_utask->m_pid == 1)))
        {
                // If signum is SIGCHLD, do not transform children into zombies when they terminate.
                // This flag is meaningful only when establishing a handler for SIGCHLD, or when setting that signal's disposition
                // to SIG_DFL.
                // If the SA_NOCLDWAIT flag is set when establishing a handler for SIGCHLD, POSIX.1 leaves it
                // unspecified whether a SIGCHLD signal is generated when a child process terminates.
                // On Linux, a SIGCHLD signal is generated in this case; on some other implementations, it is not.

                KASSERT(current_thread()->pthread != nullptr); // Should not run from interrupt context
                //  log_debug("%d ignoreing zombie", a_utask->m_pid);

                for (process_internal *chld = a_utask->m_children_zombie.first(); chld;)
                {
                        process_internal *tmp = process_child_map::next(chld);
                        KASSERT(chld->m_kernel_task->active == false);
                        KASSERT(a_utask->m_children_zombie.find(chld) != nullptr);
                        a_utask->m_children_zombie.remove(chld);
                        chld->m_refcount--;
                        if (chld->m_refcount == 0)
                        {
                                task_deallocate(chld->m_kernel_task);
                                process_free(chld);
                        }
                        chld = tmp;
                }
        }

        a_uthread->m_pending_signals &= ~SIGMASK(a_signo);
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t signal_generate_ignore(process_internal *a_utask, pthread_internal *a_uthread, int a_signo)
// ********************************************************************************************************************************
{
#if 0
        if ((a_signo == SIGCHLD)
            && (a_utask->m_user_signal_action->handler
                == SIG_DFL) /*&& (a_utask->m_user_signal_action[a_signo].flags & SA_NOCLDWAIT) != 0*/)
        {
                // If signum is SIGCHLD, do not transform children into zombies when they terminate.
                // This flag is meaningful only when establishing a handler for SIGCHLD, or when setting that signal's disposition
                // to SIG_DFL.
                // If the SA_NOCLDWAIT flag is set when establishing a handler for SIGCHLD, POSIX.1 leaves it
                // unspecified whether a SIGCHLD signal is generated when a child process terminates.
                // On Linux, a SIGCHLD signal is generated in this case; on some other implementations, it is not.

                KASSERT(current_thread()->pthread != nullptr); // Should not run from interrupt context
                // log_debug("%d ignoreing zombie", a_utask->m_pid);

                for (process_internal *chld = a_utask->m_children_zombie.first(); chld;)
                {
                        process_internal *tmp = process_child_map::next(chld);
                        KASSERT(chld->m_kernel_task->active == false);
                        KASSERT(a_utask->m_children_zombie.find(chld) != nullptr);
                        a_utask->m_children_zombie.remove(chld);
                        chld->m_refcount--;
                        if (chld->m_refcount == 0)
                        {
                                task_deallocate(chld->m_kernel_task);
                                process_free(chld);
                        }
                        chld = tmp;
                }
        }
#endif

        if ((a_signo == SIGCHLD) && (a_utask->m_user_signal_action->handler == SIG_DFL))
        {
                clear_wait(a_uthread->m_kernel_thread, THREAD_AWAKENED, true);
        }

        a_utask->m_pending_signals &= ~SIGMASK(a_signo);

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t signal_deliver_ignore(process_internal *a_utask, pthread_internal *a_uthread, int a_signo)
// ********************************************************************************************************************************
{
        panic("Should never happen");
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t signal_generate_continue(process_internal *a_utask, pthread_internal *a_uthread, int a_signo)
// ********************************************************************************************************************************
{
        KASSERT(a_signo == SIGCONT);
        a_utask->m_wstatus = __W_CONTINUED;
        task_resume(a_utask->m_kernel_task);
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t signal_deliver_continue(process_internal *a_utask, pthread_internal *a_uthread, int a_signo)
// ********************************************************************************************************************************
{
        // Do nothing
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t signal_generate_stop(process_internal *a_utask, pthread_internal *a_uthread, int a_signo)
// ********************************************************************************************************************************
{
        if (a_utask->m_kernel_task->user_stop_count > 0)
        {
                a_utask->m_pending_signals &= ~SIGMASK(a_signo);
        }
        else
        {
                a_utask->m_wstatus = __W_STOPCODE(a_signo);
        }

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t signal_deliver_stop(process_internal *a_utask, pthread_internal *a_uthread, int a_signo)
// ********************************************************************************************************************************
{
        if (a_utask->m_kernel_task->user_stop_count)
        {
                return KERN_SUCCESS;
        }

        // Current thread will have a suspend AST
        KASSERT(task_suspend(a_utask->m_kernel_task) == KERN_SUCCESS);

        if (a_utask->m_ppid)
        {
                process_internal *parent_utask = s_all_process_map.find(a_utask->m_ppid);
                utask_signal(parent_utask, SIGCHLD);
                thread_wakeup(parent_utask);
        }

        // p->p_stat = SSTOP;
        // p->p_flag &= ~SWTED;
        // wakeup((caddr_t)p->p_pptr);
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t signal_generate_catch(process_internal *a_utask, pthread_internal *a_uthread, int a_signo)
// ********************************************************************************************************************************
{
        if ((a_signo == SIGCHLD) && ((a_utask->m_user_signal_action[a_signo].flags & SA_NOCLDWAIT) != 0))
        {
                // If signum is SIGCHLD, do not transform children into zombies when they terminate.
                // This flag is meaningful only when establishing a handler for SIGCHLD, or when setting that signal's disposition
                // to SIG_DFL.
                // If the SA_NOCLDWAIT flag is set when establishing a handler for SIGCHLD, POSIX.1 leaves it
                // unspecified whether a SIGCHLD signal is generated when a child process terminates.
                // On Linux, a SIGCHLD signal is generated in this case; on some other implementations, it is not.

                KASSERT(current_thread()->pthread != nullptr); // Should not run from interrupt context
                log_debug("%d ignoreing zombie", a_utask->m_pid);
        }

        // Don't interrupt the thread if the signal is blocked
        if ((a_uthread->m_blocked_signals & SIGMASK(a_signo)) == 0)
        {
                if (a_signo == SIGCONT)
                {
                        a_utask->m_wstatus = __W_CONTINUED;
                        task_resume(a_utask->m_kernel_task);
                }
                else
                {
                        clear_wait(a_uthread->m_kernel_thread, THREAD_INTERRUPTED, true);
                }
        }

        return KERN_SUCCESS;
}

typedef struct kucontext
{
        unsigned long uc_flags;
        struct __ucontext *uc_link;
        stack_t uc_stack;
        struct sigcontext uc_mcontext;
        sigset_t uc_sigmask;
} kucontext_t;

typedef struct rt_sigframe
{
        void (*restorer)(void);
        kucontext_t uc;
        siginfo_t info;
        struct _fpstate fpstate;
} *rt_sigframe_t;

// ********************************************************************************************************************************
kern_return_t signal_deliver_catch(process_internal *a_utask, pthread_internal *a_uthread, int a_signo)
// ********************************************************************************************************************************
{
        ksigaction *sa = &a_utask->m_user_signal_action[a_signo];

        // Should not be here
        {
                if ((sa->flags & SA_RESTORER) == 0)
                {
                        log_warn("X86-64 should have a restoerer");
                        return -1;
                }

                pcb_t pcbp = a_uthread->m_kernel_thread->pcb; // a_pthread->pt_kthread->pcb;

                rt_sigframe_t sigframe = nullptr;

                {
                        // Grab the user stack pointer
                        vm_address_t ursp = pcbp->iss.return_rsp;

                        // Don't clobber the redzone
                        ursp -= 128;

                        stack_t *ss = &a_uthread->m_signal_stack;

                        // Check if we should execute on alt stack
                        if ((sa->flags & SA_ONSTACK) && ((ss->ss_flags & SS_DISABLE) == 0))
                        {
                                // If we are not already executeing on the alt stack switch to it now
                                if ((ss->ss_flags & SS_ONSTACK) == 0)
                                {
                                        ursp = reinterpret_cast<vm_address_t>(ss->ss_sp);
                                        ursp += ss->ss_size;
                                        ss->ss_flags |= SS_ONSTACK;
                                }
                                // else
                                // {
                                // In some implementations, a signal (whether or not indicated to execute on the alternate stack)
                                // shall always execute on the alternate stack if it is delivered while another signal is being
                                // caught using the alternate stack.
                                // }
                        }

                        // Create room for our signal frame
                        ursp -= sizeof(struct rt_sigframe);

                        // Align the stack so the restorer is at a 8 byte alignment
                        ursp = ROUNDDOWN(ursp - 1, 16) - 8;

                        // Put our signal frame right after the aligment
                        sigframe = (rt_sigframe_t)(ursp);
                }

                // Copyout the sigframe, starting with the restorer
                int error = emerixx::uio::copyout(&sa->restorer, &sigframe->restorer);

                // Do ucontext
                {
                        error |= emerixx::uio::copyout(0UL, &sigframe->uc.uc_flags);
                        error |= emerixx::uio::copyout((struct __ucontext *)nullptr, &sigframe->uc.uc_link);
                        // do uc_stack
                        struct sigcontext sigctx = {};

                        sigctx.cr2    = pcbp->iss.cr2;
                        sigctx.rax    = pcbp->iss.rax;
                        sigctx.rbx    = pcbp->iss.rbx;
                        sigctx.rcx    = pcbp->iss.rcx;
                        sigctx.rdx    = pcbp->iss.rdx;
                        sigctx.rsi    = pcbp->iss.rsi;
                        sigctx.rdi    = pcbp->iss.rdi;
                        sigctx.rbp    = pcbp->iss.rbp;
                        sigctx.r8     = pcbp->iss.r8;
                        sigctx.r9     = pcbp->iss.r9;
                        sigctx.r10    = pcbp->iss.r10;
                        sigctx.r11    = pcbp->iss.r11;
                        sigctx.r12    = pcbp->iss.r12;
                        sigctx.r13    = pcbp->iss.r13;
                        sigctx.r14    = pcbp->iss.r14;
                        sigctx.r15    = pcbp->iss.r15;
                        sigctx.trapno = pcbp->iss.trapnum;
                        sigctx.err    = pcbp->iss.error_code;
                        sigctx.rip    = pcbp->iss.return_rip;
                        sigctx.cs     = (unsigned short)pcbp->iss.return_cs;
                        sigctx.eflags = pcbp->iss.return_rflags;
                        sigctx.rsp    = pcbp->iss.return_rsp;

                        sigctx.fpstate = &sigframe->fpstate;

                        // What do we write into sigctx.oldmask?
                        sigctx.oldmask = static_cast<unsigned long>(a_uthread->m_blocked_signals);

                        error |= emerixx::uio::copyout(&sigctx, &sigframe->uc.uc_mcontext);

                        error |= emerixx::uio::copyout(&a_uthread->m_blocked_signals, &sigframe->uc.uc_sigmask);
                }

                // Do siginfo_t
                {
                        siginfo_t info = {};

                        info.si_signo = a_signo;

                        error |= emerixx::uio::copyout(&info, &sigframe->info);
                }

                // Do fpu state
                {
                        // HACK, if userspace touches _fpstate the process is likeley dead
                        static_assert(sizeof(struct fpsave_state) == sizeof(struct _fpstate), "Its a hack");

                        // Our state is now in the saved in the pcb(if it was not there already)
                        thread_save_fpu(a_uthread->m_kernel_thread);

                        error |= emerixx::uio::copyout((struct _fpstate *)&pcbp->ifps, &sigframe->fpstate);
                }

                if (error)
                {
                        return error;
                }

                // Make to user process return to the signal handler
                pcbp->iss.return_rsp = reinterpret_cast<natural_t>(sigframe);
                pcbp->iss.return_rip = reinterpret_cast<natural_t>(sa->handler);
                pcbp->iss.rdi        = static_cast<natural_t>(a_signo);

                // Make sure we are compatible with sysret
                // pcbp->iss.rcx = pcbp->iss.return_rip; // You cant just modify userspace without knowing if how the program
                // entered

                // If the handler takes siginfo and ucontext in addidion to signum load them
                // aswell.
                if (sa->flags & SA_SIGINFO)
                {
                        pcbp->iss.rsi = reinterpret_cast<natural_t>(&sigframe->info);
                        pcbp->iss.rdx = reinterpret_cast<natural_t>(&sigframe->uc);
                        panic("SA_SIGINFO not supported");
                }
        } // Should not be here end

        a_uthread->m_blocked_signals = sa->mask; // Should this be process wide or just the executing thread?

        if (sa->flags & SA_NODEFER) // Some syscalls do not accept nodefer
        {
                a_uthread->m_blocked_signals &= ~SIGMASK(a_signo);
        }
        else
        {
                a_uthread->m_blocked_signals |= SIGMASK(a_signo);
        }

        // Check if need to restore the signal action to default
        if (sa->flags & SA_RESETHAND)
        {
                sa->handler  = SIG_DFL;
                sa->restorer = nullptr;
                sa->mask     = SIGMASK(a_signo);
                sa->flags    = 0;
                if (a_signo == SIGCHLD)
                {
                        // sa->flags |= SA_NOCLDWAIT;
                }
                a_utask->m_kernel_signal_action[a_signo] = s_default_sigactions[a_signo];
        }

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t utask_dup(process_t *a_new_task_out, pthread_t *a_new_thread_out)
// ********************************************************************************************************************************
{
        GLOBALS_WRITE_SCOPE_LOCK(splhigh);

        pthread_internal *puthrd = static_cast<pthread_internal *>(current_thread()->pthread);
        LOCK_WRITE_SCOPE(&puthrd->m_lock);

        process_internal *putask = s_curproc; //  static_cast<process_internal *>(current_task()->utask);
        LOCK_WRITE_SCOPE(&putask->m_lock);

        utask_group_internal *putask_grp = static_cast<utask_group_internal *>(s_all_utask_group_map.find(putask->m_pgid));
        KASSERT(putask_grp != nullptr);
        LOCK_WRITE_SCOPE(&putask_grp->m_lock);

        process_internal *utask = process_alloc(pid_find_free());
        pthread_internal *uthrd = pthread_alloc(utask->m_pid);

        kern_return_t retval = task_create(putask->m_kernel_task, TASK_CREATE_SHARE_VMMAP, &utask->m_kernel_task);

        if (retval != KERN_SUCCESS)
        {
                utask->m_refcount--;
                process_free(utask);
                return KERN_FAIL(EAGAIN);
        }
        *PER_TASK_PTR_EX(utask->m_kernel_task, __s_curproc) = utask;
        utask->m_ppid                                       = putask->m_pid;
        utask->m_pgid                                       = putask->m_pgid;
        utask->m_ctty                                       = putask->m_ctty;
        putask_grp->m_members.insert(utask);
        utask->m_sid = putask->m_sid;

        putask->m_children.insert(utask);

        retval = thread_create(utask->m_kernel_task, &uthrd->m_kernel_thread);
        KASSERT(retval == KERN_SUCCESS);

        tls_dup(uthrd->m_kernel_thread);

        uthrd->m_kernel_thread->pthread = uthrd;
        uthrd->m_refcount += 1;
        uthrd->m_pid             = utask->m_pid;
        uthrd->m_csignal         = puthrd->m_csignal;
        uthrd->m_tid_address     = puthrd->m_tid_address;
        uthrd->m_signal_stack    = puthrd->m_signal_stack;
        uthrd->m_blocked_signals = puthrd->m_blocked_signals;

        fd_context_reference(uthrd->m_fdctx = puthrd->m_fdctx);
        fs_context_reference(uthrd->m_fsctx = puthrd->m_fsctx);

        thread_dup(puthrd->m_kernel_thread, uthrd->m_kernel_thread, nullptr, nullptr, nullptr);
        thread_start(uthrd->m_kernel_thread, locore_exception_return);

        for (int i = 0; i < NSIG; ++i)
        {
                utask->m_user_signal_action[i]   = putask->m_user_signal_action[i];
                utask->m_kernel_signal_action[i] = putask->m_kernel_signal_action[i];
        }

        // A child created via fork does not inherit its parent's interval timers.
        for (int i = 0; i <= ITIMER_PROF; ++i)
        {
                utask->m_itimer_elt[i].chain = {};
                utask->m_itimer_elt[i].fcn   = putask->m_itimer_elt[i].fcn;
                utask->m_itimer_elt[i].param = utask;
                utask->m_itimer_elt[i].set   = TELT_UNSET;
                utask->m_itimer_elt[i].ticks = 0;
                utask->m_itimer[i]           = {};
        }

        thread_resume(uthrd->m_kernel_thread); // Should not be here

        *a_new_task_out   = utask;
        *a_new_thread_out = uthrd;

        return retval;
}

// ********************************************************************************************************************************
kern_return_t uthread_dup(pthread_t *a_new_thread_out)
// ********************************************************************************************************************************
{
        panic("IMP");
        return 0;
}

// ********************************************************************************************************************************
process_t utask_current()
// ********************************************************************************************************************************
{
        return s_curproc;
}

// ********************************************************************************************************************************
kern_return_t uthread_signal(pthread_t a_pthread, int a_signo)
// ********************************************************************************************************************************
{
        spl_t s = splhigh();
        defer(splx(s));

        pthread_internal *uthrd = static_cast<pthread_internal *>(a_pthread);
        process_internal *utask = *PER_TASK_PTR_EX(uthrd->m_kernel_thread->task, __s_curproc);

        LOCK_WRITE_SCOPE(&uthrd->m_lock);
        LOCK_WRITE_SCOPE(&utask->m_lock);

        kern_return_t retval;

        if (uthrd->m_pending_signals & SIGMASK(a_signo))
        {
                retval = KERN_SUCCESS;
        }
        else
        {
                uthrd->m_pending_signals |= SIGMASK(a_signo);
                retval = s_sig_uthread_generation_actions[utask->m_kernel_signal_action[a_signo]](utask, uthrd, a_signo);
        }

        return retval;
}

// ********************************************************************************************************************************
utask_group *utask_group_find(pid_t a_pgid)
// ********************************************************************************************************************************
{
        GLOBALS_READ_SCOPE_LOCK(splhigh);
        return s_all_utask_group_map.find(a_pgid);
}

// ********************************************************************************************************************************
kern_return_t utask_wait_group_child(pid_t a_pgid, int *a_wstat_out, int a_options, struct rusage *a_ru_out)
// ********************************************************************************************************************************
{
        panic("IMP");
        return 0;
}

// ********************************************************************************************************************************
kern_return_t utask_wait_child(pid_t a_child_pid, int *a_wstat_out, int a_options, struct rusage *a_ru_out)
// ********************************************************************************************************************************
{
        kern_return_t retval = KERN_SUCCESS;

        process_internal *utask_parent = s_curproc;

        process_internal *utask_child = nullptr;
        do
        {
                LOCK_WRITE_SCOPE(&utask_parent->m_lock);

                utask_child = utask_parent->m_children_zombie.find(a_child_pid);

                if (utask_child != nullptr)
                {
                        utask_parent->m_children_zombie.remove(utask_child);
                        KASSERT(utask_child->m_refcount == 1); // Should only be refrenced by the parent at this point
                        KASSERT(utask_child->m_kernel_task->active == false);
                        KASSERT(utask_child->m_kernel_task->thread_count == 0);

                        if (a_ru_out != nullptr)
                        {
                                task_basic_info tinfo;
                                natural_t tinfo_count = TASK_BASIC_INFO_COUNT;
                                task_info(utask_child->m_kernel_task,
                                          TASK_BASIC_INFO,
                                          reinterpret_cast<task_info_t>(&tinfo),
                                          &tinfo_count);
                                KASSERT(tinfo_count == TASK_BASIC_INFO_COUNT);

                                a_ru_out->ru_utime.tv_sec  = tinfo.user_time.seconds;
                                a_ru_out->ru_utime.tv_usec = tinfo.user_time.microseconds;

                                a_ru_out->ru_stime.tv_sec  = tinfo.system_time.seconds;
                                a_ru_out->ru_stime.tv_usec = tinfo.system_time.microseconds;
                        }

                        // Don't need the task anymore
                        task_deallocate(utask_child->m_kernel_task);

                        retval = utask_child->m_pid;

                        if (a_wstat_out != nullptr)
                        {
                                *a_wstat_out = utask_child->m_wstatus;
                        }
                        utask_child->m_refcount--;
                        process_free(utask_child);
                        break;
                }

                if ((utask_parent->m_children.first() == nullptr) && ((a_options & WNOHANG) == 0))
                {
                        // We dont have any children
                        return KERN_FAIL(ECHILD);
                }

                // No zombie tasks, check wstatus
                if (a_child_pid == 0)
                {
                        // Return the wstatus of any waitable child
                        for (utask_child = utask_parent->m_children.first(); //
                             utask_child != nullptr;
                             utask_child = process_child_map::next(utask_child))
                        {

                                if (((a_options & WUNTRACED) && WIFSTOPPED(utask_child->m_wstatus)) || //
                                    ((a_options & WCONTINUED) && WIFCONTINUED(utask_child->m_wstatus)))
                                {
                                        break;
                                }
                        }

                        if (utask_child != nullptr)
                        {
                                // Child with changed status
                                LOCK_WRITE_SCOPE(&utask_child->m_lock); // Not atomic
                                retval = utask_child->m_pid;
                                if (a_wstat_out != nullptr)
                                {
                                        *a_wstat_out = utask_child->m_wstatus;
                                }
                                utask_child->m_wstatus = 0;
                                break;
                        }
                }
                else
                {
                        // Return the wstatus of a specific child
                        utask_child = utask_parent->m_children.find(a_child_pid);

                        if (utask_child == nullptr)
                        {
                                // The child does not exist
                                retval = KERN_FAIL(ECHILD);
                                break;
                        }

                        // When the child we are waiting for exits it locks itself and then this task(in order to insert itself as
                        // zombie) we lock ourself in order to find the child we are looking for, and then we lock the child Deadly
                        // embrace / deadlock
                        if (lock_try_write(&utask_child->m_lock) == false)
                        {
                                lock_write_done(&utask_parent->m_lock);
                                uthread_yield();
                                lock_write(&utask_parent->m_lock);
                                continue;
                        }
                        defer(lock_write_done(&utask_child->m_lock));

                        if (((a_options & WUNTRACED) && WIFSTOPPED(utask_child->m_wstatus)) || //
                            ((a_options & WCONTINUED) && WIFCONTINUED(utask_child->m_wstatus)))
                        {
                                retval = utask_child->m_pid;
                                if (a_wstat_out != nullptr)
                                {
                                        *a_wstat_out = utask_child->m_wstatus;
                                }
                                utask_child->m_wstatus = 0;
                                break;
                        }
                }

                if (a_options & WNOHANG)
                {
                        // No children with changed status
                        if (s_curproc != utask_parent)
                        {
                                retval = s_curproc->m_pid;
                        }
                        else
                        {
                                retval = KERN_FAIL(ECHILD);
                        }

                        break;
                }

                lock_write_done(&utask_parent->m_lock);
                retval = uthread_sleep(utask_parent);
                lock_write(&utask_parent->m_lock);
        }
        while (KERN_STATUS_SUCCESS(retval));

        return retval;
}

// ********************************************************************************************************************************
kern_return_t utask_group_signal(utask_group *a_utask_group, int a_signo)
// ********************************************************************************************************************************
{
        GLOBALS_READ_SCOPE_LOCK(splhigh);
        utask_group_internal *utskgrp = static_cast<utask_group_internal *>(a_utask_group);

        LOCK_READ_SCOPE(&utskgrp->m_lock);

        sigset_t signo_mask = SIGMASK(a_signo);

        kern_return_t retval = KERN_SUCCESS;
        for (process_internal *utask = utskgrp->m_members.first(); //
             utask != nullptr;                                     //
             utask = process_group_map::next(utask))
        {
                pthread_internal *uthrd;
                if (utask->m_kernel_task == current_task())
                {
                        uthrd = static_cast<pthread_internal *>(current_thread()->pthread);
                }
                else
                {
                        uthrd = static_cast<pthread_internal *>(((thread_t)(utask->m_kernel_task->thread_list.next))->pthread);
                }

                if (utask->m_pending_signals & signo_mask)
                {
                        retval = KERN_SUCCESS;
                }
                else
                {
                        utask->m_pending_signals |= signo_mask;
                        retval = s_sig_utask_generation_actions[utask->m_kernel_signal_action[a_signo]](utask, uthrd, a_signo);
                }

                if (KERN_STATUS_FAILURE(retval))
                {
                        break;
                }
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t utask_find(pid_t a_pid, process_t *a_new_task_out)
// ********************************************************************************************************************************
{
        GLOBALS_READ_SCOPE_LOCK(splhigh);

        process_t utask = s_all_process_map.find(a_pid);

        kern_return_t retval;
        if (utask == nullptr)
        {
                retval = KERN_FAIL(ESRCH);
        }
        else
        {
                if (static_cast<process_internal *>(utask)->m_kernel_task->thread_count == 0)
                {
                        retval = KERN_FAIL(ESRCH); // No threads, A.K.A a zombie, needs to be waited for
                }
                else
                {

                        retval          = KERN_SUCCESS;
                        *a_new_task_out = utask;
                }
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t utask_find_child(process_t a_parent_utask, pid_t a_pid, process_t *a_new_task_out)
// ********************************************************************************************************************************
{
        if (a_pid < 2)
        {
                return KERN_FAIL(ESRCH);
        }

        spl_t s = splhigh();
        defer(splx(s));

        process_internal *parent_utask = static_cast<process_internal *>(a_parent_utask);
        LOCK_READ_SCOPE(&parent_utask->m_lock);
        process_internal *child_utask = parent_utask->m_children.find(a_pid);

        if (child_utask == nullptr)
        {
                return KERN_FAIL(ESRCH);
        }

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
int sigismember(sigset_t const *a_set, int a_signo)
// ********************************************************************************************************************************
{
        sigset_t sset = *a_set & SIGMASK(a_signo);

        return (sset) ? 0 : -1;
}

// ********************************************************************************************************************************
kern_return_t utask_set_group_id(process_t a_utask, pid_t a_new_pgid)
// ********************************************************************************************************************************
{
        process_internal *utask = static_cast<process_internal *>(a_utask);
        LOCK_WRITE_SCOPE(&utask->m_lock);

        KASSERT(utask->m_pgid != a_new_pgid);

        utask_group_internal *utask_grp;
        {
                GLOBALS_READ_SCOPE_LOCK(splhigh);
                {
                        utask_grp = s_all_utask_group_map.find(utask);
                        KASSERT(utask_grp != nullptr);
                        lock_write(&utask_grp->m_lock);
                        KASSERT(utask_grp->m_members.find(utask));
                        utask_grp->m_members.remove(utask);

                        if (utask_grp->m_members.empty() && (--(utask_grp->m_refcount)) == 0)
                        {
                                lock_write_done(&utask_grp->m_lock);
                                utask_group_free(utask_grp);
                        }
                        else
                        {
                                lock_write_done(&utask_grp->m_lock);
                        }
                }

                utask_grp = s_all_utask_group_map.find(a_new_pgid);
        }

        if (utask_grp == nullptr)
        {
                GLOBALS_WRITE_SCOPE_LOCK(splhigh);
                utask_grp        = utask_group_alloc(a_new_pgid);
                utask_grp->m_sid = utask->m_sid;
                utask_grp->m_members.insert(utask);
        }
        else
        {
                LOCK_WRITE_SCOPE(&utask_grp->m_lock);
                KASSERT(utask_grp->m_sid == utask->m_sid);
                utask_grp->m_members.insert(utask);
        }

        utask->m_pgid = a_new_pgid;

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t utask_replace_vm_context(process_t a_utask, pthread_t a_uthread, vm_map_t a_vmctx, int a_activate)
// ********************************************************************************************************************************
{
        KASSERT(a_vmctx != nullptr);

        process_internal *utask = static_cast<process_internal *>(a_utask);
        pthread_internal *uthrd = static_cast<pthread_internal *>(a_uthread);

        vm_map_t oldmap = utask->m_kernel_task->map;

        utask->m_kernel_task->map = a_vmctx;
        vm_map_reference(a_vmctx);

        if (a_activate)
        {
                spl_t s = splhigh();
                PMAP_DEACTIVATE_USER(vm_map_pmap(oldmap), uthrd->m_kernel_thread, cpu_number());
                PMAP_ACTIVATE_USER(vm_map_pmap(a_vmctx), uthrd->m_kernel_thread, cpu_number());
                splx(s);
        }

        vm_map_deallocate(oldmap);
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
void utask_release_parent(process_t a_utask)
// ********************************************************************************************************************************
{
        GLOBALS_READ_SCOPE_LOCK(splhigh);

        process_internal *utask = s_all_process_map.find(a_utask->m_ppid);
        a_utask->m_clone_flags  = 0;
        uthread_wakeup(&a_utask->m_clone_flags);
}

// ********************************************************************************************************************************
kern_return_t utask_signal(process_t a_utask, int a_signo)
// ********************************************************************************************************************************
{
        spl_t s = splhigh();
        defer(splx(s));

        process_internal *utask = static_cast<process_internal *>(a_utask);

        pthread_internal *uthrd;
        if (utask->m_kernel_task == current_task())
        {
                uthrd = static_cast<pthread_internal *>(current_thread()->pthread);
        }
        else
        {
                uthrd = static_cast<pthread_internal *>(((thread_t)(utask->m_kernel_task->thread_list.next))->pthread);
        }

        LOCK_WRITE_SCOPE(&uthrd->m_lock);
        LOCK_WRITE_SCOPE(&utask->m_lock);

        kern_return_t retval;

        if (utask->m_pending_signals & SIGMASK(a_signo))
        {
                retval = KERN_SUCCESS;
        }
        else
        {
                utask->m_pending_signals |= SIGMASK(a_signo);
                retval = s_sig_utask_generation_actions[utask->m_kernel_signal_action[a_signo]](utask, uthrd, a_signo);
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t process_exit(int a_exit_code)
// ********************************************************************************************************************************
{
        spl_t s = splhigh();
        defer(splx(s));

        process_internal *utask = s_curproc;
        pthread_internal *uthrd = static_cast<pthread_internal *>(((thread_t)(utask->m_kernel_task->thread_list.next))->pthread);

        LOCK_WRITE_SCOPE(&uthrd->m_lock);
        LOCK_WRITE_SCOPE(&utask->m_lock);

        utask->m_exit_code = a_exit_code;
        utask->m_pending_signals |= SIGMASK(SIGKILL);
        utask->m_kernel_signal_action[SIGKILL] = SIGNAL_TERMINATE;

        return s_sig_utask_generation_actions[utask->m_kernel_signal_action[0]](utask, uthrd, 0);
}

// ********************************************************************************************************************************
pthread_t uthread_self()
// ********************************************************************************************************************************
{
        return current_thread()->pthread;
}

// ********************************************************************************************************************************
kern_return_t uthread_find(pid_t a_tid, pthread_t *a_uthread_out)
// ********************************************************************************************************************************
{
        GLOBALS_READ_SCOPE_LOCK(splhigh);

        pthread_internal *uthrd = s_all_pthread_map.find(a_tid);

        kern_return_t retval;
        if (uthrd == nullptr)
        {
                retval = KERN_FAIL(ESRCH);
        }
        else
        {
                retval         = KERN_SUCCESS;
                *a_uthread_out = uthrd;
        }

        return retval;
}

pthread_t uthread_getlast()
{
        pthread_t uthread = nullptr;
        uthread_find(s_last_pid, &uthread);
        return uthread;
}

// ********************************************************************************************************************************
process_t utask_self()
// ********************************************************************************************************************************
{
        return utask_current();
}

// ********************************************************************************************************************************
int uthread_pending_signal(pthread_t a_pthread)
// ********************************************************************************************************************************
{
        pthread_internal *uthrd = static_cast<pthread_internal *>(a_pthread);
        // LOCK_READ_SCOPE(&uthrd->m_lock);

        process_internal *utask = *PER_TASK_PTR_EX(uthrd->m_kernel_thread->task, __s_curproc);
        // LOCK_READ_SCOPE(&utask->m_lock);

        int sig = (uthrd->m_pending_signals | utask->m_pending_signals) & ~(uthrd->m_blocked_signals);

        if (sig != 0)
        {
                sig = __builtin_ffsl(sig);
        }

        return sig;
}

// ********************************************************************************************************************************
kern_return_t process_thread_fork(process_t a_process, pthread_t a_old_thread, pthread_t *a_new_thread_out)
// ********************************************************************************************************************************
{
        panic("IMP");
        return 0;
}

// ********************************************************************************************************************************
kern_return_t vm_context_dup(process_t a_process)
// ********************************************************************************************************************************
{
        process_internal *utask = static_cast<process_internal *>(a_process);
        LOCK_READ_SCOPE(&utask->m_lock);

        vm_map_t oldmap = utask->m_kernel_task->map;

        utask->m_kernel_task->map = vm_map_fork(oldmap);

        vm_map_deallocate(oldmap);

        return 0;
}

// ********************************************************************************************************************************
kern_return_t uthread_signalstack(pthread_t a_thread, const stack_t *a_ss, stack_t *a_oss)
// ********************************************************************************************************************************
{
        pthread_internal *uthrd = static_cast<pthread_internal *>(a_thread);

        LOCK_WRITE_SCOPE(&uthrd->m_lock);

        if (a_oss != nullptr)
        {
                *a_oss = uthrd->m_signal_stack;
        }

        if (a_ss != nullptr)
        {
                uthrd->m_signal_stack = *a_ss;
        }

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t uthread_exit(pthread_t a_uthread, int a_exit_code)
// ********************************************************************************************************************************
{
        panic("IMP");
        return 0;
}

// ********************************************************************************************************************************
kern_return_t uthread_get_state(pthread_t a_pthread, enum THREAD_STATE_FLAVOR a_flavor, thread_state_t a_tstate, natural_t *count)
// ********************************************************************************************************************************
{
        pthread_internal *uthrd = static_cast<pthread_internal *>(a_pthread);
        LOCK_WRITE_SCOPE(&uthrd->m_lock);

        kern_return_t retval = thread_get_state(uthrd->m_kernel_thread, a_flavor, a_tstate, count);

        if (retval != KERN_SUCCESS)
        {
                return KERN_FAIL(EINVAL);
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t uthread_set_state(pthread_t a_pthread, enum THREAD_STATE_FLAVOR a_flavor, thread_state_t a_tstate, natural_t count)
// ********************************************************************************************************************************
{
        pthread_internal *uthrd = static_cast<pthread_internal *>(a_pthread);
        LOCK_WRITE_SCOPE(&uthrd->m_lock);

        kern_return_t retval = thread_set_state(uthrd->m_kernel_thread, a_flavor, a_tstate, count);

        if (retval != KERN_SUCCESS)
        {
                return KERN_FAIL(EINVAL);
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t uthread_sigprocmask(pthread_t a_thread, int a_how, sigset_t const *a_nset, sigset_t *a_oset)
// ********************************************************************************************************************************
{
        pthread_internal *uthrd = static_cast<pthread_internal *>(a_thread);

        LOCK_WRITE_SCOPE(&uthrd->m_lock);

        if (a_oset != nullptr)
        {
                *a_oset = uthrd->m_blocked_signals;
        }

        if (a_nset)
        {
                switch (a_how)
                {
                        case SIG_BLOCK:
                                uthrd->m_blocked_signals |= (*a_nset);
                                break;

                        case SIG_SETMASK:
                                uthrd->m_blocked_signals = (*a_nset);
                                break;

                        case SIG_UNBLOCK:
                                uthrd->m_blocked_signals &= ~(*a_nset);
                                break;
                        default:
                                return KERN_FAIL(EINVAL);
                }

                uthrd->m_blocked_signals = SIGMASK_CLEAN(uthrd->m_blocked_signals);
        }

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t uthread_sigaction(int a_signum, ksigaction_t a_actp, ksigaction_t a_oactp)
// ********************************************************************************************************************************
{
        KASSERT(a_signum < (sizeof(s_default_sigactions) / sizeof(s_default_sigactions[0])));

        pthread_internal *uthrd = static_cast<pthread_internal *>(current_thread()->pthread);

        LOCK_WRITE_SCOPE(&uthrd->m_lock);

        process_internal *utask = s_curproc;

        LOCK_WRITE_SCOPE(&utask->m_lock);

        ksigaction *ksa = &(utask->m_user_signal_action[a_signum]);

        if (a_oactp != nullptr)
        {
                *a_oactp = *ksa;
        }

        if (a_actp != nullptr)
        {
                *ksa = *a_actp;

                ksa->mask = SIGMASK_CLEAN(ksa->mask);

                switch (reinterpret_cast<integer_t>(ksa->handler))
                {
                        case /*SIG_DFL*/ 0:
                                utask->m_kernel_signal_action[a_signum] = s_default_sigactions[a_signum];
                                // Make sure we reap children
                                if (a_signum == SIGCHLD)
                                {
                                        // ksa->flags |= SA_NOCLDWAIT;
                                }

                                /* code */
                                break;

                        case /*SIG_IGN*/ 1:
                                utask->m_kernel_signal_action[a_signum] = SIGNAL_IGNORE;

                                // Make sure we reap children
                                if (a_signum == SIGCHLD)
                                {
                                        // ksa->flags |= SA_NOCLDWAIT;
                                }

                                break;

                        default:
                                utask->m_kernel_signal_action[a_signum] = SIGNAL_CATCH;
                                break;
                }
        }

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t uthread_sigsuspend(sigset_t *a_sigset)
// ********************************************************************************************************************************
{
        spl_t s = splhigh();

        pthread_internal *uthrd = static_cast<pthread_internal *>(current_thread()->pthread);
        lock_write(&uthrd->m_lock);

        sigset_t oss = uthrd->m_blocked_signals;

        uthrd->m_blocked_signals = SIGMASK_CLEAN(*a_sigset);

        lock_write_done(&uthrd->m_lock);
        splx(s);

        kern_return_t retval = uthread_sleep(&oss);

        s = splhigh();
        lock_write(&uthrd->m_lock);
        uthrd->m_blocked_signals = oss;
        lock_write_done(&uthrd->m_lock);
        splx(s);

        return retval;
}

// ********************************************************************************************************************************
kern_return_t uthread_sigpending(sigset_t *a_sigset)
// ********************************************************************************************************************************
{
        pthread_internal *uthrd = static_cast<pthread_internal *>(current_thread()->pthread);
        lock_write(&uthrd->m_lock);
        *a_sigset = uthrd->m_pending_signals & uthrd->m_blocked_signals;
        lock_write_done(&uthrd->m_lock);

        return KERN_SUCCESS;
}
// ********************************************************************************************************************************
void uthread_yield()
// ********************************************************************************************************************************
{
        thread_block(nullptr);
}

// ********************************************************************************************************************************
kern_return_t utask_exec_done(process_t a_utask, pthread_t a_uthrd, vm_address_t a_regs[2], kern_return_t a_exec_result)
// ********************************************************************************************************************************
{
        pthread_internal *uthrd = static_cast<pthread_internal *>(a_uthrd);
        process_internal *utask = static_cast<process_internal *>(a_utask);

        KASSERT(lock_try_write(&uthrd->m_lock) == false); // MUST BE LOCKED, done by utask_exec_start
        KASSERT(lock_try_write(&utask->m_lock) == false); // MUST BE LOCKED
        KASSERT(a_utask->m_pid == a_uthrd->m_tid);

        if (KERN_STATUS_FAILURE(a_exec_result))
        {
                panic("Kill the process and notify parent");
                return KERN_SUCCESS;
        }

        // A child created via fork(2) inherits a copy of its parent's signal mask;
        // the signal mask is preserved across execve(2).

        // A child created via fork(2) inherits a copy of its parent's signal dispositions.
        // During an exec, the dispositions of handled signals are reset to the default;
        // The dispositions of ignored signals are left unchanged.
        for (int i = 1; i < NSIG; ++i)
        {
                if (utask->m_kernel_signal_action[i] == SIGNAL_CATCH)
                {
                        utask->m_user_signal_action[i]
                                = { .handler = SIG_DFL, .flags = 0, .restorer = nullptr, .mask = SIGMASK_CLEAN(SIGMASK(i)) };
                        utask->m_kernel_signal_action[i] = s_default_sigactions[i];

                        if (i == SIGCHLD)
                        {
                                // utask->m_user_signal_action[i].flags |= SA_NOCLDWAIT;
                        }
                }
        }

        // A successful call to exec removes any existing alternate signal stack.
        uthrd->m_signal_stack = { .ss_sp = nullptr, .ss_flags = SS_DISABLE, .ss_size = 0 };

        KASSERT(fd_context_exec(uthrd->m_fdctx) == KERN_SUCCESS);

        uthrd->m_tid_address = 0;

#if 0
        if (procp->process->p_flag & P_PPWAIT)
        {
                // Wipe out the new stack from parent address space
                vm_deallocate(procp->process->p_pptr->thread.kthread->task->map, new_stack_top, USER_STACK_SIZE);

                // Notify parent that we're are gone.
                // procp->p_pptr->signal(SIGCHLD);
                procp->process->p_flag &= ~P_PPWAIT;
                wakeup(procp->process->p_pptr);

                thread_start(procp->kthread, locore_exception_return);
                thread_resume(procp->kthread);
        }
#endif

        thread_clear_pcb(uthrd->m_kernel_thread);
        uthrd->m_kernel_thread->pcb->iss.return_rsp = a_regs[1];
        uthrd->m_kernel_thread->pcb->iss.return_rip = a_regs[0];

        // thread_state_regs_t thrd_state      = {};
        // thrd_state[THREAD_STATE_GP_REGS_PC] = a_regs[0];
        // thrd_state[THREAD_STATE_GP_REGS_SP] = a_regs[1];

        // thread_set_state(uthrd->m_kernel_thread, THREAD_STATE_FLAVOR_GP_REGS, thrd_state, sizeof(thrd_state));
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t utask_suspend(process_t a_utask)
// ********************************************************************************************************************************
{
        process_internal *utask = static_cast<process_internal *>(a_utask);
        KASSERT(lock_try_write(&utask->m_lock) == false); // MUST BE LOCKED

        task_suspend(utask->m_kernel_task);

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t utask_resume(process_t a_utask)
// ********************************************************************************************************************************
{
        process_internal *utask = static_cast<process_internal *>(a_utask);
        KASSERT(lock_try_write(&utask->m_lock) == false); // MUST BE LOCKED

        task_resume(utask->m_kernel_task);

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static void telt_to_timespec(timer_elt_t const a_telt, timespec *a_timespec)
// ********************************************************************************************************************************
{
        timespec_from_msec(a_timespec, (a_telt->ticks) * (1000 / hz));
}

// ********************************************************************************************************************************
static void timespec_to_telt(timespec const *a_timespec, timer_elt_t a_telt)
// ********************************************************************************************************************************
{
        a_telt->ticks = timespec_to_msec(a_timespec) / (1000 / hz);
}

// ********************************************************************************************************************************
kern_return_t timer_real_timeout(void *a_utask)
// ********************************************************************************************************************************
{
        process_internal *utask = static_cast<process_internal *>(a_utask);

        utask_signal(utask, SIGALRM);

        if (timespec_is_zero(&(utask->m_itimer[ITIMER_REAL].it_interval)) == false)
        {
                timespec_to_telt(&(utask->m_itimer[ITIMER_REAL].it_interval), &utask->m_itimer_elt[ITIMER_REAL]);
                set_timeout(&utask->m_itimer_elt[ITIMER_REAL], utask->m_itimer_elt[ITIMER_REAL].ticks);
        }

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t timer_virtual_timeout(void *a_proc)
// ********************************************************************************************************************************
{
        pthread_t p = static_cast<pthread_t>(a_proc);

        panic("Should not fire");

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t timer_prof_timeout(void *a_proc)
// ********************************************************************************************************************************
{
        pthread_t p = static_cast<pthread_t>(a_proc);

        panic("Should not fire");

        return KERN_SUCCESS;
}

// case ITIMER_REAL:
// Decrements in real time.
// A SIGALRM signal is delivered when this timer expires.

// case ITIMER_VIRTUAL:
// Decrements in process virtual time.
// It runs only when the process is executing.
// A SIGVTALRM signal is delivered when it expires.

// case ITIMER_PROF:
// Decrements both in process virtual time and when the system is running on behalf of the process.
// It is designed to be used by interpreters in statistically profiling the execution of interpreted programs.
// Each time the ITIMER_PROF timer expires, the SIGPROF signal is delivered.

// ********************************************************************************************************************************
kern_return_t utask_setitimer(int a_which, struct itimerspec *a_timerspec_in, struct itimerspec *a_timerspec_out)
// ********************************************************************************************************************************
{
        KASSERT(a_which == ITIMER_REAL);
        spl_t s = splhigh();

        pthread_internal *uthrd = static_cast<pthread_internal *>(current_thread()->pthread);
        lock_write(&uthrd->m_lock);

        process_internal *utask = s_curproc;
        lock_write(&utask->m_lock);

        if (a_timerspec_out != nullptr)
        {
                *a_timerspec_out = utask->m_itimer[a_which];
        }

        if (utask->m_itimer_elt[a_which].set == TELT_SET)
        {
                reset_timeout(&utask->m_itimer_elt[a_which]);
        }

        //  if it_value is 0 we shall disable the timer, regardless of the value of it_interval.
        if (timespec_is_zero(&(a_timerspec_in->it_value)) == true)
        {
                utask->m_itimer_elt[a_which] = {};
        }
        else
        {
                timespec_to_telt(&(a_timerspec_in->it_value), &utask->m_itimer_elt[a_which]);
                set_timeout(&utask->m_itimer_elt[a_which], utask->m_itimer_elt[a_which].ticks);
                utask->m_itimer[a_which] = *a_timerspec_in;
        }

        lock_write_done(&utask->m_lock);
        lock_write_done(&uthrd->m_lock);
        splx(s);

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
void utask_getitimer(int a_which, struct itimerspec *a_timerspec_out)
// ********************************************************************************************************************************
{
        spl_t s = splhigh();

        pthread_internal *uthrd = static_cast<pthread_internal *>(current_thread()->pthread);
        lock_write(&uthrd->m_lock);

        process_internal *utask = s_curproc;
        lock_write(&utask->m_lock);

        *a_timerspec_out = utask->m_itimer[a_which];

        lock_write_done(&utask->m_lock);
        lock_write_done(&uthrd->m_lock);
        splx(s);
}

// ********************************************************************************************************************************
pthread_t uthread_self_ex(process_t *a_utask_out)
// ********************************************************************************************************************************
{
        pthread_internal *uthrd = static_cast<pthread_internal *>(current_thread()->pthread);
        lock_write(&uthrd->m_lock);

        process_internal *utask = s_curproc;
        lock_write(&utask->m_lock);

        *a_utask_out = utask;

        return uthrd;
}

// ********************************************************************************************************************************
void uthread_put_ex(pthread_t a_uthrd, process_t a_utask)
// ********************************************************************************************************************************
{
        pthread_internal *uthrd = static_cast<pthread_internal *>(a_uthrd);
        process_internal *utask = static_cast<process_internal *>(a_utask);

        KASSERT(lock_try_write(&uthrd->m_lock) == false); // MUST BE LOCKED
        KASSERT(lock_try_write(&utask->m_lock) == false); // MUST BE LOCKED

        lock_write_done(&utask->m_lock);
        lock_write_done(&uthrd->m_lock);
}

// ********************************************************************************************************************************
void uthread_system_exit()
// ********************************************************************************************************************************
{
        spl_t s = splhigh();
        defer(splx(s));
        pthread_internal *uthrd = static_cast<pthread_internal *>(current_thread()->pthread);
        process_internal *utask = s_curproc;
        {
                LOCK_WRITE_SCOPE(&uthrd->m_lock);

                KASSERT(uthrd->m_kernel_thread != nullptr);

                LOCK_WRITE_SCOPE(&utask->m_lock);

                // Keep them alive in order to finish our work here.
                uthrd->m_refcount++;
                utask->m_refcount++;

                sigset_t sig = (uthrd->m_pending_signals | utask->m_pending_signals) & ~(uthrd->m_blocked_signals);

                if (sig != 0)
                {
                        sig = __builtin_ffsl(sig);

                        utask->m_pending_signals &= ~SIGMASK(sig);

                        uthrd->m_pending_signals &= ~SIGMASK(sig);

                        s_sig_delivery_actions[utask->m_kernel_signal_action[sig]](utask, uthrd, static_cast<int>(sig));
                }
        }

        lock_write(&uthrd->m_lock);
        if (--(uthrd->m_refcount) == 0)
        {
                // We hold the only ref
                lock_write_done(&uthrd->m_lock);
                pthread_free(uthrd);
        }
        else
        {
                lock_write_done(&uthrd->m_lock);
        }

        lock_write(&utask->m_lock);
        if (--(utask->m_refcount) == 0)
        {
                // We hold the only ref
                lock_write_done(&utask->m_lock);
                task_deallocate(utask->m_kernel_task);
                process_free(utask);
        }
        else
        {
                lock_write_done(&utask->m_lock);
        }
}

// subr for unistd
pid_t getpid()
{
        return s_curproc->m_pid;
}

// subr for unistd
pid_t getsid(int)
{
        return s_curproc->m_sid;
}

// subr for unistd
pid_t getpgid(int)
{
        return s_curproc->m_pgid;
}

int killpg(pid_t a_grp, int a_sig)
{
        utask_group *utask_grp = utask_group_find(a_grp);
        KASSERT(utask_grp != nullptr);
        utask_group_signal(utask_grp, a_sig);
        return KERN_SUCCESS;
}

int raise(int a_sig)
{
        utask_signal(s_curproc, a_sig);
        return KERN_SUCCESS;
}

dev_t getctty()
{
        return s_curproc->m_ctty;
}

void setctty(dev_t a_ctty)
{
        s_curproc->m_ctty = a_ctty;
}

static kern_return_t pid1init()
{
        tls_new(current_thread());

        s_all_process_map.init();
        s_all_pthread_map.init();
        s_all_utask_group_map.init();
        lock_init(&s_globals_lock, true);
        s_nprocs         = 0;
        s_nthreads       = 0;
        s_n_utask_groups = 0;
        s_last_pid       = 0;

        process_internal *init_proc = process_alloc(pid_find_free());
        init_proc->m_kernel_task    = current_task();
        *PER_TASK_PTR(__s_curproc)  = init_proc;
        init_proc->m_pgid           = init_proc->m_pid;
        init_proc->m_sid            = init_proc->m_pid;
        init_proc->m_ctty           = makedev(0xFF, 0xFF);

        init_proc->m_itimer_elt[ITIMER_REAL].fcn    = timer_real_timeout;
        init_proc->m_itimer_elt[ITIMER_VIRTUAL].fcn = timer_virtual_timeout;
        init_proc->m_itimer_elt[ITIMER_PROF].fcn    = timer_prof_timeout;

        init_proc->m_itimer_elt[ITIMER_REAL].param    = init_proc;
        init_proc->m_itimer_elt[ITIMER_VIRTUAL].param = init_proc;
        init_proc->m_itimer_elt[ITIMER_PROF].param    = init_proc;

        init_proc->m_itimer[ITIMER_REAL]    = {};
        init_proc->m_itimer[ITIMER_VIRTUAL] = {};
        init_proc->m_itimer[ITIMER_PROF]    = {};

        for (int i = 0; i < NSIG; ++i)
        {
                init_proc->m_user_signal_action[i]   = { .handler = SIG_DFL, .flags = 0, .restorer = nullptr, .mask = SIGMASK(i) };
                init_proc->m_kernel_signal_action[i] = s_default_sigactions[i];
        }
        init_proc->m_user_signal_action[SIGKILL] = { .handler = SIG_DFL, .flags = 0, .restorer = nullptr, .mask = 0 };
        init_proc->m_user_signal_action[SIGSTOP] = { .handler = SIG_DFL, .flags = 0, .restorer = nullptr, .mask = 0 };

        pthread_internal *init_thrd         = pthread_alloc(init_proc->m_pid);
        init_thrd->m_kernel_thread          = current_thread();
        init_thrd->m_kernel_thread->pthread = init_thrd;
        init_thrd->m_pid                    = init_proc->m_pid;
        init_thrd->m_csignal                = SIGCHLD;

        utask_group_internal *utsk_grp = utask_group_alloc(init_proc->m_pid);
        utsk_grp->m_sid                = init_proc->m_pid;
        utsk_grp->m_members.insert(init_proc);

        fd_context_alloc(&init_thrd->m_fdctx);
        fs_context_alloc(&init_thrd->m_fsctx);
        fs_context_umask(init_thrd->m_fsctx, S_IWGRP | S_IWOTH);

        vfs_mount_t rootfs = vfs_mount_first();
        KASSERT(rootfs != nullptr);

        vfs_node_t rootvn;
        vfs_mount_rootnode(rootfs, &rootvn);

        fs_context_chroot(init_thrd->m_fsctx, rootvn);
        fs_context_chdir(init_thrd->m_fsctx, rootvn);

        vfs_node_rel(rootvn);

        vfs_file_system_mount(makedev(13, 1), "/boot", "msdos", nullptr);

        return KERN_SUCCESS;
}

late_initcall(pid1init);
