#define RBTNODE_FUNCTION_DEFINITIONS
#include <sys/stat.h>
#include <sys/sysmacros.h>

#include <aux/init.h>

#include <emerixx/chrdev.h>
#include <emerixx/vfs/vfs_node.h>
#include <emerixx/vfs/vfs_file.h>
#include <emerixx/vfs/vfs_node_data.h>
#include <emerixx/vfs/vfs_file_data.h>

#define CHRDEV_INSERT(cdev) (rbtnode_insert((cdev), &s_rb_root, &s_rb_sentinel))
#define CHRDEV_LOOKUP(devn) (rbtnode_find<dev_t, chrdev_t>((devn), s_rb_root))
#define CHRDEV_DELETE(cdev) (rbtnode_delete(&(cdev)->m_link, &s_rb_root))

static rbtnode_data s_rb_sentinel;
static rbtnode_t s_rb_root;

template <>
// ********************************************************************************************************************************
chrdev_t rbtnode_to_type(rbtnode_t a_node)
// ********************************************************************************************************************************
{
        return ((chrdev_t)a_node);
}

template <>
// ********************************************************************************************************************************
rbtnode_t type_to_rbtnode(chrdev_t a_chrdev)
// ********************************************************************************************************************************
{
        return &a_chrdev->m_link;
}

template <>
// ********************************************************************************************************************************
bool rbtnode_is_less(chrdev_t a_lhs, chrdev_t a_rhs)
// ********************************************************************************************************************************
{
        if (major(a_lhs->m_end) == major(a_rhs->m_beg))
        {
                return (minor(a_lhs->m_end) <= minor(a_rhs->m_beg));
        }
        else
        {
                return (major(a_lhs->m_end) < major(a_rhs->m_beg));
        }
}

template <>
// ********************************************************************************************************************************
bool rbtnode_is_less(chrdev_t a_lhs, dev_t a_rhs)
// ********************************************************************************************************************************
{
        if (major(a_lhs->m_end) == major(a_rhs))
        {
                return minor(a_lhs->m_end) <= minor(a_rhs);
        }
        else
        {
                return major(a_lhs->m_end) < major(a_rhs);
        }
}

template <>
// ********************************************************************************************************************************
bool rbtnode_is_less(dev_t a_lhs, chrdev_t a_rhs)
// ********************************************************************************************************************************
{
        if (major(a_lhs) == major(a_rhs->m_beg))
        {
                return minor(a_lhs) < minor(a_rhs->m_beg);
        }
        else
        {
                return (major(a_lhs) < major(a_rhs->m_beg));
        }
}

// ********************************************************************************************************************************
kern_return_t driver_chrdev_alloc(chrdev_t a_chrdev,
                                  const char *a_name,
                                  vfs_file_operations_t a_fops,
                                  dev_t a_beg,
                                  natural_t a_count)
// ********************************************************************************************************************************
{
        // TODO: Make sure we don't overflow while adding count to the minor in a_beg

        a_chrdev->m_link = {};
        a_chrdev->m_name = a_name;
        a_chrdev->m_beg  = a_beg;
        a_chrdev->m_fops = a_fops;
        a_chrdev->m_end  = makedev(major(a_beg), minor(a_beg) + a_count);

        if (CHRDEV_INSERT(a_chrdev) != a_chrdev)
        {
                // Already exists
                return KERN_FAIL(EINVAL);
        }

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t driver_chrdev_lookup(dev_t a_devno, chrdev_t *a_chrdev_out)
// ********************************************************************************************************************************
{
        chrdev_t chrdev = CHRDEV_LOOKUP(a_devno);

        if (chrdev == nullptr)
        {
                return KERN_FAIL(ENXIO);
        }

        *a_chrdev_out = chrdev;

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t driver_chrdev_free(chrdev_t a_chrdev)
// ********************************************************************************************************************************
{
        CHRDEV_DELETE(a_chrdev);
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t chrdev_vfsn_file_open(vfs_node_t a_vfsn, vfs_file_t a_vfsf)
// ********************************************************************************************************************************
{
        KASSERT(S_ISCHR(a_vfsn->m_mode));

        chrdev_t chrdev = CHRDEV_LOOKUP(a_vfsn->m_rdev);

        if (chrdev == nullptr)
        {
                return KERN_FAIL(ENXIO);
        }

        a_vfsf->m_fops /*= a_vfsn->m_fops */ = chrdev->m_fops; // Open directly if the vfs node is reused.

        a_vfsf->m_private = chrdev;

        kern_return_t retval = a_vfsf->m_fops->fopen(a_vfsn, a_vfsf);

        return retval;
}

static struct vfs_file_operations_data s_chrdev_vfsn_file_operations = {
        .fopen = chrdev_vfsn_file_open //
};

// ********************************************************************************************************************************
void chrdev_init_vfs_node(vfs_node_t a_vfsn)
// ********************************************************************************************************************************
{
        a_vfsn->m_fops = &s_chrdev_vfsn_file_operations;
}

// ********************************************************************************************************************************
static kern_return_t driver_subsys_init()
// ********************************************************************************************************************************
{
        rbtnode_init_sentinel(&s_rb_sentinel);
        s_rb_root = &s_rb_sentinel;
        return KERN_SUCCESS;
}

subsys_initcall(driver_subsys_init);
