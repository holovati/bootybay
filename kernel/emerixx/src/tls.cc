#include <aux/init.h>

#include <etl/cstring.hh>

#include <kernel/thread_data.h>
#include <kernel/vm/vm_kern.h>

#include "tls.h"

extern char __tdata_start[];
extern char __tdata_end[];

extern char __tbss_start[];
extern char __tbss_end[];

typedef union thread_local_storage_data
{
        union thread_local_storage_data *next;
        char data[1];
} *thread_local_storage_t;

static natural_t s_tbss_size;
static natural_t s_tdata_size;
static natural_t s_tls_size;
static thread_local_storage_t s_tls_freelist;

// ********************************************************************************************************************************
kern_return_t tls_dup(thread_t a_new_thread)
// ********************************************************************************************************************************
{
        KASSERT(a_new_thread->tls == 0);
        KASSERT((a_new_thread->state & TH_RUN) == 0);

        thread_local_storage_t new_tls;

        new_tls        = s_tls_freelist;
        s_tls_freelist = new_tls->next;

        thread_t thread = current_thread();

        thread_local_storage_t thread_tls = (thread_local_storage_t)(thread->tls - s_tls_size);

        etl::memcpy64(new_tls->data, thread_tls->data, s_tls_size / sizeof(uint64_t));

        a_new_thread->tls = (natural_t)&new_tls->data[s_tls_size];

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t tls_new(thread_t a_new_thread)
// ********************************************************************************************************************************
{
        KASSERT(a_new_thread->tls == 0);

        thread_local_storage_t new_tls;

        new_tls        = s_tls_freelist;
        s_tls_freelist = new_tls->next;

        etl::memcpy64(&new_tls->data[0], &__tdata_start, s_tdata_size / sizeof(uint64_t));
        etl::memset64(&new_tls->data[s_tdata_size], 0, s_tbss_size / sizeof(uint64_t));

        a_new_thread->tls = (natural_t)&new_tls->data[s_tls_size];

        if ((a_new_thread->state & TH_RUN))
        {
                assert_wait(nullptr, false);
                thread_set_timeout(1);
                thread_block(nullptr);
        }

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
void tls_exit()
// ********************************************************************************************************************************
{
        thread_t thread = current_thread();

        thread_local_storage_t tls = (thread_local_storage_t)(thread->tls - s_tls_size);

        tls->next      = s_tls_freelist;
        s_tls_freelist = tls;

        thread->tls = 0;
}

// ********************************************************************************************************************************
static kern_return_t tls_initcall()
// ********************************************************************************************************************************
{
        s_tdata_size = (natural_t)(__tdata_end - __tdata_start);
        s_tbss_size  = (natural_t)(__tbss_end - __tbss_start);

        // Alloc TLS for thread
        {
                vm_address_t vma;

                s_tls_size = s_tdata_size + s_tbss_size;

                natural_t total_tls_size = s_tls_size * THREAD_MAX;

                kern_return_t retval = kmem_alloc_wired(kernel_map, &vma, total_tls_size);

                if (retval != KERN_SUCCESS)
                {
                        return KERN_FAIL(ENOMEM);
                }

                s_tls_freelist = nullptr;

                for (natural_t p = 0; p < total_tls_size; p += s_tls_size)
                {
                        thread_local_storage_t tls = (thread_local_storage_t)(vma + p);

                        tls->next      = s_tls_freelist;
                        s_tls_freelist = tls;
                }
        }

        return KERN_SUCCESS;
}

subsys_initcall(tls_initcall);
