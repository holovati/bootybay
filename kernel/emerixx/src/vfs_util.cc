#include <sys/stat.h>
#include <fcntl.h>

#include <aux/defer.hh>

#include <emerixx/fdesc.hh>
#include <emerixx/fsdata.hh>
#include <emerixx/process.h>
#include <emerixx/vfs/vfs_pathbuf.h>
#include <emerixx/vfs/vfs_file_data.h>
#include <emerixx/vfs/vfs_file.h>
#include <emerixx/vfs/vfs_node.h>

#include <emerixx/vfs/vfs_util.h>

// ********************************************************************************************************************************
kern_return_t vfs_util_fd_to_vfsn(int a_fd, vfs_node_t *a_vfsn_out, boolean_t a_want_lock)
// ********************************************************************************************************************************
{
        if (a_fd == AT_FDCWD)
        {
                // Start at the current working directory.
                *a_vfsn_out = fs_context_getcwd(uthread_self()->m_fsctx);
        }
        else
        {
                vfs_file_t f;
                kern_return_t retval = fd_context_get_file(uthread_self()->m_fdctx, a_fd, &f);
                if (KERN_STATUS_FAILURE(retval))
                {
                        return retval;
                }
                vfs_node_ref(*a_vfsn_out = f->m_vfsn);

                vfs_file_rel(f);
        }

        if (a_want_lock == true)
        {
                vfs_node_lock(*a_vfsn_out);
        }

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t vfs_util_lookup_first_node(const char *a_kpath, int a_dfd, vfs_node_t *a_vfsn_out, boolean_t a_want_lock)
// ********************************************************************************************************************************
{
        if (a_kpath[0] == '/')
        {
                // Absolute path, we don't care about a_at_dfd
                *a_vfsn_out = fs_context_getroot(uthread_self()->m_fsctx);

                if (a_want_lock == true)
                {
                        vfs_node_lock(*a_vfsn_out);
                }

                return KERN_SUCCESS;
        }

        return vfs_util_fd_to_vfsn(a_dfd, a_vfsn_out, a_want_lock);
}

// ********************************************************************************************************************************
kern_return_t vfs_util_simple_lookup_krn_path(const char *a_kpath,
                                              natural_t a_kpath_len,
                                              int a_dfd,
                                              vfs_node_t *a_vfsn_out,
                                              boolean_t a_follow_symlink,
                                              boolean_t a_want_lock)
// ********************************************************************************************************************************
{
        vfs_node_t start_vfsn = nullptr;

        kern_return_t retval = vfs_util_lookup_first_node(a_kpath, a_dfd, &start_vfsn, false);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        retval = vfs_node_lookup(start_vfsn, a_kpath, a_kpath_len, a_vfsn_out, a_follow_symlink);

        vfs_node_rel(start_vfsn);

        if (KERN_STATUS_SUCCESS(retval) && (a_want_lock == false))
        {
                vfs_node_ref(*a_vfsn_out);
                vfs_node_put(*a_vfsn_out);
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t vfs_util_simple_lookup_krn_path_at_cwd(const char *a_kpath,
                                                     natural_t a_kpath_len,
                                                     vfs_node_t *a_vfsn_out,
                                                     boolean_t a_follow_symlink,
                                                     boolean_t a_want_lock)
// ********************************************************************************************************************************
{
        return vfs_util_simple_lookup_krn_path(a_kpath, a_kpath_len, AT_FDCWD, a_vfsn_out, a_follow_symlink, a_want_lock);
}

// ********************************************************************************************************************************
kern_return_t vfs_util_simple_lookup_usr_path(const char *a_upath,
                                              int a_dfd,
                                              vfs_node_t *a_vfsn_out,
                                              boolean_t a_follow_symlink,
                                              boolean_t a_want_lock)
// ********************************************************************************************************************************
{
        char const *kpath   = nullptr;
        natural_t kpath_len = 0;

        kern_return_t retval = vfs_pathbuf_copyin(a_upath, &kpath, &kpath_len);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        retval = vfs_util_simple_lookup_krn_path(kpath, kpath_len, a_dfd, a_vfsn_out, a_follow_symlink, a_want_lock);

        vfs_pathbuf_free(kpath);

        return retval;
}

// ********************************************************************************************************************************
kern_return_t vfs_util_create_file_simple(const char *a_kpath, natural_t a_kpath_len, mode_t a_mode, vfs_node_t *a_vfsn_out)
// ********************************************************************************************************************************
{
        vfs_node_t start_vfsn = nullptr;

        kern_return_t retval = vfs_util_lookup_first_node(a_kpath, AT_FDCWD, &start_vfsn, false /*Want lock, lookup will lock*/);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }
        defer(vfs_node_rel(start_vfsn));

        vfs_node_t vfsn_parent   = nullptr;
        const char *child_name   = nullptr;
        natural_t child_name_len = 0;

        retval = vfs_node_lookup_create(start_vfsn,
                                        a_kpath,
                                        a_kpath_len,
                                        &vfsn_parent,
                                        &child_name,
                                        &child_name_len,
                                        nullptr,
                                        false,
                                        false);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }
        defer(vfs_node_put(vfsn_parent));

        retval = vfs_node_create(vfsn_parent, child_name, child_name_len, a_mode, a_vfsn_out);

        if (KERN_STATUS_SUCCESS(retval))
        {
                vfs_node_lock(*a_vfsn_out);
        }

        return retval;
}
