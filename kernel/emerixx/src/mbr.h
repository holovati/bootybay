#pragma once

/** @file
  Legacy Master Boot Record Format Definition.

Copyright (c) 2006 - 2008, Intel Corporation. All rights reserved.<BR>
This program and the accompanying materials
are licensed and made available under the terms and conditions of the BSD
License which accompanies this distribution.  The full text of the license may
be found at http://opensource.org/licenses/bsd-license.php

THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.

**/

#define MBR_SIGNATURE 0xaa55

#define EXTENDED_DOS_PARTITION     0x05
#define EXTENDED_WINDOWS_PARTITION 0x0F

#define MAX_MBR_PARTITIONS 4

#define PMBR_GPT_PARTITION 0xEE
#define EFI_PARTITION      0xEF

#define MBR_SIZE 512

#pragma pack(1)
///
/// MBR Partition Entry
///
typedef struct
{
        u8 BootIndicator;
        u8 StartHead;
        u8 StartSector;
        u8 StartTrack;
        u8 OSIndicator;
        u8 EndHead;
        u8 EndSector;
        u8 EndTrack;
        u8 StartingLBA[4];
        u8 SizeInLBA[4];
} mbr_partition_record_t;

///
/// MBR Partition Table
///
typedef struct
{
        u8 BootStrapCode[440];
        u8 UniqueMbrSignature[4];
        u8 Unknown[2];
        mbr_partition_record_t partition[MAX_MBR_PARTITIONS];
        u16 Signature;
} mbr_t, *mbrp_t;

#pragma pack()
