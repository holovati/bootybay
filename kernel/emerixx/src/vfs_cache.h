#pragma once

#include <etl/rbtnode.h>

typedef struct vfs_cache_link_data *vfs_cache_link_t;

typedef struct vfs_cache_data *vfs_cache_t;

struct vfs_cache_link_data
{
        struct rbtnode_data m_rblink;
        unsigned char m_plen;
        char m_path[255];
};

struct vfs_cache_data
{
        rbtnode_t m_root;
        struct rbtnode_data m_sentinel;
};

void vfs_cache_init(vfs_cache_t a_vfsc);

vfs_cache_link_t vfs_cache_lookup(vfs_cache_t a_vfsc, char const *a_cn, natural_t a_cnl);