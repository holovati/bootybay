#include <etl/algorithm.hh>
#include <etl/cstring.hh>
#include <emerixx/memory_rw.h>

int uiomove(void const *buf, size_t n, puio_t uiop)
{
        size_t vcnt = uiop->uio_iovcnt;
        size_t cpsz = 0;
        u8 *bbuf    = reinterpret_cast<uint8_t *>(const_cast<void *>(buf));

        if (uiop->uio_resid == 0)
        {
                panic("Empty uio");
        }

        // What if we start with empty vector?
        for (size_t i = 0; i < vcnt; ++i)
        {
                if (uiop->uio_iov->iov_len == 0)
                {
                        uiop->uio_iov += (--uiop->uio_iovcnt > 0);
                }
        }

        // All vectors empty, what to return?
        if (uiop->uio_iov->iov_len == 0)
        {
                panic("Vectors empty but resid set");
        }

        /* user space */
        if (uiop->uio_segflg == UIO_USERSPACE)
        {
                if (uiop->uio_rw == UIO_READ)
                {
                        /* copy to kernel  */
                        for (; n && uiop->uio_iov->iov_len;)
                        {
                                cpsz    = etl::min(uiop->uio_iov->iov_len, n);
                                int err = copyout(bbuf, uiop->uio_iov->iov_base, cpsz);
                                if (err)
                                {
                                        return err;
                                }
                                n -= cpsz;
                                bbuf += cpsz;
                                uiop->uio_resid -= cpsz;
                                uiop->uio_iov->iov_len -= cpsz;
                                uiop->uio_iov->iov_base = ((u8 *)(uiop->uio_iov->iov_base)) + cpsz;
                                uiop->uio_offset += cpsz;
                                if (uiop->uio_iov->iov_len == 0)
                                {
                                        uiop->uio_iov += (--uiop->uio_iovcnt > 0);
                                }
                        }
                }
                else
                {
                        for (; n && uiop->uio_iov->iov_len;)
                        {
                                cpsz    = etl::min(uiop->uio_iov->iov_len, n);
                                int err = copyin(uiop->uio_iov->iov_base, bbuf, cpsz);
                                if (err)
                                {
                                        return err;
                                }
                                n -= cpsz;
                                bbuf += cpsz;
                                uiop->uio_resid -= cpsz;
                                uiop->uio_iov->iov_len -= cpsz;
                                uiop->uio_iov->iov_base = ((u8 *)(uiop->uio_iov->iov_base)) + cpsz;
                                uiop->uio_offset += cpsz;
                                if (uiop->uio_iov->iov_len == 0)
                                {
                                        uiop->uio_iov += (--uiop->uio_iovcnt > 0);
                                }
                        }
                }
        }
        else if (uiop->uio_segflg == UIO_SYSSPACE)
        {
                if (uiop->uio_rw == UIO_READ)
                {
                        /* copy to kernel  */
                        for (; n && uiop->uio_iov->iov_len;)
                        {
                                cpsz = etl::min(uiop->uio_iov->iov_len, n);
                                etl::memcpy8(uiop->uio_iov->iov_base, bbuf, cpsz);
                                n -= cpsz;
                                bbuf += cpsz;
                                uiop->uio_resid -= cpsz;
                                uiop->uio_iov->iov_len -= cpsz;
                                uiop->uio_iov->iov_base = ((u8 *)(uiop->uio_iov->iov_base)) + cpsz;
                                uiop->uio_offset += cpsz;
                                if (uiop->uio_iov->iov_len == 0)
                                {
                                        uiop->uio_iov += (--uiop->uio_iovcnt > 0);
                                }
                        }
                }
                else
                {
                        for (; n && uiop->uio_iov->iov_len;)
                        {
                                cpsz = etl::min(uiop->uio_iov->iov_len, n);
                                etl::memcpy8(bbuf, uiop->uio_iov->iov_base, cpsz);
                                n -= cpsz;
                                bbuf += cpsz;
                                uiop->uio_resid -= cpsz;
                                uiop->uio_iov->iov_len -= cpsz;
                                uiop->uio_iov->iov_base = ((u8 *)(uiop->uio_iov->iov_base)) + cpsz;
                                uiop->uio_offset += cpsz;
                                if (uiop->uio_iov->iov_len == 0)
                                {
                                        uiop->uio_iov += (--uiop->uio_iovcnt > 0);
                                }
                        }
                }
        }
        else
        {
                panic("Unknown uio_segflg");
        }
        return 0;
}

kern_return_t uiomove2(void const *buf, natural_t *n, puio_t uiop)
{
        natural_t oresid     = uiop->uio_resid;
        kern_return_t retval = uiomove(buf, *n, uiop);
        if (KERN_STATUS_SUCCESS(retval))
        {
                *n = oresid - uiop->uio_resid;
        }
        return retval;
}

kern_return_t uiomove3(puio_t uiop, uiomove_func_t a_iofucn, void *a_arg)
{
        char tmpbuf[0x200];
        /* user space */
        if (uiop->uio_segflg == UIO_USERSPACE)
        {
                if (uiop->uio_rw == UIO_READ)
                {
                        /* copy from kernel to user  */
                        panic("Not implemented");
                }
                else
                {
                        /* copy from user to kernel */
                        for (natural_t i = 0; i < uiop->uio_iovcnt; ++i)
                        {
                                while (uiop->uio_iov[i].iov_len > 0)
                                {
                                        natural_t cpsz       = etl::min(uiop->uio_iov[i].iov_len, sizeof(tmpbuf));
                                        kern_return_t retval = copyin(uiop->uio_iov[i].iov_base, tmpbuf, cpsz);

                                        if (KERN_STATUS_FAILURE(retval))
                                        {
                                                return retval;
                                        }

                                        cpsz = a_iofucn(tmpbuf, cpsz, a_arg);

                                        if (cpsz == 0)
                                        {
                                                return KERN_SUCCESS;
                                        }

                                        uiop->uio_resid -= cpsz;
                                        uiop->uio_iov[i].iov_len -= cpsz;
                                        uiop->uio_iov[i].iov_base = ((u8 *)(uiop->uio_iov[i].iov_base)) + cpsz;
                                        uiop->uio_offset += cpsz;
                                }
                        }
                }
        }
        else if (uiop->uio_segflg == UIO_SYSSPACE)
        {
                panic("Not implemented");
        }
        else
        {
                panic("Unknown uio_segflg");
        }
        return 0;
}
