#include <emerixx/process.h>
#include <emerixx/sleep.h>
#include <kernel/thread.h>
#include <kernel/thread_data.h>
#include <aux/defer.hh>
#include <mach/machine/spl.h>

// ********************************************************************************************************************************
kern_return_t uthread_sleep(emerixx_wait_event_t a_event)
// ********************************************************************************************************************************
{
        spl_t s = splhigh();
        defer(splx(s));

        assert_wait(a_event, true);

        if (uthread_pending_signal(current_thread()->pthread))
        {
                clear_wait(current_thread(), THREAD_INTERRUPTED, true);
                return KERN_FAIL(EINTR);
        }

        thread_block(nullptr);

        if (uthread_pending_signal(current_thread()->pthread))
        {
                return KERN_FAIL(EINTR);
        }

        return (current_thread()->wait_result == THREAD_INTERRUPTED) ? KERN_FAIL(EINTR) : KERN_SUCCESS;
}

// ********************************************************************************************************************************
void uthread_wakeup(emerixx_wait_event_t a_event)
// ********************************************************************************************************************************
{
        thread_wakeup_interruptible_only(a_event);
}

// ********************************************************************************************************************************
kern_return_t uthread_sleep_timeout(emerixx_wait_event_t a_event, int a_timeout)
// ********************************************************************************************************************************
{
        spl_t s = splhigh();
        defer(splx(s));

        assert_wait(a_event, true);

        if (uthread_pending_signal(current_thread()->pthread))
        {
                clear_wait(current_thread(), THREAD_INTERRUPTED, true);
                return KERN_FAIL(EINTR);
        }

        if (a_event == nullptr && a_timeout == 0)
        {
                a_timeout = 1;
        }

        if (a_timeout > 0)
        {
                thread_set_timeout(a_timeout);
        }

        thread_block(nullptr);

        if (uthread_pending_signal(current_thread()->pthread))
        {
                return KERN_FAIL(EINTR);
        }

        switch (current_thread()->wait_result)
        {

                case THREAD_AWAKENED: /* normal wakeup */
                {
                        return KERN_SUCCESS;
                }

                case THREAD_TIMED_OUT: /* timeout expired */
                {
                        return KERN_FAIL(EWOULDBLOCK);
                }

                case THREAD_INTERRUPTED: /* interrupted by clear_wait */
                {
                        return KERN_FAIL(EINTR);
                }
                default:
                {
                        KASSERT("Should never happen");
                        return -1;
                }
        }
}

// ********************************************************************************************************************************
kern_return_t uthread_sleep_timeout(emerixx_wait_event_t a_event, int a_timeout, lock_t a_lock)
// ********************************************************************************************************************************
{
        spl_t s = splhigh();
        defer(splx(s));

        assert_wait(a_event, true);

        if (uthread_pending_signal(current_thread()->pthread))
        {
                clear_wait(current_thread(), THREAD_INTERRUPTED, true);
                return KERN_FAIL(EINTR);
        }

        if (a_event == nullptr && a_timeout == 0)
        {
                a_timeout = 1;
        }

        if (a_timeout > 0)
        {
                thread_set_timeout(a_timeout);
        }

        boolean_t writelock = FALSE;

        if (a_lock != nullptr)
        {
                writelock = a_lock->want_write;
                lock_done(a_lock);
        }

        thread_block(nullptr);

        if (a_lock != nullptr)
        {
                if (writelock == true)
                {
                        lock_write(a_lock);
                }
                else
                {
                        lock_read(a_lock);
                }
        }

        if (uthread_pending_signal(current_thread()->pthread))
        {
                return KERN_FAIL(EINTR);
        }

        switch (current_thread()->wait_result)
        {

                case THREAD_AWAKENED: /* normal wakeup */
                {
                        return KERN_SUCCESS;
                }

                case THREAD_TIMED_OUT: /* timeout expired */
                {
                        return KERN_FAIL(EWOULDBLOCK);
                }

                case THREAD_INTERRUPTED: /* interrupted by clear_wait */
                {
                        return KERN_FAIL(EINTR);
                }
                default:
                {
                        KASSERT("Should never happen");
                        return -1;
                }
        }
}
