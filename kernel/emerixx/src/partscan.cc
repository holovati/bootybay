/**
  @file
  Routines supporting partition discovery and
  logical device reading
  Copyright (c) 2006 - 2019, Intel Corporation. All rights reserved.<BR>
  SPDX-License-Identifier: BSD-2-Clause-Patent
**/

#include "mbr.h"

#include <mach/param.h>
#include <emerixx/memory_rw.h>

#include <emerixx/disklabel.h>

#include <etl/algorithm.hh>

#include <sys/param.h> /* For MIN/MAX */

/*
 * Extract UINT32 from char array
 */

#define UNPACK_UINT32(a) (u32)((((u8 *)a)[0] << 0) | (((u8 *)a)[1] << 8) | (((u8 *)a)[2] << 16) | (((u8 *)a)[3] << 24))

#define UNPACK_INT32(a) (i32)((((u8 *)a)[0] << 0) | (((u8 *)a)[1] << 8) | (((u8 *)a)[2] << 16) | (((u8 *)a)[3] << 24))

static boolean_t mbr_valid(mbrp_t mbr, size_t last_lba)
{
        if (mbr->Signature != MBR_SIGNATURE)
        {
                return FALSE;
        }

        //
        // The BPB also has this signature, so it can not be used alone.
        //

        boolean_t mbr_valid = FALSE;
        for (size_t idx1 = 0; idx1 < MAX_MBR_PARTITIONS; idx1++)
        {
                if (mbr->partition[idx1].OSIndicator == 0x00 || UNPACK_UINT32(mbr->partition[idx1].SizeInLBA) == 0)
                {
                        continue;
                }

                mbr_valid        = TRUE;
                u32 starting_lba = UNPACK_UINT32(mbr->partition[idx1].StartingLBA);
                u32 ending_lba   = starting_lba + UNPACK_UINT32(mbr->partition[idx1].SizeInLBA) - 1;
                if (ending_lba > last_lba)
                {
                        //
                        // Compatibility Errata:
                        //  Some systems try to hide drive space with their INT 13h driver
                        //  This does not hide space from the OS driver. This means the MBR
                        //  that gets created from DOS is smaller than the MBR created from
                        //  a real OS (NT & Win98). This leads to BlockIo->LastBlock being
                        //  wrong on some systems FDISKed by the OS.
                        //
                        //  return FALSE Because no block devices on a system are implemented
                        //  with INT 13h
                        //
                        return FALSE;
                }

                for (size_t idx2 = idx1 + 1; idx2 < MAX_MBR_PARTITIONS; idx2++)
                {
                        if (mbr->partition[idx2].OSIndicator == 0x00 || UNPACK_INT32(mbr->partition[idx2].SizeInLBA) == 0)
                        {
                                continue;
                        }

                        u32 new_ending_lba = UNPACK_UINT32(mbr->partition[idx2].StartingLBA)
                                           + UNPACK_UINT32(mbr->partition[idx2].SizeInLBA) - 1;
                        if (new_ending_lba >= starting_lba && UNPACK_UINT32(mbr->partition[idx2].StartingLBA) <= starting_lba)
                        {
                                //
                                // This region overlaps with the Index1'th region
                                //
                                return FALSE;
                        }
                }
        }

        //
        // Non of the regions overlapped so MBR is O.K.
        //
        return mbr_valid;
}
#if 0
kern_return_t partition_scan(vnode *a_vnode, size_t dev_lstlba, partitionp_t partp, size_t partsz, size_t *npartsp)
{
        //KASSERT(a_vnode->islocked());


        mbr_t mbr;

        struct iovec iov;
        iov.iov_base = &mbr;
        iov.iov_len  = sizeof(mbr);

        uio u;
        u.uio_iov    = &iov;
        u.uio_iovcnt = 1;
        u.uio_offset = 0;
        u.uio_resid  = sizeof(mbr);
        u.uio_segflg = UIO_SYSSPACE;
        u.uio_rw     = UIO_READ;

        kern_return_t result = a_vnode->uio_move(&u, 0);

        if (result == KERN_SUCCESS)
        {
                *npartsp = 0;
                if (mbr_valid(&mbr, dev_lstlba))
                {

                        for (size_t pidx = 0; pidx < MIN(MAX_MBR_PARTITIONS, partsz); ++pidx)
                        {
                                if (mbr.partition[*npartsp].OSIndicator == 0x00
                                    || UNPACK_INT32(mbr.partition[*npartsp].SizeInLBA) == 0)
                                {
                                        continue;
                                }
                                /* number of sectors in partition */
                                partp[*npartsp].p_size = UNPACK_UINT32(mbr.partition[*npartsp].SizeInLBA);

                                /* starting sector */
                                partp[*npartsp].p_offset = UNPACK_UINT32(mbr.partition[*npartsp].StartingLBA);

                                ++*npartsp;
                        }
                }
                else
                {
                        result = KERN_FAIL(ENODEV);
                }
        }

        return result;
}
#endif

int mbr_partition_scan(void *a_mbr, natural_t a_max_lba, partitionp_t partp, natural_t partp_cnt)
{
        mbr_t *mbr = (mbr_t *)a_mbr;

        if (mbr_valid(mbr, a_max_lba) == false)
        {
                return KERN_FAIL(ENODEV);
        }

        int parts     = 0;
        int max_parts = etl::min(MAX_MBR_PARTITIONS, (int)partp_cnt);

        for (int i = 0; i < max_parts; ++i)
        {
                if ((mbr->partition[i].OSIndicator == 0x00) || (UNPACK_INT32(mbr->partition[i].SizeInLBA) == 0))
                {
                        continue;
                }

                /* number of sectors in partition */
                partp[i].p_size = UNPACK_UINT32(mbr->partition[i].SizeInLBA);

                /* starting sector */
                partp[i].p_offset = UNPACK_UINT32(mbr->partition[i].StartingLBA);

                parts++;
        }

        return parts;
}
