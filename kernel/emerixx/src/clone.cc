#include <emerixx/fdesc.hh>
#include <emerixx/fsdata.hh>
#include <emerixx/process.h>
#include <emerixx/sleep.h>
#include <emerixx/uthread_status.h>
#include <kernel/sched.h>
#include <kernel/thread.h>
#include <kernel/vm/vm_map.h>
#include <kernel/thread_data.h>

// musl pthread create :
// (CLONE_VM | CLONE_FS | CLONE_FILES | CLONE_SIGHAND | CLONE_THREAD | CLONE_SYSVSEM | CLONE_SETTLS | CLONE_PARENT_SETTID |
// CLONE_CHILD_CLEARTID | CLONE_DETACHED) CSIGNAL = 0

// vfork flags :
// (CLONE_VM | CLONE_VFORK) CSIGNAL = SIGCHLD

// fork flags :
// CSIGNAL = SIGCHLD

// long clone(unsigned long flags, void *stack, int *parent_tid, int *child_tid, unsigned long tls);

kern_return_t linux_clone(pthread_t a_thread,
                          natural_t a_flags,
                          vm_address_t a_stack,
                          pid_t *a_parent_tid,
                          pid_t *a_child_tid,
                          vm_address_t a_tls)
{

        if (a_stack != 0)
        {
                return KERN_FAIL(EINVAL);
        }

        KASSERT((a_stack & ~0xF) == 0); // Must be aligned

        kern_return_t retval = KERN_SUCCESS;

        pthread_t new_uthrd;
        process_t new_utask;
        if ((a_flags & CLONE_THREAD) == 0)
        {

                retval = utask_dup(&new_utask, &new_uthrd);
                KASSERT(KERN_STATUS_SUCCESS(retval));

                if ((a_flags & CLONE_VM) == 0)
                {
                        retval = vm_context_dup(new_utask);
                        KASSERT(KERN_STATUS_SUCCESS(retval));
                }
        }
        else
        {
                retval = uthread_dup(&new_uthrd);
                KASSERT(KERN_STATUS_SUCCESS(retval));
        }

        if ((a_flags & CLONE_SIGHAND) == 0)
        {
                // retval = sighand_fork(nt->pt_sighand, &nt->pt_sighand);

                KASSERT(KERN_STATUS_SUCCESS(retval));
        }

        if ((a_flags & CLONE_FILES) == 0)
        {
                fd_context_t old = new_uthrd->m_fdctx;
                retval           = fd_context_clone(new_uthrd->m_fdctx, &new_uthrd->m_fdctx);
                KASSERT(KERN_STATUS_SUCCESS(retval));
                fd_context_deallocate(old);
        }

        if ((a_flags & CLONE_FS) == 0)
        {
                fs_context_t old = new_uthrd->m_fsctx;
                retval           = fs_context_dup(new_uthrd->m_fsctx, &new_uthrd->m_fsctx);
                KASSERT(KERN_STATUS_SUCCESS(retval));
                fs_context_deallocate(old);
        }

        if (a_flags & CLONE_CHILD_CLEARTID)
        {
                // Clear (zero) the child thread ID at the location pointed to by child_tid (clone()) or cl_args.child_tid
                // (clone3()) in child memory when the child exits, and do a wakeup on the futex at that address. The address
                // involved may be changed by the set_tid_address(2) system call.  This is used by threading libraries. }

                new_uthrd->m_tid_address = a_child_tid;
        }

        if (a_flags & CLONE_PARENT_SETTID)
        {
                // Store the child thread ID at the location pointed to by parent_tid (clone()) or cl_args.parent_tid (clone3()) in
                // the parent's memory.  (In Linux 2.5.32-2.5.48 there was a flag CLONE_SETTID that did this.) The store operation
                // completes before the clone call returns control to user space.
                *a_parent_tid = new_uthrd->m_tid;
        }

        new_uthrd->m_csignal = a_flags & CSIGNAL; // Needs to be range checked

        thread_state_regs_t thrdst;
        natural_t thrdst_count = sizeof(thrdst);

        KASSERT(KERN_STATUS_SUCCESS(uthread_get_state(new_uthrd, THREAD_STATE_FLAVOR_GP_REGS, thrdst, &thrdst_count)));

        if (a_flags & CLONE_SETTLS)
        {
                thrdst[THREAD_STATE_GP_REGS_TLS] = a_tls;
        }

        if (a_stack)
        {
                thrdst[THREAD_STATE_GP_REGS_SP] = a_stack;
        }

        thrdst[THREAD_STATE_GP_REGS_RETVAL] = KERN_SUCCESS;

        KASSERT(KERN_STATUS_SUCCESS(uthread_set_state(new_uthrd, THREAD_STATE_FLAVOR_GP_REGS, thrdst, thrdst_count)));

        new_utask->m_clone_flags = a_flags;

        // thread_start(nt->pt_kthread, locore_exception_return);

        // thread_resume(nt->pt_kthread);

        // Thread might be freed before we have a chance to collect the pid
        pid_t new_tid = new_uthrd->m_tid;

        while (new_utask->m_clone_flags & CLONE_VFORK)
        {
                uthread_sleep(&new_utask->m_clone_flags);
        }

        return new_tid;
}
