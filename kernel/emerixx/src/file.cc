#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/file.h>

#include <etl/queue.h>

#include <kernel/zalloc.h>

#include <emerixx/poll.h>
#include <emerixx/ioctl_req.h>
#include <emerixx/vfs/vfs_node.h>
#include <emerixx/vfs/vfs_node_data.h>
#include <emerixx/vfs/vfs_file_data.h>

#include <emerixx/vfs/vfs_file.h>

/* Defines */
#define LOCK_ZILCH 0 /* For clarity*/

/* Typedefs */
typedef struct advisory_lock_record_data *advisory_lock_record_t;
typedef etl::queue<struct advisory_lock_record_data> advisory_lock_list;

/* Internal data structures */
struct advisory_lock_record_data : advisory_lock_list::chain
{
        pid_t m_pid;     // Who owns this record
        int m_operation; // What kind of lock does it have
};

struct vfs_file_internal
{
        struct vfs_file_data m_vfsf;
        struct lock_data m_advirsory_lock;
        advisory_lock_list m_advirsory_lock_list;
};

/* Storage */
ZONE_DEFINE(advisory_lock_record_data, s_lock_record_zone, FILEMAX, 0x10, ZONE_EXHAUSTIBLE, 0x10);

ZONE_DEFINE(vfs_file_internal, s_file_zone, FILEMAX, 0x10, ZONE_EXHAUSTIBLE, 0x10);

/* Internal globals */
static int nfiles;

// ********************************************************************************************************************************
kern_return_t vfs_file_read(vfs_file_t a_file, memory_rdwr_t a_mrw)
// ********************************************************************************************************************************
{
        return a_file->m_fops->fread(a_file, a_mrw);
}

// ********************************************************************************************************************************
kern_return_t vfs_file_getdents(vfs_file_t a_file, memory_rdwr_t a_mrw)
// ********************************************************************************************************************************
{
        return a_file->m_fops->fgetdents(a_file, a_mrw);
}

// ********************************************************************************************************************************
kern_return_t vfs_file_write(vfs_file_t a_file, memory_rdwr_t a_mrw)
// ********************************************************************************************************************************
{
        return a_file->m_fops->fwrite(a_file, a_mrw);
}

// ********************************************************************************************************************************
kern_return_t vfs_file_lseek(vfs_file_t a_file, off_t a_offset, int a_whence)
// ********************************************************************************************************************************
{
        return a_file->m_fops->flseek(a_file, a_offset, a_whence);
}

// ********************************************************************************************************************************
kern_return_t vfs_file_ioctl(vfs_file_t a_file, ioctl_req_t a_ioctl_req)
// ********************************************************************************************************************************
{
        integer_t cmdno = ioctl_req_cmdno(a_ioctl_req);
        integer_t arg   = ioctl_req_arg(a_ioctl_req);

        // Intercept "ioctls" that modify abstract file data.
        switch (cmdno)
        {
                case FIONBIO:
                        if (arg != 0)
                        {
                                a_file->m_oflags |= O_NONBLOCK;
                        }
                        else
                        {
                                a_file->m_oflags &= ~O_NONBLOCK;
                        }
                        break;

                case FIOASYNC:
                        if (arg != 0)
                        {
                                a_file->m_oflags |= O_ASYNC;
                        }
                        else
                        {
                                a_file->m_oflags &= ~O_ASYNC;
                        }
                        break;
                default:
                        break;
        }

        return a_file->m_fops->fioctl(a_file, a_ioctl_req);
}

// ********************************************************************************************************************************
kern_return_t vfs_file_mmap(vfs_file_t a_vfsf, vm_object_t *a_object_out)
// ********************************************************************************************************************************
{
        return a_vfsf->m_fops->fmmap(a_vfsf, a_object_out);
}

// ********************************************************************************************************************************
void vfs_file_ref(vfs_file_t a_file)
// ********************************************************************************************************************************
{
        a_file->m_usecnt++;
}

// ********************************************************************************************************************************
kern_return_t vfs_file_remove_lock(vfs_file_t a_file) // Remove lock held by this pid
// ********************************************************************************************************************************
{
        struct vfs_file_internal *fi = (struct vfs_file_internal *)a_file;

        pid_t thispid = getpid();

        for (advisory_lock_record_t lr = fi->m_advirsory_lock_list.first(); //
             fi->m_advirsory_lock_list.end(lr) == false;
             lr = lr->next_element())
        {
                if (lr->m_pid == thispid)
                {
                        lock_done(&fi->m_advirsory_lock);
                        lr->unchain();
                        s_lock_record_zone.free(lr);
                        break;
                }
        }

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t vfs_file_rel(vfs_file_t a_file)
// ********************************************************************************************************************************
{
        KASSERT((a_file->m_usecnt) > 0);

        struct vfs_file_internal *fi = (struct vfs_file_internal *)a_file;

        if (--a_file->m_usecnt)
        {
                return KERN_SUCCESS;
        }
        KASSERT(fi->m_advirsory_lock_list.empty());
        KASSERT(fi->m_advirsory_lock.read_count == 0);
        KASSERT(fi->m_advirsory_lock.waiting == 0);

        kern_return_t retval = a_file->m_fops->fclose(a_file);

        vfs_node_rel(a_file->m_vfsn);

        s_file_zone.free(fi);

        nfiles--;

        return retval;
}

// ********************************************************************************************************************************
kern_return_t vfs_file_poll(vfs_file_t a_file, pollfd **a_pfd)
// ********************************************************************************************************************************
{
        return a_file->m_fops->fpoll(a_file, a_pfd);
}

// ********************************************************************************************************************************
kern_return_t vfs_file_lock(vfs_file_t a_vfsf, int a_operation)
// ********************************************************************************************************************************
{
        kern_return_t retval = KERN_SUCCESS;

        struct vfs_file_internal *fi = (struct vfs_file_internal *)a_vfsf;

        pid_t thispid = getpid();

        advisory_lock_record_t lockrec = nullptr;

        boolean_t no_blocking = !!(a_operation & LOCK_NB);

        a_operation &= ~LOCK_NB;

        for (advisory_lock_record_t lr = fi->m_advirsory_lock_list.first(); //
             fi->m_advirsory_lock_list.end(lr) == false;
             lr = lr->next_element())
        {
                if (lr->m_pid == thispid)
                {
                        lockrec = lr;
                        break;
                }
        }

        if (lockrec == nullptr)
        {
                if (a_operation == LOCK_UN)
                {
                        goto out;
                }

                lockrec = s_lock_record_zone.alloc();

                if (lockrec == nullptr)
                {
                        // No memory
                        retval = KERN_FAIL(ENOLCK);
                        goto out;
                }

                lockrec->m_pid = thispid;

                lockrec->m_operation = LOCK_ZILCH;

                fi->m_advirsory_lock_list.enqueue(lockrec);
        }
        else
        {
                if (a_operation == LOCK_UN)
                {
                        lockrec->unchain();
                        lock_done(&fi->m_advirsory_lock);
                        s_lock_record_zone.free(lockrec);
                        goto out;
                }
        }

        switch (lockrec->m_operation)
        {
                case LOCK_ZILCH:
                {
                        if (a_operation == LOCK_SH)
                        {
                                // Had nothing, wants shared
                                if (no_blocking == true)
                                {
                                        if (lock_try_read(&fi->m_advirsory_lock) == false)
                                        {
                                                retval = KERN_FAIL(EWOULDBLOCK);
                                                goto out;
                                        }
                                }
                                else
                                {
                                        lock_read(&fi->m_advirsory_lock);
                                }

                                lockrec->m_operation = LOCK_SH;
                        }
                        else if (a_operation == LOCK_EX)
                        {
                                // Had nothing, wants exclusive
                                if (no_blocking == true)
                                {
                                        if (lock_try_write(&fi->m_advirsory_lock) == false)
                                        {
                                                retval = KERN_FAIL(EWOULDBLOCK);
                                                goto out;
                                        }
                                }
                                else
                                {
                                        lock_write(&fi->m_advirsory_lock);
                                }

                                lockrec->m_operation = LOCK_EX;
                        }
                }
                break;
                case LOCK_SH:
                {
                        if (a_operation == LOCK_SH)
                        {
                                // Had shared, wants exclusive
                                if (no_blocking == true)
                                {
                                        if (lock_try_read_to_write(&fi->m_advirsory_lock) == false)
                                        {
                                                retval = KERN_FAIL(EWOULDBLOCK);
                                                goto out;
                                        }
                                }
                                else
                                {
                                        lock_read_to_write(&fi->m_advirsory_lock);
                                }

                                lockrec->m_operation = LOCK_EX;
                        }
                }
                break;
                case LOCK_EX:
                {
                        if (a_operation == LOCK_SH)
                        {
                                lock_write_to_read(&fi->m_advirsory_lock);
                                lockrec->m_operation = LOCK_EX;
                        }
                }
                break;
                default:
                        panic("Unhandled flock operation");
                        break;
        }
out:
        return retval;
}

// ********************************************************************************************************************************
kern_return_t vfs_file_splice(vfs_file_t a_file,
                              off_t *a_off,
                              vfs_file_t a_file_out,
                              off_t *a_off_out,
                              size_t a_len,
                              unsigned a_flags)
// ********************************************************************************************************************************
{
        return a_file->m_fops->fsplice(a_file, a_off, a_file_out, a_off_out, a_len, a_flags);
}

// ********************************************************************************************************************************
kern_return_t vfs_file_open(vfs_node_t a_vfsn, int a_oflags, vfs_file_t *a_vfsf_out)
// ********************************************************************************************************************************
{
        struct vfs_file_internal *fi = s_file_zone.alloc();

        if (fi == nullptr)
        {
                return KERN_FAIL(ENOMEM);
        }

        lock_init(&fi->m_advirsory_lock, true);
        fi->m_advirsory_lock_list.initialize();

        vfs_file_t f = &fi->m_vfsf;

        f->m_fops   = a_vfsn->m_fops;
        f->m_oflags = a_oflags;
        vfs_node_ref(f->m_vfsn = a_vfsn);
        f->m_private = nullptr;
        f->m_usecnt  = 1;

        kern_return_t retval = f->m_fops->fopen(a_vfsn, f);

        if (KERN_STATUS_FAILURE(retval))
        {
                vfs_node_rel(f->m_vfsn);
                s_file_zone.free(fi);
                return retval;
        }
        else
        {
                *a_vfsf_out = f;
        }

        nfiles++;

        return retval;
}

// Generic file operations

// ********************************************************************************************************************************
kern_return_t vfs_file_op_mmap_enodev(vfs_file_t, vm_object_t *)
// ********************************************************************************************************************************
{
        return KERN_FAIL(ENODEV);
}

// ********************************************************************************************************************************
kern_return_t vfs_file_op_lseek_espipe(vfs_file_t, off_t, int)
// ********************************************************************************************************************************
{
        return KERN_FAIL(ESPIPE);
}

// ********************************************************************************************************************************
kern_return_t vfs_file_op_getdents_enotdir(vfs_file_t, memory_rdwr_t)
// ********************************************************************************************************************************
{
        return KERN_FAIL(ENOTDIR);
}

// ********************************************************************************************************************************
kern_return_t vfs_file_op_ioctl_enotty(vfs_file_t, ioctl_req_t)
// ********************************************************************************************************************************
{
        return KERN_FAIL(ENOTTY);
}

// ********************************************************************************************************************************
kern_return_t vfs_file_op_vfsn_rw(vfs_file_t a_file, memory_rdwr_t a_mrw)
// ********************************************************************************************************************************
{
        return a_file->m_vfsn->m_ops->rdwr(a_file->m_vfsn, a_file->m_oflags, a_mrw);
}

// ********************************************************************************************************************************
kern_return_t vfs_file_op_open_noop(vfs_node_t, vfs_file_t)
// ********************************************************************************************************************************
{
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t vfs_file_op_close_noop(vfs_file_t)
// ********************************************************************************************************************************
{
        return KERN_SUCCESS;
}
