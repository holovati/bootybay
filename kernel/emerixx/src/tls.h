#pragma once

#include <kernel/kern_types.h>

/// @brief Duplicate the current thread tls
/// @param a_new_thread
/// @return
kern_return_t tls_dup(thread_t a_new_thread);

/// @brief Inititalze a new tls
/// @param a_new_thread
/// @return
kern_return_t tls_new(thread_t a_new_thread);

/// @brief Free the tls storage for current thread
void tls_exit();
