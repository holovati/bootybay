#include <fcntl.h>
#define RBTREE_IMPLEMENTATION
#include <aux/defer.hh>

#include <etl/rbtree.hh>
#include <etl/algorithm.hh>

#include <kernel/zalloc.h>
#include <kernel/env.h>

#include <emerixx/process.h>
#include <emerixx/memory_rw.h>
#include <emerixx/vfs/vfs_file.h>
#include <emerixx/vfs/vfs_file_data.h>

#include <emerixx/fdesc.hh>

#define PTHREAD_MAX 0x1000

typedef struct fd // Move to file.hh
{
        rbtnode_data m_open_link; // Used for lookup on open file descriptors
        rbtnode_data m_free_link; // Tree link for for file descriptors that have free space after themselves.
        vfs_file_t m_file;        // File we are describing
        int32_t m_id;             // Id of this descriptor
        int32_t m_fd_flags;       // FD_ flags (FD_CLOEXEC)
        // mode_t m_open_flags;      // The flags the file has been opened with (O_*)
        int32_t m_id_next; // Id of the next open file descriptor(used for keeping track of free space).
        int pad;
} *fd_t;

typedef RBTREE_UNIQUE_KEY(fd, m_open_link) fd_open_map;
typedef RBTREE_UNIQUE_KEY(fd, m_free_link) fd_free_map;

struct fd_data
{
        fd_open_map m_open_files; // Map of all open files
        fd_free_map m_free_ids;   // Tracking of fd's with free file ids
        fd m_free_ids_all;        // Last successfull lookup
        uint32_t m_nfiles;        // Number of open files allocated
        uint32_t m_refcnt;        // Reference count of this structure
        lock_data m_lock;         // Must be locked on all operations
};

// Zones for data structure allocation
ZONE_DEFINE(fd, s_fd_zone, PTHREAD_MAX *FILEMAX, PAGE_SIZE / sizeof(fd), ZONE_EXHAUSTIBLE, 0x10);
ZONE_DEFINE(fd_data, s_fd_context_zone, PTHREAD_MAX *FILEMAX, PAGE_SIZE / sizeof(fd_data), ZONE_EXHAUSTIBLE, 0x10);

// For open descriptors

RBTREE_FIND_LESS(fd_open_map, int const &)
// ********************************************************************************************************************************
(fd &lhs, int const &rhs)
// ********************************************************************************************************************************
{
        return lhs.m_id < rhs;
}

RBTREE_FIND_LESS(fd_open_map, int const &)
// ********************************************************************************************************************************
(int const &lhs, fd &rhs)
// ********************************************************************************************************************************
{
        return lhs < rhs.m_id;
}

RBTREE_INSERT_LESS(fd_open_map, fd)
// ********************************************************************************************************************************
(fd &lhs, fd &rhs)
// ********************************************************************************************************************************
{
        return lhs.m_id < rhs.m_id;
}

// For free descriptors

RBTREE_FIND_LESS(fd_free_map, int const &)
// ********************************************************************************************************************************
(int const &lhs, fd &rhs)
// ********************************************************************************************************************************
{
        fd_t left = fd_free_map::left(&rhs);
        return left && lhs < fd_free_map::max(left)->m_id_next;
}

RBTREE_FIND_LESS(fd_free_map, int const &)
// ********************************************************************************************************************************
(fd &lhs, int const &rhs)
// ********************************************************************************************************************************
{
        return lhs.m_id_next <= rhs;
}

RBTREE_INSERT_LESS(fd_free_map, fd)
// ********************************************************************************************************************************
(fd &lhs, fd &rhs)
// ********************************************************************************************************************************
{
        return lhs.m_id < rhs.m_id;
}

// ********************************************************************************************************************************
static inline bool fd_freelist_eligible(fd_t a_fd)
// ********************************************************************************************************************************
{

        return (a_fd->m_id + 1) < a_fd->m_id_next;
}

// ********************************************************************************************************************************
static fd_t fd_alloc(fd_context_t a_fd_data, int const a_min_fd = 0)
// ********************************************************************************************************************************
{
        fd_t newfd = s_fd_zone.calloc();

        fd_t fd = a_fd_data->m_free_ids.find(a_min_fd);

        if ((fd->m_id == 0x7) && (fd->m_id_next == a_min_fd) && (a_min_fd == 0xa) && (a_fd_data->m_nfiles == 8))
        {
                fd = a_fd_data->m_free_ids.find(a_min_fd);
        }

        if (fd != &a_fd_data->m_free_ids_all) [[likeley]]
        {
                if (a_min_fd <= fd->m_id) [[likeley]]
                {
                        newfd->m_id = fd->m_id + 1;
                }
                else
                {
                        newfd->m_id = a_min_fd;
                }
        }
        else
        {
                newfd->m_id = 0;
        }

        newfd->m_id_next = fd->m_id_next;

        fd->m_id_next = newfd->m_id;

        if (fd_freelist_eligible(fd) == false) [[likeley]]
        {
                a_fd_data->m_free_ids.remove(fd);
        }

        if (fd_freelist_eligible(newfd) == true)
        {
                a_fd_data->m_free_ids.insert(newfd);
        }

        KASSERT(newfd == a_fd_data->m_open_files.insert(newfd));

        a_fd_data->m_nfiles++;

        return newfd;
}

// ********************************************************************************************************************************
static void fd_free(fd_context_t a_fd_data, fd_t a_fd)
// ********************************************************************************************************************************
{
        if (a_fd->m_id == 0xa)
        {
                // log_debug("catch");
        }
        if (fd_freelist_eligible(a_fd) == true)
        {
                a_fd_data->m_free_ids.remove(a_fd);
        }

        fd_t prevfd = fd_open_map::prev(a_fd);

        a_fd_data->m_open_files.remove(a_fd);

        bool freelist_insert;

        if (prevfd == nullptr)
        {
                KASSERT(a_fd_data->m_free_ids_all.m_id_next == a_fd->m_id);

                prevfd = &a_fd_data->m_free_ids_all;

                freelist_insert = (a_fd->m_id == 0);
        }
        else
        {
                freelist_insert = (fd_freelist_eligible(prevfd) == false);
        }

        if (freelist_insert)
        {
                a_fd_data->m_free_ids.insert(prevfd);
        }

        prevfd->m_id_next = a_fd->m_id_next;

        a_fd_data->m_nfiles--;

        s_fd_zone.free(a_fd);
}

// ********************************************************************************************************************************
static inline fd_t fd_find_open(fd_context_t a_fd_data, int a_fd)
// ********************************************************************************************************************************
{
        return a_fd_data->m_open_files.find(a_fd);
}

// ********************************************************************************************************************************
static kern_return_t fdesc_dup2_internal(fd_context_t a_fd_data, int a_oldfd, int a_newfd, fd_t *a_fdout = nullptr)
// ********************************************************************************************************************************
{
        if ((a_oldfd < 0) || (a_oldfd >= static_cast<int>(FILEMAX)))
        {
                return KERN_FAIL(EBADF);
        }

        fd_t oldfd = fd_find_open(a_fd_data, a_oldfd);

        if (oldfd == nullptr)
        {
                return KERN_FAIL(EBADF);
        }

        if ((a_newfd < 0) || (a_newfd >= static_cast<int>(FILEMAX)))
        {
                return KERN_FAIL(EBADF);
        }

        if (a_oldfd == a_newfd)
        {
                if (a_fdout)
                {
                        *a_fdout = oldfd;
                }

                return oldfd->m_id;
        }

        fd_t newfd = fd_find_open(a_fd_data, a_newfd);

        if (newfd != nullptr)
        {
                vfs_file_rel(newfd->m_file);
        }
        else
        {
                newfd = fd_alloc(a_fd_data, a_newfd);
        }

        newfd->m_file     = oldfd->m_file;
        newfd->m_fd_flags = oldfd->m_fd_flags & ~FD_CLOEXEC;
        // newfd->m_open_flags = oldfd->m_open_flags & ~O_CLOEXEC; // Is this correct?
        vfs_file_ref(newfd->m_file);

        if (a_fdout)
        {
                *a_fdout = newfd;
        }

        return newfd->m_id;
}

// ********************************************************************************************************************************
kern_return_t fd_context_alloc(fd_context_t *a_fd_context_out)
// ********************************************************************************************************************************
{
        fd_context_t fdd = s_fd_context_zone.calloc();
        fdd->m_open_files.init();
        fdd->m_free_ids.init();
        fdd->m_refcnt = 1;
        lock_init(&fdd->m_lock, true);

        fdd->m_free_ids_all.m_id_next = FILEMAX;
        fdd->m_free_ids.insert(&fdd->m_free_ids_all);

        *a_fd_context_out = fdd;
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t fd_context_get_file(fd_context_t a_fd_data, int a_fd, vfs_file_t *a_file_out)
// ********************************************************************************************************************************
{
        LOCK_READ_SCOPE(&a_fd_data->m_lock);

        kern_return_t retval = KERN_FAIL(EBADF);

        fd_t fd = fd_find_open(a_fd_data, a_fd);

        if (fd != nullptr)
        {
                vfs_file_ref(*a_file_out = fd->m_file);
                retval = KERN_SUCCESS;

                // if (fd->m_file->f_type != DTYPE_VNODE) // Why?
                //{
                //        return KERN_FAIL(EINVAL);
                //}
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t fd_context_dup(fd_context_t a_fd_data, int a_fd)
// ********************************************************************************************************************************
{
        LOCK_WRITE_SCOPE(&a_fd_data->m_lock);

        if ((a_fd < 0) || (a_fd >= static_cast<int>(FILEMAX)))
        {
                return KERN_FAIL(EBADF);
        }

        fd_t oldfd = fd_find_open(a_fd_data, a_fd);

        if (oldfd == nullptr)
        {
                return KERN_FAIL(EBADF);
        }

        fd_t newfd = fd_alloc(a_fd_data);

        newfd->m_file     = oldfd->m_file;
        newfd->m_fd_flags = oldfd->m_fd_flags & ~FD_CLOEXEC;
        // newfd->m_open_flags = oldfd->m_open_flags & ~O_CLOEXEC; // Is this correct?
        vfs_file_ref(newfd->m_file);

        return newfd->m_id;
}

// ********************************************************************************************************************************
kern_return_t fd_context_dup2(fd_context_t a_fd_data, int a_oldfd, int a_newfd)
// ********************************************************************************************************************************
{
        LOCK_WRITE_SCOPE(&a_fd_data->m_lock);
        return fdesc_dup2_internal(a_fd_data, a_oldfd, a_newfd);
}

// ********************************************************************************************************************************
kern_return_t fd_context_dup3(fd_context_t a_fd_data, int a_oldfd, int a_newfd, int a_flags)
// ********************************************************************************************************************************
{
        LOCK_WRITE_SCOPE(&a_fd_data->m_lock);
        fd_t newfd;
        kern_return_t retval = fdesc_dup2_internal(a_fd_data, a_oldfd, a_newfd, &newfd);

        if (retval >= KERN_SUCCESS)
        {
                if (a_flags & O_CLOEXEC)
                {
                        // newfd->m_open_flags |= O_CLOEXEC;
                        newfd->m_fd_flags |= FD_CLOEXEC;
                }
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t fd_context_exec(fd_context_t a_fd_data)
// ********************************************************************************************************************************
{
        LOCK_WRITE_SCOPE(&a_fd_data->m_lock);

        for (fd_t fd = a_fd_data->m_open_files.first(); fd != nullptr;)
        {
                fd_t nextfd = fd_open_map::next(fd);
                if (fd->m_fd_flags & FD_CLOEXEC)
                {
                        vfs_file_rel(fd->m_file);
                        fd_free(a_fd_data, fd);
                }
                fd = nextfd;
        }

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t fd_context_clone(fd_context_t a_fd_data, fd_context_t *a_fd_context_out)
// ********************************************************************************************************************************
{
        LOCK_READ_SCOPE(&a_fd_data->m_lock);

        fd_context_t fdd;
        fd_context_alloc(&fdd);

        for (fd_t fd = a_fd_data->m_open_files.first(); fd != nullptr; fd = fd_open_map::next(fd))
        {
                fd_t newfd        = fd_alloc(fdd, fd->m_id);
                newfd->m_file     = fd->m_file;
                newfd->m_fd_flags = fd->m_fd_flags;
                // newfd->m_open_flags = fd->m_open_flags;
                vfs_file_ref(newfd->m_file);
        }
        *a_fd_context_out = fdd;

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t fd_context_fdalloc(pthread_t a_pthread, vfs_file_t a_vfsf)
// ********************************************************************************************************************************
{
        return fd_context_fdalloc(a_pthread->m_fdctx, a_vfsf);
}

// ********************************************************************************************************************************
kern_return_t fd_context_fdalloc(fd_context_t a_fd_data, vfs_file_t a_vfsf)
// ********************************************************************************************************************************
{
        LOCK_WRITE_SCOPE(&a_fd_data->m_lock);

        fd_t fd = fd_alloc(a_fd_data);

        if (fd == nullptr)
        {
                return KERN_FAIL(EMFILE);
        }

        vfs_file_ref(fd->m_file = a_vfsf);

        // fd->m_file->f_count++;

        // fd->m_open_flags = a_oflags;

        if (a_vfsf->m_oflags & O_CLOEXEC)
        {
                fd->m_fd_flags = FD_CLOEXEC;
        }

        return fd->m_id;
}

// ********************************************************************************************************************************
kern_return_t fd_context_fdclose(fd_context_t a_fd_data, int a_fd)
// ********************************************************************************************************************************
{
        LOCK_WRITE_SCOPE(&a_fd_data->m_lock);

        fd_t fd = fd_find_open(a_fd_data, a_fd);

        if (fd == nullptr)
        {
                return KERN_FAIL(EBADF);
        }

        vfs_file_remove_lock(fd->m_file);

        vfs_file_rel(fd->m_file);

        fd_free(a_fd_data, fd);

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
void fd_context_reference(fd_context_t a_fd_data)
// ********************************************************************************************************************************
{
        LOCK_READ_SCOPE(&a_fd_data->m_lock);
        a_fd_data->m_refcnt++;
}

// ********************************************************************************************************************************
void fd_context_deallocate(fd_context_t a_fd_data)
// ********************************************************************************************************************************
{
        bool do_free = false;

        {
                LOCK_WRITE_SCOPE(&a_fd_data->m_lock);

                do_free = --a_fd_data->m_refcnt == 0;

                if (do_free)
                {
                        for (fd_t fd = a_fd_data->m_open_files.first(); fd != nullptr;)
                        {
                                fd_t nextfd = fd_open_map::next(fd);
                                vfs_file_remove_lock(fd->m_file);
                                vfs_file_rel(fd->m_file);
                                fd_free(a_fd_data, fd);
                                fd = nextfd;
                        }
                }
        }

        if (do_free)
        {
                s_fd_context_zone.free(a_fd_data);
        }
}

// ********************************************************************************************************************************
kern_return_t fd_context_exit(fd_context_t a_fd_data)
// ********************************************************************************************************************************
{
        fd_context_deallocate(a_fd_data);
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
// Return a new file descriptor which shall be allocated as described in File Descriptor Allocation, except that it shall be
// the lowest numbered available file descriptor greater than or equal to the third argument, arg, taken as an integer of
// type int. The new file descriptor shall refer to the same open file description as the original file descriptor, and
// shall share any locks. The FD_CLOEXEC flag associated with the new file descriptor shall be cleared to keep the file open
// across calls to one of the exec functions.
// ********************************************************************************************************************************
static kern_return_t f_dupfd(fd_context_t a_fd_data, fd_t a_fd, unsigned long a_arg)
// ********************************************************************************************************************************
{
        if ((static_cast<int>(a_arg) < 0) || (a_arg >= FILEMAX))
        {
                return KERN_FAIL(EINVAL);
        }

        fd_t newfd        = fd_alloc(a_fd_data, static_cast<decltype(a_fd->m_id)>(a_arg));
        newfd->m_file     = a_fd->m_file;
        newfd->m_fd_flags = a_fd->m_fd_flags & ~FD_CLOEXEC;
        newfd->m_file     = a_fd->m_file;
        // newfd->m_open_flags = a_fd->m_open_flags & ~O_CLOEXEC;
        vfs_file_ref(newfd->m_file);

        return newfd->m_id;
}

// ********************************************************************************************************************************
// Get the file descriptor flags defined in <fcntl.h> that are associated with the file descriptor fildes. File
// descriptor flags are associated with a single file descriptor and do not affect other file descriptors that refer
// to the same file.
// ********************************************************************************************************************************
static kern_return_t f_getfd(fd_context_t, fd_t a_fd, unsigned long)
// ********************************************************************************************************************************
{
        return a_fd->m_fd_flags;
}

// ********************************************************************************************************************************
// Set the file descriptor flags defined in <fcntl.h>, that are associated with fildes, to the third argument, arg, taken as
// type int. If the FD_CLOEXEC flag in the third argument is 0, the file descriptor shall remain open across the exec
// functions; otherwise, the file descriptor shall be closed upon successful execution of one of the exec functions.
// ********************************************************************************************************************************
static kern_return_t f_setfd(fd_context_t, fd_t a_fd, unsigned long a_arg)
// ********************************************************************************************************************************
{
        a_fd->m_fd_flags = static_cast<decltype(a_fd->m_fd_flags)>(a_arg) & FD_CLOEXEC;

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
// Like F_DUPFD, but the FD_CLOEXEC flag associated with the new file descriptor shall be set.
// ********************************************************************************************************************************
static kern_return_t f_dupfd_cloexec(fd_context_t a_fd_data, fd_t a_fd, unsigned long a_arg)
// ********************************************************************************************************************************
{
        if ((static_cast<int>(a_arg) < 0) || (a_arg >= FILEMAX))
        {
                return KERN_FAIL(EINVAL);
        }

        fd_t newfd        = fd_alloc(a_fd_data, static_cast<decltype(a_fd->m_id)>(a_arg));
        newfd->m_file     = a_fd->m_file;
        newfd->m_fd_flags = a_fd->m_fd_flags | FD_CLOEXEC;
        newfd->m_file     = a_fd->m_file;
        // newfd->m_open_flags = a_fd->m_open_flags | O_CLOEXEC;
        vfs_file_ref(newfd->m_file);

        return newfd->m_id;
}

// ********************************************************************************************************************************
// Get the file status flags and file access modes, defined in <fcntl.h>, for the file description associated with fildes.
// The file access modes can be extracted from the return value using the mask O_ACCMODE, which is defined in <fcntl.h>.
// File status flags and file access modes are associated with the file description and do not affect other file descriptors
// that refer to the same file with different open file descriptions. The flags returned may include non-standard file
// status flags which the application did not set, provided that these additional flags do not alter the behavior of a
// conforming application.
// ********************************************************************************************************************************
static kern_return_t f_getfl(fd_context_t, fd_t a_fd, unsigned long)
// ********************************************************************************************************************************
{
        return (kern_return_t)a_fd->m_file->m_oflags;
}

// ********************************************************************************************************************************
// Set the file status flags, defined in <fcntl.h>, for the file description associated with fildes from the corresponding
// bits in the third argument, arg, taken as type int. Bits corresponding to the file access mode and the file creation
// flags, as defined in <fcntl.h>, that are set in arg shall be ignored. If any bits in arg other than those mentioned here
// are changed by the application, the result is unspecified. If fildes does not support non-blocking operations, it is
// unspecified whether the O_NONBLOCK flag will be ignored.
// File access mode (O_RDONLY, O_WRONLY, O_RDWR) and file creation flags (i.e., O_CREAT, O_EXCL, O_NOCTTY, O_TRUNC) in arg are
// ignored.
// On Linux, this command can change only the O_APPEND, O_ASYNC, O_DIRECT, O_NOATIME, and O_NONBLOCK flags. It is not
// possible to change the O_DSYNC and O_SYNC flags
// ********************************************************************************************************************************
static kern_return_t f_setfl(fd_context_t, fd_t a_fd, unsigned long a_oflags)
// ********************************************************************************************************************************
{
        a_oflags &= (O_APPEND | O_ASYNC | O_DIRECT | O_NOATIME | O_NONBLOCK);

        if (a_oflags == 0)
        {
                return KERN_SUCCESS;
        }

        KASSERT((a_oflags & ~(O_APPEND | O_DIRECT | O_NONBLOCK)) == 0);

        a_fd->m_file->m_oflags |= static_cast<integer_t>(a_oflags);

        return KERN_SUCCESS;
#if 0
                fp->f_flag &= ~FCNTLFLAGS;
                fp->f_flag |= ((int)arg) & FCNTLFLAGS;
                tmp   = fp->f_flag & FNONBLOCK;
                error = fp->ioctl(FIONBIO, (char *)&tmp, p);
                if (error)
                {
                        return (error);
                }
                tmp   = fp->f_flag & FASYNC;
                error = fp->ioctl(FIOASYNC, (char *)&tmp, p);
                if (!error)
                {
                        return (0);
                }
                fp->f_flag &= ~FNONBLOCK;
                tmp = 0;
                fp->ioctl(FIONBIO, (char *)&tmp, p);
                return (error);
#endif
        panic("DOES NOT WORK");
        return 0;
}

// ********************************************************************************************************************************
// If fildes refers to a socket, get the process ID or process group ID specified to receive SIGURG signals when out-of-band
// data is available. Positive values shall indicate a process ID; negative values, other than -1, shall indicate a process
// group ID; the value zero shall indicate that no SIGURG signals are to be sent. If fildes does not refer to a socket, the
// results are unspecified.
// ********************************************************************************************************************************
static kern_return_t f_getown(fd_context_t, fd_t, unsigned long)
// ********************************************************************************************************************************
{
        panic("DOES NOT WORK");

#if 0
        if (a_fd->m_file->f_type == DTYPE_SOCKET)
        {
                panic("ss");
                // return ((struct socket *)fp->f_data)->so_pgid;
        }

        pid_t pgid;

        kern_return_t retval = a_fd->m_file->ioctl(TIOCGPGRP, &pgid, nullptr);

        if (retval != KERN_SUCCESS)
        {
                return retval;
        }

        return pgid;
#endif
        return 0;
}

// ********************************************************************************************************************************
// If fildes refers to a socket, set the process ID or process group ID specified to receive SIGURG signals when out-of-band
// data is available, using the value of the third argument, arg, taken as type int. Positive values shall indicate a
// process ID; negative values, other than -1, shall indicate a process group ID; the value zero shall indicate that no
// SIGURG signals are to be sent. Each time a SIGURG signal is sent to the specified process or process group, permission
// checks equivalent to those performed by kill() shall be performed, as if kill() were called by a process with the same
// real user ID, effective user ID, and privileges that the process calling fcntl() has at the time of the call; if the
// kill() call would fail, no signal shall be sent. These permission checks may also be performed by the fcntl() call. If
// the process specified by arg later terminates, or the process group specified by arg later becomes empty, while still
// being specified to receive SIGURG signals when out-of-band data is available from fildes, then no signals shall be sent
// to any subsequently created process that has the same process ID or process group ID, regardless of permission; it is
// unspecified whether this is achieved by the equivalent of a fcntl(fildes, F_SETOWN, 0) call at the time the process
// terminates or is waited for or the process group becomes empty, or by other means. If fildes does not refer to a socket,
// the results are unspecified.
// ********************************************************************************************************************************
static kern_return_t f_setown(fd_context_t, fd_t, unsigned long)
// ********************************************************************************************************************************
{
        panic("DOES NOT WORK");

#if 0
        if (f->f_type == DTYPE_SOCKET)
        {
                //((struct socket *)fp->f_data)->so_pgid = (long)arg;
                panic("fgg");
                return (0);
        }

        if ((long)arg <= 0)
        {
                arg = (unsigned long)(-(long)arg);
        }
        else
        {

                        pthread_t p1 = &process::find((long)arg)->thread;
                        if (p1 == 0)
                                return -ESRCH;

                        arg = (unsigned long)((long)p1->process->p_pgrp->pg_id);
                panic("DOES NOT WORK");
        }

        return f->ioctl(TIOCSPGRP, &arg, nullptr);
#endif

        return 0;
}

// ********************************************************************************************************************************
// Get any lock which blocks the lock description pointed to by the third argument, arg, taken as a pointer to type struct
// flock, defined in <fcntl.h>. The information retrieved shall overwrite the information passed to fcntl() in the structure
// flock. If no lock is found that would prevent this lock from being created, then the structure shall be left unchanged
// except for the lock type which shall be set to F_UNLCK.
// ********************************************************************************************************************************
static kern_return_t f_setlkw(fd_context_t, fd_t, unsigned long arg)
// ********************************************************************************************************************************
{
        flock lk;

        kern_return_t retval = emerixx::uio::copyin(reinterpret_cast<flock *>(arg), &lk);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

#if 0
                flg |= F_WAIT;
#endif
        panic("DOES NOT WORK");
        /* Fall into F_SETLK */

#if 0
                // if (fp->f_type != DTYPE_VNODE)
                //{
                //        return -EBADF;
                //}
                vnode::locked_ptr vp{vnode::get_from_file(fp).get_raw()};
                if (vp.is_null())
                {
                        return -EBADF;
                }

                /* Copy in the lock structure */
                error = copyin((void *)arg, &fl, sizeof(fl));

                if (error)
                {
                        return (error);
                }

                if (fl.l_whence == SEEK_CUR)
                {
                        fl.l_start += fp->f_offset;
                }
                switch (fl.l_type)
                {
                case F_RDLCK:
                        if ((fp->f_flag & FREAD) == 0)
                        {
                                return -EBADF;
                        }

                        p->process->p_flag |= P_ADVLOCK;
                        log_debug("below");
                        // return vp->advlock((char *)p, F_SETLK, &fl, flg);
                        return 0;

                case F_WRLCK:
                        if ((fp->f_flag & FWRITE) == 0)
                        {
                                return -EBADF;
                        }

                        p->process->p_flag |= P_ADVLOCK;
                        log_debug("below");
                        return 0;
                        // return vp->advlock((char *)p, F_SETLK, &fl, flg);

                case F_UNLCK:
                        log_debug("below");
                        return 0;
                        // return vp->advlock((char *)p, F_UNLCK, &fl, F_POSIX);

                default:
                        return -EINVAL;
                }
#endif
        panic("DOES NOT WORK");
        return 0;
}

// ********************************************************************************************************************************
// Set or clear a file segment lock according to the lock description pointed to by the third argument, arg, taken as a
// pointer to type struct flock, defined in <fcntl.h>. F_SETLK can establish shared (or read) locks (F_RDLCK) or exclusive
// (or write) locks (F_WRLCK), as well as to remove either type of lock (F_UNLCK). F_RDLCK, F_WRLCK, and F_UNLCK are defined
// in <fcntl.h>. If a shared or exclusive lock cannot be set, fcntl() shall return immediately with a return value of -1.
// ********************************************************************************************************************************
static kern_return_t f_setlk(fd_context_t, fd_t, unsigned long arg)
// ********************************************************************************************************************************
{
        flock lk;

        kern_return_t retval = emerixx::uio::copyin(reinterpret_cast<flock *>(arg), &lk);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

#if 0
                // if (fp->f_type != DTYPE_VNODE)
                //{
                //        return -EBADF;
                //}
                vnode::locked_ptr vp{vnode::get_from_file(fp).get_raw()};
                if (vp.is_null())
                {
                        return -EBADF;
                }

                /* Copy in the lock structure */
                error = copyin((void *)arg, &fl, sizeof(fl));

                if (error)
                {
                        return (error);
                }

                if (fl.l_whence == SEEK_CUR)
                {
                        fl.l_start += fp->f_offset;
                }
                switch (fl.l_type)
                {
                case F_RDLCK:
                        if ((fp->f_flag & FREAD) == 0)
                        {
                                return -EBADF;
                        }

                        p->process->p_flag |= P_ADVLOCK;
                        log_debug("below");
                        // return vp->advlock((char *)p, F_SETLK, &fl, flg);
                        return 0;

                case F_WRLCK:
                        if ((fp->f_flag & FWRITE) == 0)
                        {
                                return -EBADF;
                        }

                        p->process->p_flag |= P_ADVLOCK;
                        log_debug("below");
                        return 0;
                        // return vp->advlock((char *)p, F_SETLK, &fl, flg);

                case F_UNLCK:
                        log_debug("below");
                        return 0;
                        // return vp->advlock((char *)p, F_UNLCK, &fl, F_POSIX);

                default:
                        return -EINVAL;
                }
#endif
        log_error("does not work");
        return KERN_FAIL(EINVAL);
}

// ********************************************************************************************************************************
// Get any lock which blocks the lock description pointed to by the third argument, arg, taken as a pointer to type struct
// flock, defined in <fcntl.h>. The information retrieved shall overwrite the information passed to fcntl() in the structure
// flock. If no lock is found that would prevent this lock from being created, then the structure shall be left unchanged
// except for the lock type which shall be set to F_UNLCK.
// ********************************************************************************************************************************
static kern_return_t f_getlk(fd_context_t, fd_t, unsigned long arg)
// ********************************************************************************************************************************
{
        flock lk;

        kern_return_t retval = emerixx::uio::copyin(reinterpret_cast<flock *>(arg), &lk);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        // if (fp->f_type != DTYPE_VNODE)
        //{
        //        return (-EBADF);
        //}
#if 0
                vnode::locked_ptr vp{vnode::get_from_file(fp).get_raw()};

                if (vp.is_null())
                {
                        return -EBADF;
                }

                /* Copy in the lock structure */
                error = copyin((void *)arg, &fl, sizeof(fl));
                if (error)
                {
                        return (error);
                }

                if (fl.l_whence == SEEK_CUR)
                {
                        fl.l_start += fp->f_offset;
                }
                log_debug("below");
#if 0
                if (error = vp->advlock((char *)p, F_GETLK, &fl, F_POSIX))
                {
                        return (error);
                }
#endif
                return copyout(&fl, (void *)arg, sizeof(fl));
#endif
        log_error("DOES NOT WORK");
        return KERN_FAIL(EINVAL);
}

// ********************************************************************************************************************************
static kern_return_t f_getsig(fd_context_t, fd_t, unsigned long)
// ********************************************************************************************************************************
{
        KASSERT(0);
        return 0;
}

static kern_return_t f_setsig(fd_context_t, fd_t, unsigned long)
{
        KASSERT(0);
        return 0;
}

static kern_return_t f_setown_ex(fd_context_t, fd_t, unsigned long)
{
        KASSERT(0);
        return 0;
}

static kern_return_t f_getown_ex(fd_context_t, fd_t, unsigned long)
{
        KASSERT(0);
        return 0;
}

static kern_return_t f_getowner_uids(fd_context_t, fd_t, unsigned long)
{
        KASSERT(0);
        return 0;
}

static kern_return_t f_ofd_getlk(fd_context_t, fd_t, unsigned long)
{
        KASSERT(0);
        return 0;
}

static kern_return_t f_ofd_setlk(fd_context_t, fd_t, unsigned long)
{
        KASSERT(0);
        return 0;
}

static kern_return_t f_ofd_setlkw(fd_context_t, fd_t, unsigned long)
{
        KASSERT(0);
        return 0;
}

static kern_return_t f_setlease(fd_context_t, fd_t, unsigned long)
{
        KASSERT(0);
        return 0;
}

static kern_return_t f_getlease(fd_context_t, fd_t, unsigned long)
{
        KASSERT(0);
        return 0;
}

static kern_return_t f_notify(fd_context_t, fd_t, unsigned long)
{
        KASSERT(0);
        return 0;
}

static kern_return_t f_cancellk(fd_context_t, fd_t, unsigned long)
{
        KASSERT(0);
        return 0;
}

static kern_return_t f_setpipe_sz(fd_context_t, fd_t, unsigned long)
{
        KASSERT(0);
        return 0;
}

static kern_return_t f_getpipe_sz(fd_context_t, fd_t, unsigned long)
{
        KASSERT(0);
        return 0;
}

static kern_return_t f_add_seals(fd_context_t, fd_t, unsigned long)
{
        KASSERT(0);
        return 0;
}

static kern_return_t f_get_seals(fd_context_t, fd_t, unsigned long)
{
        KASSERT(0);
        return 0;
}

static kern_return_t f_get_rw_hint(fd_context_t, fd_t, unsigned long)
{
        KASSERT(0);
        return 0;
}

static kern_return_t f_set_rw_hint(fd_context_t, fd_t, unsigned long)
{
        KASSERT(0);
        return 0;
}

static kern_return_t f_get_file_rw_hint(fd_context_t, fd_t, unsigned long)
{
        KASSERT(0);
        return 0;
}

static kern_return_t f_set_file_rw_hint(fd_context_t, fd_t, unsigned long)
{
        KASSERT(0);
        return 0;
}

static kern_return_t (*const s_fcntl_ops[][48])(fd_context_t, fd_t, unsigned long) = {
  // 0 - 1024
        {
         f_dupfd,         // F_DUPFD         - 0
                f_getfd,         // F_GETFD         - 1
                f_setfd,         // F_SETFD         - 2
                f_getfl,         // F_GETFL         - 3
                f_setfl,         // F_SETFL         - 4
                f_getlk,         // F_GETLK         - 5
                f_setlk,         // F_SETLK         - 6
                f_setlkw,        // F_SETLKW        - 7
                f_setown,        // F_SETOWN        - 8
                f_getown,        // F_GETOWN        - 9
                f_setsig,        // F_SETSIG        - 10
                f_getsig,        // F_GETSIG        - 11
                nullptr,         // F_UNSUED        - 12
                nullptr,         // F_UNSUED        - 13
                nullptr,         // F_UNSUED        - 14
                f_setown_ex,     // F_SETOWN_EX     - 15
                f_getown_ex,     // F_SETOWN_EX     - 16
                f_getowner_uids, // F_GETOWNER_UIDS - 17
                nullptr,         // F_UNSUED        - 18
                nullptr,         // F_UNSUED        - 19
                nullptr,         // F_UNSUED        - 20
                nullptr,         // F_UNSUED        - 21
                nullptr,         // F_UNSUED        - 22
                nullptr,         // F_UNSUED        - 23
                nullptr,         // F_UNSUED        - 24
                nullptr,         // F_UNSUED        - 25
                nullptr,         // F_UNSUED        - 26
                nullptr,         // F_UNSUED        - 27
                nullptr,         // F_UNSUED        - 28
                nullptr,         // F_UNSUED        - 29
                nullptr,         // F_UNSUED        - 30
                nullptr,         // F_UNSUED        - 31
                nullptr,         // F_UNSUED        - 32
                nullptr,         // F_UNSUED        - 33
                nullptr,         // F_UNSUED        - 34
                nullptr,         // F_UNSUED        - 35
                f_ofd_getlk,     // F_UNSUED        - 33
                f_ofd_setlk,     // F_UNSUED        - 34
                f_ofd_setlkw     // F_UNSUED        - 35
        },
 // 1024 - 2048
        {
         f_setlease,         // F_SETLEASE         - 1024
                f_getlease,         // F_GETLEASE         - 1025
                f_notify,           // F_NOTIFY           - 1026
                nullptr,            // F_UNSUED           - 1027
                nullptr,            // F_UNSUED           - 1028
                f_cancellk,         // F_CANCELLK         - 1029
                f_dupfd_cloexec,    // F_DUPFD_CLOEXEC    - 1030
                f_setpipe_sz,       // F_GETPIPE_SZ       - 1031
                f_getpipe_sz,       // F_GETPIPE_SZ       - 1032
                f_add_seals,        // F_ADD_SEALS        - 1033
                f_get_seals,        // F_GET_SEALS        - 1034
                f_get_rw_hint,      // F_GET_RW_HINT      - 1035
                f_set_rw_hint,      // F_SET_RW_HINT      - 1036
                f_get_file_rw_hint, // F_GET_FILE_RW_HINT - 1037
                f_set_file_rw_hint  // F_SET_FILE_RW_HINT - 1038
        }
  // end
};

// ********************************************************************************************************************************
kern_return_t fd_context_fcntl(fd_context_t a_fd_data, int a_fd, unsigned a_cmd, unsigned long a_arg)
// ********************************************************************************************************************************
{
        unsigned cmdgrp = (a_cmd & ~(1024 - 1)) >> 10;
        unsigned cmdno  = (a_cmd & (1024 - 1));

        if ((cmdgrp >= ARRAY_SIZE(s_fcntl_ops)) || (cmdno >= ARRAY_SIZE(*s_fcntl_ops)) || (s_fcntl_ops[cmdgrp][cmdno] == nullptr))
        {
                return KERN_FAIL(EINVAL);
        }

        LOCK_WRITE_SCOPE(&a_fd_data->m_lock);

        fd_t fd = fd_find_open(a_fd_data, a_fd);

        if (fd == nullptr)
        {
                return KERN_FAIL(EINVAL);
        }

        return s_fcntl_ops[cmdgrp][cmdno](a_fd_data, fd, a_arg);
}
