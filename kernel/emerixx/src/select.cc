#include <sys/select.h>

#include <aux/time.h>
#include <aux/defer.hh>

#include <kernel/zalloc.h>

#include <emerixx/poll.h>

static int s_select_poll_flags[] = //
        { POLLIN | POLLRDNORM,     //
          POLLOUT | POLLWRNORM,    //
          POLLPRI };

struct pollfd_storage_data
{
        pollfd pfds[0x100];
};

ZONE_DEFINE(pollfd_storage_data, s_pollfd_storage, 0x100, 0x100 / 2, ZONE_EXHAUSTIBLE, 0x20);

kern_return_t pselect(int a_nfds,
                      fd_set *__restrict a_rfds,
                      fd_set *__restrict a_wfds,
                      fd_set *__restrict a_efds,
                      const struct timespec *__restrict a_ts,
                      const sigset_t *__restrict a_mask)
{
        fd_set *fd_sets[3] = { a_rfds, a_wfds, a_efds };

        if (a_nfds > 0x100)
        {
                return KERN_FAIL(ENOMEM);
        }

        pollfd *pfds = (pollfd *)s_pollfd_storage.alloc();
        defer(s_pollfd_storage.free((pollfd_storage_data *)pfds));

        int pfds_cnt = 0;
        for (int fd = 0; fd < a_nfds; ++fd)
        {
                short events = 0;
                for (size_t i = 0; i < 3; ++i)
                {
                        if ((fd_sets[i] != nullptr) && FD_ISSET(fd, fd_sets[i]))
                        {
                                events |= (short)s_select_poll_flags[i];
                        }
                }

                if (events != 0)
                {
                        pfds[pfds_cnt++] = { fd, events, 0 };
                }
        }

        kern_return_t retval = ppoll(pfds, pfds_cnt, a_ts, a_mask);

        if (retval < 0)
        {
                return retval;
        }

        a_nfds = pfds_cnt;
        for (int i = 0; i < a_nfds; ++i)
        {
                if (pfds[i].revents & POLLNVAL)
                {
                        return KERN_FAIL(EBADF);
                }
        }

        retval = 0;

        for (int j = 0; j < a_nfds; ++j)
        {
                for (size_t i = 0; i < 3; ++i)
                {
                        if (fd_sets[i] != nullptr)
                        {
                                if ((pfds[j].revents & (pfds[j].events & s_select_poll_flags[i])))
                                {
                                        retval++;
                                }
                                else
                                {
                                        FD_CLR(pfds[j].fd, fd_sets[i]);
                                }
                        }
                }
        }

        return retval;
}

kern_return_t select(int a_nfds, //
                     fd_set *__restrict a_rfds,
                     fd_set *__restrict a_wfds,
                     fd_set *__restrict a_efds,
                     struct timeval *__restrict a_tv)
{
        timespec tmp, *ts = nullptr;

        if (a_tv != nullptr)
        {
                ts = &tmp;
                TIMEVAL_TO_TIMESPEC(a_tv, ts);
        }

        return pselect(a_nfds, a_rfds, a_wfds, a_efds, ts, nullptr);
}
