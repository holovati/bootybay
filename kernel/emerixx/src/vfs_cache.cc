#define RBTNODE_FUNCTION_DEFINITIONS

#include <string.h>

#include <etl/queue.h>
#include <etl/algorithm.hh>

#include "vfs_cache.h"

typedef struct vfs_cache_cnp
{
    char const *cn;
    unsigned char cn_len;
    char __pad [sizeof(natural_t) - 1];
} * vfs_cache_cnp_t;

template <> 
// ********************************************************************************************************************************
vfs_cache_link_t rbtnode_to_type<vfs_cache_link_t>(struct rbtnode_data *a_rbtnode)
// ********************************************************************************************************************************
{
        return rbtnode_type(a_rbtnode, vfs_cache_link_data, m_rblink);
}

template <> [[maybe_unused]]
// ********************************************************************************************************************************
struct rbtnode_data *type_to_rbtnode<vfs_cache_link_t>(vfs_cache_link_t a_vfscl)
// ********************************************************************************************************************************
{
        return &a_vfscl->m_rblink;
}

template <>
// ********************************************************************************************************************************
bool rbtnode_is_less<vfs_cache_cnp_t, vfs_cache_link_t>(vfs_cache_cnp_t a_lhs, vfs_cache_link_t a_rhs)
// ********************************************************************************************************************************
{
        return strncmp(a_lhs->cn, a_rhs->m_path, etl::min(a_rhs->m_plen, a_lhs->cn_len)) < 0;
}

template <>
// ********************************************************************************************************************************
bool rbtnode_is_less<vfs_cache_link_t, vfs_cache_cnp_t>(vfs_cache_link_t a_lhs, vfs_cache_cnp_t a_rhs)
// ********************************************************************************************************************************
{
        return strncmp(a_lhs->m_path, a_rhs->cn, etl::min(a_lhs->m_plen, a_rhs->cn_len)) < 0;
}

template <> [[maybe_unused]]
// ********************************************************************************************************************************
bool rbtnode_is_less<vfs_cache_link_t, vfs_cache_link_t>(vfs_cache_link_t a_lhs, vfs_cache_link_t a_rhs)
// ********************************************************************************************************************************
{
        return strncmp(a_lhs->m_path, a_rhs->m_path, etl::min(a_lhs->m_plen, a_rhs->m_plen)) < 0;
}

// ********************************************************************************************************************************
void vfs_cache_init(vfs_cache_t a_vfsc)
// ********************************************************************************************************************************
{
    a_vfsc->m_root = rbtnode_init_sentinel(&a_vfsc->m_sentinel);
}

// ********************************************************************************************************************************
vfs_cache_link_t vfs_cache_lookup(vfs_cache_t a_vfsc, char const *a_cn, natural_t a_cnl)
// ********************************************************************************************************************************
{
    vfs_cache_link_t vfscl = nullptr;

    if (a_cnl < 255)
    {
        struct vfs_cache_cnp cnp_tuple = { .cn = a_cn, .cn_len = (unsigned char)a_cnl };

        vfscl = rbtnode_find<vfs_cache_cnp_t, vfs_cache_link_t>(&cnp_tuple, a_vfsc->m_root);
    }

    return vfscl;
}