#include <aux/init.h>

#include <emerixx/vfs/vfs_node.h>
#include <emerixx/vfs/vfs_mount_data.h>

#include <emerixx/vfs/vfs_mount.h>

static queue_head_t s_allmount_list;

// ********************************************************************************************************************************
kern_return_t vfs_mount_init(vfs_mount_t a_vfsm)
// ********************************************************************************************************************************
{

        a_vfsm->m_ops = nullptr;
        enqueue_tail(&s_allmount_list, &a_vfsm->m_allmount_link);
        a_vfsm->m_vfsn_covered = nullptr;
        a_vfsm->m_vfsn_root    = nullptr;

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t vfs_mount_rootnode(vfs_mount_t a_vfsm, vfs_node_t *a_vfsn_out)
// ********************************************************************************************************************************
{
        if (a_vfsm->m_vfsn_root == nullptr)
        {
                kern_return_t retval = a_vfsm->m_ops->root(a_vfsm, &a_vfsm->m_vfsn_root);

                if (KERN_STATUS_FAILURE(retval))
                {
                        return retval;
                }
        }

        vfs_node_ref(*a_vfsn_out = a_vfsm->m_vfsn_root);

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t vfs_mount_stat(vfs_mount_t a_vfsm, vfs_mount_stat_t a_stat)
// ********************************************************************************************************************************
{
        kern_return_t retval = a_vfsm->m_ops->stat(a_vfsm, a_stat);
        return retval;
}

// ********************************************************************************************************************************
vfs_mount_t vfs_mount_first()
// ********************************************************************************************************************************
{
        vfs_mount_t vfsm = nullptr;

        if (queue_empty(&s_allmount_list) == false)
        {
                vfsm = queue_containing_record(queue_first(&s_allmount_list), struct vfs_mount_data, m_allmount_link);
        }

        return vfsm;
}

// ********************************************************************************************************************************
vfs_mount_t vfs_mount_next(vfs_mount_t a_vfsm)
// ********************************************************************************************************************************
{
        vfs_mount_t vfsm = nullptr;

        if (queue_end(&s_allmount_list, queue_next(&a_vfsm->m_allmount_link)) == false)
        {
                vfsm = queue_containing_record(queue_next(&a_vfsm->m_allmount_link), struct vfs_mount_data, m_allmount_link);
        }

        return vfsm;
}

// ********************************************************************************************************************************
kern_return_t vfs_mount_sync(vfs_mount_t a_mnt, boolean_t a_wait)
// ********************************************************************************************************************************
{
        kern_return_t retval = a_mnt->m_ops->sync(a_mnt, a_wait);

        return retval;
}

// ********************************************************************************************************************************
kern_return_t vfs_mount_covering_vfsn(vfs_mount_t a_mnt, vfs_node_t a_covered_node)
// ********************************************************************************************************************************
{
        vfs_node_ref(a_mnt->m_vfsn_covered = a_covered_node);
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
vfs_node_t vfs_mount_covered_vfsn(vfs_mount_t a_mnt)
// ********************************************************************************************************************************
{
        return a_mnt->m_vfsn_covered;
}

// ********************************************************************************************************************************
kern_return_t vfs_mount_op_sync_noop(vfs_mount_t a_vfsm, boolean_t a_wait)
// ********************************************************************************************************************************
{
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t early_init()
// ********************************************************************************************************************************
{
        queue_init(&s_allmount_list);

        return KERN_SUCCESS;
}

early_initcall(early_init);
