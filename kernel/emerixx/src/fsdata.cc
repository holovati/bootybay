#include <sys/stat.h>

#define __NEED_lock
#include <kernel/zalloc.h>
#include <aux/defer.hh>

#include <emerixx/fsdata.hh>
#include <emerixx/vfs/vfs_node.h>
#include <emerixx/vfs/vfs_node_data.h>

struct fs_context
{
        vfs_node_t fs_cdir;    /* current directory */
        vfs_node_t fs_rdir;    /* root directory */
        mode_t fs_cmask;    /* mask for file creation */
        uint32_t fs_refcnt; /* reference count */
        lock_data_t m_lock;
};

ZONE_DEFINE(fs_context, s_fs_context_zone, 0xFFFFFFFF, PAGE_SIZE / sizeof(fs_context), ZONE_EXHAUSTIBLE, 0x10);

// ********************************************************************************************************************************
static inline fs_context_t fs_context_alloc_internal()
// ********************************************************************************************************************************
{
        fs_context_t fsctx = s_fs_context_zone.alloc();
        fsctx->fs_cdir     = nullptr;
        fsctx->fs_rdir     = nullptr;
        fsctx->fs_cmask    = 0;
        fsctx->fs_refcnt   = 1;
        lock_init(&fsctx->m_lock, true);
        return fsctx;
}

// ********************************************************************************************************************************
static inline void fs_context_free_internal(fs_context_t a_fsctx)
// ********************************************************************************************************************************
{
        KASSERT(a_fsctx->fs_refcnt == 0);
        s_fs_context_zone.free(a_fsctx);
}

// ********************************************************************************************************************************
vfs_node_t fs_context_getcwd(fs_context_t a_fsctx)
// ********************************************************************************************************************************
{
        LOCK_READ_SCOPE(&a_fsctx->m_lock);
        vfs_node_ref(a_fsctx->fs_cdir);
        return a_fsctx->fs_cdir;
}

// ********************************************************************************************************************************
vfs_node_t fs_context_getroot(fs_context_t a_fsctx)
// ********************************************************************************************************************************
{
        LOCK_READ_SCOPE(&a_fsctx->m_lock);
        vfs_node_ref(a_fsctx->fs_rdir);
        return (vfs_node_t)a_fsctx->fs_rdir;
}

// ********************************************************************************************************************************
kern_return_t fs_context_alloc(fs_context_t *a_fsctx_out)
// ********************************************************************************************************************************
{
        *a_fsctx_out = fs_context_alloc_internal();
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t fs_context_dup(fs_context_t a_fsctx_in, fs_context_t *a_fsctx_out)
// ********************************************************************************************************************************
{
        LOCK_WRITE_SCOPE(&a_fsctx_in->m_lock);

        fs_context_t fsctx = fs_context_alloc_internal();

        if (a_fsctx_in->fs_cdir != nullptr)
        {
                vfs_node_ref((fsctx->fs_cdir = a_fsctx_in->fs_cdir));
        }

        if (a_fsctx_in->fs_rdir != nullptr)
        {
                vfs_node_ref((fsctx->fs_rdir = a_fsctx_in->fs_rdir));
        }

        fsctx->fs_cmask = a_fsctx_in->fs_cmask;

        *a_fsctx_out = fsctx;

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
void fs_context_reference(fs_context_t a_fsctx)
// ********************************************************************************************************************************
{
        LOCK_WRITE_SCOPE(&a_fsctx->m_lock);
        a_fsctx->fs_refcnt++;
}

// ********************************************************************************************************************************
mode_t fs_context_cmode(fs_context_t a_fsctx, mode_t a_mode)
// ********************************************************************************************************************************
{
        LOCK_READ_SCOPE(&a_fsctx->m_lock);
        return a_mode & ~(a_fsctx->fs_cmask);
}

// ********************************************************************************************************************************
kern_return_t fs_context_chdir(fs_context_t a_fsctx, vfs_node_t a_new_dir)
// ********************************************************************************************************************************
{
        LOCK_WRITE_SCOPE(&a_fsctx->m_lock);
        //KASSERT(a_new_dir->islocked());

        if (S_ISDIR(a_new_dir->m_mode) == false)
        {
                return KERN_FAIL(ENOTDIR);
        }

        kern_return_t retval = 0;// a_new_dir->access(VEXEC /*, a_proc->process->p_cred->pc_ucred nullptr */);

        if (KERN_STATUS_SUCCESS(retval))
        {
                if (a_fsctx->fs_cdir)
                {
                        vfs_node_rel(a_fsctx->fs_cdir);
                }

                vfs_node_ref(a_fsctx->fs_cdir = a_new_dir);
        }

        return retval;
}

// ********************************************************************************************************************************
mode_t fs_context_umask(fs_context_t a_fsctx, mode_t a_new_mask)
// ********************************************************************************************************************************
{
        LOCK_WRITE_SCOPE(&a_fsctx->m_lock);
        mode_t omsk       = a_fsctx->fs_cmask;
        a_fsctx->fs_cmask = a_new_mask;
        return omsk;
}

// ********************************************************************************************************************************
kern_return_t fs_context_chroot(fs_context_t a_fsctx, vfs_node_t a_vfsn)
// ********************************************************************************************************************************
{
        LOCK_WRITE_SCOPE(&a_fsctx->m_lock);
        vfs_node_ref(a_vfsn);
        a_fsctx->fs_rdir = a_vfsn;
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
void fs_context_deallocate(fs_context_t a_fsctx)
// ********************************************************************************************************************************
{
        lock_write(&a_fsctx->m_lock);

        if (--(a_fsctx->fs_refcnt) == 0)
        {
                lock_write_done(&a_fsctx->m_lock); // No refs, safe to unlock

                if (a_fsctx->fs_cdir != nullptr)
                {
                        vfs_node_rel(a_fsctx->fs_cdir);
                }

                if (a_fsctx->fs_rdir)
                {
                        vfs_node_rel(a_fsctx->fs_rdir);
                }

                fs_context_free_internal(a_fsctx);
        }
        else
        {
                lock_write_done(&a_fsctx->m_lock); // still in use
        }
}