#include <kernel/lock.h>
#include <kernel/zalloc.h>
#include <kernel/vm/vm_map.h>
#include <kernel/vm/vm_kern.h>

#include <etl/bit.hh>

#include <etl/algorithm.hh>

#include <emerixx/sleep.h>
#include <emerixx/memory_rw.h>

#include <emerixx/pipe.h>

#define PIPE_HAS_READER etl::bit<decltype(pipe_data::m_flags), 0>::value
#define PIPE_HAS_WRITER etl::bit<decltype(pipe_data::m_flags), 1>::value

struct pipe_data
{
        lock_data_t m_lock;
        natural_t m_refcnt;
        union
        {
                struct
                {
                        natural_t read;
                        natural_t write;
                } m_pos;
                natural_t m_pos_array[2];
        };
        natural_t m_flags;
        natural_t m_buffer_length;
        vm_address_t m_buffer;
};

ZONE_DEFINE(pipe_data, s_context_zone, 0x100, 0x10, ZONE_EXHAUSTIBLE, 0x20);

static vm_map_t g_pipe_buffer_map;

// ********************************************************************************************************************************
kern_return_t pipe_create(natural_t a_pipe_size, pipe_t *a_pipe_out)
// ********************************************************************************************************************************
{
        if ((a_pipe_size == 0) || (etl::is_power_of_two(a_pipe_size) == false))
        {
                return KERN_FAIL(EINVAL);
        }

        if (s_context_zone.is_exhausted())
        {
                return KERN_FAIL(ENOMEM);
        }

        pipe_t pipe = s_context_zone.alloc();

        lock_init(&pipe->m_lock, true);

        pipe->m_refcnt = 1;

        etl::clear(pipe->m_pos_array);

        pipe->m_flags = PIPE_HAS_READER | PIPE_HAS_WRITER;

        pipe->m_buffer_length = etl::max((natural_t)PAGE_SIZE, etl::round_next_pow2(a_pipe_size));

        vm_map_lock(g_pipe_buffer_map);

        vm_map_entry_t vment = nullptr;

        kern_return_t retval
                = vm_map_find_entry(g_pipe_buffer_map, &pipe->m_buffer, pipe->m_buffer_length << 1, 0, nullptr, &vment);

        if (retval != KERN_SUCCESS)
        {
                retval = KERN_FAIL(ENOMEM);
                goto out;
        }

        vment->flags |= VM_MAP_ENTRY_MIRROR_OBJECT;
        vment->wired_count = 0;

out:
        if (KERN_STATUS_SUCCESS(retval))
        {
                *a_pipe_out = pipe;
        }
        else
        {
                s_context_zone.free(pipe);
        }

        vm_map_unlock(g_pipe_buffer_map);

        return retval;
}

// ********************************************************************************************************************************
void pipe_ref(pipe_t a_pipe)
// ********************************************************************************************************************************
{
        lock_write(&a_pipe->m_lock);
        (a_pipe->m_refcnt)++;
        lock_done(&a_pipe->m_lock);
}

// ********************************************************************************************************************************
void pipe_rel(pipe_t a_pipe)
// ********************************************************************************************************************************
{
        lock_write(&a_pipe->m_lock);
        if (--(a_pipe->m_refcnt))
        {
                lock_done(&a_pipe->m_lock);
                return;
        }

        vm_map_remove(g_pipe_buffer_map, a_pipe->m_buffer, a_pipe->m_buffer + (a_pipe->m_buffer_length << 1));

        s_context_zone.free(a_pipe);
}

// ********************************************************************************************************************************
kern_return_t pipe_rw(pipe_t a_pipe,
                      memory_rdwr_t a_mrw,
                      natural_t a_atomic_count,
                      natural_t a_ms_timeout,
                      boolean_t a_can_wait,
                      natural_t *a_rw_cnt_out)
// ********************************************************************************************************************************
{
        natural_t totxfer = 0;

        boolean_t is_write_op = a_mrw->uio_rw == UIO_WRITE;

        kern_return_t retval = KERN_SUCCESS;

        lock_write(&a_pipe->m_lock);

        natural_t mask = a_pipe->m_buffer_length - 1;
again:

        natural_t buffer_counters[] = {
                ((a_pipe->m_pos.write - a_pipe->m_pos.read) & mask),           // Read count
                ((mask - ((a_pipe->m_pos.write - a_pipe->m_pos.read))) & mask) // Write count
        };

        if (is_write_op == true)
        {
                if ((a_pipe->m_flags & PIPE_HAS_READER) == 0)
                {
                        // No readers, why bother

                        if (totxfer == 0)
                        {
                                retval = KERN_FAIL(EPIPE);
                        }

                        goto out;
                }
        }
        else // is_read_op
        {
                if ((buffer_counters[0] == 0) && ((a_pipe->m_flags & PIPE_HAS_WRITER) == 0))
                {
                        // Read buffer is empty and the pipe will not be written to again
                        goto out;
                }
        }

        if (buffer_counters[is_write_op] > 0)
        {
                if (is_write_op == true)
                {
                        natural_t elm_count = a_mrw->uio_resid;
                        if (elm_count <= a_atomic_count)
                        {
                                // Has to be a atom write
                                if (a_can_wait == true)
                                {
                                        // O_NONBLOCK is NOT set!

                                        if (elm_count < buffer_counters[is_write_op])
                                        {
                                                // We have space, do the write.
                                                goto do_the_write;
                                        }
                                        else
                                        {
                                                // No room for a atomic write, but we are allowed to wait for it
                                                goto do_atomic_write_wait;
                                        }
                                }
                                else
                                {
                                        // O_NONBLOCK IS set!
                                        if (elm_count < buffer_counters[is_write_op])
                                        {
                                                // We have space, do the write.
                                                goto do_the_write;
                                        }
                                        else
                                        {
                                                // No room for a atomic write,and we are not allowed to block, bail with
                                                // EWOULDBLOCK.
                                                goto do_atomic_write_fail;
                                        }
                                }
                        }
                        else
                        {
                                // Does not have to be atomic.(best effort)
                                goto do_the_write;
                        }
                }

        do_the_write:
                natural_t old_resid = a_mrw->uio_resid;

                natural_t op_offset = ((a_pipe->m_pos_array[is_write_op] & mask));
                natural_t op_size   = buffer_counters[is_write_op];

                retval = uiomove((void *)(a_pipe->m_buffer + op_offset), op_size, a_mrw);

                if (KERN_STATUS_SUCCESS(retval))
                {
                        natural_t rwcnt = old_resid - a_mrw->uio_resid;

                        totxfer += rwcnt;

                        a_pipe->m_pos_array[is_write_op] += rwcnt;

                        uthread_wakeup(&a_pipe->m_pos_array[is_write_op]);

                        if (is_write_op && (a_mrw->uio_resid > 0))
                        {
                                goto again;
                        }
                }
        }
        else
        {
                // No space for operation
                if (a_can_wait == true)
                {
                do_atomic_write_wait:
                        // Wait on the opposite side of our operation.
                        // e.g. if we want to write and there is no space, we sleep on the read end.

                        retval = uthread_sleep_timeout(&a_pipe->m_pos_array[!is_write_op], (int)a_ms_timeout, &a_pipe->m_lock);
                        if (KERN_STATUS_SUCCESS(retval))
                        {
                                goto again;
                        }
                }
                else
                {
                do_atomic_write_fail:
                        retval = KERN_FAIL(EWOULDBLOCK);
                }
        }

        // If empty, reset indicies
        if ((is_write_op == false) && ((a_pipe->m_pos.write == a_pipe->m_pos.read) && (a_pipe->m_pos.write > 0)))
        {
                a_pipe->m_pos.write = a_pipe->m_pos.read = 0;
        }

out:
        lock_write_done(&a_pipe->m_lock);

        if (a_rw_cnt_out)
        {
                *a_rw_cnt_out = totxfer;
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t pipe_read(pipe_t a_pipe, void *a_data, natural_t a_data_length, boolean_t a_can_wait)
// ********************************************************************************************************************************
{
        struct iovec iov = { .iov_base = a_data, .iov_len = a_data_length };

        memory_rdwr_data memrw = { .uio_iov    = &iov,
                                   .uio_iovcnt = 1,
                                   .uio_offset = 0,
                                   .uio_resid  = iov.iov_len,
                                   .uio_segflg = UIO_SYSSPACE,
                                   .uio_rw     = UIO_READ,
                                   .aux        = {} };

        natural_t elm_xfer   = 0;
        kern_return_t retval = pipe_rw(a_pipe, &memrw, 1, 0, a_can_wait, &elm_xfer);

        if (KERN_STATUS_SUCCESS(retval))
        {
                retval = (kern_return_t)(elm_xfer);
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t pipe_write(pipe_t a_pipe, void *a_data, natural_t a_data_length, natural_t a_atomic_len, boolean_t a_can_wait)
// ********************************************************************************************************************************
{
        struct iovec iov = { .iov_base = a_data, .iov_len = a_data_length };

        memory_rdwr_data memrw = { .uio_iov    = &iov,
                                   .uio_iovcnt = 1,
                                   .uio_offset = 0,
                                   .uio_resid  = iov.iov_len,
                                   .uio_segflg = UIO_SYSSPACE,
                                   .uio_rw     = UIO_WRITE,
                                   .aux        = {} };

        natural_t elm_xfer   = 0;
        kern_return_t retval = pipe_rw(a_pipe, &memrw, a_atomic_len, 0, a_can_wait, &elm_xfer);

        if (KERN_STATUS_SUCCESS(retval))
        {
                retval = (kern_return_t)(elm_xfer);
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t pipe_read_range(pipe_t a_pipe, boolean_t a_can_wait, pipe_range_callback_t a_cb, void *a_cb_ud)
// ********************************************************************************************************************************
{
        kern_return_t retval = KERN_SUCCESS;

        lock_write(&a_pipe->m_lock);

        natural_t mask = a_pipe->m_buffer_length - 1;
again:

        natural_t rd_count = ((a_pipe->m_pos.write - a_pipe->m_pos.read) & mask);

        if ((rd_count == 0) && ((a_pipe->m_flags & PIPE_HAS_WRITER) == 0))
        {
                // Read buffer is empty and the pipe will not be written to again
                goto out;
        }

        if (rd_count > 0)
        {
                natural_t op_offset = a_pipe->m_pos.read & mask;
                // natural_t op_size   = buffer_counters[is_write_op] * a_pipe->m_elm_size;

                a_pipe->m_pos.read += a_cb((void *)(a_pipe->m_buffer + op_offset), rd_count, a_cb_ud);

                uthread_wakeup(&a_pipe->m_pos.read);
        }
        else
        {
                // No space for operation
                if (a_can_wait == true)
                {
                        // Wait on the opposite side of our operation.
                        // e.g. if we want to write and there is no space, we sleep on the read end.
                        retval = uthread_sleep_timeout(&a_pipe->m_pos.write, 0, &a_pipe->m_lock);
                        if (KERN_STATUS_SUCCESS(retval))
                        {
                                goto again;
                        }
                }
                else
                {
                        retval = KERN_FAIL(EWOULDBLOCK);
                }
        }

        // If empty, reset indicies
        if ((a_pipe->m_pos.write == a_pipe->m_pos.read) && (a_pipe->m_pos.write > 0))
        {
                a_pipe->m_pos.write = a_pipe->m_pos.read = 0;
        }

out:
        lock_write_done(&a_pipe->m_lock);

        return retval;
}

// ********************************************************************************************************************************
kern_return_t pipe_read_peek(pipe_t a_pipe, boolean_t a_can_wait, void *a_data_out)
// ********************************************************************************************************************************
{
        lock_read(&a_pipe->m_lock);

        natural_t mask = a_pipe->m_buffer_length - 1;
again:
        natural_t readcnt = ((a_pipe->m_pos.write - a_pipe->m_pos.read) & mask);

        kern_return_t retval = KERN_SUCCESS;

        if (readcnt > 0)
        {
                natural_t op_offset = a_pipe->m_pos.read & mask;

                *((void **)a_data_out) = (void *)(a_pipe->m_buffer + op_offset);
        }
        else if (a_can_wait)
        {
                if ((a_pipe->m_flags & PIPE_HAS_WRITER))
                {
                        retval = uthread_sleep_timeout(&a_pipe->m_pos.write, 0, &a_pipe->m_lock);
                        if (KERN_STATUS_SUCCESS(retval))
                        {
                                goto again;
                        }
                }

                if (KERN_STATUS_SUCCESS(retval))
                {
                        retval = KERN_FAIL(EWOULDBLOCK);
                }
        }
        lock_read_done(&a_pipe->m_lock);

        return retval;
}

// ********************************************************************************************************************************
void pipe_read_advance(pipe_t a_pipe)
// ********************************************************************************************************************************
{
        lock_write(&a_pipe->m_lock);

        natural_t readcnt = ((a_pipe->m_pos.write - a_pipe->m_pos.read) & (a_pipe->m_buffer_length - 1));

        if (readcnt > 0)
        {
                a_pipe->m_pos.read++;
        }

        lock_write_done(&a_pipe->m_lock);
}

// ********************************************************************************************************************************
natural_t pipe_read_available(pipe_t a_pipe)
// ********************************************************************************************************************************
{
        lock_read(&a_pipe->m_lock);

        natural_t readcnt = ((a_pipe->m_pos.write - a_pipe->m_pos.read) & (a_pipe->m_buffer_length - 1));

        lock_read_done(&a_pipe->m_lock);

        return readcnt;
}

// ********************************************************************************************************************************
natural_t pipe_write_available(pipe_t a_pipe)
// ********************************************************************************************************************************
{
        lock_read(&a_pipe->m_lock);

        natural_t mask = a_pipe->m_buffer_length - 1;

        natural_t writecnt = (mask /* naming :() */ - ((a_pipe->m_pos.write - a_pipe->m_pos.read) & mask));

        lock_done(&a_pipe->m_lock);

        return writecnt;
}

// ********************************************************************************************************************************
boolean_t pipe_read_closed(pipe_t a_pipe)
// ********************************************************************************************************************************
{
        lock_read(&a_pipe->m_lock);
        boolean_t read_closed = !(a_pipe->m_flags & PIPE_HAS_READER);
        lock_done(&a_pipe->m_lock);
        return read_closed;
}

// ********************************************************************************************************************************
boolean_t pipe_write_closed(pipe_t a_pipe)
// ********************************************************************************************************************************
{
        lock_read(&a_pipe->m_lock);
        boolean_t write_closed = !(a_pipe->m_flags & PIPE_HAS_WRITER);
        lock_done(&a_pipe->m_lock);
        return write_closed;
}

// ********************************************************************************************************************************
void pipe_read_shutdown(pipe_t a_pipe)
// ********************************************************************************************************************************
{
        lock_write(&a_pipe->m_lock);
        if (a_pipe->m_flags & PIPE_HAS_READER)
        {
                a_pipe->m_flags &= ~PIPE_HAS_READER;
                // Let any waiting writers know that the readers are gone.
                uthread_wakeup(&a_pipe->m_pos.read);
        }
        lock_done(&a_pipe->m_lock);
}

// ********************************************************************************************************************************
void pipe_write_shutdown(pipe_t a_pipe)
// ********************************************************************************************************************************
{
        lock_write(&a_pipe->m_lock);
        if (a_pipe->m_flags & PIPE_HAS_WRITER)
        {
                a_pipe->m_flags &= ~PIPE_HAS_WRITER;
                // Let any waiting readers know that the writers are gone.
                uthread_wakeup(&a_pipe->m_pos.write);
        }
        lock_done(&a_pipe->m_lock);
}

// ********************************************************************************************************************************
static kern_return_t init_bin_rbuf()
// ********************************************************************************************************************************
{
        vm_offset_t addr_min = 0;
        vm_offset_t addr_max = 0;
        natural_t size       = 1 << 26;

        g_pipe_buffer_map = kmem_suballoc(kernel_map, &addr_min, &addr_max, size, false);

        return g_pipe_buffer_map != nullptr ? KERN_SUCCESS : -1;
}

subsys_initcall(init_bin_rbuf);
