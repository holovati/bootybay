#include <limits.h>

#include <mach/machine/locore.h>

#include <emerixx/vfs/vfs_node.h>

#include <kernel/zalloc.h>

#include <emerixx/vfs/vfs_pathbuf.h>

typedef struct pathbuf_data
{
        char buffer[PATH_MAX];
} *pathbuf_t;

ZONE_DEFINE(pathbuf_data, s_pathbuf_zone, 16, 8, ZONE_EXHAUSTIBLE, 1);

// ********************************************************************************************************************************
char *vfs_pathbuf_alloc(natural_t *a_pathbuf_len_out)
// ********************************************************************************************************************************
{
        pathbuf_t pb = s_pathbuf_zone.alloc();

        if (a_pathbuf_len_out)
        {
                *a_pathbuf_len_out = PATH_MAX;
        }

        return pb->buffer;
}

// ********************************************************************************************************************************
kern_return_t vfs_pathbuf_copyin(char const *a_usr_path, char const **a_kern_path_out, natural_t *a_pathbuf_len_out)
// ********************************************************************************************************************************
{
        kern_return_t retval;
        pathbuf_t pb = s_pathbuf_zone.alloc();

        char *pb_end = stpncpyin(pb->buffer, const_cast<char *>(a_usr_path), sizeof(*pb), &retval);

        if (KERN_STATUS_SUCCESS(retval))
        {
                *a_kern_path_out = pb->buffer;

                natural_t pathlen = (natural_t)(pb_end - pb->buffer);

                if (pathlen == 0)
                {
                        s_pathbuf_zone.free(pb);
                        return KERN_FAIL(ENOENT);
                }

                if (a_pathbuf_len_out)
                {
                        *a_pathbuf_len_out = pathlen;
                }
        }
        else
        {
                s_pathbuf_zone.free(pb);
        }

        return retval;
}

// ********************************************************************************************************************************
void vfs_pathbuf_free(char const *a_pathbuf)
// ********************************************************************************************************************************
{
        pathbuf_t pb = (pathbuf_t)(const_cast<char *>(a_pathbuf));
        s_pathbuf_zone.free(pb);
}
