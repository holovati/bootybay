
#include <mach/conf.h>
#include <mach/param.h>

struct file;

static int nodev_ioctl(dev_t, ioctl_req_t, int)
{
        return -ENODEV;
}

static int nodev_strategy(struct buf *)
{
        return -ENODEV;
}

static int nodev_strategy2(dev_t, void *)
{
        return -ENODEV;
}

static int nodev_strategy3(dev_t, vm_page_t)
{
        return -ENODEV;
}

static int nodev_close(dev_t)
{
        return -ENODEV;
}
static int nodev_close(vfs_file_t a_file, dev_t)
{
        return -ENODEV;
}

static int nodev_open(dev_t dev, int rw, int type)
{
        return -ENODEV;
}

extern struct bdevsw atasw;

struct bdevsw bdevsw[] = {
        { nodev_open, nodev_close, nodev_strategy, nodev_strategy2, nodev_strategy3, nodev_ioctl }, /* rk = 0 */
        { nodev_open, nodev_close, nodev_strategy, nodev_strategy2, nodev_strategy3, nodev_ioctl }, /* rp = 1 */
        { nodev_open, nodev_close, nodev_strategy, nodev_strategy2, nodev_strategy3, nodev_ioctl }, /* rf = 2 */
  // tmopen,  tmclose, tmstrategy,  &tmtab, /* tm = 3 */
        { nodev_open, nodev_close, nodev_strategy, nodev_strategy2, nodev_strategy3, nodev_ioctl }, /* rf = 2 */
        { nodev_open, nodev_close, nodev_strategy, nodev_strategy2, nodev_strategy3, nodev_ioctl }, /* tc = 4 */
        { nodev_open, nodev_close, nodev_strategy, nodev_strategy2, nodev_strategy3, nodev_ioctl }, /* hs|ml = 5 */
        { nodev_open, nodev_close, nodev_strategy, nodev_strategy2, nodev_strategy3, nodev_ioctl }, /* hp = 6 */
        { nodev_open, nodev_close, nodev_strategy, nodev_strategy2, nodev_strategy3, nodev_ioctl }, /* ht = 7 */
  // nulldev, nulldev, rlstrategy,  &rltab, /* rl = 8 */
        { nodev_open, nodev_close, nodev_strategy, nodev_strategy2, nodev_strategy3, nodev_ioctl }, /* rf = 2 */
        { nodev_open, nodev_close, nodev_strategy, nodev_strategy2, nodev_strategy3, nodev_ioctl }, /* hk = 9 */
        { nodev_open, nodev_close, nodev_strategy, nodev_strategy2, nodev_strategy3, nodev_ioctl }, /* ts = 10 */
        { nodev_open, nodev_close, nodev_strategy, nodev_strategy2, nodev_strategy3, nodev_ioctl }, /* rx2 = 11 */
        { nodev_open, nodev_close, nodev_strategy, nodev_strategy2, nodev_strategy3, nodev_ioctl }, /* hm = 12 */
        { nodev_open, nodev_close, nodev_strategy, nodev_strategy2, nodev_strategy3, nodev_ioctl }, /* sd = 13 */
        { nullptr, nullptr, nullptr, nullptr }
};

int nblkdev = sizeof(bdevsw) / sizeof(bdevsw[0]);
