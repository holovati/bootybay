#include <string.h>

#include <aux/init.h>
#include <aux/defer.hh>

#include <etl/algorithm.hh>
#include <etl/queue.h>

#include <kernel/zalloc.h>

#include <emerixx/vfs/vfs_node.h>
#include <emerixx/vfs/vfs_mount.h>
#include <emerixx/vfs/vfs_util.h>

#include <emerixx/vfs/vfs_file_system.h>

typedef struct fs_mount_function_entry_data
{
        queue_chain_t m_link;
        char m_filesystem_name[32];
        fs_mount_function_t m_mount_function;
} *fs_mount_function_entry_t;

ZONE_DEFINE(fs_mount_function_entry_data, s_mount_function_entry_zone, 0x20, 0x10, ZONE_EXHAUSTIBLE, 0x10);

static queue_head_t s_fs_mount_functions;

// ********************************************************************************************************************************
kern_return_t vfs_file_system_register(char const *a_filesystem_name, fs_mount_function_t a_mount_function)
// ********************************************************************************************************************************
{
        for (queue_entry_t qentry = queue_first(&s_fs_mount_functions); //
             queue_end(&s_fs_mount_functions, qentry) == false;
             qentry = queue_next(qentry))
        {
                fs_mount_function_entry_t mfentry = queue_containing_record(qentry, struct fs_mount_function_entry_data, m_link);

                if (strncmp(mfentry->m_filesystem_name, a_filesystem_name, ARRAY_SIZE(mfentry->m_filesystem_name)) == 0)
                {
                        return KERN_FAIL(EEXIST);
                }
        }

        fs_mount_function_entry_t mfentry = s_mount_function_entry_zone.alloc();

        enqueue_tail(&s_fs_mount_functions, &mfentry->m_link);

        strncpy(mfentry->m_filesystem_name, a_filesystem_name, ARRAY_SIZE(mfentry->m_filesystem_name));

        mfentry->m_mount_function = a_mount_function;

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t vfs_file_system_mount(dev_t a_device,
                                    char const *a_target_path,
                                    char const *a_filesystem_name,
                                    void *a_file_system_data)
// ********************************************************************************************************************************
{
        fs_mount_function_t mount_function_ptr = nullptr;

        for (queue_entry_t qentry = queue_first(&s_fs_mount_functions); //
             queue_end(&s_fs_mount_functions, qentry) == false;
             qentry = queue_next(qentry))
        {
                fs_mount_function_entry_t mfentry = queue_containing_record(qentry, struct fs_mount_function_entry_data, m_link);

                if (strncmp(mfentry->m_filesystem_name, a_filesystem_name, ARRAY_SIZE(mfentry->m_filesystem_name)) == 0)
                {
                        mount_function_ptr = mfentry->m_mount_function;
                        break;
                }
        }

        if (mount_function_ptr == nullptr)
        {
                return KERN_FAIL(ENODEV);
        }

        vfs_node_t target_vfsn = nullptr;

        kern_return_t retval
                = vfs_util_simple_lookup_krn_path_at_cwd(a_target_path, strlen(a_target_path), &target_vfsn, true, true);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }
        defer(vfs_node_put(target_vfsn));

        // TODO: Check if the target can be mounted on

        vfs_mount_t mount = nullptr;

        retval = mount_function_ptr(a_device, a_file_system_data, &mount);
        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        retval = vfs_node_mount_here(target_vfsn, mount);
        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        retval = vfs_mount_covering_vfsn(mount, target_vfsn);
        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        return retval;
}

// ********************************************************************************************************************************
static kern_return_t early_init()
// ********************************************************************************************************************************
{
        queue_init(&s_fs_mount_functions);

        return KERN_SUCCESS;
}

early_initcall(early_init);
