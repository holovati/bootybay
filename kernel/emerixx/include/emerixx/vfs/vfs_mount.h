#pragma once

#if !defined(__NEED_vfs_node)
#define __NEED_vfs_node
#endif

#if !defined(__NEED_vfs_mount)
#define __NEED_vfs_mount
#endif

#if !defined(__NEED_vfs_mount_stat)
#define __NEED_vfs_mount_stat
#endif

#include <emerixx/vfs/vfs_types.h>

vfs_mount_t vfs_mount_first();

vfs_mount_t vfs_mount_next(vfs_mount_t a_vfsm);

kern_return_t vfs_mount_rootnode(vfs_mount_t a_vfsm, vfs_node_t *a_vfsn_out);

kern_return_t vfs_mount_stat(vfs_mount_t a_vfsm, vfs_mount_stat_t a_stat);

kern_return_t vfs_mount_sync(vfs_mount_t a_mnt, boolean_t a_wait);

kern_return_t vfs_mount_covering_vfsn(vfs_mount_t a_mnt, vfs_node_t a_covered_node);

vfs_node_t vfs_mount_covered_vfsn(vfs_mount_t a_mnt);
