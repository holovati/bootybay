#pragma once

#if !defined(__NEED_vfs_node)
#define __NEED_vfs_node
#endif

#if !defined(__NEED_vfs_node_stat)
#define __NEED_vfs_node_stat
#endif

#if !defined(__NEED_vfs_mount)
#define __NEED_vfs_mount
#endif

#if !defined(__NEED_vfs_node_operations)
#define __NEED_vfs_node_operations
#endif

#if !defined(__NEED_vfs_file_operations)
#define __NEED_vfs_file_operations
#endif

#include <emerixx/vfs/vfs_types.h>

#include <kernel/vm/vm_object.h>

// Flags passed to file systems during lookup, lifted from NetBSD.
#define VFS_NODE_LOOKUP_FLAG_RDONLY    0x0001000 /* lookup with read-only semantics */
#define VFS_NODE_LOOKUP_FLAG_ISDOTDOT  0x0002000 /* current component name is .. */
#define VFS_NODE_LOOKUP_FLAG_ISLASTCN  0x0008000 /* this is last component of pathname */
#define VFS_NODE_LOOKUP_FLAG_CREATEDIR 0x0200000 /* trailing slashes are ok */

typedef struct uio *memory_rdwr_t; // XXX

typedef enum vfs_node_lookup_type
{
        VFS_NODE_LOOKUP_GLANCE = 0,
        VFS_NODE_LOOKUP_CREATE,
        VFS_NODE_LOOKUP_DELETE,
        VFS_NODE_LOOKUP_RENAME
} vfs_node_lookup_type_t;

struct vfs_node_operations_data
{
        kern_return_t (*lookup)(vfs_node_t a_vfsn,
                                char const *a_cname,
                                natural_t a_cname_len,
                                vfs_node_lookup_type_t a_lookup_type,
                                int a_flags,
                                vfs_node_t *a_vfsn_out);
        kern_return_t (
                *create)(vfs_node_t a_vfsn, char const *a_cname, natural_t a_cname_len, mode_t a_mode, vfs_node_t *a_vfsn_out);

        kern_return_t (*mknod)(vfs_node_t a_vfsn, char const *a_cname, natural_t a_cname_len, mode_t a_mode, dev_t a_dev);

        kern_return_t (*mkdir)(vfs_node_t a_vfsn, char const *a_cname, natural_t a_cname_len, mode_t a_mode);

        kern_return_t (*symlink)(vfs_node_t a_vfsn,
                                 char const *a_cname,
                                 natural_t a_cname_len,
                                 char const *a_tname,
                                 natural_t a_tname_len,
                                 mode_t a_mode);

        kern_return_t (*link)(vfs_node_t a_vfsn, vfs_node_t a_target_vfsn, char const *a_tcname, natural_t a_tcname_len);

        kern_return_t (*rmdir)(vfs_node_t a_vfsn, vfs_node_t a_vfsn_dir, char const *a_dirnm, natural_t a_dirnm_len);

        kern_return_t (*remove)(vfs_node_t a_vfsn, vfs_node_t a_vfsn_file, char const *a_filenm, natural_t a_filenm_len);

        kern_return_t (*rename)(vfs_node_t a_src_prnt_vfsn,
                                char const *a_src_nam,
                                natural_t a_src_nam_len,
                                vfs_node_t a_trgt_prnt_vfsn,
                                char const *a_trgt_nam,
                                natural_t a_trgt_nam_len);
        kern_return_t (*readlink)(vfs_node_t a_vfsn, void *a_kbuf, natural_t *a_kbuf_len);

        kern_return_t (*rdwr)(vfs_node_t a_vfsn, integer_t a_oflags, memory_rdwr_t a_memrw);

        kern_return_t (*stat)(vfs_node_t a_vfsn, vfs_node_stat_t a_vfsn_stat);

        kern_return_t (*utimens)(vfs_node_t a_vfsn, struct timespec *a_timens);

        kern_return_t (*chmod)(vfs_node_t a_vfsn, mode_t a_mode);

        kern_return_t (*chown)(vfs_node_t a_vfsn, uid_t a_uid, gid_t a_gid);

        kern_return_t (*truncate)(vfs_node_t a_vfsn, off_t a_length);

        kern_return_t (*fsync)(vfs_node_t a_vfsn);
};

struct vfs_node_data
{
        struct vm_object_data m_vmo;
        vfs_node_operations_t m_ops;
        vfs_file_operations_t m_fops;
        vfs_mount_t m_mount;
        union
        {
                vfs_mount_t m_mounted_here; // Valid if dir
                natural_t m_socket_id;      // Valid if socket
        };
        mode_t m_mode;
        int32_t __Pad[1];
        dev_t m_rdev;
};

void vfs_node_init(vfs_node_t a_vfsn);
