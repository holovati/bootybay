#pragma once

#if !defined(__NEED_vfs_node)
#define __NEED_vfs_node
#endif

#include <emerixx/vfs/vfs_types.h>

kern_return_t vfs_util_fd_to_vfsn(int a_fd, vfs_node_t *a_vfsn_out, boolean_t a_want_lock);

kern_return_t vfs_util_lookup_first_node(const char *a_kpath, int a_dfd, vfs_node_t *a_vfsn_out, boolean_t a_want_lock);

kern_return_t vfs_util_simple_lookup_krn_path(const char *a_kpath,
                                              natural_t a_kpath_len,
                                              int a_dfd,
                                              vfs_node_t *a_vfsn_out,
                                              boolean_t a_follow_symlink,
                                              boolean_t a_want_lock);

kern_return_t vfs_util_simple_lookup_krn_path_at_cwd(const char *a_kpath,
                                                     natural_t a_kpath_len,
                                                     vfs_node_t *a_vfsn_out,
                                                     boolean_t a_follow_symlink,
                                                     boolean_t a_want_lock);

kern_return_t vfs_util_simple_lookup_usr_path(const char *a_upath,
                                              int a_dfd,
                                              vfs_node_t *a_vfsn_out,
                                              boolean_t a_follow_symlink,
                                              boolean_t a_want_lock);

kern_return_t vfs_util_create_file_simple(const char *a_kpath, natural_t a_kpath_len, mode_t a_mode, vfs_node_t *a_vfsn_out);
