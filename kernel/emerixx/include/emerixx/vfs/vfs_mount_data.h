#pragma once

#if !defined(__NEED_vfs_node)
#define __NEED_vfs_node
#endif

#if !defined(__NEED_vfs_mount)
#define __NEED_vfs_mount
#endif

#if !defined(__NEED_vfs_mount_stat)
#define __NEED_vfs_mount_stat
#endif

#if !defined(__NEED_vfs_mount_operations)
#define __NEED_vfs_mount_operations
#endif

#include <etl/queue.h>

#include <emerixx/vfs/vfs_types.h>

struct vfs_mount_operations_data
{
        kern_return_t (*root)(vfs_mount_t a_vfsm, vfs_node_t *a_vfsn_out);
        kern_return_t (*stat)(vfs_mount_t a_vfsm, vfs_mount_stat_t a_stat);
        kern_return_t (*sync)(vfs_mount_t a_vfsm, boolean_t a_wait);
};

struct vfs_mount_data
{
        vfs_mount_operations_t m_ops;
        queue_chain_t m_allmount_link;
        vfs_node_t m_vfsn_covered;
        vfs_node_t m_vfsn_root;
};

kern_return_t vfs_mount_init(vfs_mount_t a_vfsm);

kern_return_t vfs_mount_op_sync_noop(vfs_mount_t a_vfsm, boolean_t a_wait);
