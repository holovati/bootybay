#pragma once

kern_return_t vfs_pathbuf_copyin(char const *a_usr_path, char const **a_kern_path_out, natural_t *a_pathbuf_len_out);

char *vfs_pathbuf_alloc(natural_t *a_pathbuf_len_out);

void vfs_pathbuf_free(char const *a_pathbuf);