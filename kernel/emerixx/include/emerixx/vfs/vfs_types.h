#if defined(__NEED_vfs_private) && !defined(__DEFINED_vfs_private)
#define __DEFINED_vfs_private
typedef void *vfs_private_t;
#endif

#if defined(__NEED_vfs_mount) && !defined(__DEFINED_vfs_mount)
#define __DEFINED_vfs_mount
typedef struct vfs_mount_data *vfs_mount_t;
#endif

#if defined(__NEED_vfs_mount_stat) && !defined(__DEFINED_vfs_mount_stat)
#define __DEFINED_vfs_mount_stat
typedef struct statfs *vfs_mount_stat_t;
#endif

#if defined(__NEED_vfs_node) && !defined(__DEFINED_vfs_node)
#define __DEFINED_vfs_node
typedef struct vfs_node_data *vfs_node_t;
#endif

#if defined(__NEED_vfs_node_stat) && !defined(__DEFINED_vfs_node_stat)
#define __DEFINED_vfs_node_stat
typedef struct stat *vfs_node_stat_t;
#endif

#if defined(__NEED_vfs_file) && !defined(__DEFINED_vfs_file)
#define __DEFINED_vfs_file
typedef struct vfs_file_data *vfs_file_t;
#endif

#if defined(__NEED_ioctl_req) && !defined(__DEFINED_ioctl_req)
#define __DEFINED_ioctl_req
typedef struct ioctl_req_data *ioctl_req_t;
#endif

#if defined(__NEED_vfs_node_operations) && !defined(__DEFINED_vfs_node_operations)
#define __DEFINED_vfs_node_operations
typedef struct vfs_node_operations_data *vfs_node_operations_t;
#endif

#if defined(__NEED_vfs_mount_operations) && !defined(__DEFINED_vfs_mount_operations)
#define __DEFINED_vfs_mount_operations
typedef struct vfs_mount_operations_data *vfs_mount_operations_t;
#endif

#if defined(__NEED_vfs_file_operations) && !defined(__DEFINED_vfs_file_operations)
#define __DEFINED_vfs_file_operations
typedef struct vfs_file_operations_data *vfs_file_operations_t;
#endif
