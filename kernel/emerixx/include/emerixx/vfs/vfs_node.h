#pragma once

#if !defined(__NEED_vfs_node)
#define __NEED_vfs_node
#endif

#if !defined(__NEED_vfs_mount)
#define __NEED_vfs_mount
#endif

#if !defined(__NEED_vfs_node_stat)
#define __NEED_vfs_node_stat
#endif

#include <emerixx/vfs/vfs_types.h>

typedef struct uio *memory_rdwr_t; // XXX

kern_return_t vfs_node_lookup(vfs_node_t a_vfsn,
                              const char *a_kpath,
                              natural_t a_path_len,
                              vfs_node_t *a_vfsn_out,
                              boolean_t a_follow_symlink);

kern_return_t vfs_node_lookup_create(vfs_node_t a_vfsn,
                                     const char *a_kpath,
                                     natural_t a_path_len,
                                     vfs_node_t *a_prnt_vfsn_out,
                                     const char **a_cnpnam_out,
                                     natural_t *a_cnpnamlen_out,
                                     vfs_node_t *a_chld_vfsn_out,
                                     boolean_t a_follow_symlink,
                                     boolean_t a_create_directory);

kern_return_t vfs_node_lookup_delete(vfs_node_t a_vfsn,
                                     const char *a_kpath,
                                     natural_t a_path_len,
                                     boolean_t a_follow_symlink,
                                     vfs_node_t *a_parent_vfsn_out,
                                     vfs_node_t *a_child_vfsn_out,
                                     const char **a_child_cnp,
                                     natural_t *a_child_cnplen);

kern_return_t vfs_node_lookup_rename(vfs_node_t a_src_start_vfsn,
                                     const char *a_src_kpath,
                                     natural_t a_src_path_len,
                                     vfs_node_t a_dst_start_vfsn,
                                     const char *a_dst_kpath,
                                     natural_t a_dst_path_len,
                                     boolean_t a_follow_symlink,
                                     vfs_node_t *a_src_vfsn_out,
                                     const char **a_src_cnpnam_out,
                                     natural_t *a_src_cnpnamlen_out,
                                     vfs_node_t *a_dst_vfsn_out,
                                     const char **a_dst_cnpnam_out,
                                     natural_t *a_dst_cnpnamlen_out);

kern_return_t vfs_node_read(vfs_node_t a_vfsn, natural_t a_offset, void *a_buf_out, natural_t a_buf_len);

kern_return_t vfs_node_stat(vfs_node_t a_vfsn, vfs_node_stat_t a_vfsn_stat);

kern_return_t vfs_node_readlink(vfs_node_t a_vfsn, char *a_buf, natural_t *a_buflen);

kern_return_t vfs_node_utimens(vfs_node_t a_vfsn, struct timespec *a_timens);

kern_return_t vfs_node_chmod(vfs_node_t a_vfsn, mode_t a_mode);

kern_return_t vfs_node_chown(vfs_node_t a_vfsn, uid_t a_uid, gid_t a_gid);

kern_return_t vfs_node_fsync(vfs_node_t a_vfsn);

kern_return_t vfs_node_create(vfs_node_t a_vfsn, char const *a_cname, natural_t a_cname_len, mode_t a_mode, vfs_node_t *a_vfsn_out);

kern_return_t vfs_node_mknod(vfs_node_t a_vfsn, char const *a_cname, natural_t a_cname_len, mode_t a_mode, dev_t a_dev);

kern_return_t vfs_node_mkdir(vfs_node_t a_vfsn, char const *a_cname, natural_t a_cname_len, mode_t a_mode);

kern_return_t vfs_node_symlink(vfs_node_t a_vfsn,
                               char const *a_cname,
                               natural_t a_cname_len,
                               char const *a_tname,
                               natural_t a_tname_len);

kern_return_t vfs_node_link(vfs_node_t a_vfsn, vfs_node_t a_target_vfsn, char const *a_tcname, natural_t a_tcname_len);

kern_return_t vfs_node_unlink(vfs_node_t a_vfsn,
                              vfs_node_t a_target_vfsn,
                              char const *a_tname,
                              natural_t a_tname_len,
                              boolean_t a_remove_dir);

kern_return_t vfs_node_truncate(vfs_node_t a_vfsn, off_t a_length);

kern_return_t vfs_node_rename(vfs_node_t a_src_prnt_vfsn,
                              char const *a_src_chld_nam,
                              natural_t a_src_chld_nam_len,
                              vfs_node_t a_trgt_prnt_vfsn,
                              char const *a_trgt_chld_nam,
                              natural_t a_trgt_chld_nam_len);

dev_t vfs_node_device(vfs_node_t a_vfsn);

kern_return_t vfs_node_mount_here(vfs_node_t a_vfsn, vfs_mount_t a_mount);

kern_return_t vfs_node_socket_id_set(vfs_node_t a_vfsn, natural_t a_socket_id);

kern_return_t vfs_node_socket_id_get(vfs_node_t a_vfsn, natural_t *a_socket_id_out);

void vfs_node_ref(vfs_node_t a_vfsn);

void vfs_node_rel(vfs_node_t a_vfsn);

void vfs_node_lock(vfs_node_t a_vfsn);

void vfs_node_unlock(vfs_node_t a_vfsn);

void vfs_node_put(vfs_node_t a_vfsn);

void vfs_rename_lock_shared();

void vfs_rename_lock_shared_done();

void vfs_rename_lock_exclusive();

void vfs_rename_lock_exclusive_done();
