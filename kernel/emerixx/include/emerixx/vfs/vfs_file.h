#pragma once

#if !defined(__NEED_vfs_node)
#define __NEED_vfs_node
#endif

#if !defined(__NEED_vfs_file)
#define __NEED_vfs_file
#endif

#if !defined(__NEED_ioctl_req)
#define __NEED_ioctl_req
#endif

#include <emerixx/vfs/vfs_types.h>

#if !defined(__NEED_vm_object)
#define __NEED_vm_object
#endif

#include <kernel/vm/vm_types.h>

typedef struct uio *memory_rdwr_t; // XXX

kern_return_t vfs_file_open(vfs_node_t a_vfsn, int a_oflags, vfs_file_t *a_vfsf_out);

#define vfs_file_close(fil) (vfs_file_rel((fil)))

kern_return_t vfs_file_read(vfs_file_t a_vfsf, memory_rdwr_t a_memrw);

kern_return_t vfs_file_write(vfs_file_t a_vfsf, memory_rdwr_t a_memrw);

kern_return_t vfs_file_lseek(vfs_file_t a_vfsf, off_t a_offset, int a_whence);

kern_return_t vfs_file_ioctl(vfs_file_t a_vfsf, ioctl_req_t com);

kern_return_t vfs_file_poll(vfs_file_t a_vfsf, struct pollfd **a_pfd);

kern_return_t vfs_file_lock(vfs_file_t a_vfsf, int a_operation);

kern_return_t vfs_file_splice(vfs_file_t a_vfsf,
                              off_t *a_off,
                              vfs_file_t a_file_out,
                              off_t *a_off_out,
                              size_t a_len,
                              unsigned a_flags);

kern_return_t vfs_file_getdents(vfs_file_t a_vfsf, memory_rdwr_t a_memrw);

kern_return_t vfs_file_mmap(vfs_file_t a_vfsf, vm_object_t *a_object_out);

void vfs_file_ref(vfs_file_t a_file);

kern_return_t vfs_file_rel(vfs_file_t a_file);

kern_return_t vfs_file_remove_lock(vfs_file_t a_file);
