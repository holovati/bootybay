#if !defined(__NEED_vfs_mount)
#define __NEED_vfs_mount
#endif

#include <emerixx/vfs/vfs_types.h>

typedef kern_return_t (*fs_mount_function_t)(dev_t a_device, void *a_filesystem_data, vfs_mount_t *a_mount_out);

kern_return_t vfs_file_system_register(char const *a_filesystem_name, fs_mount_function_t a_mount_function);

kern_return_t vfs_file_system_mount(dev_t a_device, char const *a_target, char const *a_filesystem_name, void *a_file_system_data);
