#pragma once

#if !defined(__NEED_vfs_private)
#define __NEED_vfs_private
#endif

#if !defined(__NEED_vfs_node)
#define __NEED_vfs_node
#endif

#if !defined(__NEED_vfs_file_operations)
#define __NEED_vfs_file_operations
#endif

#if !defined(__NEED_vfs_file)
#define __NEED_vfs_file
#endif

#if !defined(__NEED_ioctl_req)
#define __NEED_ioctl_req
#endif

#include <emerixx/vfs/vfs_types.h>

#if !defined(__NEED_vm_object)
#define __NEED_vm_object
#endif

#include <kernel/vm/vm_types.h>

typedef struct uio *memory_rdwr_t; // XXX

struct vfs_file_operations_data
{
        kern_return_t (*fopen)(vfs_node_t, vfs_file_t);
        kern_return_t (*fread)(vfs_file_t, struct uio *uio);
        kern_return_t (*fwrite)(vfs_file_t, struct uio *uio);
        kern_return_t (*flseek)(vfs_file_t, off_t a_offset, int a_whence);
        kern_return_t (*fioctl)(vfs_file_t, ioctl_req_t com);
        kern_return_t (*fclose)(vfs_file_t);
        kern_return_t (*fgetdents)(vfs_file_t, struct uio *uio);
        kern_return_t (*fmmap)(vfs_file_t, vm_object_t *);
        kern_return_t (*fpoll)(vfs_file_t, struct pollfd **a_pfd);
        kern_return_t (*fsplice)(vfs_file_t, off_t *a_off, vfs_file_t a_file_out, off_t *a_off_out, size_t a_len, unsigned a_flags);
};

#define FILEMAX 0x100

struct vfs_file_data
{
        vfs_file_operations_t m_fops;
        vfs_node_t m_vfsn;  // The vnode this file is manipulating
        integer_t m_oflags; // flags from open
        integer_t m_usecnt;
        union
        {
                off_t m_offset;
                vfs_private_t m_private;
                natural_t u;
                integer_t s;
        };
};

// Generic file operations

kern_return_t vfs_file_op_mmap_enodev(vfs_file_t, vm_object_t *);

kern_return_t vfs_file_op_lseek_espipe(vfs_file_t, off_t, int);

kern_return_t vfs_file_op_getdents_enotdir(vfs_file_t, memory_rdwr_t);

kern_return_t vfs_file_op_ioctl_enotty(vfs_file_t, ioctl_req_t);

kern_return_t vfs_file_op_vfsn_rw(vfs_file_t, memory_rdwr_t);

kern_return_t vfs_file_op_open_noop(vfs_node_t, vfs_file_t);

kern_return_t vfs_file_op_close_noop(vfs_file_t);
