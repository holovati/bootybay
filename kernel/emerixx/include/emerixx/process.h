#pragma once

#if !defined(__NEED_vm_map)
#define __NEED_vm_map
#endif

#define __NEED_fs_context
#define __NEED_process_group
#define __NEED_signalstack
#define __NEED_fd_data
#define __NEED_rusage
#include <kernel/vm/vm_types.h>
#include <emerixx/alltypes.h> // XXX

typedef struct ksigaction *ksigaction_t;

struct ksigaction
{
        /* Disposition of signals */
        void (*handler)(int);

        /* Flags which modify the behavior of the signal */
        natural_t flags;

        /* Userspace stub that calls rt_sigreturn */
        void (*restorer)(void);

        /*
        Mask specifies a mask of signals which should be blocked
        (i.e. added to the signal mask of the thread in which the signal handler is invoked) during execution of the signal handler.
        In addition, the signal which triggered the handler will be blocked, unless the SA_NODEFER flag is used.
        */
        sigset_t mask;
};

struct process
{
        pid_t m_ppid;
        pid_t m_pid;
        pid_t m_pgid;
        pid_t m_sid;
        dev_t m_ctty;

        // Remove after execve is fixed
        int m_clone_flags;
        int pad;
};

struct pthread
{
        pid_t m_tid;
        pid_t m_pid;
        int m_csignal;
        int m_pad;
        pid_t *m_tid_address;
        fd_context_t m_fdctx;
        fs_context_t m_fsctx;
};

struct utask_group
{
        pid_t m_pgid;
        pid_t m_sid;
};

#ifdef __cplusplus
extern "C"
{
#endif

// unistd_ex
dev_t getctty();
void setctty(dev_t);

// UTASK
process_t utask_self();

kern_return_t utask_find(pid_t a_pid, process_t *a_utask_out);

kern_return_t utask_find_child(process_t a_parent_utask, pid_t a_pid, process_t *a_new_task_out);

kern_return_t utask_dup(process_t *a_new_task_out, pthread_t *a_new_thread_out);

kern_return_t utask_signal(process_t a_process, int a_signo);

kern_return_t utask_wait_group_child(pid_t a_pgid, int *a_wstat_out, int a_options, struct rusage *a_ru_out);

kern_return_t utask_wait_child(pid_t a_child_pid, int *a_wstat_out, int a_options, struct rusage *a_ru_out);

kern_return_t utask_exec_start(pthread_t a_uthrd, process_t *a_utask_out);

kern_return_t utask_exec_done(process_t a_utask, pthread_t a_uthrd, vm_address_t a_regs[2], kern_return_t a_exec_result);

kern_return_t utask_set_group_id(process_t a_utask, pid_t a_new_pgid);

kern_return_t utask_replace_vm_context(process_t a_utask, pthread_t a_uthread, vm_map_t a_vmctx, int a_activate);

kern_return_t utask_suspend(process_t a_utask);

kern_return_t utask_resume(process_t a_utask);

kern_return_t utask_setitimer(int a_which, struct itimerspec *a_timerspec_in, struct itimerspec *a_timerspec_out);

void utask_getitimer(int a_which, struct itimerspec *a_timerspec_out);

void utask_release_parent(process_t a_utask);

kern_return_t process_exit(int a_exit_code);

int uthread_pending_signal(pthread_t a_pthread);

kern_return_t vm_context_dup(process_t a_process);

kern_return_t uthread_signalstack(pthread_t a_thread, const stack_t *a_ss, stack_t *a_oss);

kern_return_t uthread_exit(pthread_t a_uthread, int a_exit_code);

// UTHREAD
pthread_t uthread_self();

pthread_t uthread_self_ex(process_t *a_utask_out);

void uthread_put_ex(pthread_t a_uthread, process_t a_utask);

kern_return_t uthread_find(pid_t a_tid, pthread_t *a_uthread_out);

kern_return_t uthread_signal(pthread_t a_uthread, int a_signo);

kern_return_t uthread_dup(pthread_t *a_new_thread_out);

kern_return_t uthread_sigprocmask(pthread_t a_thread, int a_how, sigset_t const *a_nset, sigset_t *a_oset);

kern_return_t uthread_sigaction(int a_signum, ksigaction_t a_actp, ksigaction_t a_oactp);

kern_return_t uthread_sigsuspend(sigset_t *a_sigset);

kern_return_t uthread_sigpending(sigset_t *a_sigset);

void uthread_yield();

void uthread_system_exit();

// UTASK GROUP
struct utask_group *utask_group_find(pid_t a_pgid);
kern_return_t utask_group_signal(struct utask_group *a_utask_group, int a_signo);

#ifdef __cplusplus
}
#endif
