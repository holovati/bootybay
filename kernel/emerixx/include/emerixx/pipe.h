#pragma once

#include <emerixx/memory_rw.h>

typedef natural_t (*pipe_range_callback_t)(void *a_elm_ptr, natural_t a_elm_count, void *a_userdata);

typedef struct pipe_data *pipe_t;

kern_return_t pipe_create(natural_t a_size, pipe_t *a_pipe_out);

kern_return_t pipe_rw(pipe_t a_pipe,
                      memory_rdwr_t a_mrw,
                      natural_t a_atomic_count,
                      natural_t a_ms_timeout,
                      boolean_t a_can_wait,
                      natural_t *a_rw_cnt_out);

kern_return_t pipe_read(pipe_t a_pipe, void *a_data, natural_t a_data_length, boolean_t a_can_wait);

kern_return_t pipe_write(pipe_t a_pipe, void *a_data, natural_t a_data_length, natural_t a_atomic_len, boolean_t a_can_wait);

kern_return_t pipe_read_range(pipe_t a_pipe, boolean_t a_can_wait, pipe_range_callback_t a_cb, void *a_cb_ud);

kern_return_t pipe_read_peek(pipe_t a_pipe, boolean_t a_can_wait, void *a_data_out);

void pipe_read_advance(pipe_t a_pipe);

natural_t pipe_read_available(pipe_t a_pipe);

natural_t pipe_write_available(pipe_t a_pipe);

boolean_t pipe_read_closed(pipe_t a_pipe);

boolean_t pipe_write_closed(pipe_t a_pipe);

void pipe_read_shutdown(pipe_t a_pipe);

void pipe_write_shutdown(pipe_t a_pipe);

void pipe_ref(pipe_t a_pipe);

void pipe_rel(pipe_t a_pipe);
