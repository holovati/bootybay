#pragma once
#ifdef __cplusplus
extern "C" {
#endif

#include <mach/machine/locore.h>
#include <sys/uio.h>

/*
 * Limits
 */
#define UIO_MAXIOV   1024 /* max 1K of iov's */
#define UIO_SMALLIOV 8    /* 8 on stack, else malloc */

enum uio_rw
{
        UIO_READ,
        UIO_WRITE
};

/* Segment flag values. */
enum uio_seg
{
        UIO_USERSPACE, /* from user data space */
        UIO_SYSSPACE,  /* from system space */
};

typedef struct uio
{
        struct iovec *uio_iov;
        size_t uio_iovcnt;
        off64_t uio_offset;
        size_t uio_resid;
        enum uio_seg uio_segflg;
        enum uio_rw uio_rw;
        union {
                void *p;
                integer_t s;
                natural_t u;
        } aux;
} memory_rdwr_data, uio_t, *puio_t, *memory_rdwr_t;

typedef natural_t (*uiomove_func_t)(char const *buf, natural_t n, void *a_arg);

kern_return_t uiomove(void const *buf, size_t n, puio_t uiop);

kern_return_t uiomove2(void const *buf, natural_t *n, puio_t uiop);

kern_return_t uiomove3(puio_t uiop, uiomove_func_t a_iofucn, void *a_arg);

#ifdef __cplusplus
} // __cplusplus
#endif

#ifdef __cplusplus
namespace emerixx
{
namespace uio
{
template <typename T> static inline int copyout(T const *kaddr, T *uaddr)
{
        return ::copyout(const_cast<T *>(kaddr), uaddr, sizeof(T));
}
template <typename T> static inline int copyout(T const &kref, T *uaddr) { return copyout(&kref, uaddr); }

template <typename T> static inline int copyin(T const *uaddr, T *kaddr)
{
        return ::copyin(const_cast<T *>(uaddr), kaddr, sizeof(T));
}

} // namespace uio
} // namespace emerixx
#endif // __cplusplus