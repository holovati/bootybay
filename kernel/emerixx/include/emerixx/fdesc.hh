#pragma once

#if !defined(__NEED_vfs_file)
#define __NEED_vfs_file
#endif

#include <emerixx/vfs/vfs_types.h>

#define __NEED_fd_data
#include <emerixx/alltypes.h> // XXX

kern_return_t fd_context_alloc(fd_context_t *a_fd_context_out);

kern_return_t fd_context_clone(fd_context_t a_fd_context_in, fd_context_t *a_fd_context_out);

kern_return_t fd_context_exec(fd_context_t a_fd_data);

kern_return_t fd_context_exit(fd_context_t a_fd_data);

kern_return_t fd_context_get_file(fd_context_t a_fd_data, int a_fd, vfs_file_t *a_file_out);

kern_return_t fd_context_dup(fd_context_t a_fd_data, int a_fd);

kern_return_t fd_context_dup2(fd_context_t a_fd_data, int a_oldfd, int a_newfd);

kern_return_t fd_context_dup3(fd_context_t a_fd_data, int a_oldfd, int a_newfd, int a_flags);

kern_return_t fd_context_fdalloc(fd_context_t a_fd_data, vfs_file_t a_vfsf);

kern_return_t fd_context_fdalloc(pthread_t a_pthread, vfs_file_t a_vfsf);

kern_return_t fd_context_fdclose(fd_context_t a_fd_data, int a_fd);

kern_return_t fd_context_fcntl(fd_context_t a_fd_data, int a_fd, unsigned a_cmd, unsigned long a_arg);

void fd_context_reference(fd_context_t a_fd_data);

void fd_context_deallocate(fd_context_t a_fd_data);
