#pragma once

#if !defined(__NEED_ioctl_req)
#define __NEED_ioctl_req
#endif

#include <emerixx/vfs/vfs_types.h>

#include <emerixx/memory_rw.h>

struct ioctl_req_data
{
        struct iovec m_iov;
        memory_rdwr_data m_internal;
};

kern_return_t ioctl_req_create(integer_t a_cmdno, void *a_arg, boolean_t a_is_user_req, ioctl_req_t a_ioctl_req);

kern_return_t ioctl_req_read(ioctl_req_t a_ioctl_req, void *a_data, natural_t a_data_len);

kern_return_t ioctl_req_write(ioctl_req_t a_ioctl_req, void *a_data, natural_t a_data_len);

integer_t ioctl_req_cmdno(ioctl_req_t a_ioctl_req);

integer_t ioctl_req_arg(ioctl_req_t a_ioctl_req);

#ifdef __cplusplus
// Veneer
template <typename T>
kern_return_t ioctl_req_copyin(ioctl_req_t a_ioctl_req, T *a_value)
{
        return ioctl_req_read(a_ioctl_req, a_value, sizeof(T));
}

template <typename T>
kern_return_t ioctl_req_copyout(ioctl_req_t a_ioctl_req, T const *a_value)
{
        return ioctl_req_write(a_ioctl_req, const_cast<void *>((void const *)a_value), sizeof(T));
}
#endif
