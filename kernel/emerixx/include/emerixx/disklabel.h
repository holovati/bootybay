#pragma once
#include <sys/ioctl.h>

#if !defined(__NEED_vfs_node)
#define __NEED_vfs_node
#endif

#include <emerixx/vfs/vfs_types.h>

/*
 * Each disk has a label which includes information about the hardware
 * disk geometry, filesystem partitions, and drive specific information.
 * The label is in block 0 or 1, possibly offset from the beginning
 * to leave room for a bootstrap, etc.
 */

#define DISKMAGIC (0x82564557U) /* The disk magic number */

#ifndef MAXPARTITIONS
#define MAXPARTITIONS 8
#endif

/*
 * Filesystem type and version.
 * Used to interpret other filesystem-specific
 * per-partition information.
 */
#define FS_UNUSED  0  /* unused */
#define FS_SWAP    1  /* swap */
#define FS_V6      2  /* Sixth Edition */
#define FS_V7      3  /* Seventh Edition */
#define FS_SYSV    4  /* System V */
#define FS_V71K    5  /* V7 with 1K blocks (4.1, 2.9) */
#define FS_V8      6  /* Eighth Edition, 4K blocks */
#define FS_BSDFFS  7  /* 4.2BSD fast file system */
#define FS_MSDOS   8  /* MSDOS file system */
#define FS_BSDLFS  9  /* 4.4BSD log-structured file system */
#define FS_OTHER   10 /* in use, but unknown/unsupported */
#define FS_EXT2    11 /* Ext2 file system */
#define FS_ISO9660 12 /* ISO 9660, normally CD-ROM */
#define FS_BOOT    13 /* partition contains bootstrap */

typedef struct partition
{                        /* the partition table */
        size_t p_size;   /* number of sectors in partition */
        size_t p_offset; /* starting sector */
        size_t p_fsize;  /* filesystem basic fragment size */
        u32 p_fstype;    /* filesystem type, see below */
        u32 p_frag;      /* filesystem fragments per block */
        union {
                size_t cpg; /* UFS: FS cylinders per group */
                size_t sgs; /* LFS: FS segment shift */
        };
} partition_t, *partitionp_t;

struct disklabel
{
        u64 d_magic;         /* the magic number */
        u32 d_type;          /* drive type */
        u32 d_subtype;       /* controller/d_type specific */
        char d_typename[16]; /* type name, e.g. "eagle" */
                             /*
                              * d_packname contains the pack identifier and is returned when
                              * the disklabel is read off the disk or in-core copy.
                              * d_boot0 and d_boot1 are the (optional) names of the
                              * primary (block 0) and secondary (block 1-15) bootstraps
                              * as found in /usr/mdec.  These are returned when using
                              * getdiskbyname(3) to retrieve the values from /etc/disktab.
                              */
        /* disk geometry: */
        size_t d_secsize;    /* # of bytes per sector */
        size_t d_nsectors;   /* # of data sectors per track */
        size_t d_ntracks;    /* # of tracks per cylinder */
        size_t d_ncylinders; /* # of data cylinders per unit */
        size_t d_secpercyl;  /* # of data sectors per cylinder */
        size_t d_secperunit; /* # of data sectors per unit */
        /*
         * Spares (bad sector replacements) below
         * are not counted in d_nsectors or d_secpercyl.
         * Spare sectors are assumed to be physical sectors
         * which occupy space at the end of each track and/or cylinder.
         */
        size_t d_sparespertrack; /* # of spare sectors per track */
        size_t d_sparespercyl;   /* # of spare sectors per cylinder */
        /*
         * Alternate cylinders include maintenance, replacement,
         * configuration description areas, etc.
         */
        size_t d_acylinders; /* # of alt. cylinders per unit */

        /* hardware characteristics: */
        /*
         * d_interleave, d_trackskew and d_cylskew describe perturbations
         * in the media format used to compensate for a slow controller.
         * Interleave is physical sector interleave, set up by the formatter
         * or controller when formatting.  When interleaving is in use,
         * logically adjacent sectors are not physically contiguous,
         * but instead are separated by some number of sectors.
         * It is specified as the ratio of physical sectors traversed
         * per logical sector.  Thus an interleave of 1:1 implies contiguous
         * layout, while 2:1 implies that logical sector 0 is separated
         * by one sector from logical sector 1.
         * d_trackskew is the offset of sector 0 on track N
         * relative to sector 0 on track N-1 on the same cylinder.
         * Finally, d_cylskew is the offset of sector 0 on cylinder N
         * relative to sector 0 on cylinder N-1.
         */
        size_t d_rpm;        /* rotational speed */
        size_t d_interleave; /* hardware sector interleave */
        size_t d_trackskew;  /* sector 0 skew, per track */
        size_t d_cylskew;    /* sector 0 skew, per cylinder */
        size_t d_headswitch; /* head switch time, usec */
        size_t d_trkseek;    /* track-to-track seek, usec */
        size_t d_flags;      /* generic flags */
#define NDDATA 5
#define NSPARE 5
        u32 d_drivedata[NDDATA]; /* drive-type specific info */
        u32 d_spare[NSPARE];     /* reserved for future use */
        u64 d_magic2;            /* the magic number (again) */
        size_t d_checksum;       /* xor of data incl. partitions */

        /* filesystem and partition information: */
        size_t d_npartitions; /* number of partitions in following */
        size_t d_bbsize;      /* size of boot area at sn0, bytes */
        size_t d_sbsize;      /* max size of fs superblock, bytes */
        struct partition part[MAXPARTITIONS];
        // #define	p_cpg	__partition_u1.cpg
        // #define	p_sgs	__partition_u1.sgs
};

/*
 * Structure used internally to retrieve
 * information about a partition on a disk.
 */
struct partinfo
{
        struct disklabel *disklab;
        struct partition *part;
};

/*
 * Disk-specific ioctls.
 */
/* get and set disklabel; DIOCGPART used internally */
#define DIOCGDINFO _IOR('d', 101, struct disklabel) /* get */
#define DIOCSDINFO _IOW('d', 102, struct disklabel) /* set */
#define DIOCWDINFO _IOW('d', 103, struct disklabel) /* set, update disk */
#define DIOCGPART  _IOW('d', 104, struct partinfo)  /* get partition */

/* do format operation, read or write */
#define DIOCRFORMAT _IOWR('d', 105, struct format_op)
#define DIOCWFORMAT _IOWR('d', 106, struct format_op)

#define DIOCSSTEP    _IOW('d', 107, int) /* set step rate */
#define DIOCSRETRIES _IOW('d', 108, int) /* set # of retries */
#define DIOCWLABEL   _IOW('d', 109, int) /* write en/disable label */

#define DIOCSBAD _IOW('d', 110, struct dkbad) /* set kernel dkbad */

#ifdef __cplusplus
extern "C" {
#endif
int partition_scan(vfs_node_t a_vfsn, size_t dev_lstlba, partitionp_t partp, size_t partsz, size_t *npartsp);

int mbr_partition_scan(void *a_mbr, natural_t a_max_lba, partitionp_t partp, natural_t partp_cnt);

#ifdef __cplusplus
}
#endif