#pragma once

#if !defined(__NEED_vfs_node)
#define __NEED_vfs_node
#endif

#if !defined(__NEED_vfs_file_operations)
#define __NEED_vfs_file_operations
#endif

#include <emerixx/vfs/vfs_types.h>

#include <etl/rbtnode.h>

#if defined(__cplusplus)
extern "C" {
#endif

typedef struct chrdev_data *chrdev_t;

struct chrdev_data
{
        rbtnode_data m_link;
        const char *m_name;
        vfs_file_operations_t m_fops;
        dev_t m_beg;
        dev_t m_end;
};

kern_return_t driver_chrdev_alloc(chrdev_t a_chrdev, const char *a_name, vfs_file_operations_t a_fops, dev_t a_beg, natural_t a_count);

kern_return_t driver_chrdev_lookup(dev_t a_dev, chrdev_t *a_chrdev_out);

kern_return_t driver_chrdev_free(chrdev_t a_chrdev_out);

void chrdev_init_vfs_node(vfs_node_t a_vfsn);

#if defined(__cplusplus)
}
#endif
