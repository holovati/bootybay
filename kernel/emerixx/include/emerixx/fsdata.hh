#pragma once

#if !defined(__NEED_vfs_node)
#define __NEED_vfs_node
#endif

#include <emerixx/vfs/vfs_types.h>

#define __NEED_fs_context
#include <emerixx/alltypes.h> // XXX

vfs_node_t fs_context_getcwd(fs_context_t a_fsctx);

vfs_node_t fs_context_getroot(fs_context_t a_fsctx);

mode_t fs_context_cmode(fs_context_t a_fsctx, mode_t a_mode);

mode_t fs_context_umask(fs_context_t a_fsctx, mode_t a_mode);

kern_return_t fs_context_chdir(fs_context_t a_fsctx, vfs_node_t a_new_dir);

kern_return_t fs_context_chroot(fs_context_t a_fsctx, vfs_node_t a_new_root);

kern_return_t fs_context_alloc(fs_context_t *a_fsctx_out);

kern_return_t fs_context_dup(fs_context_t a_fsctx_in, fs_context_t *a_fsctx_out);

void fs_context_reference(fs_context_t a_fsctx);

void fs_context_deallocate(fs_context_t a_fsctx);