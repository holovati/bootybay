#pragma once

#include <etl/queue.h>
#include <poll.h>

typedef queue_head_t poll_device_context_t;
typedef decltype(pollfd::events) poll_event_t;

#define poll_device_ctx_init(d) queue_init(d)

kern_return_t poll_record(poll_device_context_t *a_pdc, pollfd **a_pfd);

kern_return_t poll_wakeup(poll_device_context_t *a_pdc, poll_event_t a_event);

poll_event_t poll_enum_wanted_events(poll_device_context_t *a_pdc);
