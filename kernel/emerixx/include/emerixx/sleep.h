#pragma once

#ifdef __cplusplus
extern "C"
{
#endif

typedef void *emerixx_wait_event_t;

typedef struct lock_data *lock_t;

kern_return_t uthread_sleep(emerixx_wait_event_t a_event);

kern_return_t uthread_sleep_timeout(emerixx_wait_event_t a_event, int a_timeout, lock_t a_lock);

void uthread_wakeup(emerixx_wait_event_t a_event);

// kern_return_t pthread_sleep();

// void pthread_wakeup(pthread_t a_thread);

// void pthread_signal(pthread_t, int signum);

#ifdef __cplusplus
}
#endif
