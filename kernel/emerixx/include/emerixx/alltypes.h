#if defined(__NEED_signalstack) && !defined(__DEFINED_signalstack)
#define __DEFINED_signalstack
typedef struct sigaltstack stack_t;
#endif

#if defined(__NEED_rusage) && !defined(__DEFINED_rusage)
#define __DEFINED_rusage
struct rusage;
#endif

#if defined(__NEED_process_group) && !defined(__DEFINED_process_group)
#define __DEFINED_process_group
typedef struct process_group *process_group_t;
#endif

#if defined(__NEED_process) && !defined(__DEFINED_process)
#define __DEFINED_process
typedef struct process *process_t;
#endif

#if defined(__NEED_session) && !defined(__DEFINED_session)
#define __DEFINED_session
typedef struct session *session_t;
#endif

#if defined(__NEED_fs_context) && !defined(__DEFINED_fs_context)
#define __DEFINED_fs_context
typedef struct fs_context *fs_context_t;
#endif

#if defined(__NEED_fd_data) && !defined(__DEFINED_fd_data)
#define __DEFINED_fd_data
typedef struct fd_data *fd_context_t;
#endif

#if defined(__NEED_lock) && !defined(__DEFINED_lock)
#define __DEFINE_lock
#include <kernel/lock.h>
#endif