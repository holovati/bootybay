#include <kernel/thread_status.h>

kern_return_t uthread_get_state(pthread_t a_pthread,
                                enum THREAD_STATE_FLAVOR a_flavor,
                                thread_state_t a_tstate, /* pointer to OUT array */
                                natural_t *count /* IN/OUT */);

kern_return_t uthread_set_state(pthread_t a_pthread,
                                enum THREAD_STATE_FLAVOR a_flavor,
                                thread_state_t a_tstate, /* pointer to OUT array */
                                natural_t count);
