// clang-format off
#include <miscfs/genfs/genfs.h>
#include <miscfs/genfs/genfs_node.h> 
#include <miscfs/specfs/specdev.h> /* for v_rdev */
#include <sys/buf.h>
#include <sys/bufq.h>
#include <sys/device.h>
#include <sys/dirent.h>
#include <sys/fstrans.h>
#include <sys/kauth.h>
#include <sys/module.h>
#include <sys/namei.h>
#include <sys/signalvar.h>
#include <sys/systm.h>
#include <sys/timevar.h>
#include <sys/vnode.h>
#include <sys/vnode_impl.h>
#include <uvm/uvm.h>

#include "../bridge.h"

#ifdef clrbuf
#undef clrbuf
#endif

#define STATIC_ASSERT_STRUCT_OFFSET(type, field)                                                                                   \
        _Static_assert((offsetof(struct netbsdfs_##type, field) == offsetof(struct type, field)),                                  \
                       "NetBSD " #type "::" #field " offset mismatch");                                                            \
        _Static_assert((sizeof(((struct netbsdfs_##type *)0)->field) == sizeof(((struct type *)0)->field)),                        \
                       "NetBSD " #type "::" #field " size mismatch")

#define STATIC_ASSERT_STRUCT_SIZE(type)                                                                                            \
        _Static_assert(sizeof(struct netbsdfs_##type) == sizeof(struct type), "NetBSD " #type " size mismatch")

#define STATIC_ASSERT_ENUM_SIZE(type)                                                                                              \
        _Static_assert(sizeof(enum netbsdfs_##type) == sizeof(enum type), "NetBSD enum type( " #type ") size mismatch")
#define STATIC_ASSERT_ENUM_TYPE(type) _Static_assert((int)NETBSDFS_##type == (int)type, "NetBSD enum value(" #type ") mismatch")

/* ASSUMPTIONS */
STATIC_ASSERT_ENUM_SIZE(vtype);
STATIC_ASSERT_ENUM_TYPE(VNON);
STATIC_ASSERT_ENUM_TYPE(VREG);
STATIC_ASSERT_ENUM_TYPE(VDIR);
STATIC_ASSERT_ENUM_TYPE(VBLK);
STATIC_ASSERT_ENUM_TYPE(VCHR);
STATIC_ASSERT_ENUM_TYPE(VLNK);
STATIC_ASSERT_ENUM_TYPE(VSOCK);
STATIC_ASSERT_ENUM_TYPE(VFIFO);
STATIC_ASSERT_ENUM_TYPE(VBAD);

STATIC_ASSERT_STRUCT_SIZE(timespec);
STATIC_ASSERT_STRUCT_OFFSET(timespec, tv_sec);
STATIC_ASSERT_STRUCT_OFFSET(timespec, tv_nsec);

STATIC_ASSERT_STRUCT_SIZE(vattr);
STATIC_ASSERT_STRUCT_OFFSET(vattr, va_type);
STATIC_ASSERT_STRUCT_OFFSET(vattr, va_mode);
STATIC_ASSERT_STRUCT_OFFSET(vattr, va_nlink);
STATIC_ASSERT_STRUCT_OFFSET(vattr, va_uid);
STATIC_ASSERT_STRUCT_OFFSET(vattr, va_gid);
STATIC_ASSERT_STRUCT_OFFSET(vattr, va_fsid);
STATIC_ASSERT_STRUCT_OFFSET(vattr, va_fileid);
STATIC_ASSERT_STRUCT_OFFSET(vattr, va_size);
STATIC_ASSERT_STRUCT_OFFSET(vattr, va_blocksize);
STATIC_ASSERT_STRUCT_OFFSET(vattr, va_atime);
STATIC_ASSERT_STRUCT_OFFSET(vattr, va_mtime);
STATIC_ASSERT_STRUCT_OFFSET(vattr, va_ctime);
STATIC_ASSERT_STRUCT_OFFSET(vattr, va_birthtime);
STATIC_ASSERT_STRUCT_OFFSET(vattr, va_gen);
STATIC_ASSERT_STRUCT_OFFSET(vattr, va_flags);
STATIC_ASSERT_STRUCT_OFFSET(vattr, va_rdev);
STATIC_ASSERT_STRUCT_OFFSET(vattr, va_bytes);
STATIC_ASSERT_STRUCT_OFFSET(vattr, va_filerev);
STATIC_ASSERT_STRUCT_OFFSET(vattr, va_vaflags);
STATIC_ASSERT_STRUCT_OFFSET(vattr, va_spare);

STATIC_ASSERT_STRUCT_SIZE(iovec);
STATIC_ASSERT_STRUCT_OFFSET(iovec, iov_base);
STATIC_ASSERT_STRUCT_OFFSET(iovec, iov_len);

STATIC_ASSERT_ENUM_SIZE(uio_rw);
STATIC_ASSERT_ENUM_TYPE(UIO_READ);
STATIC_ASSERT_ENUM_TYPE(UIO_WRITE);

STATIC_ASSERT_ENUM_SIZE(uio_seg);
STATIC_ASSERT_ENUM_TYPE(UIO_USERSPACE);
STATIC_ASSERT_ENUM_TYPE(UIO_SYSSPACE);

STATIC_ASSERT_STRUCT_SIZE(uio);
STATIC_ASSERT_STRUCT_OFFSET(uio, uio_iov);
STATIC_ASSERT_STRUCT_OFFSET(uio, uio_iovcnt);
STATIC_ASSERT_STRUCT_OFFSET(uio, uio_offset);
STATIC_ASSERT_STRUCT_OFFSET(uio, uio_resid);
STATIC_ASSERT_STRUCT_OFFSET(uio, uio_rw);
STATIC_ASSERT_STRUCT_OFFSET(uio, uio_vmspace);

/* VARS */
kmutex_t proc_lock;

vmem_t *kmem_va_arena;
vmem_t *kmem_meta_arena;

volatile time_t time_uptime;

u_int module_gen;

int hz, stathz, schedhz, profhz;

char const *panicstr;

size_t coherency_unit = COHERENCY_UNIT;

struct uvm uvm;

struct cpu_info *cpu_info_list;

const char *rootfstype;

int doing_shutdown; /* shutting down */

const namei_simple_flags_t NSM_FOLLOW_NOEMULROOT;

lwp_t lwp0;

int doforce = 1;

device_t root_device;

const enum vtype iftovt_tab[16] = {
    VNON,
    VFIFO,
    VCHR,
    VNON,
    VDIR,
    VNON,
    VBLK,
    VNON,
    VREG,
    VNON,
    VLNK,
    VNON,
    VSOCK,
    VNON,
    VNON,
    VBAD,
};

const int vttoif_tab[9] = {
    0,
    S_IFREG,
    S_IFDIR,
    S_IFBLK,
    S_IFCHR,
    S_IFLNK,
    S_IFSOCK,
    S_IFIFO,
    S_IFMT,
};

volatile time_t time__second __cacheline_aligned = 1;
volatile time_t time__uptime __cacheline_aligned = 1;

int desiredvnodes; /* number of vnodes desired */

struct proc proc0;

volatile time_t time_second;

kmutex_t bufcache_lock;

int vfs_magiclinks;

int vfs_timestamp_precision;

kmutex_t vfs_list_lock;

int (**fifo_vnodeop_p)(void *);

int (**spec_vnodeop_p)(void *);

const struct vnodeopv_entry_desc spec_vnodeop_entries[] = {{&vop_default_desc, vn_default_error},
                                                           {&vop_parsepath_desc, genfs_parsepath},  /* parsepath */
                                                           {&vop_lookup_desc, spec_lookup},         /* lookup */
                                                           {&vop_create_desc, genfs_badop},         /* create */
                                                           {&vop_mknod_desc, genfs_badop},          /* mknod */
                                                           {&vop_open_desc, spec_open},             /* open */
                                                           {&vop_close_desc, spec_close},           /* close */
                                                           {&vop_access_desc, genfs_ebadf},         /* access */
                                                           {&vop_accessx_desc, genfs_ebadf},        /* accessx */
                                                           {&vop_getattr_desc, genfs_ebadf},        /* getattr */
                                                           {&vop_setattr_desc, genfs_ebadf},        /* setattr */
                                                           {&vop_read_desc, spec_read},             /* read */
                                                           {&vop_write_desc, spec_write},           /* write */
                                                           {&vop_fallocate_desc, genfs_eopnotsupp}, /* fallocate */
                                                           {&vop_fdiscard_desc, spec_fdiscard},     /* fdiscard */
                                                           {&vop_fcntl_desc, genfs_fcntl},          /* fcntl */
                                                           {&vop_ioctl_desc, spec_ioctl},           /* ioctl */
                                                           {&vop_poll_desc, spec_poll},             /* poll */
                                                           {&vop_kqfilter_desc, spec_kqfilter},     /* kqfilter */
                                                           {&vop_revoke_desc, genfs_revoke},        /* revoke */
                                                           {&vop_mmap_desc, spec_mmap},             /* mmap */
                                                           {&vop_fsync_desc, spec_fsync},           /* fsync */
                                                           {&vop_seek_desc, spec_seek},             /* seek */
                                                           {&vop_remove_desc, genfs_badop},         /* remove */
                                                           {&vop_link_desc, genfs_badop},           /* link */
                                                           {&vop_rename_desc, genfs_badop},         /* rename */
                                                           {&vop_mkdir_desc, genfs_badop},          /* mkdir */
                                                           {&vop_rmdir_desc, genfs_badop},          /* rmdir */
                                                           {&vop_symlink_desc, genfs_badop},        /* symlink */
                                                           {&vop_readdir_desc, genfs_badop},        /* readdir */
                                                           {&vop_readlink_desc, genfs_badop},       /* readlink */
                                                           {&vop_abortop_desc, genfs_badop},        /* abortop */
                                                           {&vop_inactive_desc, spec_inactive},     /* inactive */
                                                           {&vop_reclaim_desc, spec_reclaim},       /* reclaim */
                                                           {&vop_lock_desc, genfs_lock},            /* lock */
                                                           {&vop_unlock_desc, genfs_unlock},        /* unlock */
                                                           {&vop_bmap_desc, spec_bmap},             /* bmap */
                                                           {&vop_strategy_desc, spec_strategy},     /* strategy */
                                                           {&vop_print_desc, spec_print},           /* print */
                                                           {&vop_islocked_desc, genfs_islocked},    /* islocked */
                                                           {&vop_pathconf_desc, spec_pathconf},     /* pathconf */
                                                           {&vop_advlock_desc, spec_advlock},       /* advlock */
                                                           {&vop_bwrite_desc, vn_bwrite},           /* bwrite */
                                                           {&vop_getpages_desc, genfs_getpages},    /* getpages */
                                                           {&vop_putpages_desc, genfs_putpages},    /* putpages */
                                                           {NULL, NULL}};
const struct vnodeopv_desc spec_vnodeop_opv_desc        = {&spec_vnodeop_p, spec_vnodeop_entries};

int (**dead_vnodeop_p)(void *);
const struct vnodeopv_desc dead_vnodeop_opv_desc = {&spec_vnodeop_p, spec_vnodeop_entries};

const struct vnodeopv_desc fifo_vnodeop_opv_desc = {&spec_vnodeop_p, spec_vnodeop_entries};

#include <sys/mman.h>

#define LIBRUMPUSER
#include <rump-sys/kern.h>
#include <rump/rumpuser.h>

struct cpu_info ci;

struct vnodeopv_desc *d[] = {&spec_vnodeop_opv_desc, NULL};

specnode_t rootspec_vn = {};

vnode_t rootdev_vn = {};

struct specnode msdos_specnode = {};

vnode_t msdosdev_vn = {};


vnode_t *namei_simple_user_return_vn;

struct mount_extra_data
{
        vnode_t m_devvn;
        struct specnode m_specnode;
};

int netbsd_subsys_init()
{

        lwp0.l_cpu = &ci;

        {
                rumpuser_mutex_init(&rump_giantlock, RUMPUSER_MTX_SPIN);
        }

        mutex_init(&vfs_list_lock, MUTEX_DEFAULT, IPL_NONE);

        mutex_init(&bufcache_lock, MUTEX_DEFAULT, IPL_NONE);

        rump_thread_init();

        rump_biglock_init();

        rump_scheduler_init(0);

        pool_subsystem_init();

        kcpuset_sysinit();

        mi_cpu_init();

        vfsinit();

        vfs_opv_init(d);

        // For root ext2fs
        rootdev_vn.v_type  = VBLK;

        rootvp             = &rootdev_vn;
        rootvp->v_specnode = &rootspec_vn;
        rootvp->v_rdev     = makedev(13, 2);
        rootvp->v_op       = spec_vnodeop_p;

        // for /boot FAT
        msdosdev_vn.v_type = VBLK;
        msdosdev_vn.v_specnode = &msdos_specnode;
        msdosdev_vn.v_rdev = makedev(13, 1);
        msdosdev_vn.v_op  = spec_vnodeop_p;
  
        return 0;
}

#define mnt_rootdevvp mnt_renamelock

static struct mount *s_root_mp;

int netbsd_mountroot()
{
        rootfstype = "ext2fs";

        struct vfsops *v;
        mutex_enter(&vfs_list_lock);
        LIST_FOREACH(v, &vfs_list, vfs_list)
        {
                if (v->vfs_mountroot == NULL)
                        continue;

                printf("mountroot: trying %s...\n", v->vfs_name);

                if (strcmp(rootfstype, v->vfs_name) != 0)
                {
                        continue;
                }

                v->vfs_refcount++;
                mutex_exit(&vfs_list_lock);
                int error = (*v->vfs_mountroot)();
                mutex_enter(&vfs_list_lock);
                v->vfs_refcount--;
                if (!error)
                {
                        printf("root file system type: %s\n", v->vfs_name);
                        break;
                }
        }
        mutex_exit(&vfs_list_lock);

        s_root_mp->mnt_rootdevvp = &rootdev_vn;

        return 0;
}

#define MSDOSFSMNT_VERSION 3

/*
 *  Arguments to mount MSDOS filesystems.
 */
struct msdosfs_args {
	char	*fspec;		/* blocks special holding the fs to mount */
	struct	export_args30 _pad1; /* compat with old userland tools */
	uid_t	uid;		/* uid that owns msdosfs files */
	gid_t	gid;		/* gid that owns msdosfs files */
	mode_t  mask;		/* mask to be applied for msdosfs perms */
	int	flags;		/* see below */

	/* Following items added after versioning support */
	int	version;	/* version of the struct */
#define MSDOSFSMNT_VERSION	3
	mode_t  dirmask;	/* v2: mask to be applied for msdosfs perms */
	int	gmtoff;		/* v3: offset from UTC in seconds */
};


/*
 * Msdosfs mount options:
 */
#define	MSDOSFSMNT_SHORTNAME	1	/* Force old DOS short names only */
#define	MSDOSFSMNT_LONGNAME	2	/* Force Win'95 long names */
#define	MSDOSFSMNT_NOWIN95	4	/* Completely ignore Win95 entries */
#define	MSDOSFSMNT_GEMDOSFS	8	/* This is a GEMDOS-flavour */
#define MSDOSFSMNT_VERSIONED	16	/* Struct is versioned */
#define MSDOSFSMNT_UTF8		32	/* Use UTF8 filenames */

/* All flags above: */
#define	MSDOSFSMNT_MNTOPT \
	(MSDOSFSMNT_SHORTNAME|MSDOSFSMNT_LONGNAME|MSDOSFSMNT_NOWIN95 \
	 |MSDOSFSMNT_GEMDOSFS|MSDOSFSMNT_VERSIONED|MSDOSFSMNT_UTF8)

int netbsd_mount_msdosfs(unsigned a_major, unsigned a_minor, void **netbsd_mount_out)
{
        struct mount *msdosmount;
        int error = vfs_rootmountalloc("msdos", NULL, &msdosmount);
        
        if (error)
        {
                return error;
        }

        struct mount_extra_data *devinfo = netbsdfs_vfs_mount_get_extra_data_ptr(msdosmount, sizeof(struct mount_extra_data));

        assert(devinfo != NULL);

        devinfo->m_devvn.v_type = VBLK;
        devinfo->m_devvn.v_specnode = &devinfo->m_specnode;
        devinfo->m_devvn.v_rdev = makedev(a_major, a_minor);
        devinfo->m_devvn.v_op  = spec_vnodeop_p;

        msdosmount->mnt_rootdevvp = (void*)&devinfo->m_devvn;

	struct msdosfs_args args;

	memset(&args, 0, sizeof(args));
	args.fspec = "/dev/sda1";
        args.flags = MSDOSFSMNT_MNTOPT & ~(MSDOSFSMNT_GEMDOSFS | MSDOSFSMNT_NOWIN95);
	args.version = MSDOSFSMNT_VERSION;

        size_t data_len = sizeof(args);

        namei_simple_user_return_vn = &devinfo->m_devvn; // The mount code uses this, we should add lock for global state
        error = msdosmount->mnt_op->vfs_mount(msdosmount, "/boot", &args, &data_len);
        namei_simple_user_return_vn = NULL;

        if (error == 0)
        {
                *netbsd_mount_out = msdosmount;
        }

        //vnode_t *msdos_rootvp = NULL;
        //msdosmount->mnt_op->vfs_root(msdosmount, LK_EXCLUSIVE, &msdos_rootvp);
        return error;
}

/* MISC */
struct cpu_info *x86_curcpu() { return curlwp->l_cpu; }

static modinfo_t module_dummy;
__link_set_add_rodata(modules, module_dummy);

void module_init_class(modclass_t modclass)
{
        __link_set_decl(modules, modinfo_t);
        modinfo_t *const *mip;

        __link_set_foreach(mip, modules)
        {

                if (*mip == &module_dummy)
                {
                        continue;
                }
                (*mip)->mi_modcmd(MODULE_CMD_INIT, NULL);
        }
}

void __nbsd_printf(const char *fmt, ...)
{
        va_list args;
        va_start(args, fmt);
        netbsdfs_vprintf(fmt, args);
        va_end(args);
}

int module_autoload(const char *name, modclass_t class) { netbsdfs_noimp(__FUNCTION__); }

int getdisksize(struct vnode *vp, uint64_t *numsecp, unsigned int *secsizep) 
{ 
        return 1; // Just fail, msdos does not need it anyway
        netbsdfs_noimp_warn(__FUNCTION__); 
}

int getdiskinfo(struct vnode *vp, struct dkwedge_info *dkw) { netbsdfs_noimp(__FUNCTION__); }

#define TIMESPEC_TO_TIMEVAL(tv, ts) ( \
	(tv)->tv_sec = (ts)->tv_sec, \
	(tv)->tv_usec = (ts)->tv_nsec / 1000, \
	(void)0 )


void getnanotime(struct timespec *tsp) 
{ 
        netbsdfs_get_timespec(tsp);
}

void microtime(struct timeval *tvp) 
{ 
        struct timespec ts;
        netbsdfs_get_timespec(&ts);
        TIMESPEC_TO_TIMEVAL(tvp, &ts);        
}

void knote(struct klist *a, long b) { netbsdfs_noimp_warn(__FUNCTION__); }

lwp_t *x86_curlwp(void) { return &lwp0; }

unsigned int popcount32(unsigned int b)
{
        int c = 0;

        while (b)
        {
                b &= b - 1;
                c++;
        }

        return c;
}

int log() { netbsdfs_noimp(__FUNCTION__); }

int eopnotsupp(void) { netbsdfs_noimp(__FUNCTION__); }

uint32_t cprng_fast32(void) { netbsdfs_noimp(__FUNCTION__); }

uint64_t cprng_fast64(void) { netbsdfs_noimp(__FUNCTION__); }

const struct bdevsw *bdevsw_lookup(dev_t dev) 
{ 
        return 1;
}

devclass_t device_class(device_t dev) { return DV_DISK; }

void getmicrouptime(struct timeval *tvp) { netbsdfs_noimp_warn(__FUNCTION__); }

void panic(const char *s, ...) { netbsdfs_noimp(__FUNCTION__); }

void vpanic(const char *fmt, va_list ap) { netbsdfs_noimp(__FUNCTION__); }

void uprintf(const char *s, ...) { netbsdfs_noimp(__FUNCTION__); }

int uvm_km_kmem_alloc(vmem_t *vm, vmem_size_t size, vm_flag_t flags, vmem_addr_t *addr)
{
        int retval = netbsdfs_submap_alloc(vm, size, addr);
        return retval;
}

void uvm_km_kmem_free(vmem_t *vm, vmem_addr_t addr, vmem_size_t size) { netbsdfs_noimp(__FUNCTION__); }

int vmem_alloc(vmem_t *vm, vmem_size_t size, vm_flag_t flags, vmem_addr_t *addrp) { return uvm_km_kmem_alloc(vm, size, 0, addrp); }

void vmem_free(vmem_t *vm, vmem_addr_t addr, vmem_size_t size) { uvm_km_kmem_free(vm, addr, size); }

void aprint_verbose(const char *format, ...) { netbsdfs_noimp(__FUNCTION__); }
void aprint_debug(const char *format, ...) { netbsdfs_noimp(__FUNCTION__); }

struct pathbuf *pathbuf_create(const char *path) { netbsdfs_noimp(__FUNCTION__); }

void pathbuf_destroy(struct pathbuf *pb) { netbsdfs_noimp(__FUNCTION__); }

kauth_cred_t kauth_cred_hold(kauth_cred_t cred) { netbsdfs_noimp(__FUNCTION__); }

long lwp_pctr(void) { return 1; }

void kauth_cred_free(kauth_cred_t cred) { netbsdfs_noimp(__FUNCTION__); }

kauth_listener_t kauth_listen_scope(const char *a, kauth_scope_callback_t b, void *c)
{
        netbsdfs_noimp_warn(__FUNCTION__);
        return NULL;
}

void kauth_unlisten_scope(kauth_listener_t listener) { netbsdfs_noimp(__FUNCTION__); }

uid_t kauth_cred_getuid(kauth_cred_t cred)
{
        netbsdfs_noimp_warn(__FUNCTION__);
        return 0;
}

kauth_cred_t kauth_cred_get(void) { netbsdfs_noimp(__FUNCTION__); }

kauth_action_t kauth_accmode_to_action(accmode_t accmode)
{
        netbsdfs_noimp_warn(__FUNCTION__);
        return 0;
}

int kauth_authorize_vnode(kauth_cred_t a, kauth_action_t b, struct vnode *c, struct vnode *d, int e) { return 0; }

uid_t kauth_cred_geteuid(kauth_cred_t a) { return 0; }

int kauth_authorize_system(kauth_cred_t a, kauth_action_t b, enum kauth_system_req c, void *d, void *e, void *f) { return 0; }

int uiomove(void *p, size_t s, struct uio *u)
{
        if ((u->uio_iovcnt == 1) && (s <= u->uio_resid))
        {
                if (u->uio_rw == UIO_READ)
                {
                        memcpy(u->uio_iov[0].iov_base, p, s);
                }
                else
                {
                        memcpy(p, u->uio_iov[0].iov_base, s);
                }
                u->uio_iov[0].iov_base += s;
                u->uio_offset += s;
                u->uio_resid -= s;
                return 0;
        }
        
        netbsdfs_noimp(__FUNCTION__);
}

int ubc_uiomove(struct uvm_object *uobj, struct uio *uio, vsize_t todo, int advice, int flags)
{
        struct vnode *vp          = (struct vnode *)(((char *)uobj) - (offsetof(struct vnode, v_uobj)));
        struct netbsdfs_uio *nuio = (struct netbsdfs_uio *)(uio);

        int error = netbsdfs_vfs_vnode_ubc_uiomove(vp, nuio, todo);

        return error;
}

void ubc_zerorange(struct uvm_object *uobj, off_t off, size_t len, int flags) 
{ 
        struct vnode *vp = (struct vnode *)(((char *)uobj) - (offsetof(struct vnode, v_uobj)));

        netbsdfs_vfs_vnode_zero_range(vp, off, len);
}

void uio_setup_sysspace(struct uio *uio) { netbsdfs_noimp_warn(__FUNCTION__); }

int extattr_check_cred(struct vnode *vp, int attrspace, kauth_cred_t cred, int access) { netbsdfs_noimp(__FUNCTION__); }

int sysctl_createv(struct sysctllog **log,
                   int cflags,
                   const struct sysctlnode **rnode,
                   const struct sysctlnode **cnode,
                   int flags,
                   int type,
                   const char *namep,
                   const char *descr,
                   void *func,
                   u_quad_t qv,
                   void *newp,
                   size_t newlen,
                   ...)
{
        netbsdfs_noimp_warn(__FUNCTION__);
}

int sysctl_copyinstr(struct lwp *l, const void *uaddr, void *kaddr, size_t len, size_t *done) { netbsdfs_noimp(__FUNCTION__); }

int sysctl_copyout(struct lwp *l, const void *kaddr, void *uaddr, size_t len) { netbsdfs_noimp(__FUNCTION__); }

void sysctl_unlock(void) { netbsdfs_noimp(__FUNCTION__); }

void sysctl_relock(void) { netbsdfs_noimp(__FUNCTION__); }

int sysctl_notavail(SYSCTLFN_ARGS) { netbsdfs_noimp(__FUNCTION__); }

void cache_enter_id(struct vnode *vp, mode_t mode, uid_t uid, gid_t gid, bool valid) { netbsdfs_noimp(__FUNCTION__); }

int spec_strategy(void *v)
{
        struct vop_strategy_args *a = v;
        buf_t *bp                   = a->a_bp;
        vnode_t *vp                 = a->a_vp;
        // bp->b_data                  = ((void *)(vp->v_specnode->sn_dev)) + (bp->b_blkno << 9 /*BSHIFT*/);

        int error = netbsdfs_block_read(major(bp->b_dev), minor(bp->b_dev), bp->b_blkno, bp->b_bufsize, &bp->b_data);

        bp->b_oflags |= BO_DONE;

        return error;
}

int spec_bmap(void *v) { netbsdfs_noimp(__FUNCTION__); }

int spec_pathconf(void *v) { netbsdfs_noimp(__FUNCTION__); }

int spec_advlock(void *v) { netbsdfs_noimp(__FUNCTION__); }

int spec_mmap(void *v) { netbsdfs_noimp(__FUNCTION__); }

int spec_kqfilter(void *v) { netbsdfs_noimp(__FUNCTION__); }

int spec_poll(void *v) { netbsdfs_noimp(__FUNCTION__); }

int spec_ioctl(void *v) { netbsdfs_noimp(__FUNCTION__); }

int spec_fdiscard(void *v) { netbsdfs_noimp(__FUNCTION__); }

int spec_fsync(void *v) 
{ 
        struct vop_fsync_args *a = (struct vop_fsync_args *)v;
        /*
	a.a_vp = vp;
	a.a_cred = cred;
	a.a_flags = flags;
	a.a_offlo = offlo;
	a.a_offhi = offhi;
        */

       unsigned devmajor = major(a->a_vp->v_rdev);
       unsigned devminor = minor(a->a_vp->v_rdev);
       
       netbsdfs_spec_flush_buffers(devmajor, devminor);
       
       return 0; 
}

int spec_open(void *v) 
{ 
        netbsdfs_noimp_warn(__FUNCTION__); 
        return 0;
}

int spec_lookup(void *v) { netbsdfs_noimp(__FUNCTION__); }

int spec_close(void *v) { netbsdfs_noimp(__FUNCTION__); }

int spec_read(void *v) { netbsdfs_noimp(__FUNCTION__); }

int spec_write(void *v) { netbsdfs_noimp(__FUNCTION__); }

int spec_inactive(void *v) { netbsdfs_noimp(__FUNCTION__); }

int spec_reclaim(void *v) { netbsdfs_noimp(__FUNCTION__); }

int spec_print(void *v) { netbsdfs_noimp(__FUNCTION__); }

void spec_node_init(vnode_t *vp, dev_t rdev) { netbsdfs_noimp_warn(__FUNCTION__); }

void spec_node_setmountedfs(vnode_t *devvp, struct mount *mp) { netbsdfs_noimp_warn(__FUNCTION__); }

struct mount *spec_node_getmountedfs(vnode_t *devvp) 
{ 
        netbsdfs_noimp_warn(__FUNCTION__); 
        return NULL; 
}

/* GENFS */

/*ARGSUSED*/
int genfs_ebadf(void *v) { return (EBADF); }

int genfs_einval(void *v) { return (EINVAL); }

int genfs_kqfilter(void *v) { netbsdfs_noimp(__FUNCTION__); }

int genfs_pathconf(void *v) { netbsdfs_noimp(__FUNCTION__); }

int genfs_access(void *v) { netbsdfs_noimp(__FUNCTION__); }

int genfs_mmap(void *v) { netbsdfs_noimp(__FUNCTION__); }

int genfs_poll(void *v) { netbsdfs_noimp(__FUNCTION__); }

int genfs_enoioctl(void *v) { netbsdfs_noimp(__FUNCTION__); }

int genfs_parsepath(void *v) { netbsdfs_noimp(__FUNCTION__); }

int genfs_revoke(void *v) { netbsdfs_noimp(__FUNCTION__); }

int genfs_seek(void *v) { netbsdfs_noimp(__FUNCTION__); }

int genfs_abortop(void *v) { netbsdfs_noimp(__FUNCTION__); }

int genfs_nullop(void *v) { netbsdfs_noimp(__FUNCTION__); }

int genfs_getpages(void *v) { netbsdfs_noimp(__FUNCTION__); }

int genfs_putpages(void *v) 
{ 
        netbsdfs_noimp_warn(__FUNCTION__); 
	
        struct vop_putpages_args /* {
		struct vnode *a_vp;
		voff_t a_offlo;
		voff_t a_offhi;
		int a_flags;
	} */ * const ap = v;

        krwlock_t * const slock = ap->a_vp->v_uobj.vmobjlock;
        assert(rw_write_held(slock));
        rw_exit(slock);
        return 0;
}

int genfs_null_putpages(void *v) { netbsdfs_noimp(__FUNCTION__); }

int genfs_badop(void *v) { netbsdfs_noimp(__FUNCTION__); }

int genfs_accessx(void *v) { netbsdfs_noimp(__FUNCTION__); }

int genfs_fcntl(void *v) { netbsdfs_noimp(__FUNCTION__); }

int genfs_lock(void *v) { netbsdfs_noimp(__FUNCTION__); }

int genfs_unlock(void *v)
{
        netbsdfs_noimp_warn(__FUNCTION__);
        return 0;
}

int genfs_islocked(void *v) { return LK_EXCLUSIVE; }

int genfs_eopnotsupp(void *v) { netbsdfs_noimp_warn(__FUNCTION__); }

void genfs_renamelock_exit(struct mount *mp) { netbsdfs_noimp(__FUNCTION__); }

int genfs_renamelock_enter(struct mount *mp) { netbsdfs_noimp(__FUNCTION__); }

int genfs_suspendctl(struct mount *mp, int cmd) { netbsdfs_noimp(__FUNCTION__); }

int genfs_can_sticky(vnode_t *vp, kauth_cred_t cred, uid_t dir_uid, uid_t file_uid) { netbsdfs_noimp(__FUNCTION__); }

int genfs_can_chown(vnode_t *vp, kauth_cred_t cred, uid_t cur_uid, gid_t cur_gid, uid_t new_uid, gid_t new_gid)
{
        netbsdfs_noimp(__FUNCTION__);
}

int genfs_can_chmod(vnode_t *vp, kauth_cred_t cred, uid_t cur_uid, gid_t cur_gid, mode_t new_mode) { return 0; }

int genfs_can_chtimes(vnode_t *vp, kauth_cred_t cred, uid_t owner_uid, u_int vaflags) { return 0; }

int genfs_can_chflags(vnode_t *vp, kauth_cred_t cred, uid_t owner_uid, bool changing_sysflags) { return 0; }

int genfs_can_access(
    vnode_t *vp, kauth_cred_t cred, uid_t file_uid, gid_t file_gid, mode_t file_mode, struct acl *acl, accmode_t accmode)
{
        return 0;
}


int genfs_ufslike_rename_check_permitted(kauth_cred_t cred,
                                         struct vnode *fdvp,
                                         mode_t fdmode,
                                         uid_t fduid,
                                         struct vnode *fvp,
                                         uid_t fuid,
                                         struct vnode *tdvp,
                                         mode_t tdmode,
                                         uid_t tduid,
                                         struct vnode *tvp,
                                         uid_t tuid)
{
        netbsdfs_noimp(__FUNCTION__);
}

int genfs_ufslike_rename_check_possible(unsigned long fdflags,
                                        unsigned long fflags,
                                        unsigned long tdflags,
                                        unsigned long tflags,
                                        bool clobber_p,
                                        unsigned long immutable,
                                        unsigned long append)
{
        netbsdfs_noimp(__FUNCTION__);
}

int genfs_ufslike_remove_check_possible(unsigned long dflags, unsigned long flags, unsigned long immutable, unsigned long append)
{
        netbsdfs_noimp(__FUNCTION__);
}

int genfs_insane_rename(void *v,
                        int (*sane_rename)(struct vnode *fdvp,
                                           struct componentname *fcnp,
                                           struct vnode *tdvp,
                                           struct componentname *tcnp,
                                           kauth_cred_t cred,
                                           bool posixly_correct))
{
	struct vop_rename_args /* {
		struct vnode *a_fdvp;
		struct vnode *a_fvp;
		struct componentname *a_fcnp;
		struct vnode *a_tdvp;
		struct vnode *a_tvp;
		struct componentname *a_tcnp;
	} */ *ap = v;
	struct vnode *fdvp = ap->a_fdvp;
	struct vnode *fvp = ap->a_fvp;
	struct componentname *fcnp = ap->a_fcnp;
	struct vnode *tdvp = ap->a_tdvp;
	struct vnode *tvp = ap->a_tvp;
	struct componentname *tcnp = ap->a_tcnp;
	kauth_cred_t cred;
	int error;
#if 0
	KASSERT(fdvp != NULL);
	KASSERT(fvp != NULL);
	KASSERT(fcnp != NULL);
	KASSERT(fcnp->cn_nameptr != NULL);
	KASSERT(tdvp != NULL);
	KASSERT(tcnp != NULL);
	KASSERT(fcnp->cn_nameptr != NULL);
	/* KASSERT(VOP_ISLOCKED(fdvp) != LK_EXCLUSIVE); */
	/* KASSERT(VOP_ISLOCKED(fvp) != LK_EXCLUSIVE); */
	KASSERT(VOP_ISLOCKED(tdvp) == LK_EXCLUSIVE);
	KASSERT((tvp == NULL) || (VOP_ISLOCKED(tvp) == LK_EXCLUSIVE));
	KASSERT(fdvp->v_type == VDIR);
	KASSERT(tdvp->v_type == VDIR);

	cred = fcnp->cn_cred;

	/*
	 * XXX Want a better equality test.  `tcnp->cn_cred == cred'
	 * hoses p2k because puffs transmits the creds separately and
	 * allocates distinct but equivalent structures for them.
	 */
	KASSERT(kauth_cred_uidmatch(cred, tcnp->cn_cred));

	/*
	 * Sanitize our world from the VFS insanity.  Unlock the target
	 * directory and node, which are locked.  Release the children,
	 * which are referenced, since we'll be looking them up again
	 * later.
	 */

	VOP_UNLOCK(tdvp);
	if ((tvp != NULL) && (tvp != tdvp))
		VOP_UNLOCK(tvp);

	vrele(fvp);
	if (tvp != NULL)
		vrele(tvp);
#endif
	error = (*sane_rename)(fdvp, fcnp, tdvp, tcnp, cred, false);

#if 0
	/*
	 * All done, whether with success or failure.  Release the
	 * directory nodes now, as the caller expects from the VFS
	 * protocol.
	 */
	vrele(fdvp);
	vrele(tdvp);
#endif
	return error;
}

int genfs_sane_rename(const struct genfs_rename_ops *ops,
                      struct vnode *fdvp,
                      struct componentname *fcnp,
                      void *fde,
                      struct vnode *tdvp,
                      struct componentname *tcnp,
                      void *tde,
                      kauth_cred_t cred,
                      bool posixly_correct)
{
	struct mount *mp;
	struct vnode *fvp = NULL, *tvp = NULL;
	nlink_t tvp_new_nlink = 0;
	int error;

	KASSERT(ops != NULL);
	KASSERT(fdvp != NULL);
	KASSERT(fcnp != NULL);
	KASSERT(tdvp != NULL);
	KASSERT(tcnp != NULL);
	/* KASSERT(VOP_ISLOCKED(fdvp) != LK_EXCLUSIVE); */
	/* KASSERT(VOP_ISLOCKED(tdvp) != LK_EXCLUSIVE); */
	KASSERT(fdvp->v_type == VDIR);
	KASSERT(tdvp->v_type == VDIR);
	KASSERT(fdvp->v_mount == tdvp->v_mount);
	KASSERT(fcnp != tcnp);
	KASSERT(fcnp->cn_nameiop == DELETE);
	KASSERT(tcnp->cn_nameiop == RENAME);

#if 0
        /* XXX Want a better equality test.  */
	KASSERT(kauth_cred_uidmatch(cred, fcnp->cn_cred));
	KASSERT(kauth_cred_uidmatch(cred, tcnp->cn_cred));

	mp = fdvp->v_mount;
	KASSERT(mp != NULL);
	KASSERT(mp == tdvp->v_mount);
	/* XXX How can we be sure this stays true?  */
	KASSERT((mp->mnt_flag & MNT_RDONLY) == 0);

	/* Reject rename("x/..", ...) and rename(..., "x/..") early.  */
	if ((fcnp->cn_flags | tcnp->cn_flags) & ISDOTDOT)
		return EINVAL;	/* XXX EISDIR?  */

	error = genfs_rename_enter(ops, mp, cred,
	    fdvp, fcnp, fde, &fvp,
	    tdvp, tcnp, tde, &tvp);
	if (error)
		return error;

	/*
	 * Check that everything is locked and looks right.
	 */
	KASSERT(fvp != NULL);
	KASSERT(VOP_ISLOCKED(fdvp) == LK_EXCLUSIVE);
	KASSERT(VOP_ISLOCKED(fvp) == LK_EXCLUSIVE);
	KASSERT(VOP_ISLOCKED(tdvp) == LK_EXCLUSIVE);
	KASSERT((tvp == NULL) || (VOP_ISLOCKED(tvp) == LK_EXCLUSIVE));

	/*
	 * If the source and destination are the same object, we need
	 * only at most delete the source entry.  We are guaranteed at
	 * this point that the entries are distinct.
	 */
	if (fvp == tvp) {
		KASSERT(tvp != NULL);
		if (fvp->v_type == VDIR)
			/* XXX This shouldn't be possible.  */
			error = EINVAL;
		else if (posixly_correct)
			/* POSIX sez to leave them alone.  */
			error = 0;
		else if ((fdvp == tdvp) &&
		    (fcnp->cn_namelen == tcnp->cn_namelen) &&
		    (memcmp(fcnp->cn_nameptr, tcnp->cn_nameptr,
			fcnp->cn_namelen) == 0))
			/* Renaming an entry over itself does nothing.  */
			error = 0;
		else {
			/* XXX Can't use VOP_REMOVE because of locking.  */
			error = genfs_rename_remove(ops, mp, cred,
			    fdvp, fcnp, fde, fvp, &tvp_new_nlink);
			VN_KNOTE(fdvp, NOTE_WRITE);
			VN_KNOTE(fvp,
			    tvp_new_nlink == 0 ? NOTE_DELETE : NOTE_LINK);
		}
		goto out;
	}
	KASSERT(fvp != tvp);
	KASSERT((fdvp != tdvp) ||
	    (fcnp->cn_namelen != tcnp->cn_namelen) ||
	    (memcmp(fcnp->cn_nameptr, tcnp->cn_nameptr, fcnp->cn_namelen)
		!= 0));

	/*
	 * If the target exists, refuse to rename a directory over a
	 * non-directory or vice versa, or to clobber a non-empty
	 * directory.
	 */
	if (tvp != NULL) {
		if (fvp->v_type == VDIR && tvp->v_type == VDIR)
			error =
			    (ops->gro_directory_empty_p(mp, cred, tvp, tdvp)?
				0 : ENOTEMPTY);
		else if (fvp->v_type == VDIR && tvp->v_type != VDIR)
			error = ENOTDIR;
		else if (fvp->v_type != VDIR && tvp->v_type == VDIR)
			error = EISDIR;
		else
			error = 0;
		if (error)
			goto out;
		KASSERT((fvp->v_type == VDIR) == (tvp->v_type == VDIR));
	}

	/*
	 * Authorize the rename.
	 */
	error = ops->gro_rename_check_possible(mp, fdvp, fvp, tdvp, tvp);
	if (error)
		goto out;
	error = ops->gro_rename_check_permitted(mp, cred, fdvp, fvp, tdvp, tvp);
	error = kauth_authorize_vnode(cred, KAUTH_VNODE_DELETE, fvp, fdvp,
	    error);
	error = kauth_authorize_vnode(cred, KAUTH_VNODE_RENAME, tvp, tdvp,
	    error);
	if (error)
		goto out;
#endif


#if 0
	if (fdvp == tdvp)
        {
		error = genfs_rename_enter_common(ops, mp, cred, fdvp,
		    fcnp, fde_ret, fvp_ret,
		    tcnp, tde_ret, tvp_ret);
        }
        else
        {
		error = genfs_rename_enter_separate(ops, mp, cred,
		    fdvp, fcnp, fde_ret, fvp_ret,
		    tdvp, tcnp, tde_ret, tvp_ret);
        }
#endif

        fcnp->cn_flags = ISLASTCN;
        error = ops->gro_lookup(fdvp->v_mount, fdvp, fcnp, fde, &fvp);

        if (error) 
        {
                return error;
        }

        tcnp->cn_flags = ISLASTCN;
        error = ops->gro_lookup(tdvp->v_mount, tdvp, tcnp, tde, &tvp);

        if (error && !((error == ENOENT) && (tvp == NULL)))
        {
                goto out;
        }
        else if (error)
        {
                error = 0;
        }

        
        if (fvp == tvp)
        {
                error = 0; // Posix says so
                goto out;
        }       

	if (fvp->v_type == VDIR && (tvp && (tvp->v_type == VDIR)))
        {
		error = (ops->gro_directory_empty_p(mp, cred, tvp, tdvp) ? 0 : ENOTEMPTY);
        }

        if (error)
        {
                goto out;
        }

	/*
	 * Everything is hunky-dory.  Shuffle the directory entries.
	 */
	error = ops->gro_rename(mp, cred, fdvp, fcnp, fde, fvp, tdvp, tcnp, tde, tvp, &tvp_new_nlink);
#if 0
	if (error)
		goto out;

	/* Success!  */
	genfs_rename_knote(fdvp, fvp, tdvp, tvp, tvp_new_nlink);

out:
	genfs_rename_exit(ops, mp, fdvp, fvp, tdvp, tvp);
#endif

out:
        netbsdfs_vfs_vnode_rel(fvp);

        if (tvp)
        {
                netbsdfs_vfs_vnode_rel(tvp);
        }

	return error;

}

void genfs_rename_cache_purge(struct vnode *fdvp, struct vnode *fvp, struct vnode *tdvp, struct vnode *tvp)
{
        netbsdfs_noimp(__FUNCTION__);
}

void genfs_size(struct vnode *vp, off_t size, off_t *eobp, int flags) 
{
        int bsize = 1 << vp->v_mount->mnt_fs_bshift;
	*eobp = (size + bsize - 1) & ~(bsize - 1);
}


void genfs_node_init(struct vnode *vp, const struct genfs_ops *ops) 
{ 
	struct genfs_node *gp = VTOG(vp);
	//rw_init(&gp->g_glock);
	gp->g_op = ops;
}

void genfs_node_destroy(struct vnode *v) { netbsdfs_noimp_warn(__FUNCTION__); }

void genfs_gop_putrange(struct vnode *v, off_t o, off_t *po, off_t *op0) { netbsdfs_noimp(__FUNCTION__); }

int genfs_gop_write(struct vnode *v, struct vm_page **vmp, int i, int e) { netbsdfs_noimp(__FUNCTION__); }

int genfs_gop_write_rwmap(struct vnode *v, struct vm_page **vmp, int i, int e) { netbsdfs_noimp(__FUNCTION__); }

int genfs_compat_gop_write(struct vnode *v, struct vm_page **vmp, int i, int e) { netbsdfs_noimp(__FUNCTION__); }

void genfs_directio(struct vnode *v, struct uio *u, int i) { netbsdfs_noimp(__FUNCTION__); }

void genfs_node_wrlock(struct vnode *v) { netbsdfs_noimp(__FUNCTION__); }

void genfs_node_rdlock(struct vnode *v) { netbsdfs_noimp(__FUNCTION__); }

int genfs_node_rdtrylock(struct vnode *v) { netbsdfs_noimp(__FUNCTION__); }

void genfs_node_unlock(struct vnode *v) { netbsdfs_noimp(__FUNCTION__); }

int genfs_node_wrlocked(struct vnode *v) { netbsdfs_noimp(__FUNCTION__); }

int genfs_ufslike_remove_check_permitted(
    kauth_cred_t cred, struct vnode *dvp, mode_t dmode, uid_t duid, struct vnode *vp, uid_t uid)
{
        netbsdfs_noimp(__FUNCTION__);
}


/* UVM */
void uvm_page_unbusy(struct vm_page **vmp, int v) { netbsdfs_noimp(__FUNCTION__); }

void uvm_pagemarkdirty(struct vm_page *vmp, unsigned int a) { netbsdfs_noimp(__FUNCTION__); }

void uvm_pagelock(struct vm_page *vmp) { netbsdfs_noimp(__FUNCTION__); }

void uvm_pageunlock(struct vm_page *vmp) { netbsdfs_noimp(__FUNCTION__); }

void uvm_pageactivate(struct vm_page *vmp) { netbsdfs_noimp(__FUNCTION__); }

void uvm_vnp_setsize(struct vnode *v, voff_t newsz) 
{ 
        v->v_size = newsz;
        netbsdfs_vfs_vnode_setsize(v, newsz);
}

void uvm_vnp_setwritesize(struct vnode *v, voff_t newsz) 
{       
        v->v_size = newsz;
        netbsdfs_vfs_vnode_setsize(v, newsz);
}

/* VFS */
int netbsd_vfs_root(void *a_netbsd_mount, void **a_netbsd_vnode_out)
{
        struct mount *mp = a_netbsd_mount;
        return mp->mnt_op->vfs_root(mp, LK_NONE, a_netbsd_vnode_out);
}

int netbsd_vfs_statfs(void *a_netbsd_mount, void *user_data)
{
        struct mount *mp = a_netbsd_mount;

        int error = mp->mnt_op->vfs_statvfs(mp, &mp->mnt_stat);

        if (error == 0)
        {
                error = netbsdfs_vfs_statfs_callback(a_netbsd_mount,
                                                     user_data,
                                                     mp->mnt_stat.f_bsize,   /* file system block size */
                                                     mp->mnt_stat.f_frsize,  /* fundamental file system block size */
                                                     mp->mnt_stat.f_iosize,  /* optimal file system block size */
                                                     mp->mnt_stat.f_blocks,  /* number of blocks in file system, */
                                                     mp->mnt_stat.f_bfree,   /* free blocks avail in file system */
                                                     mp->mnt_stat.f_bavail,  /* free blocks avail to non-root */
                                                     mp->mnt_stat.f_bresvd,  /* blocks reserved for root */
                                                     mp->mnt_stat.f_files,   /* total file nodes in file system */
                                                     mp->mnt_stat.f_ffree,   /* free file nodes in file system */
                                                     mp->mnt_stat.f_favail,  /* free file nodes avail to non-root */
                                                     mp->mnt_stat.f_fresvd,  /* file nodes reserved for root */
                                                     mp->mnt_stat.f_fsid,    /* Posix compatible fsid */
                                                     mp->mnt_stat.f_namemax, /* maximum filename length */
                                                     mp->mnt_stat.f_fstypename /* fs type name */);
        }

        return error;
}

int netbsd_vfs_sync(void *a_netbsd_mount, int a_wait)
{
        struct mount *mp = a_netbsd_mount;

        int error = mp->mnt_op->vfs_sync(mp, a_wait, FSCRED);

        return error;
}

int netbsd_vnode_lookup(void *a_netbsd_vnode, char const *a_cn, unsigned long a_cnlen, int a_op, int a_flags, void **a_netbsd_vnode_out)
{
        struct componentname cn;

        cn.cn_nameiop = a_op;
        cn.cn_flags   = a_flags;
        cn.cn_cred    = FSCRED;
        cn.cn_namelen = a_cnlen;
        cn.cn_nameptr = a_cn;

        int netbsd_error = VOP_LOOKUP((vnode_t *)a_netbsd_vnode, a_netbsd_vnode_out, &cn);

        if (netbsd_error == EJUSTRETURN)
        {
                // Is really  - ENOENT; convert it to ENOENT as expected by the vfs system
                netbsd_error = -netbsd_error;
        }

        return netbsd_error;
}

int netbsd_vnode_create(void *a_netbsd_vnode, char const *a_cn, unsigned long a_cnlen, struct netbsdfs_vattr *a_vattr, void** a_netbsd_vnode_out)
{
        struct vnode *vn = a_netbsd_vnode;
        struct vattr *va = (struct vattr *)(a_vattr);
        
        struct componentname cn;
        cn.cn_nameiop = CREATE;
        cn.cn_flags   = ISLASTCN;
        cn.cn_cred    = FSCRED;
        cn.cn_namelen = a_cnlen;
        cn.cn_nameptr = a_cn;

        struct vnode *vnout = NULL;
        int netbsd_error = 0;

        switch (va->va_type)
        {
        case VSOCK:                
        case VREG:
                netbsd_error = VOP_CREATE(vn, &vnout, &cn, va);
                break;
        case VBLK:
        case VCHR:
        case VFIFO:
                netbsd_error = VOP_MKNOD(vn, &vnout, &cn, va);
                break;
        case VDIR:
                netbsd_error = VOP_MKDIR(vn, &vnout, &cn, va);
                break;
        default:
                netbsdfs_noimp(__FUNCTION__);
                break;
        }

        if (netbsd_error == 0)
        {
                *a_netbsd_vnode_out = vnout;
        }

        return netbsd_error;
}

int netbsd_vnode_symlink(void *a_netbsd_vnode, 
                         char const *a_cn, 
                         unsigned long a_cnlen, 
                         char const * a_tn, 
                         unsigned long a_tnlen, 
                         struct netbsdfs_vattr *a_vattr, 
                         void** a_netbsd_vnode_out)
{
        struct vnode *vn = a_netbsd_vnode;
        struct vattr *va = (struct vattr *)(a_vattr);
        
        struct componentname cn;
        cn.cn_nameiop = CREATE;
        cn.cn_flags   = ISLASTCN;
        cn.cn_cred    = FSCRED;
        cn.cn_namelen = a_cnlen;
        cn.cn_nameptr = a_cn;

        struct vnode *vnout = NULL;
        int netbsd_error = 0;

        ((char*)a_tn)[a_tnlen] = '\0';

        netbsd_error = VOP_SYMLINK(vn, &vnout, &cn, va, a_tn);

        if (netbsd_error == 0)
        {
                *a_netbsd_vnode_out = vnout;
        }

        return netbsd_error;
}

int netbsd_vnode_link(void *a_netbsd_vnode, void *a_netbsd_tvnode, char const *a_tcn, unsigned long a_tcnlen)
{
        struct vnode *vn = a_netbsd_vnode;
        struct vnode *tvn = a_netbsd_tvnode;

        struct componentname cn;
        cn.cn_nameiop = CREATE;
        cn.cn_flags   = ISLASTCN;
        cn.cn_cred    = FSCRED;
        cn.cn_namelen = a_tcnlen;
        cn.cn_nameptr = a_tcn;

        int netbsd_error = VOP_LINK(vn, tvn, &cn);

        return netbsd_error;
}


int netbsd_vnode_rmdir(void *a_netbsd_vnode, void *a_netbsd_dirvnode, char const *a_dirnm, unsigned long a_dirnm_len)
{
        struct vnode *vn = a_netbsd_vnode;
        struct vnode *dirvn = a_netbsd_dirvnode;

        struct componentname cn;
        cn.cn_nameiop = DELETE;
        cn.cn_flags   = ISLASTCN;
        cn.cn_cred    = FSCRED;
        cn.cn_namelen = a_dirnm;
        cn.cn_nameptr = a_dirnm_len;

        int netbsd_error = VOP_RMDIR(vn, dirvn, &cn);

        return netbsd_error;
}

int netbsd_vnode_remove(void *a_netbsd_vnode, void *a_netbsd_filevnode, char const *a_filenm, unsigned long a_filenm_len)
{
        struct vnode *vn = a_netbsd_vnode;
        struct vnode *filevn = a_netbsd_filevnode;

        struct componentname cn;
        cn.cn_nameiop = DELETE;
        cn.cn_flags   = ISLASTCN;
        cn.cn_cred    = FSCRED;
        cn.cn_namelen = a_filenm;
        cn.cn_nameptr = a_filenm_len;

        int netbsd_error = VOP_REMOVE(vn, filevn, &cn);

        return netbsd_error;
}

int netbsd_vnode_rename(void *a_netbsd_src_prnt_vfsn, char const *a_src_nam, unsigned long a_prnt_nam_len, void* a_netbsd_trgt_prnt_vfsn, char const *a_trgt_nam, unsigned long a_trgt_nam_len)
{
        struct vnode *fdvp = a_netbsd_src_prnt_vfsn;
        struct vnode *fvp = NULL;

        struct componentname fcn;
        fcn.cn_nameiop = DELETE;
        fcn.cn_flags   = ISLASTCN;
        fcn.cn_cred    = FSCRED;
        fcn.cn_nameptr = a_src_nam;
        fcn.cn_namelen = a_prnt_nam_len;

        struct vnode *tdvp = a_netbsd_trgt_prnt_vfsn;
        struct vnode *tvp = NULL;

        struct componentname tcn;
        tcn.cn_nameiop = RENAME;
        tcn.cn_flags   = ISLASTCN;
        tcn.cn_cred    = FSCRED;
        tcn.cn_nameptr = a_trgt_nam;
        tcn.cn_namelen = a_trgt_nam_len;

        int netbsd_error = VOP_RENAME(fdvp, fvp, &fcn, tdvp, tvp, &tcn);

        if (netbsd_error == ENOTEMPTY)
        {
                // BSD is 66 and posix is 39
                netbsd_error = 39;
        }

        return netbsd_error;
}

int netbsd_vnode_readlink(void *a_netbsd_vnode, void *a_buf, unsigned long *a_buflen)
{
        struct iovec aiov;
        aiov.iov_base = a_buf;
        aiov.iov_len  = *a_buflen;

        struct uio auio;
        auio.uio_iov    = &aiov;
        auio.uio_iovcnt = 1;
        auio.uio_rw     = UIO_READ;
        auio.uio_resid  = *a_buflen;
        auio.uio_offset = 0;
        UIO_SETUP_SYSSPACE(&auio);

        int error = VOP_READLINK(a_netbsd_vnode, &auio, FSCRED);

        if (error == 0)
        {
                *a_buflen = auio.uio_offset;
        }

        return error;
}

int netbsd_vnode_getattr(void *a_netbsd_vnode, struct netbsdfs_vattr *a_vattr)
{
        struct vnode *vn = a_netbsd_vnode;
        struct vattr *va = (struct vattr *)(a_vattr);

        int error = VOP_GETATTR(vn, va, FSCRED);

        return error;
}

int netbsd_vnode_setattr(void *a_netbsd_vnode, struct netbsdfs_vattr *a_vattr)
{
        struct vnode *vn = a_netbsd_vnode;
        struct vattr *va = (struct vattr *)(a_vattr);

        int error = VOP_SETATTR(vn, va, FSCRED);

        return error;
}

int netbsd_vnode_fsync(void *a_netbsd_vnode)
{
        struct vnode *vn = a_netbsd_vnode;

        int error = VOP_FSYNC(vn, FSCRED, FSYNC_WAIT, 0, 0);

        return error;
}

int netbsd_vnode_inactive(void *a_netbsd_vnode, int *a_reclaimed)
{
        struct vnode *vp = a_netbsd_vnode;

        VOP_INACTIVE(vp, a_reclaimed);

        return 0;
}

int netbsd_vnode_reclaim(void *a_netbsd_vnode, int a_is_deleted)
{
        struct vnode *vp = a_netbsd_vnode;

        VOP_RECLAIM(vp);

        netbsdfs_vfs_vnode_free(vp, a_is_deleted);
}

int netbsd_vnode_bmap(void *a_netbsd_vnode, unsigned long a_lblkno, unsigned long *a_dblkno, unsigned long *a_dblkcnt)
{
        struct vnode *vp = a_netbsd_vnode;
        daddr_t lblkno   = (daddr_t)a_lblkno;
        daddr_t dbblkno  = -1;
        int run          = 0;

        int error = VOP_BMAP(vp, lblkno, NULL, &dbblkno, &run);

        if (error == 0)
        {
                *a_dblkno  = (unsigned long)dbblkno;
                *a_dblkcnt = (unsigned long)run;
        }

        return error;
}

int netbsd_vnode_read(void *a_netbsd_vnode, struct netbsdfs_uio *a_uio, int a_ioflags)
{
        struct vnode *vp = a_netbsd_vnode;
        struct uio *u    = (struct uio *)(a_uio);

        int error = VOP_READ(vp, u, a_ioflags, FSCRED);

        return error;
}

int netbsd_vnode_write(void *a_netbsd_vnode, struct netbsdfs_uio *a_uio, int a_ioflags)
{
        struct vnode *vp = a_netbsd_vnode;
        struct uio *u    = (struct uio *)(a_uio);

        int error = VOP_WRITE(vp, u, a_ioflags, FSCRED);

        return error;
}

int netbsd_vnode_readdir(void *a_netbsd_vnode, long *a_offset, unsigned long a_emx_buflen, void* a_emx_file, void* a_emx_uio)
{
        struct vnode *vp = a_netbsd_vnode;

        struct vattr va;
        vn_lock(vp, LK_SHARED | LK_RETRY);
        int error = VOP_GETATTR(vp, &va, FSCRED);
        VOP_UNLOCK(vp);

        if (error != 0)
        {
                return error;
        }

        size_t buflen = MIN(PAGE_SIZE, a_emx_buflen);
        
        if (buflen < va.va_blocksize)
        {
                buflen = va.va_blocksize;
        }

        void *tbuf = netbsdfs_mem_alloc(buflen);

        vn_lock(vp, LK_EXCLUSIVE | LK_RETRY);

        off_t off  = *a_offset;

        struct iovec aiov;
again:
        aiov.iov_base = tbuf;
        aiov.iov_len  = buflen;

        struct uio auio;
        auio.uio_iov    = &aiov;
        auio.uio_iovcnt = 1;
        auio.uio_rw     = UIO_READ;
        auio.uio_resid  = buflen;
        auio.uio_offset = off;
        UIO_SETUP_SYSSPACE(&auio);

        /*
         * First we read into the malloc'ed buffer, then we massage it into user space, one record at a time.
         */
        int eofflag      = 0;
        off_t *cookiebuf = NULL;
        int ncookies     = 0;
        
        error = VOP_READDIR(vp, &auio, FSCRED, &eofflag, &cookiebuf, &ncookies);

        if (error != 0)
        {
                goto out;
        }

        char *inp     = (char *)tbuf;
        char *outp    = NULL;
        size_t len    = buflen - auio.uio_resid;
        size_t reclen = 0;
        for (off_t *cookie = cookiebuf; len > 0; len -= reclen)
        {
                struct dirent *bdp = (struct dirent *)inp;
                reclen             = bdp->d_reclen;

                if (bdp->d_fileno == 0)
                {
                        inp += reclen; /* it is a hole; squish it out */

                        if (cookie)
                        {
                                off = *cookie++;
                        }
                        else
                        {
                                off += reclen;
                        }

                        continue;
                }

                uint16_t edreclen = EMERIXX_DIRENT_RECLEN(bdp->d_namlen);

                if (a_emx_buflen < edreclen)
                {
                        break;
                }

                struct emerixx_dirent idb;
                memset(&idb, 0, sizeof(idb));

                idb.d_ino    = bdp->d_fileno;
                idb.d_off    = off;
                idb.d_reclen = edreclen;  
                idb.d_type   = bdp->d_type;

                memcpy(idb.d_name, bdp->d_name, MIN(sizeof(idb.d_name), bdp->d_namlen + 1));

                error = netbsdfs_vfs_vnode_readdir_callback(a_emx_uio, &idb, &a_emx_buflen);

                if (error != 0)
                {
                        goto out;
                }

                inp += reclen;

                if (cookie)
                {
                        off = *cookie++; /* each entry points to itself */
                }
                else
                {
                        off += reclen;
                }

                /* advance output past Dirent12-shaped entry */
                // outp += old_reclen;
                // resid -= old_reclen;
        }

        *a_offset = off;

out:
        VOP_UNLOCK(vp);
        
        if (cookiebuf)
        {
                netbsdfs_mem_free(cookiebuf);
        }
        netbsdfs_mem_free(tbuf);

        return error;
}


int netbsd_vttoif(enum netbsdfs_vtype a_vt) { return VTTOIF(a_vt); }

int netbsd_vnode_get_rdev(void *a_netbsd_vnode, unsigned *a_major, unsigned *a_minor)
{
        vnode_t *vp = a_netbsd_vnode;

        *a_major = major(vp->v_rdev);
        *a_minor = minor(vp->v_rdev);
}

unsigned netbsd_vnode_get_bshift(void *a_netbsd_vnode)
{
        vnode_t *vp = a_netbsd_vnode;
        return vp->v_mount->mnt_fs_bshift;
}


int dounmount(struct mount *mp, int flags, struct lwp *l) { netbsdfs_noimp(__FUNCTION__); }

int vfs_unixify_accmode(accmode_t *accmode) { netbsdfs_noimp(__FUNCTION__); }

long makefstype(const char *type)
{
        long rv;
        for (rv = 0; *type; type++)
        {
                rv <<= 2;
                rv ^= *type;
        }
        return rv;
}

void vfs_timestamp(struct timespec *ts) 
{ 
        netbsdfs_get_timespec(ts);
}

int fscow_run(struct buf *bp, bool data_valid) { netbsdfs_noimp(__FUNCTION__); }

int fscow_establish(struct mount *mp, int (*func)(void *, struct buf *, bool), void *arg) { netbsdfs_noimp(__FUNCTION__); }

int fscow_disestablish(struct mount *mp, int (*func)(void *, struct buf *, bool), void *arg) { netbsdfs_noimp(__FUNCTION__); }

void vfs_vnode_iterator_init(struct mount *m, struct vnode_iterator **vi) 
{ 
        netbsdfs_creation_lock_acquire();
        netbsdfs_vfs_node_iterator_init(m, vi);
}

struct vnode *vfs_vnode_iterator_next(struct vnode_iterator *vi, bool (*cb)(void *, struct vnode *), void *z)
{
        vnode_t *vp = NULL;

        do 
        {
                vp = (vnode_t *)netbsdfs_vfs_node_iterator_next(vi);

                if (vp == NULL)
                {
                        break;
                }

                int usable = cb(z, vp);
                
                if (usable || 1)
                {
                        break;
                }

        } while (1);

        return vp;
}

void vfs_vnode_iterator_destroy(struct vnode_iterator *vi) 
{ 
        netbsdfs_vfs_node_iterator_destroy(vi);
        netbsdfs_creation_lock_release(); 
}

void cache_enter(struct vnode *dvp, struct vnode *vp, const char *name, size_t namelen, uint32_t cnflags)
{
        netbsdfs_noimp_warn(__FUNCTION__);
}

bool cache_lookup(
    struct vnode *dvp, const char *name, size_t namelen, uint32_t nameiop, uint32_t cnflags, int *iswht_ret, struct vnode **vn_ret)
{
        return 0;
}

void namecache_count_pass2(void) { netbsdfs_noimp_warn(__FUNCTION__); }

void namecache_count_2passes(void) { netbsdfs_noimp_warn(__FUNCTION__); }

void cache_purge1(struct vnode *vp, const char *name, size_t namelen, int flags) { netbsdfs_noimp_warn(__FUNCTION__); }

int relookup(struct vnode *dvp, struct vnode **vpp, struct componentname *cnp, int dummy) 
{ 
        int error = VOP_LOOKUP(dvp, vpp, cnp);
        if ((cnp->cn_nameiop == RENAME) && (error == -ENOENT))
        {
                error = 0;
        }

        return error;
}

int namei_simple_user(const char *path, namei_simple_flags_t sflags, struct vnode **vp_ret) 
{
        *vp_ret = namei_simple_user_return_vn;
        return 0;
}

int sysctl_vfs_generic_fstypes() { netbsdfs_noimp(__FUNCTION__); }

void vfs_hooks_init() { return 0; }

void vfs_evfilt_fs_init() { netbsdfs_noimp_warn(__FUNCTION__); }

void nchinit() { netbsdfs_noimp_warn(__FUNCTION__); }

void vntblinit() { netbsdfs_noimp_warn(__FUNCTION__); }

void dirhash_init() { netbsdfs_noimp_warn(__FUNCTION__); }

int vfs_busy(struct mount *mp) { mp->mnt_refcnt++; }

void vfs_unbusy(struct mount *mp)
{
        if ((--(mp->mnt_refcnt)) == 0)
        {
                netbsdfs_noimp(__FUNCTION__);
        }
}

void vfs_rele(struct mount *mp) { netbsdfs_noimp(__FUNCTION__); }

void mountlist_append(struct mount *mp)
{
        s_root_mp = mp;
        netbsdfs_noimp_warn(__FUNCTION__);
}

int vfs_rootmountalloc(const char *fstypename, const char *devname, struct mount **mpp)
{
        struct vfsops *v;
        int error = ENODEV;
        mutex_enter(&vfs_list_lock);
        LIST_FOREACH(v, &vfs_list, vfs_list)
        {
                if (strcmp(v->vfs_name, fstypename) == 0)
                {
                        struct mount *mp = NULL;
                        
                        error = netbsdfs_vfs_mount_alloc(&mp, sizeof(*mp));

                        if (error == 0)
                        {
                                mp->mnt_op     = v;
                                mp->mnt_refcnt = 1;
                                TAILQ_INIT(&mp->mnt_vnodelist);

                                vfs_busy(mp);
                                *mpp = mp;
                        }

                        break;
                }
        }
        mutex_exit(&vfs_list_lock);
        return error;
}

void setrootfstime(time_t t) { netbsdfs_noimp_warn(__FUNCTION__); }

int lf_advlock(struct vop_advlock_args *ap, struct lockf **head, off_t size) { netbsdfs_noimp(__FUNCTION__); }

int vfs_stdextattrctl(struct mount *mp, int cmt, struct vnode *vp, int attrnamespace, const char *attrname)
{
        netbsdfs_noimp(__FUNCTION__);
}

int set_statvfs_info(const char *onp, int ukon, const char *fromp, int ukfrom, const char *vfsname, struct mount *mp, struct lwp *l)
{
        netbsdfs_noimp_warn(__FUNCTION__);
        return 0;
}

void copy_statvfs_info(struct statvfs *sbp, const struct mount *mp)
{
        const struct statvfs *mbp;

        if (sbp == (mbp = &mp->mnt_stat))
                return;

        (void)memcpy(&sbp->f_fsidx, &mbp->f_fsidx, sizeof(sbp->f_fsidx));
        sbp->f_fsid        = mbp->f_fsid;
        sbp->f_owner       = mbp->f_owner;
        sbp->f_flag        = mbp->f_flag;
        sbp->f_syncwrites  = mbp->f_syncwrites;
        sbp->f_asyncwrites = mbp->f_asyncwrites;
        sbp->f_syncreads   = mbp->f_syncreads;
        sbp->f_asyncreads  = mbp->f_asyncreads;
        (void)memcpy(sbp->f_spare, mbp->f_spare, sizeof(mbp->f_spare));
        (void)memcpy(sbp->f_fstypename, mbp->f_fstypename, sizeof(sbp->f_fstypename));
        (void)memcpy(sbp->f_mntonname, mbp->f_mntonname, sizeof(sbp->f_mntonname));
        (void)memcpy(sbp->f_mntfromname, mp->mnt_stat.f_mntfromname, sizeof(sbp->f_mntfromname));
        (void)memcpy(sbp->f_mntfromlabel, mp->mnt_stat.f_mntfromlabel, sizeof(sbp->f_mntfromlabel));
        sbp->f_namemax = mbp->f_namemax;
}

int bdevvp(dev_t dev, vnode_t **vpp) { netbsdfs_noimp(__FUNCTION__); }

/* VNODE */
int vn_lock(struct vnode *v, int a) { return 0; }

void vref(struct vnode *vp)
{
        if (&rootdev_vn != vp)
        {
                netbsdfs_vfs_vnode_ref(vp);
        }
        else
        {
                vp->v_usecount++;
        }
}


int vcache_get(struct mount *mp, const void *key, size_t key_len, struct vnode **vpp)
{
        vnode_t *vp = NULL;

        netbsdfs_creation_lock_acquire();

        int error = netbsdfs_vfs_cache_vnode_get(mp, sizeof(*mp), key, key_len, &vp);

        if (error)
        {
                goto out;
        }

        if (vp != NULL)
        {
                *vpp = vp;
                goto out;
        }

        error = netbsdfs_vfs_vnode_alloc(mp, sizeof(*vp), &vp);

        if (error)
        {
                goto out;
        }

        memset(vp, 0, sizeof(*vp));
        vp->v_usecount = 1; // Not used
        vp->v_mount    = mp;
        vp->v_specnode = ((vnode_t*)mp->mnt_rootdevvp)->v_specnode;
        vp->v_interlock = &vp->v_interlock;

        void *new_key;
        VFS_LOADVNODE(mp, vp, key, key_len, &new_key);
        
        struct vattr va;
        VOP_GETATTR(vp, &va, FSCRED);
        //va.va_rdev = vp->v_rdev;

        netbsdfs_vfs_vnode_init(vp, ((struct rumpuser_rw**)&vp->v_uobj.vmobjlock), new_key, key_len, mp, sizeof(*mp), &va);

        *vpp           = vp;

out:
        netbsdfs_creation_lock_release();

        return error;
}

int vcache_rekey_enter(struct mount *mp, struct vnode *vp, const void *old_key, size_t old_key_len, const void *new_key, size_t new_key_len)
{
        netbsdfs_creation_lock_acquire();

        int error = -netbsdfs_vfs_cache_rekey_part1(mp, sizeof(*mp), vp, sizeof(*vp), old_key, old_key_len, new_key, new_key_len);

        return error;
}

void vcache_rekey_exit(struct mount *mp, struct vnode *vp, const void *old_key, size_t old_key_len, const void *new_key, size_t new_key_len)
{
        int error = -netbsdfs_vfs_cache_rekey_part2(mp, sizeof(*mp), vp, sizeof(*vp), old_key, old_key_len, new_key, new_key_len);

        netbsdfs_creation_lock_release();
        
        return error;
}


void vrele(struct vnode *vp)
{
        /*
        Decrement v_usecount of unlocked vnode vp.
        Any code in the system which is using a vnode should call vrele() when it is finished with the vnode.
        If v_usecount of the vnode reaches zero and v_holdcnt is greater than zero, the vnode is placed on the holdlist.
        If both v_usecount and v_holdcnt are zero, the vnode is placed on the freelist.
        */
        if (&rootdev_vn != vp)
        {
                netbsdfs_noimp_warn(__FUNCTION__);
                //netbsdfs_vfs_vnode_rel(vp);
        }
        else
        {
                if ((--vp->v_usecount) == 0)
                {
                        netbsdfs_noimp(__FUNCTION__);
                }
        }
}

void vput(struct vnode *vp) { /*unlock and */ vrele(vp); }

bool vrecycle(vnode_t *vp) { netbsdfs_noimp(__FUNCTION__); }

int vrefcnt(struct vnode *vp) { netbsdfs_noimp(__FUNCTION__); }

void vhold(vnode_t *vp) { netbsdfs_noimp(__FUNCTION__); }

int vflush(struct mount *m, struct vnode *v, int a) { netbsdfs_noimp(__FUNCTION__); }

void holdrelel(vnode_t *vp) { netbsdfs_noimp(__FUNCTION__); }

int vinvalbuf(struct vnode *v, int a, kauth_cred_t b, struct lwp *c, bool d, int e) { return 0; }

int vflushbuf(struct vnode *vp, int a_flags) 
{ 
        int error = netbsdfs_vnode_flush_buffers(vp, a_flags);
        return error;
}

int vtruncbuf(struct vnode *vp, daddr_t lbn, bool catch_p, int slptimeo) 
{
        int error = netbsdfs_vfs_node_trunc_buffer(vp, lbn, vp->v_mount->mnt_fs_bshift, vp->v_mount->mnt_dev_bshift);
        return error;
}

int vcache_new(struct mount *mp, struct vnode *dvp, struct vattr *vap, kauth_cred_t cred, void *extra, struct vnode **vpp)
{
        vnode_t *vp;

        netbsdfs_creation_lock_acquire();

        int error = netbsdfs_vfs_vnode_alloc(mp, sizeof(*vp), &vp);

        if (error)
        {
                goto out;
        }

        memset(vp, 0, sizeof(*vp));

        vp->v_usecount = 1; // Not used
        vp->v_mount    = mp;
        vp->v_specnode = ((vnode_t*)mp->mnt_rootdevvp)->v_specnode;
        vp->v_interlock = &vp->v_interlock;

        if ((vap->va_type != VCHR) && (vap->va_type != VBLK))
        {
                vap->va_rdev = vp->v_rdev;
        }

        void *new_key;
        size_t new_key_len;
        error = VFS_NEWVNODE(mp, dvp, vp, vap, cred, extra,&new_key_len, &new_key);

        if (error)
        {
                netbsdfs_vfs_vnode_free(vp, TRUE);
                goto out;
        }

        netbsdfs_vfs_vnode_init(vp, ((struct rumpuser_rw**)&vp->v_uobj.vmobjlock), new_key, new_key_len, mp, sizeof(*mp), vap);

        *vpp           = vp;

out:
        netbsdfs_creation_lock_release();

        return error;
}

vn_open(
    struct vnode *at_dvp, struct pathbuf *pb, int nmode, int fmode, int cmode, struct vnode **ret_vp, bool *ret_domove, int *ret_fd)
{
        netbsdfs_noimp(__FUNCTION__);
}

int vn_close(struct vnode *vp, int flags, kauth_cred_t cred) { netbsdfs_noimp(__FUNCTION__); }

int vn_rdwr(enum uio_rw rw,
            struct vnode *vp,
            void *base,
            int len,
            off_t offset,
            enum uio_seg segflg,
            int ioflg,
            kauth_cred_t cred,
            size_t *aresid,
            struct lwp *l)
{
        netbsdfs_noimp(__FUNCTION__);
}

int vn_fifo_bypass(void *v) { netbsdfs_noimp(__FUNCTION__); }

int vn_bwrite(void *v) 
{ 
	struct vop_bwrite_args *ap = v;
	return (bwrite(ap->a_bp));
}

/* KMEM */

void *kmem_intr_alloc(size_t size, km_flag_t kmflags) { return kmem_alloc(size, kmflags); }

void kmem_intr_free(void *p, size_t s) { kmem_free(p, s); }

void *kmem_alloc(size_t s, km_flag_t f)
{
        void *p = netbsdfs_mem_alloc(s);
        memset(p, 0, s);
        return p;
}

void *kmem_zalloc(size_t s, km_flag_t f)
{
        void *p = kmem_alloc(s, f);
        return p;
}

void kmem_free(void *p, size_t s) { netbsdfs_mem_free(p); }

void *kern_malloc(size_t s, int a, int b) { return kmem_alloc(s, 0); }

void kern_free(void *p, int a) { kmem_free(p, 0); }

/* BIO */

buf_t *incore(struct vnode *v, daddr_t d) 
{ 
        netbsdfs_noimp_warn(__FUNCTION__); 
        return NULL; 
}

int bbusy(buf_t *bp, bool intr, int timo, kmutex_t *interlock) { netbsdfs_noimp(__FUNCTION__); }

buf_t *getblk(struct vnode *vp, daddr_t blkno, int size, int slpflag, int slptimeo)
{
        buf_t *b     = kmem_alloc(sizeof(*b), 0);
        b->b_bufsize = b->b_bcount = size;
        b->b_blkno = b->b_lblkno = b->b_rawblkno = blkno;
        b->b_vp                                  = vp;
        b->b_dev                                 = vp->v_rdev;

        if (vp->v_type == VBLK) // When writing direcly to disk the blkno is valid
        {
                // When msdos fs mirror the FAT it allocates a empty block(cluster) 
                // by calling this function and copies the real data from the real block.
                // We need to bring in the block(cluster) from disk in order to have a valid b_data.
                // TODO: Inform the netbsdfs layer that we need a empty block and skip the read from disk.
                VOP_STRATEGY(vp, b);
                assert(b->b_data != NULL);
                //assert(size == PAGE_SIZE);
                
                //memset(b->b_data, 0, size); 
        }

        return b;
}
void clrbuf (buf_t *bp)
{
        assert(bp->b_data == NULL);
        assert(bp->b_lblkno != bp->b_blkno);
        assert(bp->b_blkno >= 0);
        int error = VOP_STRATEGY(bp->b_vp, bp);
        assert(error == 0);
        memset(bp->b_data, 0, bp->b_bcount); bp->b_resid = 0;

}

void brelse(buf_t *a, int a_cflags) 
{
        if (a_cflags & (BC_INVAL | BC_NOCACHE))
        {
                dev_t deviceid = a->b_vp->v_rdev;
                netbsdfs_spec_invalidate_block(major(deviceid), minor(deviceid), a->b_blkno, a->b_bcount);
        }
        kmem_free(a, 0); 
}

int bread(struct vnode *vp, daddr_t blkno, int size, int flags, buf_t **bpp)
{
        buf_t *b     = kmem_alloc(sizeof(*b), 0);
        b->b_bufsize = b->b_bcount = size;
        b->b_blkno = b->b_lblkno = b->b_rawblkno = blkno;
        b->b_vp                                  = vp;
        b->b_dev                                 = vp->v_rdev;

        int error = VOP_STRATEGY(vp, b);

        *bpp = b;

        return error;
}

void binvalbuf(struct vnode *vp, daddr_t blkno) { netbsdfs_noimp(__FUNCTION__); }

int breadn(struct vnode *vp, daddr_t blkno, int size, daddr_t *rablks, int *rasizes, int nrablks, int flags, buf_t **bpp) 
{ 
        return bread(vp, blkno, size, flags, bpp);

        // We don't care about the read ahead blocks, we read ahead anyway in most cases.
}

int bwrite(buf_t *b) { kmem_free(b, 0);  return 0;}

void bdwrite(buf_t *b) { kmem_free(b, 0); }

void bawrite(buf_t *b) { netbsdfs_noimp(__FUNCTION__); }

int biowait(buf_t *b) { return 0; }

void biodone(buf_t *b) { netbsdfs_noimp_warn(__FUNCTION__); }

int allocbuf(buf_t *bp, int size, int preserve) { netbsdfs_noimp(__FUNCTION__); }

void bdev_strategy(struct buf *bp) { netbsdfs_noimp(__FUNCTION__); }

buf_t *getiobuf(struct vnode *vp, bool waitok) { netbsdfs_noimp(__FUNCTION__); }

void putiobuf(buf_t *bp) { netbsdfs_noimp(__FUNCTION__); }

void rump_xc_highpri(struct cpu_info *ci) { netbsdfs_noimp(__FUNCTION__); }

void rump_lwproc_curlwp_clear(struct lwp *l) { netbsdfs_noimp(__FUNCTION__); }

struct lwp *rump__lwproc_alloclwp(struct proc *p) { netbsdfs_noimp(__FUNCTION__); }

void rump_lwproc_curlwp_set(struct lwp *l) { netbsdfs_noimp(__FUNCTION__); }

void rump_lwproc_switch(struct lwp *newlwp) { netbsdfs_noimp(__FUNCTION__); }

void lwp_update_creds(struct lwp *l) { netbsdfs_noimp(__FUNCTION__); }

void rump_lwproc_releaselwp(void) { netbsdfs_noimp(__FUNCTION__); }

/*
 * The real-time timer, interrupting hz times per second.
 */
void hardclock(struct clockframe *frame) { netbsdfs_noimp(__FUNCTION__); }

void initclocks(void) { netbsdfs_noimp(__FUNCTION__); }

int getticks(void) { netbsdfs_noimp(__FUNCTION__); }

void doshutdownhooks(void) { netbsdfs_noimp(__FUNCTION__); }

pid_t proc_alloc_lwpid(struct proc *p, struct lwp *l) { return 1; }
void proc_free_lwpid(struct proc *p, pid_t pp) { netbsdfs_noimp(__FUNCTION__); }

vaddr_t uvm_default_mapaddr(struct proc *p, vaddr_t base, vsize_t sz, int topdown) { netbsdfs_noimp(__FUNCTION__); }

struct lwp *rump_lwproc_curlwp_hypercall(void) { return &lwp0; }

void evcnt_attach_dynamic(struct evcnt *ev, int type, const struct evcnt *parent, const char *group, const char *name)
{
        netbsdfs_noimp_warn(__FUNCTION__);
}

kmutex_t *mutex_obj_alloc(kmutex_type_t type, int ipl) { netbsdfs_noimp(__FUNCTION__); }

void aprint_error(const char *s, ...) { netbsdfs_noimp(__FUNCTION__); }

//////////

int nullop(void *a) { return 0; }

struct sysent rump_sysent[1];

const uint32_t rump_sysent_nomodbits[1];

struct proclist allproc;

rump_proc_vfs_init_fn rump_proc_vfs_init = (void *)nullop;

struct proc *initproc;

struct vmspace *rump_vmspace_local;

int rump_threads = 0;

struct rumpuser_cv
{
};

struct rump_sysproxy_ops rump_sysproxy_ops;

void rumpuser__thrinit(void) { netbsdfs_noimp(__FUNCTION__); }

/*ARGSUSED*/
int rumpuser_thread_create(void *(*f)(void *), void *arg, const char *thrname, int joinable, int pri, int cpuidx, void **tptr)
{
        netbsdfs_noimp(__FUNCTION__);
}

void rumpuser_thread_exit(void) { netbsdfs_noimp(__FUNCTION__); }

int rumpuser_thread_join(void *p) { return 0; }

void rumpuser_mutex_init(struct rumpuser_mtx **mtx, int flgas) { netbsdfs_mutex_alloc(mtx); }

int rumpuser_mutex_spin_p(struct rumpuser_mtx *mtx) { return false; /* XXX */ }

void rumpuser_mutex_enter(struct rumpuser_mtx *mtx) 
{ 
        struct rumpuser_mtx **pp = (struct rumpuser_mtx**)(mtx);
        
        if (*pp == mtx)
        {
                // Self, refrence, not really a mutex.
                return;
        }


        netbsdfs_mutex_enter(mtx); 
}

void rumpuser_mutex_enter_nowrap(struct rumpuser_mtx *mtx) 
{ 
        struct rumpuser_mtx **pp = (struct rumpuser_mtx**)(mtx);
        
        if (*pp == mtx)
        {
                // Self, refrence, not really a mutex.
                return;
        }

        netbsdfs_mutex_enter(mtx); 
}

int rumpuser_mutex_tryenter(struct rumpuser_mtx *mtx) 
{ 
        struct rumpuser_mtx **pp = (struct rumpuser_mtx**)(mtx);
        
        if (*pp == mtx)
        {
                // Self, refrence, not really a mutex.
                return 1;
        }


        return !netbsdfs_mutex_try_enter(mtx); 
}

void rumpuser_mutex_exit(struct rumpuser_mtx *mtx) 
{ 
        struct rumpuser_mtx **pp = (struct rumpuser_mtx**)(mtx);
        
        if (*pp == mtx)
        {
                // Self, refrence, not really a mutex.
                return;
        }

        netbsdfs_mutex_exit(mtx); 
}

void rumpuser_mutex_destroy(struct rumpuser_mtx *mtx) 
{ 
        struct rumpuser_mtx **pp = (struct rumpuser_mtx**)(mtx);
        
        if (*pp == mtx)
        {
                // Self, refrence, not really a mutex.
                return;
        }

        netbsdfs_mutex_free(mtx); 
}

void rumpuser_mutex_owner(struct rumpuser_mtx *mtx, struct lwp **lp) { netbsdfs_noimp(__FUNCTION__); }

void rumpuser_rw_init(struct rumpuser_rw **rw) 
{ 
        netbsdfs_noimp(__FUNCTION__); 
}

void rumpuser_rw_enter(int enum_rumprwlock, struct rumpuser_rw *rw)
{
        enum rumprwlock lk = enum_rumprwlock;

        switch (lk)
        {
        case RUMPUSER_RW_WRITER:
                rw->v++;
                assert(rw->v == 1);
                break;
        case RUMPUSER_RW_READER:
                assert(rw->v <= 0);
                rw->v--;
                break;
        }
}

int rumpuser_rw_tryenter(int enum_rumprwlock, struct rumpuser_rw *rw)
{
        rumpuser_rw_enter(enum_rumprwlock, rw);
        return 0;
}

void rumpuser_rw_exit(struct rumpuser_rw *rw)
{
        if (rw->v > 0)
        {
                assert(rw->v == 1);
                rw->v--;
        }
        else
        {
                rw->v++;
        }
}

void rumpuser_rw_destroy(struct rumpuser_rw *rw) { netbsdfs_noimp(__FUNCTION__); }

void rumpuser_rw_held(int enum_rumprwlock, struct rumpuser_rw *rw, int *rvp)
{
        enum rumprwlock lk = enum_rumprwlock;

        switch (lk)
        {
        case RUMPUSER_RW_WRITER:
                *rvp = rw->v > 0;
                break;
        case RUMPUSER_RW_READER:
                *rvp = rw->v < 0;
                break;
        }
}

void rumpuser_rw_downgrade(struct rumpuser_rw *rw)
{
        netbsdfs_noimp(__FUNCTION__);
#if 0        
        assert(rw->v == 1);
        rw->v = -1;
#endif
}

int rumpuser_rw_tryupgrade(struct rumpuser_rw *rw)
{
        netbsdfs_noimp(__FUNCTION__);
#if 0
        if (rw->v == -1)
        {
                rw->v = 1;
                return 0;
        }

        return EBUSY;
#endif
}

/*ARGSUSED*/
void rumpuser_cv_init(struct rumpuser_cv **cv) {}

/*ARGSUSED*/
void rumpuser_cv_destroy(struct rumpuser_cv *cv) {}

/*ARGSUSED*/
void rumpuser_cv_wait(struct rumpuser_cv *cv, struct rumpuser_mtx *mtx) {}

/*ARGSUSED*/
void rumpuser_cv_wait_nowrap(struct rumpuser_cv *cv, struct rumpuser_mtx *mtx) {}

/*ARGSUSED*/
int rumpuser_cv_timedwait(struct rumpuser_cv *cv, struct rumpuser_mtx *mtx, int64_t sec, int64_t nsec)
{
        struct timespec ts;

        /*LINTED*/
        ts.tv_sec = sec;
        /*LINTED*/
        ts.tv_nsec = nsec;

        // nanosleep(&ts, NULL);
        return 0;
}

/*ARGSUSED*/
void rumpuser_cv_signal(struct rumpuser_cv *cv) {}

/*ARGSUSED*/
void rumpuser_cv_broadcast(struct rumpuser_cv *cv) {}

/*ARGSUSED*/
void rumpuser_cv_has_waiters(struct rumpuser_cv *cv, int *rvp) { *rvp = 0; }

/*
 * curlwp
 */

void rumpuser_curlwpop(int enum_rumplwpop, struct lwp *l)
{
        enum rumplwpop op = enum_rumplwpop;

        switch (op)
        {
        case RUMPUSER_LWP_CREATE:
        case RUMPUSER_LWP_DESTROY:
                break;
        case RUMPUSER_LWP_SET:
                lwp0 = *l;
                break;
        case RUMPUSER_LWP_CLEAR:
                assert(curlwp == l);
                // curlwp = NULL;
                memset(&lwp0, 0, sizeof(lwp0));
                lwp0.l_cpu = &ci;
                break;
        }
}

struct lwp *rumpuser_curlwp(void) { return curlwp; }

int rumpuser_clock_sleep(int enum_rumpclock, int64_t sec, long nsec) { netbsdfs_noimp(__FUNCTION__); }

int rumpuser_clock_gettime(int enum_rumpclock, int64_t *sec, long *nsec) { netbsdfs_noimp(__FUNCTION__); }

void rumpuser_putchar(int c)
{
        netbsdfs_noimp(__FUNCTION__);
        // putchar(c);
}

void rumpuser_exit(int rv)
{
        netbsdfs_noimp(__FUNCTION__);

        // printf("halted\n");
        // if (rv == RUMPUSER_PANIC)
        //  abort();
        // else
        // exit(rv);
}

int rumpuser_kill(int64_t pid, int rumpsig) { return 0; }


// ufs_inode.c <----- needs some modifications

int ufs_balloc_range(struct vnode *vp, off_t off, off_t len, kauth_cred_t cred, int flags)
{
        netbsdfs_balloc_lock(vp->v_mount);

        #if 0
        struct genfs_node *gp = VTOG(vp);

        int error = GOP_ALLOC(vp, off, len, flags, cred);
#endif

	off_t eob;	/* offset next to allocated blocks */
        
	//struct uvm_object *uobj;

	int bshift = vp->v_mount->mnt_fs_bshift;

	int bsize = 1 << bshift;

	int ppb = MAX(bsize >> PAGE_SHIFT, 1);


	//struct vm_page **pgs;
	
	//UVMHIST_FUNC("ufs_balloc_range"); UVMHIST_CALLED(ubchist);
	//UVMHIST_LOG(ubchist, "vp %#jx off 0x%jx len 0x%jx u_size 0x%jx", (uintptr_t)vp, off, len, vp->v_size);

        /* file size after the operation */
	off_t neweof = MAX(vp->v_size, off + len); 

        /* offset next to the last block after the operation */
        off_t neweob = 0;

	GOP_SIZE(vp, neweof, &neweob, 0);

	//error = 0;
	//uobj = &vp->v_uobj;

	/*
	 * read or create pages covering the range of the allocation and
	 * keep them locked until the new block is allocated, so there
	 * will be no window where the old contents of the new block are
	 * visible to racing threads.
	 */

	 /* starting offset of range covered by pgs */
	off_t pagestart = trunc_page(off) & ~(bsize - 1);
	int npages = MIN(ppb, (round_page(neweob) - pagestart) >> PAGE_SHIFT);
	
        //size_t pgssize = npages * sizeof(struct vm_page *);
	//pgs = kmem_zalloc(pgssize, KM_SLEEP);

	/*
	 * adjust off to be block-aligned.
	 */

	int delta = off & (bsize - 1);
	off -= delta;
	len += delta;


#if 0
	genfs_node_wrlock(vp);
	rw_enter(uobj->vmobjlock, RW_WRITER);
	error = VOP_GETPAGES(vp, pagestart, pgs, &npages, 0,
	    VM_PROT_WRITE, 0, PGO_SYNCIO | PGO_PASTEOF | PGO_NOBLOCKALLOC |
	    PGO_NOTIMESTAMP | PGO_GLOCKHELD);
	if (error) {
		genfs_node_unlock(vp);
		goto out;
	}

#else

	int error = GOP_ALLOC(vp, off, len, flags, cred);
#endif


#if 0
	genfs_node_unlock(vp);

	/*
	 * if the allocation succeeded, mark all the pages dirty
	 * and clear PG_RDONLY on any pages that are now fully backed
	 * by disk blocks.  if the allocation failed, we do not invalidate
	 * the pages since they might have already existed and been dirty,
	 * in which case we need to keep them around.  if we created the pages,
	 * they will be clean and read-only, and leaving such pages
	 * in the cache won't cause any problems.
	 */

	GOP_SIZE(vp, off + len, &eob, 0);
	rw_enter(uobj->vmobjlock, RW_WRITER);
	for (i = 0; i < npages; i++) {
		KASSERT((pgs[i]->flags & PG_RELEASED) == 0);
		if (!error) {
			if (off <= pagestart + (i << PAGE_SHIFT) &&
			    pagestart + ((i + 1) << PAGE_SHIFT) <= eob) {
				pgs[i]->flags &= ~PG_RDONLY;
			}
			uvm_pagemarkdirty(pgs[i], UVM_PAGE_STATUS_DIRTY);
		}
		uvm_pagelock(pgs[i]);
		uvm_pageactivate(pgs[i]);
		uvm_pageunlock(pgs[i]);
	}
	uvm_page_unbusy(pgs, npages);
	rw_exit(uobj->vmobjlock);

 out:
 	kmem_free(pgs, pgssize);
	return error;
#else
        if ((error == 0))
        {
                error = netbsdfs_vfs_alloc_vnode_pages(vp, pagestart, npages);
        }
#endif

out:
        netbsdfs_balloc_unlock(vp->v_mount);
        return error;
}