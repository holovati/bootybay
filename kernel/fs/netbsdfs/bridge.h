// clang-format off
#pragma once

#if defined(__cplusplus)
extern "C" {
#endif

struct rumpuser_rw
{
        struct rumpuser_rw *self;
        int v;
        int pad;
};

// We need this so we can call getattr/setattr
enum netbsdfs_vtype
{
        NETBSDFS_VNON,
        NETBSDFS_VREG,
        NETBSDFS_VDIR,
        NETBSDFS_VBLK,
        NETBSDFS_VCHR,
        NETBSDFS_VLNK,
        NETBSDFS_VSOCK,
        NETBSDFS_VFIFO,
        NETBSDFS_VBAD
};

struct netbsdfs_timespec
{
        int64_t tv_sec; /* seconds */
        long tv_nsec;   /* and nanoseconds */
};

struct netbsdfs_vattr
{
        enum netbsdfs_vtype va_type; /* vnode type (for create) */
        uint32_t va_mode;            /* files access mode and type */
        uint32_t va_nlink;           /* number of references to file */
        uint32_t va_uid;             /* owner user id */
        uint32_t va_gid;             /* owner group id */
        int32_t __pad0[1];
        uint64_t va_fsid;                      /* file system id (dev for now) */
        uint64_t va_fileid;                    /* file id */
        uint64_t va_size;                      /* file size in bytes */
        long va_blocksize;                     /* blocksize preferred for i/o */
        struct netbsdfs_timespec va_atime;     /* time of last access */
        struct netbsdfs_timespec va_mtime;     /* time of last modification */
        struct netbsdfs_timespec va_ctime;     /* time file changed */
        struct netbsdfs_timespec va_birthtime; /* time file created */
        unsigned long va_gen;                  /* generation number of file */
        unsigned long va_flags;                /* flags defined for file */
        uint64_t va_rdev;                      /* device the special file represents */
        uint64_t va_bytes;                     /* bytes of disk space held by file */
        uint64_t va_filerev;                   /* file modification number */
        unsigned int va_vaflags;               /* operations flags, see below */
        int32_t __pad1[1];
        long va_spare; /* remain quad aligned */
};

// UIO is needed for rw calls
struct netbsdfs_iovec
{
        void *iov_base;
        uint64_t iov_len;
};

enum netbsdfs_uio_rw
{
        NETBSDFS_UIO_READ,
        NETBSDFS_UIO_WRITE
};

enum netbsdfs_uio_seg
{
        NETBSDFS_UIO_USERSPACE,
        NETBSDFS_UIO_SYSSPACE
};

struct netbsdfs_uio
{
        struct netbsdfs_iovec *uio_iov;
        int32_t uio_iovcnt;
        int32_t __pad0[1];
        int64_t uio_offset;
        uint64_t uio_resid;
        enum netbsdfs_uio_rw uio_rw;
        int32_t __pad1[1];
        void *uio_vmspace;
};

#define EMERIXX_ROUNDUP(val, round)   ((typeof(val))(((size_t)(val) + (round)-1) & (size_t)(typeof(val))~((round)-1)))
#define EMERIXX_DIRENT_RECLEN(nm_len) (EMERIXX_ROUNDUP(offsetof(struct emerixx_dirent, d_name) + (nm_len) + 2, sizeof(long)))

#pragma pack(1)
struct emerixx_dirent
{
        uint64_t d_ino;
        int64_t d_off;
        uint16_t d_reclen;
        uint8_t d_type;
        int8_t d_name[256];
};
#pragma pack(0)

// To NetBSD layer
int netbsd_subsys_init();
int netbsd_mountroot();

// VFS calls to NetBSD layer
int netbsd_vfs_root(void *a_netbsd_mount, void **a_netbsd_vnode_out);

int netbsd_vfs_statfs(void *a_netbsd_mount, void *user_data);

int netbsd_vfs_sync(void *a_netbsd_mount, int a_wait);

int netbsd_vnode_lookup(
    void *a_netbsd_vnode, char const *a_cn, unsigned long a_cnlen, int a_op, int a_flags, void **a_netbsd_vnode_out);
int netbsd_vnode_create(
    void *a_netbsd_vnode, char const *a_cn, unsigned long a_cnlen, struct netbsdfs_vattr *a_vattr, void **a_netbsd_vnode_out);
int netbsd_vnode_symlink(void *a_netbsd_vnode,
                         char const *a_cn,
                         unsigned long a_cnlen,
                         char const *a_tn,
                         unsigned long a_tnlen,
                         struct netbsdfs_vattr *a_vattr,
                         void **a_netbsd_vnode_out);
int netbsd_vnode_link(void *a_netbsd_vnode, void *a_netbsd_tvnode, char const *a_tcn, unsigned long a_tcnlen);
int netbsd_vnode_rmdir(void *a_netbsd_vnode, void *a_netbsd_dirvnode, char const *a_dirnm, unsigned long a_dirnm_len);
int netbsd_vnode_remove(void *a_netbsd_vnode, void *a_netbsd_filevnode, char const *a_filenm, unsigned long a_filenm_len);
int netbsd_vnode_rename(void *a_netbsd_src_prnt_vfsn, char const *a_src_nam, unsigned long a_prnt_nam_len, void* a_netbsd_trgt_prnt_vfsn, char const *a_trgt_nam, unsigned long a_trgt_nam_len);
int netbsd_vnode_readlink(void *a_netbsd_vnode, void *a_buf, unsigned long *a_buflen_len);
int netbsd_vnode_getattr(void *a_netbsd_vnode, struct netbsdfs_vattr *a_vattr);
int netbsd_vnode_setattr(void *a_netbsd_vnode, struct netbsdfs_vattr *a_vattr);
int netbsd_vnode_fsync(void *a_netbsd_vnode);
int netbsd_vnode_inactive(void *a_netbsd_vnode, int *a_reclaimed);
int netbsd_vnode_bmap(void *a_netbsd_vnode, unsigned long a_lblkno, unsigned long *a_dblkno, unsigned long *a_dblkcnt);
int netbsd_vnode_read(void *a_netbsd_vnode, struct netbsdfs_uio *a_uio, int a_ioflags);
int netbsd_vnode_write(void *a_netbsd_vnode, struct netbsdfs_uio *a_uio, int a_ioflags);
int netbsd_vnode_readdir(void *a_netbsd_vnode, long *a_offset, unsigned long a_buflen, void *a_emx_file, void *a_emx_uio);
int netbsd_vnode_reclaim(void *a_netbsd_vnode, int a_is_deleted);

int netbsd_vnode_get_rdev(void *a_netbsd_vnode, unsigned *a_major, unsigned *a_minor);

unsigned netbsd_vnode_get_bshift(void *a_netbsd_vnode);


int netbsd_vttoif(enum netbsdfs_vtype a_vt);

int netbsd_mount_msdosfs(unsigned a_major, unsigned a_minor, void **netbsd_mount_out);




// From NetBSD layer

// diag output
void netbsdfs_noimp(const char *a_netbsd_func);
void netbsdfs_noimp_warn(const char *a_netbsd_func);
void netbsdfs_vprintf(const char *format, va_list arg);

// VFS

void netbsdfs_creation_lock_acquire();

void netbsdfs_creation_lock_release();


int netbsdfs_vfs_statfs_callback(void *a_netbsd_mount,
                                 void *user_data,
                                 unsigned long a_bsize,   /* file system block size */
                                 unsigned long a_frsize,  /* fundamental file system block size */
                                 unsigned long a_iosize,  /* optimal file system block size */
                                 unsigned long a_blocks,  /* number of blocks in file system, */
                                 unsigned long a_bfree,   /* free blocks avail in file system */
                                 unsigned long a_bavail,  /* free blocks avail to non-root */
                                 unsigned long a_bresvd,  /* blocks reserved for root */
                                 unsigned long a_files,   /* total file nodes in file system */
                                 unsigned long a_ffree,   /* free file nodes in file system */
                                 unsigned long a_favail,  /* free file nodes avail to non-root */
                                 unsigned long a_fresvd,  /* file nodes reserved for root */
                                 unsigned long a_fsid,    /* Posix compatible fsid */
                                 unsigned long a_namemax, /* maximum filename length */
                                 char const *a_fstypename /* fs type name */);

int netbsdfs_vfs_mount_alloc(void **a_netbsd_mount_out, unsigned long a_netbsd_mount_size);
void *netbsdfs_vfs_mount_get_extra_data_ptr(void *a_netbsd_mount, unsigned a_data_size);


int netbsdfs_vfs_vnode_alloc(void *a_netbsd_mount, unsigned long a_netbsd_vnode_size, void **a_netbsdfs_vnode_out);

int netbsdfs_vfs_cache_vnode_get(void *a_netbsd_mount, unsigned long a_netbsd_mount_size, void const *a_key, unsigned long a_keylen, void **a_netbsd_vnode_out);

int netbsdfs_vfs_cache_rekey_part1(void *a_netbsd_mount, unsigned long a_netbsd_mount_size, void *a_netbsdfs_vnode, unsigned long a_netbsdfs_vnode_size, void const *a_old_key, unsigned long a_old_key_len, void const *a_new_key, unsigned long a_new_key_len);

int netbsdfs_vfs_cache_rekey_part2(void *a_netbsd_mount, unsigned long a_netbsd_mount_size, void *a_netbsdfs_vnode, unsigned long a_netbsdfs_vnode_size, void const *a_old_key, unsigned long a_old_key_len, void const *a_new_key, unsigned long a_new_key_len);

void netbsdfs_vfs_vnode_free(void *a_netbsdfs_vnode, int a_is_deleted);

void netbsdfs_vfs_vnode_init(void *a_netbsdfs_vnode, struct rumpuser_rw **a_ppvmobjlock, void const *a_key, unsigned long a_keylen, void *a_netbsd_mount, unsigned long a_netbsd_mount_size, struct netbsdfs_vattr *a_vattr);

int netbsdfs_vfs_vnode_ref(void *a_netbsdfs_vnode);
int netbsdfs_vfs_vnode_rel(void *a_netbsdfs_vnode);

void netbsdfs_vfs_vnode_setsize(void *a_netbsdfs_vnode, unsigned long a_size);

int netbsdfs_vfs_vnode_readdir_callback(void *a_emx_uio,                  // Emerixx uio (not used by NetBSD glue)
                                        struct emerixx_dirent *a_edirent, // Filled by the NetBSD gluecode
                                        unsigned long *a_buflen_out);     // Inform the NetBSD code how much space we have left

int netbsdfs_vfs_vnode_ubc_uiomove(void *a_netbsdfs_vnode, struct netbsdfs_uio *a_uio, unsigned long a_len);
void netbsdfs_vfs_vnode_zero_range(void *a_netbsdfs_vnode, long a_off, unsigned len);
int netbsdfs_vfs_alloc_vnode_pages(void *a_netbsdfs_vnode, unsigned long a_pg_offset, int npages);
int netbsdfs_vnode_flush_buffers(void *a_netbsdfs_vnode, int a_flags);
  
void netbsdfs_vfs_node_iterator_init(void *a_netbsd_mount, void **a_vfs_node_iteratorp);
void *netbsdfs_vfs_node_iterator_next(void *a_vfs_node_iterator);
void netbsdfs_vfs_node_iterator_destroy(void *a_vfs_node_iterator);

void netbsdfs_balloc_lock(void *a_netbsd_mount);
void netbsdfs_balloc_unlock(void *a_netbsd_mount);

int netbsdfs_vfs_node_trunc_buffer(void *a_netbsdfs_vnode, unsigned a_fs_block, unsigned a_fs_bshift, unsigned a_dev_bshift);

// page alloction
int netbsdfs_submap_alloc(void *a_vmmap, unsigned long a_size, void *addr_out);
void netbsdfs_submap_free(void *a_vmmap, void *addr, unsigned long a_size);

// malloc like
void *netbsdfs_mem_aligned_alloc(unsigned long a_size, unsigned long a_alignment);
void *netbsdfs_mem_alloc(unsigned long a_size);
void netbsdfs_mem_free(void *a_ptr);

// mutex
void netbsdfs_mutex_alloc(void **a_mtx);
void netbsdfs_mutex_free(void *a_mtx);

void netbsdfs_mutex_enter(void *a_mtx);
int netbsdfs_mutex_try_enter(void *a_mtx);

void netbsdfs_mutex_exit(void *a_mtx);

// block io
int netbsdfs_block_read(int a_major, int a_minor, unsigned long a_blkno, unsigned long a_size, void **addr_out);

int netbsdfs_spec_invalidate_block(int a_major, int a_minor, unsigned long a_blkno, unsigned long a_block_size);

void netbsdfs_spec_flush_buffers(unsigned a_devmajor, unsigned a_devminor);

// misc

void netbsdfs_get_timespec(struct netbsdfs_timespec *a_ts);

#if defined(__cplusplus)
}
#endif
