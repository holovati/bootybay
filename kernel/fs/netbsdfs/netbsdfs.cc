#include <aux/defer.hh>
#include <aux/init.h>

#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/statfs.h>
#include <sys/sysmacros.h>

#include <etl/algorithm.hh>
#include <etl/bit.hh>
#include <etl/hash.hh>

#include <kernel/lock.h>
#include <kernel/vm/vm_kern.h>
#include <kernel/vm/vm_page.h>
#include <kernel/vm/vm_page_map.h>
#include <kernel/vm/vm_prot.h>
#include <kernel/mach_clock.h>
#include <kernel/zalloc.h>

#include <emerixx/chrdev.h>
#include <emerixx/disklabel.h>
#include <emerixx/memory_rw.h>
#include <emerixx/ioctl_req.h>
#include <emerixx/vfs/vfs_file.h>
#include <emerixx/vfs/vfs_file_data.h>
#include <emerixx/vfs/vfs_file_system.h>
#include <emerixx/vfs/vfs_mount_data.h>
#include <emerixx/vfs/vfs_node.h>
#include <emerixx/vfs/vfs_node_data.h>
#include <emerixx/poll.h>
#include <emerixx/fs/pipefs/pipefs.h>

#include <mach/conf.h>
#include <mach/machine/spl.h>

#include "bridge.h"

#define NETBSD_VNODES_COUNT 0x100

extern "C"
{
// Thease are defnied as global pointers in the netbsd layer.
extern vm_map_t kmem_va_arena;
extern vm_map_t kmem_meta_arena;
}

#define VFSM_TO_NETBSDFS_MOUNT(x)      ((struct netbsdfs_mount *)(x))
#define NETBSDMNT_TO_NETBSDFS_MOUNT(x) ((struct netbsdfs_mount *)(((char *)(x)) - (offsetof(netbsdfs_mount, m_netbsd_mntdata))))
struct netbsdfs_mount
{
        struct vfs_mount_data m_vfsm;
        struct netbsdfs_vfsn
                *m_iterator_vfs_node; // Set to the current vfs node during iteration. The global creation lock is held during this

        struct lock_data m_balloc_lock;
        queue_head_t m_vfs_nodes; // vfs nodes associated with this mount
        natural_t m_netbsd_mntdata[0xd80 / sizeof(natural_t)];
        natural_t m_netbsd_extra_data[0x140 / sizeof(natural_t)]; // To hold the specnode and devicevpp
};

ZONE_DEFINE(netbsdfs_mount, s_nbsd_vfsm_zone, 8, 4, ZONE_EXHAUSTIBLE, 4);

typedef struct vnode_cache_entry_data
{
        queue_chain_t m_link;
        void *m_netbsd_mount;
        void const *m_key;
        natural_t m_keylen;
} *vnode_cache_entry_t;

typedef queue_head_t *vnode_cache_bucket_t;
static queue_head_t s_vnode_cache_buckets[1024];
static queue_head_t s_inactive_vnodes;
static struct lock_data s_vnode_creation_lock;

struct netbsdfs_vfsn
{
        struct vfs_node_data m_vfsn;
        natural_t m_netbsd_vndata[0x100 / sizeof(natural_t)];
        struct rumpuser_rw m_uvmobj_rw;              // Used for "vp->v_uobj.vmobjlock"
        struct vnode_cache_entry_data m_cache_entry; // Does not ref the node
        vfs_node_t m_fifo_alias;                     // If this is a named fifo this field points to a pipe
        queue_chain_t m_mount_entry;                 // Does not ref the node
        queue_chain_t m_inactive_link;
        struct vm_page_map_data m_vmp_map;
};

ZONE_DEFINE(lock_data_t, s_mtx_zone, NETBSD_VNODES_COUNT * 2, 10, ZONE_EXHAUSTIBLE, NETBSD_VNODES_COUNT);

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"

template <size_t SZ>
struct memblock
{
        natural_t size;
        uint8_t data[SZ];
};

typedef memblock<0> mblockany;

#define MEMBLOCK_ZONE(sz)                                                                                                          \
        typedef memblock<sz> mblock##sz;                                                                                           \
        ZONE_DEFINE(mblock##sz, s_mblock##sz, 1000, 10, ZONE_EXHAUSTIBLE, 10)
MEMBLOCK_ZONE(64);
MEMBLOCK_ZONE(128);
MEMBLOCK_ZONE(256);
MEMBLOCK_ZONE(512);
MEMBLOCK_ZONE(1024);
MEMBLOCK_ZONE(2048);
MEMBLOCK_ZONE(4096);
#pragma GCC diagnostic pop

#define NETBSDFS_VNOVAL ((-1))
static struct netbsdfs_vattr const s_vanoval = {

        .va_type      = NETBSDFS_VNON,
        .va_mode      = (uint32_t)NETBSDFS_VNOVAL,
        .va_nlink     = (uint32_t)NETBSDFS_VNOVAL,
        .va_uid       = (uint32_t)NETBSDFS_VNOVAL,
        .va_gid       = (uint32_t)NETBSDFS_VNOVAL,
        .va_fsid      = (uint64_t)NETBSDFS_VNOVAL,
        .va_fileid    = (uint64_t)NETBSDFS_VNOVAL,
        .va_size      = (uint64_t)NETBSDFS_VNOVAL,
        .va_blocksize = NETBSDFS_VNOVAL,
        .va_atime     = { NETBSDFS_VNOVAL, NETBSDFS_VNOVAL },
        .va_mtime     = { NETBSDFS_VNOVAL, NETBSDFS_VNOVAL },
        .va_ctime     = { NETBSDFS_VNOVAL, NETBSDFS_VNOVAL },
        .va_birthtime = { NETBSDFS_VNOVAL, NETBSDFS_VNOVAL },
        .va_gen       = (unsigned long)NETBSDFS_VNOVAL,
        .va_flags     = (unsigned long)NETBSDFS_VNOVAL,
        .va_rdev      = (uint64_t)NETBSDFS_VNOVAL,
        .va_bytes     = (uint64_t)NETBSDFS_VNOVAL,
        .va_filerev   = (uint64_t)NETBSDFS_VNOVAL,
        .va_vaflags   = (unsigned int)NETBSDFS_VNOVAL
};

int warnz;

// ********************************************************************************************************************************
void netbsdfs_noimp(const char *a_netbsd_func_name)
// ********************************************************************************************************************************
{
        panic("%s Not implemented", a_netbsd_func_name);
}

// ********************************************************************************************************************************
void netbsdfs_noimp_warn(const char *a_netbsd_func_name)
// ********************************************************************************************************************************
{
        warnz++;
        log_warn("%s not implemented", a_netbsd_func_name);
}

// ********************************************************************************************************************************
void netbsdfs_vprintf(const char *format, va_list arg)
// ********************************************************************************************************************************
{
        warnz++;
        log_info(format, arg);
}

// ********************************************************************************************************************************
int netbsdfs_submap_alloc(void *a_vmmap, unsigned long a_size, void *addr_out)
// ********************************************************************************************************************************
{
        vm_map_t vmmap      = (vm_map_t)(a_vmmap);
        vm_offset_t *addrpp = (vm_offset_t *)addr_out;

        kern_return_t retval = kmem_alloc(vmmap, addrpp, a_size);

        return retval == KERN_SUCCESS ? 0 : ENOMEM;
}

// ********************************************************************************************************************************
void netbsdfs_submap_free(void *a_vmmap, void *a_addr, unsigned long a_size)
// ********************************************************************************************************************************
{
        vm_map_t vmmap   = (vm_map_t)(a_vmmap);
        vm_offset_t addr = (vm_offset_t)a_addr;
        kmem_free(vmmap, addr, a_size);
}

// ********************************************************************************************************************************
void *netbsdfs_mem_aligned_alloc(unsigned long a_size, unsigned long a_alignment)
// ********************************************************************************************************************************
{
        KASSERT(a_alignment <= 8);

        if (a_size > PAGE_SIZE)
        {
                a_size += sizeof(natural_t);
        }

        if (etl::is_power_of_two(a_size) == false)
        {
                a_size = etl::ror(1, etl::countl_zero(a_size));
        }

        mblockany *mbany = nullptr;

        switch (a_size)
        {
                case 2:
                case 4:
                case 8:
                case 16:
                case 32:
                case 64:
                        mbany = (mblockany *)s_mblock64.alloc();
                        break;
                case 128:
                        mbany = (mblockany *)s_mblock128.alloc();
                        break;
                case 256:
                        mbany = (mblockany *)s_mblock256.alloc();
                        break;
                case 512:
                        mbany = (mblockany *)s_mblock512.alloc();
                        break;
                case 1024:
                        mbany = (mblockany *)s_mblock1024.alloc();
                        break;
                case 2048:
                        mbany = (mblockany *)s_mblock2048.alloc();
                        break;
                case 4096:
                        mbany = (mblockany *)s_mblock4096.alloc();
                        break;
                default:
                        KASSERT(kmem_alloc(kernel_map, (vm_offset_t *)&mbany, a_size) == KERN_SUCCESS);
                        break;
        }

        mbany->size = a_size;

        return mbany->data;
}

// ********************************************************************************************************************************
void *netbsdfs_mem_alloc(unsigned long a_size)
// ********************************************************************************************************************************
{
        return netbsdfs_mem_aligned_alloc(a_size, 8);
}

// ********************************************************************************************************************************
void netbsdfs_mem_free(void *a_ptr)
// ********************************************************************************************************************************
{
        mblockany *mbany = (mblockany *)(((uint8_t *)a_ptr) - sizeof(natural_t));
        switch (mbany->size)
        {
                case 2:
                case 4:
                case 8:
                case 16:
                case 32:
                case 64:
                        s_mblock64.free((mblock64 *)(mbany));
                        break;
                case 128:
                        s_mblock128.free((mblock128 *)(mbany));
                        break;
                case 256:
                        s_mblock256.free((mblock256 *)(mbany));
                        break;
                case 512:
                        s_mblock512.free((mblock512 *)(mbany));
                        break;
                case 1024:
                        s_mblock1024.free((mblock1024 *)(mbany));
                        break;
                case 2048:
                        s_mblock2048.free((mblock2048 *)(mbany));
                        break;
                case 4096:
                        s_mblock4096.free((mblock4096 *)(mbany));
                        break;
                default:
                        kmem_free(kernel_map, (vm_offset_t)mbany, mbany->size);
                        break;
        }
}

// ********************************************************************************************************************************
void netbsdfs_mutex_alloc(void **a_mtx)
// ********************************************************************************************************************************
{
        lock_t l = s_mtx_zone.alloc();
        lock_init(l, true);
        *a_mtx = l;
}

// ********************************************************************************************************************************
void netbsdfs_mutex_free(void *a_mtx)
// ********************************************************************************************************************************
{
        s_mtx_zone.free((lock_t)a_mtx);
}

// ********************************************************************************************************************************
void netbsdfs_mutex_enter(void *a_mtx)
// ********************************************************************************************************************************
{
        lock_write((lock_t)a_mtx);
}

// ********************************************************************************************************************************
int netbsdfs_mutex_try_enter(void *a_mtx)
// ********************************************************************************************************************************
{
        return lock_try_write((lock_t)a_mtx) == true;
}

// ********************************************************************************************************************************
void netbsdfs_mutex_exit(void *a_mtx)
// ********************************************************************************************************************************
{
        lock_write_done((lock_t)a_mtx);
}

// ********************************************************************************************************************************
static vfs_node_t netbsdfs_vn_to_vfsn(void *a_netbsd_vn)
// ********************************************************************************************************************************
{
        return (vfs_node_t)((char *)a_netbsd_vn - sizeof(vfs_node_data));
}

// ********************************************************************************************************************************
static struct netbsdfs_vfsn *vfsn_to_nvfsn(vfs_node_t a_vfsn)
// ********************************************************************************************************************************
{
        return (struct netbsdfs_vfsn *)(a_vfsn);
}

// ********************************************************************************************************************************
static kern_return_t netbsdfs_vfsm_root(vfs_mount_t a_vfsm, vfs_node_t *a_vfsn_out)
// ********************************************************************************************************************************
{
        struct netbsdfs_mount *nbsdm = VFSM_TO_NETBSDFS_MOUNT(a_vfsm);

        void *netbsd_vnode = nullptr;

        int netbsd_retval = netbsd_vfs_root(nbsdm->m_netbsd_mntdata, &netbsd_vnode);

        if (netbsd_retval != 0)
        {
                return KERN_FAIL(netbsd_retval);
        }

        *a_vfsn_out = netbsdfs_vn_to_vfsn(netbsd_vnode);

        return KERN_SUCCESS;
}

struct netbsdfs_vfs_statfs_callback_data
{
        vfs_mount_stat_t statbuf;
};

// ********************************************************************************************************************************
int netbsdfs_vfs_statfs_callback(void *a_netbsd_mount,
                                 void *user_data,
                                 unsigned long a_bsize,   /* file system block size */
                                 unsigned long a_frsize,  /* fundamental file system block size */
                                 unsigned long a_iosize,  /* optimal file system block size */
                                 unsigned long a_blocks,  /* number of blocks in file system, */
                                 unsigned long a_bfree,   /* free blocks avail in file system */
                                 unsigned long a_bavail,  /* free blocks avail to non-root */
                                 unsigned long a_bresvd,  /* blocks reserved for root */
                                 unsigned long a_files,   /* total file nodes in file system */
                                 unsigned long a_ffree,   /* free file nodes in file system */
                                 unsigned long a_favail,  /* free file nodes avail to non-root */
                                 unsigned long a_fresvd,  /* file nodes reserved for root */
                                 unsigned long a_fsid,    /* Posix compatible fsid */
                                 unsigned long a_namemax, /* maximum filename length */
                                 char const *a_fstypename /* fs type name */)
// ********************************************************************************************************************************
{
        struct netbsdfs_vfs_statfs_callback_data *cb_data = (struct netbsdfs_vfs_statfs_callback_data *)(user_data);

        cb_data->statbuf->f_type                           = 0xef53; // EXT2_SUPER_MAGIC; // TODO fix this
        cb_data->statbuf->f_bsize                          = a_bsize;
        cb_data->statbuf->f_blocks                         = a_blocks;
        cb_data->statbuf->f_bfree                          = a_bfree;
        cb_data->statbuf->f_bavail                         = a_bavail;
        cb_data->statbuf->f_files                          = a_files;
        cb_data->statbuf->f_ffree                          = a_ffree;
        *(unsigned long *)(cb_data->statbuf->f_fsid.__val) = a_fsid;
        cb_data->statbuf->f_namelen                        = a_namemax;

        return 0;
}

// ********************************************************************************************************************************
static kern_return_t netbsdfs_vfsm_stat(vfs_mount_t a_vfsm, vfs_mount_stat_t a_statbuf)
// ********************************************************************************************************************************
{
        struct netbsdfs_mount *nbsdm = VFSM_TO_NETBSDFS_MOUNT(a_vfsm);

        struct netbsdfs_vfs_statfs_callback_data cb_data = { .statbuf = a_statbuf };

        int netbsd_error = netbsd_vfs_statfs(nbsdm->m_netbsd_mntdata, &cb_data);

        return KERN_FAIL(netbsd_error);
}

// ********************************************************************************************************************************
static kern_return_t netbsdfs_vfsm_sync(vfs_mount_t a_vfsm, boolean_t a_wait)
// ********************************************************************************************************************************
{

        struct netbsdfs_mount *nbsdm = VFSM_TO_NETBSDFS_MOUNT(a_vfsm);

        int netbsd_error = netbsd_vfs_sync(nbsdm->m_netbsd_mntdata, !!a_wait);

        netbsdfs_creation_lock_acquire();

        if (queue_empty(&nbsdm->m_vfs_nodes))
        {
                netbsdfs_creation_lock_release();

                return KERN_FAIL(netbsd_error);
        }

        struct netbsdfs_vfsn *nvfsn
                = queue_containing_record(queue_first(&nbsdm->m_vfs_nodes), struct netbsdfs_vfsn, m_mount_entry);

        unsigned dev_major, dev_minor;
        netbsd_vnode_get_rdev(nvfsn->m_netbsd_vndata, &dev_major, &dev_minor);

        netbsdfs_creation_lock_release();

        netbsdfs_spec_flush_buffers(dev_major, dev_minor);

        return KERN_FAIL(netbsd_error);
}

static struct vfs_mount_operations_data netbsdfs_mount_ops = {

        .root = netbsdfs_vfsm_root,
        .stat = netbsdfs_vfsm_stat,
        .sync = netbsdfs_vfsm_sync
};

// ********************************************************************************************************************************
int netbsdfs_vfs_mount_alloc(void **a_netbsd_mount_out,
                             unsigned long a_netbsd_mount_size) // Called from netbsd layer when allocating a mount
// ********************************************************************************************************************************
{
        struct netbsdfs_mount *nbsdm = s_nbsd_vfsm_zone.alloc();

        if (nbsdm == nullptr)
        {
                return ENOMEM;
        }

        KASSERT(a_netbsd_mount_size == sizeof(nbsdm->m_netbsd_mntdata));

        vfs_mount_t vfsm = &nbsdm->m_vfsm;

        vfs_mount_init(vfsm);

        queue_init(&nbsdm->m_vfs_nodes);

        lock_init(&nbsdm->m_balloc_lock, true);

        etl::clear(nbsdm->m_netbsd_mntdata);

        etl::clear(nbsdm->m_netbsd_extra_data);

        vfsm->m_ops = &netbsdfs_mount_ops;

        *a_netbsd_mount_out = nbsdm->m_netbsd_mntdata;

        return 0;
}

// ********************************************************************************************************************************
void *netbsdfs_vfs_mount_get_extra_data_ptr(void *a_netbsd_mount, unsigned a_data_size)
// ********************************************************************************************************************************
{
        struct netbsdfs_mount *nbsdm = NETBSDMNT_TO_NETBSDFS_MOUNT(a_netbsd_mount);

        if (a_data_size > sizeof nbsdm->m_netbsd_extra_data)
        {
                return nullptr;
        }
        else
        {
                return nbsdm->m_netbsd_extra_data;
        }
}

// ********************************************************************************************************************************
static kern_return_t netbsdfs_file_open(vfs_node_t a_vfsn, vfs_file_t a_file)
// ********************************************************************************************************************************
{
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t netbsdfs_file_close(vfs_file_t a_file)
// ********************************************************************************************************************************
{
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
int netbsdfs_vfs_vnode_readdir_callback(void *a_uio,                      // Emerixx uio (not used by NetBSD glue)
                                        struct emerixx_dirent *a_edirent, // Filled by the NetBSD gluecode
                                        unsigned long *a_buflen_out)      // Inform the NetBSD code how much space we have left
// ********************************************************************************************************************************
{
        memory_rdwr_t memrw = (memory_rdwr_t)a_uio;

        kern_return_t retval = uiomove(a_edirent, a_edirent->d_reclen, memrw);

        if (KERN_STATUS_SUCCESS(retval))
        {
                *a_buflen_out = memrw->uio_resid;
        }

        return -retval; // Remove the sign for netbsd glue
}

// ********************************************************************************************************************************
static kern_return_t netbsdfs_getdents(vfs_file_t a_vfsf, memory_rdwr_t a_memrw)
// ********************************************************************************************************************************
{
        struct netbsdfs_vfsn *nvfsn = vfsn_to_nvfsn(a_vfsf->m_vfsn);

        // The glue code will fire callbacks per dirent, we use them du fill mem descriptor
        vm_object_lock(&nvfsn->m_vfsn.m_vmo);
        int netbsd_error = netbsd_vnode_readdir(nvfsn->m_netbsd_vndata, &a_vfsf->m_offset, a_memrw->uio_resid, a_vfsf, a_memrw);
        vm_object_unlock(&nvfsn->m_vfsn.m_vmo);

        return KERN_FAIL(netbsd_error);
}

// ********************************************************************************************************************************
static kern_return_t netbsdfs_readwrite_isdir(vfs_file_t a_vfsf, memory_rdwr_t a_memrw)
// ********************************************************************************************************************************
{
        return KERN_FAIL(EISDIR);
}

// ********************************************************************************************************************************
static kern_return_t netbsdfs_file_lseek_spipe(vfs_file_t a_vfsf, off_t a_offset, int a_whence)
// ********************************************************************************************************************************
{
        return KERN_FAIL(ESPIPE);
}

static struct vfs_file_operations_data netbsdfs_dir_ops = {

        .fopen     = netbsdfs_file_open,
        .fread     = netbsdfs_readwrite_isdir,
        .fwrite    = netbsdfs_readwrite_isdir,
        .flseek    = netbsdfs_file_lseek_spipe,
        .fioctl    = vfs_file_op_ioctl_enotty,
        .fclose    = netbsdfs_file_close,
        .fgetdents = netbsdfs_getdents,
        .fmmap     = vfs_file_op_mmap_enodev
};

// ********************************************************************************************************************************
static kern_return_t netbsdfs_getdents_nodir(vfs_file_t a_vfsf, memory_rdwr_t a_memrw)
// ********************************************************************************************************************************
{
        return KERN_FAIL(ENOTDIR);
}

// ********************************************************************************************************************************
static kern_return_t netbsdfs_file_reg_mmap(vfs_file_t a_vfsf, vm_object_t *a_vm_object_out)
// ********************************************************************************************************************************
{
        vm_object_ref(*a_vm_object_out = &a_vfsf->m_vfsn->m_vmo);
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t netbsdfs_file_reg_rw(vfs_file_t a_vfsf, memory_rdwr_t a_memrw)
// ********************************************************************************************************************************
{
        a_memrw->uio_offset = a_vfsf->m_offset;
        a_memrw->aux.s = a_vfsf->m_oflags; // We need to pass this along so we can set the correct netbsd IO flags e.g IO_APPEND

        struct netbsdfs_vfsn *nvfsn = vfsn_to_nvfsn(a_vfsf->m_vfsn);
        vm_object_lock(&nvfsn->m_vfsn.m_vmo);
        kern_return_t retval = a_vfsf->m_vfsn->m_ops->rdwr(a_vfsf->m_vfsn, a_vfsf->m_oflags, a_memrw);
        vm_object_unlock(&nvfsn->m_vfsn.m_vmo);

        if (KERN_STATUS_SUCCESS(retval))
        {
                a_vfsf->m_offset = (off_t)(a_memrw->uio_offset);
        }

        return retval;
}

// ********************************************************************************************************************************
static kern_return_t netbsdfs_file_reg_lseek(vfs_file_t a_vfsf, off_t a_offset, int a_whence)
// ********************************************************************************************************************************
{
        struct stat statbuf;

        struct netbsdfs_vfsn *nvfsn = vfsn_to_nvfsn(a_vfsf->m_vfsn);
        vm_object_lock(&nvfsn->m_vfsn.m_vmo);
        kern_return_t retval = a_vfsf->m_vfsn->m_ops->stat(a_vfsf->m_vfsn, &statbuf);
        vm_object_unlock(&nvfsn->m_vfsn.m_vmo);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        off_t new_offset = a_vfsf->m_offset;

        switch (a_whence)
        {
                case SEEK_CUR:
                        new_offset += a_offset;
                        break;
                case SEEK_END:
                        new_offset = a_offset + statbuf.st_size;
                        break;
                case SEEK_SET:
                        new_offset = a_offset;
                        break;
                default:
                        return KERN_FAIL(EINVAL);
        }

        if (new_offset < 0)
        {
                return KERN_FAIL(EINVAL);
        }

        return (kern_return_t)(a_vfsf->m_offset = new_offset);
}

// ********************************************************************************************************************************
static kern_return_t netbsdfs_reg_poll(vfs_file_t, struct pollfd **a_pfd)
// ********************************************************************************************************************************
{
        (*a_pfd)->revents = ((POLLIN | POLLRDNORM | POLLOUT | POLLWRNORM) & (*a_pfd)->events);
        return KERN_SUCCESS;
}

static struct vfs_file_operations_data netbsdfs_reg_ops = {

        .fopen     = netbsdfs_file_open,
        .fread     = netbsdfs_file_reg_rw,
        .fwrite    = netbsdfs_file_reg_rw,
        .flseek    = netbsdfs_file_reg_lseek,
        .fioctl    = vfs_file_op_ioctl_enotty,
        .fclose    = netbsdfs_file_close,
        .fgetdents = netbsdfs_getdents_nodir,
        .fmmap     = netbsdfs_file_reg_mmap,
        .fpoll     = netbsdfs_reg_poll
};

// ********************************************************************************************************************************
static kern_return_t netbsdfs_sock_open(vfs_node_t a_vfsn, vfs_file_t a_file)
// ********************************************************************************************************************************
{
        return KERN_FAIL(ENXIO);
}

static struct vfs_file_operations_data netbsdfs_sock_fops = {

        .fopen = netbsdfs_sock_open,

};

// ********************************************************************************************************************************
static kern_return_t netbsdfs_fifo_open(vfs_node_t a_vfsn, vfs_file_t a_file)
// ********************************************************************************************************************************
{
        struct netbsdfs_vfsn *nvfsn = vfsn_to_nvfsn(a_vfsn);

        kern_return_t retval = KERN_SUCCESS;

        if (nvfsn->m_fifo_alias == nullptr)
        {
                retval = pipefs_create_aliased(a_vfsn, &nvfsn->m_fifo_alias);

                // The fifo is born with one ref, we use that one
                a_file->m_vfsn = nvfsn->m_fifo_alias;
        }
        else
        {
                // Add a ref to the fifo we already have, we emulate what vfs_file_open does.
                vfs_node_ref(a_file->m_vfsn = nvfsn->m_fifo_alias);
                retval = KERN_SUCCESS;
        }

        if (KERN_STATUS_SUCCESS(retval))
        {
                a_file->m_fops = nvfsn->m_fifo_alias->m_fops;

                vm_object_unlock(&nvfsn->m_vfsn.m_vmo);
                retval = nvfsn->m_fifo_alias->m_fops->fopen(nvfsn->m_fifo_alias, a_file);
                vm_object_lock(&nvfsn->m_vfsn.m_vmo);

                if (KERN_STATUS_SUCCESS(retval))
                {
                        vfs_node_rel(a_vfsn); // Loose the ref gained in vfs_file_open
                }
                else
                {
                        goto fail;
                }
        }
        else
        {
        fail:
                // Put everyhing back in case of a failure
                a_file->m_vfsn = a_vfsn;
                a_file->m_fops = a_vfsn->m_fops;

                if (nvfsn->m_fifo_alias != nullptr)
                {
                        vfs_node_rel(nvfsn->m_fifo_alias);
                        nvfsn->m_fifo_alias = nullptr;
                }
        }

        // KASSERT(a_vfsn->m_vmo.m_usecnt == 2); // Only 2 refs at all times

        return retval;
}

static struct vfs_file_operations_data netbsdfs_fifo_fops = {
        .fopen = netbsdfs_fifo_open //
};

// ********************************************************************************************************************************
static int bmap_vmp(struct netbsdfs_vfsn *a_nvfsn, vm_page_t a_vmp)
// ********************************************************************************************************************************
{
        vm_page_io_descr_t vmpiod = a_vmp->m_io_descr;

        natural_t fs_shift = netbsd_vnode_get_bshift(a_nvfsn->m_netbsd_vndata);

        natural_t fsbpp  = PAGE_SIZE >> fs_shift;        // fs blocks pr page
        natural_t nsect  = 1 << (fs_shift - DEV_BSHIFT); // disk sectors pr fs block
        natural_t blkno  = 0;                            // file block number
        natural_t blkcnt = 0;                            // contigious blocks after blkno

        int netbsd_error = 0;

        natural_t vnsize = roundup(a_nvfsn->m_vfsn.m_vmo.m_size, 1 < fs_shift);

        for (natural_t pb = 0; pb < fsbpp; pb += blkcnt)
        {
                natural_t vnoffset = a_vmp->offset + (pb << fs_shift);

                if (vnoffset < vnsize)
                {

                        blkno = vnoffset >> fs_shift;

                        blkcnt = 0;

                        netbsd_error = netbsd_vnode_bmap(a_nvfsn->m_netbsd_vndata, blkno, &blkno, &blkcnt);

                        if (netbsd_error != 0)
                        {
                                panic("why");
                                break;
                        }
                }
                else
                {
                        blkno = ((natural_t)-1);
                }

                natural_t startidx = (pb * nsect);

                if (blkno == ((natural_t)-1))
                {
                        blkcnt = 1;

                        natural_t disk_blkcnt = (blkcnt * nsect);

                        for (natural_t sect = 0; sect < disk_blkcnt; ++sect)
                        {
                                vmpiod->m_lba[startidx + sect] = blkno;
                        }
                }
                else
                {
                        // blkcnt returns (blkno + additional) blocks, hence the + 1 to get the total
                        blkcnt = etl::min(blkcnt + 1, fsbpp - pb);

                        natural_t disk_blkcnt = (blkcnt * nsect);

                        for (natural_t block = 0; block < disk_blkcnt; ++block)
                        {
                                vmpiod->m_lba[startidx + block] = (blkno + block);
                        }
                }
        }

        vmpiod->m_write = false;

        return netbsd_error;
}

// ********************************************************************************************************************************
static kern_return_t netbsdfs_get_page(vm_object_t a_vm_obj, vm_offset_t a_vmo, vm_prot_t a_vm_prot, vm_page_t *a_vmp_out)
// ********************************************************************************************************************************
{
        vfs_node_t vfsn = (vfs_node_t)a_vm_obj;

        struct netbsdfs_vfsn *nvfsn = vfsn_to_nvfsn(vfsn);

        vm_page_t vmp = vm_page_map_lookup(&nvfsn->m_vmp_map, a_vmo);

        if (vmp == nullptr)
        {
                vmp = vm_page_alloc(&nvfsn->m_vmp_map, a_vmo);

                int netbsd_error = bmap_vmp(nvfsn, vmp);

                KASSERT(netbsd_error == 0);

                KASSERT(vmp->m_io_descr->m_write == false);

                unsigned dev_major, dev_minor;
                netbsd_vnode_get_rdev(nvfsn->m_netbsd_vndata, &dev_major, &dev_minor);

                netbsd_error = -bdevsw[dev_major].d_strategy3(makedev(dev_major, dev_minor), vmp);

                KASSERT(netbsd_error == 0);
        }

        *a_vmp_out = vmp;

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
int netbsdfs_vfs_alloc_vnode_pages(void *a_netbsdfs_vnode,
                                   unsigned long a_pg_offset,
                                   int npages) // Called when allocating new pages
// ********************************************************************************************************************************
{
        vfs_node_t vfsn             = netbsdfs_vn_to_vfsn(a_netbsdfs_vnode);
        struct netbsdfs_vfsn *nvfsn = vfsn_to_nvfsn(vfsn);

        int netbsd_error = 0;

        for (int pg = 0; pg < npages; ++pg)
        {
                vm_offset_t curoffset = (vm_offset_t)(a_pg_offset) + (PAGE_SIZE * pg);

                vm_page_t vmp = vm_page_map_lookup(&nvfsn->m_vmp_map, curoffset);

                if (vmp == nullptr)
                {
                        vmp = vm_page_alloc(&nvfsn->m_vmp_map, curoffset);

                        netbsd_error = bmap_vmp(nvfsn, vmp);

                        unsigned dev_major, dev_minor;
                        netbsd_vnode_get_rdev(nvfsn->m_netbsd_vndata, &dev_major, &dev_minor);
                        netbsd_error = -bdevsw[dev_major].d_strategy3(makedev(dev_major, dev_minor), vmp);
                }
                else
                {
                        netbsd_error = bmap_vmp(nvfsn, vmp);
                }

                if (netbsd_error != 0)
                {
                        break;
                }

                vmp->flags |= PG_DIRTY;

                if (vmp->m_io_descr->m_lba[0] == ((natural_t)-1))
                {
                        asm("nop"); // Whoa
                }
        }

        KASSERT(netbsd_error == 0);

        return netbsd_error;
}

// ********************************************************************************************************************************
static kern_return_t nvfsn_flush_buffers(netbsdfs_vfsn *a_nvfsn)
// ********************************************************************************************************************************
{
        KASSERT(vm_object_is_locked(&a_nvfsn->m_vfsn.m_vmo) || (a_nvfsn->m_vfsn.m_vmo.m_usecnt == 0));

        kern_return_t retval = KERN_SUCCESS;

        unsigned dev_major, dev_minor;
        netbsd_vnode_get_rdev(a_nvfsn->m_netbsd_vndata, &dev_major, &dev_minor);

        // Write out the dirty pages
        for (vm_page_t vmp = vm_page_map_first(&a_nvfsn->m_vmp_map); vmp != nullptr; vmp = vm_page_map_next(vmp))
        {
#if some_debug
                if (vm_page_modified(vmp) == true)
                {
                        panic("what");
                }
#endif
                if (vmp->flags & PG_DIRTY)
                {
                        vmp->m_io_descr->m_write = true;

                        retval = bdevsw[dev_major].d_strategy3(makedev(dev_major, dev_minor), vmp);

                        vmp->m_io_descr->m_write = false;

                        vmp->flags &= ~PG_DIRTY;

                        KASSERT(KERN_STATUS_SUCCESS(retval));
                }
        }

        return retval;
}

static lock_data block_pg_map_lock[3];
static vm_page_map_data block_pg_map[3];

// ********************************************************************************************************************************
int netbsdfs_vnode_flush_buffers(void *a_netbsdfs_vnode, int a_flags)
// ********************************************************************************************************************************
{
        vfs_node_t vfsn = netbsdfs_vn_to_vfsn(a_netbsdfs_vnode);

        struct netbsdfs_vfsn *nvfsn = vfsn_to_nvfsn(vfsn);

        int retval = nvfsn_flush_buffers(nvfsn);

        return retval;
}

// ********************************************************************************************************************************
void netbsdfs_spec_flush_buffers(unsigned a_devmajor, unsigned a_devminor)
// ********************************************************************************************************************************
{
        kern_return_t netbsd_retval = KERN_SUCCESS;

        lock_write(&block_pg_map_lock[a_devminor]);
        for (vm_page_t vmp = vm_page_map_first(&block_pg_map[a_devminor]); vmp != nullptr; vmp = vm_page_map_next(vmp))
        {
                if (1 /*vmp->flags & PG_DIRTY*/) // TODO We can check if the page are written to
                {
                        vmp->m_io_descr->m_write = true;

                        // vmp->m_io_descr->m_dev_blocks[0].nsect = PAGE_SIZE >> DEV_BSHIFT;

                        netbsd_retval = bdevsw[a_devmajor].d_strategy3(makedev(a_devmajor, a_devminor), vmp);

                        // vmp->m_io_descr->m_dev_blocks[0].nsect = DEV_BSIZE >> DEV_BSHIFT;

                        vmp->m_io_descr->m_write = false;

                        vmp->flags &= ~PG_DIRTY;

                        KASSERT(KERN_STATUS_SUCCESS(netbsd_retval));
                }
        }
        lock_write_done(&block_pg_map_lock[a_devminor]);
}

// ********************************************************************************************************************************
void netbsdfs_vfs_node_iterator_init(void *a_netbsd_mount, void **a_vfs_node_iteratorp)
// ********************************************************************************************************************************
{
        KASSERT(lock_try_write(&s_vnode_creation_lock) == false);

        struct netbsdfs_mount *nbsdm = NETBSDMNT_TO_NETBSDFS_MOUNT(a_netbsd_mount);

        nbsdm->m_iterator_vfs_node = nullptr;

        *a_vfs_node_iteratorp = nbsdm;
}

// ********************************************************************************************************************************
void *netbsdfs_vfs_node_iterator_next(void *a_vfs_node_iterator)
// ********************************************************************************************************************************
{
        KASSERT(lock_try_write(&s_vnode_creation_lock) == false);

        struct netbsdfs_mount *nbsdm = (struct netbsdfs_mount *)(a_vfs_node_iterator);

        queue_entry_t qentry = queue_first(&nbsdm->m_vfs_nodes);

        if (nbsdm->m_iterator_vfs_node != nullptr)
        {
                vm_object_unlock(&nbsdm->m_iterator_vfs_node->m_vfsn.m_vmo);
                qentry = queue_next(&nbsdm->m_iterator_vfs_node->m_mount_entry);
        }

        void *netbsd_vnode = nullptr;

        if (queue_end(&nbsdm->m_vfs_nodes, qentry) == false)
        {
                struct netbsdfs_vfsn *nvfsn = queue_containing_record(qentry, struct netbsdfs_vfsn, m_mount_entry);

                vm_object_lock(&nvfsn->m_vfsn.m_vmo);

                nbsdm->m_iterator_vfs_node = nvfsn;

                netbsd_vnode = nvfsn->m_netbsd_vndata;
        }

        return netbsd_vnode;
}

// ********************************************************************************************************************************
void netbsdfs_vfs_node_iterator_destroy(void *a_vfs_node_iterator)
// ********************************************************************************************************************************
{
        KASSERT(lock_try_write(&s_vnode_creation_lock) == false);

        struct netbsdfs_mount *nbsdm = (struct netbsdfs_mount *)(a_vfs_node_iterator);

        if (nbsdm->m_iterator_vfs_node != nullptr)
        {
                nbsdm->m_iterator_vfs_node = nullptr;
        }
}

// ********************************************************************************************************************************
void netbsdfs_balloc_lock(void *a_netbsd_mount)
// ********************************************************************************************************************************
{
        struct netbsdfs_mount *nbsdm = NETBSDMNT_TO_NETBSDFS_MOUNT(a_netbsd_mount);
        lock_write(&nbsdm->m_balloc_lock);
}

// ********************************************************************************************************************************
void netbsdfs_balloc_unlock(void *a_netbsd_mount)
// ********************************************************************************************************************************
{
        struct netbsdfs_mount *nbsdm = NETBSDMNT_TO_NETBSDFS_MOUNT(a_netbsd_mount);
        lock_write_done(&nbsdm->m_balloc_lock);
}

// ********************************************************************************************************************************
int netbsdfs_vfs_node_trunc_buffer(void *a_netbsdfs_vnode, unsigned a_fs_block, unsigned a_fs_bshift, unsigned a_dev_bshift)
// ********************************************************************************************************************************
{
        vfs_node_t vfsn = netbsdfs_vn_to_vfsn(a_netbsdfs_vnode);

        struct netbsdfs_vfsn *nvfsn = vfsn_to_nvfsn(vfsn);

        if (a_fs_block == 0)
        {
                vm_page_map_clear(&nvfsn->m_vmp_map);
                return KERN_SUCCESS;
        }

        natural_t dev_logical_offset = (a_fs_block << a_fs_bshift);
        natural_t dev_object_offset  = dev_logical_offset & ~PAGE_MASK;
        natural_t dev_page_offset    = dev_logical_offset & PAGE_MASK;

        vm_page_t vmp = vm_page_map_lookup(&nvfsn->m_vmp_map, dev_object_offset);

        if (vmp == nullptr)
        {
                return KERN_SUCCESS;
        }

        if (dev_page_offset > 0) // Does the new end leave a partial page, if so free the remainder.
        {
                natural_t page_sector_count = (PAGE_SIZE >> a_dev_bshift);
                for (natural_t sect = dev_page_offset >> a_dev_bshift; sect < page_sector_count; sect++)
                {
                        vmp->m_io_descr->m_lba[sect] = ((natural_t)(-1));
                }

                vmp = vm_page_map_next(vmp);
        }

        while (vmp != nullptr) // If there is more after the eventrual partial page, free it.
        {
                vm_page_t vmp_next = vm_page_map_next(vmp);

                vm_page_free(&nvfsn->m_vmp_map, vmp);

                vmp = vmp_next;
        }

        return KERN_SUCCESS;
}

int s_inactive_cnt;

// ********************************************************************************************************************************
static kern_return_t netbsdfs_inactive(vm_object_t a_vmo)
// ********************************************************************************************************************************
{
        vfs_node_t vfsn = (vfs_node_t)(a_vmo);

        struct netbsdfs_vfsn *nvfsn = vfsn_to_nvfsn(vfsn);

        if (nvfsn->m_fifo_alias != nullptr)
        {
                nvfsn->m_fifo_alias = nullptr;
        }

        int is_reclaimed = false;

        int netbsd_error = netbsd_vnode_inactive(nvfsn->m_netbsd_vndata, &is_reclaimed);

        lock_write(&s_vnode_creation_lock);
        if (!!is_reclaimed == true)
        {
                // Remove it from the hash table
                remqueue(&nvfsn->m_cache_entry.m_link);

                // Let the netbsd part do its part
                netbsd_vnode_reclaim(nvfsn->m_netbsd_vndata, is_reclaimed);
        }
        else
        {
                enqueue_head(&s_inactive_vnodes, &nvfsn->m_inactive_link);
                s_inactive_cnt++;
        }
        lock_write_done(&s_vnode_creation_lock);

        return KERN_FAIL(netbsd_error);
}

static struct vm_object_operations_data netbsdfs_vm_obj_oops = {
        //
        .get_page = netbsdfs_get_page,
        .inactive = netbsdfs_inactive
};

// ********************************************************************************************************************************
static kern_return_t netbsdfs_vfsn_lookup(vfs_node_t a_vfsn,
                                          char const *a_cname,
                                          natural_t a_cnamelen,
                                          vfs_node_lookup_type_t a_lookup_type,
                                          int a_flags,
                                          vfs_node_t *a_vfsn_out)
// ********************************************************************************************************************************
{
        struct netbsdfs_vfsn *nvfsn = vfsn_to_nvfsn(a_vfsn);

        void *netbsd_vnode = nullptr;

        int netbsd_retval
                = netbsd_vnode_lookup(nvfsn->m_netbsd_vndata, a_cname, a_cnamelen, (int)a_lookup_type, a_flags, &netbsd_vnode);

        if (netbsd_retval == 0)
        {
                *a_vfsn_out = netbsdfs_vn_to_vfsn(netbsd_vnode);
        }

        return KERN_FAIL(netbsd_retval);
}

// ********************************************************************************************************************************
static kern_return_t netbsdfs_vfsn_create(vfs_node_t a_vfsn,
                                          char const *a_cname,
                                          natural_t a_cnamelen,
                                          mode_t a_mode,
                                          vfs_node_t *a_vfsn_out)
// ********************************************************************************************************************************
{
        struct netbsdfs_vfsn *nvfsn = vfsn_to_nvfsn(a_vfsn);

        struct netbsdfs_vattr va = s_vanoval;

        switch (a_mode & S_IFMT)
        {
                case S_IFREG:
                        va.va_type = NETBSDFS_VREG;
                        break;
                case S_IFSOCK:
                        va.va_type = NETBSDFS_VSOCK;
                        break;
                default:
                        return KERN_FAIL(EINVAL);
                        break;
        }

        va.va_mode = a_mode & ~(S_ISUID | S_ISGID | S_IFMT);

        void *netbsd_vnode = nullptr;

        int netbsd_retval = netbsd_vnode_create(nvfsn->m_netbsd_vndata, a_cname, a_cnamelen, &va, &netbsd_vnode);

        if (netbsd_retval == 0)
        {
                *a_vfsn_out = netbsdfs_vn_to_vfsn(netbsd_vnode);
        }

        return KERN_FAIL(netbsd_retval);
}

// ********************************************************************************************************************************
static kern_return_t netbsdfs_vfsn_mknod(vfs_node_t a_vfsn, char const *a_cname, natural_t a_cnamelen, mode_t a_mode, dev_t a_dev)
// ********************************************************************************************************************************
{
        struct netbsdfs_vfsn *nvfsn = vfsn_to_nvfsn(a_vfsn);
        struct netbsdfs_vattr va    = s_vanoval;

        switch (a_mode & S_IFMT)
        {
                case S_IFCHR:
                        va.va_type = NETBSDFS_VCHR;
                        break;

                case S_IFBLK:
                        va.va_type = NETBSDFS_VBLK;
                        break;

                case S_IFIFO:
                        va.va_type = NETBSDFS_VFIFO;
                        break;
                default:
                        panic("Unexpected S_IFMT");
                        break;
        }

        va.va_mode = a_mode & ~(S_ISUID | S_ISGID | S_IFMT);

        va.va_rdev = a_dev;

        void *netbsd_vnode = nullptr;

        int netbsd_retval = netbsd_vnode_create(nvfsn->m_netbsd_vndata, a_cname, a_cnamelen, &va, &netbsd_vnode);

        if (netbsd_retval == 0)
        {
                // We don't use the resulting vnode
                vfs_node_rel(netbsdfs_vn_to_vfsn(netbsd_vnode));
        }

        return KERN_FAIL(netbsd_retval);
}

// ********************************************************************************************************************************
static kern_return_t netbsdfs_vfsn_mkdir(vfs_node_t a_vfsn, char const *a_cname, natural_t a_cnamelen, mode_t a_mode)
// ********************************************************************************************************************************
{
        struct netbsdfs_vfsn *nvfsn = vfsn_to_nvfsn(a_vfsn);

        struct netbsdfs_vattr va = s_vanoval;

        va.va_type = NETBSDFS_VDIR;

        va.va_mode = a_mode & ~(S_ISUID | S_ISGID | S_IFMT);

        void *netbsd_vnode = nullptr;

        int netbsd_retval = netbsd_vnode_create(nvfsn->m_netbsd_vndata, a_cname, a_cnamelen, &va, &netbsd_vnode);

        if (netbsd_retval == 0)
        {
                // We don't use the resulting vnode
                vfs_node_rel(netbsdfs_vn_to_vfsn(netbsd_vnode));
        }

        return KERN_FAIL(netbsd_retval);
}

// ********************************************************************************************************************************
static kern_return_t netbsdfs_vfsn_symlink(vfs_node_t a_vfsn,
                                           char const *a_cname,
                                           natural_t a_cname_len,
                                           char const *a_tname,
                                           natural_t a_tname_len,
                                           mode_t a_mode)
// ********************************************************************************************************************************
{
        struct netbsdfs_vfsn *nvfsn = vfsn_to_nvfsn(a_vfsn);

        struct netbsdfs_vattr va = s_vanoval;

        va.va_type = NETBSDFS_VLNK;

        va.va_mode = a_mode & ~(S_ISUID | S_ISGID | S_IFMT);

        void *netbsd_vnode = nullptr;

        int netbsd_retval
                = netbsd_vnode_symlink(nvfsn->m_netbsd_vndata, a_cname, a_cname_len, a_tname, a_tname_len, &va, &netbsd_vnode);

        if (netbsd_retval == 0)
        {
                // We don't use the resulting vnode
                vfs_node_rel(netbsdfs_vn_to_vfsn(netbsd_vnode));
        }

        return KERN_FAIL(netbsd_retval);
}

// ********************************************************************************************************************************
static kern_return_t netbsdfs_vfsn_link(vfs_node_t a_vfsn, vfs_node_t a_target_vfsn, char const *a_tcname, natural_t a_tcname_len)
// ********************************************************************************************************************************
{
        struct netbsdfs_vfsn *nvfsn  = vfsn_to_nvfsn(a_vfsn);
        struct netbsdfs_vfsn *tnvfsn = vfsn_to_nvfsn(a_target_vfsn);

        int netbsd_retval = netbsd_vnode_link(nvfsn->m_netbsd_vndata, tnvfsn->m_netbsd_vndata, a_tcname, a_tcname_len);

        return KERN_FAIL(netbsd_retval);
}

// ********************************************************************************************************************************
static kern_return_t netbsdfs_vfsn_rmdir(vfs_node_t a_vfsn, vfs_node_t a_vfsn_dir, char const *a_dirnm, natural_t a_dirnm_len)
// ********************************************************************************************************************************
{
        struct netbsdfs_vfsn *nvfsn    = vfsn_to_nvfsn(a_vfsn);
        struct netbsdfs_vfsn *dirnvfsn = vfsn_to_nvfsn(a_vfsn_dir);

        // the NetBSD code does a vput on the vnode on success, we want to keep the delted vnode around and let our vfs release it
        // vfs_node_ref(a_vfsn_dir); // Not anymore

        int netbsd_retval = netbsd_vnode_rmdir(nvfsn->m_netbsd_vndata, dirnvfsn->m_netbsd_vndata, a_dirnm, a_dirnm_len);
        return KERN_FAIL(netbsd_retval);
}

// ********************************************************************************************************************************
static kern_return_t netbsdfs_vfsn_remove(vfs_node_t a_vfsn, vfs_node_t a_vfsn_file, char const *a_filenm, natural_t a_filenm_len)
// ********************************************************************************************************************************
{
        struct netbsdfs_vfsn *nvfsn     = vfsn_to_nvfsn(a_vfsn);
        struct netbsdfs_vfsn *filenvfsn = vfsn_to_nvfsn(a_vfsn_file);

        // the NetBSD code does a vput on the vnode on success, we want to keep the delted vnode around and let our vfs release it
        // vfs_node_ref(a_vfsn_file); // Not anymore

        int netbsd_retval = netbsd_vnode_remove(nvfsn->m_netbsd_vndata, filenvfsn->m_netbsd_vndata, a_filenm, a_filenm_len);

        return KERN_FAIL(netbsd_retval);
}

// ********************************************************************************************************************************
static kern_return_t netbsdfs_vfsn_rename(vfs_node_t a_src_prnt_vfsn,
                                          char const *a_src_nam,
                                          natural_t a_src_nam_len,
                                          vfs_node_t a_trgt_prnt_vfsn,
                                          char const *a_trgt_nam,
                                          natural_t a_trgt_nam_len)
// ********************************************************************************************************************************
{
        struct netbsdfs_vfsn *src_prnt_nvfsn  = vfsn_to_nvfsn(a_src_prnt_vfsn);
        struct netbsdfs_vfsn *trgt_prnt_nvfsn = vfsn_to_nvfsn(a_trgt_prnt_vfsn);

        int netbsd_retval = netbsd_vnode_rename(src_prnt_nvfsn->m_netbsd_vndata,
                                                a_src_nam,
                                                a_src_nam_len,
                                                trgt_prnt_nvfsn->m_netbsd_vndata,
                                                a_trgt_nam,
                                                a_trgt_nam_len);

        return KERN_FAIL(netbsd_retval);
}

// ********************************************************************************************************************************
static kern_return_t netbsdfs_vfsn_readlink(vfs_node_t a_vfsn, void *a_kbuf, natural_t *a_kbuf_len)
// ********************************************************************************************************************************
{
        struct netbsdfs_vfsn *nvfsn = vfsn_to_nvfsn(a_vfsn);

        unsigned long buflen = *a_kbuf_len;

        int netbsd_retval = netbsd_vnode_readlink(nvfsn->m_netbsd_vndata, a_kbuf, &buflen);

        if (netbsd_retval == 0)
        {
                *a_kbuf_len = buflen;
        }

        return KERN_FAIL(netbsd_retval);
}

/*
 * Flags for ioflag.
 */
#define NETBSD_IO_UNIT   0x00010 /* do I/O as atomic unit */
#define NETBSD_IO_APPEND 0x00020 /* append write to end */

// ********************************************************************************************************************************
static kern_return_t netbsdfs_rdwr(vfs_node_t a_vfsn, integer_t a_oflags, memory_rdwr_t a_mrw)
// ********************************************************************************************************************************
{
        static_assert(sizeof(iovec) == sizeof(netbsdfs_iovec));

        struct netbsdfs_vfsn *nvfsn = vfsn_to_nvfsn(a_vfsn);

        netbsdfs_uio nuio;

        nuio.uio_iov     = (struct netbsdfs_iovec *)a_mrw->uio_iov;
        nuio.uio_iovcnt  = (int32_t)a_mrw->uio_iovcnt;
        nuio.uio_offset  = a_mrw->uio_offset;
        nuio.uio_resid   = a_mrw->uio_resid;
        nuio.uio_rw      = a_mrw->uio_rw == UIO_READ ? NETBSDFS_UIO_READ : NETBSDFS_UIO_WRITE;
        nuio.uio_vmspace = a_mrw;

        int netbsd_error = 0;

        if (a_mrw->uio_rw == UIO_READ)
        {
                netbsd_error = netbsd_vnode_read(nvfsn->m_netbsd_vndata, &nuio, 0);
        }
        else
        {
                netbsd_error = netbsd_vnode_write(nvfsn->m_netbsd_vndata, &nuio, a_mrw->aux.s & O_APPEND ? NETBSD_IO_APPEND : 0);
        }

        return KERN_FAIL(netbsd_error);
}

// ********************************************************************************************************************************
static kern_return_t netbsdfs_stat(vfs_node_t a_vfsn, vfs_node_stat_t a_vfsn_stat)
// ********************************************************************************************************************************
{
        struct netbsdfs_vfsn *nvfsn = vfsn_to_nvfsn(a_vfsn);

        struct netbsdfs_vattr va = {};
        int netbsd_retval        = netbsd_vnode_getattr(nvfsn->m_netbsd_vndata, &va);

        if (netbsd_retval != 0)
        {
                return KERN_FAIL(netbsd_retval);
        }

        unsigned dev_major, dev_minor;
        netbsd_vnode_get_rdev(nvfsn->m_netbsd_vndata, &dev_major, &dev_minor);

        a_vfsn_stat->st_dev     = /*va.va_rdev*/ makedev(dev_major, dev_minor) /*xxx*/;
        a_vfsn_stat->st_ino     = va.va_fileid;
        a_vfsn_stat->st_nlink   = va.va_nlink;
        a_vfsn_stat->st_mode    = netbsd_vttoif(va.va_type) | va.va_mode;
        a_vfsn_stat->st_uid     = va.va_uid;
        a_vfsn_stat->st_gid     = va.va_gid;
        a_vfsn_stat->st_rdev    = va.va_rdev;
        a_vfsn_stat->st_size    = va.va_size;
        a_vfsn_stat->st_blksize = va.va_blocksize;
        a_vfsn_stat->st_blocks  = va.va_bytes >> DEV_BSHIFT;
        a_vfsn_stat->st_atim    = { .tv_sec = va.va_atime.tv_sec, .tv_nsec = va.va_atime.tv_nsec };
        a_vfsn_stat->st_mtim    = { .tv_sec = va.va_mtime.tv_sec, .tv_nsec = va.va_mtime.tv_nsec };
        a_vfsn_stat->st_ctim    = { .tv_sec = va.va_ctime.tv_sec, .tv_nsec = va.va_ctime.tv_nsec };

        return KERN_FAIL(netbsd_retval);
}

// ********************************************************************************************************************************
static kern_return_t netbsdfs_utimens(vfs_node_t a_vfsn, struct timespec *a_timens)
// ********************************************************************************************************************************
{
        struct netbsdfs_vfsn *nvfsn = vfsn_to_nvfsn(a_vfsn);

        struct netbsdfs_vattr va = s_vanoval;

        va.va_atime = { .tv_sec = a_timens[0].tv_sec, .tv_nsec = a_timens[0].tv_nsec };
        va.va_mtime = { .tv_sec = a_timens[1].tv_sec, .tv_nsec = a_timens[1].tv_nsec };

        int netbsd_retval = netbsd_vnode_setattr(nvfsn->m_netbsd_vndata, &va);

        return KERN_FAIL(netbsd_retval);
}

// ********************************************************************************************************************************
static kern_return_t netbsdfs_chmod(vfs_node_t a_vfsn, mode_t a_mode)
// ********************************************************************************************************************************
{
        struct netbsdfs_vfsn *nvfsn = vfsn_to_nvfsn(a_vfsn);

        struct netbsdfs_vattr va = s_vanoval;

        if (a_mode & S_ISUID)
        {
                va.va_uid = 0;
        }

        if (a_mode & S_ISGID)
        {
                va.va_gid = 0;
        }

        va.va_mode = a_mode & ~(S_ISUID | S_ISGID);

        int netbsd_retval = netbsd_vnode_setattr(nvfsn->m_netbsd_vndata, &va);

        return KERN_FAIL(netbsd_retval);
}

// ********************************************************************************************************************************
static kern_return_t netbsdfs_truncate(vfs_node_t a_vfsn, off_t a_length)
// ********************************************************************************************************************************
{
        struct netbsdfs_vfsn *nvfsn = vfsn_to_nvfsn(a_vfsn);

        struct netbsdfs_vattr va = s_vanoval;

        va.va_size = a_length;

        int netbsd_retval = netbsd_vnode_setattr(nvfsn->m_netbsd_vndata, &va);

        return KERN_FAIL(netbsd_retval);
}

// ********************************************************************************************************************************
static kern_return_t netbsdfs_fsync(vfs_node_t a_vfsn)
// ********************************************************************************************************************************
{
        struct netbsdfs_vfsn *nvfsn = vfsn_to_nvfsn(a_vfsn);

        int netbsd_retval = netbsd_vnode_fsync(nvfsn->m_netbsd_vndata);

        if (netbsd_retval != 0)
        {
                return netbsd_retval;
        }

        // Now flush all the metadata
        unsigned dev_major, dev_minor;
        netbsd_vnode_get_rdev(nvfsn->m_netbsd_vndata, &dev_major, &dev_minor);

        netbsdfs_spec_flush_buffers(dev_major, dev_minor);

        return KERN_FAIL(netbsd_retval);
}

static struct vfs_node_operations_data netbsdfs_vfsn_ops = {

        .lookup   = netbsdfs_vfsn_lookup,
        .create   = netbsdfs_vfsn_create,
        .mknod    = netbsdfs_vfsn_mknod,
        .mkdir    = netbsdfs_vfsn_mkdir,
        .symlink  = netbsdfs_vfsn_symlink,
        .link     = netbsdfs_vfsn_link,
        .rmdir    = netbsdfs_vfsn_rmdir,
        .remove   = netbsdfs_vfsn_remove,
        .rename   = netbsdfs_vfsn_rename,
        .readlink = netbsdfs_vfsn_readlink,
        .rdwr     = netbsdfs_rdwr,
        .stat     = netbsdfs_stat,
        .utimens  = netbsdfs_utimens,
        .chmod    = netbsdfs_chmod,
        .truncate = netbsdfs_truncate,
        .fsync    = netbsdfs_fsync
};

// ********************************************************************************************************************************
void netbsdfs_creation_lock_acquire()
// ********************************************************************************************************************************
{
        lock_write(&s_vnode_creation_lock);
}

// ********************************************************************************************************************************
void netbsdfs_creation_lock_release()
// ********************************************************************************************************************************
{
        lock_write_done(&s_vnode_creation_lock);
}

// ********************************************************************************************************************************
static struct netbsdfs_vfsn *nvfsn_find(void *a_netbsd_mount,
                                        unsigned long a_netbsd_mount_size,
                                        void const *a_key,
                                        unsigned long a_keylen)
// ********************************************************************************************************************************
{
        struct netbsdfs_vfsn *nvfsn = nullptr;

        natural_t hash = etl::hash::djb2::hash_buffer(a_netbsd_mount, a_netbsd_mount_size);
        hash           = etl::hash::djb2::hash_buffer(a_key, a_keylen, hash);

        vnode_cache_bucket_t bucket = &s_vnode_cache_buckets[hash & (ARRAY_SIZE(s_vnode_cache_buckets) - 1)];

        for (queue_entry_t qentry = queue_first(bucket); queue_end(bucket, qentry) == false; qentry = queue_next(qentry))
        {
                vnode_cache_entry_t entry = queue_containing_record(qentry, struct vnode_cache_entry_data, m_link);

                if (entry->m_netbsd_mount != a_netbsd_mount)
                {
                        continue;
                }

                if (entry->m_keylen != a_keylen)
                {
                        continue;
                }

                if (memcmp(entry->m_key, a_key, a_keylen) != 0)
                {
                        continue;
                }

                nvfsn = queue_containing_record(entry, struct netbsdfs_vfsn, m_cache_entry);

                break;
        }

        return nvfsn;
}

// ********************************************************************************************************************************
int netbsdfs_vfs_cache_vnode_get(void *a_netbsd_mount,
                                 unsigned long a_netbsd_mount_size,
                                 void const *a_key,
                                 unsigned long a_keylen,
                                 void **a_netbsd_vnode_out)
// ********************************************************************************************************************************
{
        KASSERT(lock_try_write(&s_vnode_creation_lock) == false);

        struct netbsdfs_vfsn *nvfsn = nvfsn_find(a_netbsd_mount, a_netbsd_mount_size, a_key, a_keylen);

        if (nvfsn != nullptr)
        {
                *a_netbsd_vnode_out = nvfsn->m_netbsd_vndata;
                if (nvfsn->m_vfsn.m_vmo.m_usecnt == 0)
                {
                        // Is inactive, remove it from the inactive queue
                        remqueue(&nvfsn->m_inactive_link);
                        nvfsn->m_inactive_link = {};

                        s_inactive_cnt--;

                        KASSERT(nvfsn->m_fifo_alias == nullptr);
#if nastydebug
                        for (vm_page_t vmp = vm_page_map_first(&nvfsn->m_vmp_map); vmp != nullptr; vmp = vm_page_map_next(vmp))
                        {
                                {
                                        KASSERT(vmp->m_io_descr->m_write == false);

                                        natural_t oldhash
                                                = etl::hash::djb2::hash_buffer((void *)phystokv(vmp->phys_addr), PAGE_SIZE);

                                        spl_t s = splbio();
                                        defer(splx(s));

                                        bdevsw[13].d_strategy3(makedev(13, 2), vmp);

                                        vm_page_wait(vmp);

                                        natural_t newhash
                                                = etl::hash::djb2::hash_buffer((void *)phystokv(vmp->phys_addr), PAGE_SIZE);

                                        if (oldhash != newhash)
                                        {
                                                log_error("got him");
                                        }
                                }
                        }
#endif
                }
                vfs_node_ref(&nvfsn->m_vfsn);
        }

        return 0;
}

// ********************************************************************************************************************************
// Check if the new key exists. if not remove the old entry
int netbsdfs_vfs_cache_rekey_part1(void *a_netbsd_mount,
                                   unsigned long a_netbsd_mount_size,
                                   void *a_netbsdfs_vnode,
                                   unsigned long a_netbsdfs_vnode_size,
                                   void const *a_old_key,
                                   unsigned long a_old_key_len,
                                   void const *a_new_key,
                                   unsigned long a_new_key_len)
// ********************************************************************************************************************************
{
        vfs_node_t vfsn = netbsdfs_vn_to_vfsn(a_netbsdfs_vnode);

        struct netbsdfs_vfsn *nvfsn = vfsn_to_nvfsn(vfsn);

        struct netbsdfs_vfsn *tmpnvfsn = nvfsn_find(a_netbsd_mount, a_netbsd_mount_size, a_new_key, a_new_key_len);

        if (tmpnvfsn != nullptr)
        {
                return KERN_FAIL(EEXIST);
        }

        tmpnvfsn = nvfsn_find(a_netbsd_mount, a_netbsd_mount_size, a_old_key, a_old_key_len);

        KASSERT(tmpnvfsn == nvfsn);

        // Remove it from the hash table
        remqueue(&nvfsn->m_cache_entry.m_link);

        nvfsn->m_cache_entry = {};

        return 0;
}

// ********************************************************************************************************************************
// Enter the vnode with the new key
int netbsdfs_vfs_cache_rekey_part2(void *a_netbsd_mount,
                                   unsigned long a_netbsd_mount_size,
                                   void *a_netbsdfs_vnode,
                                   unsigned long a_netbsdfs_vnode_size,
                                   void const *,
                                   unsigned long,
                                   void const *a_new_key,
                                   unsigned long a_new_key_len)
// ********************************************************************************************************************************
{
        vfs_node_t vfsn = netbsdfs_vn_to_vfsn(a_netbsdfs_vnode);

        struct netbsdfs_vfsn *nvfsn = vfsn_to_nvfsn(vfsn);

        nvfsn->m_cache_entry.m_netbsd_mount = a_netbsd_mount;
        nvfsn->m_cache_entry.m_key          = a_new_key;
        nvfsn->m_cache_entry.m_keylen       = a_new_key_len;

        natural_t hash = etl::hash::djb2::hash_buffer(a_netbsd_mount, a_netbsd_mount_size);
        hash           = etl::hash::djb2::hash_buffer(a_new_key, a_new_key_len, hash);

        vnode_cache_bucket_t bucket = &s_vnode_cache_buckets[hash & (ARRAY_SIZE(s_vnode_cache_buckets) - 1)];
        enqueue_head(bucket, &nvfsn->m_cache_entry.m_link);

        return 0;
}

ZONE_DEFINE(netbsdfs_vfsn, s_vfsn_zone, NETBSD_VNODES_COUNT, 10, ZONE_EXHAUSTIBLE, NETBSD_VNODES_COUNT / 2);

// ********************************************************************************************************************************
int netbsdfs_vfs_vnode_alloc(void *a_netbsd_mount, unsigned long a_netbsd_vnode_size, void **a_netbsdfs_vnode_out)
// ********************************************************************************************************************************
{
        KASSERT(a_netbsd_vnode_size == 0x100);

        KASSERT(lock_try_write(&s_vnode_creation_lock) == false);

        while (s_vfsn_zone.is_exhausted())
        {
                if (queue_empty(&s_inactive_vnodes) == true)
                {
                        panic("sleep on it, and retry");
                }

                // Remove a vnode from the inactive list
                struct netbsdfs_vfsn *nvfsn
                        = queue_containing_record(dequeue_tail(&s_inactive_vnodes), struct netbsdfs_vfsn, m_inactive_link);

                s_inactive_cnt--;

                // Remove it from the hash table
                remqueue(&nvfsn->m_cache_entry.m_link);

                // Let the netbsd part do its part
                netbsd_vnode_reclaim(nvfsn->m_netbsd_vndata, false);

                // The netbsd part has called netbsdfs_vfs_vnode_free and we should be able to break out of this loop now
        }

        struct netbsdfs_vfsn *nvfsn = s_vfsn_zone.alloc();

        *a_netbsdfs_vnode_out = nvfsn->m_netbsd_vndata;

        vm_page_map_init(&nvfsn->m_vmp_map);

        vfs_node_init(&nvfsn->m_vfsn);

        struct netbsdfs_mount *nbsdm = NETBSDMNT_TO_NETBSDFS_MOUNT(a_netbsd_mount);

        nvfsn->m_fifo_alias = nullptr;

        nvfsn->m_vfsn.m_mount = &nbsdm->m_vfsm;

        enqueue_tail(&nbsdm->m_vfs_nodes, &nvfsn->m_mount_entry);

        return KERN_SUCCESS;
}

static int nvfsnodes = 0;

// ********************************************************************************************************************************
void netbsdfs_vfs_vnode_free(void *a_netbsdfs_vnode, int a_is_deleted)
// ********************************************************************************************************************************
{
        KASSERT(lock_try_write(&s_vnode_creation_lock) == false);

        nvfsnodes--;

        struct netbsdfs_vfsn *nvfsn = vfsn_to_nvfsn(netbsdfs_vn_to_vfsn(a_netbsdfs_vnode));

        KASSERT(nvfsn->m_vfsn.m_ops == &netbsdfs_vfsn_ops);

        if (!!a_is_deleted == false)
        {
                nvfsn_flush_buffers(nvfsn);
        }

        remqueue(&nvfsn->m_mount_entry);

        vm_page_map_clear(&nvfsn->m_vmp_map);

        s_vfsn_zone.free(nvfsn);
}

// ********************************************************************************************************************************
void netbsdfs_vfs_vnode_init(void *a_netbsdfs_vnode,
                             struct rumpuser_rw **a_ppvmobjlock,
                             void const *a_key,
                             unsigned long a_keylen,
                             void *a_netbsd_mount,
                             unsigned long a_netbsd_mount_size,
                             struct netbsdfs_vattr *va)
// ********************************************************************************************************************************
{
        KASSERT(lock_try_write(&s_vnode_creation_lock) == false);

        nvfsnodes++;

        vfs_node_t vfsn = netbsdfs_vn_to_vfsn(a_netbsdfs_vnode);

        struct netbsdfs_vfsn *nvfsn = vfsn_to_nvfsn(vfsn);

        { // Cache init

                nvfsn->m_cache_entry.m_netbsd_mount = a_netbsd_mount;
                nvfsn->m_cache_entry.m_key          = a_key;
                nvfsn->m_cache_entry.m_keylen       = a_keylen;

                natural_t hash = etl::hash::djb2::hash_buffer(a_netbsd_mount, a_netbsd_mount_size);
                hash           = etl::hash::djb2::hash_buffer(a_key, a_keylen, hash);

                vnode_cache_bucket_t bucket = &s_vnode_cache_buckets[hash & (ARRAY_SIZE(s_vnode_cache_buckets) - 1)];
                enqueue_head(bucket, &nvfsn->m_cache_entry.m_link);
        }

        vfsn->m_ops       = &netbsdfs_vfsn_ops;
        vfsn->m_vmo.m_ops = &netbsdfs_vm_obj_oops;
        vfsn->m_mode      = netbsd_vttoif(va->va_type) | va->va_mode;
        vfsn->m_rdev      = va->va_rdev;

        nvfsn->m_uvmobj_rw = { .self = &nvfsn->m_uvmobj_rw, .v = 0 };
        *a_ppvmobjlock     = (nvfsn->m_uvmobj_rw.self);

        switch (vfsn->m_mode & S_IFMT)
        {
                case S_IFCHR:
                        chrdev_init_vfs_node(vfsn);
                        break;
                case S_IFDIR:
                        vfsn->m_fops = &netbsdfs_dir_ops;
                        break;
                case S_IFREG:
                        vfsn->m_fops = &netbsdfs_reg_ops;
                        break;
                case S_IFSOCK:
                        vfsn->m_fops = &netbsdfs_sock_fops;
                        break;
                case S_IFIFO:
                        vfsn->m_fops = &netbsdfs_fifo_fops;
                        break;
                case S_IFLNK:
                        break;
                case S_IFBLK:
                        panic("A block device");
                default:
                        panic("Unknown vfs_node mode");
        }
}

// ********************************************************************************************************************************
void netbsdfs_vfs_vnode_setsize(void *a_netbsdfs_vnode, unsigned long a_size)
// ********************************************************************************************************************************
{
        vfs_node_t vfsn             = netbsdfs_vn_to_vfsn(a_netbsdfs_vnode);
        struct netbsdfs_vfsn *nvfsn = vfsn_to_nvfsn(vfsn);

        if (vm_offset_t new_size = round_page(a_size);
            new_size < round_page(vfsn->m_vmo.m_size) && vm_page_map_empty(&nvfsn->m_vmp_map) == false)
        {
                // panic("Untested, can we just free? do we need to check for wired down and unmap all mappings");
                //  Remove unused pages, maybe also remove mappings?

#if 0
                for (vm_page_t vmp = vm_page_map_lookup(&nvfsn->m_vmp_map, new_size); vmp != nullptr; vmp = vm_page_map_next(vmp))
                {
                        vm_page_free(&nvfsn->m_vmp_map, vmp);
                }
#endif
        }

        kern_return_t retval = vm_object_set_size(&vfsn->m_vmo, a_size);
        KASSERT(KERN_STATUS_SUCCESS(retval));
}

// ********************************************************************************************************************************
int netbsdfs_vfs_vnode_ref(void *a_netbsdfs_vnode)
// ********************************************************************************************************************************
{
        vfs_node_t vfsn = netbsdfs_vn_to_vfsn(a_netbsdfs_vnode);
        vfs_node_ref(vfsn);
        return 0;
}

// ********************************************************************************************************************************
int netbsdfs_vfs_vnode_rel(void *a_netbsdfs_vnode)
// ********************************************************************************************************************************
{
        vfs_node_t vfsn = netbsdfs_vn_to_vfsn(a_netbsdfs_vnode);
        vfs_node_rel(vfsn);
        return 0;
}

extern struct bdevsw bdevsw[];

// ********************************************************************************************************************************
static int netbsdfs_vfs_vnode_ubc_uiomove_nomrw(void *a_netbsdfs_vnode, struct netbsdfs_uio *a_uio, unsigned long a_len)
// ********************************************************************************************************************************
{
        memory_rdwr_data mrw_data = {};
        struct iovec iov[5]; // It's never more then 1
        KASSERT(a_uio->uio_iovcnt <= (int)ARRAY_SIZE(iov));

        for (int32_t i = 0; i < a_uio->uio_iovcnt; i++)
        {
                iov[i].iov_base = a_uio->uio_iov[i].iov_base;
                iov[i].iov_len  = a_uio->uio_iov[i].iov_len;
        }

        mrw_data.uio_iov    = iov;
        mrw_data.uio_iovcnt = a_uio->uio_iovcnt;
        mrw_data.uio_offset = a_uio->uio_offset;
        mrw_data.uio_resid  = a_uio->uio_resid;
        mrw_data.uio_rw     = a_uio->uio_rw == NETBSDFS_UIO_READ ? UIO_READ : UIO_WRITE;
        mrw_data.uio_segflg = UIO_SYSSPACE;

        a_uio->uio_vmspace = &mrw_data;
        int error          = netbsdfs_vfs_vnode_ubc_uiomove(a_netbsdfs_vnode, a_uio, a_len);
        a_uio->uio_vmspace = nullptr;
        return error;
}

// ********************************************************************************************************************************
int netbsdfs_vfs_vnode_ubc_uiomove(void *a_netbsdfs_vnode, struct netbsdfs_uio *a_uio, unsigned long a_len)
// ********************************************************************************************************************************
{
        vfs_node_t vfsn = netbsdfs_vn_to_vfsn(a_netbsdfs_vnode);
        // struct netbsdfs_vfsn *nvfsn = vfsn_to_nvfsn(vfsn);
        memory_rdwr_t mrw = (memory_rdwr_t)a_uio->uio_vmspace;

        if (mrw == nullptr)
        {
                return netbsdfs_vfs_vnode_ubc_uiomove_nomrw(a_netbsdfs_vnode, a_uio, a_len);
        }

        mrw->uio_offset = a_uio->uio_offset; // If IO_APPEND is set the offset is modified by the fs

        int netbsd_error = 0;

        while ((a_len > 0) && (mrw->uio_resid > 0))
        {
                natural_t object_offset = mrw->uio_offset & ~PAGE_MASK;
                natural_t page_offset   = mrw->uio_offset & PAGE_MASK;
                natural_t xfersize      = etl::min(etl::min(a_len, ((natural_t)PAGE_SIZE)), ((natural_t)PAGE_SIZE) - page_offset);

                KASSERT(xfersize > 0);

                vm_page_t vmp = nullptr;
                netbsd_error  = -vm_object_get_page(&vfsn->m_vmo, object_offset, VM_PROT_NONE, &vmp);

                KASSERT(netbsd_error == 0);

                void *bufaddr = (void *)(phystokv(vmp->phys_addr) + page_offset);

                netbsd_error = -uiomove(bufaddr, xfersize, mrw);

                KASSERT(netbsd_error == 0);

                if (((vmp->flags & PG_DIRTY) == 0) && (mrw->uio_rw == UIO_WRITE))
                {
                        vmp->flags |= PG_DIRTY;
                }

                // Keep the netbsd struct up to date.
                a_uio->uio_offset = mrw->uio_offset;
                a_uio->uio_resid  = mrw->uio_resid;
                a_len -= xfersize;
        }

        return netbsd_error;
}

// ********************************************************************************************************************************
void netbsdfs_vfs_vnode_zero_range(void *a_netbsdfs_vnode, long a_off, unsigned len)
// ********************************************************************************************************************************
{
        vfs_node_t vfsn             = netbsdfs_vn_to_vfsn(a_netbsdfs_vnode);
        struct netbsdfs_vfsn *nvfsn = vfsn_to_nvfsn(vfsn);

        natural_t object_offset = (natural_t)trunc_page(a_off);

        // TODO: Lock the block device object
        vm_page_t vmp = vm_page_map_lookup(&nvfsn->m_vmp_map, object_offset);

        if (vmp != nullptr)
        {
                natural_t page_offset = (((natural_t)(a_off)) & (PAGE_SIZE - 1));
                KASSERT((page_offset + len) <= PAGE_SIZE);

                vm_address_t pageaddr = pmap_get_dmap_addr() + vmp->phys_addr + page_offset;
                memset((char *)pageaddr, 0, len);
        }
}

// ********************************************************************************************************************************
int netbsdfs_block_read(int a_major, int a_minor, unsigned long a_blkno, unsigned long a_size, void **addr_out)
// ********************************************************************************************************************************
{
        dev_t dev                    = makedev(a_major, a_minor);
        natural_t dev_logical_offset = (a_blkno << DEV_BSHIFT);
        natural_t dev_object_offset  = dev_logical_offset & ~PAGE_MASK;
        natural_t dev_page_offset    = dev_logical_offset & PAGE_MASK;

        // TODO: Lock the block device object
        lock_write(&block_pg_map_lock[a_minor]);

        vm_page_t vmp = vm_page_map_lookup(&block_pg_map[a_minor], dev_object_offset);

        if (vmp == nullptr)
        {
                vmp = vm_page_alloc(&block_pg_map[a_minor], dev_object_offset);

                vm_page_io_descr_t vmpiod = vmp->m_io_descr;

                KASSERT(vmp->m_io_descr->m_write == false);

                natural_t npage_sectors = (PAGE_SIZE >> DEV_BSHIFT);

                // Now split up the IO descriptor so the page is described in 512 byte blocks. (AKA bmap for a block device)
                for (natural_t page_sector = 0; page_sector < npage_sectors; ++page_sector)
                {
                        vmpiod->m_lba[page_sector]
                                = ((natural_t)-1); // (dev_object_offset + (page_sector << DEV_BSHIFT)) >> DEV_BSHIFT;
                }
        }

        natural_t startsect = dev_page_offset >> DEV_BSHIFT;

        if (vmp->m_io_descr->m_lba[startsect] == ((natural_t)-1))
        {
                // When FS blocksize is smaller then pagesize, we cant read whole pages.
                // If we were to do that, blocks belonging to inodes would be pulled in here along with the filesystem metadata.
                // Then, when a sync/writeback is performed the inode blocks we have here would overwrite the valid ones held by the
                // actual vnode inode. So we do some tricks here to make sure we only read the requested data and nothing elese

                // Another problem is we cant read all the blocks described in the io descriptor.
                // because that would overwrite the existing ones(that may have been modified but not written back).

                struct vm_page_io_descr tmpdescr_data = {};

                vm_page_io_descr_t tmpdescr = &tmpdescr_data;

                for (natural_t &lba : tmpdescr->m_lba)
                {
                        lba = ((natural_t)-1);
                }

                natural_t endsect = startsect + (a_size >> DEV_BSHIFT);
                for (natural_t cursect = startsect; cursect < endsect; ++cursect)
                {
                        tmpdescr->m_lba[cursect] = vmp->m_io_descr->m_lba[cursect]
                                = (dev_object_offset + (cursect << DEV_BSHIFT)) >> DEV_BSHIFT;
                }

                etl::swap(vmp->m_io_descr, tmpdescr);

                bdevsw[a_major].d_strategy3(dev, vmp);

                etl::swap(vmp->m_io_descr, tmpdescr);
        }

        lock_write_done(&block_pg_map_lock[a_minor]);

        *addr_out = (void *)(phystokv(vmp->phys_addr) + dev_page_offset);

        return 0;
}

// ********************************************************************************************************************************
int netbsdfs_spec_invalidate_block(int a_major, int a_minor, unsigned long a_blkno, unsigned long a_blocksize)
// ********************************************************************************************************************************
{
        natural_t dev_logical_offset = (a_blkno << DEV_BSHIFT);
        natural_t dev_object_offset  = dev_logical_offset & ~PAGE_MASK;
        natural_t dev_page_offset    = dev_logical_offset & PAGE_MASK;

        // TODO: Lock the block device object
        lock_write(&block_pg_map_lock[a_minor]);

        vm_page_t vmp = vm_page_map_lookup(&block_pg_map[a_minor], dev_object_offset);

        KASSERT(vmp != nullptr);

        natural_t startsect = dev_page_offset >> DEV_BSHIFT;

        natural_t endsect = startsect + (a_blocksize >> DEV_BSHIFT);
        for (natural_t cursect = startsect; cursect < endsect; ++cursect)
        {
                vmp->m_io_descr->m_lba[cursect] = ((natural_t)-1);
        }

        lock_write_done(&block_pg_map_lock[a_minor]);
        return 0;
}

// ********************************************************************************************************************************
void netbsdfs_get_timespec(struct netbsdfs_timespec *a_ts)
// ********************************************************************************************************************************
{
        struct timespec ts = {};
        mach_get_timespec(&ts);
        a_ts->tv_nsec = ts.tv_nsec;
        a_ts->tv_sec  = ts.tv_sec;
}

// ********************************************************************************************************************************
static kern_return_t netbsdfs_mount_ext2fs(dev_t a_device, void *a_fs_data, vfs_mount_t *a_mount_out)
// ********************************************************************************************************************************
{
        panic("No impl");
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t netbsdfs_mount_msdosfs(dev_t a_device, void *a_fs_data, vfs_mount_t *a_mount_out)
// ********************************************************************************************************************************
{
        void *netbsd_mountpoint = nullptr;

        int netbsd_error = netbsd_mount_msdosfs(major(a_device), minor(a_device), &netbsd_mountpoint);

        if (netbsd_error)
        {
                return KERN_FAIL(netbsd_error);
        }

        struct netbsdfs_mount *nbsdm = NETBSDMNT_TO_NETBSDFS_MOUNT(netbsd_mountpoint);

        *a_mount_out = &nbsdm->m_vfsm;

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t something_fs()
// ********************************************************************************************************************************
{
        dev_t dev = makedev(13, 0);

        struct ioctl_req_data ioctl_ctx;

        struct disklabel di;

        ioctl_req_create(DIOCGDINFO, &di, false, &ioctl_ctx);

        int retval = bdevsw[major(dev)].d_ioctl(dev, &ioctl_ctx, 0);

        KASSERT(KERN_STATUS_SUCCESS(retval));

        di.part[0].p_size = di.d_nsectors;

        void *mbr;
        retval = netbsdfs_block_read(major(dev), minor(dev), 0, 512, &mbr);

        KASSERT(KERN_STATUS_SUCCESS(retval));

        retval = mbr_partition_scan(mbr, di.d_nsectors * di.d_secsize, &di.part[1], ARRAY_SIZE(di.part) - 1);

        KASSERT(KERN_STATUS_SUCCESS(retval));

        di.d_npartitions = retval + 1;

        ioctl_req_create(DIOCSDINFO, &di, false, &ioctl_ctx);
        bdevsw[major(dev)].d_ioctl(dev, &ioctl_ctx, 0);

        netbsd_mountroot();

        return 0;
}
fs_initcall(something_fs);

// ********************************************************************************************************************************
static kern_return_t netbsdfs_init()
// ********************************************************************************************************************************
{
        vm_address_t saddr, eaddr;

        kmem_meta_arena = kmem_suballoc(kernel_map, &saddr, &eaddr, 100 * PAGE_SIZE, true);

        if (kmem_meta_arena == nullptr)
        {
                return KERN_FAILURE;
        }

        kmem_va_arena = kmem_suballoc(kernel_map, &saddr, &eaddr, 100 * PAGE_SIZE, true);

        if (kmem_va_arena == nullptr)
        {
                return KERN_FAILURE;
        }

        for (vm_page_map_data &vmpm : block_pg_map)
        {
                vm_page_map_init(&vmpm);
        }

        for (lock_data &lock : block_pg_map_lock)
        {
                lock_init(&lock, true);
        }

        for (queue_head_t &bucket : s_vnode_cache_buckets)
        {
                queue_init(&bucket);
        }

        queue_init(&s_inactive_vnodes);

        lock_init(&s_vnode_creation_lock, true);

        netbsd_subsys_init();

        vfs_file_system_register("ext2", netbsdfs_mount_ext2fs);

        vfs_file_system_register("msdos", netbsdfs_mount_msdosfs);

        return KERN_SUCCESS;
}
subsys_initcall(netbsdfs_init);
