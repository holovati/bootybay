#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/statfs.h>

#include <aux/init.h>

#include <kernel/zalloc.h>
#include <kernel/mach_clock.h>

#include <emerixx/vfs/vfs_file.h>
#include <emerixx/vfs/vfs_node.h>

#include <emerixx/vfs/vfs_file_data.h>
#include <emerixx/vfs/vfs_node_data.h>
#include <emerixx/vfs/vfs_mount_data.h>
#include <emerixx/sleep.h>
#include <emerixx/ioctl_req.h>

#include <emerixx/pipe.h>
#include <emerixx/poll.h>
#include <emerixx/memory_rw.h>

#include <emerixx/fs/pipefs/pipefs.h>

/* Constants */
#define PIPEFS_SIZE  0x100000
#define PIPEFS_MAGIC 0x50495045

/* Forward declarations */

/* Types */
typedef struct pipe_vfsn_data *pipe_vfsn_t;
typedef struct pipe_vfsm_data *pipe_vfsm_t;

/* Helpers */
#define VFSN_TO_PVFSN(x) ((pipe_vfsn_t)(x))

static kern_return_t pipefs_vfsn_alloc(pipe_vfsn_t *a_pipe_vfsn_out);
static void pipefs_vfsn_free(pipe_vfsn_t a_pipe_vfsn);

/* Mount operations*/
static kern_return_t pipefs_mount_root(vfs_mount_t a_vfsm, vfs_node_t *a_vfsn_out);
static kern_return_t pipefs_mount_stat(vfs_mount_t a_vfsm, vfs_mount_stat_t a_stat);

/* VFS node operations*/
static kern_return_t pipefs_rdwr(vfs_node_t a_vfsn, integer_t a_oflags, memory_rdwr_t a_mrw);
static kern_return_t pipefs_stat(vfs_node_t a_vfsn, vfs_node_stat_t a_vfsn_stat);

/* File operations */
static kern_return_t pipefs_fopen(vfs_node_t, vfs_file_t);
static kern_return_t pipefs_fioctl(vfs_file_t, ioctl_req_t com);
static kern_return_t pipefs_fclose(vfs_file_t);
static kern_return_t pipefs_fpoll(vfs_file_t, struct pollfd **a_pfd);
static kern_return_t pipefs_fsplice(vfs_file_t,
                                    off_t *a_off,
                                    vfs_file_t a_file_out,
                                    off_t *a_off_out,
                                    size_t a_len,
                                    unsigned a_flags);

/* VM object operations*/
static kern_return_t pipefs_vmo_inactive(vm_object_t a_vm_obj);

/* Data types */
struct pipe_vfsm_data
{
        struct vfs_mount_data m_mount;
};

struct pipe_vfsn_data
{
        struct vfs_node_data m_vfsn;
        poll_device_context_t m_pollfds;
        pipe_t m_pipe;
        integer_t m_reader_cnt;
        integer_t m_writer_cnt;
        integer_t m_pipe_id;
        vfs_node_t m_aliased_by; // A vnode is aliasing this pipe, we need to call vfs_node_rel on it is this is not null.
};

/* Internal globals */
ZONE_DEFINE(pipe_vfsn_data, s_pipefs_vfsn_zone, FILEMAX, 0x10, ZONE_EXHAUSTIBLE, 0x10);

static struct lock_data s_global_lock;

static natural_t s_pipe_count;

static pipe_vfsm_data s_pipefs_mount;

static integer_t s_pipefs_last_id;

static struct vfs_mount_operations_data s_pipefs_vfsm_ops = {
        .root = pipefs_mount_root,     //
        .stat = pipefs_mount_stat,     //
        .sync = vfs_mount_op_sync_noop //
};

static struct vfs_node_operations_data s_pipefs_vfsn_ops = {
        .rdwr = pipefs_rdwr, //
        .stat = pipefs_stat  //
};

static struct vfs_file_operations_data s_pipefs_vfsf_ops = {
        .fopen     = pipefs_fopen,                 //
        .fread     = vfs_file_op_vfsn_rw,          //
        .fwrite    = vfs_file_op_vfsn_rw,          //
        .flseek    = vfs_file_op_lseek_espipe,     //
        .fioctl    = pipefs_fioctl,                //
        .fclose    = pipefs_fclose,                //
        .fgetdents = vfs_file_op_getdents_enotdir, //
        .fmmap     = vfs_file_op_mmap_enodev,      //
        .fpoll     = pipefs_fpoll,                 //
        .fsplice   = pipefs_fsplice,               //
};

static struct vm_object_operations_data s_pipefs_vmo_ops = {
        .get_page = vm_object_op_get_page_fail, //
        .inactive = pipefs_vmo_inactive,        //
        .collapse = vm_object_op_collapse_fail, //
        .set_size = vm_object_op_set_size_noop  //
};

// ********************************************************************************************************************************
kern_return_t pipefs_mount_root(vfs_mount_t a_vfsm, vfs_node_t *a_vfsn_out)
// ********************************************************************************************************************************
{
        // Finish implementing this later
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t pipefs_mount_stat(vfs_mount_t a_vfsm, vfs_mount_stat_t a_stat)
// ********************************************************************************************************************************
{
        a_stat->f_type    = PIPEFS_MAGIC;
        a_stat->f_bsize   = PAGE_SIZE;
        a_stat->f_namelen = _XOPEN_NAME_MAX;
        a_stat->f_frsize  = PAGE_SIZE;
        a_stat->f_type    = 0x20; // ST_VALID;
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t pipefs_rdwr(vfs_node_t a_vfsn, integer_t a_oflags, memory_rdwr_t a_mrw)
// ********************************************************************************************************************************
{
        pipe_vfsn_t pvfsn = VFSN_TO_PVFSN(a_vfsn);

        kern_return_t retval = KERN_SUCCESS;

        retval = pipe_rw(pvfsn->m_pipe, a_mrw, PIPE_BUF, 0, !(a_oflags & O_NONBLOCK), nullptr);

        poll_wakeup(&pvfsn->m_pollfds, a_mrw->uio_rw == UIO_READ ? POLLWRNORM | POLLOUT : POLLRDNORM | POLLIN);

        return retval;
}

// ********************************************************************************************************************************
kern_return_t pipefs_stat(vfs_node_t a_vfsn, vfs_node_stat_t a_vfsn_stat)
// ********************************************************************************************************************************
{
        pipe_vfsn_t pvfsn = VFSN_TO_PVFSN(a_vfsn);

        a_vfsn_stat->st_mode    = a_vfsn->m_mode;
        a_vfsn_stat->st_nlink   = pvfsn->m_reader_cnt + pvfsn->m_writer_cnt;
        a_vfsn_stat->st_ino     = pvfsn->m_pipe_id;
        a_vfsn_stat->st_blksize = PIPE_BUF;

        mach_get_timespec(&a_vfsn_stat->st_atim);

        a_vfsn_stat->st_mtim = a_vfsn_stat->st_ctim = a_vfsn_stat->st_atim;

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t pipefs_fopen(vfs_node_t a_vfsn, vfs_file_t a_vfsf)
// ********************************************************************************************************************************
{
        pipe_vfsn_t pvfsn = VFSN_TO_PVFSN(a_vfsn);

        integer_t oflags = a_vfsf->m_oflags & (O_RDONLY | O_WRONLY);

        if (oflags == O_RDONLY)
        {
                pvfsn->m_reader_cnt++;
                uthread_wakeup(&pvfsn->m_reader_cnt);
        }
        else if (oflags == O_WRONLY)
        {
                pvfsn->m_writer_cnt++;

                while (pvfsn->m_reader_cnt == 0)
                {
                        if (((a_vfsf->m_oflags) & O_NONBLOCK) || pipe_read_closed(pvfsn->m_pipe))
                        {
                                return KERN_FAIL(ENXIO);
                        }

                        kern_return_t retval
                                = uthread_sleep_timeout(&pvfsn->m_reader_cnt, 0, /*&pvfsn->m_vfsn.m_vmo.m_lock*/ nullptr);

                        if (KERN_STATUS_FAILURE(retval))
                        {
                                return retval;
                        }
                }
        }
        else /* O_RDWR */
        {
                return KERN_FAIL(EINVAL);
        }

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t pipefs_fioctl(vfs_file_t a_file, ioctl_req_t a_req)
// ********************************************************************************************************************************
{
        pipe_vfsn_t pvfsn = VFSN_TO_PVFSN(a_file->m_vfsn);

        integer_t ioctl_cmdno = ioctl_req_cmdno(a_req);

        kern_return_t retval;

        switch (ioctl_cmdno)
        {
                case FIONREAD:
                {
                        int nread = (int)pipe_read_available(pvfsn->m_pipe);

                        retval = ioctl_req_copyout(a_req, &nread);
                }
                break;

                default:
                {
                        retval = KERN_FAIL(ENOTTY);
                }
                break;
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t pipefs_fclose(vfs_file_t a_vfsf)
// ********************************************************************************************************************************
{
        pipe_vfsn_t pvfsn = VFSN_TO_PVFSN(a_vfsf->m_vfsn);

        integer_t oflags = a_vfsf->m_oflags & (O_RDONLY | O_WRONLY | O_RDWR);

        if (oflags == O_RDONLY)
        {
        close_reader:
                pvfsn->m_reader_cnt--;
                if (pvfsn->m_reader_cnt == 0)
                {
                        pipe_read_shutdown(pvfsn->m_pipe);
                        uthread_wakeup(&pvfsn->m_reader_cnt);
                }
        }
        else if (oflags & O_WRONLY)
        {
                pvfsn->m_writer_cnt--;
                if (pvfsn->m_writer_cnt == 0)
                {
                        pipe_write_shutdown(pvfsn->m_pipe);
                        poll_wakeup(&pvfsn->m_pollfds, POLLHUP);
                }
        }
        else /* O_RDWR */
        {
                KASSERT(pvfsn->m_writer_cnt == 0);
                goto close_reader;
                panic("O_RDWR in pipe close");
        }

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t pipefs_fpoll(vfs_file_t a_file, struct pollfd **a_pfd)
// ********************************************************************************************************************************
{
        pipe_vfsn_t pvfsn = VFSN_TO_PVFSN(a_file->m_vfsn);

        poll_event_t evt = (*a_pfd)->events;

        if (evt & (POLLIN | POLLRDNORM))
        {
                if (pipe_read_available(pvfsn->m_pipe))
                {
                        (*a_pfd)->revents |= (POLLIN | POLLRDNORM);
                }
        }

        /* NOTE: POLLHUP and POLLOUT/POLLWRNORM are mutually exclusive */
        if (pipe_read_closed(pvfsn->m_pipe) || pipe_write_closed(pvfsn->m_pipe)) //
        {
                (*a_pfd)->revents |= POLLHUP;
        }
        else if (evt & (POLLOUT | POLLWRNORM))
        {
                if (pipe_write_available(pvfsn->m_pipe))
                {
                        (*a_pfd)->revents |= (POLLOUT | POLLWRNORM);
                }
        }

        return poll_record(&pvfsn->m_pollfds, a_pfd);
}

// ********************************************************************************************************************************
kern_return_t pipefs_fsplice(vfs_file_t, off_t *a_off, vfs_file_t a_file_out, off_t *a_off_out, size_t a_len, unsigned a_flags)
// ********************************************************************************************************************************
{
#if 0
        pipe_vfs_node_t pvfsn = VFSF_TO_PVFSN(a_vfsf);

        KASSERT(a_off == nullptr);                                  // Pipes are not seekable, caller should know this
        KASSERT(a_off_out == nullptr);                              // Not supported yet;
        KASSERT((a_flags & ~(SPLICE_F_MOVE | SPLICE_F_MORE)) == 0); // Nothing else is supported yet
        KASSERT((a_vfsf->m_oflags & O_WRONLY) == 0);
        // KASSERT((this == &m_pipe->m_endpoints[PIPE_READ]));

        // Make sure a_file_out is not the same pipe as us
        if (a_vfsf == a_file_out)
        {
                return KERN_FAIL(EINVAL);
        }

        pipe_splice_callback_data splice_data = {};
        splice_data.kernel_buffer             = pvfsn->m_byte_buffer;
        splice_data.file_out                  = a_file_out;
        splice_data.out_len                   = a_len;

        LOCK_WRITE_SCOPE(&pvfsn->m_eplock[PIPE_READ]);
        pipe_vfs_node_lock(pvfsn);

        do
        {
                pvfsn->m_buf_offset[PIPE_READ] = rbuffer_read(pvfsn->m_buf_offset[PIPE_READ], //
                                                              pvfsn->m_buf_offset[PIPE_WRITE],
                                                              pvfsn->m_vfsn.m_vmo.m_size,
                                                              pipe_splice_callback,
                                                              &splice_data);

                if (pipe_vfs_node_writer_count(pvfsn) > 0)
                {
                        pipe_vfs_node_reader_wait(pvfsn);
                }
                else
                {
                        break;
                }
        }
        while (splice_data.out_len > 0);

        pipe_vfs_node_unlock(pvfsn);

        return (kern_return_t)(a_len - splice_data.out_len);
#endif

        panic("Not ready");

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t pipefs_vmo_inactive(vm_object_t a_vm_obj)
// ********************************************************************************************************************************
{
        pipe_vfsn_t pvfsn = VFSN_TO_PVFSN(a_vm_obj);

        if (pvfsn->m_aliased_by != nullptr)
        {
                vfs_node_rel(pvfsn->m_aliased_by);
                pvfsn->m_aliased_by = nullptr;
        }

        pipefs_vfsn_free(pvfsn);

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t pipefs_vfsn_alloc(pipe_vfsn_t *a_pipe_vfsn_out)
// ********************************************************************************************************************************
{
        kern_return_t retval = KERN_SUCCESS;

        lock_write(&s_global_lock);

        pipe_vfsn_t pipe_vfsn = s_pipefs_vfsn_zone.alloc();

        if (pipe_vfsn == nullptr)
        {
                retval = KERN_FAIL(ENOMEM);
                goto out;
        }

        vfs_node_init(&pipe_vfsn->m_vfsn);

        pipe_vfsn->m_vfsn.m_vmo.m_ops = &s_pipefs_vmo_ops;

        pipe_vfsn->m_vfsn.m_vmo.m_size = PIPEFS_SIZE;

        pipe_vfsn->m_vfsn.m_ops = &s_pipefs_vfsn_ops;

        pipe_vfsn->m_vfsn.m_fops = &s_pipefs_vfsf_ops;

        pipe_vfsn->m_vfsn.m_mount = &s_pipefs_mount.m_mount;

        pipe_vfsn->m_vfsn.m_mode = S_IFIFO | S_IRUSR | S_IWUSR;

        retval = pipe_create(PIPEFS_SIZE, &pipe_vfsn->m_pipe);

        if (KERN_STATUS_FAILURE(retval))
        {
                s_pipefs_vfsn_zone.free(pipe_vfsn);
                goto out;
        }

        pipe_vfsn->m_reader_cnt = 0;

        pipe_vfsn->m_writer_cnt = 0;

        pipe_vfsn->m_pipe_id = ++s_pipefs_last_id;

        poll_device_ctx_init(&pipe_vfsn->m_pollfds);

        pipe_vfsn->m_aliased_by = nullptr;

        *a_pipe_vfsn_out = pipe_vfsn;

        s_pipe_count++;
out:
        lock_done(&s_global_lock);
        return retval;
}

// ********************************************************************************************************************************
void pipefs_vfsn_free(pipe_vfsn_t a_pipe_vfsn)
// ********************************************************************************************************************************
{
        lock_write(&s_global_lock);

        pipe_rel(a_pipe_vfsn->m_pipe);

        *a_pipe_vfsn = {};

        s_pipefs_vfsn_zone.free(a_pipe_vfsn);

        s_pipe_count--;

        lock_done(&s_global_lock);
}

// ********************************************************************************************************************************
kern_return_t pipefs_pipe2(int a_oflags, vfs_file_t *a_files_out)
// ********************************************************************************************************************************
{
        pipe_vfsn_t pvfsn;

        kern_return_t retval = pipefs_vfsn_alloc(&pvfsn);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        vfs_file_t rdf = nullptr;

        retval = vfs_file_open(&pvfsn->m_vfsn, a_oflags | O_RDONLY, &rdf);

        vfs_node_rel(&pvfsn->m_vfsn);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        vfs_file_t wrf = nullptr;

        retval = vfs_file_open(&pvfsn->m_vfsn, a_oflags | O_WRONLY, &wrf);

        if (KERN_STATUS_FAILURE(retval))
        {
                vfs_file_rel(rdf);
                return retval;
        }

        a_files_out[0] = rdf;

        a_files_out[1] = wrf;

        return retval;
}

// ********************************************************************************************************************************
kern_return_t pipefs_create_aliased(vfs_node_t a_alias_vfsn, vfs_node_t *a_fifo_vfsn_out)
// ********************************************************************************************************************************
{
        pipe_vfsn_t pvfsn;

        kern_return_t retval = pipefs_vfsn_alloc(&pvfsn);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        vfs_node_ref((pvfsn->m_aliased_by = a_alias_vfsn));

        *a_fifo_vfsn_out = &pvfsn->m_vfsn;

        return retval;
}

// ********************************************************************************************************************************
static kern_return_t pipefs_init()
// ********************************************************************************************************************************
{
        lock_init(&s_global_lock, true);

        // vfs_mount_init(&s_pipefs_mount.m_mount);

        s_pipefs_mount.m_mount.m_ops = &s_pipefs_vfsm_ops;

        s_pipefs_last_id = 0;

        s_pipe_count = 0;

        return KERN_SUCCESS;
}

fs_initcall(pipefs_init);
