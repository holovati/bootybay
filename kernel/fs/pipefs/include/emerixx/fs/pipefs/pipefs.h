#pragma once

#if !defined(__NEED_vfs_file)
#define __NEED_vfs_file
#endif

#if !defined(__NEED_vfs_node)
#define __NEED_vfs_node
#endif

#include <emerixx/vfs/vfs_types.h>

// For pipe2 system call
kern_return_t pipefs_pipe2(int a_oflags, vfs_file_t *a_files_out);

kern_return_t pipefs_create_aliased(vfs_node_t a_alias_vfsn, vfs_node_t *a_fifo_vfsn_out);
