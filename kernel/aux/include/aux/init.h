#pragma once

typedef int (*initcall_t)();
typedef void (*exitcall_t)();

#define __define_initcall(level, fn)                                                                                               \
        static initcall_t __initcall##level##_##fn __attribute__((section(".initcall" #level))) __attribute__((used)) = fn

#define early_initcall(fn) __define_initcall(early, fn)

#define INITCALL_PURE     0
#define INITCALL_CORE     1
#define INITCALL_POSTCORE 2
#define INITCALL_ARCH     3
#define INITCALL_SUBSYS   4
#define INITCALL_FS       5
#define INITCALL_DEVICE   6
#define INITCALL_LATE     7

// A "pure" initcall has no dependencies on anything else, and purely
// initializes variables that couldn't be statically initialized.
#define pure_initcall(fn) __define_initcall(0, fn)
// Used for core kernel services and for some early arch/driver
// specific initialization that do not depend on any kernel core
// initialization.
#define core_initcall(fn) __define_initcall(1, fn)

// Initialization of functions that depend on core initialization and that do
// not fall into the other categories.
#define postcore_initcall(fn) __define_initcall(2, fn)
// Used for architecture specific initialization (i386, ARM, Mips,etc.).
#define arch_initcall(fn) __define_initcall(3, fn)
// Used to initialize kernel subsystems.
#define subsys_initcall(fn) __define_initcall(4, fn)
#define fs_initcall(fn)     __define_initcall(5, fn)
// Device initialization.
#define device_initcall(fn) __define_initcall(6, fn)
#define late_initcall(fn)   __define_initcall(7, fn)

#define module_init    device_initcall
#define module_exit(x) static void (*____f)() __attribute__((unused)) = x
