#pragma once

typedef struct font_data
{
        uint32_t width;
        uint32_t height;
        uint32_t count;
        uint32_t length;
        uint8_t *bitmap;
} * font_t;

kern_return_t psf_extract_font_data(void *a_pfs_buffer, font_t a_font_data_out);