#pragma once

typedef void (*graphics_plot_pixel_cb_t)(uint32_t a_xpos, uint32_t a_ypos, void *a_udata);

void graphics_draw_psf_glyph(uint32_t a_glyph,
                             uint32_t a_xpos,
                             uint32_t a_ypos,
                             uint8_t *a_glyph_bitmap,
                             uint32_t a_glyph_width,
                             uint32_t a_glyph_height,
                             uint32_t a_glyph_size,
                             graphics_plot_pixel_cb_t a_plot_pixel,
                             void *a_fgbg_udata /*MUST POINT TO VOID*[2] or NULL*/);

void graphics_line_to(uint32_t x1, uint32_t y1, uint32_t x2, uint32_t y2, graphics_plot_pixel_cb_t a_plot_pixel, void *a_udata);