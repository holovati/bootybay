#include <aux/graphics.h>
#include <etl/bit.hh>
#include <etl/cstdlib.hh>

/* macro to return the sign of a number */
#define sgn(x) ((x < 0) ? -1 : ((x > 0) ? 1 : 0))

// ********************************************************************************************************************************
void graphics_draw_psf_glyph(uint32_t a_glyph,
                             uint32_t a_xpos,
                             uint32_t a_ypos,
                             uint8_t *a_glyph_bitmap,
                             uint32_t a_glyph_width,
                             uint32_t a_glyph_height,
                             uint32_t a_glyph_size,
                             graphics_plot_pixel_cb_t a_plot_pixel,
                             void *a_udata /* ACTUALLY A VOID*[2] */)
// ********************************************************************************************************************************
{
        void **udata;
        uintptr_t nulldata[] = {0, 0};

        if (a_udata != nullptr)
        {
                udata = reinterpret_cast<void **>(a_udata);
        }
        else
        {
                udata = reinterpret_cast<void **>(nulldata);
        }

        uint8_t *bmap = a_glyph_bitmap + (a_glyph * a_glyph_size);
        for (uint32_t y = 0; y < a_glyph_height; ++y, ++a_ypos)
        {
                for (uint32_t x = 0; x < a_glyph_width; ++x)
                {
                        a_plot_pixel(a_xpos + x, a_ypos, udata[(etl::rol(bmap[y], x) & 1) == 0]);
                }
        }
}

// http://www.brackeen.com/vga/source/djgpp20/lines.c.html
/**************************************************************************
 *  line_fast                                                             *
 *    draws a line using Bresenham's line-drawing algorithm, which uses   *
 *    no multiplication or division.                                      *
 **************************************************************************/
// ********************************************************************************************************************************
void graphics_line_to(uint32_t x1, uint32_t y1, uint32_t x2, uint32_t y2, graphics_plot_pixel_cb_t a_plot_pixel, void *a_udata)
// ********************************************************************************************************************************
{
        int i, dx, dy, sdx, sdy, dxabs, dyabs, x, y, px, py;

        dx    = x2 - x1; /* the horizontal distance of the line */
        dy    = y2 - y1; /* the vertical distance of the line */
        dxabs = etl::abs(dx);
        dyabs = etl::abs(dy);
        sdx   = sgn(dx);
        sdy   = sgn(dy);
        x     = dyabs >> 1;
        y     = dxabs >> 1;
        px    = x1;
        py    = y1;

        a_plot_pixel(px, py, a_udata);

        if (dxabs >= dyabs) /* the line is more horizontal than vertical */
        {
                for (i = 0; i < dxabs; i++)
                {
                        y += dyabs;
                        if (y >= dxabs)
                        {
                                y -= dxabs;
                                py += sdy;
                        }
                        px += sdx;
                        a_plot_pixel(px, py, a_udata);
                }
        }
        else /* the line is more vertical than horizontal */
        {
                for (i = 0; i < dyabs; i++)
                {
                        x += dxabs;
                        if (x >= dyabs)
                        {
                                x -= dyabs;
                                px += sdx;
                        }
                        py += sdy;
                        a_plot_pixel(px, py, a_udata);
                }
        }
}