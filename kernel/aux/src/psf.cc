#include <aux/psf.h>

#define PSF1_MAGIC0 0x36
#define PSF1_MAGIC1 0x04

#define PSF1_MODE512    0x01
#define PSF1_MODEHASTAB 0x02
#define PSF1_MODEHASSEQ 0x04
#define PSF1_MAXMODE    0x05

#define PSF1_SEPARATOR 0xFFFF
#define PSF1_STARTSEQ  0xFFFE

#define PSF2_MAGIC 0x864AB572

/* bits used in flags */
#define PSF2_HAS_UNICODE_TABLE 0x01

/* max version recognized so far */
#define PSF2_MAXVERSION 0

/* UTF8 separators */
#define PSF2_SEPARATOR 0xFF
#define PSF2_STARTSEQ  0xFE

union psfhdr {
        struct
        {
                uint8_t magic[2];
                uint8_t filemode;
                uint8_t fontheight;
                uint8_t data[0];

        } psf1;

        struct
        {
                uint32_t magic;
                uint32_t version;
                uint32_t header_size;
                uint32_t flags;
                uint32_t glyph_count;
                uint32_t glyph_size;
                uint32_t glyph_height;
                uint32_t glyph_width;
                uint8_t data[0];
        } psf2;
};

kern_return_t psf_extract_font_data(void *a_pfs_buffer, font_t a_font_data_out)
{
        psfhdr *f = reinterpret_cast<psfhdr *>(a_pfs_buffer);

        kern_return_t retval = KERN_SUCCESS;

        if ((f->psf1.magic[0] == PSF1_MAGIC0) && (f->psf1.magic[1] == PSF1_MAGIC1))
        {
                a_font_data_out->width  = 8;
                a_font_data_out->height = f->psf1.fontheight;
                a_font_data_out->count  = f->psf1.filemode & PSF1_MODE512 ? 512 : 256;
                a_font_data_out->length = a_font_data_out->height;
                a_font_data_out->bitmap = f->psf1.data;
        }
        else if (f->psf2.magic == PSF2_MAGIC)
        {
                a_font_data_out->width  = f->psf2.glyph_width;
                a_font_data_out->height = f->psf2.glyph_height;
                a_font_data_out->count  = f->psf2.glyph_count;
                a_font_data_out->length = f->psf2.glyph_size;
                a_font_data_out->bitmap = f->psf2.data;
        }
        else
        {
                log_warn("Unknown font format");
                retval = KERN_FAIL(EINVAL);
        }

        return retval;
}
