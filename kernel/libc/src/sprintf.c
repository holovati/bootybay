#define STB_SPRINTF_NOFLOAT
#define STB_SPRINTF_DECORATE
#define STB_SPRINTF_IMPLEMENTATION
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpadded"
#pragma GCC diagnostic ignored "-Wshadow"
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wcast-align"
#pragma GCC diagnostic ignored "-Wcast-qual"
#include "3rdparty/stb/stb_sprintf.h"
#pragma GCC diagnostic pop
int printf(const char *format, ...);
int cnputc(int);
int printf(const char *format, ...)
{
        va_list va;
        va_start(va, format);
        char buffer[512];
        const int ret = vsnprintf(buffer, sizeof(buffer) - 1, format, va);
        va_end(va);

        for (int i = 0; i < ret; i++)
        {
                cnputc(buffer[i]);
        }

        return ret;
}