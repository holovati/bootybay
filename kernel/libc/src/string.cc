#include <limits.h>
#include <malloc.h>
#include <string.h>
#include <strings.h>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wparentheses"
#pragma GCC diagnostic ignored "-Wcast-qual"

char *strdup(const char *other) {
  size_t l = strlen(other);
  void *d  = malloc(l + 1);
  if (!d)
    return nullptr;
  return (char *)memcpy(d, (const void *)other, l + 1);
}

#define SS         (sizeof(size_t))
#define ALIGN      (sizeof(size_t) - 1)
#define ONES       ((size_t)-1 / UCHAR_MAX)
#define HIGHS      (ONES * (UCHAR_MAX / 2 + 1))
#define HASZERO(x) ((x)-ONES & ~(x)&HIGHS)

void *memchr(const void *src, int c, size_t n) {
  const unsigned char *s = (const unsigned char *)src;
  c                      = (unsigned char)c;
#ifdef __GNUC__
  for (; ((uintptr_t)s & ALIGN) && n && *s != c; s++, n--)
    ;
  if (n && *s != c) {
    typedef size_t __attribute__((__may_alias__)) word;
    const word *w;
    size_t k = ONES * (size_t)c;
    for (w = (const word *)s; n >= SS && !HASZERO(*w ^ k); w++, n -= SS)
      ;
    s = (const unsigned char *)w;
  }
#endif
  for (; n && *s != c; s++, n--)
    ;
  return n ? (void *)s : 0;
}

void bzero(void *s, size_t n) { memset(s, 0, n); }

void bcopy(const void *s1, void *s2, size_t n) { memmove(s2, s1, n); }
#pragma GCC diagnostic pop