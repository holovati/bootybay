// Lifted from musl
#include <ctype.h>
#undef isdigit

int isdigit(int c) { return (unsigned)c - '0' < 10; }

int isxdigit(int c)
{
	return isdigit(c) || ((unsigned)c|32)-'a' < 6;
}
