// Lifted from musl
#include <ctype.h>
#undef isspace

int isspace(int c) { return c == ' ' || (unsigned)c - '\t' < 5; }
