#include <assert.h>
#include <stdio.h>

extern "C" void cpu_halt(void);

extern void bflush(int dev);
extern int cold;
extern "C" void __assert_fail(const char *assertion, const char *file, int line, const char *function)
{
        if (!cold)
        {
                // bflush(-1);
        }

        printf("Assertion (%s) in function %s failed. (%s:%d)\n", assertion, function, file, line);

        // cpu_halt();
        __builtin_unreachable();
}