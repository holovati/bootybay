
#include <stdlib.h>
#include <wchar.h>

//#include "internal.h"
#undef MB_CUR_MAX
#define MB_CUR_MAX 1
/* Arbitrary encoding for representing code units instead of characters. */
#define CODEUNIT(c)    (0xdfff & (signed char)(c))
#define IS_CODEUNIT(c) ((unsigned)(c)-0xdf80 < 0x80)

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpadded"
#pragma GCC diagnostic ignored "-Wcast-qual"
#pragma GCC diagnostic ignored "-Wparentheses"
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wshadow"
#pragma GCC diagnostic ignored "-Wfloat-conversion"
#pragma GCC diagnostic ignored "-Wswitch-default"
#pragma GCC diagnostic ignored "-Wunused-parameter"

size_t wcrtomb(char *restrict s, wchar_t wc, mbstate_t *restrict st)
{
        if (!s)
                return 1;
        if ((unsigned)wc < 0x80)
        {
                *s = wc;
                return 1;
        }
        else if (MB_CUR_MAX == 1)
        {
                if (!IS_CODEUNIT(wc))
                {
#ifndef KERNEL
                        errno = EILSEQ;
#endif
                        return -1;
                }
                *s = wc;
                return 1;
        }
        else if ((unsigned)wc < 0x800)
        {
                *s++ = 0xc0 | (wc >> 6);
                *s   = 0x80 | (wc & 0x3f);
                return 2;
        }
        else if ((unsigned)wc < 0xd800 || (unsigned)wc - 0xe000 < 0x2000)
        {
                *s++ = 0xe0 | (wc >> 12);
                *s++ = 0x80 | ((wc >> 6) & 0x3f);
                *s   = 0x80 | (wc & 0x3f);
                return 3;
        }
        else if ((unsigned)wc - 0x10000 < 0x100000)
        {
                *s++ = 0xf0 | (wc >> 18);
                *s++ = 0x80 | ((wc >> 12) & 0x3f);
                *s++ = 0x80 | ((wc >> 6) & 0x3f);
                *s   = 0x80 | (wc & 0x3f);
                return 4;
        }
        errno = EILSEQ;
        return -1;
}

#pragma GCC diagnostic pop