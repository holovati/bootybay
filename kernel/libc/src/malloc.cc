// Poor man's malloc, only used by libtsm
#include <string.h>
#include <stdlib.h>

#include <kernel/zalloc.h>
#include <kernel/vm/vm_kern.h>

#include <etl/algorithm.hh>
#include <etl/bit.hh>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"

template <size_t SZ>
struct memblock
{
        natural_t size;
        uint8_t data[SZ];
};

typedef memblock<0> mblockany;

#define MEMBLOCK_ZONE(sz)                                                                                                          \
        typedef memblock<sz> mblock##sz;                                                                                           \
        ZONE_DEFINE(mblock##sz, s_mblock##sz, 1000, 10, ZONE_EXHAUSTIBLE, 10)
MEMBLOCK_ZONE(64);
MEMBLOCK_ZONE(128);
MEMBLOCK_ZONE(256);
MEMBLOCK_ZONE(512);
MEMBLOCK_ZONE(1024);
MEMBLOCK_ZONE(2048);
MEMBLOCK_ZONE(4096);
#pragma GCC diagnostic pop

extern "C"
{

// ********************************************************************************************************************************
void *aligned_alloc(size_t a_alignment, size_t a_size)
// ********************************************************************************************************************************
{
        // KASSERT(a_alignment <= 8);

        if (a_size > PAGE_SIZE)
        {
                a_size += sizeof(natural_t);
        }

        if (etl::is_power_of_two(a_size) == false)
        {
                a_size = etl::round_next_pow2(a_size);
        }

        mblockany *mbany = nullptr;

        switch (a_size)
        {
                case 2:
                case 4:
                case 8:
                case 16:
                case 32:
                case 64:
                        mbany = (mblockany *)s_mblock64.alloc();
                        break;
                case 128:
                        mbany = (mblockany *)s_mblock128.alloc();
                        break;
                case 256:
                        mbany = (mblockany *)s_mblock256.alloc();
                        break;
                case 512:
                        mbany = (mblockany *)s_mblock512.alloc();
                        break;
                case 1024:
                        mbany = (mblockany *)s_mblock1024.alloc();
                        break;
                case 2048:
                        mbany = (mblockany *)s_mblock2048.alloc();
                        break;
                case 4096:
                        mbany = (mblockany *)s_mblock4096.alloc();
                        break;
                default:
                        kmem_alloc(kernel_map, (vm_offset_t *)&mbany, a_size);
                        break;
        }

        mbany->size = a_size;

        return mbany->data;
}

// ********************************************************************************************************************************
void *malloc(size_t a_size)
// ********************************************************************************************************************************
{
        return aligned_alloc(alignof(void *), a_size);
}

// ********************************************************************************************************************************
void *calloc(size_t a_num, size_t a_size)
// ********************************************************************************************************************************
{
        void *p = malloc(a_size * a_num);
        memset(p, 0, a_size * a_num);
        return p;
}

// ********************************************************************************************************************************
void *realloc(void *a_ptr, size_t a_new_size)
// ********************************************************************************************************************************
{
        if (a_ptr != NULL)
        {
                mblockany *mbany = (mblockany *)(((uint8_t *)a_ptr) - sizeof(natural_t));

                if (a_new_size <= mbany->size)
                {
                        // Very nice
                        return a_ptr;
                }

                void *np = malloc(a_new_size);

                memcpy(np, a_ptr, mbany->size);

                free(a_ptr);

                a_ptr = np;
        }
        else
        {
                void *np = malloc(a_new_size);

                a_ptr = np;
        }

        return a_ptr;
}

// ********************************************************************************************************************************
void free(void *a_ptr)
// ********************************************************************************************************************************
{
        if (a_ptr == NULL)
        {
                return;
        }

        mblockany *mbany = (mblockany *)(((uint8_t *)a_ptr) - sizeof(natural_t));
        switch (mbany->size)
        {
                case 2:
                case 4:
                case 8:
                case 16:
                case 32:
                case 64:
                        s_mblock64.free((mblock64 *)(mbany));
                        break;
                case 128:
                        s_mblock128.free((mblock128 *)(mbany));
                        break;
                case 256:
                        s_mblock256.free((mblock256 *)(mbany));
                        break;
                case 512:
                        s_mblock512.free((mblock512 *)(mbany));
                        break;
                case 1024:
                        s_mblock1024.free((mblock1024 *)(mbany));
                        break;
                case 2048:
                        s_mblock2048.free((mblock2048 *)(mbany));
                        break;
                case 4096:
                        s_mblock4096.free((mblock4096 *)(mbany));
                        break;
                default:
                        kmem_free(kernel_map, (vm_offset_t)mbany, mbany->size);
                        break;
        }
}
}
