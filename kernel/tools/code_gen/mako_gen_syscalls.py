import argparse
import os
import json

from mako.template import Template
from mako.runtime import Context

def dict2obj(d):
    if isinstance(d, list):
        d = [dict2obj(x) for x in d]
    if not isinstance(d, dict):
        return d
    class C(object):
        pass
    o = C()
    for k in d:
        o.__dict__[k] = dict2obj(d[k])
    return o

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--template", help="path to mako template file", type=str)
    parser.add_argument("--syscall-descriptors", help="path to syscall descriptor file", type=str)
    parser.add_argument("--rs64-syscall-descriptor-files", help="RS64 system call descriptor files semicolon seperated", type=str)
    parser.add_argument("--tcb-descriptor", help="path to thread context descriptor for the architecture", type=str)
    parser.add_argument("--output", help="path to output file", type=str)
    parser.add_argument("--mako-module-dir", help="path to mako temp directory", type=str, default="/tmp/mako_modules")
    args = parser.parse_args()
    
    syscall_descriptor_file = open(args.syscall_descriptors,"r")
    tcb_descriptor_file = open(args.tcb_descriptor, "r")        

    data_dict = {"linux": json.loads(syscall_descriptor_file.read()), "tcb": json.loads(tcb_descriptor_file.read()) }

    syscall_template = Template(filename=args.template, module_directory=args.mako_module_dir)
    
    if not os.path.exists(os.path.dirname(args.output)):
        try:
            os.makedirs(os.path.dirname(args.output))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise

    #print(args.rs64_syscall_descriptor_files)

    rs64_descriptor_file_paths = args.rs64_syscall_descriptor_files.split(";")
    rs64_descriptor_list = []
    for path in rs64_descriptor_file_paths:
        descriptor_file = open(path, "r") 
        rs64_descriptor_list.append(json.loads(descriptor_file.read()))
        descriptor_file.close()

    data_dict["rs64_classes"] = rs64_descriptor_list
    data_dict["rs64_methods"] = { }
    
    tempdict = { }
    #print(json.dumps(data_dict))
    for descriptor in rs64_descriptor_list:
        for clazz in descriptor["classes"]:
            for method in clazz["methods"]:
                tempdict[method["linux_syscall"]] = dict2obj(method)

    output_file = open(args.output, "w")
    
    data_dict = dict2obj(data_dict)
    data_dict.rs64_methods = tempdict

    #print(data_dict.rs64_methods)
    output_file.write(syscall_template.render(data=data_dict))
    
    
if __name__ == "__main__":
    main()
    
    
    
    
    