// Linux ${data.linux.version_tag} system call glue
// Based on ${data.linux.syscall_table_file}

#include <mqueue.h>
#include <signal.h>
#include <sys/types.h>

% for inc_header in data.tcb.include_files:
${inc_header}
% endfor

% for rs64_class in data.rs64_classes:
#include ${"\"{0}\"".format(rs64_class.header_file) }
% endfor

typedef i32 __s32;
typedef u32 __u32;

using SyscallEntryFunction = void (*)(TaskControlBlock *);

void syscall_dispatch(${data.tcb.context_type} *);

% for syscall in [s for s in data.linux.syscalls if s.implemented and s.name in data.rs64_methods]:
static void sys_${syscall.name}(${data.tcb.context_type} *tcb)
{
<%
rs64_method = data.rs64_methods[syscall.name]
parameters = zip(syscall.parameters, rs64_method.parameters, data.tcb.arg_regs[:len(syscall.parameters)])
%>
% for linux_param, rs64_param, reg in parameters:
    ${"{0} {1} = ({2})(tcb->{3});".format(linux_param.type, linux_param.name, linux_param.type, reg)}
% endfor
    <% parameters = ["static_cast<{0}>({1})".format(rs64_param.type, linux_param.name) for linux_param, rs64_param in zip(syscall.parameters, rs64_method.parameters)] %>
    tcb->${data.tcb.return_reg} = static_cast<${data.tcb.reg_type}>(${rs64_method.plain_identifier}(${', '.join(parameters)}));
}

% endfor
// Linux system calls that are not currently implemented in RS64

% for syscall in [s for s in data.linux.syscalls if s.implemented and not s.name in data.rs64_methods]:
// ${syscall.number} - ABI: ${syscall.abi}
static void sys_${syscall.name}(${data.tcb.context_type} *tcb)
{
        UNUSED(tcb);
        panic(${"\"Linux syscall {0} does not have a RS64 binding\"".format(syscall.name)});
}

% endfor
// System calls that Linux does not implement

% for syscall in [s for s in data.linux.syscalls if not s.implemented]:
// ${syscall.number} - ABI: ${syscall.abi}
static void sys_${syscall.name}(${data.tcb.context_type} *tcb)
{
        // Not implemented
        panic("Called unimplemented linux syscall ${syscall.number}"); // For now
        tcb->${data.tcb.return_reg} = static_cast<${data.tcb.reg_type}>(KERN_FAIL(ENOSYS));
}

% endfor
static SyscallEntryFunction const syscall_dispatch_table[] = {
${',\n'.join([ "\t/* {0} */ sys_{1}".format(syscall.number, syscall.name) for syscall in data.linux.syscalls])}
};

void syscall_dispatch(${data.tcb.context_type} * tcb)
{
        if (tcb->${data.tcb.syscall_number_reg} < ${len(data.linux.syscalls) - 1})
        {
                syscall_dispatch_table[tcb->${data.tcb.syscall_number_reg}](tcb);
        }
        else
        {
                tcb->${data.tcb.return_reg} = static_cast<${data.tcb.reg_type}>(KERN_FAIL(ENOSYS));
                panic("For now");
        }
}