import json
import re
import subprocess
import sys
import argparse
import os

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--linux-dir", help="root of the linux source tree", type=str)
    parser.add_argument("--table-file", help="path to syscall table file, relative to --linux-dir", type=str)
    parser.add_argument("--abi", help="abi to export from syscall table file, common is always exported", type=str, default="")
    parser.add_argument("--git-tag", help="checkout the correct tag before exporting", type=str, default="")
    parser.add_argument("--output", help="path to file where json will be written", type=str)
    args = parser.parse_args()

    linux_src_path = os.path.realpath(args.linux_dir)

    if not os.path.isdir(linux_src_path):
        print(args.linux_dir + " not found or is not a directory")
        exit(1)
    
    syscall_table_path = os.path.realpath(linux_src_path + "/" + args.table_file)

    if not os.path.isfile(syscall_table_path):
        print(args.table_file + " not found in " + args.linux_dir)
        exit(1)

    if args.git_tag is not "":
        out = run_command("git checkout tags/" + args.git_tag + "", linux_src_path)
        print(out)

    syscall_table_dict = extract_syscall_table(syscall_table_path, args.abi)

    syscalls = extract_syscalls(linux_src_path, syscall_table_dict)

    json_dict = { 
        'version_tag': args.git_tag,
        'syscall_table_file': args.table_file,
        'abi': ["common", args.abi], 
        'syscalls': syscalls 
    }

    f = open(args.output, "w")
    f.write(json.dumps(json_dict, indent=4))
    f.close()

    #print(json.dumps(syscall_table_dict, indent = 4))
    #print(json.dumps(syscalls, indent = 4))
    #print(args.linux_dir)
    #print(args.table_file)
    #print(args.abi)
    #print(args.git_tag)
    exit(0)

def extract_syscalls(linux_path, syscalls):
    syscall_text = run_command("grep -rhA10 --include \\*.c '^_*SYSCALL_DEFINE.\?('", linux_path)
    #print(syscall_text)
    syscall_text_array = syscall_text.replace("\n", "").replace("\t","").replace("--", "").split("SYSCALL_DEFINE")
    syscall_text_array = [ text.split(")")[0] + ")" for text in syscall_text_array ]
    syscall_text_array = [ text for text in syscall_text_array if "(" in text ]
    #syscall_text_array = [ text.split("SYSCALL_DEFINE")[-1] for text in syscall_text_array ]
    #print(json.dumps(syscall_text_array, indent = 4))
    linux_syscall_objects = []

    for syscall_text in syscall_text_array:
        #print(syscall_text)
        syscall_obj = type('', (), {})()
        syscall_obj.implemented = True
        # 4(epoll_ctl, int, epfd, int, op, int, fd,struct epoll_event __user *, event)
        syscall_obj.parameter_count = int(syscall_text.split("(")[0])
        syscall_obj.name = syscall_text.split("(")[-1].split(",")[0].replace(")", "").strip()
        #print(syscall_obj.name)
        if not syscall_obj.name in syscalls:
            continue
        
        syscall = syscalls[syscall_obj.name]
        
        syscall_obj.number = syscall['number']
           
        syscall_obj.abi = syscall['abi']        
        
        syscall_obj.parameters = []

        if syscall_obj.parameter_count > 0:
            param_text = syscall_text.split("(" + syscall_obj.name + ",")[1][:-1].replace(", ",",")
            param_text = param_text.split(",")
            params = ','.join([param_text[i - 1] + "|" + param_text[i] for i in range(1, len(param_text), 2)]).split(",")
        
            for param in params:
                param_obj = type('', (), {})()
                param_type_and_attr = param.split("|")[0].strip()
                
                param_obj.name = param.split("|")[1].strip()
                
                attrib = re.compile('.*(\_\_[a-zA-Z]+)\s.*').search(param_type_and_attr)
                
                param_obj.type = param_type_and_attr            
                if attrib is not None:
                    attrib = attrib.group(1)
                    param_obj.type = param_type_and_attr.replace(attrib, "").strip().replace("  ", " ")
                else:
                    attrib = ""
                param_obj.attribute = attrib            
                param_obj.is_pointer = '*' in param_obj.type
                param_obj.is_struct = 'struct' in param_obj.type
                param_obj.is_posix_type = '_t ' in param_obj.type
                param_obj.is_const = 'const ' in param_obj.type
                                
                syscall_obj.parameters.append(param_obj.__dict__)

        linux_syscall_objects.append(syscall_obj.__dict__)
        
    
    #print (json.dumps(linux_syscall_objects, indent =4))
    
    linux_syscall_objects = sorted(linux_syscall_objects, key=lambda n: n['number'])
    
    ret = []
    for i in range(0, linux_syscall_objects[-1]['number'] + 1):
        unimplemented_obj = type('', (), {})()
        unimplemented_obj.implemented = False
        unimplemented_obj.parameter_count = 0
        unimplemented_obj.name = "NotImplemented{0}".format(i)
        unimplemented_obj.number = i
        unimplemented_obj.abi = ""
        unimplemented_obj.parameters = []
        
        ret.append(unimplemented_obj.__dict__)
    
    
    
    for syso in linux_syscall_objects:
        ret[syso['number']] = syso
           
    return ret

def extract_syscall_table(path, abi):
    table_text = open(path, "r").read()
    ret_dict = {}
    for tup in re.compile(r"(\d{1,4})\s*common\s*(\S*)").findall(table_text):
        obj = type('', (), {})()
        obj.abi = "common"
        obj.number = int(tup[0])
        ret_dict[tup[1]] = obj.__dict__
    
    for tup in re.compile("(\d{1,4})\s*"+abi+"\s*(\S*)").findall(table_text):
        obj = type('', (), {})()
        obj.abi = abi
        obj.number = int(tup[0])
        ret_dict[tup[1]] = obj.__dict__

    return ret_dict

def run_command(cmd, cwd):
    proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, cwd=cwd)
    out, err = proc.communicate()
    return out.decode(sys.stdout.encoding)
    

if __name__ == "__main__":
    main()
