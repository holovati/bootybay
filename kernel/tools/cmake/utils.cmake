macro(make_link src dest)
  execute_process(COMMAND ln "-sf" "${src}" "${dest}")
endmacro()

function(cat IN_FILE OUT_FILE)
  file(READ ${IN_FILE} CONTENTS)
  file(APPEND ${OUT_FILE} "${CONTENTS}")
endfunction()

function(target_render_mako_file_internal target in_file depends)
  set(in_file_path "${CMAKE_CURRENT_SOURCE_DIR}/${in_file}")
  set(out_file_path "${CMAKE_CURRENT_BINARY_DIR}/mako/${in_file}")
  message(STATUS ${CMAKE_CURRENT_SOURCE_DIR})

  # remove the .mako extension
  string(REGEX MATCH "^(.*)\\.[^.]*$" dummy ${out_file_path})
  set(out_file_path ${CMAKE_MATCH_1})

  get_filename_component(in_file_folder ${in_file_path} DIRECTORY)
  get_filename_component(out_file_folder ${out_file_path} DIRECTORY)
  get_filename_component(file_ext ${out_file_path} EXT)

  set(mako_render_file_code
    "from mako.template import Template; \
      s = \"<% _in_folder = '${in_file_folder}'\\n_out_folder = '${out_file_folder}' %>\"; \
      f = open(\"${CMAKE_CURRENT_SOURCE_DIR}/${in_file}\", \"r\"); \
      s += f.read(); \
      f.close(); \
      print(Template(s).render());")

  if(CLANG_FORMAT)
    set(mako_clang_format "clang-format")
  else()
    set(mako_clang_format "cat")
  endif()

  file(MAKE_DIRECTORY "${out_file_folder}")

  add_custom_command(
    OUTPUT ${out_file_path}
    COMMAND ${Python3_EXECUTABLE} "-c" "${mako_render_file_code}" "|"
    ${mako_clang_format} ">" ${out_file_path}
    WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
    DEPENDS ${in_file_path} ${depends}
    COMMENT "Processing mako template ${in_file_path}"
    VERBATIM)
  message("${file_ext}")

  get_target_property(target_type ${target} TYPE)
  message(STATUS "dir='${target_type}'")

  if(NOT "${file_ext}" STREQUAL ".h" AND NOT "${file_ext}" STREQUAL ".hh")
    if(NOT target_type STREQUAL "UTILITY")
      target_sources(${target} PRIVATE ${out_file_path})
    else()
      add_custom_target("${target}_gen"
        DEPENDS ${out_file_path}
      )
      add_dependencies(${target} "${target}_gen")
      set_source_files_properties(${out_file_path} PROPERTIES GENERATED TRUE)
    endif()
  else()
    get_property(
      dirs
      DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
      PROPERTY INCLUDE_DIRECTORIES)

    foreach(dir ${dirs})
      message(STATUS "dir='${dir}'")
    endforeach()

    target_include_directories(${target} PRIVATE ${out_file_folder})

    if(NOT target_type STREQUAL "UTILITY")
      target_sources(${target} PRIVATE ${out_file_path})
    else()
      # add_dependencies(${target} ${out_file_path})
      set_source_files_properties(${out_file_path} PROPERTIES GENERATED TRUE)
    endif()
  endif()
endfunction()

function(target_render_mako_file target in_file)
  target_render_mako_file_internal(${target} ${in_file} "")
endfunction()

function(target_render_mako_files target)
  foreach(in_file IN LISTS ARGN)
    target_render_mako_file(${target} ${in_file})
  endforeach()
endfunction()

function(target_gen_syscall_bindings build_target template_file)
  get_target_property(dirs ${build_target} INCLUDE_DIRECTORIES)
  set(target_files)

  foreach(dir ${dirs})
    file(GLOB_RECURSE dir_files ${dir}/*.h)

    foreach(header ${dir_files})
      if(${header} STREQUAL "${CMAKE_SOURCE_DIR}/include/mach/types.h")
        continue() # we don't want this file as the SYSCALL define is there
      endif()

      if(${header} MATCHES "netbsd_tree")
        continue() # we don't want this file as the SYSCALL define is there
      endif()

      file(STRINGS "${header}" lines REGEX "SYSCALL\\(.*\\)")

      if(lines)
        list(APPEND target_files ${header})
      endif()
    endforeach()

    # message(STATUS "Syscall files ${fpath}")
  endforeach()

  # message(ERROR "Syscall files ${target_files}")
  get_property(
    dirs
    DIRECTORY ${CMAKE_SOURCE_DIR}
    PROPERTY INCLUDE_DIRECTORIES)

  # message(STATUS "-- ${dirs} --- ${ops} -- ${defs}")
  add_custom_command(
    OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/mako/syscall_descriptors.json
    COMMAND
    ${CMAKE_CXX_COMPILER} -fsyntax-only -DSYSCALL_GENERATOR -w
    -fplugin=librs64 -include${CMAKE_SOURCE_DIR}/include/mach/types.h
    -D$<JOIN:$<TARGET_PROPERTY:${build_target},COMPILE_DEFINITIONS>,$<SEMICOLON>-D>
    -I$<JOIN:$<TARGET_PROPERTY:${build_target},INCLUDE_DIRECTORIES>,$<SEMICOLON>-I>
    ${dirs} ${target_files} >
    ${CMAKE_CURRENT_BINARY_DIR}/mako/syscall_descriptors.json
    DEPENDS ${target_files}
    COMMAND_EXPAND_LISTS
    COMMENT "Scanning headers for system calls")

  target_render_mako_file_internal(
    ${build_target} ${template_file}
    ${CMAKE_CURRENT_BINARY_DIR}/mako/syscall_descriptors.json)
endfunction()

macro(target_add_null_cpp_src target)
  file(WRITE "${CMAKE_CURRENT_BINARY_DIR}/null.cpp" "static void nullsub() { }")
  target_sources(${target} PRIVATE "${CMAKE_CURRENT_BINARY_DIR}/null.cpp")
endmacro(target_add_null_cpp_src)
