/*
 * Copyright (c) 1982, 1986, 1989, 1990, 1991, 1993
 *	The Regents of the University of California.  All rights reserved.
 * (c) UNIX System Laboratories, Inc.
 * All or some portions of this file are derived from material licensed
 * to the University of California by American Telephone and Telegraph
 * Co. or Unix System Laboratories, Inc. and are reproduced herein with
 * the permission of UNIX System Laboratories, Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)kern_prot.c	8.9 (Berkeley) 2/14/95
 */

/*
 * System calls related to processes and protection
 */
#include <mach/acct.h>
#include <mach/param.h>
#include <malloc.h>
//#include <sys/times.h>
#include <errno.h>
#include <strings.h>
//#include <sys/syscallargs.h>
#include <mach/mount.h>

#include <mach/ucred.h>
#include <mach/uio.h>

int getuid(pthread_t a_procp) { return a_procp->process->p_cred->p_ruid; }

int geteuid(pthread_t a_procp) { return a_procp->process->p_cred->pc_ucred->cr_uid; }

int getgid(pthread_t a_procp) { return a_procp->process->p_cred->p_rgid; }

/*
 * Get effective group ID.  The "egid" is groups[0], and could be obtained
 * via getgroups.  This syscall exists because it is somewhat painful to do
 * correctly in a library function.
 */
int getegid(pthread_t a_procp) { return a_procp->process->p_cred->pc_ucred->cr_groups[0]; }

int setuid(pthread_t a_procp, uid_t a_uid)
{
        struct pcred *pc = a_procp->process->p_cred;
        uid_t uid;
        int error;

        uid = a_uid;

        if (uid != pc->p_ruid && (error = suser(pc->pc_ucred, &a_procp->process->p_acflag)))
        {
                return (error);
        }

        /*
         * Everything's okay, do it.
         * Transfer proc count to new user.
         * Copy credentials so other references do not see our changes.
         */
        proc_change_count(pc->p_ruid, -1);
        proc_change_count(uid, 1);
        pc->pc_ucred         = crcopy(pc->pc_ucred);
        pc->pc_ucred->cr_uid = uid;
        pc->p_ruid           = uid;
        pc->p_svuid          = uid;
        a_procp->process->p_flag |= P_SUGID;
        return (0);
}

#if DAMIR
/* ARGSUSED */
int seteuid(p, uap, retval) pthread_t p;
struct seteuid_args /* {
        syscallarg(uid_t) euid;
} */ *uap;
register_t *retval;
{
        register struct pcred *pc = p->p_cred;
        register uid_t euid;
        int error;

        euid = SCARG(uap, euid);
        if (euid != pc->p_ruid && euid != pc->p_svuid && (error = suser(pc->pc_ucred, &p->p_acflag)))
                return (error);
        /*
         * Everything's okay, do it.  Copy credentials so other references do
         * not see our changes.
         */
        pc->pc_ucred         = crcopy(pc->pc_ucred);
        pc->pc_ucred->cr_uid = euid;
        p->p_flag |= P_SUGID;
        return (0);
}
#endif

int setgid(pthread_t a_procp, gid_t a_gid)
{
        struct pcred *pc = a_procp->process->p_cred;
        int error;

        if (a_gid != pc->p_rgid && (error = suser(pc->pc_ucred, &a_procp->process->p_acflag)))
        {
                return error;
        }

        pc->pc_ucred               = crcopy(pc->pc_ucred);
        pc->pc_ucred->cr_groups[0] = a_gid;
        pc->p_rgid                 = a_gid;
        pc->p_svgid                = a_gid; /* ??? */
        a_procp->process->p_flag |= P_SUGID;
        return (0);
}
#ifdef DAMIR
/* ARGSUSED */
int setegid(p, uap, retval) pthread_t p;
struct setegid_args /* {
        syscallarg(gid_t) egid;
} */ *uap;
register_t *retval;
{
        register struct pcred *pc = p->p_cred;
        register gid_t egid;
        int error;

        egid = SCARG(uap, egid);
        if (egid != pc->p_rgid && egid != pc->p_svgid && (error = suser(pc->pc_ucred, &p->p_acflag)))
                return (error);
        pc->pc_ucred               = crcopy(pc->pc_ucred);
        pc->pc_ucred->cr_groups[0] = egid;
        p->p_flag |= P_SUGID;
        return (0);
}
#endif

/*
 * Check if gid is a member of the group set.
 */
int groupmember(gid_t gid, struct ucred *cred)
{
        gid_t *gp;
        gid_t *egp;

        egp = &(cred->cr_groups[cred->cr_ngroups]);
        for (gp = cred->cr_groups; gp < egp; gp++)
                if (*gp == gid)
                        return (1);
        return (0);
}

/*
 * Test whether the specified credentials imply "super-user"
 * privilege; if so, and we have accounting info, set the flag
 * indicating use of super-powers.
 * Returns 0 or error.
 */
int suser(struct ucred *cred, integer_t *acflag)
{
        if (cred->cr_uid == 0)
        {
                if (acflag)
                        *acflag |= ASU;
                return (0);
        }
        return -EPERM;
}

/*
 * Allocate a zeroed cred structure.
 */
struct ucred *crget()
{
        struct ucred *cr;
#if DAMIR
        MALLOC(cr, struct ucred *, sizeof(*cr), M_CRED, M_WAITOK);
#else
        cr = (struct ucred *)malloc(sizeof *cr);
#endif
        bzero(cr, sizeof(*cr));
        cr->cr_ref = 1;
        return cr;
}

/*
 * Free a cred structure.
 * Throws away space when ref count gets to 0.
 */
void crfree(struct ucred *cr)
{
        int s;

        // s = splimp(); /* ??? */
        if (--cr->cr_ref == 0)
#if DAMIR
                FREE((caddr_t)cr, M_CRED);
#else
                free(cr);
#endif
        // splx(s);
}

/*
 * Copy cred structure to a new one and free the old one.
 */
struct ucred *crcopy(struct ucred *cr)
{
        struct ucred *newcr;

        if (cr->cr_ref == 1)
        {
                return cr;
        }

        newcr  = crget();
        *newcr = *cr;
        crfree(cr);
        newcr->cr_ref = 1;
        return (newcr);
}

/*
 * Dup cred struct to a new held one.
 */
struct ucred *crdup(struct ucred *cr)
{
        struct ucred *newcr;

        newcr         = crget();
        *newcr        = *cr;
        newcr->cr_ref = 1;
        return (newcr);
}

#if DAMIR
/*
 * Get login name, if available.
 */
/* ARGSUSED */
int getlogin(p, uap, retval) pthread_t p;
struct getlogin_args /* {
        syscallarg(char *) namebuf;
        syscallarg(u_int) namelen;
} */ *uap;
register_t *retval;
{
        if (SCARG(uap, namelen) > sizeof(p->p_pgrp->pg_session->s_login))
                SCARG(uap, namelen) = sizeof(p->p_pgrp->pg_session->s_login);
        return (copyout((caddr_t)p->p_pgrp->pg_session->s_login, (caddr_t)SCARG(uap, namebuf), SCARG(uap, namelen)));
}

/*
 * Set login name.
 */
/* ARGSUSED */
int setlogin(p, uap, retval) pthread_t p;
struct setlogin_args /* {
        syscallarg(char *) namebuf;
} */ *uap;
register_t *retval;
{
        int error;

        if (error = suser(p->p_ucred, &p->p_acflag))
                return (error);
        error = copyinstr((caddr_t)SCARG(uap, namebuf),
                          (caddr_t)p->p_pgrp->pg_session->s_login,
                          sizeof(p->p_pgrp->pg_session->s_login) - 1,
                          (u_int *)0);
        if (error == -ENAMETOOLONG)
                error = -EINVAL;
        return (error);
}
#endif