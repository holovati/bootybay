#define RBTREE_IMPLEMENTATION
#include <etl/rbtree.hh>

#include <aux/init.h>
#include <emerixx/pci_regs.h>
#include <kernel/mach_clock.h>
#include <mach/machine/bus/pci/pci_cfgreg.h>
#include <stdio.h>
#include <string.h>
#include <vfs/mount.h>

struct mount *g_procfsroot;

struct procfs_node : vnode
{
        rbtnode_data m_sibling_link;

        procfs_node(char const *a_nm)
            : vnode(g_procfsroot)
        {
                m_fstat.st_dev     = makedev(0, 0);
                m_fstat.st_ino     = 0;
                m_fstat.st_nlink   = 2;
                m_fstat.st_mode    = S_IFDIR | S_IROTH | S_IXOTH | S_IRGRP | S_IXGRP | S_IXUSR | S_IRUSR;
                m_fstat.st_uid     = 0;
                m_fstat.st_gid     = 0;
                m_fstat.st_rdev    = makedev(0, 0);
                m_fstat.st_size    = 0;
                m_fstat.st_blksize = PAGE_SIZE;
                m_fstat.st_blocks  = 0;

                mach_get_timespec(&m_fstat.st_atim);
                m_fstat.st_mtim = m_fstat.st_ctim = m_fstat.st_atim;

                m_dirent.d_ino    = 0;
                m_dirent.d_namlen = strlen(a_nm);
                m_dirent.d_reclen = DENT_RECLEN(m_dirent.d_namlen);
                strncpy(m_dirent.d_name, a_nm, sizeof(m_dirent.d_name));
                m_dirent.d_off      = 0;
                m_dirent.d_creatino = 0;

                m_dirent.d_type = IFTODT(m_fstat.st_mode);
        }

        virtual ~procfs_node() {}

        kern_return_t fs_readdir(fs_readdir_cb_t a_cb, fs_readdir_ctx_t *a_ctx) override
        {
                emerixx::dirent_t dirent;

                kern_return_t retval = KERN_SUCCESS;

                switch (a_ctx->fs_offset)
                {
                case 0:
                        dirent            = m_dirent;
                        dirent.d_name[0]  = '.';
                        dirent.d_name[1]  = '\0';
                        dirent.d_namlen   = 1;
                        m_dirent.d_reclen = DENT_RECLEN(m_dirent.d_namlen);
                        dirent.d_off      = 0;

                        a_ctx->fs_offset = dirent.d_reclen;

                        retval = (this->*a_cb)(&dirent, a_ctx);

                        if (a_ctx->exit || KERN_STATUS_FAILURE(retval))
                        {
                                break;
                        }

                case DENT_RECLEN(1):
                        dirent.d_ino      = 1;
                        dirent.d_name[0]  = '.';
                        dirent.d_name[1]  = '.';
                        dirent.d_name[2]  = '\0';
                        dirent.d_namlen   = 2;
                        dirent.d_type     = IFTODT(m_fstat.st_mode);
                        dirent.d_creatino = 0;
                        m_dirent.d_reclen = DENT_RECLEN(m_dirent.d_namlen);
                        dirent.d_off      = dirent.d_reclen;

                        a_ctx->fs_offset += dirent.d_reclen;

                        retval = (this->*a_cb)(&dirent, a_ctx);

                        if (a_ctx->exit || KERN_STATUS_FAILURE(retval))
                        {
                                break;
                        }

                default:
                        retval = pfs_readdir(a_cb, a_ctx);
                }

                return retval;
        }

        kern_return_t fs_dir_initialize(fstat_t *a_parent_stat) override
        {
                panic("NI");
                return KERN_NOT_IN_SET;
        }
        kern_return_t fs_dir_reparent(fstat_t *a_parent_stat) override
        {
                panic("NI");
                return KERN_NOT_IN_SET;
        }
        kern_return_t fs_dir_empty() override
        {
                panic("NI");
                return KERN_NOT_IN_SET;
        }
        kern_return_t fs_readwrite_link(uio *a_uio) override
        {
                panic("NI");
                return KERN_NOT_IN_SET;
        }
        kern_return_t fs_readwrite_spec(file *a_file, uio *a_uio, int a_ioflag) override
        {
                panic("NI");
                return KERN_NOT_IN_SET;
        }
        kern_return_t fs_strategy(buffer_cache::io_descriptor *a_io_desc) override
        {
                panic("NI");
                return KERN_NOT_IN_SET;
        }
        kern_return_t fs_ioctl(file *a_file, int a_cmd, void *a_data, int a_fflag) override
        {
                panic("NI");
                return KERN_NOT_IN_SET;
        }
        kern_return_t fs_open(file *a_file, mode_t a_mode) override
        {
                panic("NI");
                return KERN_NOT_IN_SET;
        }
        kern_return_t fs_close(file *a_file) override
        {
                // panic("NI");
                return KERN_SUCCESS;
        }
        kern_return_t fs_poll(file *a_file, pollfd **a_pfd) override
        {
                panic("NI");
                return KERN_NOT_IN_SET;
        }
        kern_return_t fs_remove_dirent(emerixx::dirent_t *a_dent) override
        {
                panic("NI");
                return KERN_NOT_IN_SET;
        }
        kern_return_t fs_insert_dirent(emerixx::dirent_t *a_dent) override
        {
                panic("NI");
                return KERN_NOT_IN_SET;
        }

        kern_return_t fs_get_page(size_t a_offset, vm_prot_t a_prot, vm_page_t *a_vm_page_out) override
        {
                panic("NI");
                return KERN_NOT_IN_SET;
        }

        virtual kern_return_t get_child_node(ino_t a_ino, vnode **a_vnode) = 0;

      private:
        virtual kern_return_t pfs_readdir(fs_readdir_cb_t a_cb, fs_readdir_ctx_t *a_ctx) = 0;
};

typedef RBTREE_UNIQUE_KEY(procfs_node, m_sibling_link) procfs_node_children_map;

// ********************************************************************************************************************************
RBTREE_FIND_LESS(procfs_node_children_map, ino_t const &)(ino_t const &lhs, procfs_node &rhs)
// ********************************************************************************************************************************
{
        return (lhs < rhs.m_fstat.st_ino);
}

// ********************************************************************************************************************************
RBTREE_FIND_LESS(procfs_node_children_map, ino_t const &)(procfs_node &lhs, ino_t const &rhs)
// ********************************************************************************************************************************
{
        return (lhs.m_fstat.st_ino < rhs);
}

// ********************************************************************************************************************************
RBTREE_INSERT_LESS(procfs_node_children_map, procfs_node)(procfs_node &lhs, procfs_node &rhs)
// ********************************************************************************************************************************
{
        return (lhs.m_fstat.st_ino < rhs.m_fstat.st_ino);
}

struct procfs_root final : procfs_node
{
        procfs_node_children_map m_children;
        ino_t m_last_ino;

        procfs_root()
            : procfs_node("/")
        {
                m_fstat.st_ino   = 1;
                m_dirent.d_ino   = 1;
                m_fstat.st_nlink = 2;

                m_children.init();
                m_last_ino = 1;
        }

        ~procfs_root() {}

        kern_return_t pfs_readdir(fs_readdir_cb_t a_cb, fs_readdir_ctx_t *a_ctx) override
        {
                kern_return_t retval = KERN_SUCCESS;

                off_t doff = 0x30;

                for (procfs_node *n = m_children.first(); n != nullptr; n = procfs_node_children_map::next(n))
                {
                        if (doff < a_ctx->fs_offset)
                        {
                                continue;
                        }

                        retval = (this->*a_cb)(&n->m_dirent, a_ctx);

                        if (a_ctx->exit || KERN_STATUS_FAILURE(retval))
                        {
                                break;
                        }

                        doff += m_dirent.d_reclen;
                        a_ctx->fs_offset += m_dirent.d_reclen;
                }

                return retval;
        }

        kern_return_t get_child_node(ino_t a_ino, vnode **a_vnode) override
        {
                *a_vnode = m_children.find(a_ino);
                return *a_vnode != nullptr ? KERN_SUCCESS : KERN_FAIL(ENOENT);
        }

        kern_return_t register_node(procfs_node *a_node)
        {
                a_node->reference();

                a_node->m_dirent.d_ino = a_node->m_fstat.st_ino = ++m_last_ino;

                a_node->v_mount = v_mount;

                m_fstat.st_nlink++;
                m_children.insert(a_node);
                return KERN_SUCCESS;
        }
};

struct pci_root final : procfs_node
{
        pci_root()
            : procfs_node("pci")
        {
        }

        ~pci_root() {}

        kern_return_t pfs_readdir(fs_readdir_cb_t a_cb, fs_readdir_ctx_t *a_ctx) override
        {
                emerixx::dirent_t dirent;

                kern_return_t retval = KERN_SUCCESS;

                for (int bus = (a_ctx->fs_offset - 0x30) / DENT_RECLEN(2); bus < 0xFF; bus++)
                {
                        for (int slot = 0; slot < 0x20; ++slot)
                        {
                                for (int func = 0; func < 8; ++func)
                                {
                                        if (pci_cfgregread(bus, slot, func, PCIR_VENDOR, sizeof(uint16_t)) != 0xFFFF)
                                        {
                                                dirent.d_ino = bus << 16 | slot << 8 | func << 0;

                                                func = 8;
                                                slot = 0x20;

                                                snprintf(dirent.d_name, sizeof(dirent.d_name), "%02x", bus);

                                                dirent.d_namlen   = 2;
                                                dirent.d_type     = IFTODT(m_fstat.st_mode);
                                                dirent.d_creatino = 0;
                                                dirent.d_reclen   = DENT_RECLEN(2);

                                                dirent.d_off = dirent.d_reclen;

                                                a_ctx->fs_offset += dirent.d_reclen;

                                                retval = (this->*a_cb)(&dirent, a_ctx);

                                                if (a_ctx->exit || KERN_STATUS_FAILURE(retval))
                                                {
                                                        bus = 0xFF;
                                                        break;
                                                }
                                        }
                                }
                        }
                }

                return retval;
        }

        kern_return_t get_child_node(ino_t a_ino, vnode **a_vnode) override
        {
                int bus  = (a_ino >> 16) & 0xFF;
                int slot = (a_ino >> 8) & 0xFF;
                int func = (a_ino >> 0) & 0xFF;
                return KERN_FAIL(ENOENT);
        }
};

struct procfs final : mount
{
        procfs_root m_root;
        procfs(vnode::ptr &a_devvp, vnode::ptr &vncovered, integer_t flags)
            : mount(0, vncovered, "procfs", flags)
        {
                m_root.reference();
                m_root.v_mount = this;
        }

        ~procfs() {}

        int start(int flags, pthread_t p) override
        {
                panic("NI");
                return KERN_SUCCESS;
        }

        int unmount(int, pthread_t) override
        {
                panic("NI");
                return KERN_SUCCESS;
        }

        int root(struct vnode::ptr &vpp) override
        {
                vpp = &m_root;
                return KERN_SUCCESS;
        }

        int quotactl(int cmds, uid_t uid, char *arg, pthread_t p) override
        {
                panic("NI");
                return KERN_SUCCESS;
        }

        int statfs(struct statfs *sbp) override
        {
                panic("NI");
                return KERN_SUCCESS;
        }

        int sync(int waitfor) override
        {
                // panic("NI");
                return KERN_SUCCESS;
        }

        int vget(emerixx::dirent_t *a_dirent, vnode *a_parent, vnode **a_vnode_out) override
        {
                kern_return_t retval;

                KASSERT(a_parent != nullptr);

                if (a_parent->m_fstat.st_ino == a_dirent->d_ino)
                {
                        *a_vnode_out = nullptr;
                        retval       = KERN_SUCCESS;
                }
                else
                {
                        retval = static_cast<procfs_node *>(a_parent)->get_child_node(a_dirent->d_ino, a_vnode_out);
                }

                return retval;
        }

        void vnode_reclaim(vnode *vp) override { panic("NI"); }

        kern_return_t block_free(vnode *a_vnode, vm_offset_t a_offset, size_t a_count) override
        {
                panic("NI");
                return KERN_SUCCESS;
        }
};

pci_root pr;

static int procfs_init()
{
        vnode::ptr v1, v2;

        mount::lock_shared();
        g_procfsroot = new procfs(v1, v2, 0);
        mount::unlock_shared();

        static_cast<procfs *>(g_procfsroot)->m_root.register_node(&pr);

        return 0;
}

fs_initcall(procfs_init);