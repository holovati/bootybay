/*
 * Copyright (c) 1989, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)spec_vnops.c	8.6 (Berkeley) 4/9/94
 */
#include <emerixx/file.h>

#include <mach/buf.h>
#include <mach/conf.h>
#include <mach/disklabel.h>
#include <mach/kalloc.h>
#include <mach/param.h>
#include <vfs/mount.h>

#include <mach/specdev.h>
#include <mach/systm.h>
#include <mach/thread.h>
#include <mach/ucred.h>
#include <mach/unistd.h>
#include <strings.h>
#include <sys/stat.h>
#include <vfs/vfs.h>
#include <vfs/vnode.h>

#include <algorithm>
/* symbolic sleep message strings for devices */
static char devopn[]  = "devopn";
static char devio[]   = "devio";
static char devwait[] = "devwait";
static char devin[]   = "devin";
static char devout[]  = "devout";
static char devioc[]  = "devioc";
static char devcls[]  = "devcls";

/*
 * Trivial lookup routine that always fails.
 */
static int spec_lookup(struct vnode *a_dvp, struct vnode **a_vpp, struct componentname *a_cnp)
{
        a_vpp = NULL;
        return -ENOTDIR;
}

/*
 * Open a special file.
 */
static int spec_open(struct vnode *a_vp, mode_t a_mode, struct ucred *a_cred, pthread_ta_p)
{
        struct vnode *bvp, *vp = a_vp;
        dev_t bdev, dev        = vp->v_un.vu_specinfo->si_rdev;
        int maj = major(dev);
        int error;

        /*
         * Don't allow open if fs is mounted -nodev.
         */
        if (vp->v_mount && (vp->v_mount->mnt_flag & MNT_NODEV))
        {
                return (-ENXIO);
        }

        switch (vp->v_type)
        {
        case VCHR:
                if ((size_t)maj >= nchrdev)
                {
                        return (-ENXIO);
                }

#if 0
      /*Disable for now(Damir) F_WRITE issues */
      if (a_cred != FSCRED && (a_mode & F_WRITE)) {
        /*
         * When running in very secure mode, do not allow
         * opens for writing of any disk character devices.
         */
        if (securelevel >= 2 && isdisk(dev, VCHR)) {
          return (EPERM);
        }
        /*
         * When running in secure mode, do not allow opens
         * for writing of /dev/mem, /dev/kmem, or character
         * devices whose corresponding block devices are
         * currently mounted.
         */
        if (securelevel >= 1) {
          if ((bdev = chrtoblk(dev)) != NODEV && vfinddev(bdev, VBLK, &bvp) &&
              bvp->v_usecount > 0 && (error = vfs_mountedon(bvp))) {
            return (error);
          }

          if (iskmemdev(dev)) {
            return (EPERM);
          }
        }
      }
#endif
                vp->unlock();
                error = (*cdevsw[maj].d_open)(dev, a_mode, S_IFCHR, a_p);
                vp->lock();
                return (error);

        case VBLK:
                if ((u32)maj >= nblkdev)
                {
                        return -ENXIO;
                }
                /*
                 * When running in very secure mode, do not allow
                 * opens for writing of any disk block devices.
                 */
#if 0
      /* (Damir) Disable for now. What does FWRITE do */
      if (securelevel >= 2 && ap->a_cred != FSCRED && (ap->a_mode & FWRITE) &&
          isdisk(dev, VBLK)) {
        return (EPERM);
      }
#endif
                /*
                 * Do not allow opens of block devices that are
                 * currently mounted.
                 */
                if (error = vfs_mountedon(vp))
                {
                        return (error);
                }
                return ((*bdevsw[maj].d_open)(dev, a_mode, S_IFBLK, a_p));
        }
        return 0;
}

/*
 * Vnode op for read
 */
static int spec_read(struct vnode *a_vp, struct uio *a_uio, int a_ioflag, struct ucred *a_cred)
{
        struct vnode *vp = a_vp;
        struct uio *uio  = a_uio;
        pthread_t p      = uio->uio_procp;
        struct buf *bp;
        size_t bn, nextbn;
        long bsize, bscale;
        struct partinfo dpart;
        int n, on, majordev, (*ioctl)(dev_t dev, ioctl_cmd_t cmd, void *data, int fflag, pthread_t p);
        int error = 0;
        dev_t dev;

        if (uio->uio_rw != UIO_READ)
        {
                panic("spec_read mode");
        }

        if (uio->uio_segflg == UIO_USERSPACE && uio->uio_procp != proc::current())
        {
                panic("spec_read proc");
        }

        if (uio->uio_resid == 0)
        {
                return (0);
        }

        switch (vp->v_type)
        {
        case VCHR:
                vp->unlock();
                error = (*cdevsw[major(vp->v_un.vu_specinfo->si_rdev)].d_read)(vp->v_un.vu_specinfo->si_rdev, uio, a_ioflag);
                vp->lock();
                return error;

        case VBLK:
                if (uio->uio_offset < 0)
                {
                        return -EINVAL;
                }

                bsize = BLKDEV_IOSIZE;
                dev   = vp->v_un.vu_specinfo->si_rdev;
                if ((majordev = major(dev)) < nblkdev && (ioctl = bdevsw[majordev].d_ioctl) != NULL
                    && (*ioctl)(dev, DIOCGPART, &dpart, FREAD, p) == 0 && dpart.part->p_fstype == FS_BSDFFS
                    && dpart.part->p_frag != 0 && dpart.part->p_fsize != 0)
                {
                        bsize = dpart.part->p_frag * dpart.part->p_fsize;
                }

                bscale = bsize / DEV_BSIZE;
                do
                {
                        bn = (uio->uio_offset / DEV_BSIZE) & ~(bscale - 1);
                        on = uio->uio_offset % bsize;
                        n  = std::min((size_t)(bsize - on), (size_t)(uio->uio_resid));
                        if (vp->v_lastr + bscale == bn)
                        {
                                nextbn = bn + bscale;
                                error  = breadn(vp, bn, (int)bsize, &nextbn, (int *)&bsize, 1, NOCRED, &bp);
                        }
                        else
                                error = bread(vp, bn, (int)bsize, NOCRED, &bp);
                        vp->v_lastr = bn;
                        n           = std::min((size_t)n, (size_t)(bsize - bp->b_resid));
                        if (error)
                        {
                                brelse(bp);
                                return (error);
                        }
                        error = uiomove((char *)bp->b_addr + on, n, uio);
                        if (n + on == bsize)
                                bp->b_flags |= B_AGE;
                        brelse(bp);
                } while (error == 0 && uio->uio_resid > 0 && n != 0);
                return (error);

        default:
                panic("spec_read type");
        }
        /* NOTREACHED */
        return -1; // Silence warning
}

/*
 * Vnode op for write
 */
static int spec_write(struct vnode *a_vp, struct uio *a_uio, int a_ioflag, struct ucred *a_cred)
{
        struct vnode *vp = a_vp;
        struct uio *uio  = a_uio;
        pthread_t p      = uio->uio_procp;
        struct buf *bp;
        size_t bn;
        int bsize, blkmask;
        struct partinfo dpart;
        int n, on;
        int error = 0;

        if (uio->uio_rw != UIO_WRITE)
        {
                panic("spec_write mode");
        }

        if (uio->uio_segflg == UIO_USERSPACE && uio->uio_procp != proc::current())
        {
                panic("spec_write proc");
        }

        switch (vp->v_type)
        {
        case VCHR:
                vp->unlock();
                error = (*cdevsw[major(vp->v_un.vu_specinfo->si_rdev)].d_write)(vp->v_un.vu_specinfo->si_rdev, uio, a_ioflag);
                vp->lock();
                return error;

        case VBLK:
                if (uio->uio_resid == 0)
                {
                        return (0);
                }

                if (uio->uio_offset < 0)
                {
                        return -EINVAL;
                }

                bsize = BLKDEV_IOSIZE;
                if ((*bdevsw[major(vp->v_un.vu_specinfo->si_rdev)].d_ioctl)(
                        vp->v_un.vu_specinfo->si_rdev, DIOCGPART, &dpart, FREAD, p)
                    == 0)
                {
                        if (dpart.part->p_fstype == FS_BSDFFS && dpart.part->p_frag != 0 && dpart.part->p_fsize != 0)
                        {
                                bsize = dpart.part->p_frag * dpart.part->p_fsize;
                        }
                }

                blkmask = (bsize / DEV_BSIZE) - 1;

                do
                {
                        bn = (uio->uio_offset / DEV_BSIZE) & ~blkmask;
                        on = uio->uio_offset % bsize;
                        n  = std::min((size_t)(bsize - on), (size_t)(uio->uio_resid));
                        if (n == bsize)
                        {
                                bp = getblk(vp, bn, bsize, 0, 0);
                        }
                        else
                        {
                                error = bread(vp, bn, bsize, NOCRED, &bp);
                        }

                        n = std::min((size_t)n, (size_t)(bsize - bp->b_resid));

                        if (error)
                        {
                                brelse(bp);
                                return (error);
                        }

                        error = uiomove((char *)bp->b_addr + on, n, uio);

                        if (n + on == bsize)
                        {
                                bp->b_flags |= B_AGE;
                                bawrite(bp);
                        }
                        else
                        {
                                bdwrite(bp);
                        }
                } while (error == 0 && uio->uio_resid > 0 && n != 0);
                return error;

        default:
                panic("spec_write type");
        }
        /* NOTREACHED */
        return -1; // Silence warning
}

/*
 * Device ioctl operation.
 */
static int spec_ioctl(struct vnode *a_vp, int a_command, char *a_data, int a_fflag, struct ucred *a_cred, pthread_ta_p)
{
        dev_t dev = a_vp->v_un.vu_specinfo->si_rdev;

        switch (a_vp->v_type)
        {
        case VCHR:
                return (*cdevsw[major(dev)].d_ioctl)(dev, a_command, a_data, a_fflag, a_p);

        case VBLK:
#if 0
      /* (Damir) We don't support tape drives for now */
      if (a_command == 0 && (natural_t)ap->a_data == B_TAPE) {
        return !((bdevsw[major(dev)].d_flags & B_TAPE) > 0);
      }
#endif
                return (*bdevsw[major(dev)].d_ioctl)(dev, a_command, a_data, a_fflag, a_p);

        default:
                panic("spec_ioctl");
                /* NOTREACHED */
        }
        return -1; // Silence warning
}

static int spec_select(struct vnode *a_vp, int a_which, int a_fflags, struct ucred *a_cred, pthread_ta_p)
{
        dev_t dev;

        switch (a_vp->v_type)
        {
        default:
                return (1); /* XXX */

        case VCHR:
                dev = a_vp->v_un.vu_specinfo->si_rdev;
                return (*cdevsw[major(dev)].d_select)(dev, a_which, a_p);
        }
}
/*
 * Synch buffers associated with a block device
 */
static int spec_fsync(struct vnode *a_vp, struct ucred *a_cred, int a_waitfor, pthread_ta_p)
{
        struct vnode *vp = a_vp;
        struct buf *bp;
        struct buf *nbp;
        int s;

        if (vp->v_type == VCHR)
        {
                return (0);
        }
        /*
         * Flush all dirty buffers associated with a block device.
         */
loop:
        s = splbio();
        for (bp = vp->v_dirtyblkhd.lh_first; bp; bp = nbp)
        {
                nbp = bp->b_vnbufs.le_next;

                if ((bp->b_flags & B_BUSY))
                {
                        continue;
                }

                if ((bp->b_flags & B_DELWRI) == 0)
                {
                        panic("spec_fsync: not dirty");
                }

                bremfree(bp);
                bp->b_flags |= B_BUSY;
                splx(s);
                bawrite(bp);
                goto loop;
        }

        if (a_waitfor == MNT_WAIT)
        {
                while (vp->v_numoutput)
                {
                        vp->v_flag |= VBWAIT;
                        sleep(&vp->v_numoutput, PRIBIO + 1);
                }
                if (vp->v_dirtyblkhd.lh_first)
                {
                        log_info("spec_fsync: dirty", vp);
                        goto loop;
                }
        }
        splx(s);
        return (0);
}

/*
 * Just call the device strategy routine
 */
static int spec_strategy(struct buf *a_bp)
{
        (*bdevsw[major(a_bp->b_dev)].d_strategy)(a_bp);
        return (0);
}

/*
 * This is a noop, simply returning what one has been given.
 */
static int spec_bmap(struct vnode *a_vp, size_t a_bn, struct vnode **a_vpp, size_t *a_bnp)
{
        if (a_vpp != NULL)
        {
                *a_vpp = a_vp;
        }

        if (a_bnp != NULL)
        {
                *a_bnp = a_bn;
        }
        return (0);
}

/*
 * At the moment we do not do any locking.
 */
/* ARGSUSED */
static int spec_lock(struct vnode *a_vp) { return (0); }

/* ARGSUSED */
static int spec_unlock(struct vnode *a_vp) { return (0); }

/*
 * Device close routine
 */
/* ARGSUSED */
static int spec_close(struct vnode *a_vp, int a_fflag, struct ucred *a_cred, pthread_ta_p)
{
        struct vnode *vp = a_vp;
        dev_t dev        = vp->v_un.vu_specinfo->si_rdev;
        int (*devclose)(dev_t);
        int mode, error;

        switch (vp->v_type)
        {
        case VCHR:
                /*
                 * Hack: a tty device that is a controlling terminal
                 * has a reference from the session structure.
                 * We cannot easily tell that a character device is
                 * a controlling terminal, unless it is the closing
                 * process' controlling terminal.  In that case,
                 * if the reference count is 2 (this last descriptor
                 * plus the session), release the reference from the session.
                 */
                if (vcount(vp) == 2 && a_p && vp == a_p->p_pgrp->pg_session->s_ttyvp)
                {
                        vnode::unreference(vp);
                        a_p->p_pgrp->pg_session->s_ttyvp = NULL;
                }
                /*
                 * If the vnode is locked, then we are in the midst
                 * of forcably closing the device, otherwise we only
                 * close on last reference.
                 */
                panic("vxlock is not what it used to be");
                if (vcount(vp) > 1 && (vp->v_flag & VXLOCK) == 0)
                {
                        return (0);
                }

                devclose = cdevsw[major(dev)].d_close;
                mode     = S_IFCHR;
                break;

        case VBLK:
                /*
                 * On last close of a block device (that isn't mounted)
                 * we must invalidate any in core blocks, so that
                 * we can, for instance, change floppy disks.
                 */
                if (error = vinvalbuf(vp, V_SAVE, a_cred, a_p, 0, 0))
                {
                        return (error);
                }
                /*
                 * We do not want to really close the device if it
                 * is still in use unless we are trying to close it
                 * forcibly. Since every use (buffer, vnode, swap, cmap)
                 * holds a reference to the vnode, and because we mark
                 * any other vnodes that alias this device, when the
                 * sum of the reference counts on all the aliased
                 * vnodes descends to one, we are on last close.
                 */
                panc("NONO VXLOCK is not what it used to be");
                if (vcount(vp) > 1 && (vp->v_flag & VXLOCK) == 0)
                {
                        return (0);
                }

                devclose = bdevsw[major(dev)].d_close;
                mode     = S_IFBLK;
                break;

        default:
                panic("spec_close: not special");
        }

        return ((*devclose)(dev /*, a_fflag, mode, a_p*/));
}

/*
 * Print out the contents of a special device vnode.
 */
static int spec_print(struct vnode *a_vp)
{
        log_info("tag VT_NON, dev %d, %d", major(a_vp->v_un.vu_specinfo->si_rdev), minor(a_vp->v_un.vu_specinfo->si_rdev));
        return 0;
}

/*
 * Return POSIX pathconf information applicable to special devices.
 */
static int spec_pathconf(struct vnode *a_vp, int a_name, int *a_retval)
{
        switch (a_name)
        {
        case _PC_LINK_MAX:
                *a_retval = LINK_MAX;
                return (0);
        case _PC_MAX_CANON:
                *a_retval = MAX_CANON;
                return (0);
        case _PC_MAX_INPUT:
                *a_retval = MAX_INPUT;
                return (0);
        case _PC_PIPE_BUF:
                *a_retval = PIPE_BUF;
                return (0);
        case _PC_CHOWN_RESTRICTED:
                *a_retval = 1;
                return (0);
        case _PC_VDISABLE:
                *a_retval = _POSIX_VDISABLE;
                return (0);
        default:
                return -EINVAL;
        }
        /* NOTREACHED */
}

/*
 * Special device advisory byte-level locks.
 */
static int spec_advlock(struct vnode *a_vp, char *a_id, int a_op, struct flock *a_fl, int a_flags) { return -EOPNOTSUPP; }

// Finish this at some time, when doing devfs
static int spec_getattr(struct vnode *a_vp, vnode::attributes *a_vap, struct ucred *a_cred, pthread_ta_p)
{
        int result;
        bzero(a_vap, sizeof *a_vap);

        switch (a_vp->v_type)
        {
        case VCHR:
                a_vap->va_type      = VCHR;
                a_vap->va_blocksize = 1024;
                a_vap->va_bytes     = 0;
                result              = 0;
                break;
        default:
                panic("Check this");
                result = -EBADF;
                break;
        }

        return result;
}

static int spec_setattr(struct vnode *a_vp, vnode::attributes *a_vap, struct ucred *a_cred, pthread_ta_p) { return -EBADF; }

static int spec_access(struct vnode *a_vp, mode_t a_mode, struct ucred *a_cred, pthread_ta_p) { return -EBADF; }

int spec_create(struct vnode *a_dvp, struct vnode **a_vpp, struct componentname *a_cnp, vnode::attributes *a_vap) { return -EBADF; }

static int spec_remove(struct vnode *a_dvp, struct vnode *a_vp, struct componentname *a_cnp)
{
        return -1; // BADOP
}

static int spec_link(struct vnode *a_vp, struct vnode *a_tdvp, struct componentname *a_cnp) { return -1; }

static int spec_rename(struct vnode *a_fdvp,
                       struct vnode *a_fvp,
                       struct componentname *a_fcnp,
                       struct vnode *a_tdvp,
                       struct vnode *a_tvp,
                       struct componentname *a_tcnp)
{
        return -1;
}

static int spec_mkdir(struct vnode *a_dvp, struct vnode **a_vpp, struct componentname *a_cnp, vnode::attributes *a_vap)
{
        return -1;
}

static int spec_rmdir(struct vnode *a_dvp, struct vnode *a_vp, struct componentname *a_cnp) { return -1; }

static int spec_readlink(struct vnode *a_vp, struct uio *a_uio, struct ucred *a_cred) { return -1; }

static int spec_symlink(
    struct vnode *a_dvp, struct vnode **a_vpp, struct componentname *a_cnp, vnode::attributes *a_vap, char *a_target)
{
        return -1;
}

static int spec_readdir(struct vnode *a_vp, struct uio *a_uio, struct ucred *a_cred, int *a_eofflag, u32 *a_cookies, int a_ncookies)
{
        return -1;
}

static int spec_seek(struct vnode *a_vp, off_t a_oldoff, off_t a_newoff, struct ucred *a_cred) { return -1; }

static int spec_mknod(struct vnode *a_dvp, struct vnode **a_vpp, struct componentname *a_cnp, vnode::attributes *a_vap)
{
        return -1;
}

static int spec_mmap(struct vnode *a_vp, int a_fflags, struct ucred *a_cred, pthread_ta_p) { return -1; }

static int spec_abortop(struct vnode *a_dvp, struct componentname *a_cnp) { return -1; }

static int spec_bmap(struct vnode *a_vp, size_t a_bn, struct vnode **a_vpp, size_t *a_bnp, int *a_runp) { return -1; }

static int spec_blkatoff(struct vnode *a_vp, off_t a_offset, char **a_res, struct buf **a_bpp) { return -1; }

static int spec_valloc(struct vnode *a_pvp, int a_mode, struct ucred *a_cred, struct vnode **a_vpp) { return -1; }

static int spec_vfree(struct vnode *a_pvp, ino_t a_ino, int a_mode) { return -1; }

static int spec_truncate(struct vnode *a_vp, off_t a_length, int a_flags, struct ucred *a_cred, pthread_ta_p) { return 0; }

static int spec_update(struct vnode *a_vp, struct timeval *a_ta, struct timeval *a_tm, int a_waitfor) { return 0; }

static int spec_bwrite(struct buf *a_bp) { return 0; }

static int spec_nullop(struct vnode *) { return 0; }

vnodeops_t spec_vnodeop = {
    .open     = spec_open,
    .close    = spec_close,
    .read     = spec_read,
    .write    = spec_write,
    .ioctl    = spec_ioctl,
    .getattr  = spec_getattr,
    .setattr  = spec_setattr,
    .access   = spec_access,
    .lookup   = spec_lookup,
    .create   = spec_create,
    .remove   = spec_remove,
    .link     = spec_link,
    .rename   = spec_rename,
    .mkdir    = spec_mkdir,
    .rmdir    = spec_rmdir,
    .readdir  = spec_readdir,
    .symlink  = spec_symlink,
    .readlink = spec_readlink,
    .fsync    = spec_fsync,
    .inactive = spec_nullop,
    .seek     = spec_seek,
    .mknod    = spec_mknod,
    .select   = spec_select,
    .mmap     = spec_mmap,
    .abortop  = spec_abortop,
    .reclaim  = spec_nullop,
    .lock     = spec_lock,
    .unlock   = spec_unlock,
    .bmap     = spec_bmap,
    .strategy = spec_strategy,
    .print    = spec_print,
    .islocked = spec_nullop,
    .pathconf = spec_pathconf,
    .advlock  = spec_advlock,
    .blkatoff = spec_blkatoff,
    .valloc   = spec_valloc,
    .vfree    = spec_vfree,
    .truncate = spec_truncate,
    .update   = spec_update,
    .bwrite   = spec_bwrite
    /* END */
};
