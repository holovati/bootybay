#pragma once
#include <fcntl.h>

#define FCNTLFLAGS (FAPPEND | FASYNC | FFSYNC | FNONBLOCK)

#define F_WAIT  0x010 /* Wait until lock is granted */
#define F_FLOCK 0x020 /* Use flock(2) semantics for lock */
#define F_POSIX 0x040 /* Use POSIX semantics for lock */

#define FHASLOCK 0x4000 /* descriptor holds advisory lock */

#define LOCK_SH 0x01 /* shared file lock */
#define LOCK_EX 0x02 /* exclusive file lock */
#define LOCK_NB 0x04 /* don't block when locking */
#define LOCK_UN 0x08 /* unlock file */
