#include <emerixx/process.h>
#include <fcntl.h>
#include <fs/specfs.h>
#include <mach/conf.h>
#include <sys/ioctl.h>

#if 0
ioctl(3, TCGETS, {
        c_iflags = 0x500, c_oflags = 0x5, c_cflags = 0xbf, c_lflags = 0x8a3b, c_line = 0,
        c_cc = "\x03\x1c\x7f\x15\x04\x00\x01\x00\x11\x13\x1a\x00\x12\x0f\x17\x16\x00\x00\x00"}
#endif

static kern_return_t pts_open(file *a_file, dev_t dev, int flag, int mode)
{
        tty *tp = &s_ptys[minor(dev)].pt;

        if ((tp->t_state & TS_ISOPEN) == 0)
        {
                tp->t_state |= TS_WOPEN;
                ttychars(tp); /* Set up default chars */
                tp->t_iflag  = TTYDEF_IFLAG;
                tp->t_oflag  = TTYDEF_OFLAG;
                tp->t_lflag  = TTYDEF_LFLAG;
                tp->t_cflag  = TTYDEF_CFLAG;
                tp->t_ispeed = tp->t_ospeed = TTYDEF_SPEED;
                ttsetwater(tp); /* would be done in xxparam() */
        }
        else if (tp->t_state & TS_XCLUDE /*&& p->p_ucred->cr_uid != 0*/)
        {
                KASSERT(0);
                return -EBUSY;
        }

        if (tp->t_oproc) /* Ctrlr still around. */
        {
                tp->t_state |= TS_CARR_ON;
        }

        kern_return_t retval = KERN_SUCCESS;
        while ((tp->t_state & TS_CARR_ON) == 0)
        {
                tp->t_state |= TS_WOPEN;
                if (flag & O_NONBLOCK)
                {
                        break;
                }

                if (retval = ttysleep(tp, &tp->t_rawq, TTIPRI | PCATCH, ttopen, 0))
                {
                        panic("Untested");
                        return retval;
                }
        }

        if (((flag & O_NOCTTY) == 0) && utask_self()->m_ctty == makedev(0xff, 0xff))
        {
                ttioctl(tp, TIOCSCTTY, nullptr, 0);
        }

        retval = (*linesw[tp->t_line].l_open)(dev, tp);

        a_file->m_private = &s_ptys[minor(dev)];

        poll_wakeup(&s_ptys[minor(dev)].pollctx, POLLOUT | POLLWRNORM | POLLIN | POLLRDNORM);
        uthread_wakeup(&tp->t_outq.c_cf); // MASTER READ
        uthread_wakeup(&tp->t_rawq.c_cf); // MASTER WRITE

        return retval;
}

static kern_return_t pts_close(file *a_file, dev_t dev)
{
        pty *p = ((pty *)a_file->m_private);
        log_error("PTY Slave closed %d", p->slaveno);
        return KERN_SUCCESS;
}

static kern_return_t pts_read(file *a_file, dev_t dev, struct uio *uio, int flag)
{
        tty *tp = &((pty *)a_file->m_private)->pt;

        kern_return_t retval = (*linesw[tp->t_line].l_read)(tp, uio, flag);
        uthread_wakeup(&tp->t_rawq.c_cf); // MASTER WRITE
        poll_wakeup(&((pty *)a_file->m_private)->pollctx, POLLOUT | POLLWRNORM);
        return retval;
}

static kern_return_t pts_write(file *a_file, dev_t dev, struct uio *uio, int flag)
{
        tty *tp = &((pty *)a_file->m_private)->pt;

        if (tp->t_oproc == nullptr)
        {
                return KERN_FAIL(EIO);
        }

        return ((*linesw[tp->t_line].l_write)(tp, uio, flag));
}

static kern_return_t pts_ioctl(file *a_file, dev_t dev, int cmd, char *addr, int flag)
{
        kern_return_t retval = KERN_SUCCESS;
        tty *tp              = &((pty *)a_file->m_private)->pt;

        int err = (*linesw[tp->t_line].l_ioctl)(a_file, tp, cmd, addr, flag);

        if (err >= 0)
        {
                return err;
        }

        switch (cmd)
        {
        default:
                retval = ttioctl(tp, cmd, addr, 0);
                break;
        }

        return retval;
}

static kern_return_t pts_poll(file *a_file, dev_t dev, pollfd **a_pfd)
{
        tty *tp = &((pty *)a_file->m_private)->pt;
        return ttpoll(tp, a_pfd);
}

static kern_return_t pts_stop(file *a_file, struct tty *tp, int rw)
{
        panic("stop");
        return KERN_FAIL(ENOTTY);
}

static struct cdevsw s_pts
{
        .d_open      = pts_open,  //
            .d_close = pts_close, //
            .d_read  = pts_read,  //
            .d_write = pts_write, //
            .d_ioctl = pts_ioctl, //
            .d_poll  = pts_poll,  //
            .d_stop  = pts_stop,  //
            .d_tty   = nullptr    //
                                  /* End */
};

static kern_return_t pts_init()
{
        KASSERT(specfs_register_character_device(makedev(19, 0), 0xFF, &s_pts) == KERN_SUCCESS);
        return KERN_SUCCESS;
}

device_initcall(pts_init);
