#pragma once
#include <mach/machine/bitops.h>

#ifdef __cplusplus
namespace emerixx {
namespace bitops {
static inline int ffs(int x) { return __builtin_ffs(x); }
static inline int ffs(long x) { return __builtin_ffsl(x); }
static inline int ffs(long long x) { return __builtin_ffsll(x); }

static inline int popcount(unsigned int x) { return __builtin_popcount(x); }
static inline int popcount(unsigned long x) { return __builtin_popcountl(x); }
static inline int popcount(unsigned long long x) {
  return __builtin_popcountll(x);
}

static inline u8 test_bit(u8 *addr, size_t bit) {
  return (u8)(*(addr + (bit >> 3)) & (1 << (bit & 7)));
}

static inline u8 set_bit(u8 *addr, size_t bit) {
  u8 old = test_bit(addr, bit);

  *(addr + (bit >> 3)) = (u8)(*(addr + (bit >> 3)) | (1 << (bit & 7)));
  return old;
}

static inline u8 clear_bit(u8 *addr, size_t bit) {
  u8 old = test_bit(addr, bit);

  *(addr + (bit >> 3)) = (u8)(*(addr + (bit >> 3)) & ~(1 << (bit & 7)));
  return old;
}

static inline size_t find_first_zero_bit(void *addr, size_t size) {
  size_t res = 0;
  u8 *begin  = (u8 *)addr;
  u8 *end    = begin + (size >> 3);

  for (u8 *buf = begin; buf < end; ++buf) {
    if (*buf == UINT8_MAX) {
      res += 8;
      continue;
    }

    for (int i = 0; i < 8; ++i) {
      if ((*buf & (1 << i)) == 0) {
        buf = end;
        break;
      }
      res++;
    }
  }

  return res;
}

static inline size_t find_next_zero_bit(void *addr,
                                        size_t size,
                                        size_t offset) {
  u8 *frstbyte = (u8 *)addr + (offset >> 3);

  for (int i = offset & 7; i < 8; ++i) {
    if (((*frstbyte) & (1 << i)) == 0) {
      return offset + (size_t)i;
    }
    size--;
  }

  return (((size_t)(frstbyte - ((u8 *)addr))) * 8)
         + find_first_zero_bit(frstbyte, size);
}

}  // namespace bitops
}  // namespace emerixx

#endif  //__cplusplus