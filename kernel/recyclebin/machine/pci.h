#pragma once
#include <rs64/driver.h>

#include "pci_config.h"
// clang-format off
/*
https://docs.microsoft.com/en-us/archive/blogs/ntdebugging/determining-the-interrupt-line-for-a-particular-pci-e-slot

The process is essentially:

1) Initialize the namespace (\\_SB_._INI) followed by all devices whose _STA method reports they are present and/or functional.

2) Execute \\_PIC(1) to inform ACPI that you are using the IOAPIC (this is required otherwise it will probably give you the wrong IRQ routing tables).

3) Find the PCI root bridge (_HID = EisaId(PNP0A03))

4) Execute its _PRT method. This will return a Package object (which is ACPI-speak for an array) containing many other package objects. 
   Iterate through each of them and the first entry is the PCI device number in the format (dev_num << 16) | 0xffff. 
   The second is the PCI pin number (1 = INTA#, 2 = INTB#, 3 = INTC#, 4 = INTD#). 
   You then need to match them up with what you found during PCI enumeration (e.g. if device 5 has the interrupt pin entry of its PCI configuration space being 3, you'd look for an entry in the _PRT response with the first item being 0x0005ffff and the second being 0x3). 
   Look up the _PRT method in the spec for further info.

5) Once you've found the correct method, then examine the third entry of the _PRT response for that device:
5a) If it is an Integer with value Zero, then the 4th entry is an Integer whose value is the Global System Interrupt that device uses. Once you know this, you find which IOAPIC it points to and the pin on that IOAPIC. 
    To do this, iterate through the MADT table looking for an IOAPIC with its gsibase field being less than and within 24 of the value you're looking for. Typically you have only one IOAPIC with gsibase being zero. 
    Thus a GSI of 16 means pin 16 on IOAPIC 1.

5b) If it is a string, it is the name of another object in the namespace (a PCI IRQ Link object, e.g. \\_SB_\LNKA). You need to execute the _CRS method of this object to find out what interrupt it uses. You will want to read the spec to see the layout of the response. This will typically give you an ISA IRQ to use, and you will have to parse MADT again looking for InterruptSourceOverride structures that will convert this ISA IRQ to a GSI. If there is none, then GSI = ISA IRQ.

6) Choose a free interrupt vector in a particular CPU's IDT and program the handler into it.

7) Program the appropriate pin on the appropriate IOAPIC to route to the particular vector on the particular processor.

On Sat, 28 Jul 2012 08:29:31 -0800 Chen Tian (CT) wrote:

CT> So I should use the interrupt pin field at offset 0x3c to find out which
CT> PCI link the device uses.

Yes, but the offset of the interrupt pin ix 0x3d.

CT> Can I use this value directly to search IO-APIC
CT> GSI in _PRT blocks?  I read some article that says this value may be local
CT> to PCI device, and it may be remapped by motherboard to another PCI link
CT> number through something like daisy chain type of mapping. For example, PCI
CT> slot 1's INTA# uses INTA#,  while PCI slot 2's INTA# actually uses INTB#.
CT>  Is this true? If so,  I think I need to handle this mapping before search
CT> _PRT blocks, right?

The _PRT method is defined in the ACPI specification as follows:

Field   Type     Description
Address DWORD    The address of the device (uses the same format as _ADR).
Pin     BYTE     The PCI pin number of the device (0­INTA, 1­INTB, 2­INTC, 3­INTD).
Source  NamePath Name of the device that allocates the interrupt to which the above pin is
        Or       connected. The name can be a fully qualified path, a relative path, or a simple
        BYTE     name segment that utilizes the namespace search rules. Note: This field is a
                 NamePath and not a String literal, meaning that it should not be surrounded by
                 quotes. If this field is the integer constant Zero (or a BYTE value of 0), then the
                 interrupt is allocated from the global interrupt pool.
Source  DWORD    Index that indicates which resource descriptor in the resource template of the
Index            device pointed to in the Source field this interrupt is allocated from. If the
                 Source field is the BYTE value zero, then this field is the global system
                 interrupt number to which the pin is connected.

Once you've found the PCI device in the ACPI namespace, you need to look at
the "Source" field to see which upstream device a PCI pin is connected to
and "Source Index" tells you where it is connected. You then need to do the
same for the upstream device, and so forth. For example, you may find that
INTA is connected to INTC upstream, and INTC is connected to INTB even
further upstream. Once you find that the "Source" field is 0, the "Source
Index" is the GSI number that you are looking for.

CT> For GSI number, I assume it is the irq number I am looking for. But I still
CT> have some questions. First, is it always fixed for each pin of each
CT> IO-APIC? For example, first pin of first IO-APIC has GSI 0, second pin has
CT> 1, ...,  and the first pin of second IO-APIC has GSI 24, second pin has 25,
CT> ...? If so, then irq number will range from 0 to 47 on a two-io-apic
CT> machine. But Linux runs on the same machine can report irq 53 for some
CT> device, which is very confusing to me.

The ACPI MADT table lists all the IOAPICs in the system. Each IOAPIC has a
certain number of pins, which you can read out from the IOAPIC itself. The
first IOAPIC serves GSI 0 through X-1, where X is the number of pins. The
second IOAPIC then serves pins X through X + (Y-1), where Y is the number of
pins of the second IOAPIC and so forth.

CT> Second, some document says GSI number can be overridden. When booting
CT> Fiasco.OC, I can see a line like ovr[0] 2->0.  Does that mean GSI 2
CT> eventually become irq 0?  I am not sure when and why a GSI number needs to
CT> be overridden, but  can the overriding happen to the device I am
CT> interested? If so, how should I deal with it in my device driver, i.e.,
CT> should I use the original GSI number as my device's irq or use the one
CT> after overriding?

The legacy IRQs are also connected to the IOAPIC, typically in a 1:1
fashion. This means IRQx is connected to GSIx, except where overrides are
used. This is only relevant if you want to drive a non-PCI legacy device,
such as the PIT, which uses IRQ0, which is typically connected to GSI2.

Cheers,
Udo
*/
// clang-format on
class c_pci_dev;

struct PciDeviceId {
  u32 vendor;
  u32 devcie;
};

class PciDriver : public Driver {
 public:
  virtual ~PciDriver()                        = default;
  virtual int Probe(c_pci_dev &device)        = 0;
  virtual const PciDeviceId *GetDeviceTable() = 0;
};

int PciRegisterDriver(PciDriver *driver);

template <typename DriverType>
static int PciRegisterDriver() {
  PciDriver *driver = nullptr;
  int error = 0, n_attached = 0;

  while (!(error < 0)) {
    driver = new DriverType();
    error  = PciRegisterDriver(driver);

    n_attached += error >= 0;
  }

  if (error) {
    delete driver;
  }

  return n_attached > 0 ? 0 : error;
}

class c_pci_dev : public Device {
 public:
  enum class command : u16 {
    iose = (1 << 0),  // I/O Space Enable
    mse  = (1 << 1),  // Memory Space Enable
    bme  = (1 << 2),  // Bus Master Enable
    sce  = (1 << 3),  // Special Cycle Enable
    mwie = (1 << 4),  // Memory Write and Invalidate Enable
    vga  = (1 << 5),  // VGA Palette Snooping Enable
    pee  = (1 << 6),  // Parity Error Response Enable
    wee  = (1 << 7),  // Wait Cycle Enable
    see  = (1 << 8),  // SERR# Enable
    fbe  = (1 << 9),  // Fast Back-to-Back Enable
    id   = (1 << 10)  // Interrupt Disable
  };

  enum class status : u16 {
    is   = (1 << 3),   // Interrupt Status
    cl   = (1 << 4),   // Capabilities List
    c66  = (1 << 5),   // 66 MHz Capable
    fbc  = (1 << 7),   // Fast Back-to-Back Capable
    dpd  = (1 << 8),   // Master Data Pariy Error Detected
    devt = (3 << 9),   // DEVSEL# Timing
    sta  = (1 << 11),  // Signaled Target-Abort
    rta  = (1 << 12),  // Received Target Abort
    rma  = (1 << 13),  // Received Master-Abort
    sse  = (1 << 14),  // Signaled System Error
    dpe  = (1 << 15)   // Detected Parity Error
  };

  enum class dev_type {
    device             = 0,
    pci2pci_bridge     = 1,
    pci2cardbus_bridge = 2
  };

  u32 bus() { return bus_; }
  u32 slot() { return slot_; }
  u32 function() { return func_; }
  c_pci_dev(i32 bus, i32 slot, i32 func)
      : c_pci_dev((u32)bus, (u32)slot, (u32)func) {}

  c_pci_dev(u32 bus, u32 slot, u32 func)
      : bus_(bus)
      , slot_(slot)
      , func_(func) {}

  template <typename T>
  T config_read(u32 reg) {
    return pci_config_read<T>(bus_, slot_, func_, reg);
  }

  template <typename T>
  void config_write(u32 reg, T val) {
    return pci_config_write<T>(bus_, slot_, func_, reg, val);
  }

  u16 vendor();
  u16 device();

  bool cmd_query(command cmd);
  void cmd_toggle(command cmd);

  u8 sts_query(status s);

  // Revision ID
  u8 rid();

  dev_type htype();

  // Is Multi-Function Device
  bool is_mfd();

  bool IsPciBridge() { return htype() == dev_type::pci2pci_bridge; }

  u8 interrupt_line();
  void SetInterruptLine(u32 line);

  u8 interrupt_pin();

  bool bus_master_enabled() { return cmd_query(command::bme); }
  void bus_master_toggle() { cmd_toggle(command::bme); }

 private:
  u32 bus_;
  u32 slot_;
  u32 func_;
  u32 pad_;
};

struct acpi_pci_routing_table;
class PciBridge : public c_pci_dev {
 public:
  PciBridge(u32 bus, u32 slot, u32 func, PciBridge *parent)
      : c_pci_dev(bus, slot, func)
      , parent_(parent)
      , prt_(nullptr) {}

  ~PciBridge();

  acpi_pci_routing_table *GetRoutingTable();

  u32 GetSubordinateBusNumber() { return config_read<u8>(0x18); }
  u32 GetSecondaryBusNumber() { return config_read<u8>(0x19); }
  u32 GetPrimaryBusNumber() { return config_read<u8>(0x1A); }

  PciBridge *GetParent() { return parent_; }

 private:
  PciBridge *parent_;
  acpi_pci_routing_table *prt_;
};