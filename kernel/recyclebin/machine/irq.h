#pragma once

#include <machine/task_control_block.h>  // TaskControlBlock
#include <rs64/driver.h>

typedef bool (*irq_handler_t)(TaskControlBlock **context, void *user_data);

using ExceptionHandler = void (*)();

void irq_register_exception_handler(size_t vec, ExceptionHandler handler);

void InterruptRequestRegisterDriver(Driver *driver);
