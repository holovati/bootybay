#ifndef INTRIN_H
#define INTRIN_H
#ifdef __cplusplus
extern "C" {
#endif

static inline u64 __readgsqword(u64 Offset) {
  u64 value;
  asm volatile("mov %0, gs:[%1]" : "=r"(value) : "r"(Offset) :);
  return value;
}

#ifdef __cplusplus
}
#endif

#endif /* INTRIN_H */
