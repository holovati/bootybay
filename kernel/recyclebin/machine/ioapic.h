#pragma once

class IoApic {
 public:
  class RedirectionEntry;

  int GetId();
  void SetId(int id);

  int GetVersion();
  int GetArbitrationId();
  int GetRedirectionEntryCount();

  RedirectionEntry GetRedirectionEntry(int pin);
  void SetRedirectionEntry(int pin, RedirectionEntry *redir_entry);

  void DumpRegisters();

 private:
  u8 volatile regsel_;
  u8 volatile regsel_reserved_[3];
  u32 volatile reserved0_[3];
  u32 volatile regwin_;
  u32 volatile reserved1_;
};

class IoApic::RedirectionEntry {
 public:
  enum POLARITY : u64 { POLARITY_HIGH_ACTIVE = 0, POLARITY_LOW_ACTIVE = 1 };
  enum TRIGGER_MODE : u64 { TRIGGER_MODE_EDGE = 0, TRIGGER_MODE_LEVEL = 1 };

  RedirectionEntry(u32 lo, u32 hi)
      : u{.raw{lo, hi}} {}

  RedirectionEntry()
      : u{.raw64 = 0} {}

  int GetInterruptVector() { return static_cast<int>(u.fields.intvec); }
  void SetInterruptVector(int intvec) {
    u.fields.intvec = static_cast<u8>(intvec);
  }

  bool IsMasked() { return u.fields.im == INTERRUPT_MASK_MASKED; }

  void Mask() { u.fields.im = INTERRUPT_MASK_MASKED; }

  void Unmask() { u.fields.im = INTERRUPT_MASK_UNMASKED; }

  bool IsActiveHigh() { return u.fields.intpol == POLARITY_HIGH_ACTIVE; }

  bool IsEdgeTriggered() { return u.fields.tm == TRIGGER_MODE_EDGE; }

  void SetPolarity(POLARITY polarity) { u.fields.intpol = polarity; }

  void SetTriggerMode(TRIGGER_MODE tm) { u.fields.tm = tm; }

  u64 GetRaw() { return u.raw64; }

 private:
  enum DELIVERY_MODE : u64 {
    DELIVERY_MODE_FIXED       = 0,
    DELIVERY_MODE_LOWEST_PRIO = 1,
    DELIVERY_MODE_SMI         = 2,
    DELIVERY_MODE_RESERVED0   = 3,
    DELIVERY_MODE_NMI         = 4,
    DELIVERY_MODE_INIT        = 5,
    DELIVERY_MODE_RESERVED1   = 6,
    DELIVERY_MODE_EXTINT      = 7
  };

  enum DESTINATION_MODE : u64 {
    DESTINATION_MODE_PHYSICAL = 0,
    DESTINATION_MODE_LOGICAL  = 1
  };

  enum DELIVERY_STATUS : u64 {
    DELIVERY_STATUS_IDLE         = 0,
    DELIVERY_STATUS_SEND_PENDING = 1
  };

  enum INTERRUPT_MASK : u64 {
    INTERRUPT_MASK_UNMASKED = 0,
    INTERRUPT_MASK_MASKED   = 1
  };

  union {
    struct [[gnu::packed]] {
      u64 intvec : 8;
      DELIVERY_MODE delmod : 3;
      DESTINATION_MODE destmod : 1;
      DELIVERY_STATUS delvis : 1;
      POLARITY intpol : 1;
      u64 irr : 1;
      TRIGGER_MODE tm : 1;
      INTERRUPT_MASK im : 1;
      u64 reserved0 : 39;
      u64 dest : 8;
    }
    fields;

    struct {
      u32 lo;
      u32 hi;
    } raw;

    u64 raw64;
  } u;
};

extern IoApic *ioapic;

static inline IoApic::RedirectionEntry gsi_get_redirecton_entry(u32 gsi) {
  // We only support 1 ioapic ATM
  return ioapic->GetRedirectionEntry(
      static_cast<int>(gsi /* - gsibase for the ioapic */));
}

static inline void gsi_set_redirecton_entry(
    u32 gsi, IoApic::RedirectionEntry *redir_ent) {
  // We only support 1 ioapic ATM
  ioapic->SetRedirectionEntry(
      static_cast<int>(gsi /* - gsibase for the ioapic */), redir_ent);
}

static inline void gsi_mask(u32 gsi) {
  IoApic::RedirectionEntry redir = gsi_get_redirecton_entry(gsi);
  redir.Mask();
  gsi_set_redirecton_entry(gsi, &redir);
}

static inline void gsi_unmask(u32 gsi) {
  IoApic::RedirectionEntry redir = gsi_get_redirecton_entry(gsi);
  redir.Unmask();
  gsi_set_redirecton_entry(gsi, &redir);
}
