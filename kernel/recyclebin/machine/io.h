#pragma once

#ifdef __cplusplus
// Write IO Space
template <typename T>
static void out(u16 port, T in) {
    asm volatile("out %0, %1\n" : : "Nd"(port), "a"(in) :);
}

// Read IO Space
template <typename T>
static inline T in(u16 port) {
    T out = 0;
    asm volatile("in %0, %1\n" : "=a"(out) : "Nd"(port) :);
    return out;
}
#endif /* __cplusplus */