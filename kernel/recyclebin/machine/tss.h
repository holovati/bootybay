#pragma once

enum TSS_IST : u64 {
  TSS_IST_LEGACY = 0,
  TSS_IST0       = 1,
  TSS_IST1       = 2,
  TSS_IST2       = 3,
  TSS_IST3       = 4,
  TSS_IST4       = 5,
  TSS_IST5       = 6,
  TSS_IST6       = 7,
  TSS_IST_COUNT  = TSS_IST6
};

// Page 322
struct [[gnu::packed]] TaskStateSegment {
  u32 resv0;
  u64 rsp[3];
  u64 resv1;
  u64 ist[TSS_IST_COUNT];
  u64 resv2;
  u32 io_map_address;

  void SetKernelStack(vm_offset_t rsp0) { rsp[0] = rsp0; }
};

static_assert(offsetof(TaskStateSegment, io_map_address) == 0x64,
              "TSS Wrong size");
