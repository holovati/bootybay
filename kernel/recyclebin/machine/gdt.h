#pragma once
#ifdef __cplusplus
#  include <machine/generic_descriptor.h>

template <typename ENTRY_IDX>
class alignas(16) GlobalDescriptorTable {
 public:
  inline void InstallNullSegment(ENTRY_IDX idx);
  inline void InstallTaskStateSegment(ENTRY_IDX idx, TaskStateSegment *tss);
  inline void InstallCodeSegment(ENTRY_IDX idx, CPU_PRIVILEGE dpl);
  inline void InstallDataSegment(ENTRY_IDX idx, CPU_PRIVILEGE dpl);
  inline SegmentSelector GetSegmentSelector(ENTRY_IDX idx);

  inline void Load();

 private:
  GenericDescriptor gdt_[((int)ENTRY_IDX::LAST) + 1UL];
};

template <typename ENTRY_IDX>
void GlobalDescriptorTable<ENTRY_IDX>::InstallNullSegment(ENTRY_IDX idx) {
  gdt_[(size_t)idx].Clear();
}

template <typename ENTRY_IDX>
void GlobalDescriptorTable<ENTRY_IDX>::InstallTaskStateSegment(
    ENTRY_IDX idx, TaskStateSegment *tss) {
  gdt_[(size_t)idx].InitializeTaskStateSegment(tss);
}

template <typename ENTRY_IDX>
void GlobalDescriptorTable<ENTRY_IDX>::InstallCodeSegment(ENTRY_IDX idx,
                                                          CPU_PRIVILEGE dpl) {
  gdt_[(size_t)idx].InitializeCodeSegment(dpl);
}

template <typename ENTRY_IDX>
void GlobalDescriptorTable<ENTRY_IDX>::InstallDataSegment(ENTRY_IDX idx,
                                                          CPU_PRIVILEGE dpl) {
  gdt_[(size_t)idx].InitializeDataSegment(dpl);
}

template <typename ENTRY_IDX>
SegmentSelector GlobalDescriptorTable<ENTRY_IDX>::GetSegmentSelector(
    ENTRY_IDX idx) {
  return SegmentSelector(
      gdt_[(size_t)idx].GetDpl(),
      SEGMENT_SELECTOR_TABLE_INDICATOR_GDT,
      (size_t)(sizeof(GenericDescriptor) * ((size_t)(idx)&0xFFFF)));
}

template <typename ENTRY_IDX>
void GlobalDescriptorTable<ENTRY_IDX>::Load() {
  struct [[gnu::packed]] __GdtRegister {
    u16 size;
    GenericDescriptor *gdt;
  };

  __GdtRegister gdtr;
  gdtr.size =
      (((((size_t)ENTRY_IDX::LAST) + 1) * sizeof(GenericDescriptor)) - 1) &
      0xFFFF;
  gdtr.gdt = &gdt_[0];
  asm volatile("lgdt %0 \n" : : "memory"(gdtr) :);
}

#endif /*__cplusplus */
