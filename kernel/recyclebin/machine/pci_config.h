#ifndef PCI_CONFIG_H
#define PCI_CONFIG_H
#include <machine/io.h>

#define CONF1_ADDR_PORT UINT16_C(0x0cf8)
#define CONF1_DATA_PORT UINT16_C(0x0cfc)

#define CONF1_ENABLE UINT32_C(0x80000000)
#define CONF1_ENABLE_CHK 0x80000000ul
#define CONF1_ENABLE_MSK 0x7f000000ul
#define CONF1_ENABLE_CHK1 0xff000001ul
#define CONF1_ENABLE_MSK1 0x80000001ul
#define CONF1_ENABLE_RES1 0x80000000ul

#define CONF2_ENABLE_PORT 0x0cf8
#define CONF2_FORWARD_PORT 0x0cfa

#define CONF2_ENABLE_CHK 0x0e
#define CONF2_ENABLE_RES 0x0e

static u16 pci_config_enable(u32 bus, u32 slot, u32 func, u32 reg) {
    u32 addr = (CONF1_ENABLE | (bus << 16) | (slot << 11) | (func << 8) | (reg & (~UINT32_C(0x3))));
    out<u32>(CONF1_ADDR_PORT, addr);
    return (u16)(CONF1_DATA_PORT + (reg & 0x03));
}

template <typename size_type>
static size_type pci_config_read(u32 bus, u32 slot, u32 func, u32 reg) {
    return in<size_type>(pci_config_enable(bus, slot, func, reg));
}

template <typename size_type>
static void pci_config_write(u32 bus, u32 slot, u32 func, u32 reg, size_type data) {
    out<size_type>(pci_config_enable(bus, slot, func, reg), data);
}

#endif /* PCI_CONFIG_H */
