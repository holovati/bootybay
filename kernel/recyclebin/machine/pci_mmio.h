#ifndef PCI_MMIO_H
#define PCI_MMIO_H
#include <machine/pci.h>

class c_pci_mmio {
    vm_offset_t va;

   public:
    i64 initialize(c_pci_dev &device, u32 bar_idx);
    void release();
    template <typename T, u32 reg>
    void write(const T &data) {
        ((*((T volatile *)(va + reg))) = data);
    }

    template <typename T, u32 reg>
    T read() {
        return (*((T volatile *)(va + reg)));
    }

    vm_offset_t base_address() { return va; }
};

#endif /* PCI_MMIO_H */
