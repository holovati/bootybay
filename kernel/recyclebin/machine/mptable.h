#pragma once

class MpConfigurationTable;

extern class MpFloatingPointerStructure *mp_fps;

class MpFloatingPointerStructure {
 public:
  bool ImcrPresent();

  u32 GetRevision();

  MpConfigurationTable *GetConfigurationTable();

 private:
  union {
    char bytes[4];
    u32 dword;
  } signature_;

  u32 configuration_table_;

  u8 length_;  // In 16 bytes (e.g. 1 = 16 bytes, 2 = 32 bytes)
  u8 revision_;
  u8 checksum_;  // This value should make all bytes in the table equal 0
                 // when added together
  u8 default_configuration_;  // If this is not zero then configuration_table
                              // should be ignored and a default configuration
                              // should be loaded instead
  u32 features_;  // If bit 7 is then the IMCR is present and PIC mode is
                  // being used, otherwise virtual wire mode is; all other
                  // bits are reserved
};

class MpConfigurationTable {
 public:
 private:
  char signature[4];  // "PCMP"
  u16 length;
  u8 mp_specification_revision;
  u8 checksum;  // Again, the byte should be all bytes in the table add up to 0
  char oem_id[8];
  char product_id[12];
  u32 oem_table;
  u16 oem_table_size;
  u16 entry_count;  // This value represents how many entries are following this
                    // table
  u32 lapic_address;  // This is the memory mapped address of the local APICs
  u16 extended_table_length;
  u8 extended_table_checksum;
  u8 reserved;
};
