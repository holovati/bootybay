#pragma once
#include "cpu.h"

namespace rs64 {

class irq_lock {
  CpuFlags old_rfl_;

 public:
  irq_lock()
      : old_rfl_(cpu::rflags()) {
    cpu_rflags_write(old_rfl_ & ~RFL_IF);
  }

  ~irq_lock() { cpu::rflags(old_rfl_); }
};

}  // namespace rs64