#pragma once

#define MSR_EFER_SCE (1ULL << 0)    /* System Call Extensions R/W */
#define MSR_EFER_LME (1ULL << 8)    /* Long Mode Enable R/W */
#define MSR_EFER_LMA (1ULL << 10)   /* Long Mode Active R/W */
#define MSR_EFER_NXE (1ULL << 11)   /* No-Execute Enable R/W */
#define MSR_EFER_SVME (1ULL << 12)  /* Secure Virtual Machine Enable R/W */
#define MSR_EFER_LMSLE (1ULL << 13) /* Long Mode Segment Limit Enable R/W */
#define MSR_EFER_FFXSR (1ULL << 14) /* Fast FXSAVE/FXRSTOR R/W */
#define MSR_EFER_TCE (1ULL << 15)   /* Translation Cache Extension R/W */

#ifdef __cplusplus
enum MSR : u32 {
  MSR_APIC_BASE      = 0x0000001B,
  MSR_SYSENTER_CS    = 0x00000174,
  MSR_SYSENTER_ESP   = 0x00000175,
  MSR_SYSENTER_EIP   = 0x00000176,
  MSR_MTRR_DEFTYPE   = 0x000002FF,
  MSR_EFER           = 0xC0000080,
  MSR_STAR           = 0xC0000081,
  MSR_LSTAR          = 0xC0000082,
  MSR_CSTAR          = 0xC0000083,
  MSR_SF_MASK        = 0xC0000084,
  MSR_FS_BASE        = 0xC0000100,
  MSR_GS_BASE        = 0xC0000101,
  MSR_KERNEL_GS_BASE = 0xC0000102,
  MSR_TSC_AUX        = 0xC0000103,
  MSR_TSC_RATIO      = 0xC0000104
};

namespace msr {
template <u64 ADDR>
struct _msr {
  operator u64() {
    u32 hi, lo;
    u64 res;

    asm volatile("rdmsr" : "=a"(lo), "=d"(hi) : "c"(ADDR) :);

    res = hi;

    res <<= 32;

    res |= lo;

    return res;
  }

  void operator=(u64 val) {
    u32 hi, lo;

    lo = (u32)(val & 0xFFFFFFFF);
    hi = (u32)(val >> 32);

    asm volatile("wrmsr" : : "a"(lo), "d"(hi), "c"(ADDR) :);
  }

  void operator|=(u64 val) { operator=((operator u64() | val)); }

  void operator&=(u64 val) { operator=((operator u64() & val)); }

  void operator^=(u64 val) { operator=((operator u64() ^ val)); }

  static _msr<ADDR> value;
};

typedef _msr<0x0000001B> apic_base;
typedef _msr<0xC0000080> efer;
}  // namespace msr

#endif /* __cplusplus */
