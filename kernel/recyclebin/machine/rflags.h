#pragma once
#ifdef __cplusplus
#  include <rs64/bitset.h>

enum RFLAGS : u64 {
  RFL_CF  = (1ULL << 0),  /* Carry Flag R/W */
  RFL_PF  = (1ULL << 2),  /* Parity Flag R/W */
  RFL_AF  = (1ULL << 4),  /* Auxiliary Flag R/W */
  RFL_ZF  = (1ULL << 6),  /* Zero Flag R/W */
  RFL_SF  = (1ULL << 7),  /* Sign Flag R/W */
  RFL_TF  = (1ULL << 8),  /* Trap Flag R/W */
  RFL_IF  = (1ULL << 9),  /* Interrupt Flag R/W */
  RFL_DF  = (1ULL << 10), /* Direction Flag R/W */
  RFL_OF  = (1ULL << 11), /* Overflow Flag R/W */
  RFL_NT  = (1ULL << 14), /* Nested Task R/W */
  RFL_RF  = (1ULL << 16), /* Resume Flag R/W */
  RFL_VM  = (1ULL << 17), /* Virtual-8086 Mode R/W */
  RFL_AC  = (1ULL << 18), /* Alignment Check R/W */
  RFL_VIF = (1ULL << 19), /* Virtual Interrupt Flag R/W */
  RFL_VIP = (1ULL << 20), /* Virtual Interrupt Pending R/W */
  RFL_ID  = (1ULL << 21), /* ID Flag R/W */
};

BITSET(RFLAGS) CpuFlags;

#endif