#pragma once

enum MTRR_MEMORY : u8 {
  MTRR_MEMORY_UNCACHEABLE  = 0x00,
  MTRR_MEMORY_WRITECOMBINE = 0x01,
  MTRR_MEMORY_WRITETHROUGH = 0x04,
  MTRR_MEMORY_WRITEPROTECT = 0x05,
  MTRR_MEMORY_WRITEBACK    = 0x06
};

struct mtrr_def_type {
  MTRR_MEMORY type : 8;
  u8 reserved_mbz0 : 2;
  u8 fixed_range_enable : 1;
  u8 mtrr_enable : 1;
  u8 reserved_mbz1 : 4;
  u8 reserved_mbz2[6];
};

static_assert(sizeof(mtrr_def_type) == 8, "mtrr_def_type must be 8 bytes");
