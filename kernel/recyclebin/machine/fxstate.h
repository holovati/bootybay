#pragma once
#include <machine/inline_asm.h>

// Intel manual page p.997
class alignas(16) FxState {
 public:
  void Save() { RS64_ASM("fxsave [%0] " : : "r"(this) :); }
  void Restore() { RS64_ASM("fxrstor [%0]" : : "r"(this) :); }

 private:
  u64 data_[64];
};