#pragma once
#ifdef __cplusplus
#  include <machine/generic_descriptor.h>
#  include <machine/inline_asm.h>
#  include <machine/task_control_block.h>

enum CPU_EXCEPTION : int {
  CPU_EXCEPTION_DE   = 0,   // Divide-by-Zero-Error DIV, IDIV, AAM instructions
  CPU_EXCEPTION_DB   = 1,   // Debug Instruction accesses and data accesses
  CPU_EXCEPTION_NMI  = 2,   // Non-Maskable-Interrupt External NMI signal
  CPU_EXCEPTION_BP   = 3,   // Breakpoint INT3 instruction
  CPU_EXCEPTION_OF   = 4,   // Overflow INTO instruction
  CPU_EXCEPTION_BR   = 5,   // Bound-Range BOUND instruction
  CPU_EXCEPTION_UD   = 6,   // Invalid-Opcode Invalid instructions
  CPU_EXCEPTION_NM   = 7,   // Device-Not-Available x87 instructions
  CPU_EXCEPTION_R9   = 9,   // Reserved
  CPU_EXCEPTION_MF   = 16,  // x87 Floating-Point ExceptionPending
  CPU_EXCEPTION_MC   = 18,  // Machine-Check Model specific
  CPU_EXCEPTION_XF   = 19,  // SIMD Floating-Point floating-point instructions
  CPU_EXCEPTION_R20  = 20,  // Reserved
  CPU_EXCEPTION_R21  = 21,  // Reserved
  CPU_EXCEPTION_R22  = 22,  // Reserved
  CPU_EXCEPTION_R23  = 23,  // Reserved
  CPU_EXCEPTION_R24  = 24,  // Reserved
  CPU_EXCEPTION_R25  = 25,  // Reserved
  CPU_EXCEPTION_R26  = 26,  // Reserved
  CPU_EXCEPTION_R27  = 27,  // Reserved
  CPU_EXCEPTION_HV   = 28,  // Hypervisor Injection Exception Event injection
  CPU_EXCEPTION_VC   = 29,  // VMM Communication Exception Virtualization event
  CPU_EXCEPTION_R31  = 31,  // Reserved
  CPU_EXCEPTION_LAST = CPU_EXCEPTION_R31
};

enum CPU_EXCEPTION_ERROR : int {
  CPU_EXCEPTION_DF = 8,   // Double-Fault
  CPU_EXCEPTION_TS = 10,  // Invalid-TSS Task-state segment access
  CPU_EXCEPTION_NP = 11,  // Segment-Not-Present Segment register loads
  CPU_EXCEPTION_SS = 12,  // Stack SS register loads and stack references
  CPU_EXCEPTION_GP = 13,  // General-Protection
  CPU_EXCEPTION_PF = 14,  // Page-Fault #PF Memory accesses when paging enabled
  CPU_EXCEPTION_AC = 17,  // Alignment-Check Misaligned memory accesses
  CPU_EXCEPTION_SX = 30,  // Security Exception Security-sensitive event in host
};

enum CPU_INTR : int {
  CPU_INTR_0 = 0,
  CPU_INTR_1,
  CPU_INTR_2,
  CPU_INTR_3,
  CPU_INTR_4,
  CPU_INTR_5,
  CPU_INTR_6,
  CPU_INTR_7,
  CPU_INTR_8,
  CPU_INTR_9,
  CPU_INTR_10,
  CPU_INTR_11,
  CPU_INTR_12,
  CPU_INTR_13,
  CPU_INTR_14,
  CPU_INTR_15,
  CPU_INTR_16,
  CPU_INTR_17,
  CPU_INTR_18,
  CPU_INTR_19,
  CPU_INTR_20,
  CPU_INTR_21,
  CPU_INTR_22,
  CPU_INTR_23,
  CPU_INTR_24,
  CPU_INTR_25,
  CPU_INTR_26,
  CPU_INTR_27,
  CPU_INTR_28,
  CPU_INTR_29,
  CPU_INTR_30,
  CPU_INTR_31,
  CPU_INTR_32,
  CPU_INTR_33,
  CPU_INTR_34,
  CPU_INTR_35,
  CPU_INTR_36,
  CPU_INTR_37,
  CPU_INTR_38,
  CPU_INTR_39,
  CPU_INTR_40,
  CPU_INTR_41,
  CPU_INTR_42,
  CPU_INTR_43,
  CPU_INTR_44,
  CPU_INTR_45,
  CPU_INTR_46,
  CPU_INTR_47,
  CPU_INTR_48,
  CPU_INTR_49,
  CPU_INTR_50,
  CPU_INTR_51,
  CPU_INTR_52,
  CPU_INTR_53,
  CPU_INTR_54,
  CPU_INTR_55,
  CPU_INTR_56,
  CPU_INTR_57,
  CPU_INTR_58,
  CPU_INTR_59,
  CPU_INTR_60,
  CPU_INTR_61,
  CPU_INTR_62,
  CPU_INTR_63,
  CPU_INTR_64,
  CPU_INTR_65,
  CPU_INTR_66,
  CPU_INTR_67,
  CPU_INTR_68,
  CPU_INTR_69,
  CPU_INTR_70,
  CPU_INTR_71,
  CPU_INTR_72,
  CPU_INTR_73,
  CPU_INTR_74,
  CPU_INTR_75,
  CPU_INTR_76,
  CPU_INTR_77,
  CPU_INTR_78,
  CPU_INTR_79,
  CPU_INTR_80,
  CPU_INTR_81,
  CPU_INTR_82,
  CPU_INTR_83,
  CPU_INTR_84,
  CPU_INTR_85,
  CPU_INTR_86,
  CPU_INTR_87,
  CPU_INTR_88,
  CPU_INTR_89,
  CPU_INTR_90,
  CPU_INTR_91,
  CPU_INTR_92,
  CPU_INTR_93,
  CPU_INTR_94,
  CPU_INTR_95,
  CPU_INTR_96,
  CPU_INTR_97,
  CPU_INTR_98,
  CPU_INTR_99,
  CPU_INTR_100,
  CPU_INTR_101,
  CPU_INTR_102,
  CPU_INTR_103,
  CPU_INTR_104,
  CPU_INTR_105,
  CPU_INTR_106,
  CPU_INTR_107,
  CPU_INTR_108,
  CPU_INTR_109,
  CPU_INTR_110,
  CPU_INTR_111,
  CPU_INTR_112,
  CPU_INTR_113,
  CPU_INTR_114,
  CPU_INTR_115,
  CPU_INTR_116,
  CPU_INTR_117,
  CPU_INTR_118,
  CPU_INTR_119,
  CPU_INTR_120,
  CPU_INTR_121,
  CPU_INTR_122,
  CPU_INTR_123,
  CPU_INTR_124,
  CPU_INTR_125,
  CPU_INTR_126,
  CPU_INTR_127,
  CPU_INTR_128,
  CPU_INTR_129,
  CPU_INTR_130,
  CPU_INTR_131,
  CPU_INTR_132,
  CPU_INTR_133,
  CPU_INTR_134,
  CPU_INTR_135,
  CPU_INTR_136,
  CPU_INTR_137,
  CPU_INTR_138,
  CPU_INTR_139,
  CPU_INTR_140,
  CPU_INTR_141,
  CPU_INTR_142,
  CPU_INTR_143,
  CPU_INTR_144,
  CPU_INTR_145,
  CPU_INTR_146,
  CPU_INTR_147,
  CPU_INTR_148,
  CPU_INTR_149,
  CPU_INTR_150,
  CPU_INTR_151,
  CPU_INTR_152,
  CPU_INTR_153,
  CPU_INTR_154,
  CPU_INTR_155,
  CPU_INTR_156,
  CPU_INTR_157,
  CPU_INTR_158,
  CPU_INTR_159,
  CPU_INTR_160,
  CPU_INTR_161,
  CPU_INTR_162,
  CPU_INTR_163,
  CPU_INTR_164,
  CPU_INTR_165,
  CPU_INTR_166,
  CPU_INTR_167,
  CPU_INTR_168,
  CPU_INTR_169,
  CPU_INTR_170,
  CPU_INTR_171,
  CPU_INTR_172,
  CPU_INTR_173,
  CPU_INTR_174,
  CPU_INTR_175,
  CPU_INTR_176,
  CPU_INTR_177,
  CPU_INTR_178,
  CPU_INTR_179,
  CPU_INTR_180,
  CPU_INTR_181,
  CPU_INTR_182,
  CPU_INTR_183,
  CPU_INTR_184,
  CPU_INTR_185,
  CPU_INTR_186,
  CPU_INTR_187,
  CPU_INTR_188,
  CPU_INTR_189,
  CPU_INTR_190,
  CPU_INTR_191,
  CPU_INTR_192,
  CPU_INTR_193,
  CPU_INTR_194,
  CPU_INTR_195,
  CPU_INTR_196,
  CPU_INTR_197,
  CPU_INTR_198,
  CPU_INTR_199,
  CPU_INTR_200,
  CPU_INTR_201,
  CPU_INTR_202,
  CPU_INTR_203,
  CPU_INTR_204,
  CPU_INTR_205,
  CPU_INTR_206,
  CPU_INTR_207,
  CPU_INTR_208,
  CPU_INTR_209,
  CPU_INTR_210,
  CPU_INTR_211,
  CPU_INTR_212,
  CPU_INTR_213,
  CPU_INTR_214,
  CPU_INTR_215,
  CPU_INTR_216,
  CPU_INTR_217,
  CPU_INTR_218,
  CPU_INTR_219,
  CPU_INTR_220,
  CPU_INTR_221,
  CPU_INTR_222,
  CPU_INTR_223,
  CPU_INTR_LAST = CPU_INTR_223
};

template <typename ErrorType>
using ExceptionErrorGateTarget = void (*)(TaskControlBlock *, ErrorType);
using ExceptionGateTarget      = void (*)(TaskControlBlock *);

template <ExceptionGateTarget Function>
static [[gnu::naked]] void exception_gate_target() {
  RS64_ASM(INLINE_ASM_STORE_REGISTERS(rdi));
  RS64_ASM("push rdi");
  RS64_ASM("call %0" : : "i"(Function) :);
  RS64_ASM("pop rdi");
  RS64_ASM(INLINE_ASM_RESTORE_REGISTERS(rdi));
  RS64_ASM("iretq");
}

template <typename ErrorType, ExceptionErrorGateTarget<ErrorType> Function>
static [[gnu::naked]] void exception_gate_target() {
  RS64_ASM("pop [rsp - %0]" : : "i"(offsetof(TaskControlBlock, cs)) :);
  RS64_ASM(INLINE_ASM_STORE_REGISTERS(rdi));
  RS64_ASM("mov rsi, [rsp - 8]");
  RS64_ASM("push rdi");
  RS64_ASM("call %0" : : "i"(Function) :);
  RS64_ASM("pop rdi");
  RS64_ASM(INLINE_ASM_RESTORE_REGISTERS(rdi));
  RS64_ASM("iretq");
}

using InterruptHandler = void (*)(TaskControlBlock *, CPU_INTR);

template <CPU_INTR interrupt, InterruptHandler handler>
[[gnu::naked]] void irq_wrapper() {
  RS64_ASM(INLINE_ASM_STORE_REGISTERS(rdi));
  RS64_ASM("push rdi");
  RS64_ASM("mov esi, %0" : : "i"(interrupt) :);
  RS64_ASM("call %0" : : "i"(handler) :);
  RS64_ASM("pop rdi");
  RS64_ASM(INLINE_ASM_RESTORE_REGISTERS(rdi));
  RS64_ASM("iretq");
}

class alignas(16) InterruptDescriptorTable {
 public:
  template <CPU_EXCEPTION EXCEPTION, ExceptionGateTarget TARGET>
  inline void InstallExceptionHandler(SYSTEM_SEGMENT_DESCRIPTOR_TYPE type,
                                      SegmentSelector selector,
                                      CPU_PRIVILEGE dpl,
                                      TSS_IST ist);

  template <CPU_EXCEPTION_ERROR EXCEPTION,
            typename ErrorType,
            ExceptionErrorGateTarget<ErrorType> TARGET>
  inline void InstallExceptionHandler(SYSTEM_SEGMENT_DESCRIPTOR_TYPE type,
                                      SegmentSelector selector,
                                      CPU_PRIVILEGE dpl,
                                      TSS_IST ist);
  template <CPU_INTR INTERRUPT, InterruptHandler handler>
  inline void InstallInterruptHandler(SYSTEM_SEGMENT_DESCRIPTOR_TYPE type,
                                      SegmentSelector selector,
                                      CPU_PRIVILEGE dpl,
                                      TSS_IST ist);

  inline void Load();

 private:
  GenericDescriptor
      descriptors_[((CPU_EXCEPTION_LAST + 1) + (CPU_INTR_LAST + 1))];
};

static_assert(sizeof(InterruptDescriptorTable) == 0x1000,
              "InteruptDescriptorTable wrong size");

template <CPU_EXCEPTION EXCEPTION, ExceptionGateTarget TARGET>
void InterruptDescriptorTable::InstallExceptionHandler(
    SYSTEM_SEGMENT_DESCRIPTOR_TYPE type,
    SegmentSelector selector,
    CPU_PRIVILEGE dpl,
    TSS_IST ist) {
  descriptors_[EXCEPTION].InitializeGate(
      type, exception_gate_target<TARGET>, selector, dpl, ist);
}

template <CPU_EXCEPTION_ERROR EXCEPTION,
          typename ErrorType,
          ExceptionErrorGateTarget<ErrorType> TARGET>
void InterruptDescriptorTable::InstallExceptionHandler(
    SYSTEM_SEGMENT_DESCRIPTOR_TYPE type,
    SegmentSelector selector,
    CPU_PRIVILEGE dpl,
    TSS_IST ist) {
  descriptors_[EXCEPTION].InitializeGate(
      type, exception_gate_target<ErrorType, TARGET>, selector, dpl, ist);
}

template <CPU_INTR INTERRUPT, InterruptHandler handler>
void InterruptDescriptorTable::InstallInterruptHandler(
    SYSTEM_SEGMENT_DESCRIPTOR_TYPE type,
    SegmentSelector selector,
    CPU_PRIVILEGE dpl,
    TSS_IST ist) {
  descriptors_[(CPU_EXCEPTION_LAST + 1 + INTERRUPT)].InitializeGate(
      type, irq_wrapper<INTERRUPT, handler>, selector, dpl, ist);
}

void InterruptDescriptorTable::Load() {
  struct [[gnu::packed]] {
    uint16_t size;
    GenericDescriptor *ptr;
  }
  idtr;

  idtr.size = (sizeof(descriptors_) - 1) & 0xFFFF;
  idtr.ptr  = &(descriptors_[0]);

  asm volatile("lidt %0 \n" : : "memory"(idtr) :);
}

#endif