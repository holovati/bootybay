#pragma once

class Apic {
 public:
  class Timer;
  enum LVTR_TYPE : u32 {
    LVTR_TYPE_TIMER = 0 * sizeof(u32),
    LVTR_TYPE_THERM = 1 * sizeof(u32),
    LVTR_TYPE_PERFC = 2 * sizeof(u32),
    LVTR_TYPE_LINT0 = 3 * sizeof(u32),
    LVTR_TYPE_LINT1 = 4 * sizeof(u32),
    LVTR_TYPE_ERROR = 5 * sizeof(u32)
  };

  enum DELIVERY_STATUS : u32 {
    DELIVERY_STATUS_IDLE    = 0,
    DELIVERY_STATUS_PENDING = 1
  };

  enum TRIGGER_MODE : u32 { TRIGGER_MODE_EDGE = 0, TRIGGER_MODE_LEVEL = 1 };

  enum PIN_POLARITY : u32 {
    PIN_POLARITY_ACTIVE_HIGH = 0,
    PIN_POLARITY_ACTIVE_LOW
  };

  enum MASK : u32 { NOT_MASKED = 0, MASKED = 1 };

  enum MESSAGE_TYPE : u32 {
    MESSAGE_TYPE_FIXED  = 0x0,
    MESSAGE_TYPE_SMI    = 0x2,
    MESSAGE_TYPE_NMI    = 0x4,
    MESSAGE_TYPE_INIT   = 0x5,
    MESSAGE_TYPE_EXTINT = 0x7
  };

  union SpuriousInterruptRegister {
    struct [[gnu::packed]] {
      u32 VEC : 8;  // Vector R/W
      u32 ASE : 1;  // APIC Software Enable R/W
      u32 FCC : 1;  // Focus CPU Core Checking R/W
    }
    field;
    u32 raw;
  };

  union LocalVectorTableRegister {
    struct [[gnu::packed]] {
      u32 VEC : 8;
      MESSAGE_TYPE MT : 3;
      u32 RESV0 : 1;
      DELIVERY_STATUS DS : 1;
      PIN_POLARITY POL : 1;
      u32 RIR : 1;
      TRIGGER_MODE TGM : 1;
      MASK M : 1;
      u32 TMM : 2;
      u32 RESV1 : 13;
    }
    field;
    u32 raw;
  };

  LocalVectorTableRegister GetLocalVectorRegister(LVTR_TYPE lvtr_type);
  void SetLocalVectorRegister(LVTR_TYPE lvtr_type,
                              LocalVectorTableRegister lvtr);

  inline SpuriousInterruptRegister ReadSpuriousInterruptRegister();
  inline void WriteSpuriousInterruptRegister(SpuriousInterruptRegister sir);

  inline void EndOfInterrupt();

  inline u32 GetId();
  inline void SetId(u32 id);

  inline bool ExtendedRegisterSpacePresent();
  inline u32 MaxLVTEntries();
  inline u32 GetVersion();

  void SetTaskPriority(u32 priority, u32 sub_priority);

  void DumpRegisters();

  // For bootstraping
  static Apic *GetInstance();

 private:
  enum REGISTER : u32 {
    REGISTER_ID       = 0x20 / sizeof(u32),
    REGISTER_VERSION  = 0x30 / sizeof(u32),
    REGISTER_TPR      = 0x80 / sizeof(u32),
    REGISTER_APR      = 0x90 / sizeof(u32),
    REGISTER_PPR      = 0xA0 / sizeof(u32),
    REGISTER_EOI      = 0xB0 / sizeof(u32),
    REGISTER_REMOTE_R = 0xC0 / sizeof(u32),
    REGISTER_LDR      = 0xD0 / sizeof(u32),
    REGISTER_DFR      = 0xE0 / sizeof(u32),
    REGISTER_SPIV     = 0xF0 / sizeof(u32),

    // 100-170h In-Service Register (ISR) 00000000h
    REGISTER_ISR0    = 0x100 / sizeof(u32),
    REGISTER_ISR1    = 0x110 / sizeof(u32),
    REGISTER_ISR2    = 0x120 / sizeof(u32),
    REGISTER_ISR3    = 0x130 / sizeof(u32),
    REGISTER_ISR4    = 0x140 / sizeof(u32),
    REGISTER_ISR5    = 0x150 / sizeof(u32),
    REGISTER_ISR6    = 0x160 / sizeof(u32),
    REGISTER_ISR7    = 0x170 / sizeof(u32),
    REGISTER_ISR_END = 0x180 / sizeof(u32),
    // 180-1F0h Trigger Mode Register (TMR) 00000000h
    REGISTER_TMR0    = 0x180 / sizeof(u32),
    REGISTER_TMR1    = 0x190 / sizeof(u32),
    REGISTER_TMR2    = 0x1A0 / sizeof(u32),
    REGISTER_TMR3    = 0x1B0 / sizeof(u32),
    REGISTER_TMR4    = 0x1C0 / sizeof(u32),
    REGISTER_TMR5    = 0x1D0 / sizeof(u32),
    REGISTER_TMR6    = 0x1E0 / sizeof(u32),
    REGISTER_TMR7    = 0x1F0 / sizeof(u32),
    REGISTER_TMR_END = 0x200 / sizeof(u32),
    // 200-270h Interrupt Request Register (IRR) 00000000h
    REGISTER_IRR0    = 0x200 / sizeof(u32),
    REGISTER_IRR1    = 0x210 / sizeof(u32),
    REGISTER_IRR2    = 0x220 / sizeof(u32),
    REGISTER_IRR3    = 0x230 / sizeof(u32),
    REGISTER_IRR4    = 0x240 / sizeof(u32),
    REGISTER_IRR5    = 0x250 / sizeof(u32),
    REGISTER_IRR6    = 0x260 / sizeof(u32),
    REGISTER_IRR7    = 0x270 / sizeof(u32),
    REGISTER_IRR_END = 0x280 / sizeof(u32),

    REGISTER_ESR     = 0x280 / sizeof(u32),
    REGISTER_ICR     = 0x300 / sizeof(u32),
    REGISTER_ICR2    = 0x310 / sizeof(u32),
    REGISTER_LVTT    = 0x320 / sizeof(u32),
    REGISTER_LVTTHMR = 0x330 / sizeof(u32),
    REGISTER_LVTPC   = 0x340 / sizeof(u32),
    REGISTER_LVT0    = 0x350 / sizeof(u32),
    REGISTER_LVT1    = 0x360 / sizeof(u32),
    REGISTER_LVTERR  = 0x370 / sizeof(u32),
    REGISTER_TIC     = 0x380 / sizeof(u32),
    REGISTER_TCC     = 0x390 / sizeof(u32),
    REGISTER_DCR     = 0x3E0 / sizeof(u32),
    // 0x400/sizeof(u32) Extended APIC Feature Register 00040007h
    // 0x410/sizeof(u32) Extended APIC Control Register 00000000h
    REGISTER_SEOI = 0x420 / sizeof(u32),
    // 480-4F0h Interrupt Enable Registers (IER) FFFFFFFFh
    // 500-530h Extended Interrupt [3:0] Local Vector Table Registers 00000000h
    REGISTER_LAST = 0x530 / sizeof(u32)
  };

  u32 volatile reg_[REGISTER_LAST];
};

class Apic::Timer {
 public:
  enum DIVIDE : u32 {
    DIVIDE_BY_2   = 0,
    DIVIDE_BY_4   = 1,
    DIVIDE_BY_8   = 2,
    DIVIDE_BY_16  = 3,
    DIVIDE_BY_32  = 8,
    DIVIDE_BY_64  = 9,
    DIVIDE_BY_128 = 10,
    DIVIDE_BY_1   = 11
  };

  enum MODE : u32 {
    MODE_ONE_SHOT     = 0,
    MODE_PERIODIC     = 1,
    MODE_TSC_DEADLINE = 2
  };

  union LocalVectorTableRegister {
    struct [[gnu::packed]] {
      u32 VEC : 8;
      u32 RESV0 : 4;
      DELIVERY_STATUS DS : 1;
      u32 RESV1 : 3;
      MASK M : 1;
      MODE TMM : 2;
      u32 RESV2 : 13;
    }
    field;
    u32 raw;
  };

  inline DIVIDE ReadDivideRegister();
  inline void WriteDivideRegister(DIVIDE reg);

  inline LocalVectorTableRegister ReadLocalVectorTableRegister();
  inline void WriteLocalVectorTableRegister(LocalVectorTableRegister *reg);

  inline u32 GetCurrentCount();

  inline u32 GetInitialCount();
  inline void SetInitialCount(u32 count);

  // For bootstraping
  static bool IsDeadlineModeSupported();
  static bool IsAlwaysRunning();
  static Timer *GetInstance();

 private:
  u32 volatile apic_reg_[256];
};

// Apic OPS
Apic::SpuriousInterruptRegister Apic::ReadSpuriousInterruptRegister() {
  return SpuriousInterruptRegister{.raw = reg_[60]};
}

void Apic::WriteSpuriousInterruptRegister(Apic::SpuriousInterruptRegister sir) {
  reg_[60] = sir.raw;
}

void Apic::EndOfInterrupt() { reg_[44] = 0; }

u32 Apic::GetId() { return (reg_[REGISTER_ID] >> 24); }

void Apic::SetId(u32 id) {
  u32 apic_id       = (reg_[REGISTER_ID] & (~(0xFU) << 24));
  reg_[REGISTER_ID] = apic_id | (id << 24);
}

bool Apic::ExtendedRegisterSpacePresent() {
  return reg_[REGISTER_VERSION] >> 31;
}

u32 Apic::MaxLVTEntries() {
  return ((reg_[REGISTER_VERSION] >> 16) & 0xFU) + 1;
}

u32 Apic::GetVersion() { return (reg_[REGISTER_VERSION] & 0xFU); }

// Apic Timer OPS
Apic::Timer::DIVIDE Apic::Timer::ReadDivideRegister() {
  return DIVIDE(apic_reg_[248]);
}

void Apic::Timer::WriteDivideRegister(Apic::Timer::DIVIDE reg) {
  apic_reg_[248] = reg;
}

Apic::Timer::LocalVectorTableRegister
Apic::Timer::ReadLocalVectorTableRegister() {
  return LocalVectorTableRegister{.raw = apic_reg_[200]};
}

void Apic::Timer::WriteLocalVectorTableRegister(
    Apic::Timer::LocalVectorTableRegister *reg) {
  apic_reg_[200] = reg->raw;
}

u32 Apic::Timer::GetCurrentCount() { return apic_reg_[228]; }

u32 Apic::Timer::GetInitialCount() { return apic_reg_[224]; }

void Apic::Timer::SetInitialCount(u32 count) { apic_reg_[224] = count; }
