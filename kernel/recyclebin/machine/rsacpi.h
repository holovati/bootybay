#pragma once

#include <acpi.h>

class c_pci_dev;

namespace rs64 {

acpi_pci_routing_table *acpi_pci_get_routing_table();

acpi_pci_routing_table *acpi_pci_get_routing(acpi_pci_routing_table *prt,
                                             u32 slot,
                                             u32 function,
                                             u32 pin);

acpi_pci_routing_table *acpi_pci_get_routing_table(c_pci_dev *dev);

size_t acpi_get_device_crs(c_pci_dev *device, acpi_resource **resources_out);

}  // namespace rs64
