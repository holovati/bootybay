#pragma once

#define _RS64_ASM(s) asm volatile(s)

#define RS64_ASM(...) _RS64_ASM(__VA_ARGS__)

#define RS64_JUMP(where) RS64_ASM("jmp %0\n\t" : : "i"(where) :)

#define INLINE_ASM_STORE_REGISTERS(dst)                                        \
  "push r15\n\t"                                                               \
  "push r14\n\t"                                                               \
  "push r13\n\t"                                                               \
  "push r12\n\t"                                                               \
  "push r11\n\t"                                                               \
  "push r10\n\t"                                                               \
  "push r9\n\t"                                                                \
  "push r8\n\t"                                                                \
  "push rbp\n\t"                                                               \
  "push rdi\n\t"                                                               \
  "push rsi\n\t"                                                               \
  "push rdx\n\t"                                                               \
  "push rcx\n\t"                                                               \
  "push rbx\n\t"                                                               \
  "push rax\n\t"                                                               \
  "mov rcx, cr3\n\t"                                                           \
  "push rcx\n\t"                                                               \
  "mov " #dst ", rsp\n\t"

#define INLINE_ASM_RESTORE_REGISTERS(src)                                      \
  "mov rsp, " #src                                                             \
  "\n\t"                                                                       \
  "pop rcx\n\t"                                                                \
  "mov cr3, rcx\n\t"                                                           \
  "pop rax\n\t"                                                                \
  "pop rbx\n\t"                                                                \
  "pop rcx\n\t"                                                                \
  "pop rdx\n\t"                                                                \
  "pop rsi\n\t"                                                                \
  "pop rdi\n\t"                                                                \
  "pop rbp\n\t"                                                                \
  "pop r8\n\t"                                                                 \
  "pop r9\n\t"                                                                 \
  "pop r10\n\t"                                                                \
  "pop r11\n\t"                                                                \
  "pop r12\n\t"                                                                \
  "pop r13\n\t"                                                                \
  "pop r14\n\t"                                                                \
  "pop r15\n\t"
