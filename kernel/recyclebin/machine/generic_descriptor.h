#pragma once
#include <machine/tss.h>

enum DESCRIPTOR_TYPE : u64 {
  DESCRIPTOR_TYPE_SYSTEM = 0,  // LDT, TSS, GATE
  DESCRIPTOR_TYPE_USER   = 1   // Code, Data
};

enum SYSTEM_SEGMENT_DESCRIPTOR_TYPE : u64 {
  SYSTEM_SEGMENT_DESCRIPTOR_TYPE_LDT       = 0x2,
  SYSTEM_SEGMENT_DESCRIPTOR_TYPE_TSS       = 0x9,
  SYSTEM_SEGMENT_DESCRIPTOR_TYPE_BUSY_TSS  = 0xB,
  SYSTEM_SEGMENT_DESCRIPTOR_TYPE_CALL      = 0xC,
  SYSTEM_SEGMENT_DESCRIPTOR_TYPE_INTERRUPT = 0xE,
  SYSTEM_SEGMENT_DESCRIPTOR_TYPE_TRAP      = 0xF
};

enum USER_SEGMENT_DESCRIPTOR_TYPE : u64 {
  USER_SEGMENT_DESCRIPTOR_TYPE_DATA = 0x0,
  USER_SEGMENT_DESCRIPTOR_TYPE_CODE = 0x1
};

enum CPU_PRIVILEGE : u64 {
  CPU_PRIVILEGE_RING0 = 0,
  CPU_PRIVILEGE_RING1 = 1,
  CPU_PRIVILEGE_RING2 = 2,
  CPU_PRIVILEGE_RING3 = 3
};

enum SEGMENT_SELECTOR_TABLE_INDICATOR : u64 {
  SEGMENT_SELECTOR_TABLE_INDICATOR_GDT = 0,
  SEGMENT_SELECTOR_TABLE_INDICATOR_LDT = 1
};

class [[gnu::packed]] SegmentSelector {
 public:
  SegmentSelector(CPU_PRIVILEGE rpl,
                  SEGMENT_SELECTOR_TABLE_INDICATOR table_indicator,
                  size_t table_index)
      : u({.selector{(u16)(table_index & 0x1FFF), table_indicator, rpl}}) {}

  CPU_PRIVILEGE GetRpl() {
    return (CPU_PRIVILEGE)(u.selector.requestor_privilege_level_ & 2);
  }

  SEGMENT_SELECTOR_TABLE_INDICATOR GetTableIndicator() {
    return (SEGMENT_SELECTOR_TABLE_INDICATOR)(u.selector.table_indicator_ >> 1);
  }

  size_t GetTableIndex() { return (size_t)(u.selector.table_index_ >> 3); }

  operator u16() { return u.raw; }

 private:
  union {
    struct [[gnu::packed]] {
      u16 table_index_ : 13;
      SEGMENT_SELECTOR_TABLE_INDICATOR table_indicator_ : 1;
      CPU_PRIVILEGE requestor_privilege_level_ : 2;
    }
    selector;
    u16 raw;
  } u;
};

static_assert(sizeof(SegmentSelector) == 2, "Segment selector wrong size");

class GenericDescriptor {
 public:
  using TargetFunction = void (*)();

  // Zero
  inline void Clear();

  // For task state segment
  inline void InitializeTaskStateSegment(TaskStateSegment *tss);

  inline void InitializeDataSegment(CPU_PRIVILEGE dpl);

  inline void InitializeCodeSegment(CPU_PRIVILEGE dpl);

  inline void InitializeGate(SYSTEM_SEGMENT_DESCRIPTOR_TYPE type,
                             TargetFunction target,
                             SegmentSelector selector,
                             CPU_PRIVILEGE dpl,
                             TSS_IST ist);

  inline CPU_PRIVILEGE GetDpl() { return descriptor.segment.dpl; }

 private:
  union {
    // SYSTEM
    struct [[gnu::packed]] {  // TSS, LDT
      vm_offset_t limit0 : 16;
      vm_offset_t address0 : 16;
      vm_offset_t address1 : 8;
      SYSTEM_SEGMENT_DESCRIPTOR_TYPE system_type : 4;
      DESCRIPTOR_TYPE descriptor_type : 1;
      CPU_PRIVILEGE dpl : 2;
      u64 present : 1;
      vm_offset_t limit1 : 4;
      u64 available : 1;
      u64 unused : 2;
      u64 granularity : 1;
      vm_offset_t address2 : 8;
      vm_offset_t address3 : 32;
      u64 zero1 : 32;
    }
    segment;

    struct [[gnu::packed]] {  // INTERRUPT, TRAP GATES
      vm_offset_t offset0 : 16;
      SegmentSelector selector;
      TSS_IST ist : 3;
      u64 reserved0 : 5;
      SYSTEM_SEGMENT_DESCRIPTOR_TYPE system_type : 4;
      DESCRIPTOR_TYPE descriptor_type : 1;
      CPU_PRIVILEGE dpl : 2;
      u64 present : 1;
      vm_offset_t offset1 : 16;
      vm_offset_t offset2 : 32;
      u64 zero0 : 32;
    }
    gate;

    // USER
    struct [[gnu::packed]] {  // CODE SEGMENT
      vm_offset_t segment_limit0 : 16;
      vm_offset_t base_address0 : 16;
      vm_offset_t base_address1 : 8;
      u64 accessed : 1;
      u64 readable : 1;
      u64 conforming : 1;
      USER_SEGMENT_DESCRIPTOR_TYPE user_type : 1;
      DESCRIPTOR_TYPE descriptor_type : 1;
      CPU_PRIVILEGE destination_privilege_level : 2;
      u64 present : 1;
      vm_offset_t segment_limit1 : 4;
      u64 unused_avail : 1;
      u64 long_mode : 1;
      u64 default_operand_size : 1;
      u64 granularity : 1;
      vm_offset_t base_address2 : 8;
      u64 copy;
    }
    code;

    struct [[gnu::packed]] {  // DATA SEGMENT
      vm_offset_t segment_limit0 : 16;
      vm_offset_t base_address0 : 16;
      vm_offset_t base_address1 : 8;
      u64 accessed : 1;
      u64 writable : 1;
      u64 expand_down : 1;
      USER_SEGMENT_DESCRIPTOR_TYPE user_type : 1;
      DESCRIPTOR_TYPE descriptor_type : 1;
      CPU_PRIVILEGE destination_privilege_level : 2;
      u64 present : 1;
      vm_offset_t segment_limit1 : 4;
      u64 unused_avail : 1;
      u64 unused : 1;
      u64 default_operand_size : 1;
      u64 granularity : 1;
      vm_offset_t base_address2 : 8;
      u64 copy;
    }
    data;

    struct {
      u64 low;
      u64 hi;
    } raw;

  } descriptor{.raw{0, 0}};
};

static_assert(sizeof(GenericDescriptor) == 16, "GenericDescriptor wrong size");

void GenericDescriptor::Clear() {
  descriptor.raw.low = 0;
  descriptor.raw.hi  = 0;
}

void GenericDescriptor::InitializeTaskStateSegment(TaskStateSegment *tss) {
  descriptor.segment.present = 1;

  descriptor.segment.address0 = (u32)(((vm_offset_t)tss) >> 0x00) & 0x0000FFFF;
  descriptor.segment.address1 = (u32)(((vm_offset_t)tss) >> 0x10) & 0x000000FF;
  descriptor.segment.address2 = (u32)(((vm_offset_t)tss) >> 0x18) & 0x000000FF;
  descriptor.segment.address3 = (u32)(((vm_offset_t)tss) >> 0x20) & 0xFFFFFFFF;

  descriptor.segment.limit0      = (sizeof(TaskStateSegment) >> 0x00) & 0xFFFF;
  descriptor.segment.limit1      = (sizeof(TaskStateSegment) >> 0x10) & 0x000F;
  descriptor.segment.granularity = 0;  // Limit is in bytes not pages

  descriptor.segment.system_type     = SYSTEM_SEGMENT_DESCRIPTOR_TYPE_TSS;
  descriptor.segment.descriptor_type = DESCRIPTOR_TYPE_SYSTEM;
  descriptor.segment.dpl             = CPU_PRIVILEGE_RING0;

  descriptor.segment.available = 0;
  descriptor.segment.unused    = 0;

  descriptor.segment.zero1 = 0;
}

void GenericDescriptor::InitializeDataSegment(CPU_PRIVILEGE dpl) {
  descriptor.data.segment_limit0 = 0;
  descriptor.data.base_address0  = 0;
  descriptor.data.base_address1  = 0;
  descriptor.data.accessed       = 0;
  descriptor.data.writable       = 1;
  descriptor.data.expand_down    = 0;

  descriptor.data.user_type       = USER_SEGMENT_DESCRIPTOR_TYPE_DATA;
  descriptor.data.descriptor_type = DESCRIPTOR_TYPE_USER;
  descriptor.data.destination_privilege_level = dpl;

  descriptor.data.present              = 1;
  descriptor.data.segment_limit1       = 0;
  descriptor.data.unused_avail         = 0;
  descriptor.data.unused               = 0;
  descriptor.data.default_operand_size = 1;
  descriptor.data.granularity          = 0;
  descriptor.data.base_address2        = 0;

  // We store a copy of the descriptor right above + 8 bytes
  // this is because SYSCALL and sysret need their selectors
  // in a special order
  descriptor.data.copy = descriptor.raw.low;
}

void GenericDescriptor::InitializeCodeSegment(CPU_PRIVILEGE dpl) {
  descriptor.code.segment_limit0 = 0;  // Ignored in long mode
  descriptor.code.base_address0  = 0;  // Ignored in long mode
  descriptor.code.base_address1  = 0;  // Ignored in long mode
  descriptor.code.accessed       = 0;  // Ignored in long mode
  descriptor.code.readable       = 1;  // Ignored in long mode
  descriptor.code.conforming     = 0;

  descriptor.code.user_type       = USER_SEGMENT_DESCRIPTOR_TYPE_CODE;
  descriptor.code.descriptor_type = DESCRIPTOR_TYPE_USER;

  descriptor.code.destination_privilege_level = dpl;
  descriptor.code.present                     = 1;
  descriptor.code.segment_limit1              = 0;  // Ignored in long mode
  descriptor.code.unused_avail                = 0;
  descriptor.code.long_mode                   = 1;
  descriptor.code.default_operand_size        = 0;
  descriptor.code.granularity                 = 1;  // Ignored in long mode
  descriptor.code.base_address2               = 0;  // Ignored in long mode

  // We store a copy of the descriptor right above (+ 8 bytes)
  // this is because SYSCALL and SYSRET need their selectors
  // in a special order
  descriptor.data.copy = descriptor.raw.low;
}

void GenericDescriptor::InitializeGate(SYSTEM_SEGMENT_DESCRIPTOR_TYPE type,
                                       TargetFunction target,
                                       SegmentSelector selector,
                                       CPU_PRIVILEGE dpl,
                                       TSS_IST ist) {
  descriptor.gate.offset0 = (u32)(((vm_offset_t)target) >> 0x00) & 0x0000FFFF;
  descriptor.gate.offset1 = (u32)(((vm_offset_t)target) >> 0x10) & 0x0000FFFF;
  descriptor.gate.offset2 = (u32)(((vm_offset_t)target) >> 0x20) & 0xFFFFFFFF;

  descriptor.gate.selector    = selector;
  descriptor.gate.ist         = ist;
  descriptor.gate.system_type = type;  // MUST BE INTERRUPT OR TRAP
  descriptor.gate.dpl         = dpl;

  descriptor.gate.descriptor_type = DESCRIPTOR_TYPE_SYSTEM;

  descriptor.gate.present = 1;

  descriptor.gate.reserved0 = 0;
  descriptor.gate.zero0     = 0;
}
