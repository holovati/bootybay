#if 0
#define SIGNAL_INCLUDE_SIGPROP

#undef SIGNAL_INCLUDE_SIGPROP

#include <elf.h>
#include <mach/errno.h>
#include <mach/fcntl.h>
#include <mach/filedesc.h>

#include <mach/thread.h>
#include <mach/vm/vm_fault.h>
#include <mach/vm_param.h>
#include <malloc.h>
#include <string.h>
#include <strings.h>
#include <sys/resource.h>

#include <algorithm>
#include <kernel/zalloc.h>
#include <mach/bitops.h>
#include <mach/sys/wait.h>
#include <mach/time.h>
#include <mach/tty.h>
#include <mach/ucred.h>
#include <mach/vm/vm_kern.h>
#include <mach/vnode.h>
#include <string>
#include <sys/sysinfo.h>
#include <vector>

using proc_zone_t  = zone<process, 1024, 512>::type;
using pcred_zone_t = zone<pcred, 1024, 512>::type; /* Should not be here */

static proc_zone_t s_proc_zone;
static pcred_zone_t s_pcred_zone;

#define PIDHASH(pid) (&pidhashtbl[(pid)&pidhash])
static LIST_HEAD(pidhashhead, process) * pidhashtbl;
static size_t pidhash;

LIST_HEAD(proclist, process);
static struct proclist allproc;
static struct proclist zombproc;

/*
 * Structure associated with user cacheing.
 */
struct uidinfo
{
        LIST_ENTRY(uidinfo) ui_hash;
        uid_t ui_uid;
        i32 ui_proccnt;
};
#define UIHASH(uid)  (&uihashtbl[(uid)&uihash])
static LIST_HEAD(uihashhead, uidinfo) * uihashtbl;
static size_t uihash; /* size of hash table - 1 */

static size_t nprocs  = 1;  /* process 0 */
static size_t maxproc = 32; /* process 0 */

/* Used in fork_internal for finding a available pid */
static int nextpid;
static int pidchecked;

/* Components of the first process -- never freed. */
static struct session session0;
static struct pgrp pgrp0;
static process proc0;
static struct pcred cred0;
static struct filedesc0 filedesc0;
// struct plimit limit0;
// struct vmspace vmspace0;
// pthread_tcurproc = &proc0;

static kern_return_t timer_real_timeout(void *a_proc);

static kern_return_t timer_virtual_timeout(void *a_proc);

static kern_return_t timer_prof_timeout(void *a_proc);

static void initproc_init(process_t a_procp)
{
        s_proc_zone.init(&s_proc_zone, "proc");
        s_pcred_zone.init(&s_pcred_zone, "pcred");

        bzero(a_procp, sizeof *a_procp);

        /*
         * Create process 1 (the init process).
         */
        LIST_INSERT_HEAD(&allproc, a_procp, p_list);
        a_procp->p_pgrp = &pgrp0;
        // LIST_INIT(&pgrp0.pg_members);
        pgrp0.pg_members.init();
        // LIST_INSERT_HEAD(&pgrp0.pg_members, a_procp, p_pglist);
        pgrp0.pg_members.insert_head(a_procp);

        pgrp0.pg_session  = &session0;
        session0.s_count  = 1;
        session0.s_leader = a_procp;

        a_procp->p_flag = P_INMEM;
        a_procp->p_stat = SRUN;
        // a_procp->p_nice = NZERO;

        // bcopy("swapper", p->p_comm, sizeof("swapper"));

        /* Create credentials. */
        cred0.p_refcnt                        = 1;
        a_procp->p_cred                       = &cred0;
        a_procp->p_cred->pc_ucred             = crget();
        a_procp->p_cred->pc_ucred->cr_ngroups = 1; /* group 0 */

        /* Set pid and mach thread */
        a_procp->p_pid = 1;
        //
        a_procp->thread.tid              = 1;
        a_procp->thread.exit_signal      = SIGCHLD;
        a_procp->thread.kthread          = current_thread();
        a_procp->thread.kthread->pthread = &a_procp->thread;
        a_procp->thread.process          = a_procp;

        /* Create the file descriptor table. */
        struct filedesc0 *fdp    = &filedesc0;
        a_procp->p_fd            = &fdp->fd_fd;
        fdp->fd_fd.fd_refcnt     = 1;
        fdp->fd_fd.fd_cmask      = S_IWGRP | S_IWOTH;
        fdp->fd_fd.fd_ofiles     = fdp->fd_dfiles;
        fdp->fd_fd.fd_ofileflags = fdp->fd_dfileflags;
        fdp->fd_fd.fd_nfiles     = NDFILE;

        /*
         * Initialize signal state for process 0;
         * set to ignore signals that are ignored by default.
         */

        for (int i = 0; i < NSIG; i++)
        {
                if (s_sigprop[i] & SA_IGNORE && i != SIGCONT)
                {
                        a_procp->p_sigignore |= SIGMASK(i);
                }
        }
#if DAMIR
        /* Create the limits structures. */
        p->p_limit = &limit0;
        for (i = 0; i < sizeof(p->p_rlimit) / sizeof(p->p_rlimit[0]); i++)
                limit0.pl_rlimit[i].rlim_cur = limit0.pl_rlimit[i].rlim_max = RLIM_INFINITY;
        limit0.pl_rlimit[RLIMIT_NOFILE].rlim_cur  = NOFILE;
        limit0.pl_rlimit[RLIMIT_NPROC].rlim_cur   = MAXUPRC;
        i                                         = ptoa(cnt.v_free_count);
        limit0.pl_rlimit[RLIMIT_RSS].rlim_max     = i;
        limit0.pl_rlimit[RLIMIT_MEMLOCK].rlim_max = i;
        limit0.pl_rlimit[RLIMIT_MEMLOCK].rlim_cur = i / 3;
        limit0.p_refcnt                           = 1;

        /* Allocate a prototype map so we have something to fork. */
        p->p_vmspace       = &vmspace0;
        vmspace0.vm_refcnt = 1;
        pmap_pinit(&vmspace0.vm_pmap);
        vm_map_init(&p->p_vmspace->vm_map, round_page(VM_MIN_ADDRESS), trunc_page(VM_MAX_ADDRESS), TRUE);
        vmspace0.vm_map.pmap = &vmspace0.vm_pmap;
        p->p_addr            = proc0paddr; /* XXX */
        /*
         * We continue to place resource usage info and signal
         * actions in the user struct so they're pageable.
         */
        p->p_stats   = &p->p_addr->u_stats;
        p->p_sigacts = &p->p_addr->u_sigacts;
#endif

        a_procp->p_itimer_elt[ITIMER_REAL].param    = a_procp;
        a_procp->p_itimer_elt[ITIMER_VIRTUAL].param = a_procp;
        a_procp->p_itimer_elt[ITIMER_PROF].param    = a_procp;

        a_procp->p_itimer_elt[ITIMER_REAL].fcn    = timer_real_timeout;
        a_procp->p_itimer_elt[ITIMER_VIRTUAL].fcn = timer_virtual_timeout;
        a_procp->p_itimer_elt[ITIMER_PROF].fcn    = timer_prof_timeout;

        /*
         * Charge root for one process.
         */
        proc_change_count(0, 1);
}

/*
 * General routine to allocate a hash table.
 */
template <typename T> static T *hashinit(size_t elements, int type, size_t *hashmask)
{
        UNUSED(type);
        size_t hashsize;
        T *hashtbl;
        size_t i;

        if (elements <= 0)
                panic("hashinit: bad cnt");
        for (hashsize = 1; hashsize <= elements; hashsize <<= 1)
                continue;
        hashsize >>= 1;
        hashtbl = (T *)malloc(hashsize * sizeof(*hashtbl) /*, type, M_WAITOK*/);
        for (i = 0; i < hashsize; i++)
                LIST_INIT(&hashtbl[i]);
        *hashmask = hashsize - 1;
        return (hashtbl);
}

/*
 * Initialize global process hashing structures.
 */
process_t proc_init()
{
        LIST_INIT(&allproc);
        LIST_INIT(&zombproc);
        pidhashtbl = hashinit<pidhashhead>(maxproc / 4, /*M_PROC*/ 0, &pidhash);

        initproc_init(&proc0);

        proc_group_init(maxproc, &proc0);

        uihashtbl = hashinit<uihashhead>(maxproc / 16, /*M_PROC*/ 0, &uihash);

        nextpid    = 1;
        pidchecked = 0;

        return &proc0;
}

process_t process::current()
{
        pthread_t self = current_thread()->pthread;

        if (self)
                return self->process;
        else
                return nullptr;
}

#if 0
int proc_set_tid_address(pthread_t procp, int *tidptr)
{
        pthread_t self    = (pthread_t)procp;
        self->tid_address = tidptr;
        return self->tid;
}
#endif

/*
 * Manipulate signal mask.
 * Note that we receive new mask, not pointer,
 * and return old mask as return value;
 * the library stub does the rest.
 */
kern_return_t process::sigprocmask(int how, sigset_t *a_nsetp, sigset_t *a_osetp, size_t sigsetsize)
{
        /* If user space has different view of sigset_t bail out */
        if (sigsetsize != sizeof(sigset_t))
        {
                return -EINVAL;
        }

        /* Disallow any modification of the process sigset */
        spl_t s = splsched();

        /* Grab the old sigmask if the user wants it */
        int error = 0;
        if (a_osetp)
        {
                *a_osetp = p_sigmask;
        }

        /* Set the new sigmask if requested */
        if (!error && a_nsetp)
        {
                switch (how)
                {
                case SIG_BLOCK:
                        p_sigmask |= (*a_nsetp) & ~(SIGMASK(SIGKILL) | SIGMASK(SIGSTOP));
                        break;
                case SIG_UNBLOCK:
                        p_sigmask &= ~(*a_nsetp);
                        break;
                case SIG_SETMASK:
                        p_sigmask = (*a_nsetp) & ~(SIGMASK(SIGKILL) | SIGMASK(SIGSTOP));
                        break;
                default:
                        error = -EINVAL;
                        break;
                }
                // Setting a new sigmask might have unblocked a held signal.
                // Check for it and schedule an AST if needed
                if (!error && p_siglist & ~p_sigmask)
                {
                        thread_ast_set(thread.kthread, AST_UNIX);
                        // No need to check the thread context before propagating
                        // as we know we are running in the right context during syscalls
                        ast_propagate(thread.kthread, cpu_number());
                }
        }

        splx(s);

        return error;
}

int process::sigaction(int signum, struct ksigaction const *a_actp, struct ksigaction *a_oactp, size_t sigsetsize)
{
        /* If user space has different view of sigset_t bail out */
        if (sigsetsize != sizeof(sigset_t))
        {
                return -EINVAL;
        }

        // Sanity check( Is NSIG - 1???? )
        if (signum <= 0 || signum >= NSIG || signum == SIGKILL || signum == SIGSTOP)
        {
                return -EINVAL;
        }

        struct ksigaction *sa = &p_sigacts[signum];

        // If the caller wants the current action settings copy them out
        if (a_oactp)
        {
                *a_oactp = *sa;
        }

        if (a_actp == nullptr)
        {
                // User does not want to set any actions
                return 0;
        }

        // Copy in the new settings
        *sa = *a_actp;

        sa->mask &= ~(SIGMASK(SIGKILL) | SIGMASK(SIGSTOP));

        // Maybe splhigh?

        sigset_t sigmask = SIGMASK(signum);

#if DAMIR // We have sigacts in the proc structure not in uarea, so we don't
          // need this

        // The signal should restart the current syscall if SA_RESTART.
        // If not it should be interrupted
        if (sa->flags & SA_RESTART)
        {
                procp->ps_sigintr &= ~sigmask;
        }
        else
        {
                procp->ps_sigintr |= sigmask;
        }

        // Should the signal handler on an alternate signal stack provided by
        // sigaltstack(2)? If an alternate stack is not available, the default stack
        // will be used.
        if (sa->flags & SA_ONSTACK)
        {
                procp->ps_sigonstack |= sigmask;
        }
        else
        {
                procp->ps_sigonstack &= ~sigmask;
        }
#endif

        // If signum is SIGCHLD, do not receive notification when child processes stop
        // (i.e., when they receive one of SIGSTOP, SIGTSTP, SIGTTIN or SIGTTOU) or
        // resume (i.e., they receive SIGCONT) (see wait(2)).
        if (signum == SIGCHLD)
        {
                if (sa->flags & SA_NOCLDSTOP)
                {
                        p_flag |= P_NOCLDSTOP;
                }
                else
                {
                        p_flag &= ~P_NOCLDSTOP;
                }
        }

        /*
         * Set bit in p_sigignore for signals that are set to SIG_IGN,
         * and for signals set to SIG_DFL where the default is to ignore.
         * However, don't put SIGCONT in p_sigignore,
         * as we have to restart the process.
         */
        if (sa->handler == SIG_IGN || (sa->handler == SIG_DFL && s_sigprop[signum] & SA_IGNORE))
        {
                p_siglist &= ~sigmask; /* never to be seen again */

                if (signum != SIGCONT)
                {
                        p_sigignore |= sigmask; /* easier in psignal */
                }

                p_sigcatch &= ~sigmask;
        }
        else
        {
                p_sigignore &= ~sigmask;
                if (sa->handler == SIG_DFL)
                {
                        p_sigcatch &= ~sigmask;
                }
                else
                {
                        p_sigcatch |= sigmask;
                }
        }

        return 0;
}

int proc_getpid(pthread_t a_procp)
{
        /**/
        // log_set_level(LOG_TRACE);
        return (int)a_procp->process->p_pid;
}

int proc_gettid(pthread_t a_procp)
{
        // log_set_level(LOG_TRACE);
        return (int)a_procp->tid;
}

int proc_getppid(pthread_t a_pthread) { return a_pthread->process->p_pptr ? (int)a_pthread->process->p_pptr->p_pid : 0; }

bool process::is_session_leader() { return p_pgrp->pg_session->s_leader == this; }

process_t process::find(pid_t pid)
{
        process_t p;

        for (p = PIDHASH(pid)->lh_first; p != 0; p = p->p_hash.le_next)
        {
                if (p->p_pid == pid)
                {
                        return (p);
                }
        }
        return nullptr;
}

/*
 * Is p an inferior of the current process?
 */
int proc_is_inferior(process_t p)
{
        for (; p != process::current(); p = p->p_pptr)
                if (p->p_pid == 0)
                        return (0);
        return (1);
}

/*
 * Change the count associated with number of processes
 * a given user is using.
 */
int proc_change_count(uid_t uid, int diff)
{
        UNUSED(uid);
        UNUSED(diff);
#if DAMIR
        struct uidinfo *uip;
        struct uihashhead *uipp;

        uipp = UIHASH(uid);
        for (uip = uipp->lh_first; uip != 0; uip = uip->ui_hash.le_next)
                if (uip->ui_uid == uid)
                        break;
        if (uip)
        {
                uip->ui_proccnt += diff;
                if (uip->ui_proccnt > 0)
                        return (uip->ui_proccnt);
                if (uip->ui_proccnt < 0)
                        panic("chgproccnt: procs < 0");
                LIST_REMOVE(uip, ui_hash);
                FREE(uip, M_PROC);
                return (0);
        }
        if (diff <= 0)
        {
                if (diff == 0)
                        return (0);
                panic("chgproccnt: lost user");
        }
        MALLOC(uip, struct uidinfo *, sizeof(*uip), M_PROC, M_WAITOK);
        LIST_INSERT_HEAD(uipp, uip, ui_hash);
        uip->ui_uid     = uid;
        uip->ui_proccnt = diff;
        return (diff);
#endif
        return 0;
}

/*
 * Put the argument process into the stopped state and notify the parent
 * via wakeup.  Signals are handled elsewhere.  The process must not be
 * on the run queue.
 */
static void proc_stop(pthread_t p)
{
        panic("Does not work");
#if 0
        p->p_stat = SSTOP;
        p->p_flag &= ~P_WAITED;
        wakeup(p->p_pptr);
#endif
}

/*
 * Send the signal to the process.  If the signal has an action, the action
 * is usually performed by the target process rather than the caller; we add
 * the signal to the set of pending signals for the process.
 *
 * Exceptions:
 *   o When a stop signal is sent to a sleeping process that takes the
 *     default action, the process is stopped without awakening it.
 *   o SIGCONT restarts stopped processes (or puts them back to sleep)
 *     regardless of the signal action (eg, blocked or ignored).
 *
 * Other ignored signals are discarded immediately.
 */
void process::signal(int signum)
{
        spl_t s;
        int prop;
        sig_t action;
        sigset_t mask;

        if (signum >= NSIG || signum == 0)
        {
                panic("psignal signal number");
        }

        mask = SIGMASK(signum);

        prop = s_sigprop[signum];
        /*
         * If proc is traced, always give parent a chance.
         */
        if (p_flag & P_TRACED)
                action = SIG_DFL;
        else
        {
                /*
                 * If the signal is being ignored,
                 * then we forget about it immediately.
                 * (Note: we don't set SIGCONT in p_sigignore,
                 * and if it is set to SIG_IGN,
                 * action will be SIG_DFL here.)
                 */
                if (p_sigignore & mask)
                {
                        return;
                }

                if (p_sigmask & mask)
                {
                        action = SIG_HOLD;
                }
                else if (p_sigcatch & mask)
                {
                        action = SIG_CATCH;
                }
                else
                {
                        action = SIG_DFL;
                }
        }

#if DAMIR // We don't have nice
        if (p_nice > NZERO && action == SIG_DFL && (prop & SA_KILL) && (p_flag & P_TRACED) == 0)
        {
                p_nice = NZERO;
        }
#endif

        if (prop & SA_CONT)
        {
                p_siglist &= ~STOPSIGMASK;
        }

        if (prop & SA_STOP)
        {
                /*
                 * If sending a tty stop signal to a member of an orphaned
                 * process group, discard the signal here if the action
                 * is default; don't stop the process below if sleeping,
                 * and don't clear any pending SIGCONT.
                 */
                if (prop & SA_TTYSTOP && p_pgrp->pg_jobc == 0 && action == SIG_DFL)
                {
                        return;
                }

                p_siglist &= ~CONTSIGMASK;
        }

        p_siglist |= mask;

        /*
         * Defer further processing for signals which are held,
         * except that stopped processes must be continued by SIGCONT.
         */
        if (action == SIG_HOLD && ((prop & SA_CONT) == 0 || p_stat != SSTOP))
        {
                return;
        }

        s = splhigh();
        switch (p_stat)
        {
        case SSLEEP:
                /*
                 * If process is sleeping uninterruptibly
                 * we can't interrupt the sleep... the signal will
                 * be noticed when the process returns through
                 * trap() or syscall().
                 */
                if ((p_flag & P_SINTR) == 0)
                {
                        goto out;
                }
                /*
                 * Process is sleeping and traced... make it runnable
                 * so it can discover the signal in issignal() and stop
                 * for the parent.
                 */
                if (p_flag & P_TRACED)
                {
                        goto run;
                }
                /*
                 * If SIGCONT is default (or ignored) and process is
                 * asleep, we are finished; the process should not
                 * be awakened.
                 */
                if ((prop & SA_CONT) && action == SIG_DFL)
                {
                        p_siglist &= ~mask;
                        goto out;
                }
                /*
                 * When a sleeping process receives a stop
                 * signal, process immediately if possible.
                 * All other (caught or default) signals
                 * cause the process to run.
                 */
                if (prop & SA_STOP)
                {
                        if (action != SIG_DFL)
                        {
                                goto runfast;
                        }
                        /*
                         * If a child holding parent blocked,
                         * stopping could cause deadlock.
                         */
                        if (p_flag & P_PPWAIT)
                        {
                                goto out;
                        }

                        p_siglist &= ~mask;

                        p_xstat = signum;

                        if ((p_pptr->p_flag & P_NOCLDSTOP) == 0)
                        {
                                p_pptr->signal(SIGCHLD);
                        }

                        proc_stop(&thread);
                        goto out;
                }
                else
                        goto runfast;
                /*NOTREACHED*/

        case SSTOP:
                /*
                 * If traced process is already stopped,
                 * then no further action is necessary.
                 */
                if (p_flag & P_TRACED)
                        goto out;

                /*
                 * Kill signal always sets processes running.
                 */
                if (signum == SIGKILL)
                        goto runfast;

                if (prop & SA_CONT)
                {
                        /*
                         * If SIGCONT is default (or ignored), we continue the
                         * process but don't leave the signal in p_siglist, as
                         * it has no further action.  If SIGCONT is held, we
                         * continue the process and leave the signal in
                         * p_siglist.  If the process catches SIGCONT, let it
                         * handle the signal itself.  If it isn't waiting on
                         * an event, then it goes back to run state.
                         * Otherwise, process goes back to sleep state.
                         */
                        if (action == SIG_DFL)
                        {
                                p_siglist &= ~mask;
                        }

                        if (action == SIG_CATCH)
                        {
                                goto runfast;
                        }

                        if (thread.kthread->wait_event == 0)
                        {
                                goto run;
                        }

                        p_stat = SSLEEP;

                        goto out;
                }

                if (prop & SA_STOP)
                {
                        /*
                         * Already stopped, don't need to stop again.
                         * (If we did the shell could get confused.)
                         */
                        p_siglist &= ~mask; /* take it away */
                        goto out;
                }

                /*
                 * If process is sleeping interruptibly, then simulate a
                 * wakeup so that when it is continued, it will be made
                 * runnable and can look at the signal.  But don't make
                 * the process runnable, leave it stopped.
                 */
                if (thread.kthread->wait_event && p_flag & P_SINTR)
                {
                        clear_wait(thread.kthread, 0, true);
                        // unsleep(p);
                }
                goto out;

        default:
                /*
                 * SRUN, SIDL, SZOMB do nothing with the signal,
                 * other than kicking ourselves if we are running.
                 * It will either never be noticed, or noticed very soon.
                 */
                if (thread.kthread == current_thread())
                {
                        // signotify(p);
                        thread_ast_set(thread.kthread, AST_UNIX);
                        ast_propagate(thread.kthread, cpu_number());
                }
                goto out;
        }
        /*NOTREACHED*/

runfast:
/*
 * Raise priority to at least PUSER.
 */
#if DAMIR
        if (p_priority > PUSER)
                p_priority = PUSER;
#endif
run:
#if DAMIR
        setrunnable(this);
#else
        clear_wait(thread.kthread, THREAD_INTERRUPTED, true);
        thread_ast_set(thread.kthread, AST_UNIX);

        if (current_thread() == thread.kthread)
        {
                ast_propagate(thread.kthread, cpu_number());
        }

#endif

out:
        splx(s);
}

/*
 * Warn that a system table is full.
 */
static void tablefull(const char *tab) { log_error("%s: table is full", tab); }
static int get_next_pid()
{
        /*
         * Find an unused process ID.  We remember a range of unused IDs
         * ready to use (from nextpid+1 through pidchecked-1).
         */
        nextpid++;
retry:
        /*
         * If the process ID prototype has wrapped around,
         * restart somewhat above 0, as the low-numbered procs
         * tend to include daemons that don't exit.
         */
        if (nextpid >= PID_MAX)
        {
                nextpid    = 100;
                pidchecked = 0;
        }
        if (nextpid >= pidchecked)
        {
                int doingzomb = 0;

                pidchecked = PID_MAX;
                /*
                 * Scan the active and zombie procs to check whether this pid
                 * is in use.  Remember the lowest pid that's greater
                 * than nextpid, so we can avoid checking for a while.
                 */
                process_t p2 = allproc.lh_first;
        again:
                for (; p2 != 0; p2 = p2->p_list.le_next)
                {
                        while (p2->p_pid == nextpid || p2->p_pgrp->pg_id == nextpid)
                        {
                                nextpid++;
                                if (nextpid >= pidchecked)
                                        goto retry;
                        }
                        if (p2->p_pid > nextpid && pidchecked > p2->p_pid)
                                pidchecked = (int)p2->p_pid;
                        if (p2->p_pgrp->pg_id > nextpid && pidchecked > p2->p_pgrp->pg_id)
                                pidchecked = p2->p_pgrp->pg_id;
                }
                if (!doingzomb)
                {
                        doingzomb = 1;
                        p2        = zombproc.lh_first;
                        goto again;
                }
        }

        nprocs++;

        return nextpid;
}

static void fork_init_child(process_t a_parent_procp, process_t a_child_procp)
{
        // Zero these
        // a_procp->p_oppid     = 0;       /* Save parent pid during ptrace. XXX */
        // a_child_procp->p_dupfd = 0; /* Sideways return value from fdopen. XXX */
        // a_procp->p_estcpu    = 0;       /* Time averaged value of p_cpticks. */
        // a_procp->p_cpticks   = 0;       /* Ticks of cpu time. */
        // a_procp->p_pctcpu    = 0;       /* %cpu for this process during p_swtime
        // */ a_procp->p_wchan     = 0;       /* Sleep address. */ a_procp->p_wmesg
        // = 0;       /* Reason for sleep. */ a_procp->p_swtime    = 0;       /*
        // Time swapped in or out. */ a_procp->p_slptime   = 0;       /* Time since
        // last blocked. */ a_procp->p_realtimer = 0;       /* Alarm timer. */
        // a_procp->p_rtime     = 0;       /* Real time. */
        // a_procp->p_uticks    = 0;       /* Statclock hits in user mode. */
        // a_procp->p_sticks    = 0;       /* Statclock hits in system mode. */
        // a_procp->p_iticks    = 0;       /* Statclock hits processing intr. */
        // a_procp->p_traceflag = 0;       /* Kernel trace points. */
        // a_procp->p_tracep    = 0;       /* Trace to vnode. */
        a_child_procp->p_siglist = 0; /* Signals arrived but not delivered. */
        // a_procp->p_textvp  = nullptr; /* Vnode of executable. */
        // a_procp->p_locks        = 0;     /* DEBUG: lockmgr count of held locks */
        // a_procp->p_simple_locks = 0;     /* DEBUG: count of held simple locks */
        // a_procp->p_spare        = 0 [2]; /* pad to 256, avoid shifting eproc. */

        // copy these
        a_child_procp->p_sigmask = a_parent_procp->p_sigmask; /* Current signal mask. */

        a_child_procp->p_sigignore = a_parent_procp->p_sigignore; /* Signals being ignored. */

        a_child_procp->p_sigcatch = a_parent_procp->p_sigcatch; /* Signals being caught by user. */

        a_child_procp->ps_oldmask = a_parent_procp->ps_oldmask; /* saved mask from before sigpause */

        memcpy(a_child_procp->p_sigacts, a_parent_procp->p_sigacts, sizeof(a_parent_procp->p_sigacts));

        a_child_procp->p_sigaltstack = a_parent_procp->p_sigaltstack; /* sp & on stack state variable */

        bzero(a_child_procp->p_itimer_elt, sizeof(a_child_procp->p_itimer_elt));

        a_child_procp->p_itimer_elt[ITIMER_REAL].param    = a_child_procp;
        a_child_procp->p_itimer_elt[ITIMER_VIRTUAL].param = a_child_procp;
        a_child_procp->p_itimer_elt[ITIMER_PROF].param    = a_child_procp;

        a_child_procp->p_itimer_elt[ITIMER_REAL].fcn    = timer_real_timeout;
        a_child_procp->p_itimer_elt[ITIMER_VIRTUAL].fcn = timer_virtual_timeout;
        a_child_procp->p_itimer_elt[ITIMER_PROF].fcn    = timer_prof_timeout;

        a_child_procp->p_itimer[ITIMER_REAL]    = {};
        a_child_procp->p_itimer[ITIMER_VIRTUAL] = {};
        a_child_procp->p_itimer[ITIMER_PROF]    = {};

        // */
        // a_from->p_priority  = a_to->p_priority;  /* Process priority. */
        // a_from->p_usrpri
        //    = a_to->p_usrpri;          /* User-priority based on p_cpu and p_nice.
        //    */
        // a_from->p_nice = a_to->p_nice; /* Process "nice" value. */
        // a_from->p_comm = a_to->p_comm[MAXCOMLEN + 1];
        a_child_procp->vm_maxsaddr = a_parent_procp->vm_maxsaddr;
        a_child_procp->vm_ssize    = a_parent_procp->vm_ssize;

        a_child_procp->p_pgrp = a_parent_procp->p_pgrp; /* Pointer to process group. */
}

#if 0
static int fork_internal(process_t p1, bool isvfork, natural_t a_vfork_sp)
{
        uid_t uid;
        process_t *hash;
        UNUSED(hash);
        int count;
        UNUSED(count);

        /*
         * Although process entries are dynamically created, we still keep
         * a global limit on the maximum number we will create.  Don't allow
         * a nonprivileged user to use the last process; don't let root
         * exceed the limit. The variable nprocs is the current number of
         * processes, maxproc is the limit.
         */
        uid = p1->p_cred->p_ruid;
        if ((nprocs >= maxproc - 1 && uid != 0) || nprocs >= maxproc)
        {
                tablefull("proc");
                return -EAGAIN;
        }

        /*
         * Increment the count of procs running with this uid. Don't allow
         * a nonprivileged user to exceed their current limit.
         */
        count = proc_change_count(uid, 1);
#if DAMIR
        if (uid != 0 && count > p1->p_rlimit[RLIMIT_NPROC].rlim_cur)
        {
                proc_change_count(uid, -1);
                return -EAGAIN;
        }
#endif
#if DAMIR
        /* Allocate new proc. */
        MALLOC(newproc, pthread_t, sizeof(struct proc), M_PROC, M_WAITOK);
#else
        process_t p2 = s_proc_zone.alloc();
        bzero(p2, sizeof(*p2));
#endif

        p2->p_stat     = SIDL; /* protect against others */
        p2->p_pid      = get_next_pid();
        p2->thread.tid = p2->thread.tid;
        LIST_INSERT_HEAD(&allproc, p2, p_list);

#if DAMIR
        /* Mach manages thease */
        /* Doubly-linked run/sleep queue. */
        p2->p_forw = p2->p_back = NULL; /* shouldn't be necessary */
#endif
        LIST_INSERT_HEAD(PIDHASH(p2->p_pid), p2, p_hash);

        /*
         * Make a proc table entry for the new process.
         * Start by zeroing the section of proc that is zero-initialized,
         * then copy the section that is copied directly from the parent.
         */
#if DAMIR
        bzero(&p2->p_startzero, (size_t)(((vm_offset_t)&p2->p_endzero) - ((vm_offset_t)&p2->p_startzero)));

        bcopy(&p1->p_startcopy, &p2->p_startcopy, (size_t)(((vm_offset_t)&p2->p_endcopy) - ((vm_offset_t)&p2->p_startcopy)));

#else
        fork_init_child(p1, p2);

#endif
#if DAMIR
        /*
         * Duplicate sub-structures as needed.
         * Increase reference counts on shared objects.
         * The p_stats and p_sigacts substructs are set in vm_fork.
         */
        p2->p_flag = P_INMEM;
        if (p1->p_flag & P_PROFIL)
        {
                startprofclock(p2);
        }
#endif

#if DAMIR
        MALLOC(p2->p_cred, struct pcred *, sizeof(struct pcred), M_SUBPROC, M_WAITOK);
#else
        p2->p_cred = s_pcred_zone.alloc();
#endif
        memcpy(p2->p_cred, p1->p_cred, sizeof(*p2->p_cred));
        p2->p_cred->p_refcnt = 1;

        crhold(p1->p_cred->pc_ucred);
#if DAMIR
        /* bump references to the text vnode (for procfs) */
        p2->p_textvp = p1->p_textvp;
        if (p2->p_textvp)
        {
                VREF(p2->p_textvp);
        }
#endif
        p2->p_fd = fdcopy(&p1->thread);

#if DAMIR
        /*
         * If p_limit is still copy-on-write, bump refcnt,
         * otherwise get a copy that won't be modified.
         * (If PL_SHAREMOD is clear, the structure is shared
         * copy-on-write.)
         */
        if (p1->p_limit->p_lflags & PL_SHAREMOD)
        {
                p2->p_limit = limcopy(p1->p_limit);
        }
        else
        {
                p2->p_limit = p1->p_limit;
                p2->p_limit->p_refcnt++;
        }
#endif
        if (p1->p_pgrp->pg_session->s_ttyvp != NULL && p1->p_flag & P_CONTROLT)
        {
                p2->p_flag |= P_CONTROLT;
        }

        if (isvfork)
        {
                p2->p_flag |= P_PPWAIT;
        }

        LIST_INSERT_AFTER(p1, p2, p_pglink);
        p2->p_pptr = p1;
        // LIST_INSERT_HEAD(&p1->p_children, p2, p_sibling);
        p1->p_children.insert_head(p2);
        // LIST_INIT(&p2->p_children);
        p2->p_children.init();

        task_t p2_task = nullptr;
        kern_return_t res;

        if (isvfork == false)
        {

                res = task_create(p1->thread.kthread->task, TASK_CREATE_INHERIT_VMMAP, &p2_task);

                if (res != KERN_SUCCESS)
                {
                        panic("Mach task creation failed");
                }
        }
        else
        {
                p2_task = p1->thread.kthread->task;
        }

        res = thread_create(p2_task, &p2->thread.kthread);

        if (res != KERN_SUCCESS)
        {
                panic("Mach thread creation failed");
        }

        p2->thread.kthread->pthread = &p2->thread;
        *p2->thread.kthread->pcb    = *p1->thread.kthread->pcb;
        p2->thread.exit_signal      = SIGCHLD;
        p2->thread.process          = p2;
        p2->thread.tid_address      = p1->thread.tid_address;
        p2->thread.tid              = p2->p_pid;

        if (isvfork && a_vfork_sp)
        {
                p2->thread.kthread->pcb->iss.return_rsp = a_vfork_sp;
        }

        // Should not be here
        p2->thread.kthread->pcb->iss.rax = 0;

        p2->p_stat = SRUN;

        thread_start(p2->thread.kthread, locore_exception_return);

        thread_resume(p2->thread.kthread);

#ifdef KTRACE
        /*
         * Copy traceflag and tracefile if enabled.
         * If not inherited, these were zeroed above.
         */
        if (p1->p_traceflag & KTRFAC_INHERIT)
        {
                p2->p_traceflag = p1->p_traceflag;
                if ((p2->p_tracep = p1->p_tracep) != NULL)
                        VREF(p2->p_tracep);
        }
#endif

#if DAMIR
        /*
         * This begins the section where we must prevent the parent
         * from being swapped.
         */
        p1->p_flag |= P_NOSWAP;
        /*
         * Set return values for child before vm_fork,
         * so they can be copied to child stack.
         * We return parent pid, and mark as child in retval[1].
         * NOTE: the kernel stack may be at a different location in the child
         * process, and thus addresses of automatic variables (including retval)
         * may be invalid after vm_fork returns in the child process.
         */
        retval[0] = p1->p_pid;
        retval[1] = 1;
        if (vm_fork(p1, p2, isvfork))
        {
                /*
                 * Child process.  Set start time and get to work.
                 */
                splclock();
                p2->p_stats->p_start = time;
                spl0();
                p2->p_acflag = AFORK;
                return (0);
        }

        /*
         * Make child runnable and add to run queue.
         */
        splhigh();
        p2->p_stat = SRUN;
        setrunqueue(p2);
        spl0();

        /*
         * Now can be swapped.
         */
        p1->p_flag &= ~P_NOSWAP;

        /*
         * Preserve synchronization semantics of vfork.  If waiting for
         * child to exec or exit, set P_PPWAIT on child, and sleep on our
         * proc (in case of exit).
         */
        if (isvfork)
        {
                while (p2->p_flag & P_PPWAIT)
                {
                        tsleep(p1, PWAIT, "ppwait", 0);
                }
        }

        /*
         * Return child pid to parent process,
         * marking us as parent via retval[1].
         */
        retval[0] = p2->p_pid;
        retval[1] = 0;
#endif

        /*
         * Preserve synchronization semantics of vfork.  If waiting for
         * child to exec or exit, set P_PPWAIT on child, and sleep on our
         * proc (in case of exit).
         */
        if (isvfork)
        {
                while (p2->p_flag & P_PPWAIT)
                {
                        tsleep(p1, PWAIT, "ppwait", 0);
                }
        }

        return (int)p2->p_pid;
}

int proc_fork(pthread_t p) { return fork_internal(p->process, false, 0); }

int proc_vfork(pthread_t p) { return fork_internal(p->process, true, 0); }

int proc_vfork_clone(pthread_t p, natural_t a_sp) { return fork_internal(p->process, true, a_sp); }

int proc_wait4(pthread_t a_procp, pid_t a_pid, int *stat_addr, int options, struct rusage *usage)
{
        int nfound;
        process_t p, *t;
        UNUSED(t);
        int error;
        int retval;
        UNUSED(retval);

        if (a_pid == 0)
        {
                a_pid = -(a_procp->process->p_pgrp->pg_id);
        }

#ifdef notyet
        if (uap->options & ~(WUNTRACED | WNOHANG))
                return -EINVAL;
#endif

loop:
        nfound = 0;
        for (p = a_procp->process->p_children.lh_first; p != nullptr; p = p->p_sibling.le_next)
        {
                if (a_pid != WAIT_ANY && p->p_pid != a_pid && p->p_pgrp->pg_id != -a_pid)
                {
                        continue;
                }

                nfound++;
                if (p->p_stat == SZOMB)
                {
                        // retval = p->p_pid;

                        if (/*uap->status*/ stat_addr)
                        {
#if DAMIR
                                status = p->p_xstat; /* convert to int */
#endif
                                int xstat = p->p_xstat;
                                error     = copyout(&xstat, stat_addr, sizeof *stat_addr);
                                if (error)
                                {
                                        return (error);
                                }
                        }
#if DAMIR
                        if (uap->rusage && (error = copyout((caddr_t)p->p_ru, (caddr_t)uap->rusage, sizeof(struct rusage))))
                                return (error);
                        /*
                         * If we got the child via a ptrace 'attach',
                         * we need to give it back to the old parent.
                         */
                        if (p->p_oppid && (t = pfind(p->p_oppid)))
                        {
                                p->p_oppid = 0;
                                proc_reparent(p, t);
                                psignal(t, SIGCHLD);
                                wakeup((caddr_t)t);
                                return (0);
                        }
                        p->p_xstat = 0;
                        ruadd(&q->p_stats->p_cru, p->p_ru);
                        FREE(p->p_ru, M_ZOMBIE);
#else
                        // Calculations are wrong!!
                        a_procp->process->p_usage.ru_utime.tv_sec += p->p_usage.ru_utime.tv_sec;
                        a_procp->process->p_usage.ru_utime.tv_usec += p->p_usage.ru_utime.tv_usec;

                        a_procp->process->p_usage.ru_stime.tv_sec += p->p_usage.ru_stime.tv_sec;
                        a_procp->process->p_usage.ru_stime.tv_usec += p->p_usage.ru_stime.tv_usec;

                        if (usage)
                        {
                                panic("Not ready");
                        }

#endif
                        /*
                         * Decrement the count of procs running with this uid.
                         */
                        proc_change_count(p->p_cred->p_ruid, -1);

                        /*
                         * Free up credentials.
                         */
                        if (--p->p_cred->p_refcnt == 0)
                        {
                                crfree(p->p_cred->pc_ucred);
#if DAMIR
                                FREE(p->p_cred, M_SUBPROC);
#else
                                //        free(p->p_cred);
                                s_pcred_zone.free(p->p_cred);
#endif
                        }

#if DAMIR
                        /*
                         * Release reference to text vnode
                         */
                        if (p->p_textvp)
                                vrele(p->p_textvp);
#endif
                        /*
                         * Finally finished with old proc entry.
                         * Unlink it from its process group and free it.
                         */
                        proc_group_leave(&p->thread);
                        LIST_REMOVE(p, p_list); /* off zombproc */
                        LIST_REMOVE(p, p_sibling);
#if DAMIR
                        /*
                         * Give machine-dependent layer a chance
                         * to free anything that cpu_exit couldn't
                         * release while still running in process context.
                         */

                        cpu_wait(p);
#endif

#if DAMIR
                        FREE(p, M_PROC);
#else
                        // free(p);
                        int pid = (int)p->p_pid;
                        s_proc_zone.free(p);
#endif
                        nprocs--;
                        return pid;
                }

                if (p->p_stat == SSTOP && (p->p_flag & P_WAITED) == 0 && (p->p_flag & P_TRACED || options & WUNTRACED))
                {
                        p->p_flag |= P_WAITED;
                        int pid = p->p_pid;

                        if (stat_addr)
                        {
                                int status = __W_STOPCODE(p->p_xstat);
                                error      = emerixx::uio::copyout(&status, stat_addr);
                        }
                        else
                        {
                                error = 0;
                        }
                        return (error);
                }
        }

        if (nfound == 0)
        {
                return -ECHILD;
        }

        if (options & WNOHANG)
        {
                return 0;
        }

        error = tsleep(a_procp, PWAIT | PCATCH, "wait", 0);
        if (error)
        {
                return (error);
        }

        goto loop;
}

ZONE_DEFINE(pthread, s_pthread_zone, 0x100, 010, ZONE_EXHAUSTIBLE, 0x10);

kern_return_t pthread_create(pthread_t *a_pthread_out)
{
        *a_pthread_out = s_pthread_zone.alloc();

        if (*a_pthread_out != nullptr)
        {
                (*a_pthread_out)->tid         = get_next_pid();
                (*a_pthread_out)->tid_address = nullptr;
        }

        return *a_pthread_out ? KERN_SUCCESS : KERN_FAIL(ENOMEM);
}

/*
 * Exit: deallocate address space and other resources, change proc state
 * to zombie, and unlink proc from allproc and parent's lists.  Save exit
 * status and rusage for wait().  Check for child processes and orphan them.
 */
static int proc_exit_internal(pthread_t a_procp, int a_ec)
{
        process_t q, nq;
        /*
         * Release a filedesc structure.
         */
        if (a_procp->process->p_pid == 1)
        {
                panic("init died (signal %d, exit %d)", WTERMSIG(a_ec), WEXITSTATUS(a_ec));
        }

#ifdef PGINPROF
        vmsizmon();
#endif

        if (a_procp->process->p_flag & P_PROFIL)
        {
#if DAMIR
                stopprofclock(p);
#endif
        }

#if DAMIR
        MALLOC(p->p_ru, struct rusage *, sizeof(struct rusage), M_ZOMBIE, M_WAITOK);
#endif
        /*
         * If parent is waiting for us to exit or exec,
         * P_PPWAIT is set; we will wakeup the parent below.
         */
        bool isvfork = (a_procp->process->p_flag & P_PPWAIT) != 0;

        a_procp->process->p_flag &= ~(P_TRACED | P_PPWAIT);
        a_procp->process->p_flag |= P_WEXIT;
        a_procp->process->p_sigignore = ~0;
        a_procp->process->p_siglist   = 0;
#if DAMIR
        untimeout(realitexpire, (caddr_t)p);
#else
        for (timer_elt &t : a_procp->process->p_itimer_elt)
        {
                reset_timeout(&t);
        }

#endif
        /*
         * Close open files and release open-file table.
         * This may block!
         */
        fdfree(a_procp);

#if DAMIR
        /* The next two chunks should probably be moved to vmspace_exit. */
        vm = p->p_vmspace;
#ifdef SYSVSHM
        if (vm->vm_shm)
                shmexit(p);
#endif
#endif
#ifdef DAMIR
        /*
         * Release user portion of address space.
         * This releases references to vnodes,
         * which could cause I/O if the file has been unlinked.
         * Need to do this early enough that we can still sleep.
         * Can't free the entire vmspace as the kernel stack
         * may be mapped within that space also.
         */

        if (vm->vm_refcnt == 1)
                vm_map_remove(&vm->vm_map, VM_MIN_ADDRESS, VM_MAXUSER_ADDRESS);
#endif

        if (a_procp->process->is_session_leader())
        {
                struct session *sp = a_procp->process->p_pgrp->pg_session;

                if (sp->s_ttyvp)
                {
                        /*
                         * Controlling process.
                         * Signal foreground pgrp,
                         * drain controlling terminal
                         * and revoke access to controlling terminal.
                         */
                        if (sp->s_ttyp->t_session == sp)
                        {
                                if (sp->s_ttyp->t_pgrp)
                                {
                                        /* Moving
                                        sp->s_ttyp->t_pgrp->signal(SIGHUP, 1);
                                        */
                                }

                                ttywait(sp->s_ttyp);
                                /*
                                 * The tty could have been revoked
                                 * if we blocked.
                                 */
                                if (sp->s_ttyvp)
                                {
#ifdef DAMIR
                                        VOP_REVOKE(sp->s_ttyvp, REVOKEALL);
#endif
                                }
                        }
                        if (sp->s_ttyvp)
                        {
                                sp->s_ttyvp->unreference();
                        }

                        sp->s_ttyvp = NULL;
                        /*
                         * s_ttyp is not zero'd; we use this to indicate
                         * that the session once had a controlling terminal.
                         * (for logging and informational purposes)
                         */
                }
                sp->s_leader = NULL;
        }
#if DAMIR
        fixjobc(p, p->p_pgrp, 0);
        p->p_rlimit[RLIMIT_FSIZE].rlim_cur = RLIM_INFINITY;
        acct_process(p);
#ifdef KTRACE
        /*
         * release trace file
         */
        p->p_traceflag = 0; /* don't trace the vrele() */
        if (p->p_tracep)
                vrele(p->p_tracep);
#endif
#endif
        /*
         * Remove proc from allproc queue and pidhash chain.
         * Place onto zombproc.  Unlink from parent's child list.
         */
        LIST_REMOVE(a_procp->process, p_list);
        LIST_INSERT_HEAD(&zombproc, a_procp->process, p_list);
        a_procp->process->p_stat = SZOMB;

        LIST_REMOVE(a_procp->process, p_hash);

        q = a_procp->process->p_children.lh_first;

        if (q)
        { /* only need this if any child is S_ZOMB */
                wakeup(&proc0);
        }

        for (; q != 0; q = nq)
        {
                nq = q->p_sibling.le_next;
                LIST_REMOVE(q, p_sibling);
                // LIST_INSERT_HEAD(&proc0.p_children, q, p_sibling);
                proc0.p_children.insert_head(q);
                q->p_pptr = &proc0;
                /*
                 * Traced processes are killed
                 * since their existence means someone is screwing up.
                 */
                if (q->p_flag & P_TRACED)
                {
                        q->p_flag &= ~P_TRACED;
                        q->signal(SIGKILL);
                }
        }

        /*
         * Save exit status and final rusage info, adding in child rusage
         * info and self times.
         */
        a_procp->process->p_xstat = a_ec;
#if DAMIR
        *p->p_ru = p->p_stats->p_ru;
        calcru(p, &p->p_ru->ru_utime, &p->p_ru->ru_stime, NULL);
        ruadd(p->p_ru, &p->p_stats->p_cru);
#else
        time_value_t user_time;
        time_value_t system_time;
        spl_t opl = splsched();
        thread_read_times(a_procp->kthread, &user_time, &system_time);
        splx(opl);

        a_procp->process->p_usage.ru_utime.tv_sec  = user_time.seconds;
        a_procp->process->p_usage.ru_utime.tv_usec = user_time.microseconds;

        a_procp->process->p_usage.ru_stime.tv_sec  = system_time.seconds;
        a_procp->process->p_usage.ru_stime.tv_usec = system_time.microseconds;
#endif
        /*
         * Notify parent that we're gone.
         */
        a_procp->process->p_pptr->signal(SIGCHLD);
        wakeup(a_procp->process->p_pptr);

#if DAMIR
        /*
         * Clear curproc after we've done all operations
         * that could block, and before tearing down the rest
         * of the process state that might be used from clock, etc.
         * Also, can't clear curproc while we're still runnable,
         * as we're not on a run queue (we are current, just not
         * a proper proc any longer!).
         *
         * Other substructures are freed from wait().
         */
        curproc = NULL;
        if (--p->p_limit->p_refcnt == 0)
                FREE(p->p_limit, M_SUBPROC);

        /*
         * Finally, call machine-dependent code to release the remaining
         * resources including address space, the kernel stack and pcb.
         * The address space is released by "vmspace_free(p->p_vmspace)";
         * This is machine-dependent, as we may have to change stacks
         * or ensure that the current one isn't reallocated before we
         * finish.  cpu_exit will end with a call to cpu_swtch(), finishing
         * our execution (pun intended).
         */
        cpu_exit(p);
#endif

        if (isvfork == false)
        {
                KASSERT(a_procp->kthread->task->thread_count == 1);

                thread_deallocate(a_procp->kthread);
                /* Is there a better way to loose the ref given to us by task_create */
                a_procp->kthread->task->ref_count--;
                task_terminate(a_procp->kthread->task);
        }
        else
        {
                KASSERT(a_procp->kthread->task->thread_count == 2);
                thread_terminate(a_procp->kthread);
                thread_deallocate(a_procp->kthread);
        }

        return 0;
}



int proc_signal_exit(pthread_t a_proc, int signum)
{
        signum = __W_EXITCODE(0, signum);

        if (s_sigprop[signum] & SA_CORE)
        {
                // Pretend we have dumnped ass
                signum |= __WCOREFLAG;
        }

        return proc_exit_internal(a_proc, signum);
}

int proc_exit(pthread_t a_procp, int a_ec)
{
        if (a_procp == &a_procp->process->thread)
        {
                // Main thread
                return proc_exit_internal(a_procp, __W_EXITCODE((a_ec & 0xFF), 0));
        }
        else
        {
                // A child thread
                pthread_t self = a_procp;
                emerixx::uio::copyout(0, self->tid_address);
                wakeup(self->tid_address);

                thread_terminate(self->kthread);
                thread_deallocate(self->kthread);

                self->kthread = nullptr;

                // s_pthread_zone.free(self);

                return KERN_SUCCESS;
        }
}



int proc_exit_group(pthread_t procp, int error_code)
{
        error_code = __W_EXITCODE((error_code & 0xFF), 0);
        /* Temp, just get shit working */
        /* We should be killing all threads and then run the code above */
        return proc_exit_internal(procp, error_code);
}
#endif

int process::on_sleep(char const *wmesg, bool interruptible)
{
        // Are we cathing signals during this sleep?
        if (interruptible)
        {
                // Are there any signals we are catching?
                if (p_siglist & ~p_sigmask)
                {
                        // We are cancelling this sleep request, a signal is pressent
                        return -EINTR;
                }
                // No catchable signals, set our interruptible flag
                p_flag |= P_SINTR;
        }

        // Record the reason for sleeping
        p_wmesg = wmesg;

        // We are going to sleep
        p_stat = SSLEEP;

        return 0;
}

int process::on_wakeup()
{
        // We have been woken up, could be a event or someone interrupted us
        p_stat = SRUN;

        // Clear the sleep reason
        p_wmesg = nullptr;

        // Remove our interruptible flag
        p_flag &= ~P_SINTR;

        // Are there any signals pending?
        if (p_siglist & ~p_sigmask)
        {
                thread_ast_set(thread.kthread, AST_UNIX);
                // No need to check the thread context before propagating
                // as we know we are running in the right context during syscalls
                ast_propagate(thread.kthread, cpu_number());
        }

        int result;
        if (thread.kthread->wait_result == THREAD_INTERRUPTED)
        {
                result = -EINTR;
        }
        else if (thread.kthread->wait_result == THREAD_TIMED_OUT)
        {
                result = -EWOULDBLOCK;
        }
        else
        {
                result = 0;
        }

        return result;
}

// Return a signal to be processed turning it off in the siglist
// Negative will cause this process to exit with abs(signal)
// Positive will dispatch the signal to a userspace handler
// Zero will return to userspace context before syscall/preempt
int process::get_pending_signal()
{
        using namespace emerixx::bitops;

        int signum       = 0;
        sigset_t sigmask = 0;

        // Iterate all pending signals signals
        for (signum = ffs((p_siglist & ~p_sigmask)), sigmask = SIGMASK(signum); signum;
             signum = ffs((p_siglist &= ~sigmask) & ~p_sigmask), sigmask = SIGMASK(signum))
        {
                // Are we ignoring this signum
                if (p_sigignore & sigmask)
                {
                        if (signum == SIGCHLD)
                        {
                                panic("Not Ready");
                                // Free process entries of zombie children
                        }

                        continue;
                }

                // If we are cathing this signum means we have a non default(exit) handler
                if (p_sigcatch & sigmask)
                {
                        break;
                }

                // We perform the default action for this signal(exit)
                signum = -signum;
                break;
        }

        // Remove the signal form siglist if needed
        if (signum)
        {
                p_siglist &= ~sigmask;
        }

        return signum;
}

// Called right before the we return to usermode handler
void process::on_signal_dispatch(int signum)
{
        struct ksigaction *sa = &p_sigacts[signum];

        if (sa->flags & SA_NODEFER)
        {
                p_sigmask = sa->mask & ~SIGMASK(signum);
        }
        else
        {
                p_sigmask = sa->mask | SIGMASK(signum);
        }

        // Check if need to restore the signal action to default
        if (sa->flags & SA_RESETHAND)
        {
                // Is this correct
                bzero(sa, sizeof *sa);
        }
}

// Called after we finished running a userspace handler and are about to
// return to the previous userspace context
natural_t process::on_signal_return(int signum, sigset_t sigmask, natural_t syscall_result)
{
        {
                spl_t s = splsched();

                p_sigmask = sigmask & ~(SIGMASK(SIGKILL) | SIGMASK(SIGSTOP));

                // Setting a new sigmask might have unblocked a held signal.
                // Check for it and schedule an AST if needed
                if (p_siglist & ~p_sigmask)
                {
                        thread_ast_set(thread.kthread, AST_UNIX);
                        // No need to check the thread context before propagating
                        // as we know we are running in the right context during syscalls
                        ast_propagate(thread.kthread, cpu_number());
                }

                splx(s);
        }

        // Sanity check( Is NSIG - 1???? )
        if (signum <= 0 || signum >= NSIG)
        {
                return -EINTR;
        }

        struct ksigaction *sa = &p_sigacts[signum];
        switch (syscall_result)
        {
        case -ERESTART:
        case -EINTR:
        case -ERESTARTSYS:
                /*
                -ERESTARTSYS is interpreted as "shift the instruction pointer
                back to syscall instruction" if SA_RESTART had been set when
                we'd installed the handler and turned into -EINTR otherwise
                */

                if (sa->flags & SA_RESTART)
                {
                        syscall_result = -ERESTART;
                }
                else
                {
                        syscall_result = -EINTR;
                }

                break;
        case -ERESTARTNOINTR: {
                /*
                -ERESTARTNOINTR is unconditional "shift back" (fork(2) et.al.)
                 */
                syscall_result = -ERESTART;
                break;
        }

        case -ERESTARTNOHAND:
                /*
                -ERESTARTNOHAND is "shift back" if no handler is set, -EINTR
                otherwise (sigsuspend(2))
                */
                syscall_result = -EINTR;
                break;

        case -ERESTART_RESTARTBLOCK:
                /*
                -ERESTART_RESTARTBLOCK is -EINTR if there's a handler and
                a horrible pile of hacks otherwise (timeout-related ones).
                */
                syscall_result = -EINTR;
                break;
        default:
                break;
        }

        return syscall_result;
}

pid_t proc_get_pid(pthread_t p) { return (int)p->process->p_pid; }
void proc_signal(pthread_t a_proc, int a_sig) { a_proc->process->signal(a_sig); }

// do TKILL AND TGKILL

int proc_kill(pid_t pid, int signum)
{
        process_t p = process::find(pid);
        int error   = 0;
        if (p)
        {
                // TODO: CHECK signum range
                if (WIFSIGNALED(signum))
                {
                        p->signal(WTERMSIG(signum));
                }
                else
                {
                        error = -EINVAL;
                }
        }
        else
        {
                error = -ESRCH;
        }

        log_trace("KILL %d", pid);
        return error;
}

void process::resource_usage(resource a_resource)
{
        // unused
}

struct file *process::fd_to_file(u32 a_fd)
{
        struct file *fp = nullptr;

        if (a_fd < p_fd->fd_nfiles)
        {
                fp = p_fd->fd_ofiles[a_fd];
        }

        return fp;
}

kern_return_t proc_getrusage(pthread_t a_proc, int who, struct rusage *a_rusage)
{
        rusage rusage_out = {};

        switch (who)
        {
        case RUSAGE_SELF: {
                time_value_t user_time;
                time_value_t system_time;
                spl_t opl = splsched();
                thread_read_times(a_proc->process->thread.kthread, &user_time, &system_time);
                splx(opl);

                a_proc->process->p_usage.ru_utime.tv_sec  = user_time.seconds;
                a_proc->process->p_usage.ru_utime.tv_usec = user_time.microseconds;

                a_proc->process->p_usage.ru_stime.tv_sec  = system_time.seconds;
                a_proc->process->p_usage.ru_stime.tv_usec = system_time.microseconds;

                rusage_out = a_proc->process->p_usage;
                break;
        }
        case RUSAGE_CHILDREN:
                break;
        case RUSAGE_THREAD:
                break;

        default:
                return KERN_FAIL(EINVAL);
                break;
        }

        return emerixx::uio::copyout(&rusage_out, a_rusage);
}

int proc_tkill(pthread_t a_proc, pid_t a_pid, int sig)
{
        // This should search on tid, but ok for now
        return proc_kill(a_pid, sig);
}

static void telt_to_timespec(timer_elt_t const a_telt, timespec *a_timespec)
{
        timespec_from_msec(a_timespec, (a_telt->ticks) * (1000 / hz));
}

static void timespec_to_telt(timespec const *a_timespec, timer_elt_t a_telt)
{
        a_telt->ticks = timespec_to_msec(a_timespec) / (1000 / hz);
}

kern_return_t timer_real_timeout(void *a_proc)
{
        pthread_t p = static_cast<pthread_t>(a_proc);

        p->process->signal(SIGALRM);

        if (timespec_is_zero(&(p->process->p_itimer[ITIMER_REAL].it_interval)) == false)
        {
                timespec_to_telt(&(p->process->p_itimer[ITIMER_REAL].it_interval), &p->process->p_itimer_elt[ITIMER_REAL]);
                set_timeout(&p->process->p_itimer_elt[ITIMER_REAL], p->process->p_itimer_elt[ITIMER_REAL].ticks);
        }

        return KERN_SUCCESS;
}

kern_return_t timer_virtual_timeout(void *a_proc)
{
        pthread_t p = static_cast<pthread_t>(a_proc);

        panic("Should not fire");

        return KERN_SUCCESS;
}

kern_return_t timer_prof_timeout(void *a_proc)
{
        pthread_t p = static_cast<pthread_t>(a_proc);

        panic("Should not fire");

        return KERN_SUCCESS;
}

// case ITIMER_REAL:
// Decrements in real time.
// A SIGALRM signal is delivered when this timer expires.

// case ITIMER_VIRTUAL:
// Decrements in process virtual time.
// It runs only when the process is executing.
// A SIGVTALRM signal is delivered when it expires.

// case ITIMER_PROF:
// Decrements both in process virtual time and when the system is running on behalf of the process.
// It is designed to be used by interpreters in statistically profiling the execution of interpreted programs.
// Each time the ITIMER_PROF timer expires, the SIGPROF signal is delivered.

kern_return_t process::setitimer(int a_which, itimerspec const *a_itimer)
{
        KASSERT(a_which == ITIMER_REAL);

        if (p_itimer_elt[a_which].set == TELT_SET)
        {
                reset_timeout(&p_itimer_elt[a_which]);
        }

        //  if it_value is 0 we shall disable the timer, regardless of the value of it_interval.
        if (timespec_is_zero(&(a_itimer->it_value)) == true)
        {
                p_itimer[a_which] = {};
        }
        else
        {
                timespec_to_telt(&(a_itimer->it_value), &p_itimer_elt[a_which]);

                set_timeout(&p_itimer_elt[a_which], p_itimer_elt[a_which].ticks);

                p_itimer[a_which] = *a_itimer;
        }

        return KERN_SUCCESS;
}
#endif