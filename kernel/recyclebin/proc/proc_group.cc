#include <errno.h>

#include <kernel/zalloc.h>
#include <mach/queue.h>
#include <mach/tty.h>
#include <mach/ucred.h>
#include <mach/uio.h>
#include <malloc.h>
#include <string.h>

using pgrp_zone_t = zone<pgrp, 1024, 512>::type;
static pgrp_zone_t s_pgrp_zone;

using session_zone_t = zone<session, 1024, 512>::type;
static session_zone_t s_session_zone;

#define PGRPHASH(pgid) (&pgrphashtbl[(pgid)&pgrphash])
LIST_HEAD(pgrphashhead, pgrp);
static struct pgrphashhead *pgrphashtbl;
static size_t pgrphash;

/*
 * General routine to allocate a hash table.
 */

static pgrphashhead *hashinit(size_t elements, int type, size_t *hashmask)
{
        UNUSED(type);
        size_t hashsize;
        pgrphashhead *hashtbl;
        size_t i;

        if (elements <= 0)
                panic("hashinit: bad cnt");
        for (hashsize = 1; hashsize <= elements; hashsize <<= 1)
                continue;
        hashsize >>= 1;
        hashtbl = (struct pgrphashhead *)malloc(hashsize * sizeof(*hashtbl) /*, type, M_WAITOK*/);
        for (i = 0; i < hashsize; i++)
                LIST_INIT(&hashtbl[i]);
        *hashmask = hashsize - 1;
        return (hashtbl);
}

void proc_group_init(size_t maxproc, process_t initproc)
{
        pgrp_zone_t::init(&s_pgrp_zone, "pgrp");
        session_zone_t::init(&s_session_zone, "session");

        pgrphashtbl = hashinit(maxproc / 4, /* M_PROC*/ 0, &pgrphash);

        LIST_INSERT_HEAD(PGRPHASH(1), initproc->p_pgrp, pg_hash);
}

/*
 * A process group has become orphaned;
 * if there are any stopped processes in the group,
 * hang-up all process in that group.
 */
static void orphanpg(pgrp *pg)
{
        for (process_t p : pg->pg_members)
        {
                if (p->p_stat == SSTOP)
                {
                        for (process_t pp : pg->pg_members)
                        {
                                pp->signal(SIGHUP);
                                pp->signal(SIGCONT);
                        }
                }
        }

#if DAMIR
        for (p = pg->pg_members.lh_first; p != 0; p = p->p_pglist.le_next)
        {
                if (p->p_stat == SSTOP)
                {
                        for (p = pg->pg_members.lh_first; p != 0; p = p->p_pglist.le_next)
                        {
                                p->signal(SIGHUP);
                                p->signal(SIGCONT);
                        }
                        return;
                }
        }
#endif
}

/*
 * Adjust pgrp jobc counters when specified process changes process group.
 * We count the number of processes in each process group that "qualify"
 * the group for terminal job control (those with a parent in a different
 * process group of the same session).  If that count reaches zero, the
 * process group becomes orphaned.  Check both the specified process'
 * process group and that of its children.
 * entering == 0 => p is leaving specified group.
 * entering == 1 => p is entering specified group.
 */
static void fixjobc(process_t p, struct pgrp *pgrp, int entering)
{
        struct pgrp *hispgrp;
        struct session *mysession = pgrp->pg_session;

        /*
         * Check p's parent to see whether p qualifies its own process
         * group; if so, adjust count for p's process group.
         */
        if ((hispgrp = p->p_pptr->p_pgrp) != pgrp && hispgrp->pg_session == mysession)
        {
                if (entering)
                {
                        pgrp->pg_jobc++;
                }
                else if (--pgrp->pg_jobc == 0)
                {
                        orphanpg(pgrp);
                }
        }

        /*
         * Check this process' children to see whether they qualify
         * their process groups; if so, adjust counts for children's
         * process groups.
         */
        for (p = p->p_children.lh_first; p != 0; p = p->p_sibling.le_next)
        {
                if ((hispgrp = p->p_pgrp) != pgrp && hispgrp->pg_session == mysession && p->p_stat != SZOMB)
                {
                        if (entering)
                        {
                                hispgrp->pg_jobc++;
                        }
                        else if (--hispgrp->pg_jobc == 0)
                        {
                                orphanpg(hispgrp);
                        }
                }
        }
}

/*
 * Send a signal to a process group.  If checktty is 1,
 * limit to members which have a controlling terminal.
 */
void pgrp::signal(int signum, int checkctty)
{
        // pthread_t p;

        if (this)
        {
                for (process_t p : pg_members)
                {
                        if (checkctty == 0 || p->p_flag & P_CONTROLT)
                        {
                                p->signal(signum);
                        }
                }
#if DAMIR
                for (p = pgrp->pg_members.lh_first; p != 0; p = p->p_pglist.le_next)
                {
                        if (checkctty == 0 || p->p_flag & P_CONTROLT)
                        {
                                p->signal(signum);
                        }
                }
#endif
        }
}

/*
 * Locate a process group by number
 */
struct pgrp *proc_group_find(pid_t pgid)
{
        struct pgrp *pgrp;
        for (pgrp = PGRPHASH(pgid)->lh_first; pgrp != 0; pgrp = pgrp->pg_hash.le_next)
                if (pgrp->pg_id == pgid)
                        return (pgrp);
        return (NULL);
}

/*
 * delete a process group
 */
static void proc_group_delete(struct pgrp *a_pgrp)
{
#if MOVING
        if (a_pgrp->pg_session->s_ttyp != NULL && a_pgrp->pg_session->s_ttyp->t_pgrp == a_pgrp)
        {
                a_pgrp->pg_session->s_ttyp->t_pgrp = NULL;
        }

        LIST_REMOVE(a_pgrp, pg_hash);

        if (--a_pgrp->pg_session->s_count == 0)
        {
#ifdef DAMIR
                FREE(pgrp->pg_session, M_SESSION);
#else
                // free(a_pgrp->pg_session);
                s_session_zone.free(a_pgrp->pg_session);
#endif
        }
#ifdef DAMIR
        FREE(pgrp, M_PGRP);
#else
        // free(a_pgrp);
        s_pgrp_zone.free(a_pgrp);
#endif
#endif
}

/*
 * Move p to a new or existing process group (and session)
 */
int proc_group_enter(process_t p, pid_t pgid, int mksess)
{
        struct pgrp *pgrp = proc_group_find(pgid);

#ifdef DIAGNOSTIC
        if (pgrp != NULL && mksess) /* firewalls */
                panic("enterpgrp: setsid into non-empty pgrp");
        if (SESS_LEADER(p))
                panic("enterpgrp: session leader attempted setpgrp");
#endif
        if (pgrp == NULL)
        {
                pid_t savepid = (pid_t)p->p_pid;
                process_t np;
                /*
                 * new process group
                 */
#ifdef DIAGNOSTIC
                if (p->p_pid != pgid)
                        panic("enterpgrp: new pgrp and pid != pgid");
#endif
#ifdef DAMIR
                MALLOC(pgrp, struct pgrp *, sizeof(struct pgrp), M_PGRP, M_WAITOK);
#else
                pgrp = s_pgrp_zone.alloc();
#endif
                if ((np = process::find(savepid)) == NULL || np != p)
                {
                        return -ESRCH;
                }

                if (mksess)
                {
                        struct session *sess;

/*
 * new session
 */
#ifdef DAMIR
                        MALLOC(sess, struct session *, sizeof(struct session), M_SESSION, M_WAITOK);
#else
                        sess = s_session_zone.alloc();
#endif
                        sess->s_leader = p;
                        sess->s_count  = 1;
                        sess->s_ttyvp  = NULL;
                        sess->s_ttyp   = NULL;
                        memcpy(sess->s_login, p->p_pgrp->pg_session->s_login, sizeof(sess->s_login));
                        p->p_flag &= ~P_CONTROLT;
                        pgrp->pg_session = sess;
#ifdef DIAGNOSTIC
                        if (p != curproc)
                                panic("enterpgrp: mksession and p != curproc");
#endif
                }
                else
                {
                        pgrp->pg_session = p->p_pgrp->pg_session;
                        pgrp->pg_session->s_count++;
                }
                pgrp->pg_id = pgid;
                // LIST_INIT(&pgrp->pg_members);
                pgrp->pg_members.init();
                LIST_INSERT_HEAD(PGRPHASH(pgid), pgrp, pg_hash);
                pgrp->pg_jobc = 0;
        }
        else if (pgrp == p->p_pgrp)
        {
                return (0);
        }

        /*
         * Adjust eligibility of affected pgrps to participate in job control.
         * Increment eligibility counts before decrementing, otherwise we
         * could reach 0 spuriously during the first call.
         */
        fixjobc(p, pgrp, 1);
        fixjobc(p, p->p_pgrp, 0);

        // LIST_REMOVE(p, p_pglist);
        p->p_pgrp->pg_members.remove(p);

        if (p->p_pgrp->pg_members.empty())
        {
                proc_group_delete(p->p_pgrp);
        }

        p->p_pgrp = pgrp;
        // LIST_INSERT_HEAD(&pgrp->pg_members, p, p_pglist);
        pgrp->pg_members.insert_head(p);
        return (0);
}

/*
 * remove process from process group
 */
int proc_group_leave(pthread_t a_procp)
{
        // LIST_REMOVE(a_procp, p_pglist);
        a_procp->process->p_pgrp->pg_members.remove(a_procp->process);

        if (a_procp->process->p_pgrp->pg_members.empty())
        {
                proc_group_delete(a_procp->process->p_pgrp);
        }

        a_procp->process->p_pgrp = nullptr;
        return (0);
}

/* Get process group ID; note that POSIX getpgrp takes no parameter */
int proc_group_getpgrp(pthread_t a_procp) { return a_procp->process->p_pgrp->pg_id; }

size_t proc_group_getgroups(pthread_t a_procp, int a_gidsetsize, gid_t *a_gidset)
{
        struct pcred *pc = a_procp->process->p_cred;
        size_t ngrp;
        int error;

        if ((ngrp = (size_t)a_gidsetsize) == 0)
        {
                return pc->pc_ucred->cr_ngroups;
        }

        if (ngrp < pc->pc_ucred->cr_ngroups)
        {
                return -EINVAL;
        }

        ngrp = pc->pc_ucred->cr_ngroups;

        error = copyout(pc->pc_ucred->cr_groups, a_gidset, ngrp * sizeof(gid_t));
        if (error)
        {
                return (error);
        }

        return ngrp;
}

int proc_group_setsid(pthread_t a_pthread)
{
        process_t a_procp = a_pthread->process;

        if (a_procp->p_pgrp->pg_id == a_procp->p_pid || proc_group_find((pid_t)a_procp->p_pid))
        {
                return -EPERM;
        }
        else
        {
                proc_group_enter(a_procp, (pid_t)a_procp->p_pid, 1);
                return (int)a_procp->p_pid;
        }
}

/*
 * set process group (setpgid/old setpgrp)
 *
 * caller does setpgid(targpid, targpgid)
 *
 * pid must be caller or child of caller (ESRCH)
 * if a child
 *	pid must be in same session (EPERM)
 *	pid can't have done an exec (EACCES)
 * if pgid != pid
 * 	there must exist some pid in same session having pgid (EPERM)
 * pid must not be session leader (EPERM)
 */
int proc_group_setpgid(pthread_t a_pthread, pid_t a_pid, pid_t a_pgid)
{
        process_t a_procp = a_pthread->process;
        process_t targp;   /* target process */
        struct pgrp *pgrp; /* target pgrp */

        if (a_pid != 0 && a_pid != a_procp->p_pid)
        {
                if ((targp = process::find(a_pid)) == 0 || !proc_is_inferior(targp))
                {
                        return -ESRCH;
                }

                if (targp->p_pgrp->pg_session != a_procp->p_pgrp->pg_session)
                {
                        return -EPERM;
                }

                if (targp->p_flag & P_EXEC)
                {
                        return -EACCES;
                }
        }
        else
                targp = a_procp;

        if (targp->is_session_leader())
        {
                return -EPERM;
        }

        if (a_pgid == 0)
        {
                a_pgid = (pid_t)targp->p_pid;
        }
        else if (a_pgid != targp->p_pid)
        {
                if ((pgrp = proc_group_find(a_pgid)) == 0 || pgrp->pg_session != a_procp->p_pgrp->pg_session)
                {
                        return -EPERM;
                }
        }
        return proc_group_enter(targp, a_pgid, 0);
}

/*
NAME
getpgid - get the process group ID for a process

DESCRIPTION
The getpgid() function shall return the process group ID of the process whose
process ID is equal to pid. If pid is equal to 0, getpgid() shall return the
process group ID of the calling process.

RETURN VALUE
Upon successful completion, getpgid() shall return a process group ID.
Otherwise, it shall return (pid_t)-1 and set errno to indicate the error.

ERRORS
The getpgid() function shall fail if:

[EPERM]
The process whose process ID is equal to pid is not in the same session as the
calling process, and the implementation does not allow access to the process
group ID of that process from the calling process. [ESRCH] There is no process
with a process ID equal to pid. The getpgid() function may fail if:

[EINVAL]
The value of the pid argument is invalid.
*/
int proc_group_getpgid(pthread_t a_procp, pid_t a_pid)
{
        if (a_pid == 0)
        {
                return a_procp->process->p_pgrp->pg_id;
        }

        process_t tprocp = process::find(a_pid);

        if (tprocp == nullptr)
        {
                return -ESRCH;
        }

        if (tprocp->p_pgrp->pg_id != a_procp->process->p_pgrp->pg_id)
        {
                return -EPERM;
        }

        return 0;
}

int proc_group_setgroups(pthread_t a_procp, int a_gidsetsize, gid_t *a_gidsetp)
{
        struct pcred *pc = a_procp->process->p_cred;
        size_t ngrp;
        int error;

        error = suser(pc->pc_ucred, &a_procp->process->p_acflag);
        if (error)
        {
                return (error);
        }

        ngrp = (size_t)a_gidsetsize;
        if (ngrp < 1 || ngrp > NGROUPS)
        {
                return -EINVAL;
        }

        pc->pc_ucred = crcopy(pc->pc_ucred);

        error = copyin(a_gidsetp, pc->pc_ucred->cr_groups, ngrp * sizeof(gid_t));
        if (error)
        {
                return (error);
        }

        pc->pc_ucred->cr_ngroups = ngrp;
        a_procp->process->p_flag |= P_SUGID;
        return 0;
}
