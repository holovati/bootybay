#include <mach/cpu_number.h>

#include <mach/thread.h>
/*
 * Compatibility with BSD device drivers.
 */
int sleep(event_t evt, int p, char const *a_wmsg)
{
#if DAMIR
        assert_wait((event_t)channel, FALSE); /* not interruptible XXX */
        thread_block(nullptr);
#endif
        return tsleep(evt, p & 0, a_wmsg, 0);
}

kern_return_t wakeup(event_t evt)
{
        thread_wakeup(evt);
        return 0;
}

int tsleep(event_t evt, int pri, char const *wmesg, int timeout)
{
        bool interruptible = (pri & 0) != 0;

        assert_wait(evt, interruptible);

        thread_t cur_thread = current_thread();

        if (evt == nullptr && timeout == 0)
        {
                /*
                log_error(
                    "Thread(%p) with wmsg %s entered with NULL event and no timeout, would "
                    "block forever",
                    cur_thread,
                    wmesg);
                */
                timeout = 1;
        }

        process_t cur_proc = nullptr;
        if (cur_thread->pthread != nullptr)
        {
                cur_proc = cur_thread->pthread->process;
        }

        int result = 0;

        if (cur_proc && (result = cur_proc->on_sleep(wmesg, interruptible)))
        {
                // The process wants to cancel this sleep for some reason
                clear_wait(cur_thread, THREAD_INTERRUPTED, true);
                return result;
        }

        if (timeout)
        {
                thread_set_timeout(timeout);
        }

        thread_block(nullptr);

        if (cur_proc)
        {
                // Fetch the result of the sleep, there might be a signal
                result = cur_proc->on_wakeup();

                if (!evt)
                {
                        // If we did not have a event(nullptr) we don't care about the result from
                        // on_wakeup.
                        result = 0;
                }
        }

        return result;
}
