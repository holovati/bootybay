#pragma once
#ifdef __cplusplus
#include <mach/filedesc.h>
extern "C" {
#endif
#include <mach/fcntl.h>
#include <sys/ioctl.h>
//#include <mach/kernel.h>
#include <mach/param.h>
//#include <mach/resourcevar.h>
//#include <sys/socket.h>
#include <errno.h>
#include <mach/queue.h>
#include <mach/stat.h>
#include <mach/syslimits.h>
#include <mach/unistd.h>
#include <string.h>
#include <sys/param.h>

int closef(struct file *fp, pthread_t p);

#ifdef __cplusplus
}
#endif
#include <mach/file.h>
#include <mach/mount.h>

#include <mach/ucred.h>
#include <mach/vnode.h>