extern "C" {
#include <mach/ioccom.h>
#include <mach/machine/spl.h>
#include <mach/thread.h>
#include <termios.h>
}
#include <mach/socketvar.h>
#include <mach/vfs.h>
#include <malloc.h>

#include "fdesc_internal.h"
#if DAMIR

/*
 * Common code for dup, dup2, and fcntl(F_DUPFD).
 */
static int finishdup(struct filedesc *fdp, int fdold, int fdnew)
{
        struct file *fp;
        fp                        = fdp->fd_ofiles[fdold];
        fdp->fd_ofiles[fdnew]     = fp;
        fdp->fd_ofileflags[fdnew] = fdp->fd_ofileflags[fdold] & ~FD_CLOEXEC;
        fp->f_count++;

        if (fdnew > fdp->fd_lastfile)
        {
                fdp->fd_lastfile = fdnew;
        }

        return fdnew;
}

/*
 * Duplicate a file descriptor.
 */
static kern_return_t fdesc_dup_internal(pthread_t p, int a_fildes, int a_want)
{
        struct filedesc *fdp;
        int fdold = a_fildes;
        int fdnew;
        int error;

        if ((a_fildes < 0) || (a_fildes >= 0x100))
        {
                return KERN_FAIL(EINVAL);
        }

        if ((a_want < 0) || (a_want >= 0x100))
        {
                return KERN_FAIL(EINVAL);
        }

        fdp = p->process->p_fd;
        if (fdold >= fdp->fd_nfiles || fdp->fd_ofiles[fdold] == NULL)
                return -EBADF;

        if (error = fdalloc(p->process, a_want, &fdnew))
                return error;

        return finishdup(fdp, fdold, fdnew);
}

/*
 * Duplicate a file descriptor.
 */
int fdesc_dup(pthread_t p, int a_fildes) { return fdesc_dup_internal(p, a_fildes, 0); }

static void munmapfd(pthread_t p, unsigned int fd)
{
#ifdef DEBUG
        if (mmapdebug & MDB_FOLLOW)
                printf("munmapfd(%d): fd %d\n", p->p_pid, fd);
#endif

        /*
         * XXX should vm_deallocate any regions mapped to this file
         */
        // p->p_fd->fd_ofileflags[fd] &= ~UF_MAPPED;
}

/*
 * The dup2() function shall cause the file descriptor fildes2 to refer to the same open file description as the file descriptor
 * fildes and to share any locks, and shall return fildes2
 */
/* ARGSUSED */

kern_return_t fdesc_dup2(pthread_t p, int a_fildes, int a_fildes2)
{
        struct filedesc *fdp = p->process->p_fd;

        kern_return_t retval;

        if ((a_fildes2 < 0) || (a_fildes2 >= 0x100))
        {
                return KERN_FAIL(EBADF);
        }

        if ((a_fildes < 0) || (a_fildes >= 0x100))
        {
                return KERN_FAIL(EBADF);
        }

        if (a_fildes >= fdp->fd_nfiles || fdp->fd_ofiles[a_fildes] == nullptr
            /*|| fdnew >= p->p_rlimit[RLIMIT_NOFILE].rlim_cur || fdnew >= maxfiles*/)
        {
                return KERN_FAIL(EBADF);
        }

        if (a_fildes == a_fildes2)
        {
                return a_fildes2;
        }

        int new_fd;
        if (a_fildes2 >= fdp->fd_nfiles)
        {
                if (retval = fdalloc(p->process, a_fildes2, &new_fd))
                {
                        return retval;
                }

                if (a_fildes2 != new_fd)
                {
                        panic("dup2: fdalloc");
                }
        }

        // If fildes2 is already a valid open file descriptor, it shall be closed first, unless fildes is equal to fildes2
        // in which case dup2() shall return fildes2 without closing it.
        if ((a_fildes2 < fdp->fd_nfiles) && (fdp->fd_ofiles[a_fildes2] != nullptr))
        {
                // retval = fdesc_close(p, a_fildes2);

                if (retval != KERN_SUCCESS)
                {
                        return retval;
                }
        }

        KASSERT(fdp->fd_ofiles[a_fildes2] == nullptr);

        return finishdup(fdp, a_fildes, a_fildes2);
}

/*
 * The file control system call.
 */
/* ARGSUSED */
int fdesc_fcntl(pthread_t p, unsigned int fd, unsigned int cmd, unsigned long arg)
{
        struct filedesc *fdp = p->process->p_fd;
        struct file *fp;
        char *pop;
        int i, tmp, error, flg = F_POSIX;
        struct flock fl;
        u32 newmin;

        if ((u32)fd >= fdp->fd_nfiles || (fp = fdp->fd_ofiles[fd]) == NULL)
                return (-EBADF);
        pop = &fdp->fd_ofileflags[fd];
        switch (cmd)
        {

        // Return a new file descriptor which shall be allocated as described in File Descriptor Allocation, except that it shall be
        // the lowest numbered available file descriptor greater than or equal to the third argument, arg, taken as an integer of
        // type int. The new file descriptor shall refer to the same open file description as the original file descriptor, and
        // shall share any locks. The FD_CLOEXEC flag associated with the new file descriptor shall be cleared to keep the file open
        // across calls to one of the exec functions.
        case F_DUPFD: {
                return fdesc_dup_internal(p, fd, arg);
        }

        // Like F_DUPFD, but the FD_CLOEXEC flag associated with the new file descriptor shall be set.
        case F_DUPFD_CLOEXEC: {
                int retval = fdesc_dup_internal(p, fd, arg);

                if (retval > 0)
                {
                        fdp->fd_ofileflags[retval] |= FD_CLOEXEC;
                }
                return retval;
        }

        // Get the file descriptor flags defined in <fcntl.h> that are associated with the file descriptor fildes. File
        // descriptor flags are associated with a single file descriptor and do not affect other file descriptors that refer
        // to the same file.
        case F_GETFD:
                return (*pop & 1);

        // Set the file descriptor flags defined in <fcntl.h>, that are associated with fildes, to the third argument, arg, taken as
        // type int. If the FD_CLOEXEC flag in the third argument is 0, the file descriptor shall remain open across the exec
        // functions; otherwise, the file descriptor shall be closed upon successful execution of one of the exec functions.
        case F_SETFD:
                *pop = (*pop & ~FD_CLOEXEC) | (((long)arg) & FD_CLOEXEC);
                return (0);

        // Get the file status flags and file access modes, defined in <fcntl.h>, for the file description associated with fildes.
        // The file access modes can be extracted from the return value using the mask O_ACCMODE, which is defined in <fcntl.h>.
        // File status flags and file access modes are associated with the file description and do not affect other file descriptors
        // that refer to the same file with different open file descriptions. The flags returned may include non-standard file
        // status flags which the application did not set, provided that these additional flags do not alter the behavior of a
        // conforming application.
        case F_GETFL:
                return fp->f_flag;

        // Set the file status flags, defined in <fcntl.h>, for the file description associated with fildes from the corresponding
        // bits in the third argument, arg, taken as type int. Bits corresponding to the file access mode and the file creation
        // flags, as defined in <fcntl.h>, that are set in arg shall be ignored. If any bits in arg other than those mentioned here
        // are changed by the application, the result is unspecified. If fildes does not support non-blocking operations, it is
        // unspecified whether the O_NONBLOCK flag will be ignored.
        case F_SETFL:
                fp->f_flag &= ~FCNTLFLAGS;
                fp->f_flag |= ((int)arg) & FCNTLFLAGS;
                tmp   = fp->f_flag & FNONBLOCK;
                error = fp->ioctl(FIONBIO, (char *)&tmp, p);
                if (error)
                {
                        return (error);
                }
                tmp   = fp->f_flag & FASYNC;
                error = fp->ioctl(FIOASYNC, (char *)&tmp, p);
                if (!error)
                {
                        return (0);
                }
                fp->f_flag &= ~FNONBLOCK;
                tmp = 0;
                fp->ioctl(FIONBIO, (char *)&tmp, p);
                return (error);

        // If fildes refers to a socket, get the process ID or process group ID specified to receive SIGURG signals when out-of-band
        // data is available. Positive values shall indicate a process ID; negative values, other than -1, shall indicate a process
        // group ID; the value zero shall indicate that no SIGURG signals are to be sent. If fildes does not refer to a socket, the
        // results are unspecified.
        case F_GETOWN:
                if (fp->f_type == DTYPE_SOCKET)
                {
                        panic("ss");
                        // return ((struct socket *)fp->f_data)->so_pgid;
                }

                pid_t pgid;
                error = fp->ioctl(TIOCGPGRP, &pgid, p);
                if (error)
                {
                        return error;
                }
                return pgid;

        // If fildes refers to a socket, set the process ID or process group ID specified to receive SIGURG signals when out-of-band
        // data is available, using the value of the third argument, arg, taken as type int. Positive values shall indicate a
        // process ID; negative values, other than -1, shall indicate a process group ID; the value zero shall indicate that no
        // SIGURG signals are to be sent. Each time a SIGURG signal is sent to the specified process or process group, permission
        // checks equivalent to those performed by kill() shall be performed, as if kill() were called by a process with the same
        // real user ID, effective user ID, and privileges that the process calling fcntl() has at the time of the call; if the
        // kill() call would fail, no signal shall be sent. These permission checks may also be performed by the fcntl() call. If
        // the process specified by arg later terminates, or the process group specified by arg later becomes empty, while still
        // being specified to receive SIGURG signals when out-of-band data is available from fildes, then no signals shall be sent
        // to any subsequently created process that has the same process ID or process group ID, regardless of permission; it is
        // unspecified whether this is achieved by the equivalent of a fcntl(fildes, F_SETOWN, 0) call at the time the process
        // terminates or is waited for or the process group becomes empty, or by other means. If fildes does not refer to a socket,
        // the results are unspecified.
        case F_SETOWN:
                if (fp->f_type == DTYPE_SOCKET)
                {
                        //((struct socket *)fp->f_data)->so_pgid = (long)arg;
                        panic("fgg");
                        return (0);
                }

                if ((long)arg <= 0)
                {
                        arg = (unsigned long)(-(long)arg);
                }
                else
                {
                        pthread_t p1 = &process::find((long)arg)->thread;
                        if (p1 == 0)
                                return -ESRCH;

                        arg = (unsigned long)((long)p1->process->p_pgrp->pg_id);
                }

                return fp->ioctl(TIOCSPGRP, &arg, p);

        // Get any lock which blocks the lock description pointed to by the third argument, arg, taken as a pointer to type struct
        // flock, defined in <fcntl.h>. The information retrieved shall overwrite the information passed to fcntl() in the structure
        // flock. If no lock is found that would prevent this lock from being created, then the structure shall be left unchanged
        // except for the lock type which shall be set to F_UNLCK.
        case F_SETLKW:
                flg |= F_WAIT;
                /* Fall into F_SETLK */

        // Set or clear a file segment lock according to the lock description pointed to by the third argument, arg, taken as a
        // pointer to type struct flock, defined in <fcntl.h>. F_SETLK can establish shared (or read) locks (F_RDLCK) or exclusive
        // (or write) locks (F_WRLCK), as well as to remove either type of lock (F_UNLCK). F_RDLCK, F_WRLCK, and F_UNLCK are defined
        // in <fcntl.h>. If a shared or exclusive lock cannot be set, fcntl() shall return immediately with a return value of -1.
        case F_SETLK: {
                // if (fp->f_type != DTYPE_VNODE)
                //{
                //        return -EBADF;
                //}
                vnode::locked_ptr vp{vnode::get_from_file(fp).get_raw()};
                if (vp.is_null())
                {
                        return -EBADF;
                }

                /* Copy in the lock structure */
                error = copyin((void *)arg, &fl, sizeof(fl));

                if (error)
                {
                        return (error);
                }

                if (fl.l_whence == SEEK_CUR)
                {
                        fl.l_start += fp->f_offset;
                }
                switch (fl.l_type)
                {
                case F_RDLCK:
                        if ((fp->f_flag & FREAD) == 0)
                        {
                                return -EBADF;
                        }

                        p->process->p_flag |= P_ADVLOCK;
                        log_debug("below");
                        // return vp->advlock((char *)p, F_SETLK, &fl, flg);
                        return 0;

                case F_WRLCK:
                        if ((fp->f_flag & FWRITE) == 0)
                        {
                                return -EBADF;
                        }

                        p->process->p_flag |= P_ADVLOCK;
                        log_debug("below");
                        return 0;
                        // return vp->advlock((char *)p, F_SETLK, &fl, flg);

                case F_UNLCK:
                        log_debug("below");
                        return 0;
                        // return vp->advlock((char *)p, F_UNLCK, &fl, F_POSIX);

                default:
                        return -EINVAL;
                }
        }

        // Get any lock which blocks the lock description pointed to by the third argument, arg, taken as a pointer to type struct
        // flock, defined in <fcntl.h>. The information retrieved shall overwrite the information passed to fcntl() in the structure
        // flock. If no lock is found that would prevent this lock from being created, then the structure shall be left unchanged
        // except for the lock type which shall be set to F_UNLCK.
        case F_GETLK: {
                // if (fp->f_type != DTYPE_VNODE)
                //{
                //        return (-EBADF);
                //}

                vnode::locked_ptr vp{vnode::get_from_file(fp).get_raw()};

                if (vp.is_null())
                {
                        return -EBADF;
                }

                /* Copy in the lock structure */
                error = copyin((void *)arg, &fl, sizeof(fl));
                if (error)
                {
                        return (error);
                }

                if (fl.l_whence == SEEK_CUR)
                {
                        fl.l_start += fp->f_offset;
                }
                log_debug("below");
#if 0
                if (error = vp->advlock((char *)p, F_GETLK, &fl, F_POSIX))
                {
                        return (error);
                }
#endif
                return copyout(&fl, (void *)arg, sizeof(fl));
        }
        default:
                return -EINVAL;
        }
        /* NOTREACHED */
        log_error("maybe");
}
#endif
/*
 * Apply an advisory lock on a file descriptor.
 *
 * Just attempt to get a record lock of the requested type on
 * the entire file (l_whence = SEEK_SET, l_start = 0, l_len = 0).
 */

#if 0
/*
 * Close a file descriptor.
 */
int fdesc_close(pthread_t p, unsigned int fd)
{
        struct filedesc *fdp = p->process->p_fd;
        struct file *fp;

        if ((u32)fd >= fdp->fd_nfiles || (fp = fdp->fd_ofiles[fd]) == NULL)
        {
                return -EBADF;
        }

        fdp->fd_ofileflags[fd] = 0;

        // if (*pf & UF_MAPPED)
        //{
        //        munmapfd(p, fd);
        //}

        fdp->fd_ofiles[fd] = nullptr;

        while (fdp->fd_lastfile > 0 && fdp->fd_ofiles[fdp->fd_lastfile] == NULL)
        {
                fdp->fd_lastfile--;
        }

        if (fd < fdp->fd_freefile)
        {
                fdp->fd_freefile = fd;
        }

        return (closef(fp, p));
}

/*
 * Read system call.
 */
int fdesc_read(pthread_t p, unsigned int fd, char const *buf, size_t nbyte)
{
        struct file *fp;
        struct filedesc *fdp = p->p_fd;
        struct uio auio;
        struct iovec aiov;
        long cnt, error = 0;
#ifdef KTRACE
        struct iovec ktriov;
#endif

        if (fd >= fdp->fd_nfiles || (fp = fdp->fd_ofiles[fd]) == NULL || (fp->f_flag & FREAD) == 0)
        {
                return -EBADF;
        }

        aiov.iov_base   = (void *)buf;
        aiov.iov_len    = nbyte;
        auio.uio_iov    = &aiov;
        auio.uio_iovcnt = 1;
        auio.uio_resid  = nbyte;
        auio.uio_rw     = UIO_READ;
        auio.uio_segflg = UIO_USERSPACE;
        auio.uio_procp  = p;
#ifdef KTRACE
        /*
         * if tracing, save a copy of iovec
         */
        if (KTRPOINT(p, KTR_GENIO))
                ktriov = aiov;
#endif
        cnt = nbyte;
        if (error = fp->read(&auio, fp->f_cred))
        {
                if (auio.uio_resid != cnt && (error == -ERESTART || error == -EINTR || error == -EWOULDBLOCK))
                {
                        error = 0;
                }
        }
        cnt -= auio.uio_resid;
#ifdef KTRACE
        if (KTRPOINT(p, KTR_GENIO) && error == 0)
                ktrgenio(p->p_tracep, uap->fd, UIO_READ, &ktriov, cnt, error);
#endif
        //*retval = cnt; read the linux description

        // return (error);

        if (error)
        {
                return error;
        }
        else
        {
                return (int)cnt;
        }
}

/*
 * Scatter read system call.
 */
int fdesc_readv(pthread_t p, unsigned int fd, struct iovec const *iovp, size_t iovcnt)
{
        struct file *fp;
        struct filedesc *fdp = p->p_fd;
        struct uio auio;
        struct iovec *iov;
        struct iovec *needfree;
        struct iovec aiov[UIO_SMALLIOV];
        long i, error = 0;
        size_t cnt = 0;
        size_t iovlen;

#ifdef KTRACE
        struct iovec *ktriov = NULL;
#endif

        if (fd >= fdp->fd_nfiles || (fp = fdp->fd_ofiles[fd]) == NULL || (fp->f_flag & FREAD) == 0)
        {
                return -EBADF;
        }

        /* note: can't use iovlen until iovcnt is validated */
        iovlen = iovcnt * sizeof(struct iovec);
        if (iovcnt > UIO_SMALLIOV)
        {
                if (iovcnt > UIO_MAXIOV)
                {
                        return (-EINVAL);
                }
                // MALLOC(iov, struct iovec *, iovlen, M_IOV, M_WAITOK);
                iov      = (struct iovec *)malloc(sizeof(struct iovec) * iovlen);
                needfree = iov;
        }
        else
        {
                iov      = aiov;
                needfree = NULL;
        }
        auio.uio_iov    = iov;
        auio.uio_iovcnt = iovcnt;
        auio.uio_rw     = UIO_READ;
        auio.uio_segflg = UIO_USERSPACE;
        auio.uio_procp  = p;
        if (error = copyin(iovp, iov, iovlen))
        {
                goto done;
        }

        auio.uio_resid = 0;

        for (i = 0; i < iovcnt; i++)
        {
                if (iov->iov_len < 0)
                {
                        error = -EINVAL;
                        goto done;
                }

                auio.uio_resid += iov->iov_len;

                if (auio.uio_resid < 0)
                {
                        error = -EINVAL;
                        goto done;
                }
                iov++;
        }

#ifdef KTRACE
        /*
         * if tracing, save a copy of iovec
         */
        if (KTRPOINT(p, KTR_GENIO))
        {
                MALLOC(ktriov, struct iovec *, iovlen, M_TEMP, M_WAITOK);
                bcopy((caddr_t)auio.uio_iov, (caddr_t)ktriov, iovlen);
        }
#endif

        cnt = auio.uio_resid;
        if (error = fp->read(&auio, fp->f_cred))
                if (auio.uio_resid != cnt && (error == -ERESTART || error == -EINTR || error == -EWOULDBLOCK))
                        error = 0;
        cnt -= auio.uio_resid;

#ifdef KTRACE
        if (ktriov != NULL)
        {
                if (error == 0)
                        ktrgenio(p->p_tracep, uap->fdes, UIO_READ, ktriov, cnt, error);
                FREE(ktriov, M_TEMP);
        }
#endif
// Read the description of the linux version
//*retval = cnt;
done:
        if (needfree)
        {
                // FREE(needfree, M_IOV);
                free(needfree);
        }
        // return (error);

        return (int)cnt;
}

/*
 * Write system call
 */
int fdesc_write(pthread_t p, unsigned int fd, char const *buf, size_t nbyte)
{
        struct file *fp;
        struct filedesc *fdp = p->process->p_fd;
        struct uio auio;
        struct iovec aiov;
        size_t cnt;
        int error = 0;
#ifdef KTRACE
        struct iovec ktriov;
#endif

        if (fd >= fdp->fd_nfiles || (fp = fdp->fd_ofiles[fd]) == NULL || (fp->f_flag & FWRITE) == 0)
        {
                return -EBADF;
        }
        aiov.iov_base   = (void *)buf;
        aiov.iov_len    = nbyte;
        auio.uio_iov    = &aiov;
        auio.uio_iovcnt = 1;
        auio.uio_resid  = nbyte;
        auio.uio_rw     = UIO_WRITE;
        auio.uio_segflg = UIO_USERSPACE;
        auio.uio_procp  = p->process;
#ifdef KTRACE
        /*
         * if tracing, save a copy of iovec
         */
        if (KTRPOINT(p, KTR_GENIO))
                ktriov = aiov;
#endif
        cnt = nbyte;
        if (error = fp->write(&auio, fp->f_cred))
        {
                if (auio.uio_resid != cnt && (error == -ERESTART || error == -EINTR || error == -EWOULDBLOCK))
                        error = 0;

                if (error == -EPIPE)
                {
                        p->process->signal(SIGPIPE);
                }
        }
        cnt -= auio.uio_resid;
#ifdef KTRACE
        if (KTRPOINT(p, KTR_GENIO) && error == 0)
                ktrgenio(p->p_tracep, uap->fd, UIO_WRITE, &ktriov, cnt, error);
#endif
        // Read the linux description of write
        //*retval = cnt;
        return cnt;
}

/*
 * Gather write system call
 */
int fdesc_writev(pthread_t p, unsigned int fd, struct iovec const *iovp, size_t iovcnt)
{
        struct file *fp;
        struct filedesc *fdp = p->process->p_fd;
        struct uio auio;
        struct iovec *iov;
        struct iovec *needfree;
        struct iovec aiov[UIO_SMALLIOV];
        long i, error = 0;
        size_t cnt = 0;
        size_t iovlen;

#ifdef KTRACE
        struct iovec *ktriov = NULL;
#endif

        if (fd >= fdp->fd_nfiles || (fp = fdp->fd_ofiles[fd]) == NULL || (fp->f_flag & FWRITE) == 0)
        {
                return (-EBADF);
        }

        /* note: can't use iovlen until iovcnt is validated */
        iovlen = iovcnt * sizeof(struct iovec);
        if (iovcnt > UIO_SMALLIOV)
        {
                if (iovcnt > UIO_MAXIOV)
                {
                        return (-EINVAL);
                }
                // MALLOC(iov, struct iovec *, iovlen, M_IOV, M_WAITOK);
                iov      = (struct iovec *)malloc(sizeof(struct iovec) * iovlen);
                needfree = iov;
        }
        else
        {
                iov      = aiov;
                needfree = NULL;
        }

        auio.uio_iov    = iov;
        auio.uio_iovcnt = iovcnt;
        auio.uio_rw     = UIO_WRITE;
        auio.uio_segflg = UIO_USERSPACE;
        auio.uio_procp  = p->process;

        if (error = copyin(iovp, iov, iovlen))
        {
                goto done;
        }

        auio.uio_resid = 0;

        for (i = 0; i < iovcnt; i++)
        {
                if (iov->iov_len < 0)
                {
                        error = -EINVAL;
                        goto done;
                }

                auio.uio_resid += iov->iov_len;

                if (auio.uio_resid < 0)
                {
                        error = -EINVAL;
                        goto done;
                }
                iov++;
        }
#ifdef KTRACE
        /*
         * if tracing, save a copy of iovec
         */
        if (KTRPOINT(p, KTR_GENIO))
        {
                MALLOC(ktriov, struct iovec *, iovlen, M_TEMP, M_WAITOK);
                bcopy((caddr_t)auio.uio_iov, (caddr_t)ktriov, iovlen);
        }
#endif
        cnt = auio.uio_resid;

        if (cnt > 0 && (error = fp->write(&auio, fp->f_cred)))
        {
                if (auio.uio_resid != cnt && (error == -ERESTART || error == -EINTR || error == -EWOULDBLOCK))
                {
                        error = 0;
                }
                if (error == -EPIPE)

                {
                        log_debug("EPIPE");
                        //  p->signal(SIGPIPE);
                }
        }

        cnt -= auio.uio_resid;

#ifdef KTRACE
        if (ktriov != NULL)
        {
                if (error == 0)
                        ktrgenio(p->p_tracep, uap->fd, UIO_WRITE, ktriov, cnt, error);
                FREE(ktriov, M_TEMP);
        }
#endif
        // Read the description for the linux writev
        //*retval = cnt;
done:
        if (needfree)
        {
                // FREE(needfree, M_IOV);
                panic("picnic");
        }

        if (error == -EPIPE)
        {
                cnt = KERN_FAIL(EAGAIN);
        }
        // return (error);
        return cnt;
}

/*
 * Ioctl system call
 */
int fdesc_ioctl(pthread_t p, unsigned int fd, ioctl_cmd_t cmd, unsigned long arg)
{
        struct file *fp;
        struct filedesc *fdp;
        size_t size;
        void *data = nullptr;
        void *memp;
        int tmp;
#define STK_PARAMS 128
        char stkbuf[STK_PARAMS];

        fdp = p->process->p_fd;
        if (fd >= fdp->fd_nfiles || (fp = fdp->fd_ofiles[fd]) == NULL)
        {
                return (-EBADF);
        }

        if ((fp->f_flag & (FREAD | FWRITE)) == 0)
        {
                return (-EBADF);
        }

        switch (cmd)
        {
        case FIONCLEX:
                fdp->fd_ofileflags[fd] &= ~FD_CLOEXEC;
                return (0);
        case FIOCLEX:
                fdp->fd_ofileflags[fd] |= FD_CLOEXEC;
                return (0);
        }

        /*
         * Interpret high order word to find amount of data to be
         * copied to/from the user's address space.
         */
        size = IOCPARM_LEN(cmd);
        if (size > IOCPARM_MAX)
        {
                return (-ENOTTY);
        }

        memp = NULL;

        if (size > sizeof(stkbuf))
        {
                memp = malloc(size /*, M_IOCTLOPS , M_WAITOK*/);
                data = memp;
        }
        else
        {
                data = stkbuf;
        }

        if (cmd & IOC_IN)
        {
                if (size)
                {
                        int error = copyin(data, (void *)arg, size);
                        if (error)
                        {
                                if (memp)
                                {
                                        free(memp /*, M_IOCTLOPS*/);
                                }
                                return error;
                        }
                }
                else
                {
                        *(unsigned long *)data = arg;
                }
        }
        else if ((cmd & IOC_OUT) && size)
        {
                /*
                 * Zero the buffer so the user always
                 * gets back something deterministic.
                 */
                bzero(data, size);
        }
        else if (cmd & IOC_VOID)
        {
                *(unsigned long *)data = arg;
        }

        int error;
        switch (cmd)
        {
        case FIONBIO:
                if (tmp = *(int *)data)
                        fp->f_flag |= FNONBLOCK;
                else
                        fp->f_flag &= ~FNONBLOCK;
                error = fp->ioctl(FIONBIO, &tmp, p);
                break;

        case FIOASYNC:
                if (tmp = *(int *)data)
                        fp->f_flag |= FASYNC;
                else
                        fp->f_flag &= ~FASYNC;
                error = fp->ioctl(FIOASYNC, &tmp, p);
                break;

        case FIOSETOWN:
                tmp = *(int *)data;
                if (fp->f_type == DTYPE_SOCKET)
                {
                        panic("sf");
                        //((struct socket *)fp->f_data)->so_pgid = tmp;
                        error = 0;
                        break;
                }
                if (tmp <= 0)
                {
                        tmp = -tmp;
                }
                else
                {
                        pthread_t p1 = &process::find(tmp)->thread;
                        if (p1 == 0)
                        {
                                error = -ESRCH;
                                break;
                        }
                        tmp = p1->process->p_pgrp->pg_id;
                }

                error = fp->ioctl(TIOCSPGRP, &tmp, p);
                break;

        case FIOGETOWN:
                if (fp->f_type == DTYPE_SOCKET)
                {
                        error = 0;
                        panic("df");
                        //*(int *)data = ((struct socket *)fp->f_data)->so_pgid;
                        break;
                }
                error        = fp->ioctl(TIOCGPGRP, data, p);
                *(int *)data = -*(int *)data;
                break;

        default:
                // HACK DAMIR
                if (((cmd >> 8) & 0xFF) == 0x54)
                {
                        size = 0;

                        if (cmd == TIOCGWINSZ)
                        {
                                size  = sizeof(struct winsize);
                                error = 0;
                        }
                        else if (cmd == TIOCSWINSZ)
                        {
                                error = copyin((void *)arg, data, sizeof(struct winsize));
                        }
                        else if (cmd == TIOCSPGRP)
                        {
                                error = copyin((void *)arg, data, sizeof(pid_t));
                        }
                        else if (cmd == TIOCGPGRP)
                        {
                                size  = sizeof(pid_t);
                                error = 0;
                        }
                        else if (cmd == TCGETS)
                        {
                                size  = sizeof(struct termios);
                                error = 0;
                        }
                        else if (cmd == TCSETS)
                        {
                                error = copyin((void *)arg, data, sizeof(struct termios));
                        }
                        else if (cmd == TCSETSW)
                        {
                                error = copyin((void *)arg, data, sizeof(struct termios));
                        }
                        else if (cmd == TCXONC)
                        {
                                // Don't know how to handle this one
                                return 0;
                        }
                        else
                        {
                                error = 0;
                                panic("Linux ioctl not supported");
                        }

                        if (error)
                        {
                                break;
                        }

                        error = fp->ioctl(cmd, data, p);

                        if (!error && size)
                        {
                                error = copyout(data, (void *)arg, size);
                                //
                        }
                }
                else
                {
                        error = fp->ioctl(cmd, data, p);
                        /*
                         * Copy any data to user, size was
                         * already set and checked above.
                         */

                        if (error == 0 && (cmd & IOC_OUT) && size)
                        {
                                error = copyout(data, (void *)arg, size);
                        }
                }
                break;
        }

        if (memp)
        {
                free(memp /*, M_IOCTLOPS*/);
        }
        return error;
}

int fdesc_getdents64(pthread_t a_procp, unsigned int a_fd, struct linux_dirent64 *a_bufp, size_t a_count)
{
        struct file *fp;
        struct uio auio;
        struct iovec aiov;
        int error, eofflag;

        if (error = fdesc_getfile(a_fd, a_procp->process->p_fd, &fp))
        {
                return (error);
        }

        if ((fp->f_flag & FREAD) == 0)
        {
                return -EBADF;
        }

        vnode::locked_ptr vp{vnode::get_from_file(fp).get_raw()}; // add constref assign

        if (vp.is_null())
        {
                return -EINVAL;
        }

        if (S_ISDIR(vp->m_fstat.st_mode) == false)
        {
                return KERN_FAIL(ENOTDIR);
        }

        aiov.iov_base   = a_bufp;
        aiov.iov_len    = a_count;
        auio.uio_iov    = &aiov;
        auio.uio_iovcnt = 1;
        auio.uio_rw     = UIO_READ;
        auio.uio_segflg = UIO_USERSPACE;
        auio.uio_procp  = a_procp->process;
        auio.uio_resid  = a_count;

        off64_t loff = auio.uio_offset = fp->f_offset;
        // error                          = vp->readdir(&auio, fp->f_cred, &eofflag, NULL, 0);
        error        = vp->getdents(&auio);
        fp->f_offset = auio.uio_offset;

        if (error)
        {
                return error;
        }

        // error   = copyout(&loff, basep, sizeof(loff));
        //*retval = a_count - auio.uio_resid;
        return a_count - auio.uio_resid;
}


/*
 * Sync an open file.
 */
int fdesc_sync(pthread_t a_procp, unsigned int a_fd)
{
        struct file *fp;
        int error;

        if (error = fdesc_getfile(a_fd, a_procp->process->p_fd, &fp))
        {
                return (error);
        }

        vnode::locked_ptr vp{vnode::get_from_file(fp).get_raw()};

        if (vp.is_null())
        {
                return -EINVAL;
        }

        /* Clean buffer cache */
        error = vp->fsync(fp->f_cred, MNT_WAIT, a_procp);
        return error;
}


/*
 * Reposition read/write file offset.
 */
int fdesc_lseek(pthread_t a_procp, unsigned int a_fd, off_t a_offset, int a_whence)
{
        struct ucred *cred   = a_procp->process->p_cred->pc_ucred;
        struct filedesc *fdp = a_procp->process->p_fd;
        struct file *fp;
        int error;

        if (a_fd >= fdp->fd_nfiles || (fp = fdp->fd_ofiles[a_fd]) == NULL)
        {
                return -EBADF;
        }

        vnode::locked_ptr vn{vnode::get_from_file(fp).get_raw()};

        if (vn.is_null())
        {
                return -ESPIPE;
        }

        switch (a_whence)
        {
        case SEEK_CUR:
                fp->f_offset += a_offset;
                break;
        case SEEK_END:
                struct stat statbuf;
                bzero(&statbuf, sizeof(statbuf));
                if ((error = vn->stat(&statbuf)))
                {
                        return error;
                }
                fp->f_offset = a_offset + statbuf.st_size;
                break;
        case SEEK_SET:
                fp->f_offset = a_offset;
                break;
        default:
                return -EINVAL;
        }
        return (int)fp->f_offset;
}

int fdesc_advise64(pthread_t a_procp, unsigned int fd, off_t offset, size_t len, int advice) { return 0; }
#endif