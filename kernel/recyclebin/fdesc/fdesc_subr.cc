/*
 * Copyright (c) 1982, 1986, 1989, 1991, 1993
 *	The Regents of the University of California.  All rights reserved.
 * (c) UNIX System Laboratories, Inc.
 * All or some portions of this file are derived from material licensed
 * to the University of California by American Telephone and Telegraph
 * Co. or Unix System Laboratories, Inc. and are reproduced herein with
 * the permission of UNIX System Laboratories, Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)kern_descrip.c	8.8 (Berkeley) 2/14/95
 */
#include "fdesc_internal.h"
#include <algorithm>
#include <mach/init.h>
/*
 * Descriptor management.
 */
// struct filelist filehead; /* head of list of open files */

/*
 * System calls on descriptors.
 */

/*
 * Return pathconf information about a file descriptor.
 */
/* ARGSUSED */
int fpathconf(pthread_t p, int fd, int name)
{
        panic("sd");
        struct filedesc *fdp = p->process->p_fd;
        struct file *fp;

        if ((u32)fd >= fdp->fd_nfiles || (fp = fdp->fd_ofiles[fd]) == NULL)
                return -EBADF;
        switch (fp->f_type)
        {
        case DTYPE_SOCKET:
                if (name != _PC_PIPE_BUF)
                        return -EINVAL;
                return PIPE_BUF;

        case DTYPE_VNODE:
                // vp = (struct vnode *)fp->f_data;

#ifdef DAMIR
                return VOP_PATHCONF(vp, name, retval);
#endif
        default:
                /*panic("fpathconf");*/
                log_fatal("pathconf");
                while (1)
                        ;
        }
        /*NOTREACHED*/
}

/*
 * Allocate a file descriptor for the process.
 */
int fdexpand;

int fdalloc(process_t p, int want, int *resultfd)
{
        struct filedesc *fdp = p->p_fd;
        int i;
        size_t lim = 0x100, last, nfiles;
        struct file **newofile;
        char *newofileflags;

/*
 * Search for a free descriptor starting at the higher
 * of want or fd_freefile.  If that fails, consider
 * expanding the ofile array.
 */
#ifdef DAMIR
        lim = min((int)p->p_rlimit[RLIMIT_NOFILE].rlim_cur, maxfiles);
#endif

        for (;;)
        {
                last = std::min(fdp->fd_nfiles, lim);
                if ((i = want) < fdp->fd_freefile)
                        i = fdp->fd_freefile;
                for (; i < last; i++)
                {
                        if (fdp->fd_ofiles[i] == NULL)
                        {
                                fdp->fd_ofileflags[i] = 0;
                                if (i > fdp->fd_lastfile)
                                        fdp->fd_lastfile = i;
                                if (want <= fdp->fd_freefile)
                                        fdp->fd_freefile = i;
                                *resultfd = (unsigned int)i;
                                return (0);
                        }
                }

                /*
                 * No space in current array.  Expand?
                 */
                if (fdp->fd_nfiles >= lim)
                        return -EMFILE;
                if (fdp->fd_nfiles < NDEXTENT)
                        nfiles = NDEXTENT;
                else
                        nfiles = 2 * fdp->fd_nfiles;
#ifdef DAMIR
                MALLOC(newofile, struct file **, nfiles * OFILESIZE, M_FILEDESC, M_WAITOK);
#endif
                newofile = (struct file **)malloc(nfiles * OFILESIZE);

                newofileflags = (char *)&newofile[nfiles];
                /*
                 * Copy the existing ofile and ofileflags arrays
                 * and zero the new portion of each array.
                 */
                memmove(newofile, fdp->fd_ofiles, (i = sizeof(struct file *) * fdp->fd_nfiles));
                bzero((char *)newofile + i, nfiles * sizeof(struct file *) - i);

                memmove(newofileflags, fdp->fd_ofileflags, (i = sizeof(char) * fdp->fd_nfiles));
                bzero(newofileflags + i, nfiles * sizeof(char) - i);
                if (fdp->fd_nfiles > NDFILE)
                {
#ifdef DAMIR
                        FREE(fdp->fd_ofiles, M_FILEDESC);
#endif
                        free(fdp->fd_ofiles);
                }

                fdp->fd_ofiles     = newofile;
                fdp->fd_ofileflags = newofileflags;
                fdp->fd_nfiles     = nfiles;
                fdexpand++;
        }
        panic("Pretty shitty");
}

/*
 * Check to see whether n user file descriptors
 * are available to the process p.
 */
int fdavail(pthread_t p, int n)
{
        struct filedesc *fdp = p->process->p_fd;
        struct file **fpp;
        int i, lim = 0;
#if DAMIR
        lim = min((int)p->p_rlimit[RLIMIT_NOFILE].rlim_cur, maxfiles);
#endif
        if ((i = lim - fdp->fd_nfiles) > 0 && (n -= i) <= 0)
                return (1);
        fpp = &fdp->fd_ofiles[fdp->fd_freefile];
        for (i = fdp->fd_nfiles - fdp->fd_freefile; --i >= 0; fpp++)
                if (*fpp == NULL && --n <= 0)
                        return (1);
        return (0);
}

#ifdef DAMIR
/*
 * Create a new open file structure and allocate
 * a file decriptor for the process that refers to it.
 */
int file_alloc(pthread_t p, struct file **resultfp, unsigned int *resultfd)
{
        panic("sd");
        return -1;
        struct file *fp, *fq;
        int error;
        int i;

        if (error = fdalloc(p, 0, &i))
                return (error);

        if (file::nfiles >= file::maxfiles)
        {
                log_error("file table full");
                // tablefull("file");
                return -ENFILE;
        }
        /*
         * Allocate a new file descriptor.
         * If the process has file descriptor zero open, add to the list
         * of open files at that point, otherwise put it at the front of
         * the list of open files.
         */
        // file::nfiles++;

        MALLOC(fp, struct file *, sizeof(struct file), M_FILE, M_WAITOK);

        // fp = (struct file *)malloc(sizeof(struct file));
        // fp = new (fp) file{0, 0, p->p_cred->pc_ucred};

        // bzero(fp, sizeof(struct file));

        file::filehead.remove(fp);

        if (fq = p->p_fd->fd_ofiles[0])
        {
                file::filehead.insert_after(fq, fp);
                // LIST_INSERT_AFTER(fq, fp, f_list_link);
        }
        else
        {
                file::filehead.insert_head(fp);
                // LIST_INSERT_HEAD(&filehead, fp, f_list_link);
        }

        p->p_fd->fd_ofiles[i] = fp;
        fp->f_count           = 1;
        // fp->f_cred            = p->p_cred->pc_ucred;

        // crhold(fp->f_cred);

        if (resultfp)
                *resultfp = fp;
        if (resultfd)
                *resultfd = i;
        return (0);
}

/*
 * Free a file descriptor.
 */
void file_free(struct file *fp)
{
        // LIST_REMOVE(fp, f_list_link);
        panic("sd");
        file::filehead.remove(fp);

        crfree(fp->f_cred);

#ifdef DIAGNOSTIC
        fp->f_count = 0;
#endif
        file::nfiles--;
#ifdef DAMIR
        FREE(fp, M_FILE);
#endif
}
#endif
/*
 * Copy a filedesc structure.
 */
struct filedesc *fdcopy(pthread_t p)
{
        struct filedesc *newfdp, *fdp = p->process->p_fd;
        struct file **fpp;
        int i;
#ifdef DAMIR
        MALLOC(newfdp, struct filedesc *, sizeof(struct filedesc0), M_FILEDESC, M_WAITOK);
#endif
        newfdp = (struct filedesc *)malloc(sizeof(struct filedesc0));
        bcopy(fdp, newfdp, sizeof(struct filedesc));
        newfdp->fd_cdir->reference();
        if (newfdp->fd_rdir)
        {
                newfdp->fd_rdir->reference();
        }
        newfdp->fd_refcnt = 1;

        /*
         * If the number of open files fits in the internal arrays
         * of the open file structure, use them, otherwise allocate
         * additional memory for the number of descriptors currently
         * in use.
         */
        if (newfdp->fd_lastfile < NDFILE)
        {
                newfdp->fd_ofiles     = ((struct filedesc0 *)newfdp)->fd_dfiles;
                newfdp->fd_ofileflags = ((struct filedesc0 *)newfdp)->fd_dfileflags;
                i                     = NDFILE;
        }
        else
        {
                /*
                 * Compute the smallest multiple of NDEXTENT needed
                 * for the file descriptors currently in use,
                 * allowing the table to shrink.
                 */
                i = newfdp->fd_nfiles;
                while (i > 2 * NDEXTENT && i > newfdp->fd_lastfile * 2)
                        i /= 2;
#ifdef DAMIR
                MALLOC(newfdp->fd_ofiles, struct file **, i * OFILESIZE, M_FILEDESC, M_WAITOK);
#endif
                newfdp->fd_ofiles     = (struct file **)malloc(i * OFILESIZE);
                newfdp->fd_ofileflags = (char *)&newfdp->fd_ofiles[i];
        }
        newfdp->fd_nfiles = i;
        memmove(newfdp->fd_ofiles, fdp->fd_ofiles, i * sizeof(struct file **));
        memmove(newfdp->fd_ofileflags, fdp->fd_ofileflags, i * sizeof(char));
        fpp = newfdp->fd_ofiles;
        for (i = newfdp->fd_lastfile; i-- >= 0; fpp++)
                if (*fpp != NULL)
                        (*fpp)->f_count++;
        return (newfdp);
}

/*
 * Release a filedesc structure.
 */
void fdfree(pthread_t p)
{
        struct filedesc *fdp = p->process->p_fd;
        struct file **fpp;
        int i;

        if (--fdp->fd_refcnt > 0)
                return;
        fpp = fdp->fd_ofiles;
        for (i = fdp->fd_lastfile; i-- >= 0; fpp++)
                if (*fpp)
                        closef(*fpp, p);
        if (fdp->fd_nfiles > NDFILE)
#ifdef DAMIR
                FREE(fdp->fd_ofiles, M_FILEDESC);
#endif
        free(fdp->fd_ofiles);
        fdp->fd_cdir->unreference();
        if (fdp->fd_rdir)
                fdp->fd_rdir->unreference();
#ifdef DAMIR
        FREE(fdp, M_FILEDESC);
#endif
        free(fdp);
}

/*
 * Internal form of close.
 * Decrement reference count on file structure.
 * Note: p may be NULL when closing a file
 * that was being passed in a message.
 */
int closef(struct file *fp, pthread_t p)
{
        struct flock lf;
        int error;

        if (fp == NULL)
        {
                return (0);
        }

        // struct vnode::ptr vp = vnode::get_from_file(fp);

        /*
         * POSIX record locking dictates that any close releases ALL
         * locks owned by this process.  This is handled by setting
         * a flag in the unlock to free ONLY locks obeying POSIX
         * semantics, and not to free BSD-style file locks.
         * If the descriptor was in a message, POSIX-style locks
         * aren't passed with the descriptor.
         */
        if (p && (p->process->p_flag & P_ADVLOCK) && fp->f_type == DTYPE_VNODE)
        {
                lf.l_whence = SEEK_SET;
                lf.l_start  = 0;
                lf.l_len    = 0;
                lf.l_type   = F_UNLCK;
                // vp          = (struct vnode *)fp->f_data;
                // VOP_ADVLOCK(vp, (char *)p, F_UNLCK, &lf, F_POSIX);
        }
        if (--fp->f_count > 0)
        {
                return (0);
        }

        if (fp->f_count < 0)
        {
                panic("closef: count < 0");
        }

        if ((fp->f_flag & FHASLOCK) && fp->f_type == DTYPE_VNODE)
        {
                lf.l_whence = SEEK_SET;
                lf.l_start  = 0;
                lf.l_len    = 0;
                lf.l_type   = F_UNLCK;
                // vp          = (struct vnode *)fp->f_data;
                // VOP_ADVLOCK(vp, (char *)fp, F_UNLCK, &lf, F_FLOCK);
        }

        // if (fp->f_ops)
        error = fp->close(p);
        // else
        //        error = 0;
        // file_free(fp);

        delete fp;

        return (error);
}
#if 0
/*
 * File Descriptor pseudo-device driver (/dev/fd/).
 *
 * Opening minor device N dup()s the file (if any) connected to file
 * descriptor N belonging to the calling process.  Note that this driver
 * consists of only the ``open()'' routine, because all subsequent
 * references to this file will be direct to the other driver.
 */
/* ARGSUSED */
int fdopen(dev_t dev, mode_t mode, int type, pthread_t p)
{
        /*
         * XXX Kludge: set curproc->p_dupfd to contain the value of the
         * the file descriptor being sought for duplication. The error
         * return ensures that the vnode for this device will be released
         * by vn_open. Open will detect this special error and take the
         * actions in dupfdopen below. Other callers of vn_open or VOP_OPEN
         * will simply report the error.
         */
        p->p_dupfd = minor(dev);
        return -ENODEV;
}
#endif
/*
 * Duplicate the specified descriptor to a free descriptor.
 */
int dupfdopen(struct filedesc *fdp, int indx, int dfd, mode_t mode, int error)
{
        struct file *wfp;
        struct file *fp;

        /*
         * If the to-be-dup'd fd number is greater than the allowed number
         * of file descriptors, or the fd to be dup'd has already been
         * closed, reject.  Note, check for new == old is necessary as
         * file_alloc could allocate an already closed to-be-dup'd descriptor
         * as the new descriptor.
         */
        fp = fdp->fd_ofiles[indx];
        if ((u32)dfd >= fdp->fd_nfiles || (wfp = fdp->fd_ofiles[dfd]) == NULL || fp == wfp)
                return -EBADF;
        ;

        /*
         * There are two cases of interest here.
         *
         * For ENODEV simply dup (dfd) to file descriptor
         * (indx) and return.
         *
         * For ENXIO steal away the file structure from (dfd) and
         * store it in (indx).  (dfd) is effectively closed by
         * this operation.
         *
         * Any other error code is just returned.
         */
        switch (error)
        {
        case -ENODEV:
                /*
                 * Check that the mode the file is being opened for is a
                 * subset of the mode of the existing descriptor.
                 */
                if (((mode & (FREAD | FWRITE)) | wfp->f_flag) != wfp->f_flag)
                        return (-EACCES);
                fdp->fd_ofiles[indx]     = wfp;
                fdp->fd_ofileflags[indx] = fdp->fd_ofileflags[dfd];
                wfp->f_count++;
                if (indx > fdp->fd_lastfile)
                        fdp->fd_lastfile = indx;
                return (0);

        case -ENXIO:
                /*
                 * Steal away the file pointer from dfd, and stuff it into indx.
                 */
                fdp->fd_ofiles[indx]     = fdp->fd_ofiles[dfd];
                fdp->fd_ofiles[dfd]      = NULL;
                fdp->fd_ofileflags[indx] = fdp->fd_ofileflags[dfd];
                fdp->fd_ofileflags[dfd]  = 0;
                /*
                 * Complete the clean up of the filedesc structure by
                 * recomputing the various hints.
                 */
                if (indx > fdp->fd_lastfile)
                        fdp->fd_lastfile = indx;
                else
                        while (fdp->fd_lastfile > 0 && fdp->fd_ofiles[fdp->fd_lastfile] == NULL)
                                fdp->fd_lastfile--;
                if (dfd < fdp->fd_freefile)
                        fdp->fd_freefile = dfd;
                return (0);

        default:
                return (error);
        }
        /* NOTREACHED */
}

int fdesc_getfile(unsigned int a_fd, struct filedesc *a_fdp, struct file **a_fpp)
{
        struct file *fp;

        if (a_fd >= a_fdp->fd_nfiles || (fp = a_fdp->fd_ofiles[a_fd]) == NULL)
        {
                return -EBADF;
        }

        if (fp->f_type != DTYPE_VNODE)
        {
                return -EINVAL;
        }

        *a_fpp = fp;
        return 0;
}
