#pragma once

#ifdef __cplusplus
#include <vfs/vnode.h>

struct vfs_lookup_context
{
        using path_buffer_t = char[PATH_MAX];

        vfs_lookup_context(pthread_t a_proc, char const *a_path, bool a_follow_symlinks, bool a_is_kernel_path = false);

        ~vfs_lookup_context();

        kern_return_t lookup_at(int a_dir_fd);

        bool is_dot() { return (m_component_name_len == 1) && (m_component_name[0] == '.'); }

        bool is_dotdot() { return (m_component_name_len == 2) && (m_component_name[0] == '.') && (m_component_name[1] == '.'); }

        // Functions for allocating buffers of MAX_PATH size
        static path_buffer_t *alloc_path_buffer();
        static void free_path_buffer(path_buffer_t *a_path_buffer);

        // vnodes where results are stored
        vnode::locked_ptr m_parent_vn;
        vnode::locked_ptr m_child_vn;

        // Information about the last processed component
        char const *m_component_name;
        size_t m_component_name_len;

        // Internal buffer where the user path and symlinks are stored
        char *m_buffer;
        char *m_buffer_end;

        // Pointer to a user address containing the path
        char const *m_userpath;

        // Points to the path copied in from userspace
        char *m_path;
        size_t m_path_len;

        // The calling process
        pthread_t m_proc;

        // These don't have to 4 bytes a pop, it's just for the padding
        natural_t m_follow_symlinks : 1, m_is_kernel_path : 1, m_has_trailing_slash : 1, __pad : (sizeof(natural_t) * CHAR_BIT) - 3;
};

#else
struct vnode;
#endif

struct mount;

/*
 * Check to see if a filesystem is mounted on a block device.
 */
int vfs_mountedon(struct vnode *vp);

/*
 * Move a vnode from one mount queue to another.
 */
void insmntque(struct vnode *vp, struct mount *mp);