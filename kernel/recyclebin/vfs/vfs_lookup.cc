
#include <etl/queue.h>
#define RBTNODE_FUNCTION_DEFINITIONS
#include <etl/algorithm.hh>
#include <etl/rbtnode.h>

#include <emerixx/file.h>
#include <fcntl.h>
#include <mach/defer.hh>
#include <vfs/mount.h>

#include <etl/utility.hh>
#include <kernel/zalloc.h>
#include <mach/thread.h>
#include <string.h>
#include <vfs/vfs.h>
#include <vfs/vfs_cache.h>

#include <emerixx/fsdata.hh>
#include <emerixx/process.h>

using path_buffer_zone_t = zone<vfs_lookup_context::path_buffer_t, FILEMAX, 1>;

static path_buffer_zone_t s_path_buffer_zone;

static kern_return_t lookup_loop(vfs_lookup_context *a_ctx, //
                                 char *a_cname,
                                 size_t a_cnamlen,
                                 i32 a_loop_cnt,
                                 bool a_last_cn);

static kern_return_t lookup_context_copyin_user_path(vfs_lookup_context *a_ctx)
{
        KASSERT(a_ctx->m_path == nullptr);

        a_ctx->m_buffer = a_ctx->m_path = (char *)s_path_buffer_zone.alloc();
        a_ctx->m_buffer_end             = a_ctx->m_buffer + PATH_MAX;

        KASSERT(a_ctx->m_buffer);

        // retval = copyinstr(a_ctx->m_userpath, a_ctx->m_path, PATH_MAX, &(a_ctx->m_path_len));
        kern_return_t retval = KERN_SUCCESS;
        char *c;
        if (!a_ctx->m_is_kernel_path)
        {
                c = stpncpyin(a_ctx->m_path, a_ctx->m_userpath, PATH_MAX, &retval);
        }
        else
        {
                c = stpncpy(a_ctx->m_path, a_ctx->m_userpath, PATH_MAX);
        }

        if (KERN_STATUS_SUCCESS(retval))
        {
                a_ctx->m_path_len = static_cast<size_t>(c - a_ctx->m_path);

                // Save the terminator
                a_ctx->m_buffer += a_ctx->m_path_len + 1;

                // But don't count it
                // a_ctx->m_path_len -= 1;
        }

        return retval;
}

static kern_return_t lookup_context_readlink(vfs_lookup_context *a_ctx, char **a_strout, size_t *a_strout_len)
{
        char buffer[_POSIX_SYMLINK_MAX];

        iovec iov;
        iov.iov_base = buffer;
        iov.iov_len  = sizeof(buffer);

        uio u;
        u.uio_iov    = &iov;
        u.uio_iovcnt = 1;
        u.uio_offset = 0;
        u.uio_resid  = sizeof(buffer);
        u.uio_segflg = UIO_SYSSPACE;
        u.uio_rw     = UIO_READ;

        int error = a_ctx->m_child_vn->uio_move(&u, 0);

        if (!error)
        {
                // + 1 for a terminating zero
                size_t len = u.uio_offset + 1;

                if (!((a_ctx->m_buffer + len) < a_ctx->m_buffer_end))
                {
                        return -ENAMETOOLONG;
                }

                *a_strout = a_ctx->m_buffer;

                a_ctx->m_buffer += len;

                // We don't include the terminating zero in the length returned to the caller
                *a_strout_len = len - 1;

                strncpy(*a_strout, buffer, *a_strout_len);

                (*a_strout)[(*a_strout_len)] = 0;
        }
        return error;
}

static char *_path_get_component(char *path, size_t *len)
{
        *len = 0;

        while (*path == '/')
        {
                path++;
        }

        while (path[*len] && path[*len] != '/')
        {
                (*len)++;
        }

        return *len ? path : NULL;
}

#define get_first_component(path, len) (_path_get_component((path), (len)))
#define get_next_component(path, len)  (_path_get_component((path) + *(len), (len)))

#define TRAILING_SLASH BIT(0)
#define LAST_COMPONENT BIT(1)
#define IN_LINK_LOOP   BIT(2)
#define LOOKUP_SUCCESS BIT(3)
#define LOOKUP_FAILURE 0

static kern_return_t start_lookup(vfs_lookup_context *a_ctx, char *a_path, UNUSED size_t a_path_len, i32 a_loop_cnt)
{
        kern_return_t retval = KERN_SUCCESS;

        bool trailing_slash = a_path[a_path_len - 1] == '/';

        a_loop_cnt++;
        char *cname, *cname_next;
        natural_t cnamlen, cnamlen_next;
        for (cname = get_first_component(a_path, &cnamlen); //
             cname != nullptr && KERN_STATUS_SUCCESS(retval);
             cname = cname_next, cnamlen = cnamlen_next)
        {
                cnamlen_next = cnamlen;
                cname_next   = get_next_component(cname, &cnamlen_next);

                // Move the child into the parent
                a_ctx->m_parent_vn = etl::move(a_ctx->m_child_vn);

                retval = lookup_loop(a_ctx, cname, cnamlen, a_loop_cnt, (cname_next == nullptr));
                if (KERN_STATUS_SUCCESS(retval) && (S_ISDIR(a_ctx->m_child_vn->m_fstat.st_mode) == true)
                    && (a_ctx->m_child_vn->v_un.vu_mountedhere != nullptr))
                {
                        vnode::ptr nroot;
                        KASSERT(KERN_STATUS_SUCCESS(a_ctx->m_child_vn->v_un.vu_mountedhere->root(nroot)));
                        if (nroot == a_ctx->m_parent_vn)
                        {
                                a_ctx->m_child_vn = etl::move(a_ctx->m_parent_vn);
                        }
                        else
                        {
                                a_ctx->m_child_vn = etl::move(nroot);
                        }
                }

                switch (trailing_slash | ((cname_next == nullptr) << 1) | ((a_loop_cnt > 0) << 2) | ((retval == KERN_SUCCESS) << 3))
                {
                case LOOKUP_SUCCESS: {
                        if ((S_ISDIR(a_ctx->m_child_vn->m_fstat.st_mode) == false))
                        {
                                retval = KERN_FAIL(ENOTDIR);
                        }

                        break;
                }

                case LOOKUP_FAILURE: {
                        a_ctx->m_parent_vn = nullptr;
                        a_ctx->m_child_vn  = nullptr;
                        break;
                }

                case IN_LINK_LOOP | LOOKUP_FAILURE: {
                        a_ctx->m_parent_vn = nullptr;
                        a_ctx->m_child_vn  = nullptr;
                        break;
                }
                case IN_LINK_LOOP | LOOKUP_SUCCESS: {
                        if ((S_ISDIR(a_ctx->m_child_vn->m_fstat.st_mode) == false))
                        {
                                retval = KERN_FAIL(ENOTDIR);
                        }

                        break;
                }
                case LAST_COMPONENT | LOOKUP_FAILURE: {

                        a_ctx->m_component_name     = cname;
                        a_ctx->m_component_name_len = cnamlen;
                        break;
                }

                case LAST_COMPONENT | LOOKUP_SUCCESS: {

                        a_ctx->m_component_name     = cname;
                        a_ctx->m_component_name_len = cnamlen;
                        break;
                }

                case LAST_COMPONENT | IN_LINK_LOOP | LOOKUP_FAILURE: {
                        asm volatile("nop");
                        break;
                }

                case LAST_COMPONENT | IN_LINK_LOOP | LOOKUP_SUCCESS: {
                        asm volatile("nop");
                        break;
                }

                case TRAILING_SLASH | IN_LINK_LOOP | LOOKUP_FAILURE: {
                        asm volatile("nop");
                        break;
                }

                case TRAILING_SLASH | IN_LINK_LOOP | LOOKUP_SUCCESS: {
                        asm volatile("nop");
                        break;
                }

                case TRAILING_SLASH | LOOKUP_FAILURE: {
                        a_ctx->m_parent_vn = nullptr;
                        a_ctx->m_child_vn  = nullptr;
                        break;
                }

                case TRAILING_SLASH | LOOKUP_SUCCESS: {
                        if ((S_ISDIR(a_ctx->m_child_vn->m_fstat.st_mode) == false))
                        {
                                retval = KERN_FAIL(ENOTDIR);
                        }
                        break;
                }

                case TRAILING_SLASH | LAST_COMPONENT | LOOKUP_FAILURE: {
                        a_ctx->m_component_name     = cname;
                        a_ctx->m_component_name_len = cnamlen;
                        break;
                }

                case TRAILING_SLASH | LAST_COMPONENT | LOOKUP_SUCCESS: {
                        if ((S_ISDIR(a_ctx->m_child_vn->m_fstat.st_mode) == false))
                        {
                                retval = KERN_FAIL(ENOTDIR);
                        }

                        a_ctx->m_component_name     = cname;
                        a_ctx->m_component_name_len = cnamlen;
                        break;
                }

                case TRAILING_SLASH | LAST_COMPONENT | IN_LINK_LOOP | LOOKUP_FAILURE: {
                        asm volatile("nop");
                        break;
                }

                case TRAILING_SLASH | LAST_COMPONENT | IN_LINK_LOOP | LOOKUP_SUCCESS: {
                        asm volatile("nop");
                        break;
                }

                default: {
                        panic("Unknown state");
                        break;
                }
                }

                if (retval != KERN_SUCCESS)
                {
                        break;
                }
        }

        return retval;
}

kern_return_t lookup_loop(vfs_lookup_context *a_ctx, char *a_cname, size_t a_cnamlen, i32 a_loop_cnt, bool a_last_cn)
{
        if (a_cnamlen >= NAME_MAX)
        {
                return KERN_FAIL(ENAMETOOLONG);
        }

        // '.' as a component means the parent we already have
        if (a_cnamlen == 1 && (a_cname[0] == '.'))
        {
                a_ctx->m_child_vn = etl::move(a_ctx->m_parent_vn);
                return KERN_SUCCESS;
        }

        // Check access for lookup
        kern_return_t retval = a_ctx->m_parent_vn->access(VREAD);

        if (retval != KERN_SUCCESS)
        {
                return retval;
        }

        while (true)
        {
                /*
                2 PID 3 path         /bin
                                     resolves to
                                     /usr/bin

                3 holds         0xffffffff8029dad0 (usr)
                3 tries to lock 0xffffffff802a1990 (bin)

                PID 4 path         /bin/../lib/gcc/x86_64-rs64-musl/8.2.0/specs
                                   resolves to
                                   /usr/bin/../lib/gcc/x86_64-rs64-musl/8.2.0/specs

                4 holds         0xffffffff802a1990 (bin)
                4 tries to lock 0xffffffff8029dad0 (usr)
                */

                // Access okay, fetch the child node through the namecache.
                retval = _vfs_cache_lookup(a_ctx->m_parent_vn, a_cname, a_cnamlen, a_ctx->m_child_vn);

                if (retval != KERN_FAIL(EAGAIN))
                {
                        break;
                }

                vnode::ptr unlocked_parent{etl::move(a_ctx->m_parent_vn)};
                thread_block(nullptr); // Yield
                a_ctx->m_parent_vn = etl::move(unlocked_parent);
        }

        if (retval != KERN_SUCCESS)
        {
                return retval;
        }

        // If the lookup succeed and the child is null means the component
        // resolved
        // to the parent itself. In this case we move the parent to the
        // child and continue.
        if (a_ctx->m_child_vn.is_null())
        {
                a_ctx->m_child_vn = etl::move(a_ctx->m_parent_vn);
                return KERN_SUCCESS;
        }

        // If not dealing with a link, we are done with this component.
        if (S_ISLNK((a_ctx->m_child_vn->m_fstat.st_mode)) == false)
        {
                return KERN_SUCCESS;
        }

        // The a_ctx->m_follow_symlinks is set when symbolic links are to be
        // followed when they occur at the end of the name translation process.
        // Symbolic links are always followed for all other pathname
        // components other than the last.

        // If we are not in a link loop, the last component is reached and we are
        // not following links, we are done.
        if ((a_loop_cnt == 0) && (a_last_cn == true) && (a_ctx->m_follow_symlinks == false))
        {
                return KERN_SUCCESS;
        }

        // Can we even loop?
        if ((a_loop_cnt + 1) >= _POSIX_SYMLOOP_MAX)
        {
                return KERN_FAIL(ELOOP);
        }

        char *path;
        size_t path_len;

        retval = lookup_context_readlink(a_ctx, &path, &path_len);

        if (retval != KERN_SUCCESS)
        {
                return retval;
        }

        if (path_len == 0)
        {
                return KERN_FAIL(ENOENT);
        }

        // We need to setup the lookup context for another loop by loading
        // the start dir into m_child_vn

        // Determine the parent vnode(loaded into child), if path is relative
        // we use the current parent
        if ((path[0] == '/') && (a_ctx->m_parent_vn.get_raw() != vnode::root.get_raw()))
        {
                // Should use a_proc->process->p_fd->fd_rdir when ready
                a_ctx->m_parent_vn = vnode::root;
        }

        a_ctx->m_child_vn = etl::move(a_ctx->m_parent_vn);

        return start_lookup(a_ctx, path, path_len, a_loop_cnt);
}

vfs_lookup_context::vfs_lookup_context(pthread_t a_proc, char const *a_user_path, bool a_follow_symlinks, bool a_is_kernel_path)
    : m_component_name(nullptr)
    , m_component_name_len(0)
    , m_buffer(nullptr)
    , m_buffer_end(nullptr)
    , m_userpath(a_user_path)
    , m_path(nullptr)
    , m_path_len(0)
    , m_proc(a_proc)
    , m_follow_symlinks(a_follow_symlinks)
    , m_is_kernel_path(a_is_kernel_path)
    , m_has_trailing_slash(false)
{
}

vfs_lookup_context::~vfs_lookup_context()
{
        if (m_path)
        {
                KASSERT(m_buffer_end != nullptr);
                s_path_buffer_zone.free((char(*)[PATH_MAX])m_path);
        }
}

struct vfs_cache_node
{
        rbtnode_data m_rblink;
        queue_chain_t m_lrulink;
        char m_path[NAME_MAX];
        char m_pad[1];
};

queue_head_t s_cache_lru;
rbtnode_t s_cache_root;
rbtnode_data s_cache_sentinel;
vfs_cache_node s_vfs_cache_storage[0x100];

// Stats
int vfs_cache_hits;
int vfs_cache_miss;

template <> vfs_cache_node *rbtnode_to_type<vfs_cache_node *>(rbtnode_data *a_rbtnode)
{
        return rbtnode_type(a_rbtnode, vfs_cache_node, m_rblink);
}

template <> rbtnode_data *type_to_rbtnode<vfs_cache_node *>(vfs_cache_node *a_vfs_cache_node)
{
        return &a_vfs_cache_node->m_rblink;
}

template <> bool rbtnode_is_less<char *, vfs_cache_node *>(char *a_lhs, vfs_cache_node *a_rhs)
{
        return strcmp(a_lhs, a_rhs->m_path) < 0;
}

template <> bool rbtnode_is_less<vfs_cache_node *, char *>(vfs_cache_node *a_lhs, char *a_rhs)
{
        return strcmp(a_lhs->m_path, a_rhs) < 0;
}

template <> bool rbtnode_is_less<vfs_cache_node *, vfs_cache_node *>(vfs_cache_node *a_lhs, vfs_cache_node *a_rhs)
{
        return strcmp(a_lhs->m_path, a_rhs->m_path) < 0;
}

// ********************************************************************************************************************************
static kern_return_t vfs_cache_init()
// ********************************************************************************************************************************
{
        queue_init(&s_cache_lru);

        s_cache_root = rbtnode_init_sentinel(&s_cache_sentinel);

        for (vfs_cache_node &node : s_vfs_cache_storage)
        {
                node.m_rblink  = {};
                node.m_lrulink = {};
                etl::clear(node.m_path);
                etl::clear(node.m_pad);

                enqueue_head(&s_cache_lru, &node.m_lrulink);
        }

        vfs_cache_hits = 0;
        vfs_cache_miss = 0;

        return KERN_SUCCESS;
}

early_initcall(vfs_cache_init);

kern_return_t vfs_lookup_context::lookup_at(int a_dir_fd)
{
        kern_return_t retval = lookup_context_copyin_user_path(this);

        if (retval != KERN_SUCCESS)
        {
                return retval;
        }

        if (m_path_len == 0)
        {
                return KERN_FAIL(ENOENT);
        }

        if (m_path_len < NAME_MAX && !(m_path_len == 1 && m_path[0] == '.')
            && !(m_path_len == 2 && m_path[0] == '.' && m_path[1] == '.'))
        {
                vfs_cache_node *cache = rbtnode_find<char *, vfs_cache_node *>(m_path, s_cache_root);

                if (cache == nullptr)
                {
                        // Miss
                        vfs_cache_miss++;

                        cache = queue_containing_record(queue_last(&s_cache_lru), vfs_cache_node, m_lrulink);

                        if (cache->m_path[0] != 0)
                        {
                                rbtnode_delete(&cache->m_rblink, &s_cache_root);
                        }

                        strcpy(cache->m_path, m_path);

                        KASSERT(cache == rbtnode_insert<vfs_cache_node *>(cache, &s_cache_root, &s_cache_sentinel));
                }
                else
                {
                        // Hit
                        vfs_cache_hits++;
                        KASSERT(strcmp(cache->m_path, m_path) == 0);
                }

                if (queue_first(&s_cache_lru) != &cache->m_lrulink)
                {
                        remqueue(&cache->m_lrulink);
                        enqueue_head(&s_cache_lru, &cache->m_lrulink);
                }
        }

        m_has_trailing_slash = m_path[m_path_len - 1] == '/';

        if (m_path[0] == '/')
        {
                // Absolute path, we don't care about a_at_dir_fd
                m_parent_vn = vnode::root; // Should use a_proc->process->p_fd->fd_rdir when ready
        }
        else
        {
                // Relative path
                if (a_dir_fd == AT_FDCWD)
                {
                        // Start at the current working directory.
                        m_parent_vn = fs_context_getcwd(uthread_self()->m_fsctx);
                }
                else
                {
                        // We start at the dir pointed to by the file descriptor
                        struct file *f;
                        // m_proc->process->fd_to_file(a_dir_fd);
                        retval = file::get(current_thread()->pthread, a_dir_fd, &f);

                        if (KERN_STATUS_FAILURE(retval))
                        {
                                return retval;
                        }

                        m_parent_vn = vnode::get_from_file(f);

                        if (m_parent_vn.is_null())
                        {
                                return KERN_FAIL(EINVAL);
                        }

                        if (S_ISDIR(m_parent_vn->m_fstat.st_mode) == false)
                        {
                                return KERN_FAIL(ENOTDIR);
                        }
                }
        }

        // We start with the parent as the child in order to match the lookup loop sementics.
        m_child_vn = etl::move(m_parent_vn);

        return start_lookup(this, m_path, m_path_len, -1);
}

// Functions for allocating buffers of MAX_PATH size
vfs_lookup_context::path_buffer_t *vfs_lookup_context::alloc_path_buffer() { return s_path_buffer_zone.alloc(); }

void vfs_lookup_context::free_path_buffer(vfs_lookup_context::path_buffer_t *a_path_buffer)
{
        s_path_buffer_zone.free(a_path_buffer);
}

static int init_user_path_zone()
{
        path_buffer_zone_t::init(&s_path_buffer_zone, "user path");
        return 0;
}

subsys_initcall(init_user_path_zone);
