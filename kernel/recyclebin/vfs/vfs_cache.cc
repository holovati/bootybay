
#include <aux/init.h>
#include <kernel/zalloc.h>
#include <string.h>
#include <vfs/vfs_cache.h>
#include <vfs/vnode.h>

#include <mach/intrusive/hash_map.hh>

/*
 * Invalidate a all entries to particular vnode.
 *
 * We actually just increment the v_id, that will do it. The entries will
 * be purged by lookup as they get found. If the v_id wraps around, we
 * need to ditch the entire cache, to avoid confusion. No valid vnode will
 * ever have (v_id == 0).
 */
void vfs_cache_purge(struct vnode *vp) { vp->m_age = 0; }

namespace
{

struct lru_node_metainfo
{
        lru_node_metainfo(vnode::locked_ptr &a_parent_vnode, const char *a_child_name, size_t a_child_name_len);

        lru_node_metainfo(vnode::locked_ptr &a_parent_vnode,
                          vnode::locked_ptr &a_child_vnode,
                          const char *a_child_name,
                          size_t a_child_name_len,
                          char *a_local_name_buffer);

        bool is_purged() const;

        char const *m_name;
        size_t m_namelen;
        size_t m_parent_vnode_age;
        size_t m_child_vnode_age;
        vnode::ptr m_parent_vnode;
        vnode::ptr m_child_vnode;
};

inline lru_node_metainfo::lru_node_metainfo(vnode::locked_ptr &a_parent_vnode, const char *a_child_name, size_t a_child_name_len)
    : m_name(a_child_name)
    , m_namelen(a_child_name_len)
    , m_parent_vnode_age(a_parent_vnode->m_age)
    , m_child_vnode_age(0)
    , m_parent_vnode(a_parent_vnode.get_raw())
    , m_child_vnode(nullptr)
{
}

inline lru_node_metainfo::lru_node_metainfo(vnode::locked_ptr &a_parent_vnode,
                                            vnode::locked_ptr &a_child_vnode,
                                            const char *a_child_name,
                                            size_t a_child_name_len,
                                            char *a_local_name_buffer)
    : lru_node_metainfo(a_parent_vnode, a_child_name, a_child_name_len)
{
        // Check if its a negative entry
        if (!a_child_vnode.is_null())
        {
                m_child_vnode     = a_child_vnode;
                m_child_vnode_age = a_child_vnode->m_age;
        }

        strncpy(a_local_name_buffer, m_name, m_namelen);
        m_name = a_local_name_buffer;
}

inline bool lru_node_metainfo::is_purged() const
{
        return (m_namelen == 0) && (m_parent_vnode_age == 0) && (m_child_vnode_age == 0) && (m_name == (char *)vfs_cache_purge);
}

static inline size_t hash_type(lru_node_metainfo const &a_key)
{
        size_t hash = 5381;
        for (int i = 0; i < a_key.m_namelen; ++i)
        {
                hash = ((hash << 5) + hash) + a_key.m_name[i]; /* hash * 33 + c */
        }
        return hash;
}

static bool operator==(lru_node_metainfo const &a_cached_entry, lru_node_metainfo const &a_search_entry)
{
        // We never match purged entries, let them be pushed out over time
        if (a_cached_entry.is_purged())
        {
                return false;
        }

        // Are the name lengths equal?
        if (a_cached_entry.m_namelen != a_search_entry.m_namelen)
        {
                return false;
        }

        // Do the parents have the same age?
        if (a_cached_entry.m_parent_vnode_age != a_search_entry.m_parent_vnode_age)
        {
                return false;
        }

        // Is the parent pointer the same, should be!
        if (const_cast<lru_node_metainfo &>(a_cached_entry).m_parent_vnode.get_raw()
            != const_cast<lru_node_metainfo &>(a_search_entry).m_parent_vnode.get_raw())
        {
                return false;
        }

        // Do the parents have the same mountpoint?
        if (const_cast<lru_node_metainfo &>(a_cached_entry).m_parent_vnode->v_mount
            != const_cast<lru_node_metainfo &>(a_search_entry).m_parent_vnode->v_mount)
        {
                return false;
        }

        // Last compare the names
        if (strncmp(a_cached_entry.m_name, a_search_entry.m_name, a_cached_entry.m_namelen))
        {
                return false;
        }

        return true;
}

struct lru_node : vnode::observer
{
        lru_node(vnode::locked_ptr &a_parent_vnode,
                 vnode::locked_ptr &a_child_vnode,
                 const char *a_child_name,
                 size_t a_childe_name_len);
        ~lru_node();

        void touch();

        void purge();

        void on_vnode_dentry_created(vnode *a_parent, vnode *a_child, const char *a_child_name, size_t a_child_name_len) override;

        void on_vnode_dentry_removed(vnode *a_parent, vnode *a_child, const char *a_child_name, size_t a_child_name_len) override;

        static void *operator new(size_t);
        static void operator delete(void *);

        char m_namebuffer[NAME_MAX + 1];
        lru_node_metainfo m_metainfo;

        using hash_link_t = intrusive::static_unordered::link<lru_node>;
        hash_link_t m_hash_link;
        using hash_map_t
            = intrusive::static_unordered::map<lru_node, &lru_node::m_hash_link, lru_node_metainfo, &lru_node::m_metainfo, 32>;

        using dequeue_link_t = intrusive::tailq::link<lru_node>;
        dequeue_link_t m_dequeue_link;
        using dequeue_t = dequeue_link_t::head<&lru_node::m_dequeue_link>;
};

using lru_node_zone_t = zone<lru_node, 1024, 128, ZONE_EXHAUSTIBLE>::type;

static lru_node_zone_t s_lru_node_zone;
static lru_node::hash_map_t s_node_map;
static lru_node::dequeue_t s_node_queue;

static size_t s_positive_match_count;
static size_t s_negative_match_count;
static size_t s_no_match_count;
static size_t s_name_to_long_count;

inline void *lru_node::operator new(size_t)
{
        void *p;

        while (!(p = s_lru_node_zone.alloc()))
        {
                // We don't have space for a new entry, evict last one
                delete s_node_queue.back(); // Might sleep
        }

        return p;
}

inline void lru_node::operator delete(void *p) { s_lru_node_zone.free((lru_node *)p); }

inline lru_node::lru_node(vnode::locked_ptr &a_parent_vnode,
                          vnode::locked_ptr &a_child_vnode,
                          const char *a_child_name,
                          size_t a_child_name_len)
    : m_metainfo(a_parent_vnode, a_child_vnode, a_child_name, a_child_name_len, m_namebuffer)
{
        s_node_map.insert(this);
        s_node_queue.insert_head(this);
        m_metainfo.m_parent_vnode->observer_attach(this);
}

inline lru_node::~lru_node()
{
        if (!m_metainfo.is_purged())
        {
                m_metainfo.m_parent_vnode->observer_detach(this);
        }

        s_node_queue.remove(this);
        s_node_map.remove(this);
}

void lru_node::touch()
{
        s_node_queue.remove(this);
        s_node_queue.insert_head(this);
}

void lru_node::purge()
{
        m_metainfo.m_name             = (char *)(vfs_cache_purge);
        m_metainfo.m_namelen          = 0;
        m_metainfo.m_parent_vnode_age = 0;
        m_metainfo.m_child_vnode_age  = 0;
        m_metainfo.m_child_vnode      = nullptr;

        m_metainfo.m_parent_vnode->observer_detach(this);
        m_metainfo.m_parent_vnode = nullptr;
}

} // namespace

void vfs_cache_flush()
{
        s_node_queue.for_each([](lru_node *a_node) { delete a_node; });

        s_positive_match_count = 0;
        s_negative_match_count = 0;
        s_no_match_count       = 0;
        s_name_to_long_count   = 0;
}

void lru_node::on_vnode_dentry_created(vnode *a_parent, vnode *a_child, const char *a_child_name, size_t a_child_name_len)
{
        KASSERT(a_parent == m_metainfo.m_parent_vnode.get_raw());

        if (m_metainfo.m_namelen != a_child_name_len)
        {
                return;
        }

        if (strncmp(m_metainfo.m_name, a_child_name, m_metainfo.m_namelen) != 0)
        {
                return;
        }

        KASSERT(m_metainfo.m_child_vnode.get_raw() == nullptr);

        m_metainfo.m_child_vnode = a_child;

        touch();
}

void lru_node::on_vnode_dentry_removed(vnode *a_parent, vnode *a_child, const char *a_child_name, size_t a_child_name_len)
{
        KASSERT(a_parent == m_metainfo.m_parent_vnode.get_raw());

        if (m_metainfo.m_child_vnode.get_raw() != a_child)
        {
                return;
        }

        if (a_child)
        {
                KASSERT(m_metainfo.m_child_vnode->m_age == a_child->m_age);
        }

        purge();
}

/*
 * Add an entry to the cache.
 */
void vfs_cache_enter(struct vnode *dvp, struct vnode *vp, struct componentname *cnp)
{
        return;
#if 0
  // Name to long, bail.
  if (cnp->cn_namelen > sizeof(lru_node::m_namebuffer)) {
    s_name_to_long_count++;
    cnp->cn_flags &= ~MAKEENTRY;
    return;
  }

  // Just for testing soo we don't enter duplicates(DEBUG)
  lru_node *node = s_node_map.find({dvp, cnp});
  if (node) {
    if (node->m_metainfo.m_vnode_age == vp->m_age) {
      node->touch();
      return;
    }
    node->purge();
  }

  new lru_node{vp, dvp, cnp};
#endif
}

/*
 * Lookup an entry in the cache
 *
 * We don't do this if the segment name is long, simply so the cache
 * can avoid holding long names (which would either waste space, or
 * add greatly to the complexity).
 *
 * Lookup is called with dvp pointing to the directory to search,
 * cnp pointing to the name of the entry being sought. If the lookup
 * succeeds, the vnode is returned in *vpp, and a status of -1 is
 * returned. If the lookup determines that the name does not exist
 * (negative cacheing), a status of ENOENT is returned. If the lookup
 * fails, a status of zero is returned.
 */
#if 0
struct vnode *vfs_cache_lookup(struct vnode *dvp,
                               struct componentname *cnp,
                               enum vfs_cache_lookup_result *a_result_out) {
  vnode *result = nullptr;

  // Disable the cache while we are moving everything to the new lookup routine.
  *a_result_out = VFS_CACHE_NO_MATCH;
  return result;
#if 0
  // Name to long
  if (cnp->cn_namelen > sizeof(lru_node::m_namebuffer)) {
    s_name_to_long_count++;
    cnp->cn_flags &= ~MAKEENTRY;
    *a_result_out = VFS_CACHE_NAME_TO_LONG;
    return result;
  }

  lru_node *node = s_node_map.find({dvp, cnp});

  // Not found, bail
  if (node == nullptr) {
    s_no_match_count++;
    *a_result_out = VFS_CACHE_NO_MATCH;
    return result;
  }

  result = node->m_metainfo.m_vnode;

  // Found, but we need to verify age on the vnode. vfs_cache_purge zeros this
  // out on the vnode
  if (result && (result->m_age != node->m_metainfo.m_vnode_age)) {
    s_no_match_count++;
    // Node has been purged, let the cache know about it.
    node->purge();
    *a_result_out = VFS_CACHE_NO_MATCH;

    return result;
  }

  bool keep_match = cnp->cn_nameiop == 0;

  // We have a match, is it positive or negative
  if (result) {
    s_positive_match_count++;
    // Positive match, what do we do with it?
    *a_result_out = VFS_CACHE_POSITIVE_MATCH;
    // Does the client want it?
    if (keep_match && cnp->cn_flags & MAKEENTRY) {
      // Yes, it's still wanted
      node->touch();
    } else {
      // Its not wanted anymore
      node->purge();
    }
  } else {
    s_negative_match_count++;
    // Negative match
    *a_result_out = VFS_CACHE_NEGATIVE_MATCH;
    // Are we planning to operate on it?
    if (!keep_match) {
      // Yes we are, lets purge the negative node
      // And report we did not find it
      node->purge();
      *a_result_out = VFS_CACHE_NO_MATCH;

    } else {
      // It remains as a negative node
      node->touch();
    }
  }

  return result;
#endif
}
#endif
int _vfs_cache_enter(vnode::locked_ptr &a_parent_vnode,
                     vnode::locked_ptr &a_child_vn,
                     char const *a_child_name,
                     size_t a_child_name_len)
{
        return 0;
}

int _vfs_cache_lookup(vnode::locked_ptr &a_parent_vnode,
                      char const *a_child_name,
                      size_t a_child_name_len,
                      vnode::locked_ptr &a_child_vn_out)
{
        KASSERT(!(a_child_name_len == 1 && a_child_name[0] == '.'));
        KASSERT(a_child_name_len < sizeof(lru_node::m_namebuffer));

        lru_node *node = s_node_map.find({a_parent_vnode, a_child_name, a_child_name_len});

        if (node)
        {
                node->touch();

                if (node->m_metainfo.m_child_vnode.is_null())
                {
                        // Negative entry
                        s_negative_match_count++;
                        return -ENOENT;
                }

                if (int error = node->m_metainfo.m_child_vnode->try_lock(); error)
                {
                        return error;
                }

                node->m_metainfo.m_child_vnode->unlock();

                a_child_vn_out = node->m_metainfo.m_child_vnode;

                return 0;
        }

        int error = a_parent_vnode->lookup(a_child_name, a_child_name_len, a_child_vn_out);

        if (error && error != -ENOENT)
        {
                return error;
        }

        // Create a new entry might be negative
        if (!(error == KERN_SUCCESS && a_child_vn_out.is_null()))
        {
                node = new lru_node{a_parent_vnode, a_child_vn_out, a_child_name, a_child_name_len};
        }

        return error;
}

static int init_vfs_cache()
{
        s_node_map.init();
        s_node_queue.init();

        s_positive_match_count = 0;
        s_negative_match_count = 0;
        s_no_match_count       = 0;
        s_name_to_long_count   = 0;

        s_lru_node_zone.init(&s_lru_node_zone, "vfs namecache");

        // lru_node *nodes = (lru_node *)mallc(sizeof(lru_node) * 32);

        // for (int i = 0; i < 32; ++i) {
        //  s_node_freelist.insert_head(&nodes[i]);
        //}

        return 0;
}

subsys_initcall(init_vfs_cache);
