#pragma once
#include <vfs/vnode.h>

int _vfs_cache_enter(vnode::locked_ptr &a_parent_vnode,
                     vnode::locked_ptr &a_child_vn,
                     char const *a_child_name,
                     size_t a_child_name_len);

int _vfs_cache_lookup(vnode::locked_ptr &a_parent_vnode,
                      char const *a_child_name,
                      size_t a_child_name_len,
                      vnode::locked_ptr &a_child_vn_out);

void vfs_cache_flush();