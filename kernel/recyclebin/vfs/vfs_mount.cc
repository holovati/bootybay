
#include <emerixx/sleep.h>
#include <kernel/lock.h>
#include <mach/thread.h>
#include <malloc.h>
#include <strings.h>
#include <sys/sysmacros.h>
#include <vfs/mount.h>
#include <vfs/vfs.h>

/* Mounted filesystem list */
mount::mnt_list mount::mountlist;

/* Allocation lock */
static simple_lock_data_t s_mnt_lock;

__attribute__((constructor)) static void amodule_init()
{
        mount::mountlist.init();
        simple_lock_init(&s_mnt_lock);
}

mount::mount(dev_t dev, vnode::ptr &vncovered, const char *fsname, integer_t flags)
    : m_dev(dev)
    , mnt_vnodecovered(vncovered)
    , fs_name(fsname)
    , mnt_flag(flags)
    , mnt_maxsymlinklen(0)
    , m_vn_active(0)
{
        if (s_mnt_lock.lock_data == 0)
        {
                panic("Contructing mount without lock");
        }

        bzero(&mnt_stat, sizeof(mnt_stat));
        mnt_vnodelist.init(); /* list of vnodes this mount */

        mountlist.insert_tail(this);
}

mount::~mount()
{
        if (s_mnt_lock.lock_data == 0)
        {
                panic("Destroying mount without lock");
        }
        mountlist.remove(this);
}

int mount::lock()
{
        while (mnt_flag & MNT_MLOCK)
        {
                mnt_flag |= MNT_MWAIT;
                // sleep(this, PVFS, __PRETTY_FUNCTION__);
                uthread_sleep(this);
        }
        mnt_flag |= MNT_MLOCK;
        return (0);
}

void mount::unlock()
{
        if ((mnt_flag & MNT_MLOCK) == 0)
        {
                panic("vfs_unlock: not locked");
        }

        mnt_flag &= ~MNT_MLOCK;

        if (mnt_flag & MNT_MWAIT)
        {
                mnt_flag &= ~MNT_MWAIT;
                uthread_wakeup(this);
        }
}

int mount::busy()
{
        while (mnt_flag & MNT_MPBUSY)
        {
                mnt_flag |= MNT_MPWANT;
                // sleep(&mnt_flag, PVFS, __PRETTY_FUNCTION__);
                uthread_sleep(&mnt_flag);
        }

        if (mnt_flag & MNT_UNMOUNT)
        {
                return (1);
        }

        mnt_flag |= MNT_MPBUSY;
        return (0);
}

void mount::unbusy()
{
        if ((mnt_flag & MNT_MPBUSY) == 0)
        {
                panic("vfs_unbusy: not busy");
        }

        mnt_flag &= ~MNT_MPBUSY;

        if (mnt_flag & MNT_MPWANT)
        {
                mnt_flag &= ~MNT_MPWANT;
                // wakeup(&mnt_flag);
                uthread_wakeup(&mnt_flag);
        }
}

#if DIAGNOSTIC
int busyprt            = 0; /* print out busy vnodes */
struct ctldebug debug1 = {"busyprt", &busyprt};
#endif

#if 0
extern void vclean(struct vnode *vp, int flags);

int mount::vflush(struct vnode *skipvp, int flags)
{
        struct vnode *vp, *nvp;
        int busy = 0;
        if ((mnt_flag & MNT_MPBUSY) == 0)
        {
                panic("vflush: not busy");
        }
loop:
        for (vp = mnt_vnodelist.lh_first; vp; vp = nvp)
        {
                if (vp->v_mount != this)
                {
                        goto loop;
                }

                nvp = vp->v_mntvnodes.le_next;
                /*
                 * Skip over a selected vnode.
                 */
                if (vp == skipvp)
                {
                        continue;
                }

                /*
                 * Skip over a vnodes marked VSYSTEM.
                 */
#if 0
                if ((flags & SKIPSYSTEM) && (vp->v_flag & VSYSTEM))
                {
                        continue;
                }
#endif
                /*
                 * If WRITECLOSE is set, only flush out regular file
                 * vnodes open for writing.
                 */
                if ((flags & WRITECLOSE) && (/*vp->v_writecount == 0 ||*/ S_ISREG(vp->m_fstat.st_mode) == false))
                {
                        continue;
                }
                /*
                 * With v_usecount == 0, all we need to do is clear
                 * out the vnode data structures and we are done.
                 */
                if (vp->m_usecount == 0)
                {
                        // vgone(vp);
                        // If the vnode use count is zero it should be gone already
                        // vnode::unreference
                        panic("How has this happened");
                        continue;
                }
                /*
                 * If FORCECLOSE is set, forcibly close the vnode.
                 * For block or character devices, revert to an
                 * anonymous device. For all other files, just kill them.
                 */
                if (flags & FORCECLOSE)
                {
                        panic("Check this");
#if DAMIR
                        if (vp->v_type != VBLK && vp->v_type != VCHR)
                        {
                                vgone(vp);
                        }
                        else
                        {
                                vclean(vp, 0);
                                vp->vnops = &spec_vnodeop;
                                insmntque(vp, NULL);
                        }
#endif
                        continue;
                }
#if DIAGNOSTIC
                if (busyprt)
                        vprint("vflush: busy vnode", vp);
#endif
                busy++;
        }

        if (busy)
        {
                return -EBUSY;
        }

        return (0);
}

#endif
/*
 * Lookup a mount point by filesystem identifier.
 */
mount *mount::getvfs(fsid_t *fsid)
{
        for (mount_t mp : mountlist)
        {
                if (mp->mnt_stat.f_fsid.__val[0] == fsid->__val[0] && mp->mnt_stat.f_fsid.__val[1] == fsid->__val[1])
                        return (mp);
        }

        return nullptr;
}

/*
 * Get a new unique fsid
 */
static u16 xxxfs_mntid = 0;
extern int nblkdev;
void mount::newfsid(int mtype)
{
        fsid_t tfsid;
        mnt_stat.f_fsid.__val[0] = makedev(nblkdev + mtype, 0);
        mnt_stat.f_fsid.__val[1] = mtype;
        if (xxxfs_mntid == 0)
        {
                ++xxxfs_mntid;
        }

        tfsid.__val[0] = makedev(nblkdev + mtype, xxxfs_mntid);
        tfsid.__val[1] = mtype;

        if (mount::mountlist.tqh_first != NULL)
        {
                while (getvfs(&tfsid))
                {
                        tfsid.__val[0]++;
                        xxxfs_mntid++;
                }
        }

        mnt_stat.f_fsid.__val[0] = tfsid.__val[0];
}

void mount::lock_shared() { simple_lock(&s_mnt_lock); }

void mount::unlock_shared() { simple_unlock(&s_mnt_lock); }

void *mount::operator new(size_t object_size)
{
        if (s_mnt_lock.lock_data == 0)
        {
                panic("Allocating mount without lock");
        }

        void *object_ptr = malloc(object_size);

        if (object_ptr == nullptr)
        {
                panic("Could not allocate a mount structure");
        }

        return object_ptr;
}

void mount::operator delete(void *object_ptr)
{
        if (s_mnt_lock.lock_data == 0)
        {
                panic("Freeing mount without lock");
        }

        free(object_ptr);
}