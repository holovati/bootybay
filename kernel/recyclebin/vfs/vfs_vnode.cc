#include <mach/machine/spl.h>
#include <mach/thread.h> // sleep

#include <aux/init.h>
#include <emerixx/driver.h>
#include <emerixx/poll.h>
#include <emerixx/process.h>

#include <etl/algorithm.hh>

#include <fs/specfs.h>

#include <kernel/mach_clock.h>
#include <mach/conf.h>
#include <vfs/mount.h>

#include <string.h>
#include <vfs/vfs_cache.h>
#include <vfs/vnode.h>

#include <fcntl.h>
#include <sys/ioctl.h>

#include <emerixx/fdesc.hh>

static kern_return_t vn_fread(file_t a_so_file, struct uio *a_uio);
static kern_return_t vn_fwrite(file_t a_so_file, struct uio *a_uio);
static kern_return_t vn_flseek(file_t, off_t a_offset, int a_whence);
static kern_return_t vn_fioctl(file_t a_so_file, ioctl_cmd_t a_cmd, void *a_data);
static kern_return_t vn_fclose(file_t a_so_file);
static kern_return_t vn_getdents(file_t, struct uio *);
static kern_return_t vn_fmmap(file_t a_so_file, void *);
static kern_return_t vn_fpoll(file_t a_so_file, struct pollfd **a_pfd);
static kern_return_t vn_fsplice(
    file_t a_so_file, off_t *a_off, file_t a_file_out, off_t *a_off_out, size_t a_len, unsigned a_flags);

static file_ops_data s_vn_fops = {
    //
    .fread     = vn_fread,
    .fwrite    = vn_fwrite,
    .flseek    = vn_flseek,
    .fioctl    = vn_fioctl,
    .fclose    = vn_fclose,
    .fgetdents = vn_getdents,
    .fmmap     = vn_fmmap,
    .fpoll     = vn_fpoll,
    .fsplice   = vn_fsplice
    //
};

#define VNF_OFFSET(f) (*((off_t *)(&(f)->m_private)))

//-------------------------------------------------------------------------------

static size_t s_vnode_age = 1;

// Holds pointer to the root vnode
vnode::ptr vnode::root;

// ********************************************************************************************************************************
vnode::vnode(mount *mp)
    : vm_object(0)
    , m_fstat({})
    , m_dirent({})
    // , v_flag(0)
    //, v_writecount(0)
    //, v_holdcnt(0)
    //, v_lastr(0)
    , m_age(s_vnode_age++)
    , v_mount(mp)
    //, v_numoutput(0)
    , v_un({{}})
//, v_lease(nullptr)
//, v_lastw(0)
//, v_cstart(0)
//, v_lasta(0)
//, v_clen(0)
//, v_ralen(0)
//, v_maxra(0)
//, v_tag(tag)
//, v_cache_state(VC_FREE) // ????
//, v_type(type)
//, m_block_shift(a_block_shift) // Remove
// ********************************************************************************************************************************
{
        // v_freelist_link.init();
        // v_mntvnodes.init();
        // v_cleanblkhd.init();
        // v_dirtyblkhd.init();

        m_observers.init();

        if (s_vnode_age == 0)
        {
                s_vnode_age = 1;
                vfs_cache_flush();
        }
}

// ********************************************************************************************************************************
vnode::~vnode()
// ********************************************************************************************************************************
{
}

// ********************************************************************************************************************************
kern_return_t vnode::open(file *a_file, mode_t a_open_flags, file **a_file_out)
// ********************************************************************************************************************************
{
        KASSERT(islocked());

        kern_return_t retval = fs_open(a_file, a_open_flags);

        return retval;
}

// ********************************************************************************************************************************
kern_return_t vnode::close(file *a_file, int a_fflag)
// ********************************************************************************************************************************
{
        KASSERT(islocked());
        return fs_close(a_file);
}

// ********************************************************************************************************************************
kern_return_t vnode::ioctl(file *a_file, int a_cmd, void *a_data, int a_fflag)
// ********************************************************************************************************************************
{
        KASSERT(islocked());
        return fs_ioctl(a_file, a_cmd, a_data, a_fflag);
}

// ********************************************************************************************************************************
kern_return_t vnode::create(const char *a_name, size_t a_namlen, mode_t a_mode, vnode::locked_ptr &a_vnode_out)
// ********************************************************************************************************************************
{
        KASSERT(islocked());

        if ((a_mode & S_IFMT) == 0)
        {
                a_mode |= S_IFREG;
        }

        KASSERT(((a_namlen == 1) && (a_name[0] == '.')) == false);
        KASSERT(((a_namlen == 2) && (a_name[0] == '.' && a_name[1] == '.')) == false);
        {
                emerixx::dirent_t dent;
                dent.d_ino      = 0;
                dent.d_off      = 0;
                dent.d_reclen   = ROUNDUP(offsetof(emerixx::dirent_t, d_name) + (a_namlen) + 2, sizeof(long));
                dent.d_type     = IFTODT(a_mode & S_IFMT);
                dent.d_name[0]  = 0;
                dent.d_namlen   = a_namlen;
                dent.d_creatino = m_dirent.d_ino;

                strncpy(dent.d_name, a_name, a_namlen);

                v_mount->lock();
                vnode *vn;
                kern_return_t retval = v_mount->vget(&dent, this, &vn);
                vn->reference();
                v_mount->unlock();

                a_vnode_out = vn;
                vn->unreference();

                if (retval != KERN_SUCCESS)
                {
                        panic("not ready for this");
                        a_vnode_out = nullptr;
                        return retval;
                }
        }

        KASSERT(a_vnode_out->m_fstat.st_nlink == 0);

        a_vnode_out->m_fstat.st_mode = a_mode;

        kern_return_t retval = fs_insert_dirent(&a_vnode_out->m_dirent);

        if (retval == KERN_SUCCESS)
        {
                // If we created a dir initialize it
                if (S_ISDIR(a_mode))
                {
                        retval = a_vnode_out->fs_dir_initialize(&m_fstat);

                        // . and us
                        a_vnode_out->m_fstat.st_nlink = 2;

                        // .. in the new dir
                        m_fstat.st_nlink++;
                }
                else
                {
                        // Not a dir, just our reference
                        a_vnode_out->m_fstat.st_nlink = 1;
                }

                mach_get_timespec(&(a_vnode_out->m_fstat.st_ctim));
                a_vnode_out->m_fstat.st_atim = a_vnode_out->m_fstat.st_ctim;
                a_vnode_out->m_fstat.st_mtim = a_vnode_out->m_fstat.st_ctim;

                notify_dentry_created(a_vnode_out, a_name, a_namlen);
        }
        else
        {
                panic("why");
                // Will be collected when refcount drops to zero below
                a_vnode_out->m_fstat.st_nlink = 0;
                a_vnode_out                   = nullptr;
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t vnode::mknod(const char *a_name, size_t a_name_len, dev_t a_dev, mode_t a_mode, vnode::locked_ptr &a_vnode_out)
// ********************************************************************************************************************************
{
        KASSERT(islocked());
        panic("Whoops");
        return KERN_FAIL(ENOSYS);
}

// ********************************************************************************************************************************
kern_return_t vnode::unlink(vnode::locked_ptr &a_vnode, const char *a_name, size_t a_namlen)
// ********************************************************************************************************************************
{
        KASSERT(islocked());
        KASSERT(S_ISDIR(m_fstat.st_mode));

        // No removal of . and ..
        KASSERT(((a_namlen == 1) && (a_name[0] == '.')) == false);
        KASSERT(((a_namlen == 2) && (a_name[0] == '.' && a_name[1] == '.')) == false);

        // Check if the vnode has been found in us
        if ((a_vnode->m_dirent.d_creatino != m_dirent.d_ino) || //
            (a_namlen != a_vnode->m_dirent.d_namlen) ||         //
            (strncmp(a_name, a_vnode->m_dirent.d_name, a_namlen) != 0))
        {
                // Remove the vnode from the cache under the name it has been cached
                notify_dentry_removed(a_vnode, a_vnode->m_dirent.d_name, a_vnode->m_dirent.d_namlen);

                // The vnode is not found in this directory
                // This dir
                //    a_vnode
                //        a_vnode subdir <------- a_vnode found here under the name ".."
                // or hardlink

                fs_readdir_ctx_t rdctx;
                rdctx.name      = a_name;
                rdctx.namelen   = a_namlen;
                rdctx.exit      = false;
                rdctx.fs_offset = 0;
                rdctx.user_data = &a_vnode;

                kern_return_t retval = fs_readdir(&vnode::dirent_update_callback, &rdctx);

                if (retval != KERN_SUCCESS)
                {
                        return retval;
                }

                // Should be found
                KASSERT(rdctx.exit == true);
        }
        else
        {
                // Remove the vnode from the cache under the name it has been cached
                notify_dentry_removed(a_vnode, a_name, a_namlen);
        }

        kern_return_t retval = fs_remove_dirent(&(a_vnode->m_dirent));

        // This assertion is wrong, what if there is a hardlink
        // KASSERT(a_vnode->m_fstat.st_nlink == 0);

        if (retval == KERN_SUCCESS)
        {
                if (S_ISDIR(a_vnode->m_fstat.st_mode))
                {
                        // for ..
                        KASSERT((m_fstat.st_nlink - 1) >= 0);
                        m_fstat.st_nlink--;

                        // for .
                        KASSERT((a_vnode->m_fstat.st_nlink - 1) >= 0);
                        a_vnode->m_fstat.st_nlink--;
                }

                // for our dirent
                KASSERT((a_vnode->m_fstat.st_nlink - 1) >= 0);
                a_vnode->m_fstat.st_nlink--;
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t vnode::link(vnode::locked_ptr &a_vnode, const char *a_name, size_t a_namlen)
// ********************************************************************************************************************************
{
        KASSERT(((a_namlen == 1) && (a_name[0] == '.')) == false);
        KASSERT(((a_namlen == 2) && (a_name[0] == '.' && a_name[1] == '.')) == false);

        KASSERT(islocked());
        KASSERT(S_ISDIR(m_fstat.st_mode));
        KASSERT(a_vnode->v_mount == v_mount);

        if (S_ISDIR(a_vnode->m_fstat.st_mode))
        {
                return KERN_FAIL(EPERM);
        }

        emerixx::dirent_t dent;
        dent.d_ino      = a_vnode->m_dirent.d_ino;
        dent.d_off      = 0;
        dent.d_reclen   = ROUNDUP(offsetof(emerixx::dirent_t, d_name) + (a_namlen) + 2, sizeof(long));
        dent.d_type     = a_vnode->m_dirent.d_type;
        dent.d_name[0]  = 0;
        dent.d_namlen   = a_namlen;
        dent.d_creatino = m_dirent.d_ino;

        strncpy(dent.d_name, a_name, a_namlen);

        kern_return_t retval = fs_insert_dirent(&dent);

        if (retval == KERN_SUCCESS)
        {
                a_vnode->m_fstat.st_nlink++;
                notify_dentry_created(a_vnode, a_name, a_namlen);
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t vnode::rename(vnode::locked_ptr &a_source_vnode,
                            char const *a_source_name,
                            size_t a_source_namlen,
                            vnode::locked_ptr &a_target_parent_vnode,
                            vnode::locked_ptr &a_target_vnode,
                            char const *a_target_name,
                            size_t a_target_namlen)
// ********************************************************************************************************************************
{
        // A source must always be present
        KASSERT(!a_source_vnode.is_null());
        // Directory we are moving the source to must always be present
        KASSERT(!a_target_parent_vnode.is_null());
        // Expect us to be locked
        KASSERT(this->islocked());
        // Expect the target parent to be locked
        KASSERT(a_target_parent_vnode->islocked());
        // Expecting locked source vnode
        KASSERT(a_source_vnode->islocked());

        // Check if the vnode we are renaming has the same mount as it's new parent
        if (a_source_vnode->v_mount != a_target_parent_vnode->v_mount)
        {
                return KERN_FAIL(EXDEV);
        }

        // If we are replacing a vnode check that it has the same mount point as the source
        if (!a_target_vnode.is_null() && a_target_vnode->v_mount != a_source_vnode->v_mount)
        {
                return KERN_FAIL(EXDEV);
        }

        int error = 0;

        // Check if we are the parent and the a_target_parent_vnode
        if (a_target_parent_vnode == this)
        {
                if (!a_target_vnode.is_null())
                {
                        // Expect the target we are replacing to be locked
                        KASSERT(a_target_vnode->islocked());

                        // Remove direntry for a_source_vnode from this directory
                        // Remove direntry for a_target_vnode from this directory
                        // Create new direntry for a_source vnode according to a_target_cn
                        // Free a_target_vnode

                        if (S_ISDIR(a_target_vnode->m_fstat.st_mode))
                        {
                                error = a_target_vnode->fs_dir_empty();

                                if (error)
                                {
                                        return error;
                                }
                        }

                        // error = fs_remove_dirent(a_source_vnode);
                        error = vnode::unlink(a_source_vnode, a_source_name, a_source_namlen);

                        if (error == 0)
                        {
                                // error = fs_remove_dirent(a_target_vnode);
                                error = vnode::unlink(a_target_vnode, a_target_name, a_target_namlen);

                                if (error == 0)
                                {
                                        // Should be correct, fixed in unlink if necessary.
                                        KASSERT(a_source_vnode->m_dirent.d_creatino == m_dirent.d_ino);

                                        strncpy(a_source_vnode->m_dirent.d_name, a_target_name, a_target_namlen);
                                        a_source_vnode->m_dirent.d_name[a_target_namlen] = 0;
                                        a_source_vnode->m_dirent.d_namlen                = a_target_namlen;
                                        a_source_vnode->m_dirent.d_off                   = 0;
                                        a_source_vnode->m_dirent.d_reclen
                                            = ROUNDUP(offsetof(emerixx::dirent_t, d_name) + (a_target_namlen) + 2, sizeof(long));
                                        fs_insert_dirent(&a_source_vnode->m_dirent);

                                        // For us
                                        a_source_vnode->m_fstat.st_nlink++;

                                        if (S_ISDIR(a_source_vnode->m_fstat.st_mode))
                                        {
                                                // for the ..
                                                m_fstat.st_nlink++;

                                                // for the .
                                                a_source_vnode->m_fstat.st_nlink++;
                                        }

                                        notify_dentry_created(a_source_vnode, a_target_name, a_target_namlen);
                                }
                        }
                }
                else
                {
                        // Remove direntry for a_source_vnode from this directory
                        // Create new direntry for a_source vnode according to a_target_cn
                        // error = destroy_direntry(this, source_inode);
                        // error = fs_remove_dirent(a_source_vnode);
                        error = vnode::unlink(a_source_vnode, a_source_name, a_source_namlen);

                        if (error == 0)
                        {
                                // Should be correct, fixed in unlink if necessary.
                                KASSERT(a_source_vnode->m_dirent.d_creatino == m_dirent.d_ino);

                                strncpy(a_source_vnode->m_dirent.d_name, a_target_name, a_target_namlen);
                                a_source_vnode->m_dirent.d_name[a_target_namlen] = 0;
                                a_source_vnode->m_dirent.d_namlen                = a_target_namlen;

                                a_source_vnode->m_dirent.d_off = 0;
                                a_source_vnode->m_dirent.d_reclen
                                    = ROUNDUP(offsetof(emerixx::dirent_t, d_name) + (a_target_namlen) + 2, sizeof(long));
                                fs_insert_dirent(&a_source_vnode->m_dirent);

                                // For us
                                a_source_vnode->m_fstat.st_nlink++;

                                if (S_ISDIR(a_source_vnode->m_fstat.st_mode))
                                {
                                        // for the ..
                                        m_fstat.st_nlink++;

                                        // for the .
                                        a_source_vnode->m_fstat.st_nlink++;
                                }

                                notify_dentry_created(a_source_vnode, a_target_name, a_target_namlen);
                        }
                }
        }
        else
        {
                // ext2_dir_inode *target_parent_dir = a_target_parent_vnode.to_raw<ext2_dir_inode>();
                if (!a_target_vnode.is_null())
                {
                        // Expect the target we are replacing to be locked
                        KASSERT(a_target_vnode->islocked());

                        // inode *target_inode = a_target_vnode.to_raw<inode>();

                        // Remove direntry for a_source_vnode from this directory
                        // Remove direntry for a_target_vnode from a_target_parent_vnode directory
                        // Create new direntry for a_source_vnode inside a_target_parent_vnode directory
                        // Free a_target_vnode

                        if (S_ISDIR(a_target_vnode->m_fstat.st_mode))
                        {
                                error = a_target_vnode->fs_dir_empty();

                                if (error)
                                {
                                        return error;
                                }
                        }

                        // error = fs_remove_dirent(a_source_vnode);
                        error = vnode::unlink(a_source_vnode, a_source_name, a_source_namlen);

                        if (error == 0)
                        {
                                // error = fs_remove_dirent(a_target_vnode);

                                error = a_target_parent_vnode->unlink(a_target_vnode, a_target_name, a_target_namlen);

                                if (error == 0)
                                {
                                        // Should be correct, fixed in unlink if necessary.
                                        KASSERT(a_source_vnode->m_dirent.d_creatino == m_dirent.d_ino);

                                        a_source_vnode->m_dirent.d_creatino = a_target_parent_vnode->m_dirent.d_ino;

                                        strncpy(a_source_vnode->m_dirent.d_name, a_target_name, a_target_namlen);
                                        a_source_vnode->m_dirent.d_name[a_target_namlen] = 0;
                                        a_source_vnode->m_dirent.d_namlen                = a_target_namlen;
                                        a_source_vnode->m_dirent.d_off                   = 0;
                                        a_source_vnode->m_dirent.d_reclen
                                            = ROUNDUP(offsetof(emerixx::dirent_t, d_name) + (a_target_namlen) + 2, sizeof(long));

                                        error = a_target_parent_vnode->fs_insert_dirent(&a_source_vnode->m_dirent);
                                }

                                // For a_target_parent_vnode
                                a_source_vnode->m_fstat.st_nlink++;

                                if (S_ISDIR(a_source_vnode->m_fstat.st_mode))
                                {
                                        error = a_source_vnode->fs_dir_reparent(&a_target_parent_vnode->m_fstat);

                                        // for ..
                                        a_target_parent_vnode->m_fstat.st_nlink++;

                                        // for .
                                        a_source_vnode->m_fstat.st_nlink++;
                                }

                                a_target_parent_vnode->notify_dentry_created(a_source_vnode, a_target_name, a_target_namlen);
                        }
                }
                else
                {
                        // Remove direntry for a_source_vnode from this directory
                        // Create new direntry for a_source_vnode inside a_target_parent_vnode directory

                        // error = fs_remove_dirent(a_source_vnode);
                        error = vnode::unlink(a_source_vnode, a_source_name, a_source_namlen);

                        // error = fs_remove_dirent(a_source_vnode);
                        if (error == 0)
                        {
                                strncpy(a_source_vnode->m_dirent.d_name, a_target_name, a_target_namlen);
                                a_source_vnode->m_dirent.d_name[a_target_namlen] = 0;
                                a_source_vnode->m_dirent.d_namlen                = a_target_namlen;
                                a_source_vnode->m_dirent.d_off                   = 0;
                                a_source_vnode->m_dirent.d_reclen
                                    = ROUNDUP(offsetof(emerixx::dirent_t, d_name) + (a_target_namlen) + 2, sizeof(long));

                                error = a_target_parent_vnode->fs_insert_dirent(&a_source_vnode->m_dirent);

                                // For a_target_parent_vnode
                                a_source_vnode->m_fstat.st_nlink++;

                                if (S_ISDIR(a_source_vnode->m_fstat.st_mode))
                                {
                                        error = a_source_vnode->fs_dir_reparent(&a_target_parent_vnode->m_fstat);

                                        // for ..
                                        a_target_parent_vnode->m_fstat.st_nlink++;

                                        // for .
                                        a_source_vnode->m_fstat.st_nlink++;
                                }

                                a_target_parent_vnode->notify_dentry_created(a_source_vnode, a_target_name, a_target_namlen);
                        }
                }
        }

        return error;
}

// ********************************************************************************************************************************
kern_return_t vnode::fsync(int a_waitfor)
// ********************************************************************************************************************************
{
        KASSERT(islocked());

        for (vm_page_t vmp = vm_page_map_first(&m_page_map); vmp != nullptr; vmp = vm_page_map_next(vmp))
        {
                if (vmp->m_io_header == nullptr)
                {
                        continue;
                }

                for (vm_offset_t blkoff = vmp->offset; blkoff < PAGE_SIZE; blkoff += m_fstat.st_blksize)
                {
                        buffer_cache::io_descriptor *io_desc = buffer_cache::get(this, vmp->offset, m_fstat.st_blksize);

                        KASSERT(io_desc);

                        if ((io_desc->m_flags & IO_DESCRIPTOR_DIRTY) == false)
                        {
                                buffer_cache::release(io_desc);
                                continue;
                        }

                        if (a_waitfor)
                        {
                                buffer_cache::write(io_desc, false);
                        }
                        else
                        {
                                buffer_cache::write_async(io_desc);
                        }
                }
        }
        buffer_cache::page_out(this);

        return KERN_SUCCESS;
}

// virtual int seek(off_t a_oldoff, off_t a_newoff, struct ucred *a_cred) = 0;
#if 0
// ********************************************************************************************************************************
kern_return_t vnode::select(int a_which, int a_fflags, struct ucred *a_cred, pthread_t a_p)
// ********************************************************************************************************************************
{
        KASSERT(islocked());
        return fs_select(a_which, a_fflags);
}
#endif
// ********************************************************************************************************************************
kern_return_t vnode::poll(file *a_file, pollfd **a_pfd)
// ********************************************************************************************************************************
{
        KASSERT(islocked());

        if (S_ISCHR(m_fstat.st_mode))
        {
                return fs_poll(a_file, a_pfd);
        }

        if ((*a_pfd)->revents = (*a_pfd)->events & (POLLIN | POLLRDNORM | POLLOUT | POLLWRNORM))
        {
                (*a_pfd)->revents = (POLLIN | POLLRDNORM | POLLOUT | POLLWRNORM);
        }

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t vnode::truncate(off_t a_length, int a_flags)
// ********************************************************************************************************************************
{
        KASSERT(this->islocked());

        if (a_length < 0)
        {
                return KERN_FAIL(E2BIG);
        }

        u64 new_length    = static_cast<u64>(a_length);
        u64 old_length    = m_fstat.st_size;
        size_t block_size = m_fstat.st_blksize;

        vm_offset_t new_end_blk_offset = ROUNDUP(new_length, block_size);
        vm_offset_t old_end_blk_offset = ROUNDUP(old_length, block_size);

        vm_offset_t zero_bytes_blk_offset = 0;
        vm_offset_t zero_bytes_offset     = 0;

        kern_return_t result = KERN_SUCCESS;
        if (new_length < old_length)
        {
                // Shorten the file
                zero_bytes_blk_offset = new_length & ~(block_size - 1);
                zero_bytes_offset     = new_length & (block_size - 1);

                size_t free_block_count = (old_end_blk_offset - new_end_blk_offset) / block_size;

                if (free_block_count)
                {
                        result = v_mount->block_free(this, new_end_blk_offset, free_block_count);
                }
        }
        else if (new_length > old_length)
        {
                // Lengthen the file
                zero_bytes_blk_offset = old_length & ~(block_size - 1);
                zero_bytes_offset     = old_length & (block_size - 1);
                // size_t new_block_count = (new_end_blk_offset - old_end_blk_offset) / block_size;

                // if (new_block_count)
                //{
                //        result = VFSTOEXT2(v_mount)->block_alloc(this, old_end_blk_offset, new_block_count);
                //}
        }
        else
        {
                return result;
        }

        // else no need to add/remove new blocks

        // Do we need to trim tail of the current i_size
        if (result == KERN_SUCCESS)
        {
                if (zero_bytes_offset)
                {
                        buffer_cache::io_descriptor *io_desc;

                        result = buffer_cache::read(this, zero_bytes_blk_offset, block_size, &io_desc);

                        if (result == KERN_SUCCESS)
                        {
                                char *free_data;
                                size_t block_residue;
                                io_desc->get_data(&free_data, zero_bytes_offset, &block_residue);
                                memset(free_data, 0, block_residue);
                                buffer_cache::write(io_desc);
                        }
                }
        }

        m_fstat.st_size = static_cast<u32>(a_length);

        m_size = round_page(a_length);

        return result;
        // struct timeval tv;
        // get_time
        // return update(&tv, &tv, 0);
}

// ********************************************************************************************************************************
kern_return_t vnode::buffered_uio_move(uio *a_uio)
// ********************************************************************************************************************************
{
        KASSERT(S_ISBLK(m_fstat.st_mode) || S_ISREG(m_fstat.st_mode));

        kern_return_t retval = KERN_SUCCESS;

        buffer_cache::io_descriptor *io_desc;

        while ((a_uio->uio_offset < m_fstat.st_size) && a_uio->uio_resid)
        {
                size_t block_residue;
                char *data;
                retval = vnode::read_fs_type(a_uio->uio_offset, &data, &io_desc, &block_residue);

                if (retval != KERN_SUCCESS)
                {
                        break;
                }

                size_t move_size = etl::min(a_uio->uio_resid, block_residue);

                move_size = etl::min(move_size, static_cast<size_t>(m_fstat.st_size - a_uio->uio_offset));

                retval = uiomove(data, move_size, a_uio);

                if (a_uio->uio_rw == UIO_READ)
                {
                        buffer_cache::release(io_desc);
                }
                else
                {
                        buffer_cache::write(io_desc);
                }
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t vnode::uio_move(uio *a_uio, int a_ioflag)
// ********************************************************************************************************************************
{
        KASSERT(this->islocked());
        kern_return_t retval = KERN_SUCCESS;

        switch (m_fstat.st_mode & S_IFMT)
        {
        case S_IFREG: {
                size_t iosize = static_cast<size_t>(a_uio->uio_offset + a_uio->uio_resid);

                if ((a_uio->uio_rw == UIO_WRITE) && (iosize > m_fstat.st_size))
                {
                        retval = vnode::truncate(iosize, a_ioflag);
                }

                if (retval == KERN_SUCCESS)
                {
                        retval = vnode::buffered_uio_move(a_uio);
                }

                // Set the flags in m_fstat;
                // i_flag |= IN_ACCESS;
                break;
        }

        case S_IFDIR: {
                retval = KERN_FAIL(EISDIR);
                break;
        }

        case S_IFBLK:
        case S_IFCHR: {
                retval = fs_readwrite_spec(nullptr, a_uio, a_ioflag);
                break;
        }

        case S_IFLNK: {
                retval = fs_readwrite_link(a_uio);
                break;
        }

        case S_IFIFO:
        case S_IFSOCK: {
                // Implement
                retval = KERN_FAIL(ENXIO);
                break;
        }

        default: {
                retval = KERN_FAIL(EINVAL);
                // Caller did not do propper userspace checking or some other bug
                panic("Unknown vnode type");
        }
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t vnode::uio_move(file *a_file, uio *a_uio, int a_ioflag)
// ********************************************************************************************************************************
{
        KASSERT(this->islocked());
        kern_return_t retval = KERN_SUCCESS;

        switch (m_fstat.st_mode & S_IFMT)
        {
        case S_IFREG: {
                size_t iosize = static_cast<size_t>(a_uio->uio_offset + a_uio->uio_resid);

                if ((a_uio->uio_rw == UIO_WRITE) && (iosize > m_fstat.st_size))
                {
                        retval = vnode::truncate(iosize, a_ioflag);
                }

                if (retval == KERN_SUCCESS)
                {
                        retval = vnode::buffered_uio_move(a_uio);
                }

                // Set the flags in m_fstat;
                // i_flag |= IN_ACCESS;
                break;
        }

        case S_IFDIR: {
                retval = KERN_FAIL(EISDIR);
                break;
        }

        case S_IFBLK:
        case S_IFCHR: {
                retval = fs_readwrite_spec(a_file, a_uio, a_ioflag);
                break;
        }

        case S_IFLNK: {
                retval = fs_readwrite_link(a_uio);
                break;
        }

        case S_IFIFO:
        case S_IFSOCK: {
                // Implement
                retval = KERN_FAIL(ENXIO);
                break;
        }

        default: {
                retval = KERN_FAIL(EINVAL);
                // Caller did not do propper userspace checking or some other bug
                panic("Unknown vnode type");
        }
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t vnode::stat(fstat_t *a_fstat)
// ********************************************************************************************************************************
{
        KASSERT(islocked());
        kern_return_t retval;

        *a_fstat = m_fstat;

        if (S_ISBLK(m_fstat.st_mode) || S_ISCHR(m_fstat.st_mode))
        {
                // Linux does not show thease
                a_fstat->st_size   = 0;
                a_fstat->st_blocks = 0;
        }

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t vnode::chown(uid_t a_uid, gid_t a_gid)
// ********************************************************************************************************************************
{
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t vnode::chmod(mode_t a_mode)
// ********************************************************************************************************************************
{
        KASSERT(this->islocked());

        m_fstat.st_mode = (m_fstat.st_mode & S_IFMT) | a_mode;

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t vnode::access(mode_t a_mode)
// ********************************************************************************************************************************
{
        KASSERT(this->islocked());

        // root always gets access.
        if (/*a_cred == nullptr || a_cred->cr_uid == 0*/ 1)
        {
                return KERN_SUCCESS;
        }

        panic("Not ready for %08x", a_mode);

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t vnode::utimens(timespec *a_utimens, int a_flags)
// ********************************************************************************************************************************
{
        KASSERT(this->islocked());
        m_fstat.st_atim = a_utimens[0];
        m_fstat.st_mtim = a_utimens[1];
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t vnode::lookup(const char *a_name, size_t a_namelen, vnode::locked_ptr &a_vnode_out)
// ********************************************************************************************************************************
{
        KASSERT(this->islocked());

        fs_readdir_ctx_t rdctx;
        rdctx.name      = a_name;
        rdctx.namelen   = a_namelen;
        rdctx.exit      = false;
        rdctx.fs_offset = 0;
        rdctx.user_data = &a_vnode_out;

        kern_return_t retval = fs_readdir(&vnode::lookup_callback, &rdctx);

        if ((retval == KERN_SUCCESS) && (a_vnode_out.is_null() == true))
        {
                if (rdctx.exit == false)
                {
                        retval = KERN_FAIL(ENOENT);
                }
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t vnode::getdents(uio *a_uio)
// ********************************************************************************************************************************
{
        if (S_ISDIR(m_fstat.st_mode) == false)
        {
                return KERN_FAIL(ENOTDIR);
        }

        if (a_uio->uio_rw != UIO_READ)
        {
                return KERN_FAIL(EINVAL);
        }

        if (a_uio->uio_offset >= m_fstat.st_size)
        {
                return KERN_SUCCESS;
        }

        fs_readdir_ctx_t rdctx;
        rdctx.name      = nullptr;
        rdctx.namelen   = 0;
        rdctx.exit      = false;
        rdctx.fs_offset = a_uio->uio_offset;
        rdctx.user_data = a_uio;

        kern_return_t retval = fs_readdir(&vnode::readdir_callback, &rdctx);

        a_uio->uio_offset = rdctx.fs_offset;

        return retval;
}

// ********************************************************************************************************************************
kern_return_t vnode::strategy(buffer_cache::io_descriptor *a_iodesc)
// ********************************************************************************************************************************
{
        KASSERT(this->islocked());

        kern_return_t retval = KERN_SUCCESS;

        retval = fs_strategy(a_iodesc);

        return retval;
}

// ********************************************************************************************************************************
vnode::ptr vnode::get_from_file(file *a_file)
// ********************************************************************************************************************************
{
        vnode::ptr vp;
        if (a_file->m_fops == &s_vn_fops)
        {
                vp = a_file->m_vnode;
        }
        return vp;
}

// ********************************************************************************************************************************
kern_return_t vnode::alloc_file(integer_t a_oflags)
// ********************************************************************************************************************************
{
        if (S_ISCHR(m_fstat.st_mode))
        {
                if (v_un.vu_private == nullptr)
                {
                        kern_return_t retval = driver_chrdev_lookup(m_fstat.st_rdev, (chrdev_t *)&v_un.vu_private);
                        if (KERN_STATUS_FAILURE(retval))
                        {
                                return retval;
                        }
                }

                chrdev_t cdev = (chrdev_t)v_un.vu_private;

                file_t f;
                int fd = fd_context_fdalloc(uthread_self()->m_fdctx, a_oflags, cdev->m_fops, this, 0, &f);

                if (KERN_STATUS_FAILURE(fd))
                {
                        return fd;
                }

                KASSERT(KERN_STATUS_SUCCESS(f->open()));

                return fd;
        }
        else if (S_ISBLK(m_fstat.st_mode))
        {
                return specfs_file_alloc(m_fstat.st_rdev, m_fstat.st_mode, a_oflags);
        }
        else
        {
                file_t f;
                int fd = fd_context_fdalloc(uthread_self()->m_fdctx, a_oflags, &s_vn_fops, this, 0, &f);

                if (KERN_STATUS_FAILURE(fd))
                {
                        return fd;
                }

                // Vnode already locked
                kern_return_t retval = open(f, a_oflags);

                if (KERN_STATUS_FAILURE(retval))
                {
                        fd_context_fdclose(uthread_self()->m_fdctx, fd);
                }
                else
                {
                        retval = fd;
                }

                return retval;
        }
}

// ********************************************************************************************************************************
void vnode::do_unreference()
// ********************************************************************************************************************************
{
        KASSERT(islocked());

        if (m_fstat.st_nlink == 0)
        {
                // We are a goner, free all blocks
                vnode::truncate(0, 0);
        }

        // Writeout the data associated with the vnode
        buffer_cache::page_out(this);

        mount *m = v_mount;

        if ((m->mnt_flag & MNT_FLUSHING_VFS_CACHE) == false)
        {
                m->lock();
                m->vnode_reclaim(this);
                m->unlock();
        }
        else
        {
                m->vnode_reclaim(this);
        }
        // This context is dead from now on
}

// ********************************************************************************************************************************
bool vnode::do_extend(size_t)
// ********************************************************************************************************************************
{
        KASSERT(islocked());
        // vnodes can only be extended by truncate
        return false;
}

// ********************************************************************************************************************************
kern_return_t vnode::readdir_callback(emerixx::dirent_t *a_dirent, fs_readdir_ctx_t *a_ctx)
// ********************************************************************************************************************************
{
        kern_return_t retval = KERN_SUCCESS;
        uio *dest            = reinterpret_cast<uio *>(a_ctx->user_data);

        if (a_dirent->d_reclen <= dest->uio_resid)
        {
                retval = uiomove(a_dirent, a_dirent->d_reclen, dest);
        }
        else
        {
                a_ctx->exit = true;
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t vnode::lookup_callback(emerixx::dirent_t *a_dirent, fs_readdir_ctx_t *a_ctx)
// ********************************************************************************************************************************
{
        kern_return_t retval = KERN_SUCCESS;

        if ((a_ctx->namelen == a_dirent->d_namlen) && (strncmp(a_ctx->name, a_dirent->d_name, a_ctx->namelen) == 0))
        {
                a_ctx->exit = true;

                vnode::locked_ptr *tmp = reinterpret_cast<vnode::locked_ptr *>(a_ctx->user_data);

                // If we are direntry points to this inode we succeed without returning a child
                if (a_dirent->d_ino != m_dirent.d_ino) [[likeley]]
                {
                        v_mount->lock();
                        vnode *vn;
                        retval = v_mount->vget(a_dirent, this, &vn);

                        if (retval == KERN_SUCCESS)
                        {
                                vn->reference();
                        }

                        v_mount->unlock();

                        if (retval == KERN_SUCCESS)
                        {
                                retval = vn->try_lock();
                                if (retval == KERN_SUCCESS)
                                {
                                        vn->unlock();
                                        (*tmp) = vn;
                                }

                                // Loose the temp ref gained gained in mount locked scope
                                vn->unreference();
                        }
                }

                if ((retval == KERN_SUCCESS) && ((*tmp).is_null() == false))
                {
                        if (a_dirent->d_namlen == 2 && (a_dirent->d_name[0] == '.') && (a_dirent->d_name[1] == '.'))
                        {
                                // .. always requires a lookup
                                (*tmp)->m_dirent.d_creatino = 0;
                        }
                        else
                        {
                                (*tmp)->m_dirent            = *a_dirent;
                                (*tmp)->m_dirent.d_creatino = m_dirent.d_ino;
                        }
                }
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t vnode::dirent_update_callback(emerixx::dirent_t *a_dirent, fs_readdir_ctx_t *a_ctx)
// ********************************************************************************************************************************
{
        kern_return_t retval = KERN_SUCCESS;

        // No removal of .
        if ((a_ctx->namelen == 1) && (a_ctx->name[0] == '.'))
        {
                panic("Should never happen");
        }

        // No removal of ..
        if ((a_ctx->namelen == 2) && (a_ctx->name[0] == '.' && a_ctx->name[1] == '.'))
        {
                panic("Should never happen");
        }

        if ((a_ctx->namelen == a_dirent->d_namlen) && (strncmp(a_ctx->name, a_dirent->d_name, a_ctx->namelen) == 0))
        {
                vnode::locked_ptr *tmp = reinterpret_cast<vnode::locked_ptr *>(a_ctx->user_data);
                if ((*tmp)->m_dirent.d_ino == a_dirent->d_ino)
                {
                        (*tmp)->m_dirent            = *a_dirent;
                        (*tmp)->m_dirent.d_creatino = m_dirent.d_ino;
                        a_ctx->exit                 = true;
                }
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t vnode::get_page(size_t a_offset, vm_prot_t a_prot, vm_page_t *a_vm_page_out)
// ********************************************************************************************************************************
{
        KASSERT(this->islocked());
        KASSERT(is_memory_mappable());

        kern_return_t retval;
        if (S_ISCHR(m_fstat.st_mode) == false)
        {

                size_t read_size = etl::min(static_cast<size_t>(PAGE_SIZE),
                                            static_cast<size_t>(ROUNDUP(m_fstat.st_size, m_fstat.st_blksize) - a_offset));

                buffer_cache::io_descriptor *iodesc;

                retval = buffer_cache::read_page(this, a_offset, m_fstat.st_blksize, read_size, &iodesc);

                if (retval == KERN_SUCCESS)
                {
                        *a_vm_page_out = iodesc->get_vm_page();

                        buffer_cache::release(iodesc);
                }
        }
        else
        {
                KASSERT(m_size >= PAGE_SIZE);

                // Check if we have the page at the given offset
                *a_vm_page_out = vm_page_map_lookup(&m_page_map, a_offset);

                // If we do, return it.
                if (*a_vm_page_out)
                {
                        retval = KERN_SUCCESS;
                }
                else
                {
                        retval = fs_get_page(a_offset, a_prot, a_vm_page_out);
                }
        }

        return retval;
}

/*
 * Package up an I/O request on a vnode into a uio and do it.
 */
// ********************************************************************************************************************************
int vnode::rdwr_uio(enum uio_rw rw, char *base, int len, off_t offset, enum uio_seg segflg, int ioflg, int *aresid)
// ********************************************************************************************************************************
{
        KASSERT((ioflg & IO_NODELOCKED));
        struct uio auio;
        struct iovec aiov;
        int error;

        /* Keep the memory object cache in sync with the buffer cache */
        if (S_ISREG(m_fstat.st_mode))
        {
#if DAMIR
                if (rw == UIO_READ)
                        vn_cache_state_buf_read(vp);
                else
                        vn_cache_state_buf_write(vp);
#endif
        }

        auio.uio_iov    = &aiov;
        auio.uio_iovcnt = 1;
        aiov.iov_base   = base;
        aiov.iov_len    = len;
        auio.uio_resid  = len;
        auio.uio_offset = offset;
        auio.uio_segflg = segflg;
        auio.uio_rw     = rw;

        /*
        if (rw == UIO_READ)
        {
                error = this->read(&auio, ioflg);
        }
        else
        {
                error = this->write(&auio, ioflg);
        }
*/
        error = this->uio_move(&auio, ioflg);

        if (aresid)
        {
                *aresid = auio.uio_resid;
        }
        else if (auio.uio_resid && error == 0)
        {
                error = -EIO;
        }

        return error;
}

// ********************************************************************************************************************************
kern_return_t vn_fread(file_t a_file, struct uio *a_uio)
// ********************************************************************************************************************************
{
        vnode::locked_ptr vp{a_file->m_vnode};

        a_uio->uio_offset    = VNF_OFFSET(a_file);
        int count            = a_uio->uio_resid;
        kern_return_t retval = vp->uio_move(a_file, a_uio, (a_file->m_oflags & O_NONBLOCK) ? IO_NDELAY : 0);
        VNF_OFFSET(a_file)   = VNF_OFFSET(a_file) + (count - a_uio->uio_resid);

        return retval;
}

// ********************************************************************************************************************************
kern_return_t vn_fwrite(file_t a_file, struct uio *a_uio)
// ********************************************************************************************************************************
{
        vnode::locked_ptr vp{a_file->m_vnode};

        int ioflag = 0;

        if (S_ISREG(a_file->m_vnode->m_fstat.st_mode) && (a_file->m_oflags & O_APPEND))
        {
                VNF_OFFSET(a_file) = a_file->m_vnode->m_fstat.st_size;
                ioflag |= IO_APPEND;
        }

        if (a_file->m_oflags & O_NONBLOCK)
        {
                ioflag |= IO_NDELAY;
        }

        a_uio->uio_offset    = VNF_OFFSET(a_file);
        int count            = a_uio->uio_resid;
        kern_return_t retval = vp->uio_move(a_file, a_uio, ioflag);

        if (ioflag & IO_APPEND)
        {
                VNF_OFFSET(a_file) = a_uio->uio_offset;
        }
        else
        {
                VNF_OFFSET(a_file) += count - a_uio->uio_resid;
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t vn_flseek(file_t a_file, off_t a_offset, int a_whence)
// ********************************************************************************************************************************
{
        vnode::locked_ptr vp{a_file->m_vnode};

        switch (vp->m_fstat.st_mode & S_IFMT)
        {
        case S_IFREG:
        case S_IFBLK:
                break;

        case S_IFDIR:
        case S_IFCHR:
        case S_IFIFO:
        case S_IFSOCK:
        default:
                return KERN_FAIL(ESPIPE);
        }

        switch (a_whence)
        {
        case SEEK_CUR:
                VNF_OFFSET(a_file) += a_offset;
                break;
        case SEEK_END:
                VNF_OFFSET(a_file) = a_offset + vp->m_fstat.st_size;
                break;
        case SEEK_SET:
                VNF_OFFSET(a_file) = a_offset;
                break;
        default:
                return KERN_FAIL(EINVAL);
        }

        return (kern_return_t)VNF_OFFSET(a_file);
}

// ********************************************************************************************************************************
kern_return_t vn_fioctl(file_t a_file, ioctl_cmd_t a_cmd, void *a_data)
// ********************************************************************************************************************************
{
        vnode::locked_ptr vp{a_file->m_vnode};

        switch (vp->m_fstat.st_mode & S_IFMT)
        {

        case S_IFIFO:
        case S_IFCHR:
        case S_IFBLK: {
                kern_return_t retval = vp->ioctl(a_file, a_cmd, a_data, a_file->m_oflags);
                if (KERN_STATUS_SUCCESS(retval) && a_cmd == TIOCSCTTY)
                {
                        // is set by the tty layer
                        // panic("picnic");
                        // p->process->p_pgrp->pg_session->s_ttyvp = vp.get_raw();
                        // vp->reference();
                }
                return retval;
        }
        case S_IFREG:
        case S_IFDIR:
                if (a_cmd == FIONREAD)
                {
                        struct stat statbuf;
                        statbuf = {};

                        kern_return_t retval = vp->stat(&statbuf);

                        if (KERN_STATUS_FAILURE(retval))
                        {
                                return retval;
                        }

                        *(int *)a_data = statbuf.st_size - VNF_OFFSET(a_file);

                        return retval;
                }

                if (a_cmd == FIONBIO || a_cmd == FIOASYNC)
                {                            /* XXX */
                        return KERN_SUCCESS; /* XXX */
                                             /* fall into ... */
                }
                return KERN_FAIL(ENOTTY);

        default:
                return KERN_FAIL(ENOTTY);
        }

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t vn_fclose(file_t a_file)
// ********************************************************************************************************************************
{
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t vn_getdents(file_t a_file, struct uio *a_uio)
// ********************************************************************************************************************************
{
        vnode::locked_ptr vp{a_file->m_vnode};

        if (S_ISDIR(vp->m_fstat.st_mode) == false)
        {
                return KERN_FAIL(ENOTDIR);
        }

        a_uio->uio_offset = VNF_OFFSET(a_file);

        kern_return_t retval = vp->getdents(a_uio);

        VNF_OFFSET(a_file) = a_uio->uio_offset;

        return retval;
}

// ********************************************************************************************************************************
kern_return_t vn_fmmap(file_t a_file, void *)
// ********************************************************************************************************************************
{
        panic("noop");
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t vn_fpoll(file_t a_file, struct pollfd **a_pfd)
// ********************************************************************************************************************************
{
        kern_return_t retval = vnode::locked_ptr(a_file->m_vnode)->poll(a_file, a_pfd);
        return retval;
}

// ********************************************************************************************************************************
kern_return_t vn_fsplice(file_t a_so_file, off_t *a_off, file_t a_file_out, off_t *a_off_out, size_t a_len, unsigned a_flags)
// ********************************************************************************************************************************
{
        panic("noop");
        return KERN_SUCCESS;
}
