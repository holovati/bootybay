#pragma once
#include <sys/statfs.h>
#include <vfs/vfs_cache.h>
#include <vfs/vnode.h>

/*
 * Mount flags.
 *
 * Unmount uses MNT_FORCE flag.
 */
#define MNT_RDONLY      0x00000001 /* read only filesystem */
#define MNT_SYNCHRONOUS 0x00000002 /* file system written synchronously */
#define MNT_NOEXEC      0x00000004 /* can't exec from filesystem */
#define MNT_NOSUID      0x00000008 /* don't honor setuid bits on fs */
#define MNT_NODEV       0x00000010 /* don't interpret special files */
#define MNT_UNION       0x00000020 /* union with underlying filesystem */
#define MNT_ASYNC       0x00000040 /* file system written asynchronously */

/*
 * exported mount flags.
 */
#define MNT_EXRDONLY    0x00000080 /* exported read only */
#define MNT_EXPORTED    0x00000100 /* file system is exported */
#define MNT_DEFEXPORTED 0x00000200 /* exported to the world */
#define MNT_EXPORTANON  0x00000400 /* use anon uid mapping for everyone */
#define MNT_EXKERB      0x00000800 /* exported with Kerberos uid mapping */

/*
 * Flags set by internal operations.
 */
#define MNT_LOCAL  0x00001000 /* filesystem is stored locally */
#define MNT_QUOTA  0x00002000 /* quotas are enabled on filesystem */
#define MNT_ROOTFS 0x00004000 /* identifies the root filesystem */

/*
 * filesystem control flags.
 *
 * MNT_MLOCK lock the mount entry so that name lookup cannot proceed
 * past the mount point.  This keeps the subtree stable during mounts
 * and unmounts.
 */

#define MNT_USER               0x00008000 /* mounted by a user */
#define MNT_UPDATE             0x00010000 /* not a real mount, just an update */
#define MNT_DELEXPORT          0x00020000 /* delete export host lists */
#define MNT_RELOAD             0x00040000 /* reload filesystem data */
#define MNT_FORCE              0x00080000 /* force unmount or readonly change */
#define MNT_MLOCK              0x00100000 /* lock so that subtree is stable */
#define MNT_MWAIT              0x00200000 /* someone is waiting for lock */
#define MNT_MPBUSY             0x00400000 /* scan of mount point in progress */
#define MNT_MPWANT             0x00800000 /* waiting for mount point */
#define MNT_UNMOUNT            0x01000000 /* unmount in progress */
#define MNT_WANTRDWR           0x02000000 /* want upgrade to read/write */
#define MNT_FLUSHING_VFS_CACHE 0x04000000 /* The mount ran out of vnodes and is performing a vfs_cache_flush */

/*
 * Flags for various system call interfaces.
 *
 * waitfor flags to vfs_sync() and getfsstat()
 */
#define MNT_WAIT   1
#define MNT_NOWAIT 2

#define NMOUNT 3 /* number of mountable file systems */

/*
 * file system statistics
 */

#define MFSNAMELEN 16 /* length of fs type name, including nul */
#define MNAMELEN   90 /* length of buffer for returned name */

/*
 * File system types.
 */
#define MOUNT_NONE    0
#define MOUNT_UFS     1  /* Fast Filesystem */
#define MOUNT_NFS     2  /* Sun-compatible Network Filesystem */
#define MOUNT_MFS     3  /* Memory-based Filesystem */
#define MOUNT_MSDOS   4  /* MS/DOS Filesystem */
#define MOUNT_LFS     5  /* Log-based Filesystem */
#define MOUNT_LOFS    6  /* Loopback Filesystem */
#define MOUNT_FDESC   7  /* File Descriptor Filesystem */
#define MOUNT_PORTAL  8  /* Portal Filesystem */
#define MOUNT_NULL    9  /* Minimal Filesystem Layer */
#define MOUNT_UMAP    10 /* User/Group Identifer Remapping Filesystem */
#define MOUNT_KERNFS  11 /* Kernel Information Filesystem */
#define MOUNT_PROCFS  12 /* /proc Filesystem */
#define MOUNT_SPECFS  13 /* /dev Filesystem */
#define MOUNT_CD9660  14 /* ISO9660 (aka CDROM) Filesystem */
#define MOUNT_UNION   15 /* Union (translucent) Filesystem */
#define MOUNT_EXT2FS  16 /* Linux ext2 Filesystem */
#define MOUNT_MINIXFS 17 /* Linux minix Filesystem */
#define MOUNT_MAXTYPE 17

/*
 * Operations supported on mounted file system.
 */
struct fid;
struct mount;

/*
 * Mount structure.
 * One allocated on every mount.
 * Used to find the super block.
 */
/*
 * Structure per mounted file system.  Each mounted file system has an
 * array of operations and an instance record.  The file systems are
 * put on a doubly linked list.
 */
struct mount
{
        using tailq_link = intrusive::tailq::link<mount>;

        tailq_link mnt_list_link; /* mount list */
        using mnt_list = tailq_link::head<&mount::mnt_list_link>;

        dev_t m_dev;                 /* device mounted */
        vnode::ptr mnt_vnodecovered; /* vnode we mounted on */
        char const *fs_name;
        integer_t mnt_flag;
        size_t mnt_maxsymlinklen;
        integer_t m_vn_active;
        struct statfs mnt_stat;
        vnode::mntvnodes_list mnt_vnodelist; /* list of vnodes this mount */

        mount(dev_t dev, vnode::ptr &vncovered, const char *fs_name, integer_t flags);
        virtual ~mount();

        /*
         * Lock a filesystem.
         * Used to prevent access to it while mounting and unmounting.
         */
        int lock();

        /*
         * Unlock a locked filesystem.
         * Panic if filesystem is not locked.
         */
        void unlock();

        /*
         * Mark a mount point as busy.
         * Used to synchronize access and to delay unmounting.
         */
        int busy();

        /*
         * Free a busy filesystem.
         * Panic if filesystem is not busy.
         */
        void unbusy();

        /*
         * Remove any vnodes in the vnode table belonging to mount point mp.
         *
         * If MNT_NOFORCE is specified, there should not be any active ones,
         * return error if any are found (nb: this is a user error, not a
         * system error). If MNT_FORCE is specified, detach any active vnodes
         * that are found.
         */
        int vflush(struct vnode *skipvp, int flags);

        /*
         * Get a new unique fsid
         */
        void newfsid(int mtype);

        /*
         * Lookup a mount point by filesystem identifier.
         */
        mount *getvfs(fsid_t *fsid);

        virtual int start(int flags, pthread_t p) = 0;

        virtual int unmount(int mntflags, pthread_t p) = 0;

        virtual int root(vnode::ptr &vpp) = 0;

        virtual int quotactl(int cmds, uid_t uid, char *arg, pthread_t p) = 0;

        virtual int statfs(struct statfs *sbp) = 0;

        virtual int sync(int waitfor) = 0;

        virtual int vget(emerixx::dirent_t *a_dirent, vnode *a_parent, vnode **a_vnode_out) = 0;

        virtual kern_return_t block_free(vnode *a_vnode, vm_offset_t a_offset, size_t a_count) = 0;

        virtual void vnode_reclaim(vnode *a_vnode) = 0;

        template <typename T, typename... Args> int new_vnode(T **a_vnode_out, Args... args)
        {
                kern_return_t retval = KERN_SUCCESS;

                KASSERT(m_vn_active >= 0);

                if (vnode::storage<T>::mem.is_exhausted())
                {
                        mnt_flag |= MNT_FLUSHING_VFS_CACHE;
                        vfs_cache_flush();
                        mnt_flag &= ~MNT_FLUSHING_VFS_CACHE;

                        // panic("What do we do now");
                        // If we unlock and sleep waiting for memory the node might already be loaded by some other thread
                        // We will address this when the mount code get the same overhaul as the vnode.
                }

                // perform checking from getnewvnode and return a vnode if possible
                *a_vnode_out = vnode::storage<T>::mem.alloc_new(args...);

                KASSERT((*a_vnode_out) != nullptr);
                mnt_vnodelist.insert_head(*a_vnode_out);
                m_vn_active++;
                return retval;
        }

        template <typename T> void delete_vnode(T *vp)
        {
                KASSERT(m_vn_active > 0);

                mnt_vnodelist.remove(vp);
                vnode::storage<T>::mem.free_delete(vp);
                m_vn_active--;
        }

        template <typename T> void delete_vnode(vnode *vp) { delete_vnode<T>(static_cast<T *>(vp)); }

        /* List of all mounted file systems */
        static mnt_list mountlist;

        static void lock_shared();
        static void unlock_shared();

        static void *operator new(size_t sz);
        static void operator delete(void *p);
};

typedef struct mount *mount_t;