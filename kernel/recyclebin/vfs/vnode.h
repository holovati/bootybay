/*
 * Mach Operating System
 * Copyright (c) 1994 Johannes Helander
 * All Rights Reserved.
 *
 * Permission to use, copy, modify and distribute this software and its
 * documentation is hereby granted, provided that both the copyright
 * notice and this permission notice appear in all copies of the
 * software, derivative works or modified versions, and any portions
 * thereof, and that both notices appear in supporting documentation.
 *
 * JOHANNES HELANDER ALLOWS FREE USE OF THIS SOFTWARE IN ITS "AS IS"
 * CONDITION.  JOHANNES HELANDER DISCLAIMS ANY LIABILITY OF ANY KIND
 * FOR ANY DAMAGES WHATSOEVER RESULTING FROM THE USE OF THIS SOFTWARE.
 */
/*
 * HISTORY
 * $Log: vnode.h,v $
 * Revision 1.1.1.1  1995/03/02  21:49:36  mike
 * Initial Lites release from hut.fi
 *
 */
/*
 *	File:	include/sys/vnode.h
 *	Origin:	Adapted to Lites from 4.4 BSD Lite.
 */
/*
 * Copyright (c) 1989, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)vnode.h	8.7 (Berkeley) 2/4/94
 */

#pragma once

#ifdef KERNEL
// #include "nfs.h"

#endif
#include <kernel/zalloc.h>
#include <mach/uio.h>
#include <sys/stat.h>
#ifdef __cplusplus
#include <dirent.h>
#include <kernel/vm/vm_object.h>
#include <mach/buf.h>
#include <mach/intrusive/list.hh>
#include <mach/intrusive/tailq.hh>

#ifndef IFTODT
#define IFTODT(mode) (((mode)&0170000) >> 12)
#endif

#ifndef DTTOIF
#define DTTOIF(dirtype) ((dirtype) << 12)
#endif

#define DENT_RECLEN(nm_len) (ROUNDUP(offsetof(emerixx::dirent_t, d_name) + (nm_len) + 2, sizeof(long)))

namespace emerixx
{

struct dirent_t : ::dirent
{
        size_t d_namlen;
        ino_t d_creatino;
};

} // namespace emerixx
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#pragma GCC diagnostic ignored "-Wpadded"
#pragma GCC diagnostic pop
#endif

struct pollfd;
struct vnode;
typedef struct stat fstat_t;
typedef struct file *file_t;

/*
 * The vnode is the focus of all file activity in UNIX.  There is a
 * unique vnode allocated for each active file, each current directory,
 * each mounted-on file, text file, and the root.
 */

/*
 * Vnode tag types.
 * These are for the benefit of external programs only (e.g., pstat)
 * and should NEVER be inspected by the kernel.
 */
enum vtagtype
{
        VT_NON,
        VT_PROC,
        VT_EXT2,
        VT_NFS,
        VT_MFS,
        VT_PC,
        VT_LFS,
        VT_LOFS,
        VT_FDESC,
        VT_PORTAL,
        VT_NULL,
        VT_UMAP,
        VT_KERNFS,
        VT_PROCFS,
        VT_AFS,
        VT_ISOFS,
        VT_UNION
};

/*
 * Consistency between object and buffer cache.
 *
 * VC_FREE	  : No read or write rights. Object cache is empty or void.
 * VC_READ 	  : Read access granted for both caches.
 * VC_MO_WRITE	  : Write (and read) access granted to mapped I/O,
 *		    buffer cache has no access.
 * VC_BUF_WRITE	  : Write (and read) access granted to rpc I/O,
 *		    mapped I/O has no access.
 * VC_MO_CLEANING : Waiting for MO cache to be cleaned by the kernel
 *		    in transition VC_MO_WRITE -> VC_CLEANING -> VC_READ.
 * VC_MO_FLUSHING : Waiting for MO cache to be flushed by the kernel
 *		    in transition VC_READ -> VC_MO_FLUSHING -> VC_FREE.
 *
 */
enum vcache_state
{
        VC_FREE,
        VC_READ,
        VC_MO_WRITE,
        VC_BUF_WRITE,
        VC_MO_CLEANING,
        VC_MO_FLUSHING
};
#ifdef __cplusplus

/*
 * Each underlying filesystem allocates its own private area and hangs
 * it from v_data.  If non-null, this area is freed in getnewvnode().
 */
struct vnode : vm_object
{
        using tailq_link = intrusive::tailq::link<vnode>;
        using list_link  = intrusive::list::link<vnode>;

        template <typename T, size_t MAX_ELEMENTS = 512> struct storage
        {
                using type = typename zone<T, MAX_ELEMENTS, 0, ZONE_FIXED>::type;

                static type mem;

                static void init() { type::init(&mem, "vnode"); }
        };

        struct locked_ptr;

        struct ptr
        {
                ptr()
                    : m_vnode(nullptr)
                {
                }

                ptr &operator=(ptr &&) = delete;

                ptr(ptr &other)
                    : ptr()
                {
                        operator=(other);
                }

                ptr(vnode *a_raw_vnode)
                    : ptr()
                {
                        operator=(a_raw_vnode);
                }

                ptr(ptr &&) = delete;

                inline ptr(locked_ptr &&a_locked_ptr);

                ptr &operator=(const ptr &other) { return operator=(other.m_vnode); }

                ptr &operator=(vnode *a_raw_vnode)
                {
                        if (m_vnode)
                        {
                                m_vnode->unreference();
                                m_vnode = nullptr;
                        }

                        if (a_raw_vnode)
                        {
                                a_raw_vnode->reference();
                                m_vnode = a_raw_vnode;
                        }

                        return *this;
                }

                inline ptr &operator=(locked_ptr &a_locked_ptr);

                ~ptr()
                {
                        if (m_vnode)
                        {
                                m_vnode->unreference();
                        }
                }

                vnode *operator->() { return m_vnode; }

                vnode *get_raw() { return m_vnode; }

                bool is_null() { return m_vnode == nullptr; }

                inline bool operator==(locked_ptr &a_locked_ptr);

                bool operator!=(locked_ptr &a_locked_ptr) { return !operator==(a_locked_ptr); };

              private:
                vnode *m_vnode;
        };

        struct locked_ptr
        {
                locked_ptr()
                    : m_ptr()
                {
                }

                ~locked_ptr()
                {
                        if (!m_ptr.is_null())
                        {
                                m_ptr->unlock();
                        }
                }

                locked_ptr(ptr &a_ptr)
                    : locked_ptr()
                {
                        operator=(a_ptr.get_raw());
                }

                locked_ptr(vnode *a_raw_vnode)
                    : locked_ptr()
                {
                        operator=(a_raw_vnode);
                }

                locked_ptr &operator=(vnode *a_raw_vnode)
                {
                        if (!m_ptr.is_null())
                        {
                                m_ptr->unlock();
                        }

                        m_ptr = a_raw_vnode;

                        if (!m_ptr.is_null())
                        {
                                m_ptr->lock();
                        }

                        return *this;
                }

                locked_ptr &operator=(const ptr &a_ptr)
                {
                        if (!m_ptr.is_null())
                        {
                                m_ptr->unlock();
                        }

                        m_ptr = a_ptr;

                        if (!m_ptr.is_null())
                        {
                                m_ptr->lock();
                        }

                        return *this;
                }

                locked_ptr &operator=(locked_ptr &&a_other)
                {
                        if (!m_ptr.is_null())
                        {
                                m_ptr->unlock();
                        }

                        m_ptr = a_other.m_ptr;

                        a_other.m_ptr = nullptr;

                        return *this;
                }

                vnode *operator->() { return m_ptr.operator->(); }

                vnode *get_raw() { return m_ptr.get_raw(); }

                template <typename T> T *to_raw() { return static_cast<T *>(get_raw()); }

                bool is_null() { return m_ptr.is_null(); }

                bool operator==(vnode *a_other_raw) { return get_raw() == a_other_raw; }

                bool operator!=(vnode *a_other_raw) { return !operator==(a_other_raw); }

                bool operator==(locked_ptr &a_other) { return get_raw() == a_other.get_raw(); }

                bool operator!=(locked_ptr &a_other) { return !operator==(a_other); }

              private:
                vnode::ptr m_ptr;
        };

        struct observer
        {
                virtual void on_vnode_truncate(vnode * /* a_vnode*/, off_t /*a_new_length*/) {}
                virtual void on_vnode_dentry_created(vnode * /* a_parent */,
                                                     vnode * /* a_child */,
                                                     const char * /*a_child_name*/,
                                                     size_t /*a_child_name_len*/)
                {
                }

                virtual void on_vnode_dentry_removed(vnode * /*a_parent*/,
                                                     vnode * /*a_child*/,
                                                     const char * /*a_child_name*/,
                                                     size_t /*a_child_name_len*/)
                {
                }

                intrusive::list::link<observer> m_observer_link;
        };

        fstat_t m_fstat;
        emerixx::dirent_t m_dirent;
        // i32 v_flag; /* vnode flags (see below) */
        //  integer_t v_writecount; /* reference count of writers */
        //  integer_t v_holdcnt;   /* page & buffer references */
        //  size_t v_lastr;        /* last f (read-ahead) */
        // i32 pad;
        size_t m_age;          /* capability identifier */

        struct mount *v_mount; /* ptr to vfs we are in */
                               // size_t v_numoutput;     /* num of writes in progress */

        union {
                struct mount *vu_mountedhere; /* ptr to mounted vfs (VDIR) */
                                              // struct file *vu_file;
                struct socket *vu_socket;     /* unix ipc (VSOCK) */
                void *vu_private;
                //        struct vn_pager *vu_vmdata;   /* private data for vm (VREG) */
                //        struct specinfo *vu_specinfo; /* device (VCHR, VBLK) */
                //        struct fifoinfo *vu_fifoinfo; /* fifo (VFIFO) */
        } v_un;

        // struct nqlease *v_lease;         /* Soft reference to lease */
        // size_t v_lastw;                  /* last write (write cluster) */
        // size_t v_cstart;                 /* start block of cluster */
        // size_t v_lasta;                  /* last allocation */
        // size_t v_clen;                   /* length of current cluster */
        // size_t v_ralen;                  /* Read-ahead length */
        // size_t v_maxra;                  /* last readahead block */
        // enum vtagtype v_tag;             /* type of underlying data */
        // enum vcache_state v_cache_state; /* cache consistency */
        // enum vtype v_type; /* vnode type */
        // int m_block_shift;               /* round to 128 bytes */
        // int pad__;
        // tailq_link v_freelist_link; /* vnode freelist link*/
        list_link v_mntvnodes; /* vnodes for mount point */
        // buf::vnbufs_list v_cleanblkhd; /* clean blocklist head */
        // buf::vnbufs_list v_dirtyblkhd; /* dirty blocklist head */
        intrusive::list::link<observer>::head<&observer::m_observer_link> m_observers;

        static vnode::ptr root;

        // using freelist       = tailq_link::head<&vnode::v_freelist_link>;
        using mntvnodes_list = list_link::head<&vnode::v_mntvnodes>;

        vnode(struct mount *mp);

        virtual ~vnode();

        void notify_dentry_created(vnode::locked_ptr &a_child, const char *a_child_name, size_t a_child_name_len)
        {
                vnode *child = a_child.get_raw();
                m_observers.for_each([=](observer *o) { o->on_vnode_dentry_created(this, child, a_child_name, a_child_name_len); });
        }

        void notify_dentry_removed(vnode::locked_ptr &a_child, const char *a_child_name, size_t a_child_name_len)
        {
                KASSERT(a_child->islocked());
                a_child->unlock();
                vnode *child = a_child.get_raw();

                m_observers.for_each([=](observer *o) { o->on_vnode_dentry_removed(this, child, a_child_name, a_child_name_len); });
                a_child->lock();
        }

        void observer_attach(observer *a_observer) { m_observers.insert_head(a_observer); }

        void observer_detach(observer *a_observer) { m_observers.remove(a_observer); }

        // void on_buffer_attach(buf_t *a_buffer);

        // void on_buffer_detach(buf_t *a_buffer);

        int rdwr_uio(enum uio_rw r, char *base, int len, off_t offset, enum uio_seg segflg, int ioflg, int *aresid);

        /*
         * Check for write permissions on the specified vnode.
         * The read-only status of the file system is checked.
         * Also, prototype text segments cannot be written.
         */
        // int writecheck();

        kern_return_t get_page(size_t a_offset, vm_prot_t a_prot, vm_page_t *a_vm_page_out) override;

        bool collapse(vm_object *, vm_object **, vm_offset_t *) override { return false; }

        bool is_memory_mappable()
        {
                // Needs more checking, not all char devices can be mapped
                return S_ISREG(m_fstat.st_mode) || S_ISBLK(m_fstat.st_mode) || (S_ISCHR(m_fstat.st_mode) && (m_size >= PAGE_SIZE));
        }

        static vnode::ptr get_from_file(file *a_file);

        kern_return_t alloc_file(integer_t a_oflags);

        kern_return_t open(file *a_file, mode_t a_open_flags, file **a_file_out = nullptr);

        kern_return_t close(file *a_file, int a_fflag);

        kern_return_t ioctl(file *a_file, int a_cmd, void *a_data, int a_fflag);

        kern_return_t create(const char *a_name, size_t a_name_len, mode_t a_mode, vnode::locked_ptr &a_vnode_out);

        kern_return_t mknod(const char *a_name, size_t a_name_len, dev_t a_dev, mode_t a_mode, vnode::locked_ptr &a_vnode_out);

        kern_return_t unlink(vnode::locked_ptr &a_vnode, const char *a_name, size_t a_name_len);

        kern_return_t link(vnode::locked_ptr &a_target_vn, const char *a_name, size_t a_name_len);

        kern_return_t rename(vnode::locked_ptr &a_from_vn,
                             char const *a_from_cnp_name,
                             size_t a_from_cnp_name_len,
                             vnode::locked_ptr &a_target_parent_vn,
                             vnode::locked_ptr &a_target_vn,
                             char const *a_target_cnp_name,
                             size_t a_target_cnp_name_len);

        kern_return_t fsync(int a_waitfor);

        // virtual int seek(off_t a_oldoff, off_t a_newoff, struct ucred *a_cred) = 0;

        // kern_return_t select(int a_which, int a_fflags, struct ucred *a_cred, pthread_t a_p);

        kern_return_t poll(file *a_file, pollfd **a_pfd);

        // virtual int pathconf(int a_name, int *a_retval) = 0;

        // virtual int advlock(char *a_id, int a_op, struct flock *a_fl, int a_flags) = 0;

        kern_return_t truncate(off_t a_length, int a_flags);

      public:
        // public interface for a vnode
        kern_return_t uio_move(uio *a_uio, int a_ioflags);
        kern_return_t uio_move(file *a_file, uio *a_uio, int a_ioflags);
        kern_return_t stat(fstat_t *a_fstat);
        kern_return_t strategy(buffer_cache::io_descriptor *a_io_desc);
        kern_return_t chmod(mode_t a_mode);
        kern_return_t chown(uid_t a_uid, gid_t a_gid);
        kern_return_t access(mode_t a_mode);
        kern_return_t utimens(timespec *a_utimens, int a_flags);
        kern_return_t lookup(const char *a_name, size_t a_namelen, vnode::locked_ptr &a_vnode_out);
        kern_return_t getdents(uio *a_uio);

      private:
        // Overrides from vm_object
        void do_unreference() override;
        bool do_extend(size_t a_size) override;

      protected:
        // Types for use by the filesystem implementation
        struct fs_readdir_ctx_t
        {
                char const *name;
                u32 namelen;
                u32 exit;
                off_t fs_offset;
                void *user_data;
        };

        using fs_readdir_cb_t = kern_return_t (vnode::*)(emerixx::dirent_t *a_dirent, fs_readdir_ctx_t *a_ctx);

        // Utility methods for the filesystem implementation
        kern_return_t read_fs_block(vm_offset_t a_block_offset, buffer_cache::io_descriptor **a_iodesc_out)
        {
                return buffer_cache::read(this, a_block_offset, static_cast<size_t>(m_fstat.st_blksize), a_iodesc_out);
        }

        template <typename T>
        kern_return_t read_fs_type(vm_offset_t a_offset,
                                   T **a_type_out,
                                   buffer_cache::io_descriptor **a_iodesc_out,
                                   size_t *a_block_residue = nullptr)
        {
                kern_return_t retval = read_fs_block(a_offset & ~(m_fstat.st_blksize - 1), a_iodesc_out);

                if (retval == KERN_SUCCESS)
                {
                        (*a_iodesc_out)->get_data(a_type_out, a_offset & (m_fstat.st_blksize - 1), a_block_residue);
                }
                else
                {
                        *a_type_out = nullptr;
                }

                return retval;
        }

        kern_return_t buffered_uio_move(uio *a_uio);

      private:
        // Callbacks from the filesystem implementation
        kern_return_t readdir_callback(emerixx::dirent_t *a_dirent, fs_readdir_ctx_t *a_ctx);
        kern_return_t lookup_callback(emerixx::dirent_t *a_dirent, fs_readdir_ctx_t *a_ctx);
        kern_return_t dirent_update_callback(emerixx::dirent_t *a_dirent, fs_readdir_ctx_t *a_ctx);

        // Filesystem specific methods
        virtual kern_return_t fs_readdir(fs_readdir_cb_t a_cb, fs_readdir_ctx_t *a_ctx)                = 0;
        virtual kern_return_t fs_dir_initialize(fstat_t *a_parent_stat)                                = 0;
        virtual kern_return_t fs_dir_reparent(fstat_t *a_parent_stat)                                  = 0;
        virtual kern_return_t fs_dir_empty()                                                           = 0;
        virtual kern_return_t fs_readwrite_link(uio *a_uio)                                            = 0;
        virtual kern_return_t fs_readwrite_spec(file *a_file, uio *a_uio, int a_ioflag)                = 0;
        virtual kern_return_t fs_strategy(buffer_cache::io_descriptor *a_io_desc)                      = 0;
        virtual kern_return_t fs_ioctl(file *a_file, int a_cmd, void *a_data, int a_fflag)             = 0;
        virtual kern_return_t fs_open(file *a_file, mode_t a_mode)                                     = 0;
        virtual kern_return_t fs_close(file *a_file)                                                   = 0;
        virtual kern_return_t fs_poll(file *a_file, pollfd **a_pfd)                                    = 0;
        virtual kern_return_t fs_remove_dirent(emerixx::dirent_t *a_dent)                              = 0;
        virtual kern_return_t fs_insert_dirent(emerixx::dirent_t *a_dent)                              = 0;
        virtual kern_return_t fs_get_page(size_t a_offset, vm_prot_t a_prot, vm_page_t *a_vm_page_out) = 0;

      public:
};

vnode::ptr::ptr(vnode::locked_ptr &&a_locked_ptr)
    : vnode::ptr::ptr()
{
        operator=(a_locked_ptr.get_raw());
        a_locked_ptr = nullptr;
}

bool vnode::ptr::operator==(locked_ptr &a_locked_ptr) { return m_vnode == a_locked_ptr.get_raw(); }

vnode::ptr &vnode::ptr::operator=(locked_ptr &a_locked_ptr) { return operator=(a_locked_ptr.get_raw()); }

#endif // __cplusplus

/*
 * Vnode flags.
 */
#define VROOT    0x0001 /* root of its file system */
#define VTEXT    0x0002 /* vnode is a pure text prototype */
#define VSYSTEM  0x0004 /* vnode being used by kernel */
#define VXLOCK   0x0100 /* vnode is locked to change underlying type */
#define VXWANT   0x0200 /* process is waiting for vnode */
#define VBWAIT   0x0400 /* waiting for output to complete */
#define VALIASED 0x0800 /* vnode has an alias */
#define VMAPPED  0x1000 /* memory mapped by bio */

/*
 * Flags for va_cflags.
 */
#define VA_UTIMES_NULL 0x01 /* utimes argument was NULL */

/*
 * Flags for ioflag.
 */
#define IO_UNIT       0x01 /* do I/O as atomic unit */
#define IO_APPEND     0x02 /* append write to end */
#define IO_SYNC       0x04 /* do I/O synchronously */
#define IO_NODELOCKED 0x08 /* underlying node already locked */
#define IO_NDELAY     0x10 /* FNDELAY flag set in file table */

/*
 *  Modes.  Some values same as Ixxx entries from inode.h for now.
 */
#define VSUID  04000 /* set user id on execution */
#define VSGID  02000 /* set group id on execution */
#define VSVTX  01000 /* save swapped text even after use */
#define VREAD  00400 /* read, write, execute permissions */
#define VWRITE 00200
#define VEXEC  00100

#ifdef KERNEL
/*
 * Convert between vnode types and inode formats (since POSIX.1
 * defines mode word of stat structure in terms of inode formats).
 */
// extern enum vtype iftovt_tab[];
// extern int vttoif_tab[];
// #define IFTOVT(mode)          (iftovt_tab[((mode)&S_IFMT) >> 12])
// #define VTTOIF(indx)          (vttoif_tab[(int)(indx)])
// #define MAKEIMODE(indx, mode) (int)(VTTOIF(indx) | (mode))

/*
 * Flags to various vnode functions.
 */
#define SKIPSYSTEM 0x0001 /* vflush: skip vnodes marked VSYSTEM */
#define FORCECLOSE 0x0002 /* vflush: force file closeure */
#define WRITECLOSE 0x0004 /* vflush: only close writeable files */
#define DOCLOSE    0x0008 /* vclean: close active files */
#define V_SAVE     0x0001 /* vinvalbuf: sync file first */
#define V_SAVEMETA 0x0002 /* vinvalbuf: leave indirect blocks */

/*
 * Macro/function to check for client cache inconsistency w.r.t. leasing.
 */
#define LEASE_READ  0x1 /* Check lease for readers */
#define LEASE_WRITE 0x2 /* Check lease for modifiers */

#ifdef NFS
#if NFS
void lease_check(struct vnode *vp, pthread_t p, struct ucred *ucred, int flag);
void lease_updatetime(int deltat);
#define LEASE_CHECK(vp, p, cred, flag) lease_check((vp), (p), (cred), (flag))
#define LEASE_UPDATETIME(dt)           lease_updatetime(dt)
#else
#define LEASE_CHECK(vp, p, cred, flag)
#define LEASE_UPDATETIME(dt)
#endif
#endif /* NFS */
#endif /* KERNEL */
#define LEASE_CHECK(vp, p, cred, flag)

/*
 * Mods for exensibility.
 */

/*
 * Flags for vdesc_flags:
 */
#define VDESC_MAX_VPS 16
/* Low order 16 flag bits are reserved for willrele flags for vp arguments. */
#define VDESC_VP0_WILLRELE 0x0001
#define VDESC_VP1_WILLRELE 0x0002
#define VDESC_VP2_WILLRELE 0x0004
#define VDESC_VP3_WILLRELE 0x0008
#define VDESC_NOMAP_VPP    0x0100
#define VDESC_VPP_WILLRELE 0x0200

/*
 * VDESC_NO_OFFSET is used to identify the end of the offset list
 * and in places where no such field exists.
 */
#define VDESC_NO_OFFSET -1
