#pragma once
/*
 * Mach Operating System
 * Copyright (c) 1991,1990,1989,1988,1987 Carnegie Mellon University
 * All Rights Reserved.
 *
 * Permission to use, copy, modify and distribute this software and its
 * documentation is hereby granted, provided that both the copyright
 * notice and this permission notice appear in all copies of the
 * software, derivative works or modified versions, and any portions
 * thereof, and that both notices appear in supporting documentation.
 *
 * CARNEGIE MELLON ALLOWS FREE USE OF THIS SOFTWARE IN ITS "AS IS"
 * CONDITION.  CARNEGIE MELLON DISCLAIMS ANY LIABILITY OF ANY KIND FOR
 * ANY DAMAGES WHATSOEVER RESULTING FROM THE USE OF THIS SOFTWARE.
 *
 * Carnegie Mellon requests users of this software to return to
 *
 *  Software Distribution Coordinator  or  Software.Distribution@CS.CMU.EDU
 *  School of Computer Science
 *  Carnegie Mellon University
 *  Pittsburgh PA 15213-3890
 *
 * any improvements or extensions that they make and grant Carnegie Mellon
 * the rights to redistribute these changes.
 */
/*
 *	File:	vm/vm_user.h
 *	Author:	Avadis Tevanian, Jr., Michael Wayne Young
 *	Date:	1986
 *
 *	Declarations of user-visible virtual address space
 *	management functionality.
 */

#include <kernel/vm/vm_prot.h>

#ifdef __cplusplus
extern "C" {
#endif

kern_return_t vm_allocate(vm_map_t map, vm_offset_t *addr, vm_size_t size, boolean_t anywhere);

kern_return_t vm_allocate_with_pager(
    vm_map_t map, vm_offset_t *addr, vm_size_t size, boolean_t fitit, vm_pager_t pager, vm_offset_t poffset, boolean_t internal);

kern_return_t vm_deallocate(vm_map_t map, vm_offset_t start, vm_size_t size);
kern_return_t vm_inherit();
kern_return_t vm_protect(vm_map_t map, vm_offset_t start, vm_size_t size, boolean_t set_maximum, vm_prot_t new_protection);
kern_return_t _vm_statistics();
kern_return_t vm_read();
kern_return_t vm_write();
kern_return_t vm_copy();

#ifdef __cplusplus
}
#endif
