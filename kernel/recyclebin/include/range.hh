#pragma once

namespace emerixx {
namespace range {

template <typename T>
class __reverse {
  T &x;

 public:
  __reverse(T &x)
      : x(x) {}

  auto begin() const -> decltype(this->x.rbegin()) { return x.rbegin(); }

  auto end() const -> decltype(this->x.rend()) { return x.rend(); }
};

template <typename T>
__reverse<T> reverse(T &x) {
  return __reverse<T>(x);
}

}  // namespace range
}  // namespace emerixx