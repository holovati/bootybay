#pragma once
#include <mach/vm_prot.h>
struct vnode;

#ifdef __cplusplus
extern "C" {
#endif

int vm_kmmap(vm_map_t map,
             vm_offset_t *addr,
             vm_size_t size,
             vm_prot_t prot,
             vm_prot_t maxprot,
             int flags,
             struct vnode *handle,
             vm_offset_t foff);

#ifdef __cplusplus
}
#endif