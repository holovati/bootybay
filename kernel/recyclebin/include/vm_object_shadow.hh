#pragma once

#ifndef VM_OBJECT_HEADER
#error "Include vm_object.h instead."
#endif

struct vm_object_shadow final : vm_object
{
        kern_return_t get_page(size_t a_offset, vm_prot_t a_prot, vm_page_t *a_vm_page_out) override;

        bool collapse(vm_object *a_source, vm_object **a_backing_object, vm_offset_t *a_backing_object_offset) override;

        static vm_object_shadow *create(vm_object *a_source_object, vm_offset_t a_source_offset, size_t a_size);

        vm_object_shadow(vm_object *a_source, vm_offset_t a_source_offset, size_t a_source_size);
        ~vm_object_shadow();

        vm_object *m_shadow;         /* My shadow */
        vm_offset_t m_shadow_offset; /* Offset in shadow */

      private:
        void do_unreference() override;
        bool do_extend(size_t a_size) override;
};
