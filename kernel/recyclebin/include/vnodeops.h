#pragma once

struct file;
struct mount;
struct nameidata;
struct proc;
struct stat;
struct ucred;
struct uio;
struct vattr;
struct vnode;
struct componentname;
struct flock;
struct timeval;
struct buf;

typedef int (*vop_open_fnp_t)(struct vnode *a_vp,
                              mode_t a_mode,
                              struct ucred *a_cred,
                              struct proc *a_p);

typedef int (*vop_strategy_fnp_t)(struct buf *a_bp);

/* Methods and macros to call the vnode ops */
typedef struct vnodeops {
  vop_open_fnp_t open;

  int (*close)(struct vnode *a_vp,
               int a_fflag,
               struct ucred *a_cred,
               struct proc *a_p);

  int (*read)(struct vnode *a_vp,
              struct uio *a_uio,
              int a_ioflag,
              struct ucred *a_cred);

  int (*write)(struct vnode *a_vp,
               struct uio *a_uio,
               int a_ioflag,
               struct ucred *a_cred);

  int (*ioctl)(struct vnode *a_vp,
               int a_command,
               char *a_data,
               int a_fflag,
               struct ucred *a_cred,
               struct proc *a_p);

  int (*getattr)(struct vnode *a_vp,
                 struct vattr *a_vap,
                 struct ucred *a_cred,
                 struct proc *a_p);

  int (*setattr)(struct vnode *a_vp,
                 struct vattr *a_vap,
                 struct ucred *a_cred,
                 struct proc *a_p);

  int (*access)(struct vnode *a_vp,
                mode_t a_mode,
                struct ucred *a_cred,
                struct proc *a_p);

  int (*lookup)(struct vnode *a_vp,
                struct vnode **a_tdvp,
                struct componentname *a_cnp);

  int (*create)(struct vnode *a_dvp,
                struct vnode **a_vpp,
                struct componentname *a_cnp,
                struct vattr *a_vap);

  int (*remove)(struct vnode *a_dvp,
                struct vnode *a_vp,
                struct componentname *a_cnp);

  int (*link)(struct vnode *a_vp,
              struct vnode *a_tdvp,
              struct componentname *a_cnp);

  int (*rename)(struct vnode *a_fdvp,
                struct vnode *a_fvp,
                struct componentname *a_fcnp,
                struct vnode *a_tdvp,
                struct vnode *a_tvp,
                struct componentname *a_tcnp);

  int (*mkdir)(struct vnode *a_dvp,
               struct vnode **a_vpp,
               struct componentname *a_cnp,
               struct vattr *a_vap);

  int (*rmdir)(struct vnode *a_dvp,
               struct vnode *a_vp,
               struct componentname *a_cnp);

  int (*readdir)(struct vnode *a_vp,
                 struct uio *a_uio,
                 struct ucred *a_cred,
                 int *a_eofflag,
                 u32 *a_cookies,
                 int a_ncookies);

  int (*symlink)(struct vnode *a_dvp,
                 struct vnode **a_vpp,
                 struct componentname *a_cnp,
                 struct vattr *a_vap,
                 char *a_target);

  int (*readlink)(struct vnode *a_vp, struct uio *a_uio, struct ucred *a_cred);

  int (*fsync)(struct vnode *a_vp,
               struct ucred *a_cred,
               int a_waitfor,
               struct proc *a_p);

  int (*inactive)(struct vnode *a_vp);

  int (*seek)(struct vnode *a_vp,
              off_t a_oldoff,
              off_t a_newoff,
              struct ucred *a_cred);

  int (*mknod)(struct vnode *a_dvp,
               struct vnode **a_vpp,
               struct componentname *a_cnp,
               struct vattr *a_vap);

  int (*select)(struct vnode *a_vp,
                int a_which,
                int a_fflags,
                struct ucred *a_cred,
                struct proc *a_p);

  int (*mmap)(struct vnode *a_vp,
              int a_fflags,
              struct ucred *a_cred,
              struct proc *a_p);

  int (*abortop)(struct vnode *a_dvp, struct componentname *a_cnp);

  int (*reclaim)(struct vnode *a_vp);

  int (*lock)(struct vnode *a_vp);

  int (*unlock)(struct vnode *a_vp);

  int (*bmap)(struct vnode *a_vp,
              size_t a_bn,
              struct vnode **a_vpp,
              size_t *a_bnp,
              int *a_runp);

  vop_strategy_fnp_t strategy;

  int (*print)(struct vnode *vp);

  int (*islocked)(struct vnode *a_vp);

  int (*pathconf)(struct vnode *a_vp, int a_name, int *a_retval);

  int (*advlock)(struct vnode *a_vp,
                 char *a_id,
                 int a_op,
                 struct flock *a_fl,
                 int a_flags);

  int (*blkatoff)(struct vnode *a_vp,
                  off_t a_offset,
                  char **a_res,
                  struct buf **a_bpp);

  int (*valloc)(struct vnode *a_pvp,
                int a_mode,
                struct ucred *a_cred,
                struct vnode **a_vpp);

  int (*vfree)(struct vnode *a_pvp, ino_t a_ino, int a_mode);

  int (*truncate)(struct vnode *a_vp,
                  off_t a_length,
                  int a_flags,
                  struct ucred *a_cred,
                  struct proc *a_p);

  int (*update)(struct vnode *a_vp,
                struct timeval *a_ta,
                struct timeval *a_tm,
                int a_waitfor);

  int (*bwrite)(struct buf *a_bp);
} vnodeops_t;
