#pragma once

#define ILIST_ENTRY(type, list_type_name)                                                                                          \
        intrusive::list::link<type> __##list_type_name##_link;                                                                     \
        using list_type_name = intrusive::list::link<type>::head<&type::__##list_type_name##_link>

namespace intrusive
{
namespace list
{

template <typename T> struct link;

template <typename T, link<T> T::*FIELD> struct __head;

/*
 * List definitions.
 */
template <typename T> struct link
{
        template <link<T> T::*FIELD> using head = __head<T, FIELD>;

        /* next element */
        T *le_next;

        /* address of previous next element */
        T **le_prev;
};

template <typename T, link<T> T::*FIELD> struct __head
{
        T *lh_first; /* first element */

        /*
         * List functions.
         */
        void init() { lh_first = nullptr; }

        void insert_after(T *listelm, T *elm)
        {
                if (((elm->*FIELD).le_next = (listelm->*FIELD).le_next) != nullptr)
                {
                        ((listelm->*FIELD).le_next->*FIELD).le_prev = &(elm->*FIELD).le_next;
                }

                (listelm->*FIELD).le_next = (elm);
                (elm->*FIELD).le_prev     = &(listelm->*FIELD).le_next;
        }

        void insert_head(T *elm)
        {
                if (((elm->*FIELD).le_next = lh_first) != nullptr)
                {
                        (lh_first->*FIELD).le_prev = &(elm->*FIELD).le_next;
                }
                lh_first              = (elm);
                (elm->*FIELD).le_prev = &lh_first;
        }

        void remove(T *elm)
        {
                if ((elm->*FIELD).le_next != nullptr)
                {
                        ((elm->*FIELD).le_next->*FIELD).le_prev = (elm->*FIELD).le_prev;
                }

                *(elm->*FIELD).le_prev = (elm->*FIELD).le_next;
        }

        T *remove_head()
        {
                T *first = lh_first;
                remove(first);
                return first;
        }

        bool empty() { return lh_first == nullptr; }

        T *front() { return lh_first; }

        struct iterator;

        iterator begin() { return iterator(lh_first); }

        iterator end() { return iterator(nullptr); }

        template <typename F> void for_each(F a_functor)
        {
                iterator it_beg = begin();
                iterator it_end = end();
                for (iterator it = it_beg; it != it_end;)
                {
                        iterator _it = it;
                        ++it;
                        a_functor(*_it);
                }
        }
};

template <typename T, link<T> T::*FIELD> struct __head<T, FIELD>::iterator
{
        typedef iterator self_type;
        typedef T value_type;
        typedef T *&reference;
        typedef T *pointer;
        // typedef std::forward_iterator_tag iterator_category;

        iterator(pointer ptr)
            : ptr_(ptr)
        {
        }

        // Prefix
        self_type operator++()
        {
                // always use postfix
                return operator++(0);
        }

        // Postfix
        self_type operator++(int)
        {
                self_type i = *this;
                ptr_        = (ptr_->*FIELD).le_next;
                return i;
        }

        reference operator*() { return ptr_; }

        pointer operator->() { return ptr_; }

        bool operator==(const self_type &rhs) { return ptr_ == rhs.ptr_; }

        bool operator!=(const self_type &rhs) { return ptr_ != rhs.ptr_; }

        pointer ptr_;
};

} // namespace list
} // namespace intrusive
