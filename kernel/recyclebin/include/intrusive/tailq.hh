#pragma once

namespace emerixx
{

template <typename T, typename P = T> static inline T *add_ptr(P *ptr, size_t offset)
{
        return ((T *)(((vm_address_t)(ptr)) + offset));
}

} // namespace emerixx

namespace intrusive
{
namespace tailq
{

template <typename T> struct link;

template <typename T, link<T> T::*FIELD> struct __head;

template <typename T> struct link
{
        using object_type = T;

        template <link<object_type> object_type::*FIELD> using head = __head<object_type, FIELD>;

        object_type *tqe_next;  /* next element */
        object_type **tqe_prev; /* address of previous next element */
};

template <typename T, link<T> T::*FIELD> struct __head
{
        T *tqh_first; /* first element */
        T **tqh_last; /* addr of last next element */

        bool empty() { return (tqh_first == nullptr); }

        T *front() { return tqh_first; }

        T *back() { return **(emerixx::add_ptr<T **>(tqh_last, sizeof(T *))); }

        void init()
        {
                tqh_first = nullptr;
                tqh_last  = &tqh_first;
        }

        void insert_head(T *elm)
        {
                if (((elm->*FIELD).tqe_next = tqh_first) != nullptr)
                {
                        ((elm->*FIELD).tqe_next->*FIELD).tqe_prev = &(elm->*FIELD).tqe_next;
                }
                else
                {
                        tqh_last = &(elm->*FIELD).tqe_next;
                }
                tqh_first              = (elm);
                (elm->*FIELD).tqe_prev = &tqh_first;
        }

        void insert_tail(T *elm)
        {
                (elm->*FIELD).tqe_next = nullptr;
                (elm->*FIELD).tqe_prev = tqh_last;
                *tqh_last              = (elm);
                tqh_last               = &(elm->*FIELD).tqe_next;
        }

        T *remove(T *elm)
        {
                if (((elm->*FIELD).tqe_next) != nullptr)
                {
                        ((elm->*FIELD).tqe_next->*FIELD).tqe_prev = (elm->*FIELD).tqe_prev;
                }
                else
                {
                        tqh_last = (elm->*FIELD).tqe_prev;
                }

                *(elm->*FIELD).tqe_prev = (elm->*FIELD).tqe_next;

                return elm;
        }

        void insert_after(T *listelm, T *elm)
        {
                if (((elm->*FIELD).tqe_next = (listelm->*FIELD).tqe_next) != nullptr)
                {
                        ((elm->*FIELD).tqe_next->*FIELD).tqe_prev = &(elm->*FIELD).tqe_next;
                }
                else
                {
                        tqh_last = &(elm->*FIELD).tqe_next;
                }

                (listelm->*FIELD).tqe_next = elm;
                (elm->*FIELD).tqe_prev     = &(listelm->*FIELD).tqe_next;
        }

        T *remove_front() { return remove(front()); }

        T *remove_back() { return remove(back()); }

        struct iterator;

        iterator begin() { return iterator(tqh_first); }

        iterator end() { return iterator(nullptr); }

        template <typename F> void for_each(F a_functor)
        {
                iterator it_beg = begin();
                iterator it_end = end();
                for (iterator it = it_beg; it != it_end;)
                {
                        iterator _it = it;
                        ++it;
                        a_functor(*_it);
                }
        }
};

template <typename T, link<T> T::*FIELD> struct __head<T, FIELD>::iterator
{
        typedef iterator self_type;
        typedef T value_type;
        typedef T *&reference;
        typedef T *pointer;
        // typedef std::forward_iterator_tag iterator_category;

        iterator(pointer ptr)
            : ptr_(ptr)
        {
        }

        // Prefix
        self_type operator++()
        {
                // Always use postfix
                return operator++(0);
                // ptr_ = (ptr_->*FIELD).tqe_next;
                // return *this;
        }

        // Postfix
        self_type operator++(int)
        {
                self_type i = *this;
                ptr_        = (ptr_->*FIELD).tqe_next;
                return i;
        }

        reference operator*() { return ptr_; }

        pointer operator->() { return ptr_; }

        bool operator==(const self_type &rhs) { return ptr_ == rhs.ptr_; }

        bool operator!=(const self_type &rhs) { return ptr_ != rhs.ptr_; }

        pointer ptr_;
};

} // namespace tailq
} // namespace intrusive