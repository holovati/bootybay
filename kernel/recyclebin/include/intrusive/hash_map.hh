#pragma once

#include "list.hh"

namespace intrusive
{
namespace static_unordered
{

template <typename T> using link = list::link<T>;

template <typename T> struct hash
{
        size_t operator()(T const &s) const { return hash_type(s); }
};

template <typename T> struct equal
{
        constexpr bool operator()(const T &lhs, const T &rhs) const { return lhs == rhs; }
};

template <typename T,
          link<T> T::*LinkField,
          typename Key,
          Key const T::*KeyField,
          size_t SIZE       = 128,
          typename Hash     = hash<Key>,
          typename KeyEqual = equal<Key>>

struct map
{
        static_assert((SIZE & (SIZE - 1)) == 0, "Static hash map size must be pow of 2");

        using bucket_type = typename link<T>::template head<LinkField>;

        void init()
        {
                for (bucket_type &bucketref : buckets)
                {
                        bucketref.init();
                }
        }

        void insert(T *elm) { buckets[Hash{}(elm->*KeyField) & (SIZE - 1)].insert_head(elm); }

        T *find(Key const *key)
        {
                bucket_type &b = buckets[Hash{}(*key) & (SIZE - 1)];
                if (!b.empty())
                {
                        for (T *elm : b)
                        {
                                if (KeyEqual{}(elm->*KeyField, *key))
                                {
                                        return elm;
                                }
                        }
                }
                return nullptr;
        }

        T *find(Key const &key) { return find(&key); }

        void remove(T *elm) { buckets[Hash{}(elm->*KeyField) & (SIZE - 1)].remove(elm); }

        T *remove(Key const *key)
        {
                bucket_type *bucket = &buckets[Hash{}(*key) & (SIZE - 1)];
                for (T *elm : *bucket)
                {
                        if (KeyEqual(elm->*KeyField), *key)
                        {
                                bucket->remove(elm);
                                return elm;
                        }
                }
                return nullptr;
        }

        T *remove(Key const &key) { return remove(&key); }

        bucket_type buckets[SIZE];
};

} // namespace static_unordered
} // namespace intrusive
