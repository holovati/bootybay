#pragma once

#ifndef VM_OBJECT_HEADER
#error "Include vm_object.h instead."
#endif

struct vm_object_copy final : vm_object
{
        kern_return_t get_page(size_t a_offset, vm_prot_t a_prot, vm_page_t *a_vm_page_out) override;

        bool collapse(vm_object *, vm_object **, vm_offset_t *) override { return false; }

        vm_object_copy(vm_object *a_source);

        ~vm_object_copy();

        static vm_object_copy *create(vm_object *a_source);

      private:
        void do_unreference() override;
        bool do_extend(size_t a_size) override;
};
