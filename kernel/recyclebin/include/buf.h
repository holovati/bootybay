#pragma once

#include <mach/intrusive/list.hh>

struct vnode;

#ifdef __cplusplus
namespace buffer_cache
{

#define IO_DESCRIPTOR_READ     BIT(0)
#define IO_DESCRIPTOR_ACCESSED BIT(1)
#define IO_DESCRIPTOR_DIRTY    BIT(2)
#define IO_DESCRIPTOR_BUSY     BIT(3)
#define IO_DESCRIPTOR_WANTED   BIT(4)
#define IO_DESCRIPTOR_IODONE   BIT(5)
#define IO_DESCRIPTOR_IOERROR  BIT(6)
#define IO_DESCRIPTOR_ASYNC    BIT(7)

using block_address_phys_t = u64;
using block_address_log_t  = u64;

struct io_descriptor
{
        using driver_link_t = intrusive::list::link<io_descriptor>;

        u64 m_io_header_idx : 4;
        u64 m_flags : 8;
        u64 m_fs_flags : 4;

        u64 m_lba_offset : 48;

        driver_link_t m_driver_link;

        block_address_phys_t m_phys_block_address;

        block_address_log_t get_logical_block_address();

        vm_page_t get_vm_page();

        vm_address_t get_phys_address();

        size_t get_block_size();

        void clear_data();

        void io_done(kern_return_t a_result);

        void get_data(void **a_buf_out, size_t a_buf_size, size_t a_block_offset, size_t *a_block_residue);

        template <typename T> void get_data(T **a_buf, size_t a_block_offset = 0, size_t *a_block_residue = nullptr)
        {
                get_data(reinterpret_cast<void **>(a_buf), sizeof(T), a_block_offset, a_block_residue);
        }

        template <typename T> void get_data(T *a_buf, size_t a_block_offset = 0, size_t *a_block_residue = nullptr)
        {
                T *tmp;
                get_data(reinterpret_cast<void **>(&tmp), sizeof(T), a_block_offset, a_block_residue);
                *a_buf = *tmp;
        }
};

io_descriptor *get(vnode *a_vnode, vm_offset_t a_logical_offset, size_t a_block_size);

kern_return_t read(vnode *a_vnode, vm_offset_t a_logical_offset, size_t a_block_size, io_descriptor **a_io_desc);

kern_return_t read_page(
    vnode *a_vnode, vm_offset_t a_logical_offset, size_t a_block_size, size_t a_read_size, io_descriptor **a_first_io_desc);

kern_return_t write(io_descriptor *a_io_desc, bool a_delay = true);

kern_return_t write_async(io_descriptor *a_io_desc);

void page_out(vnode *a_vnode);

void release(io_descriptor *a_io_desc);

bool invalidate(vnode *a_vnode, vm_offset_t a_logical_offset, size_t a_block_size);

} // namespace buffer_cache

#endif