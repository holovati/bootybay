#pragma once
struct vnode;
struct pagerops;
/*
 * VNODE pager private data.
 */
struct vn_pager {
  natural_t vnp_flags;  /* flags */
  struct vnode *vnp_vp; /* vnode */
  vm_size_t vnp_size;   /* vnode current size */
};
typedef struct vn_pager *vn_pager_t;

#define VN_PAGER_NULL ((vn_pager_t)0)
#define VNP_PAGING    0x01 /* vnode used for pageout */
#define VNP_CACHED    0x02 /* vnode is cached */

/*
 * Remove vnode associated object from the object cache.
 *
 * XXX unlock the vnode if it is currently locked.
 * We must do this since uncaching the object may result in its
 * destruction which may initiate paging activity which may necessitate
 * re-locking the vnode.
 */
boolean_t vnode_pager_uncache(struct vnode *vp);
void vnode_pager_umount(struct mount *mp);
void vnode_pager_setsize(struct vnode *vp, size_t nsize);
struct pagerops const *vnode_pager_getops();