#pragma once
#include <kernel/lock.h>
#include <kernel/vm/vm_page.h>
#include <kernel/vm/vm_prot.h>
#include <kernel/vm/vm_page_map.h>

struct _vm_object_t
{
};

#ifdef __cplusplus

#ifndef VM_OBJECT_HEADER
#define VM_OBJECT_HEADER
#endif

/*
 * Flags
 */
#define VM_OBJ_CANPERSIST 0x0001 /* allow to persist */
#define VM_OBJ_INTERNAL   0x0002 /* internally created object */
#define VM_OBJ_ACTIVE     0x0004 /* used to mark active objects */
#define VM_OBJ_NEW_TYPE   0x0008

/*
 *	Types defined:
 *
 *	vm_object_t		Virtual memory object.
 */

struct vm_object_shadow;
struct vm_object_copy;

struct vm_object
{
        // using tailq_link = intrusive::tailq::link<vm_object>;

        // tailq_link object_list_link; /* list of all objects */
        // using object_list = tailq_link::head<&vm_object::object_list_link>;

        // tailq_link cached_list_link; /* for persistence */
        // using cached_list = tailq_link::head<&vm_object::cached_list_link>;

        // natural_t flags; /* see below */
        /* Paging (in or out) so don't collapse or destroy */
        // natural_t paging_in_progress;
        // size_t ref_count;           /* How many refs?? */

        size_t m_size; /* Object size */
        // vm_object *m_copy; /* Object that holds copies of my changed pages */
        // i32 m_flag;
        integer_t m_usecount;
        lock_data_t m_lock;
        // vm_pager_t pager;           /* Where to get data */
        // vm_offset_t paging_offset; /* Offset into paging space */

        struct vm_page_map_data m_page_map; /* Resident memory */
        // size_t m_resident_page_count;         /* number of resident pages */
        // void reference();

        // void ocopy(vm_offset_t, vm_size_t, vm_object_t *, vm_offset_t *, boolean_t
        // *);

        // vm_object_t oshadow(vm_offset_t *, vm_size_t);

        // void opmap_remove(vm_offset_t, vm_offset_t);

        // void _opmap_copy(vm_offset_t, vm_offset_t);
        // void page_remove(vm_offset_t, vm_offset_t);

        // void collapse();

        // virtual boolean_t coalesce(vm_object_t, vm_offset_t, vm_offset_t, vm_size_t, vm_size_t);

        // void setpager(vm_pager_t a_pager, vm_offset_t a_off, boolean_t
        // a_read_only);

        virtual kern_return_t get_page(vm_offset_t a_offset, vm_prot_t a_prot, vm_page_t *a_vm_page_out)                    = 0;
        virtual bool collapse(vm_object *a_destination, vm_object **a_backing_object, vm_offset_t *a_backing_object_offset) = 0;

        /// ----------------------------------------------
        kern_return_t lock();
        kern_return_t unlock();
        kern_return_t try_lock();
        kern_return_t islocked();

        bool extend(size_t a_new_size);

        void reference();
        void unreference();

        void map_protection_change(vm_prot_t a_prot);

        /* the single kernel object */
        static vm_object *kernel;

        static vm_object *create_unnamed(size_t a_size);

        static vm_object_shadow *create_shadow(vm_object *a_source, vm_offset_t a_source_offset, size_t a_shadow_size);

        static vm_object_shadow *create_copy(vm_object *a_source, vm_offset_t a_source_offset, size_t a_copy_size);

        static vm_object *get_zero_object();

        static u8 const *zero_page_bytes();

        vm_object() = default;
        vm_object(size_t a_size);
        virtual ~vm_object();

      private:
        virtual void do_unreference()         = 0;
        virtual bool do_extend(size_t a_size) = 0;
};

// vm_object_t vm_object_allocate(vm_size_t);
// void vm_object_deallocate(vm_object_t);
// void vm_object_initialize(vm_size_t, vm_object_t);
void vm_object_bootstrap(void);

#include <kernel/vm/vm_object_copy.hh>
#include <kernel/vm/vm_object_shadow.hh>
#endif
