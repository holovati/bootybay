#pragma once
#include <mach/signal.h>
#include <mach/time.h>
#include <mach/time_out.h>

struct rlimit;
struct rusage;
struct sysinfo;

typedef struct thread *thread_t;

#ifdef __cplusplus
#include <mach/param.h>
#include <sys/resource.h>

#include <mach/poll.h>

typedef struct filedesc *filedesc_t;

struct pthread
{
        process_t process;         // The process this thread is a part of
        thread_t kthread;          // Kernel part of this thread
        integer_t clone_flags;     // clone(...) flags this thread was created with
        filedesc_t filedesc;       // File descriptor table used by this thread
        ksigaction *signal_action; // NSIG of signal actions
        sigset_t signal_pending;   //
        sigset_t signal_blocked;   //
        sigset_t signal_ignored;   //
        sigset_t signal_masked;    //
        integer_t exit_signal;     // Signal we are to send to the parent thread when this thread exits (0 == nothing is sent)
        tid_t *tid_address;        // User address we are supposed to clear when we exit
        tid_t tid;                 // Unique number representing this thread
};

/* stat codes */
#define SSLEEP 1 /* awaiting an event */
#define SWAIT  2 /* (abandoned state) */
#define SRUN   3 /* running */
#define SIDL   4 /* intermediate state in process creation */
#define SZOMB  5 /* intermediate state in process termination */
#define SSTOP  6 /* process being traced */

/* flag codes */
#define SLOAD  01 /* in core */
#define SSYS   02 /* scheduling process */
#define SLOCK  04 /* process cannot be swapped */
#define SSWAP  010 /* process is being swapped out */
#define STRC   020 /* process is being traced */
#define SWTED  040 /* another tracing flag */
#define SULOCK 0100 /* user settable lock in core */

/* These flags are kept in p_flags. */
#define P_ADVLOCK   0x00001 /* Process may hold a POSIX advisory lock. */
#define P_CONTROLT  0x00002 /* Has a controlling terminal. */
#define P_INMEM     0x00004 /* Loaded into memory. */
#define P_NOCLDSTOP 0x00008 /* No SIGCHLD when children stop. */
#define P_PPWAIT    0x00010 /* Parent is waiting for child to exec/exit. */
#define P_PROFIL    0x00020 /* Has started profiling. */
#define P_SELECT    0x00040 /* Selecting; wakeup/waiting danger. */
#define P_SINTR     0x00080 /* Sleep is interruptible. */
#define P_SUGID     0x00100 /* Had set id privileges since last exec. */
#define P_SYSTEM    0x00200 /* System proc: no sigs, stats or swapping. */
#define P_TIMEOUT   0x00400 /* Timing out during sleep. */
#define P_TRACED    0x00800 /* Debugged process being traced. */
#define P_WAITED    0x01000 /* Debugging process has waited for child. */
#define P_WEXIT     0x02000 /* Working on exiting. */
#define P_EXEC      0x04000 /* Process called exec. */

/*
 * One structure allocated per session.
 */
struct session
{
        size_t s_count;           /* Ref cnt; pgrps in session. */
        process_t s_leader;       /* Session leader. */
        struct vnode *s_ttyvp;    /* Vnode of controlling terminal. */
        struct tty *s_ttyp;       /* Controlling terminal. */
        char s_login[MAXLOGNAME]; /* Setlogin() name. */
};

#define PID_MAX 30000
#define NO_PID  30001

struct process
{
        pthread thread; // The main thread

        using list_link = intrusive::list::link<process>;

        enum class resource
        {
                in_block,
                out_block
        };
        list_link p_pglink;                              /* List link of processes in pgrp. */
        list_link p_hash;                                /* Hash chain. */
        list_link p_list;                                /* List of all processes. */
        list_link p_sibling;                             /* List link of sibling processes. */
        list_link::head<&process::p_sibling> p_children; /* List of children. */
        integer_t p_acflag;                              /* Accounting flags. */
        integer_t p_flag;                                /* P_* flags. */
        integer_t p_uid;                                 /* user id, used to direct tty signals */
        integer_t p_pid;                                 /* unique process id */
        integer_t p_stat;                                /* S* process status. */
        integer_t p_xstat;                               /* Exit status for wait; also stop signal. */
        struct pcred *p_cred;                            /* Process owner's identity. */
        char const *p_wmesg;                             /* Event the process is awaiting */
        struct pgrp *p_pgrp;                             /* Pointer to process group. */
        process_t p_pptr;                                /* Pointer to parent process. */
        struct filedesc *p_fd;                           /* Ptr to open files structure. */
        // size_t p_clktim;       /* time to alarm clock signal */
        // size_t p_dupfd;               /* Sideways return value from fdopen. XXX */
        sigset_t p_siglist;    /* Signals arrived but not delivered.*/
        sigset_t p_sigmask;    /* Current signal mask. */
        sigset_t p_sigignore;  /* Signals being ignored. */
        sigset_t p_sigcatch;   /* Signals being caught by user. */
        sigset_t ps_oldmask;   /* saved mask from before sigpause */
        stack_t p_sigaltstack; /* sp & on stack state variable */

        timer_elt_data_t p_itimer_elt[3]; /* for getitimer/setitimer */
        itimerspec p_itimer[3];           /*part of the above */

        struct ksigaction p_sigacts[NSIG];

        // size_t vm_rssize; /* current resident set size in pages */
        // size_t vm_swrss;  /* resident set size before last swap */
        // size_t vm_tsize;  /* text size (pages) XXX */
        // size_t vm_dsize;  /* data size (pages) XXX */
        size_t vm_ssize; /* stack size (pages) */
        // vm_address_t vm_taddr;    /* user virtual address of text XXX */
        // vm_address_t vm_daddr;    /* user virtual address of data XXX */
        vm_address_t vm_maxsaddr; /* user VA at max stack growth */
        struct rusage p_usage;

        int on_sleep(char const *wmesg, bool interruptible);
        int on_wakeup();

        void on_signal_dispatch(int signum);
        natural_t on_signal_return(int signum, sigset_t sigmask, natural_t sysret_val);

        int get_pending_signal();

        void signal(int signum);

        kern_return_t setitimer(int a_which, itimerspec const *a_itimerspec);

        kern_return_t sigaction(int signum, struct ksigaction const *a_actp, struct ksigaction *a_oactp, size_t sigsetsize);

        kern_return_t sigprocmask(int how, sigset_t *a_nsetp, sigset_t *a_osetp, size_t sigsetsize);

        bool is_session_leader();

        void resource_usage(resource a_resource);

        struct file *fd_to_file(u32 a_fd);

        static process_t find(pid_t pid);
        static process_t current();
};

process_t proc_init();

int proc_vfork_clone(pthread_t p, natural_t a_sp);

int proc_is_inferior(process_t a_procp);

/*
 * Change the count associated with number of processes
 * a given user is using.
 */
int proc_change_count(uid_t uid, int diff);

int proc_signal_exit(pthread_t a_proc, int signum);

kern_return_t pthread_create(pthread_t *a_pthread_out);

#endif // __cplusplus

#ifdef __cplusplus
extern "C"
{
#endif //__cplusplus
        pid_t proc_get_pid(pthread_t p);
        void proc_signal(pthread_t a_proc, int a_sig);
        // Syscalls

#ifdef __cplusplus
}
#endif // __cplusplus
