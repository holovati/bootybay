#pragma once

#include <signal.h>

/* Size of sigset_t in bytes */
#define SIGSET_SIZE (_NSIG / 8)

#define SIGMASK(s) (((sigset_t)1) << (((sigset_t)(s)) - 1))

/* additional signal action values, used only temporarily/internally */
#define SIG_CATCH (void (*)(int))3

/*
 * Signal properties and actions.
 * The array below categorizes the signals and their default actions
 * according to the following properties:
 */
#define SA_KILL     0x01 /* terminates process by default */
#define SA_CORE     0x02 /* ditto and coredumps */
#define SA_STOP     0x04 /* suspend process */
#define SA_TTYSTOP  0x08 /* ditto, from tty */
#define SA_IGNORE   0x10 /* ignore by default */
#define SA_CONT     0x20 /* continue if suspended */
#define SA_CANTMASK 0x40 /* non-maskable, catchable */

// https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/signal.h.html
#ifdef SIGNAL_INCLUDE_SIGPROP
static int s_sigprop[NSIG] = {
    0,                    /* unused */
    SA_KILL,              /* 1 SIGHUP */
    SA_KILL,              /* 2 SIGINT */
    SA_KILL | SA_CORE,    /* 3 SIGQUIT */
    SA_KILL | SA_CORE,    /* 4 SIGILL */
    SA_KILL | SA_CORE,    /* 5 SIGTRAP */
    SA_KILL | SA_CORE,    /* 6 SIGABRT */
    SA_KILL | SA_CORE,    /* 7 SIGBUS */
    SA_KILL | SA_CORE,    /* 8 SIGFPE */
    SA_KILL,              /* 9 SIGKILL */
    SA_KILL,              /* 10 SIGUSR1 */
    SA_KILL | SA_CORE,    /* 11 SIGSEGV */
    SA_KILL,              /* 12 SIGUSR2 */
    SA_KILL,              /* 13 SIGPIPE */
    SA_KILL,              /* 14 SIGALRM */
    SA_KILL,              /* 16 SIGTERM */
    SA_IGNORE,            /* 17 SIGCHLD */
    SA_IGNORE | SA_CONT,  /* 18 SIGCONT */
    SA_STOP,              /* 19 SIGSTOP */
    SA_STOP | SA_TTYSTOP, /* 20 SIGTSTP */
    SA_STOP | SA_TTYSTOP, /* 21 SIGTTIN */
    SA_STOP | SA_TTYSTOP, /* 22 SIGTTOU */
    SA_IGNORE,            /* 23 SIGURG */
    SA_KILL,              /* 24 SIGXCPU */
    SA_KILL,              /* 25 SIGXFSZ */
    SA_KILL,              /* 26 SIGVTALRM */
    SA_KILL,              /* 27 SIGPROF */
    SA_IGNORE,            /* 28 SIGWINCH  */
    SA_KILL,              /* 29 SIGPOLL/SIGIO (BSD=SA_IGNORE, POSIX=SA_KILL) */
    SA_KILL,              /* 30 SIGPWR */
    SA_KILL | SA_CORE,    /* 31 SIGSYS */
};
#endif // SIGNAL_INCLUDE_SIGPROP

#define CONTSIGMASK (SIGMASK(SIGCONT))
#define STOPSIGMASK (SIGMASK(SIGSTOP) | SIGMASK(SIGTSTP) | SIGMASK(SIGTTIN) | SIGMASK(SIGTTOU))

#ifndef NONO
struct ksigaction
{
        sighandler_t handler;   /* Disposition of signals */
        integer_t flags;        /* Flags which modify the behavior of the signal */
        void (*restorer)(void); /* Userspace stub that calls rt_sigreturn */
        sigset_t mask;          /* mask specifies a mask of signals which should be blocked (i.e.,
                                    added to the signal mask of the thread in which the signal
                                    handler is invoked) during execution of the signal handler. In
                                    addition, the signal which triggered the handler will be blocked,
                                    unless the SA_NODEFER flag is used. */
};
#endif

int proc_sig_create_frame(pthread_t a_procp, int signum);
