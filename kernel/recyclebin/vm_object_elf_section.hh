#pragma once
#include <elf.h>
#include <emerixx/file.h>
#include <vfs/vnode.h>

struct vm_object_elf_section final : vm_object
{
        // Vnode holding the data
        vnode::ptr m_vnode;

        // Refcounter
        integer_t m_ref_count;

        // The file size of the segment we are representing
        size_t m_file_size;

        // The file offset of the file we are representing
        vm_offset_t m_file_offset;

        // Number of pages we are expected to fix up (head, tail and zeropages)
        size_t m_pages_expected;

        kern_return_t get_page(vm_offset_t a_offset, vm_prot_t a_prot, vm_page_t *a_vm_page_out) override;

        bool collapse(vm_object *a_destination, vm_object **a_backing_object, vm_offset_t *a_backing_object_offset) override;

        vm_object_elf_section(Elf64_Phdr *a_phdr, vnode *a_vnode);
        ~vm_object_elf_section() = default;

        static vm_object *create(Elf64_Phdr *a_phdr, vnode *a_vnode);

      private:
        void do_unreference() override;
        bool do_extend(size_t a_size) override;
};
