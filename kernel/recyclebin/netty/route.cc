#include <aux/init.h>
#include <emerixx/net/route.h>
#include <kernel/zalloc.h>
#include <sys/ioctl.h>

/*
 * Mach Operating System
 * Copyright (c) 1989 Carnegie-Mellon University
 * Copyright (c) 1988 Carnegie-Mellon University
 * All rights reserved.  The CMU software License Agreement specifies
 * the terms and conditions for use and redistribution.
 */

/*
 * Copyright (c) 1980, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)route.c	7.1 (Berkeley) 6/4/86
 */

#ifdef GATEWAY
#define RTHASHSIZ 64
#else
#define RTHASHSIZ 8
#endif

#if (RTHASHSIZ & (RTHASHSIZ - 1)) == 0
#define RTHASHMOD(h) ((h) & (RTHASHSIZ - 1))
#else
#define RTHASHMOD(h) ((h) % RTHASHSIZ)
#endif

static queue_head_t rthost[RTHASHSIZ];
static queue_head_t rtnet[RTHASHSIZ];
static struct rtstat rtstat;

static int rttrash;              /* routes not in table but not freed */
static struct sockaddr wildcard; /* zero valued cookie for wildcard searches */
// int rthashsize = RTHASHSIZ; /* for netstat, etc. */

static void null_hash(struct sockaddr *addr, struct afhash *hp);
static int null_netmatch(sockaddr_t a1, sockaddr_t a2);

ZONE_DEFINE(rtentry, s_rtentry_zone, 0x100, 0x10, ZONE_EXHAUSTIBLE, 0x10);

struct afswitch afswitch[AF_MAX] = {
  //
        {.af_hash = null_hash,  .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
        { .af_hash = null_hash, .af_netmatch = null_netmatch, .af_equal = null_netmatch},
 //
};

// ********************************************************************************************************************************
void null_hash(struct sockaddr *addr, struct afhash *hp)
// ********************************************************************************************************************************
{
        hp->afh_nethash = hp->afh_hosthash = 0;
}

// ********************************************************************************************************************************
int null_netmatch(sockaddr_t a1, sockaddr_t a2)
// ********************************************************************************************************************************
{
        return (0);
}

/*
 * Packet routing routines.
 */
// ********************************************************************************************************************************
void rtalloc(route_t ro)
// ********************************************************************************************************************************
{
        if (ro->ro_rt && ro->ro_rt->rt_ifp && (ro->ro_rt->rt_flags & RTF_UP))
        {
                return; /* XXX */
        }

        if (ro->ro_dst.sa_family >= AF_MAX)
        {
                return;
        }

        struct afhash h;
        struct sockaddr *dst = &ro->ro_dst;
        afswitch[ro->ro_dst.sa_family].af_hash(dst, &h);
        uint32_t hash  = h.afh_hosthash;
        sa_family_t af = dst->sa_family;

        queue_head_t *table = rthost;
        int doinghost       = 1;

again:
        for (struct rtentry *rt = queue_containing_record(queue_first(&table[RTHASHMOD(hash)]), struct rtentry, rt_bucket_link);
             queue_end(&table[RTHASHMOD(hash)], &rt->rt_bucket_link) == false;
             rt = queue_containing_record(queue_next(&rt->rt_bucket_link), struct rtentry, rt_bucket_link))
        {
                if (rt->rt_hash != hash)
                {
                        continue;
                }

                if ((rt->rt_flags & RTF_UP) == 0 || (rt->rt_ifp->if_flags & IFF_UP) == 0)
                {
                        continue;
                }

                if (doinghost)
                {
                        if (afswitch[rt->rt_dst.sa_family].af_equal(&rt->rt_dst, dst))
                        {
                                continue;
                        }
                }
                else
                {
                        if (rt->rt_dst.sa_family != af || !afswitch[af].af_netmatch(&rt->rt_dst, dst))
                        {
                                continue;
                        }
                }

                rt->rt_refcnt++;

                if (dst == &wildcard)
                {
                        rtstat.rts_wildcard++;
                }

                ro->ro_rt = rt;

                return;
        }

        if (doinghost)
        {
                doinghost = 0;
                hash      = h.afh_nethash;
                table     = rtnet;
                goto again;
        }

        /*
         * Check for wildcard gateway, by convention network 0.
         */
        if (dst != &wildcard)
        {
                dst = &wildcard, hash = 0;
                goto again;
        }

        rtstat.rts_unreach++;
}

/*
 * Carry out a request to change the routing table.  Called by
 * interfaces at boot time to make their ``local routes'' known,
 * for ioctl's, and as the result of routing redirects.
 */
// ********************************************************************************************************************************
kern_return_t rtrequest(ioctl_req_t req, rtentry_t entry)
// ********************************************************************************************************************************
{
        queue_head_t *bucket;
        queue_entry_t curr;
        struct rtentry *rt;
        struct afhash h;
        uint32_t hash;
        struct ifaddr *ifa;

        if (entry->rt_dst.sa_family >= AF_MAX)
        {
                return KERN_FAIL(EAFNOSUPPORT);
        }

        afswitch[entry->rt_dst.sa_family].af_hash(&entry->rt_dst, &h);

        if (entry->rt_flags & RTF_HOST)
        {
                hash   = h.afh_hosthash;
                bucket = &rthost[RTHASHMOD(hash)];
        }
        else
        {
                hash   = h.afh_nethash;
                bucket = &rtnet[RTHASHMOD(hash)];
        }

        // for (mfirst = mprev; m = *mprev; mprev = &m->m_next)
        for (curr = queue_first(bucket); queue_end(bucket, curr) == false; curr = queue_next(curr))
        {
                // rt = mtod(m, struct rtentry *);
                rt = queue_containing_record(curr, rtentry, rt_bucket_link);

                if (rt->rt_hash != hash)
                {
                        continue;
                }

                if (entry->rt_flags & RTF_HOST)
                {
                        if (afswitch[entry->rt_dst.sa_family].af_equal(&rt->rt_dst, &entry->rt_dst) == false)
                        {
                                continue;
                        }
                }
                else
                {
                        if (rt->rt_dst.sa_family != entry->rt_dst.sa_family
                            || afswitch[entry->rt_dst.sa_family].af_netmatch(&rt->rt_dst, &entry->rt_dst) == 0)
                        {
                                continue;
                        }
                }
                if (afswitch[entry->rt_dst.sa_family].af_equal(&rt->rt_gateway, &entry->rt_gateway))
                {
                        break;
                }
        }

        kern_return_t retval = KERN_SUCCESS;
        switch (ioctl_req_cmdno(req))
        {
                case SIOCDELRT:
                        // if (curr == nullptr)
                        if (queue_end(bucket, curr) == true)
                        {
                                retval = KERN_FAIL(ESRCH);
                                goto bad;
                        }

                        remqueue(curr);

                        if (rt->rt_refcnt > 0)
                        {
                                rt->rt_flags &= ~RTF_UP;
                                rttrash++;
                                // m->m_next = 0;
                        }
                        else
                        {
                                s_rtentry_zone.free(rt);
                        }

                        break;

                case SIOCADDRT:
                        if (queue_end(bucket, curr) == false)
                        {
                                retval = KERN_FAIL(EEXIST);
                                goto bad;
                        }

                        if ((entry->rt_flags & RTF_GATEWAY) == 0)
                        {
                                /*
                                 * If we are adding a route to an interface,
                                 * and the interface is a pt to pt link
                                 * we should search for the destination
                                 * as our clue to the interface.  Otherwise
                                 * we can use the local address.
                                 */
                                ifa = nullptr;
                                if (entry->rt_flags & RTF_HOST)
                                {
                                        ifa = ifa_ifwithdstaddr(&entry->rt_dst);
                                }

                                if (ifa == nullptr)
                                {
                                        ifa = ifa_ifwithaddr(&entry->rt_gateway);
                                }
                        }
                        else
                        {
                                /*
                                 * If we are adding a route to a remote net
                                 * or host, the gateway may still be on the
                                 * other end of a pt to pt link.
                                 */
                                ifa = ifa_ifwithdstaddr(&entry->rt_gateway);
                        }

                        if (ifa == nullptr)
                        {
                                ifa = ifa_ifwithnet(&entry->rt_gateway);
                                if (ifa == nullptr)
                                {
                                        retval = KERN_FAIL(ENETUNREACH);
                                        goto bad;
                                }
                        }

                        rt = s_rtentry_zone.alloc();

                        if (rt == nullptr)
                        {
                                retval = KERN_FAIL(ENOBUFS);
                                goto bad;
                        }

                        enqueue(bucket, &rt->rt_bucket_link);

                        rt->rt_hash    = hash;
                        rt->rt_dst     = entry->rt_dst;
                        rt->rt_gateway = entry->rt_gateway;
                        rt->rt_flags   = RTF_UP | (entry->rt_flags & (RTF_HOST | RTF_GATEWAY | RTF_DYNAMIC));
                        rt->rt_refcnt  = 0;
                        rt->rt_use     = 0;
                        rt->rt_ifp     = ifa->ifa_ifp;
                        break;
                default:
                        panic("XXX");
        }
bad:
        return retval;
}

/*
 * Set up a routing table entry, normally
 * for an interface.
 */
// ********************************************************************************************************************************
void rtinit(sockaddr_t dst, sockaddr_t gateway, ioctl_req_t cmd, int32_t flags)
// ********************************************************************************************************************************
{
        struct rtentry route = {};

        route.rt_dst     = *dst;
        route.rt_gateway = *gateway;
        route.rt_flags   = flags;

        rtrequest(cmd, &route);
}

// ********************************************************************************************************************************
void rtfree(rtentry_t rt)
// ********************************************************************************************************************************
{
        if (rt->rt_refcnt > 0)
        {
                (rt)->rt_refcnt--;
        }

        if (rt->rt_refcnt == 0 && (rt->rt_flags & RTF_UP) == 0)
        {
                rttrash--;
                s_rtentry_zone.free(rt);
        }
}

/*
 * Force a routing table entry to the specified
 * destination to go through the given gateway.
 * Normally called as a result of a routing redirect
 * message from the network layer.
 *
 * N.B.: must be called at splnet or higher
 *
 */
// ********************************************************************************************************************************
void rtredirect(sockaddr_t dst, sockaddr_t gateway, sockaddr_t src, int32_t flags)
// ********************************************************************************************************************************
{
        struct route ro;
        struct rtentry *rt;

        /* verify the gateway is directly reachable */
        if (ifa_ifwithnet(gateway) == nullptr)
        {
                rtstat.rts_badredirect++;
                return;
        }

        ro.ro_dst = *dst;
        ro.ro_rt  = nullptr;
        rtalloc(&ro);
        rt = ro.ro_rt;

        /*
         * If the redirect isn't from our current router for this dst,
         * it's either old or wrong.  If it redirects us to ourselves,
         * we have a routing loop, perhaps as a result of an interface
         * going down recently.
         */
        if ((rt && (afswitch[rt->rt_gateway.sa_family].af_equal(src, &rt->rt_gateway) == false)) || ifa_ifwithaddr(gateway))
        {
                rtstat.rts_badredirect++;
                if (rt)
                {
                        rtfree(rt);
                }

                return;
        }
        /*
         * Create a new entry if we just got back a wildcard entry
         * or the the lookup failed.  This is necessary for hosts
         * which use routing redirects generated by smart gateways
         * to dynamically build the routing tables.
         */
        if (rt && (*afswitch[dst->sa_family].af_netmatch)(&wildcard, &rt->rt_dst))
        {
                rtfree(rt);
                rt = nullptr;
        }

        if (rt == nullptr)
        {
                panic("commedted");
#if 0
                rtinit(dst, gateway, SIOCADDRT, (flags & RTF_HOST) | RTF_GATEWAY | RTF_DYNAMIC);
                rtstat.rts_dynamic++;
#endif
                return;
        }

        /*
         * Don't listen to the redirect if it's
         * for a route to an interface.
         */
        if (rt->rt_flags & RTF_GATEWAY)
        {
                if (((rt->rt_flags & RTF_HOST) == 0) && (flags & RTF_HOST))
                {
                        /*
                         * Changing from route to net => route to host.
                         * Create new route, rather than smashing route to net.
                         */
#if 0
                        rtinit(dst, gateway, (int)SIOCADDRT, flags | RTF_DYNAMIC);
#endif

                        rtstat.rts_dynamic++;
                }
                else
                {
                        /*
                         * Smash the current notion of the gateway to
                         * this destination.
                         */
                        rt->rt_gateway = *gateway;
                        rt->rt_flags |= RTF_MODIFIED;
                        rtstat.rts_newgateway++;
                }
        }
        else
        {
                rtstat.rts_badredirect++;
        }

        rtfree(rt);
}

/*
 * Routing table ioctl interface.
 */
// ********************************************************************************************************************************
kern_return_t rtioctl(ioctl_req_t cmd, char *data)
// ********************************************************************************************************************************
{
        return rtrequest(cmd, (struct rtentry *)data);
}

// ********************************************************************************************************************************
static kern_return_t route_init()
// ********************************************************************************************************************************
{
        for (queue_head_t &h : rthost)
        {
                queue_init(&h);
        }

        for (queue_head_t &h : rtnet)
        {
                queue_init(&h);
        }

        return KERN_SUCCESS;
}

early_initcall(route_init);
