#include <arpa/inet.h>

#include <lwip/pbuf.h>

#include <aux/init.h>

#include <emerixx/ioctl_req.h>
#include <emerixx/net/arp.h>
#include <emerixx/net/if.h>
#include <emerixx/net/route.h>

#include <kernel/sched_prim.h>
#include <kernel/thread.h>
#include <kernel/vm/pmap.h>
#include <kernel/vm/vm_kern.h>
#include <kernel/zalloc.h>

#include <mach/task.h>
#include <mach/thread.h>

#include <etl/algorithm.hh>

#include "domain.h"

#define N_NETDEVICE_BUFFERS (0x1000000 / PAGE_SIZE)

#define NETDEVICE_PRIVATE_SIZE                                                                                                     \
        ((PAGE_SIZE - sizeof(struct iface) - sizeof(queue_chain_t) - sizeof(natural_t) - sizeof(ifaddr_t) - sizeof(iface_t)        \
          - sizeof(natural_t))                                                                                                     \
         / sizeof(uint64_t))

#define NETDEVICE_TO_INTERNAL(nd) ((netdevice_internal *)((nd)))

#define ENQUEUE_POLL_DEV(pollhead, device) enqueue(pollhead, &NETDEVICE_TO_INTERNAL(device)->m_polllnk)
#define DEQUEUE_POLL_DEV(pollhead)         ((iface_t)(((natural_t)dequeue((pollhead))) - offsetof(netdevice_internal, m_polllnk)))

#define ENQUEUE_NETDEVICE_BUFFER(head, elt) enqueue((head), &(elt)->m_dev_link)
#define DEQUEUE_NETDEVICE_BUFFER(head)      ((iface_buffer_t)(((natural_t)dequeue((head))) - offsetof(iface_buffer, m_dev_link)))

struct netdevice_internal
{
        iface m_netdev;
        natural_t m_flags;
        queue_chain_t m_polllnk;
        ifaddr_t m_addrlist;
        iface_t m_next;
        natural_t pad;
        uint64_t m_private_data[NETDEVICE_PRIVATE_SIZE];
};

struct iface_buffer_internal
{
        struct iface_buffer ifbuf;
        struct pbuf_custom lwip_pc;
};

static_assert(sizeof(netdevice_internal) == PAGE_SIZE);

ZONE_DEFINE(netdevice_internal, s_netdevices, 10, 10, ZONE_EXHAUSTIBLE, 10);

static thread_t s_harvester_thread;
static queue_head_t s_pollq;
static queue_head_t s_netdev_buf_freelist;
static natural_t s_bufs_in_use;
static natural_t arp_tc;

// ********************************************************************************************************************************
static NORETURN void harvester_loop()
// ********************************************************************************************************************************
{
        if (current_thread()->wait_result == THREAD_TIMED_OUT)
        {
                domain_do_timeouts(50);
        }
        else if (current_thread()->wait_result == THREAD_INTERRUPTED)
        {
                domain_do_timeouts(50);
        }

        if ((elapsed_ticks - arp_tc) >= (hz * 10ul))
        {
                arp_tc = elapsed_ticks;
                arp_timer();
        }

        spl_t s = splbio();
        while (queue_empty(&s_pollq) == false)
        {
                iface_t netdev                         = DEQUEUE_POLL_DEV(&s_pollq);
                NETDEVICE_TO_INTERNAL(netdev)->m_flags = 0;
                splx(s);
                netdev->rx(netdev);
                s = splbio();
        }
        assert_wait((event_t)harvester_loop, true);
        splx(s);
        thread_set_timeout(hz / 20);
        thread_block(harvester_loop);
        UNREACHABLE();
}

// ********************************************************************************************************************************
void iface_network_wakeup()
// ********************************************************************************************************************************
{
        clear_wait(s_harvester_thread, THREAD_INTERRUPTED, false);
}

// ********************************************************************************************************************************
void iface_rx_sched(iface_t a_iface)
// ********************************************************************************************************************************
{
        spl_t s = splbio();
        if (NETDEVICE_TO_INTERNAL(a_iface)->m_flags == 0)
        {
                NETDEVICE_TO_INTERNAL(a_iface)->m_flags = 1;
                ENQUEUE_POLL_DEV(&s_pollq, a_iface);
                clear_wait(s_harvester_thread, THREAD_AWAKENED, true);
        }
        splx(s);
}

static iface_t ___g_if; // Yeah i know!

// ********************************************************************************************************************************
iface_t iface_alloc(natural_t a_private_size)
// ********************************************************************************************************************************
{
        KASSERT(a_private_size <= NETDEVICE_PRIVATE_SIZE);

        netdevice_internal *netdev_intern = s_netdevices.alloc();

        netdev_intern->m_netdev   = {};
        netdev_intern->m_polllnk  = {};
        netdev_intern->m_flags    = 0;
        netdev_intern->m_addrlist = nullptr;
        netdev_intern->m_next     = nullptr;

        ___g_if = &netdev_intern->m_netdev;
        return &netdev_intern->m_netdev;
}

// ********************************************************************************************************************************
void *iface_private(iface_t a_iface)
// ********************************************************************************************************************************
{
        return NETDEVICE_TO_INTERNAL(a_iface)->m_private_data;
}

// ********************************************************************************************************************************
static void iface_buffer_free_pbuf(struct pbuf *p)
// ********************************************************************************************************************************
{
        iface_buffer_t ifbuf = (iface_buffer_t)((char *)(p)-offsetof(iface_buffer_internal, lwip_pc));

        s_bufs_in_use--;

        ifbuf->m_phys_addr &= ~PAGE_MASK;
        ENQUEUE_NETDEVICE_BUFFER(&s_netdev_buf_freelist, ifbuf);
}

// ********************************************************************************************************************************
kern_return_t iface_buffer_alloc(iface_t a_iface, iface_buffer_t *a_iface_buffer_out)
// ********************************************************************************************************************************
{
        kern_return_t retval;

        if (queue_empty(&s_netdev_buf_freelist) == false)
        {
                *a_iface_buffer_out = DEQUEUE_NETDEVICE_BUFFER(&s_netdev_buf_freelist);

                if (a_iface == nullptr)
                {
                        a_iface = ___g_if;
                }

                // Should put it on the device queue
                (*a_iface_buffer_out)->m_iface = a_iface;

                (*a_iface_buffer_out)->m_offset     = 0;
                (*a_iface_buffer_out)->m_flags      = 0;
                (*a_iface_buffer_out)->m_layer_curr = 0;
                (*a_iface_buffer_out)->m_proto      = nullptr;

                iface_buffer_internal *ifbuf = ((iface_buffer_internal *)((*a_iface_buffer_out)));

                ifbuf->lwip_pc.custom_free_function = iface_buffer_free_pbuf;

                struct pbuf *p = &ifbuf->lwip_pc.pbuf;

                *p = {};

                p->payload = (void *)ifbuf->ifbuf.m_virt_addr;
                p->len = p->tot_len = PAGE_SIZE;
                p->ref              = 1;
                p->flags            = PBUF_FLAG_IS_CUSTOM;

                s_bufs_in_use++;

                retval = KERN_SUCCESS;
        }
        else
        {
                retval = KERN_FAIL(ENOMEM);
        }

        return retval;
}

// ********************************************************************************************************************************
natural_t iface_buffer_available_count()
// ********************************************************************************************************************************
{
        return N_NETDEVICE_BUFFERS - s_bufs_in_use;
}

// ********************************************************************************************************************************
void *iface_buffer_free(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        s_bufs_in_use--;

        a_iface_buffer->m_phys_addr &= ~PAGE_MASK;
        ENQUEUE_NETDEVICE_BUFFER(&s_netdev_buf_freelist, a_iface_buffer);
        return nullptr;
}

// ********************************************************************************************************************************
struct pbuf *iface_buffer_get_pbuf(iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        return &((iface_buffer_internal *)(a_iface_buffer))->lwip_pc.pbuf;
}

// ********************************************************************************************************************************
void iface_buffer_recv(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        a_iface_buffer->m_offset     = 0;
        a_iface_buffer->m_flags      = 0;
        a_iface_buffer->m_layer_curr = 0;
        a_iface_buffer->m_proto      = nullptr;

        iface_buffer_get_pbuf(a_iface_buffer)->len = a_iface_buffer->m_packet_size;

        typedef void *(*iface_io_func_t)(iface_t, iface_buffer_t);

        for (iface_io_func_t inf = a_iface->input; inf != nullptr;)
        {
                inf = (iface_io_func_t)inf(a_iface, a_iface_buffer);
        }
}

// ********************************************************************************************************************************
void *iface_buffer_send(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        if ((a_iface_buffer->m_flags & IFACE_BUFFER_FL_REFLECT) == 0)
        {
                a_iface_buffer->m_phys_addr |= (a_iface_buffer->m_size - a_iface_buffer->m_offset);
        }
        a_iface->tx(a_iface, a_iface_buffer);
        return nullptr;
}

void iface_buffer_send_pbuf(struct pbuf *a_pbuf)
{
        iface_buffer_t ibuf;
        kern_return_t retval = iface_buffer_alloc(nullptr, &ibuf);

        if (KERN_STATUS_FAILURE(retval))
        {
                return;
        }

        pbuf_copy_partial_pbuf(iface_buffer_get_pbuf(ibuf), a_pbuf, a_pbuf->tot_len, 0);

        ibuf->m_packet_size = a_pbuf->tot_len;

        ibuf->m_iface->tx(ibuf->m_iface, ibuf);
}

/*
 * Locate an interface based on a complete address.
 */
// ********************************************************************************************************************************
ifaddr_t ifa_ifwithaddr(sockaddr_t a_sa)
// ********************************************************************************************************************************
{
        for (ifaddr_t ifa = NETDEVICE_TO_INTERNAL(___g_if)->m_addrlist; ifa != nullptr; ifa = ifa->ifa_next)
        {
                if ((ifa->ifa_addr.sa_family == a_sa->sa_family)
                    && (afswitch[a_sa->sa_family].af_equal(a_sa, &ifa->ifa_addr) == true))
                {
                        return ifa;
                }
        }
        return nullptr;
}

/*
 * Locate the point to point interface with a given destination address.
 */
// ********************************************************************************************************************************
ifaddr_t ifa_ifwithdstaddr(sockaddr_t a_sa)
// ********************************************************************************************************************************
{
        for (iface_t ifp = ___g_if; ifp != nullptr; ifp = NETDEVICE_TO_INTERNAL(ifp)->m_next)
        {
                if (ifp->if_flags & IFF_POINTOPOINT)
                {
                        for (ifaddr_t ifa = NETDEVICE_TO_INTERNAL(ifp)->m_addrlist; ifa; ifa = ifa->ifa_next)
                        {
                                if (ifa->ifa_addr.sa_family != a_sa->sa_family)
                                {
                                        continue;
                                }

                                if (afswitch[a_sa->sa_family].af_equal(&ifa->ifa_dstaddr, a_sa) == true)
                                {
                                        return ifa;
                                }
                        }
                }
        }
        return nullptr;
}

/*
 * Find an interface on a specific network.  If many, choice is first found.
 */
// ********************************************************************************************************************************
ifaddr_t ifa_ifwithnet(sockaddr_t addr)
// ********************************************************************************************************************************
{
        if (addr->sa_family >= AF_MAX)
        {
                return nullptr;
        }

        for (iface_t ifp = ___g_if; ifp; ifp = NETDEVICE_TO_INTERNAL(ifp)->m_next)
        {
                for (ifaddr_t ifa = NETDEVICE_TO_INTERNAL(ifp)->m_addrlist; ifa != nullptr; ifa = ifa->ifa_next)
                {
                        if (ifa->ifa_addr.sa_family != addr->sa_family)
                        {
                                continue;
                        }

                        if (afswitch[addr->sa_family].af_netmatch(&ifa->ifa_addr, addr))
                        {
                                return ifa;
                        }
                }
        }

        return nullptr;
}

// ********************************************************************************************************************************
void iface_register_address(ifaddr_t a_ifaddr)
// ********************************************************************************************************************************
{
        a_ifaddr->ifa_next                         = NETDEVICE_TO_INTERNAL(___g_if)->m_addrlist;
        a_ifaddr->ifa_ifp                          = ___g_if;
        NETDEVICE_TO_INTERNAL(___g_if)->m_addrlist = a_ifaddr;
}

int ___nbufs;
// ********************************************************************************************************************************
static kern_return_t netdevice_init()
// ********************************************************************************************************************************
{
        s_harvester_thread = kernel_thread(kernel_task, harvester_loop, nullptr);
        thread_suspend(s_harvester_thread);
        s_harvester_thread->max_priority = BASEPRI_USER;
        s_harvester_thread->priority     = BASEPRI_USER;
        s_harvester_thread->sched_pri    = BASEPRI_USER;
        thread_resume(s_harvester_thread);

        s_bufs_in_use = 0;

        queue_init(&s_pollq);
        queue_init(&s_netdev_buf_freelist);

        iface_buffer_internal *ndbufs = nullptr;
        kmem_alloc_wired(kernel_map, (vm_address_t *)&ndbufs, sizeof(iface_buffer_internal) * N_NETDEVICE_BUFFERS);
        KASSERT(ndbufs != nullptr);

        vm_address_t ndbuf_data = 0;
        kmem_alloc_wired(kernel_map, &ndbuf_data, PAGE_SIZE * N_NETDEVICE_BUFFERS);
        KASSERT(ndbuf_data != 0);

        ___nbufs = 0;
        for (uint32_t i = 0; i < N_NETDEVICE_BUFFERS; ++i)
        {
                ndbufs[i].ifbuf.m_virt_addr   = ndbuf_data + (i * PAGE_SIZE);
                ndbufs[i].ifbuf.m_phys_addr   = pmap_extract(kernel_pmap, ndbufs[i].ifbuf.m_virt_addr);
                ndbufs[i].ifbuf.m_size        = PAGE_SIZE;
                ndbufs[i].ifbuf.m_packet_size = 0;

                queue_init(&ndbufs[i].ifbuf.m_dev_link);

                ENQUEUE_NETDEVICE_BUFFER(&s_netdev_buf_freelist, &ndbufs[i].ifbuf);
                ___nbufs++;
        }

        // Initialize protocols
        extern initcall_t __initcall_pr_start[];
        extern initcall_t __initcall_pr_end[];
        size_t n_calls = (size_t)(((size_t)__initcall_pr_end - (size_t)__initcall_pr_start) / sizeof(uintptr_t));
        for (size_t i = 0; i < n_calls; ++i)
        {
                KASSERT((__initcall_pr_start[i]()) == 0);
        }

        return KERN_SUCCESS;
}

subsys_initcall(netdevice_init);
