#include <emerixx/inet/ip_icmp.h>
#include <emerixx/net/if.h>

#include <etl/algorithm.hh>

#include "domain.h"
#include "in_cksum.h"
#include "protocol.h"

#define ICMP_HDR(buf) ((icmphdr_t)((buf)->m_virt_addr + (buf)->m_offset))
#define ICMP(buf)     ((icmp_t)ICMP_HDR(buf))

static void *icmp_output(iface_t a_iface, iface_buffer_t a_iface_buffer);

static inline bool icmphdr_csum_valid(icmphdr_t a_icmphdr, natural_t a_len)
{
        uint16_t csum     = 0;
        in_cksum_vec cksv = {.m_buf = a_icmphdr, .m_len = a_len};

        etl::swap(csum, a_icmphdr->checksum);
        a_icmphdr->checksum = in_cksum(&cksv, 1);
        etl::swap(csum, a_icmphdr->checksum);
        return csum == a_icmphdr->checksum;
}

static inline void icmphdr_calc_csum(icmphdr_t a_icmphdr, natural_t a_len)
{
        a_icmphdr->checksum = 0;
        in_cksum_vec cksv   = {.m_buf = a_icmphdr, .m_len = a_len};
        a_icmphdr->checksum = in_cksum(&cksv, 1);
}

/*
Destination Unreachable Message

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |     Type      |     Code      |          Checksum             |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                             unused                            |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |      Internet Header + 64 bits of Original Data Datagram      |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

   IP Fields:

   Destination Address

      The source network and address from the original datagram's data.

   ICMP Fields:

   Type

      3

   Code

      0 = net unreachable;

      1 = host unreachable;

      2 = protocol unreachable;

      3 = port unreachable;

      4 = fragmentation needed and DF set;

      5 = source route failed.

   Checksum ...

   Internet Header + 64 bits of Data Datagram

      The internet header plus the first 64 bits of the original
      datagram's data.  This data is used by the host to match the
      message to the appropriate process.  If a higher level protocol
      uses port numbers, they are assumed to be in the first 64 data
      bits of the original datagram's data.

   Description

      If, according to the information in the gateway's routing tables,
      the network specified in the internet destination field of a
      datagram is unreachable, e.g., the distance to the network is
      infinity, the gateway may send a destination unreachable message
      to the internet source host of the datagram.  In addition, in some
      networks, the gateway may be able to determine if the internet
      destination host is unreachable.  Gateways in these networks may
      send destination unreachable messages to the source host when the
      destination host is unreachable.

      If, in the destination host, the IP module cannot deliver the
      datagram  because the indicated protocol module or process port is
      not active, the destination host may send a destination
      unreachable message to the source host.

      Another case is when a datagram must be fragmented to be forwarded
      by a gateway yet the Don't Fragment flag is on.  In this case the
      gateway must discard the datagram and may return a destination
      unreachable message.

      Codes 0, 1, 4, and 5 may be received from a gateway.  Codes 2 and
      3 may be received from a host.
*/
// ********************************************************************************************************************************
static void *icmp_unreachable(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        log_debug("ICMP UNREACHABLE");
        return NETSTACK_CALL(iface_buffer_free);
}

/*
Redirect Message

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |     Type      |     Code      |          Checksum             |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                 Gateway Internet Address                      |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |      Internet Header + 64 bits of Original Data Datagram      |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

   IP Fields:

   Destination Address

      The source network and address of the original datagram's data.

   ICMP Fields:

   Type

      5

   Code

      0 = Redirect datagrams for the Network.

      1 = Redirect datagrams for the Host.

      2 = Redirect datagrams for the Type of Service and Network.

      3 = Redirect datagrams for the Type of Service and Host.

   Checksum ...

   Gateway Internet Address

      Address of the gateway to which traffic for the network specified
      in the internet destination network field of the original
      datagram's data should be sent.

   Internet Header + 64 bits of Data Datagram

      The internet header plus the first 64 bits of the original
      datagram's data.  This data is used by the host to match the
      message to the appropriate process.  If a higher level protocol
      uses port numbers, they are assumed to be in the first 64 data
      bits of the original datagram's data.

   Description

      The gateway sends a redirect message to a host in the following
      situation.  A gateway, G1, receives an internet datagram from a
      host on a network to which the gateway is attached.  The gateway,
      G1, checks its routing table and obtains the address of the next
      gateway, G2, on the route to the datagram's internet destination
      network, X.  If G2 and the host identified by the internet source
      address of the datagram are on the same network, a redirect
      message is sent to the host.  The redirect message advises the
      host to send its traffic for network X directly to gateway G2 as
      this is a shorter path to the destination.  The gateway forwards
      the original datagram's data to its internet destination.

      For datagrams with the IP source route options and the gateway
      address in the destination address field, a redirect message is
      not sent even if there is a better route to the ultimate
      destination than the next address in the source route.

      Codes 0, 1, 2, and 3 may be received from a gateway.
*/
// ********************************************************************************************************************************
static void *icmp_redirect(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        log_debug("ICMP REDIRECT");
        return NETSTACK_CALL(iface_buffer_free);
}

/*
Echo or Echo Reply Message:

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |     Type      |     Code      |          Checksum             |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |           Identifier          |        Sequence Number        |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |     Data ...
   +-+-+-+-+-

   IP Fields:

   Addresses

      The address of the source in an echo message will be the
      destination of the echo reply message.  To form an echo reply
      message, the source and destination addresses are simply reversed,
      the type code changed to 0, and the checksum recomputed.

   IP Fields:

   Type

      8 for echo message;

      0 for echo reply message.

   Code

      0

   Checksum ...

   Identifier

      If code = 0, an identifier to aid in matching echos and replies,
      may be zero.

   Sequence Number
      If code = 0, a sequence number to aid in matching echos and
      replies, may be zero.

   Description

      The data received in the echo message must be returned in the echo
      reply message.

      The identifier and sequence number may be used by the echo sender
      to aid in matching the replies with the echo requests.  For
      example, the identifier might be used like a port in TCP or UDP to
      identify a session, and the sequence number might be incremented
      on each echo request sent.  The echoer returns these same values
      in the echo reply.

      Code 0 may be received from a gateway or a host.
*/
// ********************************************************************************************************************************
static void *icmp_echo(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        icmphdr_t icmph = ICMP_HDR(a_iface_buffer);
        // a_iface_buffer->m_offset += sizeof(struct icmphdr);

        log_info("ICMP ECHO");

        icmph->type = ICMP_ECHOREPLY;

        a_iface_buffer->m_flags |= IFACE_BUFFER_FL_REFLECT;

        return NETSTACK_CALL(icmp_output);
}

// ********************************************************************************************************************************
static void *icmp_router_advertisement(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        log_debug("ICMP Router advertisement");
        return NETSTACK_CALL(iface_buffer_free);
}

// ********************************************************************************************************************************
static void *icmp_router_solicitation(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        log_debug("ICMP Router solicitation");
        return NETSTACK_CALL(iface_buffer_free);
}

/*
Time Exceeded Message

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |     Type      |     Code      |          Checksum             |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                             unused                            |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |      Internet Header + 64 bits of Original Data Datagram      |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

   IP Fields:

   Destination Address

      The source network and address from the original datagram's data.

   ICMP Fields:

   Type

      11

   Code

      0 = time to live exceeded in transit;

      1 = fragment reassembly time exceeded.

   Checksum ...

   Internet Header + 64 bits of Data Datagram

      The internet header plus the first 64 bits of the original
      datagram's data.  This data is used by the host to match the
      message to the appropriate process.  If a higher level protocol
      uses port numbers, they are assumed to be in the first 64 data
      bits of the original datagram's data.

   Description

      If the gateway processing a datagram finds the time to live field
      is zero it must discard the datagram.  The gateway may also notify
      the source host via the time exceeded message.

      If a host reassembling a fragmented datagram cannot complete the
      reassembly due to missing fragments within its time limit it
      discards the datagram, and it may send a time exceeded message.

      If fragment zero is not available then no time exceeded need be
      sent at all.

      Code 0 may be received from a gateway.  Code 1 may be received
      from a host.
*/

// ********************************************************************************************************************************
static void *icmp_time_exceeded(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        log_debug("ICMP TIME EXCEEDED");
        return NETSTACK_CALL(iface_buffer_free);
}

/*
Parameter Problem Message

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |     Type      |     Code      |          Checksum             |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |    Pointer    |                   unused                      |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |      Internet Header + 64 bits of Original Data Datagram      |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

   IP Fields:

   Destination Address

      The source network and address from the original datagram's data.

   ICMP Fields:

   Type

      12

   Code

      0 = pointer indicates the error.

   Checksum ...

   Pointer

      If code = 0, identifies the octet where an error was detected.

   Internet Header + 64 bits of Data Datagram

      The internet header plus the first 64 bits of the original
      datagram's data.  This data is used by the host to match the
      message to the appropriate process.  If a higher level protocol
      uses port numbers, they are assumed to be in the first 64 data
      bits of the original datagram's data.

   Description

      If the gateway or host processing a datagram finds a problem with
      the header parameters such that it cannot complete processing the
      datagram it must discard the datagram.  One potential source of
      such a problem is with incorrect arguments in an option.  The
      gateway or host may also notify the source host via the parameter
      problem message.  This message is only sent if the error caused
      the datagram to be discarded.

      The pointer identifies the octet of the original datagram's header
      where the error was detected (it may be in the middle of an
      option).  For example, 1 indicates something is wrong with the
      Type of Service, and (if there are options present) 20 indicates
      something is wrong with the type code of the first option.

      Code 0 may be received from a gateway or a host.
*/
// ********************************************************************************************************************************
static void *icmp_parameter_problem(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        log_debug("ICMP PARAMETER PROBE");
        return NETSTACK_CALL(iface_buffer_free);
}

/*
Timestamp or Timestamp Reply Message

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |     Type      |      Code     |          Checksum             |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |           Identifier          |        Sequence Number        |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |     Originate Timestamp                                       |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |     Receive Timestamp                                         |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |     Transmit Timestamp                                        |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

   IP Fields:

   Addresses

      The address of the source in a timestamp message will be the
      destination of the timestamp reply message.  To form a timestamp
      reply message, the source and destination addresses are simply
      reversed, the type code changed to 14, and the checksum
      recomputed.

   IP Fields:

   Type

      13 for timestamp message;

      14 for timestamp reply message.

   Code

      0

   Checksum ...

   Identifier
      If code = 0, an identifier to aid in matching timestamp and
      replies, may be zero.

   Sequence Number

      If code = 0, a sequence number to aid in matching timestamp and
      replies, may be zero.

   Description

      The data received (a timestamp) in the message is returned in the
      reply together with an additional timestamp.  The timestamp is 32
      bits of milliseconds since midnight UT.  One use of these
      timestamps is described by Mills [5].

      The Originate Timestamp is the time the sender last touched the
      message before sending it, the Receive Timestamp is the time the
      echoer first touched it on receipt, and the Transmit Timestamp is
      the time the echoer last touched the message on sending it.

      If the time is not available in miliseconds or cannot be provided
      with respect to midnight UT then any time can be inserted in a
      timestamp provided the high order bit of the timestamp is also set
      to indicate this non-standard value.

      The identifier and sequence number may be used by the echo sender
      to aid in matching the replies with the requests.  For example,
      the identifier might be used like a port in TCP or UDP to identify
      a session, and the sequence number might be incremented on each
      request sent.  The destination returns these same values in the
      reply.

      Code 0 may be received from a gateway or a host.
*/
// ********************************************************************************************************************************
static void *icmp_timestamp(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        log_debug("ICMP TIMESTAMP");
        return NETSTACK_CALL(iface_buffer_free);
}

/*
Information Request or Information Reply Message

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |     Type      |      Code     |          Checksum             |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |           Identifier          |        Sequence Number        |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

   IP Fields:

   Addresses

      The address of the source in a information request message will be
      the destination of the information reply message.  To form a
      information reply message, the source and destination addresses
      are simply reversed, the type code changed to 16, and the checksum
      recomputed.

   IP Fields:

   Type

      15 for information request message;

      16 for information reply message.

   Code

      0

   Checksum

      The checksum is the 16-bit ones's complement of the one's
      complement sum of the ICMP message starting with the ICMP Type.
      For computing the checksum , the checksum field should be zero.
      This checksum may be replaced in the future.

   Identifier

      If code = 0, an identifier to aid in matching request and replies,
      may be zero.

   Sequence Number

      If code = 0, a sequence number to aid in matching request and
      replies, may be zero.

   Description

      This message may be sent with the source network in the IP header
      source and destination address fields zero (which means "this"
      network).  The replying IP module should send the reply with the
      addresses fully specified.  This message is a way for a host to
      find out the number of the network it is on.

      The identifier and sequence number may be used by the echo sender
      to aid in matching the replies with the requests.  For example,
      the identifier might be used like a port in TCP or UDP to identify
      a session, and the sequence number might be incremented on each
      request sent.  The destination returns these same values in the
      reply.

      Code 0 may be received from a gateway or a host.
*/
// ********************************************************************************************************************************
static void *icmp_info_request(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        log_debug("ICMP UNREACHABLE");
        return NETSTACK_CALL(iface_buffer_free);
}

// ********************************************************************************************************************************
static void *icmp_forward_raw(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        log_debug("ICMP \"FORWARD RAW\"", ICMP_HDR(a_iface_buffer)->type);
        return NETSTACK_CALL(iface_buffer_free);
}

// ********************************************************************************************************************************
static void *icmp_drop(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        // a_iface_buffer->m_offset += sizeof(struct icmphdr);
        log_warn("Dropping ICMP type %000d ", ICMP_HDR(a_iface_buffer)->type);
        return NETSTACK_CALL(iface_buffer_free);
}

static void *(*s_icmp_type_ops[NR_ICMP_TYPES + 1])(iface_t, iface_buffer_t) = {
    icmp_forward_raw,          // ICMP_ECHOREPLY
    icmp_drop,                 // drop ? Unassigned
    icmp_drop,                 // drop ? Unassigned
    icmp_unreachable,          // ICMP_DEST_UNREACH
    icmp_drop,                 // ICMP_SOURCE_QUENCH (Deprecated)
    icmp_redirect,             // ICMP_REDIRECT
    icmp_drop,                 // drop ? Alternate Host Address (Deprecated)
    icmp_drop,                 // drop ? Unassigned
    icmp_echo,                 // ICMP_ECHO
    icmp_router_advertisement, // Router Advertisement
    icmp_router_solicitation,  // Router Solicitation
    icmp_time_exceeded,        // ICMP_TIME_EXCEEDED
    icmp_parameter_problem,    // ICMP_PARAMETERPROB
    icmp_timestamp,            // ICMP_TIMESTAMP
    icmp_forward_raw,          // ICMP_TIMESTAMPREPLY
    icmp_info_request,         // ICMP_INFO_REQUEST
    icmp_forward_raw,          // ICMP_INFO_REPLY
    icmp_drop,                 // ICMP_ADDRESS (Deprecated)
    icmp_drop                  // ICMP_ADDRESSREPLY (Deprecated)
};

// ********************************************************************************************************************************
void *icmp_input(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        icmphdr_t icmph = ICMP_HDR(a_iface_buffer);

        if (((a_iface_buffer->m_packet_size - a_iface_buffer->m_offset) < ICMP_MINLEN)
            || icmphdr_csum_valid(icmph, a_iface_buffer->m_packet_size - a_iface_buffer->m_offset) == false)
        {
                return NETSTACK_CALL(icmp_drop);
        }

        if (icmph->type <= NR_ICMP_TYPES)
        {
                return NETSTACK_CALL(s_icmp_type_ops[icmph->type]);
        }

        return NETSTACK_CALL(icmp_drop);
}

// ********************************************************************************************************************************
void *icmp_output(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        if (a_iface_buffer->m_flags & IFACE_BUFFER_FL_REFLECT)
        {
                icmphdr_t icmph = ICMP_HDR(a_iface_buffer);

                icmphdr_calc_csum(icmph, a_iface_buffer->m_packet_size - a_iface_buffer->m_offset);
        }
        return NETSTACK_CALL(a_iface_buffer->m_proto->m_output);
}

// ********************************************************************************************************************************
static kern_return_t icmp_init()
// ********************************************************************************************************************************
{
        return KERN_SUCCESS;
}

protocol_initcall(icmp_init);
