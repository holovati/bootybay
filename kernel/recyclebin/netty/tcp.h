#pragma once

#define TCP_PROTO_REQ_ENQ_OUT_BUF 3

typedef struct tcp_state_ops *tcp_state_ops_t;

typedef struct tcb_data *tcb_t;

typedef struct tcb_proto_ops *tcb_proto_ops_t;

struct tcb_data
{
        tcp_state_ops_t m_state_ops;
        union {
                struct
                {
                        integer_t backlog;
                        queue_head_t acceptq;
                } listen;
                struct
                {
                        queue_head_t outq;

                } syn_sent;
        } m_state_data;

        tcb_proto_ops_t m_proto_ops;

        /*

                        --------SND.WND--------
                        |                     |
                        |                     |
                        V                     V
                   1         2          3          4
              ----------|----------|----------|----------
                     SND.UNA    SND.NXT    SND.UNA
                                          +SND.WND

        1 - old sequence numbers that have been acknowledged
        2 - sequence numbers of unacknowledged data
        3 - sequence numbers allowed for new data transmission
        4 - future sequence numbers that are not yet allowed
        */
        uint32_t snd_una;           // old sequence numbers that have been acknowledged
        uint32_t snd_nxt;           // OUR next sequence number
        uint16_t snd_wnd;           // How much is the PEER willing to recieve
        uint16_t snd_wnd_shift_cnt; // Send window scale(HOW MUCH IS THE PEER WILLING TO RECIEVE)
        uint32_t snd_wl1;           // segment sequence number used for last window update
        uint32_t snd_wl2;           // segment acknowledgment number used for last window update
        uint32_t snd_mss;           // Maximum Segment Size of the peer
        uint32_t snd_up;            // send urgent pointer
        /*

                             ---RCV.WND--
                             |          |
                             |          |
                             V          V
                       1          2          3
                   ----------|----------|----------
                          RCV.NXT    RCV.NXT
                                    +RCV.WND

        1 - old sequence numbers that have been acknowledged
        2 - sequence numbers allowed for new reception
        3 - future sequence numbers that are not yet allowed
        */
        uint32_t rcv_nxt;           // sequence numbers allowed for new reception
        uint16_t rcv_wnd;           // space advertised to sender
        uint16_t rcv_wnd_shift_cnt; // Recieve window scale advertised to sender
        uint32_t rcv_up;            // receive urgent pointer
        uint32_t rcv_mss;           // Maximum Segment Size advertised to sender

        uint32_t iss; // initial send sequence number(set by us)
        uint32_t irs; // initial receive sequence number(set by the sender)

        uint32_t ack_timeout;
        uint32_t ack_rmss;
        uint32_t acked_rcv_nxt;
};

enum TCP_CONNECTION_STATE
{
        TCP_CONNECTION_STATE_CLOSED,
        TCP_CONNECTION_STATE_LISTEN,
        TCP_CONNECTION_STATE_SYN_RCVD,
        TCP_CONNECTION_STATE_SYN_SENT,
        TCP_CONNECTION_STATE_ESTAB,
        TCP_CONNECTION_STATE_FINWAIT_1,
        TCP_CONNECTION_STATE_FINWAIT_2,
        TCP_CONNECTION_STATE_CLOSING,
        TCP_CONNECTION_STATE_TIME_WAIT,
        TCP_CONNECTION_STATE_CLOSE_WAIT,
        TCP_CONNECTION_STATE_LAST_ACK,
};

struct tcb_proto_ops
{
        void (*accept_ready)(tcb_t, int);
        bool (*src_dest_equal)(tcb_t, iface_buffer_t, iface_buffer_t);
        kern_return_t (*proto_connect)(tcb_t, iface_buffer_t);
        void (*enq_out_network)(tcb_t, iface_buffer_t);
        kern_return_t (*enq_out_user)(tcb_t, iface_buffer_t);
        void (*state_change)(tcb_t, enum TCP_CONNECTION_STATE a_new_conn_state);
};

void tcp_tcb_init(tcb_t a_tcb, tcb_proto_ops_t);

void tcp_get_ports(iface_buffer_t a_iface_buffer, uint16_t iop[2]);

int tcp_csum_valid(iface_buffer_t a_iface_buffer);

int tcp_csum_set(iface_buffer_t a_iface_buffer, void *a_psuedo_hdr, natural_t a_phdr_len);

void *tcp_input(iface_t a_iface, iface_buffer_t a_iface_buffer);

void *tcp_output(iface_t a_iface, iface_buffer_t a_iface_buffer);

void tcp_pulse(net_proto_t, tcb_t a_tcb, natural_t a_ms_elapsed);

kern_return_t tcp_usr_request(tcb_t, net_usr_req_op_t, ...);
