#include "domain.h"

#include <emerixx/arpa/inet.h>
#include <emerixx/net/arp.h>
#include <emerixx/net/ethernet.h>
#include <emerixx/net/if.h>
#include <emerixx/ioctl_req.h>
#include <emerixx/net/route.h>
#include <etl/algorithm.hh>
#include <etl/queue.h>
#include <kernel/lock.h>
#include <kernel/sched_prim.h>
#include <string.h>

#include "protocol.h"

#include "ipv4.h"
#include "socket.h"
#include "tcp.h"

#define SATOSIN(sa) ((sockaddr_in *)(sa))

#define INET_TCP_CTX(so) ((tcp_ctx_t)((so)->m_proto_private))

#define INET_TCP_CTX_Q_REC(addr, link) (queue_containing_record((addr), tcp_ctx_data, link))

#define INET_TCP_CTX_FIRST()     (INET_TCP_CTX_Q_REC(queue_first(&s_inet_tcp_all_sock), m_allsock_link))
#define INET_TCP_CTX_NEXT(ctx)   (INET_TCP_CTX_Q_REC(queue_next(&(ctx)->m_allsock_link), m_allsock_link))
#define INET_TCP_CTX_PREV(ctx)   (INET_TCP_CTX_Q_REC(queue_prev(&(ctx)->m_allsock_link), m_allsock_link))
#define INET_TCP_CTX_END(ctx)    (queue_end(&s_inet_tcp_all_sock, &(ctx)->m_allsock_link))
#define INET_TCP_CTX_ENQUE(tctx) (enqueue(&s_inet_tcp_all_sock, &(tctx)->m_allsock_link))

#define INET_FREE_CTX_FIRST()     (INET_TCP_CTX_Q_REC(queue_first(&s_inet_tcp_del_sock), m_allsock_link))
#define INET_FREE_CTX_NEXT(ctx)   (INET_TCP_CTX_Q_REC(queue_next(&(ctx)->m_allsock_link), m_allsock_link))
#define INET_FREE_CTX_PREV(ctx)   (INET_TCP_CTX_Q_REC(queue_prev(&(ctx)->m_allsock_link), m_allsock_link))
#define INET_FREE_CTX_END(ctx)    (queue_end(&s_inet_tcp_del_sock, &(ctx)->m_allsock_link))
#define INET_FREE_CTX_ENQUE(tctx) (enqueue(&s_inet_tcp_del_sock, &(tctx)->m_allsock_link))
#define INET_FREE_CTX_DEQUE()     (INET_TCP_CTX_Q_REC(dequeue(&s_inet_tcp_del_sock), m_allsock_link))
#define INET_FREE_CTX_EMPTY()     (queue_empty(&s_inet_tcp_del_sock))

#define INET_USR_REQ_ENQUEUE(tctx, req) (enqueue(&(tctx)->m_usr_req_queue, &(req)->m_link))
#define INET_USR_REQ_DEQUEUE(tctx)      (queue_containing_record(dequeue(&(tctx)->m_usr_req_queue), inet_usr_req, m_link))

/*
 * Interface address, Internet version.  One of these structures
 * is allocated for each interface with an Internet address.
 * The ifaddr structure contains the protocol-independent part
 * of the structure and is assumed to be first.
 */
struct in_ifaddr
{
        struct ifaddr ia_ifa; /* protocol-independent info */
#define ia_addr      ia_ifa.ifa_addr
#define ia_broadaddr ia_ifa.ifa_broadaddr
#define ia_dstaddr   ia_ifa.ifa_dstaddr
#define ia_ifp       ia_ifa.ifa_ifp
        struct in_addr ia_net;          /* network number of interface */
        struct in_addr ia_netmask;      /* mask of net part */
        struct in_addr ia_subnet;       /* subnet number, including net */
        struct in_addr ia_subnetmask;   /* mask of net + subnet */
        struct in_addr ia_netbroadcast; /* broadcast addr for (logical) net */
        int ia_flags;
        struct in_ifaddr *ia_next; /* next in list of internet addresses */
};

typedef struct tcp_ctx_data *tcp_ctx_t;
typedef struct inet_usr_req *inet_usr_req_t;

enum usr_send_req_ctx_state : natural_t
{
        USR_SEND_CTX_STATE_BUFFER_ALLOC,
        USR_SEND_CTX_STATE_BUFFER_FREE,
        USR_SEND_CTX_STATE_BUFFER_SEND
};

struct usr_send_req_ctx
{
        queue_head_t m_buffers;
        natural_t m_send_len;
        usr_send_req_ctx_state m_state;
};

enum usr_recv_req_ctx_state : natural_t
{
        USR_RECV_CTX_STATE_BUFFER_GET,
        USR_RECV_CTX_STATE_BUFFER_PUT
};

struct usr_recv_req_ctx
{
        iface_buffer_t m_buffer;
        natural_t m_count;
        usr_recv_req_ctx_state m_state;
};

struct tcp_ctx_data
{
        rbtnode_data m_link;          // Bound link
        queue_chain_t m_allsock_link; // A link for a queue of all sockets
        lock_data_t m_usr_req_lock;
        queue_head_t m_usr_req_queue;
        natural_t m_outbuf_count;
        queue_head_t m_user_buffer_out; // Outgoing user buffers (net -> user)
        queue_head_t m_user_buffer_in;  // Incoming user buffers (user -> net)
        int32_t m_refcount;
        int32_t m_flags;
        route m_route;

        usr_recv_req_ctx m_rcv_ctx;

        socket_t m_so;
        tcb_data m_tcb;
};

struct inet_usr_req
{
        queue_chain_t m_link;
        natural_t m_arg;
        net_usr_req_op_t m_op;
        kern_return_t m_retval;
};

/*static*/ void *inet_tcp_input(iface_t, iface_buffer_t);
static void *inet_tcp_output(iface_t, iface_buffer_t);
static kern_return_t inet_tcp_init(struct socket *a_so);
static kern_return_t inet_tcp_user_request(struct socket *a_so, net_usr_req_op_t a_op, ...);
static void inet_tcp_timeout(natural_t a_ms_elapsed);

static net_proto_data s_inet_proto[] = {
    //
    {
        //
        .m_domain             = nullptr,
        .m_type               = SOCK_STREAM,
        .m_protocol           = 0,
        .m_private_data_size  = sizeof(tcp_ctx_data),
        .m_private_data_align = alignof(tcp_ctx_data),
        .m_input              = inet_tcp_input,
        .m_output             = inet_tcp_output,
        .m_proto_init         = inet_tcp_init,
        .m_user_request       = inet_tcp_user_request,
        .m_timeout            = inet_tcp_timeout,
        //
    }
    //
};

static net_domain_data s_inet_domain = {
    //
    .m_domain         = AF_INET,
    .m_sockaddr_size  = sizeof(sockaddr_in),
    .m_sockaddr_align = alignof(sockaddr_in),
    .m_link           = {},
    .m_proto_beg      = s_inet_proto,
    .m_proto_end      = &s_inet_proto[ARRAY_SIZE(s_inet_proto)]
    //
};

static struct in_ifaddr s_ina[2] = {
    //
    {
        .ia_ifa          = {},
        .ia_net          = {.s_addr = 0},
        .ia_netmask      = {.s_addr = 0},
        .ia_subnet       = {.s_addr = 0},
        .ia_subnetmask   = {.s_addr = 0},
        .ia_netbroadcast = {.s_addr = 0},
        .ia_flags        = 0,
        .ia_next         = nullptr
        //
    },
    //
    {
        .ia_ifa          = {},
        .ia_net          = {.s_addr = 0},
        .ia_netmask      = {.s_addr = 0},
        .ia_subnet       = {.s_addr = 0},
        .ia_subnetmask   = {.s_addr = 0},
        .ia_netbroadcast = {.s_addr = 0},
        .ia_flags        = 0,
        .ia_next         = nullptr
        //
    }
    //
};

static in_ifaddr *s_in_ifaddr;

static void inet_tcp_can_accept(tcb_t, int);
static bool inet_tcp_src_dst_equal(tcb_t, iface_buffer_t, iface_buffer_t);
static kern_return_t inet_tcp_proto_connect(tcb_t a_tcb, iface_buffer_t a_ifbuf);
static void inet_tcp_enq_out_network(tcb_t, iface_buffer_t);
static kern_return_t inet_tcp_enq_out_user(tcb_t, iface_buffer_t);
static void inet_tcp_state_change(tcb_t a_tcb, enum TCP_CONNECTION_STATE a_new_conn_state);

static tcb_proto_ops s_tcp_proto_ops = {
    //
    .accept_ready    = inet_tcp_can_accept,
    .src_dest_equal  = inet_tcp_src_dst_equal,
    .proto_connect   = inet_tcp_proto_connect,
    .enq_out_network = inet_tcp_enq_out_network,
    .enq_out_user    = inet_tcp_enq_out_user,
    .state_change    = inet_tcp_state_change
    //
};

static lock_data_t s_inet_tcp_all_sock_lock;
static queue_head_t s_inet_tcp_all_sock;
static queue_head_t s_inet_tcp_del_sock;

#define TCTX_CAN_LOOKUP BIT0

// ********************************************************************************************************************************
static tcp_ctx_t in_pcblookup(sockaddr_in *a_faddr, sockaddr_in *a_laddr, bool a_wildcard)
// ********************************************************************************************************************************
{
        socket_t so, match = nullptr;
        int32_t matchwild = 3, wildcard;

        for (tcp_ctx_t tctx = INET_TCP_CTX_FIRST(); INET_TCP_CTX_END(tctx) == false; tctx = INET_TCP_CTX_NEXT(tctx))
        {
                if ((tctx->m_flags & TCTX_CAN_LOOKUP) == 0)
                {
                        continue;
                }

                so = tctx->m_so;

                if (SATOSIN(so->m_sa)->sin_port != a_laddr->sin_port)
                {
                        // Ignore if local ports unuqual
                        continue;
                }

                wildcard = 0;

                if (SATOSIN(so->m_sa)->sin_addr.s_addr != INADDR_ANY)
                {
                        if (a_laddr->sin_addr.s_addr == INADDR_ANY)
                        {
                                wildcard++;
                        }
                        else if (SATOSIN(so->m_sa)->sin_addr.s_addr != a_laddr->sin_addr.s_addr)
                        {
                                continue;
                        }
                }
                else
                {
                        if (a_laddr->sin_addr.s_addr != INADDR_ANY)
                        {
                                wildcard++;
                        }
                }

                if (SATOSIN(so->m_peer_sa)->sin_addr.s_addr != INADDR_ANY)
                {
                        if (a_faddr->sin_addr.s_addr == INADDR_ANY)
                        {
                                wildcard++;
                        }
                        else if ((SATOSIN(so->m_peer_sa)->sin_addr.s_addr != a_faddr->sin_addr.s_addr)
                                 || (SATOSIN(so->m_peer_sa)->sin_port != a_faddr->sin_port))
                        {
                                continue;
                        }
                }
                else
                {
                        if (a_faddr->sin_addr.s_addr != INADDR_ANY)
                        {
                                wildcard++;
                        }
                }

                if (wildcard && (a_wildcard == false))
                {
                        // Wildcard match not allowed
                        continue;
                }

                if (wildcard < matchwild)
                {
                        match     = tctx->m_so;
                        matchwild = wildcard;
                        if (matchwild == 0)
                        {
                                // Exact match
                                break;
                        }
                }
        }

        return match ? INET_TCP_CTX(match) : nullptr;
}

/*
 * Return the network number from an internet address.
 */
// ********************************************************************************************************************************
static uint32_t in_netof(in_addr in)
// ********************************************************************************************************************************
{
        uint32_t i   = ntohl(in.s_addr);
        uint32_t net = 0;
        struct in_ifaddr *ia;

        if (IN_CLASSA(i))
        {
                net = i & IN_CLASSA_NET;
        }
        else if (IN_CLASSB(i))
        {
                net = i & IN_CLASSB_NET;
        }
        else if (IN_CLASSC(i))
        {
                net = i & IN_CLASSC_NET;
        }
        else if (IN_CLASSD(i))
        {
                net = i & /*IN_CLASSD_NET*/ 0xf0000000;
        }

        /*
         * Check whether network is a subnet;
         * if so, return subnet number.
         */
        for (ia = s_in_ifaddr; ia; ia = ia->ia_next)
        {
                if (net == ia->ia_net.s_addr)
                {
                        return (i & ia->ia_subnetmask.s_addr);
                }
        }
        return net;
}

/*
 * Formulate an Internet address from network + host.
 */
// ********************************************************************************************************************************
static struct in_addr in_makeaddr(in_addr_t net, in_addr_t host)
// ********************************************************************************************************************************
{
        in_addr_t mask;

        if (IN_CLASSA(net))
        {
                mask = IN_CLASSA_HOST;
        }
        else if (IN_CLASSB(net))
        {
                mask = IN_CLASSB_HOST;
        }
        else if (IN_CLASSC(net))
        {
                mask = IN_CLASSC_HOST;
        }
        else
        {
                panic("in_makeaddr: bad net 0x%08x", net);
        }

        for (struct in_ifaddr *ia = s_in_ifaddr; ia; ia = ia->ia_next)
        {
                if ((ia->ia_netmask.s_addr & net) == ia->ia_net.s_addr)
                {
                        mask = ~ia->ia_subnetmask.s_addr;
                        break;
                }
        }

        return {.s_addr = htonl(net | (host & mask))};
}

/*
 *Return address info for specified internet network.
 */
static in_ifaddr *in_iaonnetof(uint32_t net)
{
        for (struct in_ifaddr *ia = s_in_ifaddr; ia; ia = ia->ia_next)
        {
                if (ia->ia_subnet.s_addr == net)
                {
                        return (ia);
                }
        }
        return nullptr;
}

/*
 * Return 1 if the address is a local broadcast address.
 */
int in_broadcast(struct in_addr in)
{
        /*
         * Look through the list of addresses for a match
         * with a broadcast address.
         */
        for (struct in_ifaddr *ia = s_in_ifaddr; ia; ia = ia->ia_next)
        {
                if ((SATOSIN(&ia->ia_broadaddr)->sin_addr.s_addr == in.s_addr) && (ia->ia_ifp->if_flags & IFF_BROADCAST))
                {
                        return true;
                }
        }
        return false;
}

// ********************************************************************************************************************************
static void inet_hash(sockaddr_t sin, struct afhash *hp)
// ********************************************************************************************************************************
{
        uint32_t n = in_netof(SATOSIN(sin)->sin_addr);
        if (n)
        {
                while ((n & 0xff) == 0)
                {
                        n >>= 8;
                }
        }

        hp->afh_nethash  = n;
        hp->afh_hosthash = ntohl(SATOSIN(sin)->sin_addr.s_addr);
}

// ********************************************************************************************************************************
static int inet_netmatch(sockaddr_t sin1, sockaddr_t sin2)
// ********************************************************************************************************************************
{
        return (in_netof(SATOSIN(sin1)->sin_addr) == in_netof(SATOSIN(sin2)->sin_addr));
}

// ********************************************************************************************************************************
static int inet_equal(sockaddr_t a_addr1, sockaddr_t a_addr2)
// ********************************************************************************************************************************
{
        return SATOSIN(a_addr1)->sin_addr.s_addr == SATOSIN(a_addr2)->sin_addr.s_addr;
}

int ___nbufout;
int ___nbufout_def;
int ___nbufin;

#define INET_TCP_USR_INCOMING_DEQ(tctx)       (queue_containing_record(dequeue(&(tctx)->m_user_buffer_in), iface_buffer, m_dev_link))
#define INET_TCP_USR_INCOMING_ENQ(tctx, ibuf) (enqueue(&(tctx)->m_user_buffer_in, &(ibuf)->m_dev_link))
#define INET_TCP_USR_INCOMING_EMPTY(tctx)     (queue_empty(&(tctx)->m_user_buffer_in))

#define INET_TCP_USR_OUTGOING_DEQ(tctx)       (queue_containing_record(dequeue(&(tctx)->m_user_buffer_out), iface_buffer, m_dev_link))
#define INET_TCP_USR_OUTGOING_ENQ(tctx, ibuf) (enqueue(&(tctx)->m_user_buffer_out, &(ibuf)->m_dev_link))
#define INET_TCP_USR_OUTGOING_EMPTY(tctx)     (queue_empty(&(tctx)->m_user_buffer_out))
#define INET_TCP_USR_OUTGOING_FIRST(tctx)                                                                                          \
        (queue_containing_record(queue_first(&(tctx)->m_user_buffer_out), iface_buffer, m_dev_link))

// ********************************************************************************************************************************
static void inet_tcp_pump_output(tcp_ctx_t a_tctx)
// ********************************************************************************************************************************
{
        while (INET_TCP_USR_INCOMING_EMPTY(a_tctx) == false)
        {
                iface_buffer_t ibuf = INET_TCP_USR_INCOMING_DEQ(a_tctx);
                ___nbufin--;
                for (protocol_op_t inf = inet_tcp_output; inf != nullptr;)
                {
                        inf = (protocol_op_t)inf(ibuf->m_iface, ibuf);
                }
        }
}

static natural_t s_outport_counter;

// ********************************************************************************************************************************
static uint16_t port_alloc()
// ********************************************************************************************************************************
{
        sockaddr_in sa[] = {
            //
            {.sin_family = AF_INET, .sin_port = 0, .sin_addr = {}},
            {.sin_family = AF_INET, .sin_port = (htons((0x4000 + ++s_outport_counter) & 0xFFFF)), .sin_addr = {}}
            //
        };

        while (in_pcblookup(&sa[0], &sa[1], false) != nullptr)
        {
                sa[1].sin_port = (htons((0x4000 + ++s_outport_counter) & 0xFFFF));
        }

        return sa[1].sin_port;
}

static void inet_tcp_tctx_init(tcp_ctx_t a_tctx, socket_t a_so)
{
        a_tctx->m_link         = {};
        a_tctx->m_allsock_link = {};
        lock_init(&a_tctx->m_usr_req_lock, true);
        queue_init(&a_tctx->m_usr_req_queue);
        a_tctx->m_so = a_so;

        queue_init(&a_tctx->m_user_buffer_out);
        queue_init(&a_tctx->m_user_buffer_in);

        *((sockaddr_in *)a_so->m_sa)      = {};
        *((sockaddr_in *)a_so->m_peer_sa) = {};

        a_tctx->m_outbuf_count = 0;
        tcp_tcb_init(&a_tctx->m_tcb, &s_tcp_proto_ops);

        a_tctx->m_refcount = 1;
        a_tctx->m_flags    = 0;
        a_tctx->m_route    = {};

        a_tctx->m_rcv_ctx = {.m_buffer = nullptr, .m_count = 0};
}

// ********************************************************************************************************************************
kern_return_t inet_tcp_init(socket_t a_so)
// ********************************************************************************************************************************
{
        tcp_ctx_t tctx = INET_TCP_CTX(a_so);

        inet_tcp_tctx_init(tctx, a_so);

        lock_write(&s_inet_tcp_all_sock_lock);
        INET_TCP_CTX_ENQUE(tctx);
        lock_write_done(&s_inet_tcp_all_sock_lock);
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
void *inet_tcp_input(iface_t a_if, iface_buffer_t a_ifbuf)
// ********************************************************************************************************************************
{
        a_ifbuf->m_proto = &s_inet_proto[0];

        switch (a_ifbuf->m_layer_curr)
        {
        case NETSTACK_LAYER_LINK:
                if (___nbufout >= (int)(iface_buffer_available_count() - 0x20))
                {
                        log_debug("Out of buffers!");
                        return NETSTACK_CALL(iface_buffer_free);
                }

                a_ifbuf->m_layer_curr                         = NETSTACK_LAYER_NETWORK;
                a_ifbuf->m_layer_data[NETSTACK_LAYER_NETWORK] = a_ifbuf->m_virt_addr + a_ifbuf->m_offset;
                return NETSTACK_CALL(ipv4_input);
        case NETSTACK_LAYER_NETWORK: {
                a_ifbuf->m_layer_curr                           = NETSTACK_LAYER_TRANSPORT;
                a_ifbuf->m_layer_data[NETSTACK_LAYER_TRANSPORT] = a_ifbuf->m_virt_addr + a_ifbuf->m_offset;

                if (tcp_csum_valid(a_ifbuf) == false)
                {
                        log_debug("Dropping tcp packet, invalid CSUM");
                        return NETSTACK_CALL(iface_buffer_free);
                }

                in_port_t iop[2];
                tcp_get_ports(a_ifbuf, iop);

                in_addr iaddr[2];
                ipv4_get_addr(a_ifbuf, iaddr);

                sockaddr_in laddr = {.sin_family = AF_INET, .sin_port = iop[1], .sin_addr = iaddr[1]};
                sockaddr_in faddr = {.sin_family = AF_INET, .sin_port = iop[0], .sin_addr = iaddr[0]};

                tcp_ctx_t tctx = in_pcblookup(&faddr, &laddr, true);
                if (tctx != nullptr)
                {
                        a_ifbuf->m_layer_pcb[NETSTACK_LAYER_TRANSPORT] = &tctx->m_tcb;
                        return NETSTACK_CALL(tcp_input);
                }

                // No sock found
                log_debug("Dropping tcp packet, no listen socket");
                return NETSTACK_CALL(iface_buffer_free);
        }
        case NETSTACK_LAYER_TRANSPORT: {
                tcp_ctx_t tctx
                    = (tcp_ctx_t)(((vm_address_t)(a_ifbuf->m_layer_pcb[a_ifbuf->m_layer_curr])) - offsetof(tcp_ctx_data, m_tcb));
                a_ifbuf->m_layer_curr                      = NETSTACK_LAYER_USER;
                a_ifbuf->m_layer_data[NETSTACK_LAYER_USER] = a_ifbuf->m_virt_addr + a_ifbuf->m_offset;
                a_ifbuf->m_layer_pcb[NETSTACK_LAYER_USER]  = tctx;

                inet_tcp_pump_output(tctx);
                enqueue(&tctx->m_user_buffer_out, &a_ifbuf->m_dev_link);
                tctx->m_outbuf_count++;
                ___nbufout++;
                break;
        }
        default:
                panic("should not happen %d", a_ifbuf->m_layer_curr);
                break;
        }

        return nullptr;
}

struct tcp_pseudo_hdr
{
        uint32_t saddr;
        uint32_t daddr;
        uint8_t resv_msb;
        uint8_t proto;
        uint16_t tcp_seg_len;
};

// ********************************************************************************************************************************
void *inet_tcp_output(iface_t a_if, iface_buffer_t a_ifbuf)
// ********************************************************************************************************************************
{
        switch (a_ifbuf->m_layer_curr)
        {
        case NETSTACK_LAYER_USER: {
                tcp_ctx_t tctx = (tcp_ctx_t)a_ifbuf->m_layer_pcb[a_ifbuf->m_layer_curr];

                if (a_ifbuf->m_flags & IFACE_BUFFER_FL_REFLECT)
                {
                        a_ifbuf->m_layer_curr = NETSTACK_LAYER_TRANSPORT;
                        return NETSTACK_CALL(tcp_output);
                }

                a_ifbuf->m_layer_curr                       = NETSTACK_LAYER_TRANSPORT;
                a_ifbuf->m_layer_pcb[a_ifbuf->m_layer_curr] = &tctx->m_tcb;

                a_ifbuf->m_layer_data[a_ifbuf->m_layer_curr]
                    = a_ifbuf->m_virt_addr + (a_ifbuf->m_size - (a_ifbuf->m_offset + sizeof(in_port_t[2])));

                in_port_t *inp = (in_port_t *)a_ifbuf->m_layer_data[a_ifbuf->m_layer_curr];
                inp[0]         = SATOSIN(tctx->m_so->m_sa)->sin_port;
                inp[1]         = SATOSIN(tctx->m_so->m_peer_sa)->sin_port;

                return NETSTACK_CALL(tcp_output);
        }
        case NETSTACK_LAYER_TRANSPORT: {
                if (a_ifbuf->m_flags & IFACE_BUFFER_FL_REFLECT)
                {
                        a_ifbuf->m_layer_curr = NETSTACK_LAYER_NETWORK;
                        return NETSTACK_CALL(ipv4_output);
                }

                tcp_ctx_t tctx = (tcp_ctx_t)a_ifbuf->m_layer_pcb[NETSTACK_LAYER_USER];

                a_ifbuf->m_payload_len[NETSTACK_LAYER_TRANSPORT]
                    = a_ifbuf->m_layer_data[NETSTACK_LAYER_USER] - a_ifbuf->m_layer_data[NETSTACK_LAYER_TRANSPORT];

                a_ifbuf->m_packet_size += a_ifbuf->m_payload_len[NETSTACK_LAYER_TRANSPORT];

                a_ifbuf->m_layer_curr = NETSTACK_LAYER_NETWORK;
                a_ifbuf->m_layer_data[a_ifbuf->m_layer_curr]
                    = a_ifbuf->m_virt_addr + (a_ifbuf->m_size - (a_ifbuf->m_offset + sizeof(in_addr[2])));

                in_addr *ina = (in_addr *)a_ifbuf->m_layer_data[a_ifbuf->m_layer_curr];
                ina[0]       = SATOSIN(tctx->m_so->m_sa)->sin_addr;
                ina[1]       = SATOSIN(tctx->m_so->m_peer_sa)->sin_addr;

                return NETSTACK_CALL(ipv4_output);
        }
        case NETSTACK_LAYER_NETWORK: {
                if (a_ifbuf->m_flags & IFACE_BUFFER_FL_REFLECT)
                {
                        a_ifbuf->m_layer_curr = NETSTACK_LAYER_LINK;
                        return NETSTACK_CALL(a_if->output);
                }

                a_ifbuf->m_payload_len[NETSTACK_LAYER_NETWORK]
                    = a_ifbuf->m_layer_data[NETSTACK_LAYER_TRANSPORT] - a_ifbuf->m_layer_data[NETSTACK_LAYER_NETWORK];

                a_ifbuf->m_packet_size += a_ifbuf->m_payload_len[NETSTACK_LAYER_NETWORK];

                a_ifbuf->m_layer_curr = NETSTACK_LAYER_LINK;
                a_ifbuf->m_layer_data[NETSTACK_LAYER_LINK]
                    = a_ifbuf->m_virt_addr + (a_ifbuf->m_size - a_ifbuf->m_offset - sizeof(ether_header));

                ether_header *ehdr = (ether_header *)a_ifbuf->m_layer_data[a_ifbuf->m_layer_curr];

                *ehdr            = {};
                ehdr->ether_type = htons(ETH_P_IP);

                tcp_ctx_t tctx = (tcp_ctx_t)a_ifbuf->m_layer_pcb[NETSTACK_LAYER_USER];

                KASSERT(tctx->m_route.ro_rt != nullptr);
                KASSERT(tctx->m_route.ro_rt->rt_ifp != nullptr);

                if (arp_resolve_ex(&tctx->m_route, ehdr) == false)
                {
                        panic("arp_resolve_ex failed, should hold the buffer until arp_resolve_ex is done");
                }

                return NETSTACK_CALL(a_if->output);
        }
        case NETSTACK_LAYER_LINK: {
                if (a_ifbuf->m_flags & IFACE_BUFFER_FL_REFLECT)
                {
                        a_ifbuf->m_layer_curr = NETSTACK_LAYER_UNUSED;
                        return NETSTACK_CALL(iface_buffer_send);
                }

                a_ifbuf->m_payload_len[NETSTACK_LAYER_LINK]
                    = a_ifbuf->m_layer_data[NETSTACK_LAYER_NETWORK] - a_ifbuf->m_layer_data[NETSTACK_LAYER_LINK];

                a_ifbuf->m_packet_size += a_ifbuf->m_payload_len[NETSTACK_LAYER_LINK];

                a_ifbuf->m_layer_curr = NETSTACK_LAYER_UNUSED;

                in_addr iaddr[2];
                ipv4_get_addr(a_ifbuf, iaddr);

                tcp_pseudo_hdr tph = {
                    //
                    .saddr    = iaddr[0].s_addr,
                    .daddr    = iaddr[1].s_addr,
                    .resv_msb = 0,
                    .proto    = IPPROTO_TCP,
                    .tcp_seg_len
                    = htons(a_ifbuf->m_payload_len[NETSTACK_LAYER_USER] + a_ifbuf->m_payload_len[NETSTACK_LAYER_TRANSPORT])
                    //
                };

                tcp_csum_set(a_ifbuf, &tph, sizeof(tph));

                ipv4_csum_set(a_ifbuf);
                return NETSTACK_CALL(iface_buffer_send);
        }
        default:
                panic("should not happen %d", a_ifbuf->m_layer_curr);
                break;
        }

        return nullptr;
}

static kern_return_t inet_tcp_send_usr_req(tcp_ctx_t a_tctx, net_usr_req_op_t a_op, void *a_arg)
{
        inet_usr_req ureq = {.m_link = {}, .m_arg = (natural_t)a_arg, .m_op = a_op};
        lock_write(&a_tctx->m_usr_req_lock);
        INET_USR_REQ_ENQUEUE(a_tctx, &ureq);
        assert_wait(&ureq, false);
        lock_write_done(&a_tctx->m_usr_req_lock);
        iface_network_wakeup();
        thread_block(nullptr);
        return ureq.m_retval;
}

static kern_return_t inet_tcp_alloc_user_buffer(tcp_ctx_t a_tctx, iface_buffer_t *a_ibuf)
{
        kern_return_t retval = iface_buffer_alloc(nullptr, a_ibuf);
        if (KERN_STATUS_SUCCESS(retval))
        {
                (*a_ibuf)->m_layer_curr                         = NETSTACK_LAYER_USER;
                (*a_ibuf)->m_layer_pcb[(*a_ibuf)->m_layer_curr] = a_tctx;
                (*a_ibuf)->m_proto                              = &s_inet_proto[0];
                (*a_ibuf)->m_offset                             = 32;
                (*a_ibuf)->m_packet_size                        = 0;
                (*a_ibuf)->m_layer_data[(*a_ibuf)->m_layer_curr]
                    = (*a_ibuf)->m_virt_addr + ((*a_ibuf)->m_size - (*a_ibuf)->m_offset);
                (*a_ibuf)->m_payload_len[(*a_ibuf)->m_layer_curr] = 0;
        }

        return retval;
}

#include <mach/param.h> // ALIGN(...)

// ********************************************************************************************************************************
kern_return_t inet_tcp_user_request(socket_t a_so, net_usr_req_op_t a_op, ...)
// ********************************************************************************************************************************
{
        tcp_ctx_t tctx = INET_TCP_CTX(a_so);

        va_list ap;
        void *ureq_arg = nullptr;
        va_start(ap, a_op);
        switch (a_op)
        {
        case NET_USR_REQ_BIND: {
                ureq_arg = va_arg(ap, sockaddr_in *);

// Check if there are any available interfaces
#if 0
                if (in_ifaddr == 0)
                {
                        return (EADDRNOTAVAIL);
                }
#endif

                if ((SATOSIN(a_so->m_sa)->sin_port != 0) || (SATOSIN(a_so->m_sa)->sin_addr.s_addr != INADDR_ANY))
                {
                        // Already bound
                        va_end(ap);
                        return KERN_FAIL(EINVAL);
                }

                break;
        }

        case NET_USR_REQ_LISTEN: {
                // integer_t
                ureq_arg = va_arg(ap, void *);
                break;
        }

        case NET_USR_REQ_ACCEPT: {
                ureq_arg = va_arg(ap, socket_t);
                break;
        }
        case NET_USR_REQ_SHUTDOWN: {
                ureq_arg = (void *)va_arg(ap, int); // SHUT_?
                break;
        }

        case NET_USR_REQ_SEND: {
                usr_send_req_ctx snd_ctx;
                queue_init(&snd_ctx.m_buffers);
                snd_ctx.m_state    = USR_SEND_CTX_STATE_BUFFER_ALLOC;
                snd_ctx.m_send_len = va_arg(ap, natural_t);
                proto_rw_cb_t cb   = va_arg(ap, proto_rw_cb_t);
                void *ud           = va_arg(ap, void *);
                va_end(ap);

                kern_return_t retval = inet_tcp_send_usr_req(tctx, NET_USR_REQ_SEND, &snd_ctx);

                if (KERN_STATUS_FAILURE(retval))
                {
                        return retval;
                }

                natural_t osend_len = snd_ctx.m_send_len;
                for (queue_entry_t elt = queue_first(&snd_ctx.m_buffers); //
                     queue_end(&snd_ctx.m_buffers, elt) == false;
                     elt = queue_next(elt))
                {
                        iface_buffer_t ibuf = queue_containing_record(elt, iface_buffer, m_dev_link);

                        natural_t databuf_len = etl::min(1460ul /*MSS*/ - ibuf->m_offset, snd_ctx.m_send_len);

                        ibuf->m_offset += databuf_len;

                        ibuf->m_offset = ALIGN(ibuf->m_offset, alignof(uint32_t));

                        natural_t databuf = ibuf->m_virt_addr + (ibuf->m_size - ibuf->m_offset);

                        retval = cb(a_so, (void *)databuf, databuf_len, ud);

                        if (KERN_STATUS_FAILURE(retval))
                        {
                                // Buffer copy failed, free everything
                                snd_ctx.m_state = USR_SEND_CTX_STATE_BUFFER_FREE;

                                retval = inet_tcp_send_usr_req(tctx, NET_USR_REQ_SEND, &snd_ctx);

                                return retval;
                        }

                        KASSERT(databuf_len == ((natural_t)retval));

                        snd_ctx.m_send_len -= databuf_len;

                        if (snd_ctx.m_send_len == 0)
                        {
                                ibuf->m_flags |= IFACE_BUFFER_FL_PUSH_USER;
                        }

                        ibuf->m_payload_len[ibuf->m_layer_curr] = databuf_len;
                        ibuf->m_packet_size += databuf_len;
                        ibuf->m_layer_data[ibuf->m_layer_curr] = databuf;
                }

                // TODO: set push flag on last buffer when snd_ctx.m_send_len == 0
                snd_ctx.m_state = USR_SEND_CTX_STATE_BUFFER_SEND;

                inet_tcp_send_usr_req(tctx, NET_USR_REQ_SEND, &snd_ctx);

                return (kern_return_t)(osend_len - snd_ctx.m_send_len);
        }

        case NET_USR_REQ_RECV: {
                int *push_ptr    = va_arg(ap, int *);
                proto_rw_cb_t cb = va_arg(ap, proto_rw_cb_t);
                void *ud         = va_arg(ap, void *);
                va_end(ap);
                kern_return_t retval;
                if (tctx->m_rcv_ctx.m_count == 0)
                {
                        retval = inet_tcp_send_usr_req(tctx, NET_USR_REQ_RECV, &tctx->m_rcv_ctx);

                        if (KERN_STATUS_FAILURE(retval))
                        {
                                if (retval == -1)
                                {
                                        retval = 0;
                                }

                                return retval;
                        }
                }

                kern_return_t totlen = 0;
                while (tctx->m_rcv_ctx.m_count)
                {
                        while (tctx->m_rcv_ctx.m_buffer->m_payload_len[NETSTACK_LAYER_USER] > 0)
                        {
                                char *data        = ((char *)tctx->m_rcv_ctx.m_buffer->m_layer_data[NETSTACK_LAYER_USER]);
                                natural_t datalen = tctx->m_rcv_ctx.m_buffer->m_payload_len[NETSTACK_LAYER_USER];

                                retval = cb(a_so, data, datalen, ud);

                                if (KERN_STATUS_FAILURE(retval))
                                {
                                        return retval;
                                }

                                if (retval == 0)
                                {
                                        if (totlen)
                                        {
                                                *push_ptr = !!(tctx->m_rcv_ctx.m_buffer->m_flags & IFACE_BUFFER_FL_PUSH_USER);
                                                return totlen;
                                        }
                                        else
                                        {
                                                return KERN_FAIL(EAGAIN);
                                        }
                                }

                                tctx->m_rcv_ctx.m_buffer->m_layer_data[NETSTACK_LAYER_USER] += retval;
                                tctx->m_rcv_ctx.m_buffer->m_payload_len[NETSTACK_LAYER_USER] -= retval;
                                totlen += retval;
                        }

                        if (tctx->m_rcv_ctx.m_buffer->m_payload_len[NETSTACK_LAYER_USER] == 0)
                        {
                                *push_ptr = !!(tctx->m_rcv_ctx.m_buffer->m_flags & IFACE_BUFFER_FL_PUSH_USER);

                                tctx->m_rcv_ctx.m_count--;

                                if (tctx->m_rcv_ctx.m_count > 0)
                                {
                                        tctx->m_rcv_ctx.m_buffer = queue_containing_record(
                                            queue_next(&tctx->m_rcv_ctx.m_buffer->m_dev_link), iface_buffer, m_dev_link);
                                }
                                else
                                {
                                        tctx->m_rcv_ctx.m_buffer = nullptr;
                                }

                                if (*push_ptr)
                                {
                                        break;
                                }
                        }
                }

                if (totlen == 0)
                {
                        totlen = KERN_FAIL(EAGAIN);
                }
                return kern_return_t(totlen);
        }

        case NET_USR_REQ_CONNECT: {
                ureq_arg = va_arg(ap, sockaddr_in *);

                if (SATOSIN(ureq_arg)->sin_port == 0)
                {
                        va_end(ap);
                        return KERN_FAIL(EADDRNOTAVAIL);
                }
#if 0
                if (in_ifaddr)
                {
                        /*
                         If the foreign IP address is 0.0.0.0 (INADDR_ANY), then 0.0.0.0 is replaced with the IP address of the primary IP
                         interface. This means the calling process is connecting to a peer on this host. If the foreign IP address
                         is 255.255.255.255 (INADDR_BROADCAST) and the primary interface supports broadcasting, then
                         255.255.255.255 is replaced with the broadcast address of the primary interface. This allows a UDP
                         application to broadcast on the primary interface without having to figure out its IP address i t can
                         simply send datagrams to 255.255.255.255, and the kernel converts this to the appropriate IP address
                         for the interface.
                        */

                        /*
                         * If the destination address is INADDR_ANY,
                         * use the primary local address.
                         * If the supplied address is INADDR_BROADCAST,
                         * and the primary interface supports broadcast,
                         * choose the broadcast address for that interface.
                         */
#define satosin(sa) ((struct sockaddr_in *)(sa))
                        if (sin->sin_addr.s_addr == INADDR_ANY)
                                sin->sin_addr = IA_SIN(in_ifaddr)->sin_addr;
                        else if (sin->sin_addr.s_addr == (u_long)INADDR_BROADCAST && (in_ifaddr->ia_ifp->if_flags & IFF_BROADCAST))
                                sin->sin_addr = satosin(&in_ifaddr->ia_broadaddr)->sin_addr;
                }
#endif
                break;
        }
        case NET_USR_REQ_USER:

        case NET_USR_REQ_CLOSE: {

                // Just forward
                break;
        }

        default:
                va_end(ap);
                return KERN_FAIL(EOPNOTSUPP);
        }
        va_end(ap);

        return inet_tcp_send_usr_req(tctx, a_op, ureq_arg);
}

#define INET_TCB_TO_TCP_CTX(tcb) ((tcp_ctx_t)(((vm_address_t)(tcb)) - offsetof(tcp_ctx_data, m_tcb)))

// ********************************************************************************************************************************
void inet_tcp_can_accept(tcb_t a_tcb, int a_can_accept)
// ********************************************************************************************************************************
{
        tcp_ctx_t tctx = INET_TCB_TO_TCP_CTX(a_tcb);
        if (a_can_accept)
        {
                tctx->m_so->m_flags |= SOCKET_CANACCEPT;
        }
        else
        {
                tctx->m_so->m_flags &= ~SOCKET_CANACCEPT;
        }
        thread_wakeup(&tctx->m_so->m_flags);
}

// ********************************************************************************************************************************
bool inet_tcp_src_dst_equal(tcb_t a_tcb, iface_buffer_t a_ifbuf_lhs, iface_buffer_t a_ifbuf_rhs)
// ********************************************************************************************************************************
{
        in_port_t inp_lhs[2], inp_rhs[2];
        tcp_get_ports(a_ifbuf_lhs, inp_lhs);
        tcp_get_ports(a_ifbuf_rhs, inp_rhs);

        in_addr_t ina_lhs[2], ina_rhs[2];
        ipv4_get_addr(a_ifbuf_lhs, (struct in_addr *)ina_lhs);
        ipv4_get_addr(a_ifbuf_rhs, (struct in_addr *)ina_rhs);

        return etl::equal(inp_lhs, inp_rhs) && etl::equal(ina_lhs, ina_rhs);
}

// ********************************************************************************************************************************
kern_return_t inet_tcp_proto_connect(tcb_t a_tcb, iface_buffer_t a_ifbuf)
// ********************************************************************************************************************************
{
        tcp_ctx_t tctx = INET_TCB_TO_TCP_CTX(a_tcb);

        uint16_t iop[2];
        tcp_get_ports(a_ifbuf, iop);

        in_addr iaddr[2];
        ipv4_get_addr(a_ifbuf, iaddr);

        *SATOSIN(tctx->m_so->m_sa)      = {.sin_family = AF_INET, .sin_port = iop[1], .sin_addr = iaddr[1]};
        *SATOSIN(tctx->m_so->m_peer_sa) = {.sin_family = AF_INET, .sin_port = iop[0], .sin_addr = iaddr[0]};

        if (/*INET_TCP_SOCK_INSERT(tctx->m_so) != tctx->m_so*/ in_pcblookup(
            SATOSIN(tctx->m_so->m_peer_sa), SATOSIN(tctx->m_so->m_sa), false))
        {
                *SATOSIN(tctx->m_so->m_sa)      = {.sin_family = AF_INET};
                *SATOSIN(tctx->m_so->m_peer_sa) = {.sin_family = AF_INET};
                return KERN_FAIL(EADDRINUSE);
        }

        tctx->m_flags |= TCTX_CAN_LOOKUP;

        ++(tctx->m_refcount);

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
void inet_tcp_enq_out_network(tcb_t a_tcb, iface_buffer_t a_ifbuf)
// ********************************************************************************************************************************
{
        tcp_ctx_t tctx = INET_TCB_TO_TCP_CTX(a_tcb);

        a_ifbuf->m_flags = 0;

        (a_ifbuf)->m_layer_curr                          = NETSTACK_LAYER_USER;
        (a_ifbuf)->m_layer_pcb[(a_ifbuf)->m_layer_curr]  = tctx;
        (a_ifbuf)->m_proto                               = &s_inet_proto[0];
        (a_ifbuf)->m_offset                              = 32;
        (a_ifbuf)->m_packet_size                         = 0;
        (a_ifbuf)->m_layer_data[(a_ifbuf)->m_layer_curr] = (a_ifbuf)->m_virt_addr + ((a_ifbuf)->m_size - (a_ifbuf)->m_offset);
        (a_ifbuf)->m_payload_len[a_ifbuf->m_layer_curr]  = 0;

        a_ifbuf->m_layer_curr                        = NETSTACK_LAYER_TRANSPORT;
        a_ifbuf->m_layer_pcb[a_ifbuf->m_layer_curr]  = a_tcb;
        a_ifbuf->m_layer_data[a_ifbuf->m_layer_curr] = a_ifbuf->m_virt_addr + (a_ifbuf->m_size - (a_ifbuf->m_offset));

        INET_TCP_USR_INCOMING_ENQ(tctx, a_ifbuf);
}

// ********************************************************************************************************************************
void inet_tcp_state_change(tcb_t a_tcb, enum TCP_CONNECTION_STATE a_new_conn_state)
// ********************************************************************************************************************************
{
        tcp_ctx_t tctx = INET_TCB_TO_TCP_CTX(a_tcb);

        switch (a_new_conn_state)
        {
        case TCP_CONNECTION_STATE_CLOSED: {
                KASSERT(tctx->m_refcount > 0);
                --(tctx->m_refcount);

                if (tctx->m_refcount == 0)
                {
                        tctx->m_flags &= ~TCTX_CAN_LOOKUP;
                        // INET_TCP_SOCK_REMOVE(tctx->m_so);
                }

                tctx->m_so->m_flags &= ~(SOCKET_CANACCEPT | SOCKET_CANWRITE);
        }
        break;
        case TCP_CONNECTION_STATE_LISTEN: {
                asm("nop");
        }
        break;
        case TCP_CONNECTION_STATE_SYN_RCVD: {
                asm("nop");
        }
        break;
        case TCP_CONNECTION_STATE_SYN_SENT: {
                asm("nop");
        }
        break;
        case TCP_CONNECTION_STATE_ESTAB: {
                asm("nop");
        }
        break;
        case TCP_CONNECTION_STATE_FINWAIT_1: {
                asm("nop");
        }
        break;
        case TCP_CONNECTION_STATE_FINWAIT_2: {
                asm("nop");
        }
        break;
        case TCP_CONNECTION_STATE_CLOSING: {
                asm("nop");
        }
        break;
        case TCP_CONNECTION_STATE_TIME_WAIT: {
                asm("nop");
        }
        break;
        case TCP_CONNECTION_STATE_CLOSE_WAIT: {
                tctx->m_so->m_flags |= SOCKET_HANGUP;
        }
        break;
        case TCP_CONNECTION_STATE_LAST_ACK: {
                asm("nop");
        }
        break;
        default: {
                panic("Nasty bug, all enum values are accounted for");
        }
        break;
        }
}

// ********************************************************************************************************************************
kern_return_t inet_tcp_enq_out_user(tcb_t a_tcb, iface_buffer_t a_ifbuf)
// ********************************************************************************************************************************
{
        tcp_ctx_t tctx = INET_TCB_TO_TCP_CTX(a_tcb);

        if (a_ifbuf == nullptr)
        {
                kern_return_t retval = inet_tcp_alloc_user_buffer(tctx, &a_ifbuf);

                if (KERN_STATUS_FAILURE(retval))
                {
                        return retval;
                }
        }

        enqueue_head(&tctx->m_user_buffer_in, &a_ifbuf->m_dev_link);

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
void inet_tcp_timeout(natural_t a_ms_elapsed)
// ********************************************************************************************************************************
{
        if (lock_try_read(&s_inet_tcp_all_sock_lock) == true)
        {
                for (tcp_ctx_t tctx = INET_TCP_CTX_FIRST(); INET_TCP_CTX_END(tctx) == false; tctx = INET_TCP_CTX_NEXT(tctx))
                {
                        if (lock_try_read(&tctx->m_usr_req_lock) == false)
                        {
                                continue;
                        }

                        if (tctx->m_refcount == 0)
                        {
                                KASSERT(queue_empty(&tctx->m_user_buffer_out));
                                KASSERT(queue_empty(&tctx->m_user_buffer_in));

                                tcp_ctx_t tctx_prev = INET_TCP_CTX_PREV(tctx);
                                remqueue(&tctx->m_allsock_link);

                                if (tctx->m_route.ro_rt != nullptr)
                                {
                                        rtfree(tctx->m_route.ro_rt);
                                }

                                INET_FREE_CTX_ENQUE(tctx);
                                lock_read_done(&tctx->m_usr_req_lock);
                                tctx = tctx_prev;
                                continue;
                        }

                        tcp_pulse(&s_inet_proto[0], &tctx->m_tcb, a_ms_elapsed);

                        while (queue_empty(&tctx->m_usr_req_queue) == false)
                        {
                                inet_usr_req_t ureq = INET_USR_REQ_DEQUEUE(tctx);

                                kern_return_t retval = KERN_SUCCESS;

                                switch (ureq->m_op)
                                {
                                case NET_USR_REQ_BIND: {
                                        sockaddr_in *sin = SATOSIN(ureq->m_arg);

                                        if (sin->sin_addr.s_addr != INADDR_ANY)
                                        {
                                                if (ifa_ifwithaddr((sockaddr_t)sin) == nullptr)
                                                {
                                                        retval = KERN_FAIL(EADDRNOTAVAIL);
                                                        break;
                                                }
                                        }

                                        if (sin->sin_port != 0)
                                        {
                                                sockaddr_in faddr = {.sin_family = AF_INET};

                                                if (in_pcblookup(&faddr, sin, false) != nullptr)
                                                {
                                                        retval = KERN_FAIL(EADDRINUSE);
                                                        break;
                                                }
                                        }

                                        if (sin->sin_port == 0)
                                        {
                                                sin->sin_port = port_alloc();
                                        }

                                        *SATOSIN(tctx->m_so->m_sa) = *sin;

                                        tctx->m_refcount++;
                                        tctx->m_flags |= TCTX_CAN_LOOKUP;

                                        retval = KERN_SUCCESS;
                                        break;
                                }

                                case NET_USR_REQ_LISTEN: {
                                        integer_t backlog = (integer_t)(ureq->m_arg);
                                        retval            = tcp_usr_request(&tctx->m_tcb, NET_USR_REQ_LISTEN, backlog);

                                        break;
                                }

                                case NET_USR_REQ_ACCEPT: {
                                        socket_t newso    = (socket_t)(ureq->m_arg);
                                        tcp_ctx_t newtctx = INET_TCP_CTX(newso);

                                        retval = tcp_usr_request(&tctx->m_tcb, NET_USR_REQ_ACCEPT, &newtctx->m_tcb);

                                        if (KERN_STATUS_FAILURE(retval))
                                        {
                                                tctx->m_so->m_flags &= ~SOCKET_CANACCEPT;
                                                break;
                                        }

                                        KASSERT(newtctx->m_route.ro_rt == nullptr);
                                        *SATOSIN(&newtctx->m_route.ro_dst) = *SATOSIN(newso->m_peer_sa);
                                        rtalloc(&newtctx->m_route);

                                        break;
                                }

                                case NET_USR_REQ_CONNECT: {
                                        retval = KERN_SUCCESS;

                                        sockaddr_in *lsin = SATOSIN(tctx->m_so->m_sa);
                                        sockaddr_in *fsin = SATOSIN(tctx->m_so->m_peer_sa);

                                        *fsin = *SATOSIN(ureq->m_arg);

                                        if (lsin->sin_addr.s_addr == INADDR_ANY)
                                        {
                                                iface_t ifp;

                                                /*
                                                 * If route is known or can be allocated now,
                                                 * our src addr is taken from the i/f, else punt.
                                                 */
                                                if (tctx->m_route.ro_rt != nullptr
                                                    && SATOSIN(&tctx->m_route.ro_dst)->sin_addr.s_addr != lsin->sin_addr.s_addr)
                                                {
                                                        rtfree(tctx->m_route.ro_rt);
                                                        tctx->m_route.ro_rt = nullptr;
                                                }

                                                if (/*(inp->inp_socket->so_options & SO_DONTROUTE) == 0 && XXX*/
                                                    (tctx->m_route.ro_rt == nullptr || tctx->m_route.ro_rt->rt_ifp == nullptr))
                                                {
                                                        /* No route yet, so try to acquire one */
                                                        tctx->m_route.ro_dst.sa_family           = AF_INET;
                                                        SATOSIN(&tctx->m_route.ro_dst)->sin_addr = fsin->sin_addr;
                                                        rtalloc(&tctx->m_route);
                                                }

                                                /*
                                                 * If we found a route, use the address
                                                 * corresponding to the outgoing interface
                                                 * unless it is the loopback (in case a route
                                                 * to our address on another net goes to loopback).
                                                 */
                                                struct in_ifaddr *ia = nullptr;
                                                if (tctx->m_route.ro_rt && (ifp = tctx->m_route.ro_rt->rt_ifp)
                                                    && (ifp->if_flags & IFF_LOOPBACK) == 0)
                                                {

                                                        ia = (struct in_ifaddr *)ifa_ifwithnet(
                                                            (sockaddr_t)SATOSIN(&tctx->m_route.ro_dst));
#if 0
                                                        for (ia = s_in_ifaddr; ia; ia = ia->ia_next)
                                                        {
                                                                if (ia->ia_ifp == ifp)
                                                                {
                                                                        break;
                                                                }
                                                        }
#endif
                                                }

                                                if (ia == nullptr)
                                                {
                                                        ia = (struct in_ifaddr *)ifa_ifwithdstaddr((sockaddr_t)fsin);

                                                        if (ia == nullptr)
                                                        {
                                                                ia = in_iaonnetof(in_netof(fsin->sin_addr));
                                                        }

                                                        if (ia == nullptr)
                                                        {
                                                                ia = s_in_ifaddr;
                                                        }

                                                        if (ia == nullptr)
                                                        {
                                                                retval = KERN_FAIL(EADDRNOTAVAIL);
                                                                break;
                                                        }
                                                }

                                                *lsin = *SATOSIN(&ia->ia_addr);
                                        }

                                        if (in_pcblookup(fsin, lsin, true))
                                        {
                                                retval = KERN_FAIL(EADDRINUSE);
                                                break;
                                        }

                                        if (lsin->sin_addr.s_addr == INADDR_ANY)
                                        {
                                                //  inp->inp_laddr = ifaddr->sin_addr;
                                        }

                                        if (lsin->sin_port == 0)
                                        {
                                                lsin->sin_port = port_alloc();
                                        }

                                        if (KERN_STATUS_SUCCESS(retval))
                                        {
                                                iface_buffer_t ibuf;
                                                retval = inet_tcp_alloc_user_buffer(tctx, &ibuf);

                                                if (KERN_STATUS_FAILURE(retval))
                                                {
                                                        break;
                                                }

                                                ibuf->m_layer_curr                    = NETSTACK_LAYER_TRANSPORT;
                                                ibuf->m_layer_pcb[ibuf->m_layer_curr] = &tctx->m_tcb;
                                                in_port_t inp[2]                      = {lsin->sin_port, fsin->sin_port};

                                                retval = tcp_usr_request(&tctx->m_tcb, NET_USR_REQ_CONNECT, ibuf, inp);

                                                if (KERN_STATUS_FAILURE(retval))
                                                {
                                                        *lsin = {};
                                                        *fsin = {};
                                                        iface_buffer_free(nullptr, ibuf);
                                                }
                                                else
                                                {
                                                        tctx->m_refcount++;
                                                        tctx->m_flags |= TCTX_CAN_LOOKUP;
                                                        INET_TCP_USR_INCOMING_ENQ(tctx, ibuf);
                                                }
                                        }
                                        break;
                                }

                                case NET_USR_REQ_SEND: {
                                        usr_send_req_ctx *snd_ctx = (usr_send_req_ctx *)(ureq->m_arg);

                                        switch (snd_ctx->m_state)
                                        {
                                        case USR_SEND_CTX_STATE_BUFFER_ALLOC: {
                                                for (natural_t len = 0; len < snd_ctx->m_send_len; len += 1460)
                                                {
                                                        iface_buffer_t ibuf;
                                                        retval = inet_tcp_alloc_user_buffer(tctx, &ibuf);

                                                        if (KERN_STATUS_FAILURE(retval))
                                                        {
                                                                break;
                                                        }

                                                        enqueue(&snd_ctx->m_buffers, &ibuf->m_dev_link);
                                                }
                                                break;
                                        }
                                        case USR_SEND_CTX_STATE_BUFFER_FREE: {
                                                while (queue_empty(&snd_ctx->m_buffers) == false)
                                                {
                                                        iface_buffer_t ibuf = queue_containing_record(
                                                            dequeue(&snd_ctx->m_buffers), iface_buffer, m_dev_link);
                                                        iface_buffer_free(nullptr, ibuf);
                                                }
                                                break;
                                        }
                                        case USR_SEND_CTX_STATE_BUFFER_SEND: {
                                                while (queue_empty(&snd_ctx->m_buffers) == false)
                                                {
                                                        iface_buffer_t ibuf = queue_containing_record(
                                                            dequeue_tail(&snd_ctx->m_buffers), iface_buffer, m_dev_link);

                                                        if (ibuf->m_payload_len[ibuf->m_layer_curr] == 0)
                                                        {
                                                                iface_buffer_free(nullptr, ibuf);
                                                        }
                                                        else
                                                        {
                                                                /*
                                                                log_debug("(USR SEND)INET_TCP_USR_INCOMING_ENQ(tctx=%p, ibuf=%p)",
                                                                          tctx,
                                                                          ibuf);
                                                                */
                                                                INET_TCP_USR_INCOMING_ENQ(tctx, ibuf);
                                                        }
                                                }

                                                break;
                                        }

                                        default:
                                                panic("HOW?");
                                                break;
                                        }

                                        break;
                                }

                                case NET_USR_REQ_RECV: {
                                        usr_recv_req_ctx *rcv_ctx = (usr_recv_req_ctx *)(ureq->m_arg);

                                        retval = KERN_SUCCESS;

                                        while ((INET_TCP_USR_OUTGOING_EMPTY(tctx) == false)
                                               && INET_TCP_USR_OUTGOING_FIRST(tctx)->m_payload_len[NETSTACK_LAYER_USER] == 0)
                                        {
                                                --___nbufout;
                                                tctx->m_outbuf_count--;
                                                iface_buffer_free(nullptr, INET_TCP_USR_OUTGOING_DEQ(tctx));
                                                // log_debug("Freeing user buffer %08x", tctx->m_outbuf_count);
                                        }

                                        if ((rcv_ctx->m_count = tctx->m_outbuf_count) > 0)
                                        {
                                                rcv_ctx->m_buffer = INET_TCP_USR_OUTGOING_FIRST(tctx);
                                                break;
                                        }

                                        retval = tcp_usr_request(&tctx->m_tcb, NET_USR_REQ_RECV);

                                        break;
                                }

                                case NET_USR_REQ_CLOSE: {
                                        KASSERT(tctx->m_refcount > 0);

                                        --(tctx->m_refcount);

                                        in_port_t inp[2]
                                            = {SATOSIN(tctx->m_so->m_sa)->sin_port, SATOSIN(tctx->m_so->m_peer_sa)->sin_port};

                                        retval = tcp_usr_request(&tctx->m_tcb, NET_USR_REQ_CLOSE, inp);
                                        break;
                                }

                                case NET_USR_REQ_SHUTDOWN: {
                                        int how = (int)(ureq->m_arg);
                                        log_debug("TODO: Handle shutdown %d", how);
                                }
                                break;

                                case NET_USR_REQ_USER: {
                                        // log_debug("USER REQ");
                                        break;
                                }

                                default:
                                        retval = KERN_FAIL(EOPNOTSUPP);
                                        break;
                                }

                                ureq->m_retval = retval;
                                thread_wakeup(ureq);
                        }

                        inet_tcp_pump_output(tctx);

                        // Update flags
                        {
                                if ((tctx->m_so->m_flags & SOCKET_CANACCEPT))
                                {
                                        // Check if we still can accept and toggle flag
                                }

                                while ((INET_TCP_USR_OUTGOING_EMPTY(tctx) == false)
                                       && INET_TCP_USR_OUTGOING_FIRST(tctx)->m_payload_len[NETSTACK_LAYER_USER] == 0)
                                {
                                        --___nbufout;
                                        tctx->m_outbuf_count--;
                                        iface_buffer_free(nullptr, INET_TCP_USR_OUTGOING_DEQ(tctx));
                                        // log_debug("Freeing user buffer %08x", tctx->m_outbuf_count);
                                }

                                if (tctx->m_outbuf_count > 0)
                                {
                                        tctx->m_so->m_flags |= SOCKET_CANREAD;
                                }
                                else
                                {
                                        tctx->m_so->m_flags &= ~SOCKET_CANREAD;
                                }

                                if ((tctx->m_so->m_flags & SOCKET_HANGUP) == 0)
                                {
                                        if (INET_TCP_USR_OUTGOING_EMPTY(tctx))
                                        {
                                                tctx->m_so->m_flags |= SOCKET_CANWRITE;
                                        }
                                        else
                                        {
                                                tctx->m_so->m_flags &= ~SOCKET_CANWRITE;
                                                thread_wakeup(&tctx->m_so->m_flags);
                                        }
                                }
                                else
                                {
                                        tctx->m_so->m_flags &= ~SOCKET_CANWRITE;
                                        thread_wakeup(&tctx->m_so->m_flags);
                                }
                        }

                        extern int network_try_events(socket_t a_socket, integer_t a_events);
                        if (tctx->m_so->m_flags & (SOCKET_CANREAD | SOCKET_CANACCEPT | SOCKET_CANWRITE | SOCKET_HANGUP)
                            /*&& queue_empty(&tctx->m_so->m_poll_context) == false*/)
                        {
                                network_try_events(tctx->m_so, tctx->m_so->m_flags);
                        }

                        lock_read_done(&tctx->m_usr_req_lock);
                }
                lock_read_done(&s_inet_tcp_all_sock_lock);
        }

        // Try to free unused sockets
        extern int network_try_socket_free(socket_t a_socket);
        while (INET_FREE_CTX_EMPTY() == false)
        {
                tcp_ctx_t tctx = INET_FREE_CTX_DEQUE();
                if (network_try_socket_free(tctx->m_so) == false)
                {
                        INET_FREE_CTX_ENQUE(tctx);
                        break;
                }
        }
}

// ********************************************************************************************************************************
static kern_return_t inet_ifaddr_debug()
// ********************************************************************************************************************************
{
        afswitch[AF_INET] = {.af_hash = inet_hash, .af_netmatch = inet_netmatch, .af_equal = inet_equal};

        // LAN address
        *SATOSIN(&(s_ina[0].ia_ifa.ifa_addr)) = {.sin_family = AF_INET};
        inet_aton("192.168.1.123", &SATOSIN(&(s_ina[0].ia_ifa.ifa_addr))->sin_addr);

        s_in_ifaddr = &s_ina[0];

        inet_aton("192.168.1.0", &s_ina[0].ia_subnet);
        inet_aton("255.255.255.0", &s_ina[0].ia_subnetmask);
        inet_aton("192.168.1.0", &s_ina[0].ia_net);
        inet_aton("255.255.255.0", &s_ina[0].ia_netmask);

        iface_register_address(&s_ina[0].ia_ifa);

        // WSL2 address
        *SATOSIN(&(s_ina[1].ia_ifa.ifa_addr)) = {.sin_family = AF_INET};
        inet_aton("172.30.16.31", &SATOSIN(&(s_ina[1].ia_ifa.ifa_addr))->sin_addr);

        s_in_ifaddr->ia_next = &s_ina[1];

        inet_aton("172.30.16.0", &s_ina[1].ia_subnet);
        inet_aton("255.255.240.0", &s_ina[1].ia_subnetmask);
        inet_aton("172.30.16.0", &s_ina[1].ia_net);
        inet_aton("255.255.240.0", &s_ina[1].ia_netmask);

        iface_register_address(&s_ina[1].ia_ifa);

        s_ina[0].ia_ifa.ifa_ifp->if_flags |= IFF_UP;

        // Default route
        sockaddr_in singate = {.sin_family = AF_INET};
        inet_aton("192.168.1.1", &singate.sin_addr);
        sockaddr_in sindst = {.sin_family = AF_INET};
        inet_aton("0.0.0.0", &sindst.sin_addr);
        
        #if 0
        rtinit((sockaddr_t)&sindst, (sockaddr_t)&singate, /*SIOCADDRT*/ 0x890B, RTF_GATEWAY);

        // Local route
        sindst.sin_addr = in_makeaddr(ntohl(s_ina[0].ia_subnet.s_addr), INADDR_ANY);
        rtinit((sockaddr_t)&sindst, &s_ina[0].ia_addr, /*SIOCADDRT*/ 0x890B, 0);

        // WSL2 route
        sindst.sin_addr = in_makeaddr(ntohl(s_ina[1].ia_subnet.s_addr), INADDR_ANY);
        rtinit((sockaddr_t)&sindst, &s_ina[1].ia_addr, /*SIOCADDRT*/ 0x890B, 0);
        #endif
        
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t inet_init()
// ********************************************************************************************************************************
{
        lock_init(&s_inet_tcp_all_sock_lock, true);
        queue_init(&s_inet_tcp_all_sock);
        queue_init(&s_inet_tcp_del_sock);

        s_outport_counter = 0;

        domain_register(&s_inet_domain);

        return KERN_SUCCESS;
}

protocol_initcall(inet_init);

// late_initcall(inet_ifaddr_debug);
