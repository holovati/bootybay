#include <lwip/pbuf.h>

#include <emerixx/arpa/inet.h>
#include <emerixx/net/arp.h>
#include <emerixx/net/ethernet.h>
#include <emerixx/net/if.h>

#include <etl/algorithm.hh>

#include "domain.h"
#include "protocol.h"

extern void *ipv4_input(iface_t, iface_buffer_t);
extern void *arp_input(iface_t, iface_buffer_t);

// void *ethernet_input(iface_t a_iface, iface_buffer_t a_iface_buffer);
// void *ehternet_output(iface_t, iface_buffer_t);
/*static*/ extern void *inet_tcp_input(iface_t, iface_buffer_t);

static struct net_proto_data s_raw_ethernet_proto = {
        //
        .m_domain             = nullptr,
        .m_type               = 0,
        .m_protocol           = 0,
        .m_private_data_size  = 0,
        .m_private_data_align = 0,
        .m_input              = ethernet_input,
        .m_output             = ehternet_output,
        .m_proto_init         = nullptr,
        .m_user_request       = nullptr,
        .m_timeout            = nullptr
        //
};

extern void sys_arch_ether_arp(struct pbuf *a_pbuf);

// ********************************************************************************************************************************
void *ethernet_input(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        sys_arch_ether_arp(iface_buffer_get_pbuf(a_iface_buffer));
        return nullptr;

        a_iface_buffer->m_layer_curr                      = NETSTACK_LAYER_LINK;
        a_iface_buffer->m_layer_data[NETSTACK_LAYER_LINK] = a_iface_buffer->m_virt_addr + a_iface_buffer->m_offset;

        ether_header *ehdr = (ether_header *)(a_iface_buffer->m_virt_addr + a_iface_buffer->m_offset);

        if (ETHER_IS_VALID_LEN(a_iface_buffer->m_packet_size + 4) == false)
        {
                log_warn("Ethernet frame size invalid %d", a_iface_buffer->m_packet_size);
                goto freebuf_return;
        }

        a_iface_buffer->m_offset += sizeof(ether_header);

        switch (ntohs(ehdr->ether_type))
        {
                case ETH_P_IP: // Internet Protocol
                        // return NETSTACK_CALL(ipv4_input);
                        return NETSTACK_CALL(inet_tcp_input);

                case ETH_P_ARP: // Address Resoulution Protocol{
                                // a_iface_buffer->m_proto = &s_raw_ethernet_proto;
                {
                        sys_arch_ether_arp(iface_buffer_get_pbuf(a_iface_buffer));
                        return nullptr;

                        // return NETSTACK_CALL(iface_buffer_free);
                }
                default:
                        log_debug("Ethernet unknown proto: dest: %hhX:%hhX:%hhX:%hhX:%hhX:%hhX src: %hhX:%hhX:%hhX:%hhX:%hhX:%hhX "
                                  "type: "
                                  "%04X size: %04d",
                                  ehdr->ether_dhost[0],
                                  ehdr->ether_dhost[1],
                                  ehdr->ether_dhost[2],
                                  ehdr->ether_dhost[3],
                                  ehdr->ether_dhost[4],
                                  ehdr->ether_dhost[5],
                                  ehdr->ether_shost[0],
                                  ehdr->ether_shost[1],
                                  ehdr->ether_shost[2],
                                  ehdr->ether_shost[3],
                                  ehdr->ether_shost[4],
                                  ehdr->ether_shost[5],
                                  ntohs(ehdr->ether_type), // ntohs
                                  a_iface_buffer->m_packet_size);
                        break;
        }

freebuf_return:
        return NETSTACK_CALL(iface_buffer_free);
}

// ********************************************************************************************************************************
void *ehternet_output(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        if (a_iface_buffer->m_flags & IFACE_BUFFER_FL_REFLECT)
        {
                ether_header *ehdr = (ether_header *)(a_iface_buffer->m_layer_data[NETSTACK_LAYER_LINK]);

                etl::swap(ehdr->ether_shost, ehdr->ether_dhost);
        }
        else
        {
                ether_header *ehdr = ((ether_header *)a_iface_buffer->m_layer_data[a_iface_buffer->m_layer_curr]);

                KASSERT(ehdr->ether_type == htons(ETH_P_IP));

                a_iface_buffer->m_offset += sizeof(struct ether_header);

                a_iface_buffer->m_layer_data[a_iface_buffer->m_layer_curr]
                        = (a_iface_buffer->m_virt_addr + (a_iface_buffer->m_size - a_iface_buffer->m_offset));
        }

        if (a_iface_buffer->m_proto == &s_raw_ethernet_proto)
        {
                return NETSTACK_CALL(iface_buffer_send);
        }
        else
        {
                return NETSTACK_CALL(a_iface_buffer->m_proto->m_output);
        }
}

// ********************************************************************************************************************************
static kern_return_t ethernet_init()
// ********************************************************************************************************************************
{
        return KERN_SUCCESS;
}

protocol_initcall(ethernet_init);
