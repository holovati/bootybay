#pragma once

typedef struct in_cksum_vec *in_cksum_vec_t;

struct in_cksum_vec
{
        void *m_buf;
        natural_t m_len;
};

uint16_t in_cksum(in_cksum_vec_t a_vec, natural_t a_vec_cnt);
