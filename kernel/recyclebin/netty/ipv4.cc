#include <emerixx/arpa/inet.h>
#include <emerixx/inet/ip.h>
#include <emerixx/net/if.h>

#include <etl/algorithm.hh>

#include "domain.h"
#include "in_cksum.h"
#include "ipv4.h"
#include "protocol.h"

extern void *icmp_input(iface_t, iface_buffer_t);
extern void *udp_input(iface_t, iface_buffer_t);
extern void *tcp_input(iface_t, iface_buffer_t);

static protocol_ops s_ipv4_ops = {
    //
    .input  = ipv4_input,
    .output = ipv4_output
    //
};

void ipv4_get_addr(iface_buffer_t a_iface_buffer, in_addr a_in_addr[2])
{
        struct ip *iph = (struct ip *)(a_iface_buffer->m_layer_data[NETSTACK_LAYER_NETWORK]);
        a_in_addr[0]   = iph->ip_src;
        a_in_addr[1]   = iph->ip_dst;
}

// ********************************************************************************************************************************
int ipv4_csum_set(iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        iphdr *iph = (iphdr *)(a_iface_buffer->m_layer_data[NETSTACK_LAYER_NETWORK]);

        in_cksum_vec cksv = {.m_buf = iph, .m_len = (natural_t)(iph->ihl << 2)};

        return iph->check = in_cksum(&cksv, 1);
}

// ********************************************************************************************************************************
void *ipv4_input(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        iphdr *iph = (iphdr *)(a_iface_buffer->m_layer_data[NETSTACK_LAYER_NETWORK]);

        a_iface_buffer->m_offset += (iph->ihl << 2);

        switch (iph->protocol)
        {
        case IPPROTO_ICMP: // Internet Control Message Protocol
                // So we can return to this layer after icmp is done
                a_iface_buffer->m_layer_curr = NETSTACK_LAYER_TRANSPORT;
                return NETSTACK_CALL(icmp_input);
        case IPPROTO_TCP:
                return NETSTACK_CALL(a_iface_buffer->m_proto->m_input);
        case IPPROTO_UDP: // User Datagram Protocol
                return NETSTACK_CALL(udp_input);

        default:
                log_warn("Unknown IP protocol %000d", iph->protocol);
                break;
        }

        return NETSTACK_CALL(iface_buffer_free);
}

// ********************************************************************************************************************************
void *ipv4_output(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        if (a_iface_buffer->m_flags & IFACE_BUFFER_FL_REFLECT)
        {
                iphdr *iph = (iphdr *)(a_iface_buffer->m_layer_data[NETSTACK_LAYER_NETWORK]);

                etl::swap(iph->saddr, iph->daddr);

                iph->ttl = IPDEFTTL;

                iph->check        = 0;
                in_cksum_vec cksv = {.m_buf = iph, .m_len = (natural_t)(iph->ihl << 2)};
                iph->check        = in_cksum(&cksv, 1);
        }
        else
        {
                in_addr ina[2];
                ina[0] = ((in_addr *)a_iface_buffer->m_layer_data[a_iface_buffer->m_layer_curr])[0];
                ina[1] = ((in_addr *)a_iface_buffer->m_layer_data[a_iface_buffer->m_layer_curr])[1];

                a_iface_buffer->m_offset += sizeof(struct ip);
                a_iface_buffer->m_layer_data[a_iface_buffer->m_layer_curr]
                    = (a_iface_buffer->m_virt_addr + (a_iface_buffer->m_size - a_iface_buffer->m_offset));

                struct ip *iph = (struct ip *)a_iface_buffer->m_layer_data[a_iface_buffer->m_layer_curr];

                iph->ip_v   = IPVERSION & 0xF;
                iph->ip_hl  = (sizeof(struct ip) >> 2) & 0xF;
                iph->ip_tos = 0;
                iph->ip_len = htons(sizeof(struct ip) + a_iface_buffer->m_packet_size);
                iph->ip_id  = htons(4053);
                iph->ip_off = htons(0x4000); // DONT FRAG
                iph->ip_ttl = IPDEFTTL;
                iph->ip_p   = IPPROTO_TCP;
                iph->ip_sum = 0;
                iph->ip_src = ina[0];
                iph->ip_dst = ina[1];
        }
        return NETSTACK_CALL(a_iface_buffer->m_proto->m_output);
}

// ********************************************************************************************************************************
static kern_return_t ipv4_init()
// ********************************************************************************************************************************
{
        return KERN_SUCCESS;
}

protocol_initcall(ipv4_init);
