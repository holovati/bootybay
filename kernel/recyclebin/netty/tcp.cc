#include <emerixx/arpa/inet.h>
#include <emerixx/inet/ip.h>
#include <emerixx/inet/tcp.h>
#include <emerixx/net/if.h>
#include <mach/param.h>

#include <etl/algorithm.hh>

#include "protocol.h"

#include "domain.h"
#include "in_cksum.h"
#include "tcp_seq.h"

#include "tcp.h"

#define TCP_TCB(buf) ((tcb_t)((buf)->m_layer_pcb[NETSTACK_LAYER_TRANSPORT]))

#define TCP_HDR(buf)     ((tcphdr *)((buf)->m_layer_data[NETSTACK_LAYER_TRANSPORT]))
#define IPV4_HDR(if_buf) ((iphdr *)(if_buf)->m_layer_data[NETSTACK_LAYER_NETWORK])

#define tcp_segment_size(if_buf) ((natural_t)(ntohs(IPV4_HDR(if_buf)->tot_len) - ((IPV4_HDR(if_buf))->ihl << 2)))
#define tcp_payload_size(if_buf) (tcp_segment_size(if_buf) - (TCP_HDR(if_buf)->doff << 2))
#define tcp_payload_ptr(if_buf)  ((char *)(((vm_address_t)(TCP_HDR(if_buf))) + (TCP_HDR(if_buf)->doff << 2)))

#define TCP_SEG_LEN(if_buf) ((uint32_t)tcp_payload_size((if_buf)) + (!!(TCP_HDR((if_buf))->th_flags & (TH_SYN | TH_FIN))))

struct tcp_state_ops
{
        protocol_op_t m_input;
        protocol_op_t m_output;
        void (*m_pulse)(net_proto_t, tcb_t a_tcb, natural_t a_ms_elapsed);
        kern_return_t (*m_user_request)(tcb_t, net_usr_req_op_t, va_list);
};

struct tcp_pseudo_hdr
{
        uint32_t saddr;
        uint32_t daddr;
        uint8_t resv_msb;
        uint8_t proto;
        uint16_t tcp_seg_len;
};

static void *tcp_drop(iface_t a_iface, iface_buffer_t a_iface_buffer);
static void tcp_pulse_noop(net_proto_t, tcb_t a_tcb, natural_t a_ms_elapsed);
static void *tcp_time_wait_input(iface_t a_iface, iface_buffer_t a_iface_buffer);
static kern_return_t tcp_time_wait_usr_request(tcb_t, net_usr_req_op_t, va_list);

static tcp_state_ops s_time_wait_ops = {
    //
    .m_input        = tcp_time_wait_input,
    .m_output       = tcp_drop,
    .m_pulse        = tcp_pulse_noop,
    .m_user_request = tcp_time_wait_usr_request
    //
};

static void *tcp_last_ack_input(iface_t a_iface, iface_buffer_t a_iface_buffer);
// static void *tcp_last_ack_output(iface_t a_iface, iface_buffer_t a_iface_buffer);
static kern_return_t tcp_last_ack_usr_request(tcb_t, net_usr_req_op_t, va_list);

static tcp_state_ops s_last_ack_input_ops = {
    //
    .m_input        = tcp_last_ack_input,
    .m_output       = tcp_drop,
    .m_pulse        = tcp_pulse_noop,
    .m_user_request = tcp_last_ack_usr_request
    //
};

static void *tcp_closing_input(iface_t a_iface, iface_buffer_t a_iface_buffer);
static kern_return_t tcp_closing_usr_request(tcb_t, net_usr_req_op_t, va_list);

static tcp_state_ops s_closing_ops = {
    //
    .m_input        = tcp_closing_input,
    .m_output       = tcp_drop,
    .m_pulse        = tcp_pulse_noop,
    .m_user_request = tcp_closing_usr_request
    //
};

static void *tcp_close_wait_input(iface_t a_iface, iface_buffer_t a_iface_buffer);
static void *tcp_close_wait_output(iface_t a_iface, iface_buffer_t a_iface_buffer);
static kern_return_t tcp_close_wait_usr_request(tcb_t, net_usr_req_op_t, va_list);

static tcp_state_ops s_close_wait_ops = {
    //
    .m_input        = tcp_close_wait_input,
    .m_output       = tcp_close_wait_output,
    .m_pulse        = tcp_pulse_noop,
    .m_user_request = tcp_close_wait_usr_request
    //
};

static void *tcp_fin_wait2_input(iface_t a_iface, iface_buffer_t a_iface_buffer);
static kern_return_t tcp_fin_wait2_usr_request(tcb_t, net_usr_req_op_t, va_list);

static tcp_state_ops s_fin_wait2_ops = {
    //
    .m_input        = tcp_fin_wait2_input,
    .m_output       = tcp_drop,
    .m_pulse        = tcp_pulse_noop,
    .m_user_request = tcp_fin_wait2_usr_request
    //
};

static void *tcp_fin_wait1_input(iface_t a_iface, iface_buffer_t a_iface_buffer);
static kern_return_t tcp_fin_wait1_usr_request(tcb_t, net_usr_req_op_t, va_list);

static tcp_state_ops s_fin_wait1_ops = {
    //
    .m_input        = tcp_fin_wait1_input,
    .m_output       = tcp_drop,
    .m_pulse        = tcp_pulse_noop,
    .m_user_request = tcp_fin_wait1_usr_request
    //
};

static void *tcp_established_input(iface_t a_iface, iface_buffer_t a_iface_buffer);
static void *tcp_established_output(iface_t a_iface, iface_buffer_t a_iface_buffer);
static void tcp_established_pulse(net_proto_t, tcb_t a_tcb, natural_t a_ms_elapsed);
static kern_return_t tcp_established_usr_request(tcb_t, net_usr_req_op_t, va_list);

static tcp_state_ops s_established_ops = {
    //
    .m_input        = tcp_established_input,
    .m_output       = tcp_established_output,
    .m_pulse        = tcp_established_pulse,
    .m_user_request = tcp_established_usr_request
    //
};

static void *tcp_syn_recv_input(iface_t a_iface, iface_buffer_t a_iface_buffer);
static kern_return_t tcp_syn_recv_usr_request(tcb_t, net_usr_req_op_t, va_list);

static tcp_state_ops s_syn_recv_ops = {
    //
    .m_input        = tcp_syn_recv_input,
    .m_output       = tcp_drop,
    .m_pulse        = tcp_pulse_noop,
    .m_user_request = tcp_syn_recv_usr_request
    //
};

static void *tcp_syn_sent_input(iface_t a_iface, iface_buffer_t a_iface_buffer);
static void *tcp_syn_sent_output(iface_t a_iface, iface_buffer_t a_iface_buffer);
static kern_return_t tcp_syn_sent_usr_request(tcb_t, net_usr_req_op_t, va_list);

static tcp_state_ops s_syn_sent_ops = {
    //
    .m_input        = tcp_syn_sent_input,
    .m_output       = tcp_syn_sent_output,
    .m_pulse        = tcp_pulse_noop,
    .m_user_request = tcp_syn_sent_usr_request
    //
};

static void *tcp_listen_input(iface_t a_iface, iface_buffer_t a_iface_buffer);
static void tcp_listen_pulse(net_proto_t, tcb_t a_tcb, natural_t a_ms_elapsed);
static kern_return_t tcp_listen_usr_request(tcb_t, net_usr_req_op_t, va_list);

static tcp_state_ops s_listen_ops = {
    //
    .m_input        = tcp_listen_input,
    .m_output       = tcp_drop,
    .m_pulse        = tcp_listen_pulse,
    .m_user_request = tcp_listen_usr_request
    //
};

static void *tcp_closed_input(iface_t a_iface, iface_buffer_t a_iface_buffer);
static void tcp_closed_pulse(net_proto_t, tcb_t, natural_t);
static kern_return_t tcp_closed_usr_request(tcb_t, net_usr_req_op_t, va_list);

static tcp_state_ops s_closed_ops = {
    //
    .m_input        = tcp_closed_input,
    .m_output       = tcp_drop,
    .m_pulse        = tcp_closed_pulse,
    .m_user_request = tcp_closed_usr_request
    //
};

// ********************************************************************************************************************************
static void tcp_state_change(tcb_t a_tcb, enum TCP_CONNECTION_STATE a_new_state, ...)
// ********************************************************************************************************************************
{
        va_list args;
        va_start(args, a_new_state);
        switch (a_new_state)
        {
        case TCP_CONNECTION_STATE_CLOSED: {
                a_tcb->m_state_ops = &s_closed_ops;
        }
        break;
        case TCP_CONNECTION_STATE_LISTEN: {
                a_tcb->m_state_ops = &s_listen_ops;
                queue_init(&a_tcb->m_state_data.listen.acceptq);
                a_tcb->m_state_data.listen.backlog = va_arg(args, integer_t);
        }
        break;
        case TCP_CONNECTION_STATE_SYN_RCVD: {
                a_tcb->m_state_ops = &s_syn_recv_ops;
        }
        break;
        case TCP_CONNECTION_STATE_SYN_SENT: {
                a_tcb->m_state_ops = &s_syn_sent_ops;
                queue_init(&a_tcb->m_state_data.syn_sent.outq);
        }
        break;
        case TCP_CONNECTION_STATE_ESTAB: {
                a_tcb->m_state_ops = &s_established_ops;
        }
        break;
        case TCP_CONNECTION_STATE_FINWAIT_1: {
                a_tcb->m_state_ops = &s_fin_wait1_ops;
        }
        break;
        case TCP_CONNECTION_STATE_FINWAIT_2: {
                a_tcb->m_state_ops = &s_fin_wait2_ops;
        }
        break;
        case TCP_CONNECTION_STATE_CLOSING: {
                a_tcb->m_state_ops = &s_closing_ops;
        }
        break;
        case TCP_CONNECTION_STATE_TIME_WAIT: {
                a_tcb->m_state_ops = &s_time_wait_ops;
        }
        break;
        case TCP_CONNECTION_STATE_CLOSE_WAIT: {
                a_tcb->m_state_ops = &s_close_wait_ops;
        }
        break;
        case TCP_CONNECTION_STATE_LAST_ACK: {
                a_tcb->m_state_ops = &s_last_ack_input_ops;
        }
        break;
        default: {
                panic("Nasty bug, all enum values are accounted for");
        }
        break;
        }
        va_end(args);
        a_tcb->m_proto_ops->state_change(a_tcb, a_new_state);
}

// ********************************************************************************************************************************
static uint16_t tcp_csum(iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        uint16_t tmp = 0;

        tcp_pseudo_hdr tph = {
            //
            .saddr       = IPV4_HDR(a_iface_buffer)->saddr,
            .daddr       = IPV4_HDR(a_iface_buffer)->daddr,
            .resv_msb    = 0,
            .proto       = IPPROTO_TCP,
            .tcp_seg_len = htons((uint16_t)tcp_segment_size(a_iface_buffer))
            //
        };

        if (a_iface_buffer->m_flags & IFACE_BUFFER_FL_REFLECT)
        {
                etl::swap(tph.saddr, tph.daddr);
        }

        in_cksum_vec sumvec[] = {
            //
            {.m_buf = &tph, .m_len = sizeof(tph)},
            {.m_buf = TCP_HDR(a_iface_buffer), .m_len = tcp_segment_size(a_iface_buffer)}
            //
        };

        etl::swap(tmp, TCP_HDR(a_iface_buffer)->check);
        TCP_HDR(a_iface_buffer)->check = in_cksum(sumvec, ARRAY_SIZE(sumvec));
        etl::swap(tmp, TCP_HDR(a_iface_buffer)->check);

        return tmp;
}

// ********************************************************************************************************************************
int tcp_csum_valid(iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        return TCP_HDR(a_iface_buffer)->check == tcp_csum(a_iface_buffer);
}

// ********************************************************************************************************************************
int tcp_csum_set(iface_buffer_t a_iface_buffer, void *a_psuedo_hdr, natural_t a_phdr_len)
// ********************************************************************************************************************************
{
        in_cksum_vec sumvec[] = {
            //
            {.m_buf = a_psuedo_hdr, .m_len = a_phdr_len},
            {.m_buf = TCP_HDR(a_iface_buffer), .m_len = tcp_segment_size(a_iface_buffer)}
            //
        };

        return TCP_HDR(a_iface_buffer)->check = in_cksum(sumvec, ARRAY_SIZE(sumvec));
}

// ********************************************************************************************************************************
void tcp_get_ports(iface_buffer_t a_iface_buffer, uint16_t iop[2])
// ********************************************************************************************************************************
{
        iop[0] = TCP_HDR(a_iface_buffer)->source;
        iop[1] = TCP_HDR(a_iface_buffer)->dest;
}

// ********************************************************************************************************************************
static void *tcp_drop(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        tcphdr *tcph = TCP_HDR(a_iface_buffer);

        log_debug("TCP: dropping packet for port %d", ntohs(tcph->dest));

        return NETSTACK_CALL(iface_buffer_free);
}

// ********************************************************************************************************************************
static void *tcp_reflect(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        natural_t payload_len             = tcp_payload_size(a_iface_buffer);
        IPV4_HDR(a_iface_buffer)->tot_len = htons((uint16_t)(ntohs(IPV4_HDR(a_iface_buffer)->tot_len) - payload_len));
        a_iface_buffer->m_packet_size -= payload_len;

        tcphdr *tcph = TCP_HDR(a_iface_buffer);
        etl::swap(tcph->dest, tcph->source);
        a_iface_buffer->m_flags |= IFACE_BUFFER_FL_REFLECT;
        tcph->check = tcp_csum(a_iface_buffer);
        return NETSTACK_CALL(a_iface_buffer->m_proto->m_output);
}

// ********************************************************************************************************************************
static void *tcp_reflect_ack(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        tcphdr *tcph = TCP_HDR(a_iface_buffer);
        tcb_t tcb    = TCP_TCB(a_iface_buffer);

        tcph->seq     = htonl(tcb->snd_nxt);
        tcph->ack_seq = htonl(tcb->acked_rcv_nxt = tcb->rcv_nxt);
        tcph->th_win  = htons(tcb->rcv_wnd);
        tcph->th_flags |= TH_ACK;
        log_debug("Reflecting ack %p", a_iface_buffer);
        return NETSTACK_CALL(tcp_reflect);
}

// ********************************************************************************************************************************
static void *tcp_sequence_check_failure(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        tcphdr *tcph     = TCP_HDR(a_iface_buffer);
        tcb_t tcb        = TCP_TCB(a_iface_buffer);
        uint32_t seg_len = TCP_SEG_LEN(a_iface_buffer);
        uint32_t seg_ack = ntohl(tcph->ack_seq);
        uint32_t seg_seq = ntohl(tcph->seq);

        log_debug("Seq check fail SEG.SEQ=%u SEG.ACK=%u SEG.LEN=%u RCV.NXT=%u RCV.WND=%u",
                  seg_seq,
                  seg_ack,
                  seg_len,
                  tcb->rcv_nxt,
                  tcb->rcv_wnd);
        // check security and precedence??
        if (tcph->rst)
        {
                /*
                   If the RST bit is set then, any outstanding RECEIVEs and SEND
                   should receive "reset" responses.  All segment queues should be
                   flushed.  Users should also receive an unsolicited general
                   "connection reset" signal.  Enter the CLOSED state, delete the
                   TCB, and return.
                */
                panic("TCP: Got rst");
        }
        tcph->th_flags = 0;

        return NETSTACK_CALL(tcp_reflect_ack);
}

// ********************************************************************************************************************************
static bool tcp_process_ack(tcb_t a_tcb, uint32_t a_seg_ack, uint32_t a_seg_seq, uint16_t a_seg_wnd)
// ********************************************************************************************************************************
{
        if (SEQ_LT(a_tcb->snd_una, a_seg_ack) && SEQ_LEQ(a_seg_ack, a_tcb->snd_nxt))
        {
                /*
                Any segments on the retransmission queue which are thereby
                entirely acknowledged are removed.  Users should receive
                positive acknowledgments for buffers which have been SENT and
                fully acknowledged (i.e., SEND buffer should be returned with "ok" response).
                */
                a_tcb->snd_una = a_seg_ack;
        }
        else if (SEQ_LT(a_seg_ack, a_tcb->snd_una))
        {
                // If the ACK is a duplicate (SEG.ACK < SND.UNA), it can be ignored.
        }
        else if (SEQ_GT(a_seg_ack, a_tcb->snd_nxt))
        {
                /*
                If the ACK acks something not yet sent (SEG.ACK > SND.NXT) then send an ACK,
                drop the segment, and return.
                */
                return false;
        }

        /*
        Send window should be updated.
        If (SND.WL1 < SEG.SEQ or (SND.WL1 = SEG.SEQ and SND.WL2 =< SEG.ACK)),
        set SND.WND <- SEG.WND,
        set SND.WL1 <- SEG.SEQ,
        set SND.WL2 <- SEG.ACK.
        */
        if (SEQ_LT(a_tcb->snd_wl1, a_seg_seq) || (SEQ_EQ(a_tcb->snd_wl1, a_seg_seq) && SEQ_LEQ(a_tcb->snd_wl2, a_seg_ack)))
        {
#if 0
                log_debug(
                    "Updating send window (snd_wnd = %u, snd_wl1 = %u, snd_wl2 = %d) -> (snd_wnd = %u, snd_wl1 = %u, snd_wl2 = %d)",
                    a_tcb->snd_wnd,
                    a_tcb->snd_wl1,
                    a_tcb->snd_wl2,
                    a_seg_wnd,
                    a_seg_seq,
                    a_seg_ack);
#endif
                a_tcb->snd_wnd = a_seg_wnd;
                a_tcb->snd_wl1 = a_seg_seq;
                a_tcb->snd_wl2 = a_seg_ack;
        }
        return true;
}

// ********************************************************************************************************************************
void *tcp_time_wait_input(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        panic("IMP");
        return nullptr;
}

// ********************************************************************************************************************************
kern_return_t tcp_time_wait_usr_request(tcb_t, net_usr_req_op_t, va_list)
// ********************************************************************************************************************************
{
        panic("IMP");
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
void *tcp_last_ack_input(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        tcb_t tcb = TCP_TCB(a_iface_buffer);
        tcp_state_change(tcb, TCP_CONNECTION_STATE_CLOSED);

        // enter closed state
        return NETSTACK_CALL(iface_buffer_free);
}

// ********************************************************************************************************************************
kern_return_t tcp_last_ack_usr_request(tcb_t, net_usr_req_op_t, va_list)
// ********************************************************************************************************************************
{
        panic("IMP");
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
void *tcp_closing_input(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        panic("IMP");
        return nullptr;
}

// ********************************************************************************************************************************
kern_return_t tcp_closing_usr_request(tcb_t, net_usr_req_op_t, va_list)
// ********************************************************************************************************************************
{
        panic("IMP");
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
void *tcp_close_wait_input(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        tcphdr *tcph = TCP_HDR(a_iface_buffer);

        if (tcph->rst)
        {
                /*
                   If the RST bit is set then, any outstanding RECEIVEs and SEND
                   should receive "reset" responses.  All segment queues should be
                   flushed.  Users should also receive an unsolicited general
                   "connection reset" signal.  Enter the CLOSED state, delete the
                   TCB, and return.
                */
                panic("TCP: Got rst in close wait");
        }

        if (tcph->syn)
        {
                /*
                If the SYN is in the window it is an error, send a reset, any
                outstanding RECEIVEs and SEND should receive "reset" responses,
                all segment queues should be flushed, the user should also
                receive an unsolicited general "connection reset" signal, enter
                the CLOSED state, delete the TCB, and return.

                If the SYN is not in the window this step would not be reached
                and an ack would have been sent in the first step (sequence number check).
                */
                panic("TCP: Got syn in close wait");
        }

        tcb_t tcb = TCP_TCB(a_iface_buffer);

        uint32_t seg_ack = ntohl(tcph->ack_seq);
        uint32_t seg_seq = ntohl(tcph->seq);

        if (tcph->th_flags & TH_ACK)
        {
                // Drop the segment and return.
                return NETSTACK_CALL(tcp_drop);
        }
        else
        {
                if (tcp_process_ack(tcb, seg_ack, seg_seq, ntohs(tcph->window)) == false)
                {
                        tcph->th_flags = 0;
                        return NETSTACK_CALL(tcp_reflect_ack);
                }
        }

        return NETSTACK_CALL(tcp_reflect_ack);
}

// ********************************************************************************************************************************
void *tcp_close_wait_output(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        log_debug("sending in close wait");
        if (a_iface_buffer->m_flags & IFACE_BUFFER_FL_REFLECT)
        {
#if 0
                tcphdr *tcph = TCP_HDR(a_iface_buffer);
                if (a_iface_buffer->m_flags & IFACE_BUFFER_FL_READ_STOP)
                {
                        tcb_t tcb = TCP_TCB(a_iface_buffer);

                        tcph->seq        = htonl(tcb->snd_nxt);
                        tcph->ack_seq    = htonl(tcb->rcv_nxt);
                        tcph->th_flags   = TH_FIN | TH_ACK;
                        tcb->m_state_ops = &s_last_ack_input_ops;

                        return NETSTACK_CALL(tcp_reflect);
                }
                else
                {
                        tcph->th_flags = 0;
                }
#endif
                return NETSTACK_CALL(tcp_drop);
        }
        else
        {
                panic("How");
        }
        return NETSTACK_CALL(tcp_drop);
}

#define TCP_IBUF_OUT_DATA(ibuf) (ibuf->m_virt_addr + (ibuf->m_size - (ibuf->m_offset)))

// ********************************************************************************************************************************
kern_return_t tcp_close_wait_usr_request(tcb_t a_tcb, net_usr_req_op_t a_op, va_list a_ap)
// ********************************************************************************************************************************
{
        switch (a_op)
        {
        case NET_USR_REQ_RECV: {
                return -1; // We can't recv any more in this state
        }

        case NET_USR_REQ_CLOSE: {

                iface_buffer_t ifbuf;
                KASSERT(KERN_STATUS_SUCCESS(iface_buffer_alloc(nullptr, &ifbuf))); // TODO: Make sure this is available

                in_port_t *inp = va_arg(a_ap, in_port_t *);

                a_tcb->m_proto_ops->enq_out_network(a_tcb, ifbuf); // Will reset the buffer

                KASSERT(IS_ALIGNED(ifbuf->m_offset, sizeof(uint32_t)));

                ifbuf->m_offset += sizeof(tcphdr);
                ifbuf->m_layer_data[ifbuf->m_layer_curr] = TCP_IBUF_OUT_DATA(ifbuf);

                tcphdr *thdr   = TCP_HDR(ifbuf);
                thdr->th_sport = inp[0];
                thdr->th_dport = inp[1];
                thdr->th_seq   = htonl(a_tcb->snd_nxt);
                thdr->th_ack   = htonl(a_tcb->acked_rcv_nxt = a_tcb->rcv_nxt);
                thdr->th_x2    = 0;
                thdr->th_off   = (sizeof(tcphdr) / sizeof(uint32_t)) & 0x0F; // header and options in 32bit words
                thdr->th_win   = htons(a_tcb->rcv_wnd);
                thdr->th_flags = TH_FIN | TH_ACK;
                thdr->th_sum   = 0;
                thdr->th_urp   = 0;

                a_tcb->snd_nxt++;

                tcp_state_change(a_tcb, TCP_CONNECTION_STATE_LAST_ACK);
                break;
        }

        default:
                panic("IMP");
                break;
        }

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static void *tcp_fin_wait2_input(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        panic("IMP");
        return nullptr;
}

// ********************************************************************************************************************************
kern_return_t tcp_fin_wait2_usr_request(tcb_t, net_usr_req_op_t, va_list)
// ********************************************************************************************************************************
{
        panic("IMP");
        return KERN_SUCCESS;
}

#define TCB_RCV_WND(t) (((uint32_t)(t)->rcv_wnd) << (t)->rcv_wnd_shift_cnt)

#define IS_IN_RCV_WINDOW(t, w) (SEQ_EQ((t)->rcv_nxt, (w)) && (SEQ_LEQ((w), ((t)->rcv_nxt + TCB_RCV_WND(t)))))

// ********************************************************************************************************************************
void *tcp_fin_wait1_input(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        tcphdr *tcph     = TCP_HDR(a_iface_buffer);
        tcb_t tcb        = TCP_TCB(a_iface_buffer);
        uint32_t seg_len = TCP_SEG_LEN(a_iface_buffer);
        uint32_t seg_ack = ntohl(tcph->ack_seq);
        uint32_t seg_seq = ntohl(tcph->seq);

        // RFC793 pg 26 and 69
        if ((seg_len == 0) && (tcb->rcv_wnd == 0))
        {
                if (seg_seq != tcb->rcv_nxt)
                {
                        return NETSTACK_CALL(tcp_sequence_check_failure);
                }
        }
        else if ((seg_len == 0) && (tcb->rcv_wnd > 0))
        {
                if (IS_IN_RCV_WINDOW(tcb, seg_seq) == false)
                {
                        return NETSTACK_CALL(tcp_sequence_check_failure);
                }
        }
        else if ((seg_len > 0) && (tcb->rcv_wnd == 0))
        {
                if ((seg_len == 1) && SEQ_EQ(seg_seq, tcb->rcv_nxt - 1))
                {
                        log_debug("Keep alive");
                        return NETSTACK_CALL(tcp_reflect_ack);
                }

                return NETSTACK_CALL(tcp_sequence_check_failure);
        }
        else if (((seg_len > 0) && (tcb->rcv_wnd > 0)))
        {
                if ((IS_IN_RCV_WINDOW(tcb, seg_seq) == false) && (IS_IN_RCV_WINDOW(tcb, seg_seq + seg_len - 1) == false))
                {
                        if ((seg_len == 1) && SEQ_EQ(seg_seq, tcb->rcv_nxt - 1))
                        {
                                log_debug("Keep alive");
                                return NETSTACK_CALL(tcp_reflect_ack);
                        }

                        return NETSTACK_CALL(tcp_sequence_check_failure);
                }
        }
        else
        {
                panic("Not possible!");
        }

        if (tcph->th_flags & TH_SYN)
        {
                panic("TCP: Got syn in FIN-WAIT 1, handle!");
        }

        // check security and precedence??
        if (tcph->th_flags & TH_RST)
        {
                /*
                   If the RST bit is set then, any outstanding RECEIVEs and SEND
                   should receive "reset" responses.  All segment queues should be
                   flushed.  Users should also receive an unsolicited general
                   "connection reset" signal.  Enter the CLOSED state, delete the
                   TCB, and return.
                */
                panic("TCP: Got rst in FIN-WAIT 1, handle!");
        }

        if (tcph->th_flags & TH_ACK)
        {
                if (tcp_process_ack(tcb, seg_ack, seg_seq, ntohs(tcph->window)) == false)
                {
                        tcph->th_flags = 0;
                        return NETSTACK_CALL(tcp_reflect_ack);
                }
        }
        else
        {
                /*
                   If the ACK bit is off, drop the segment and return.
                */
                return NETSTACK_CALL(tcp_drop);
        }

        // if the FIN segment is now acknowledged, then enter FIN-WAIT-2 and continue processing in that state.
        if (seg_ack == tcb->snd_nxt)
        {
                tcp_state_change(tcb, TCP_CONNECTION_STATE_FINWAIT_2);
        }

        /*
           If the URG bit is set, signal the user "urgent" data has
           arrived.
        */
        if (tcph->th_flags & TH_URG)
        {
                panic("IMP");
        }

        // Since we know there is no syn here, the straddle is data

        natural_t payload_len = tcp_payload_size(a_iface_buffer);

        tcb->rcv_nxt = seg_seq + seg_len;

        tcb->ack_rmss += (uint32_t)(payload_len);

        a_iface_buffer->m_offset += (tcph->doff << 2);
        a_iface_buffer->m_payload_len[NETSTACK_LAYER_USER] = payload_len;

        if (tcph->th_flags & TH_PUSH)
        {
                /*
                If the segment empties and carries an PUSH flag, then
                the user is informed, when the buffer is returned, that a PUSH
                has been received.
                */
                a_iface_buffer->m_flags |= IFACE_BUFFER_FL_PUSH_USER;
        }

        if (tcph->th_flags & TH_FIN)
        {

                a_iface_buffer->m_flags |= IFACE_BUFFER_FL_PUSH_USER;

                log_debug("GOT FIN in finwait!!");

                if (seg_ack == tcb->snd_nxt)
                {
                        // Enter the TIME-WAIT state.  Start the time-wait timer, turn off the other
                        //  timers.
                        tcp_state_change(tcb, TCP_CONNECTION_STATE_TIME_WAIT);
                }
                else
                {
                        // Enter the CLOSE-WAIT state.
                        tcp_state_change(tcb, TCP_CONNECTION_STATE_CLOSING);
                }
        }

        if (payload_len > 0)
        {
                return NETSTACK_CALL(a_iface_buffer->m_proto->m_input);
        }

        tcph->th_flags = 0;
        return NETSTACK_CALL(tcp_reflect_ack);
}

// ********************************************************************************************************************************
kern_return_t tcp_fin_wait1_usr_request(tcb_t, net_usr_req_op_t, va_list)
// ********************************************************************************************************************************
{
        panic("IMP");
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
void *tcp_established_input(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        tcphdr *tcph     = TCP_HDR(a_iface_buffer);
        tcb_t tcb        = TCP_TCB(a_iface_buffer);
        uint32_t seg_len = TCP_SEG_LEN(a_iface_buffer);
        uint32_t seg_ack = ntohl(tcph->ack_seq);
        uint32_t seg_seq = ntohl(tcph->seq);

        // RFC793 pg 26 and 69
        if ((seg_len == 0) && (tcb->rcv_wnd == 0))
        {
                if (seg_seq != tcb->rcv_nxt)
                {
                        return NETSTACK_CALL(tcp_sequence_check_failure);
                }
        }
        else if ((seg_len == 0) && (tcb->rcv_wnd > 0))
        {
                if (IS_IN_RCV_WINDOW(tcb, seg_seq) == false)
                {
                        return NETSTACK_CALL(tcp_sequence_check_failure);
                }
        }
        else if ((seg_len > 0) && (tcb->rcv_wnd == 0))
        {
                if ((seg_len == 1) && SEQ_EQ(seg_seq, tcb->rcv_nxt - 1))
                {
                        log_debug("Keep alive");
                        return NETSTACK_CALL(tcp_reflect_ack);
                }

                return NETSTACK_CALL(tcp_sequence_check_failure);
        }
        else if (((seg_len > 0) && (tcb->rcv_wnd > 0)))
        {
                if ((IS_IN_RCV_WINDOW(tcb, seg_seq) == false) && (IS_IN_RCV_WINDOW(tcb, seg_seq + seg_len - 1) == false))
                {
                        if ((seg_len == 1) && SEQ_EQ(seg_seq, tcb->rcv_nxt - 1))
                        {
                                log_debug("Keep alive");
                                return NETSTACK_CALL(tcp_reflect_ack);
                        }

                        return NETSTACK_CALL(tcp_sequence_check_failure);
                }
        }
        else
        {
                panic("Not possible!");
        }

        // check security and precedence??
        if (tcph->rst)
        {
                /*
                   If the RST bit is set then, any outstanding RECEIVEs and SEND
                   should receive "reset" responses.  All segment queues should be
                   flushed.  Users should also receive an unsolicited general
                   "connection reset" signal.  Enter the CLOSED state, delete the
                   TCB, and return.
                */
                panic("TCP: Got rst in established");
        }

        if (tcph->syn)
        {
                /*
                If the SYN is in the window it is an error, send a reset, any
                outstanding RECEIVEs and SEND should receive "reset" responses,
                all segment queues should be flushed, the user should also
                receive an unsolicited general "connection reset" signal, enter
                the CLOSED state, delete the TCB, and return.

                If the SYN is not in the window this step would not be reached
                and an ack would have been sent in the first step (sequence number check).
                */
                panic("TCP: Got syn in established");
        }

        if (tcph->th_flags & TH_ACK)
        {
                if (tcp_process_ack(tcb, seg_ack, seg_seq, ntohs(tcph->window)) == false)
                {
                        tcph->th_flags = 0;
                        return NETSTACK_CALL(tcp_reflect_ack);
                }
        }
        else
        {
                // Drop the segment and return.
                return NETSTACK_CALL(tcp_drop);
        }

        if (tcph->urg)
        {
                /*
                If the URG bit is set, RCV.UP <- max(RCV.UP,SEG.UP), and signal
                the user that the remote side has urgent data if the urgent
                pointer (RCV.UP) is in advance of the data consumed.  If the
                user has already been signaled (or is still in the "urgent
                mode") for this continuous sequence of urgent data, do not
                signal the user again.
                */
                panic("urgent");
        }

        // Since we know there is no syn here, the straddle is data

        natural_t payload_len = tcp_payload_size(a_iface_buffer);
        // char *data            = tcp_payload_ptr(a_iface_buffer);

        uint32_t seq_straddle = seg_seq - tcb->rcv_nxt;

        if (seq_straddle > 0)
        {
                log_debug("Straddle %d valid in seg %d", seq_straddle, (seg_seq + seg_len - 1) - tcb->rcv_nxt);
        }

        tcb->rcv_nxt = seg_seq + seg_len;

        tcb->ack_rmss += (uint32_t)(payload_len);

        a_iface_buffer->m_offset += (tcph->doff << 2);
        a_iface_buffer->m_payload_len[NETSTACK_LAYER_USER] = payload_len;

        if (tcph->psh)
        {
                /*
                If the segment empties and carries an PUSH flag, then
                the user is informed, when the buffer is returned, that a PUSH
                has been received.
                */
                a_iface_buffer->m_flags |= IFACE_BUFFER_FL_PUSH_USER;
        }

        protocol_op_t pop = iface_buffer_free /*tcp_drop*/;
        if (tcph->fin)
        {
                // Enter the CLOSE-WAIT state.
                log_debug("GOT FIN!!");
                tcp_state_change(tcb, TCP_CONNECTION_STATE_CLOSE_WAIT);
                pop = a_iface_buffer->m_proto->m_input;
                a_iface_buffer->m_flags |= IFACE_BUFFER_FL_PUSH_USER;
        }
        else
        {
                if (payload_len > 0)
                {
                        // a_iface_buffer->m_proto->m_proto_request(NETSTACK_LAYER_TRANSPORT, TCP_PROTO_REQ_ENQ_OUT_BUF, tcb);
                        // a_iface_buffer->m_flags |= IFACE_BUFFER_FL_REFLECT;
                        pop = a_iface_buffer->m_proto->m_input;
                        // log_info("Push %d bytes", payload_len);
                }
        }

        return NETSTACK_CALL(pop);
#if 0
        // log_debug("SEQ.SEQ %d RCV.NXT %d SEG_LEN %d", seg_seq, tcb->rcv_nxt, seg_len);

        /*
        Once the TCP takes responsibility for the data it advances
        RCV.NXT over the data accepted, and adjusts RCV.WND as
        appropriate to the current buffer availability.  The total of
        RCV.NXT and RCV.WND should not be reduced.
        */
        // tcb->rcv_nxt += payload_len;

        /*
        Once in the ESTABLISHED state, it is possible to deliver segment
        text to user RECEIVE buffers.  Text from segments can be moved
        into buffers until either the buffer is full or the segment is
        empty.

        When the TCP takes responsibility for delivering the data to the
        user it must also acknowledge the receipt of the data.

        Send an acknowledgment of the form:

        <SEQ=SND.NXT><ACK=RCV.NXT><CTL=ACK>

        This acknowledgment should be piggybacked on a segment being
        transmitted if possible without incurring undue delay.
        */
#endif
}

// ********************************************************************************************************************************
void *tcp_established_output(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        if (a_iface_buffer->m_flags & IFACE_BUFFER_FL_REFLECT)
        {
                return NETSTACK_CALL(iface_buffer_free);
        }
        else
        {
                KASSERT(IS_ALIGNED(a_iface_buffer->m_offset, sizeof(uint32_t)));
                tcb_t tcb = TCP_TCB(a_iface_buffer);

                in_port_t inp[2];
                inp[0] = ((in_port_t *)a_iface_buffer->m_layer_data[a_iface_buffer->m_layer_curr])[0];
                inp[1] = ((in_port_t *)a_iface_buffer->m_layer_data[a_iface_buffer->m_layer_curr])[1];

                a_iface_buffer->m_offset += sizeof(tcphdr);
                a_iface_buffer->m_layer_data[a_iface_buffer->m_layer_curr] = TCP_IBUF_OUT_DATA(a_iface_buffer);
                //

                tcphdr *thdr   = TCP_HDR(a_iface_buffer);
                thdr->th_sport = inp[0];
                thdr->th_dport = inp[1];
                thdr->th_seq   = htonl(tcb->snd_nxt);
                thdr->th_ack   = htonl(tcb->acked_rcv_nxt = tcb->rcv_nxt);
                thdr->th_x2    = 0;
                thdr->th_off   = (sizeof(tcphdr) / sizeof(uint32_t)) & 0x0F; // header and options in 32bit words
                thdr->th_win   = htons(tcb->rcv_wnd);
                thdr->th_flags = TH_ACK;
                thdr->th_sum   = 0;
                thdr->th_urp   = 0;

                if (a_iface_buffer->m_flags & IFACE_BUFFER_FL_PUSH_USER)
                {
                        thdr->th_flags |= TH_PUSH;
                }

                tcb->snd_nxt += (uint32_t)(a_iface_buffer->m_payload_len[a_iface_buffer->m_layer_curr + 1]);

                return NETSTACK_CALL(a_iface_buffer->m_proto->m_output);
        }
}

// ********************************************************************************************************************************
static void tcp_established_pulse(net_proto_t a_proto, tcb_t a_tcb, natural_t a_ms_elapsed)
// ********************************************************************************************************************************
{
        a_tcb->ack_timeout += (uint32_t)(a_ms_elapsed);

        a_tcb->rcv_wnd
            = (uint16_t)etl::min((((iface_buffer_available_count()) * a_tcb->rcv_mss) >> a_tcb->rcv_wnd_shift_cnt), 0x200ul);

        if ((a_tcb->ack_timeout >= 200) || (a_tcb->ack_rmss >= (a_tcb->rcv_mss * 2)))
        {

                if (SEQ_GT(a_tcb->rcv_nxt, a_tcb->acked_rcv_nxt)
                    && KERN_STATUS_SUCCESS(a_tcb->m_proto_ops->enq_out_user(a_tcb, nullptr)))
                {
                        a_tcb->ack_timeout = 0;
                        a_tcb->ack_rmss    = 0;
                }
        }
}

// ********************************************************************************************************************************
kern_return_t tcp_established_usr_request(tcb_t a_tcb, net_usr_req_op_t a_op, va_list a_ap)
// ********************************************************************************************************************************
{
        kern_return_t retval = KERN_SUCCESS;
        switch (a_op)
        {
        case NET_USR_REQ_RECV: {
                // We can recv in established
                break;
        }

        case NET_USR_REQ_CLOSE: {
                iface_buffer_t ifbuf;
                KASSERT(KERN_STATUS_SUCCESS(iface_buffer_alloc(nullptr, &ifbuf))); // TODO: Make sure this is available

                in_port_t *inp = va_arg(a_ap, in_port_t *);

                a_tcb->m_proto_ops->enq_out_network(a_tcb, ifbuf); // Will reset the buffer

                KASSERT(IS_ALIGNED(ifbuf->m_offset, sizeof(uint32_t)));

                ifbuf->m_offset += sizeof(tcphdr);
                ifbuf->m_layer_data[ifbuf->m_layer_curr] = TCP_IBUF_OUT_DATA(ifbuf);

                tcphdr *thdr   = TCP_HDR(ifbuf);
                thdr->th_sport = inp[0];
                thdr->th_dport = inp[1];
                thdr->th_seq   = htonl(a_tcb->snd_nxt);
                thdr->th_ack   = htonl(a_tcb->acked_rcv_nxt = a_tcb->rcv_nxt);
                thdr->th_x2    = 0;
                thdr->th_off   = (sizeof(tcphdr) / sizeof(uint32_t)) & 0x0F; // header and options in 32bit words
                thdr->th_win   = htons(a_tcb->rcv_wnd);
                thdr->th_flags = TH_FIN | TH_ACK;
                thdr->th_sum   = 0;
                thdr->th_urp   = 0;

                a_tcb->snd_nxt++;

                tcp_state_change(a_tcb, TCP_CONNECTION_STATE_FINWAIT_1);
                break;
        }

        default:
                panic("IMP");
                break;
        }
        return retval;
}

// ********************************************************************************************************************************
void *tcp_syn_recv_input(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        if (tcp_payload_size(a_iface_buffer) > 0)
        {
                log_warn("TCP: There is data in syn_recv state, check it out!");
        }

        tcphdr *tcph = TCP_HDR(a_iface_buffer);
        tcb_t tcb    = TCP_TCB(a_iface_buffer);

        // Do some seq number checking here.

        if (tcph->rst)
        {
                /*
                   If this connection was initiated with a passive OPEN (i.e.,
                   came from the LISTEN state), then return this connection to
                   LISTEN state and return.  The user need not be informed.  If
                   this connection was initiated with an active OPEN (i.e., came
                   from SYN-SENT state) then the connection was refused, signal
                   the user "connection refused".  In either case, all segments
                   on the retransmission queue should be removed.  And in the
                   active OPEN case, enter the CLOSED state and delete the TCB,
                   and return.
                */

                log_debug("TCP: RST received in syn_recv state, returning to listen");
                // tcb->m_state_ops = &s_listen_ops;
                panic("need to enter listen state");
                return NETSTACK_CALL(tcp_drop);
        }

        // check security and precedence here???

        if (tcph->syn)
        {
                /*
                   If the SYN is in the window it is an error, send a reset, any
                   outstanding RECEIVEs and SEND should receive "reset" responses,
                   all segment queues should be flushed, the user should also
                   receive an unsolicited general "connection reset" signal, enter
                   the CLOSED state, delete the TCB, and return.
                */
                panic("Handle syn");
        }

        protocol_op_t next_proto_op = iface_buffer_free;

        if (!tcph->ack)
        {
                // Drop the segment and return
                return NETSTACK_CALL(tcp_drop);
        }
        else
        {
                if (SEQ_LT(tcb->snd_una, ntohl(tcph->ack_seq)) && SEQ_LEQ(ntohl(tcph->ack_seq), tcb->snd_nxt))
                {
                        // Enter ESTABLISHED state and continue processing.
                        tcp_state_change(tcb, TCP_CONNECTION_STATE_ESTAB);
                        tcb->snd_wnd = ntohs(tcph->window);
                        tcb->snd_wl1 = ntohl(tcph->seq);
                        tcb->snd_wl1 = ntohl(tcph->ack_seq);
                }
                else
                {
                        // Reset reply
                        tcph->seq      = tcph->ack_seq;
                        tcph->th_flags = TH_RST;
                        next_proto_op  = tcp_reflect;
                }
        }

        if (tcph->fin)
        {
                // Enter the CLOSE-WAIT state.
                tcp_state_change(tcb, TCP_CONNECTION_STATE_CLOSE_WAIT);
        }

        return NETSTACK_CALL(next_proto_op);
}

// ********************************************************************************************************************************
kern_return_t tcp_syn_recv_usr_request(tcb_t, net_usr_req_op_t a_op, va_list)
// ********************************************************************************************************************************
{
        switch (a_op)
        {
        case NET_USR_REQ_RECV: {
                return KERN_FAIL(EAGAIN);
        }

        default:
                panic("IMP");
                break;
        }
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
void *tcp_syn_sent_input(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        tcb_t tcb        = TCP_TCB(a_iface_buffer);
        tcphdr *thdr     = TCP_HDR(a_iface_buffer);
        uint32_t seg_ack = htonl(thdr->th_ack);
        uint32_t seg_seq = htonl(thdr->th_seq);
        uint16_t seg_wnd = htons(thdr->th_win);

        if (thdr->th_flags & TH_ACK)
        {
                if (SEQ_LEQ(seg_ack, tcb->iss) || SEQ_GT(seg_ack, tcb->snd_nxt))
                {
                        if (thdr->th_flags & TH_RST)
                        {
                                return NETSTACK_CALL(tcp_drop);
                        }
                        else
                        {
                                thdr->th_flags = TH_RST;
                                return NETSTACK_CALL(tcp_reflect_ack);
                        }
                }

                if ((SEQ_LT(tcb->snd_una, seg_ack) && SEQ_LEQ(seg_ack, tcb->snd_nxt)) == false)
                {
                        panic("unacceptable ack");
                }
        }

        if (thdr->th_flags & TH_RST)
        {
                if (thdr->th_flags & TH_ACK)
                {
                        log_debug("signal to the user \"error: connection reset\"");
                        tcp_state_change(tcb, TCP_CONNECTION_STATE_CLOSED);
                }

                return NETSTACK_CALL(tcp_drop);
        }

        if (thdr->th_flags & TH_SYN)
        {
                for (uint8_t *o = (uint8_t *)(thdr + 1); o < (uint8_t *)tcp_payload_ptr(a_iface_buffer);)
                {
                        uint8_t len = o[1];
                        KASSERT(len > 0);
                        switch (o[0])
                        {
                        case 0: // End
                                o = (uint8_t *)tcp_payload_ptr(a_iface_buffer);
                                break;
                        case 1: // NOP
                                o++;
                                break;
                        case 2: // MSS (Maximum segment size)
                                KASSERT(len == 4);
                                tcb->snd_mss = ntohs(*((uint16_t *)(o + 2)));
                                for (uint8_t i = 0; i < len; ++i)
                                {
                                        o[i] = 1; // NOP
                                }
                                o += len;
                                break;
                        case 3: // Window scale option
                                KASSERT(len == 3);
                                tcb->snd_wnd_shift_cnt = ((uint32_t)(*(o + 2)));
                                for (uint8_t i = 0; i < len; ++i)
                                {
                                        o[i] = 1; // NOP
                                }
                                o += len;
                                break;
                        case 4: // SACK permitted
                                // Nop them
                                o[0] = 1;
                                o[1] = 1;
                                o += 2;
                                break;
                        case 8: // Timestamp
                                KASSERT(len == 10);
                                for (uint8_t i = 0; i < len; ++i)
                                {
                                        o[i] = 1; // NOP
                                }
                                o += len;
                                break;
                        default:
                                o += len;
                                break;
                        }
                }

                tcb->irs     = seg_seq;
                tcb->rcv_nxt = seg_seq + 1;

                tcb->snd_wnd = seg_wnd;
                tcb->snd_wl1 = seg_seq;
                tcb->snd_wl2 = seg_ack;

                if (thdr->th_flags & TH_ACK)
                {
                        tcb->snd_una = tcb->snd_una + 1;
                        log_warn("TODO: any segments on the retransmission queue that are thereby acknowledged should be removed "
                                 "%p",
                                 a_iface_buffer);
                }

                thdr->th_flags = 0;

                if (SEQ_GT(tcb->snd_una, tcb->iss))
                {
                        // Our syn has been ack'ed
                        while (queue_empty(&tcb->m_state_data.syn_sent.outq) == false)
                        {
                                iface_buffer_t ifbuf
                                    = queue_containing_record(dequeue(&tcb->m_state_data.syn_sent.outq), iface_buffer, m_dev_link);
                                tcb->m_proto_ops->enq_out_user(tcb, ifbuf);
                        }

                        tcp_state_change(tcb, TCP_CONNECTION_STATE_ESTAB);
                }
                else
                {
                        log_error("Remember to requeue eventual outgoing buffers");
                        thdr->th_flags = TH_SYN;
                        tcp_state_change(tcb, TCP_CONNECTION_STATE_SYN_RCVD);
                }

                return NETSTACK_CALL(tcp_reflect_ack);
        }

        return NETSTACK_CALL(tcp_drop);
}

typedef uint8_t tcp_opt_eol_t;
typedef uint8_t tcp_opt_nop_t;
typedef uint16_t tcp_opt_mss_t;
typedef uint8_t tcp_opt_ws_t;
typedef uint8_t tcp_opt_sack_t;
typedef struct
{
        uint32_t val;
        uint32_t secr;
} tcp_opt_ts_t;

#define TCP_OPTIONS_FOREACH(DO)                                                                                                    \
        DO(0, 1, tcp_opt_eol_t, END_OF_LIST, eol)                                                                                  \
        DO(1, 1, tcp_opt_nop_t, NO_OPERATION, nop)                                                                                 \
        DO(2, 4, tcp_opt_mss_t, MAXIMUM_SEGMENT_SIZE, mss)                                                                         \
        DO(3, 3, tcp_opt_ws_t, WINDOW_SCALE, ws)                                                                                   \
        DO(4, 2, tcp_opt_sack_t, SACK_PERMITTED, sack)                                                                             \
        DO(8, 10, tcp_opt_ts_t, TIME_STAMP, ts)

#define TCP_OPTIONS_ENUM(kind, len, dtype, name, ...) TCP_OPT_KIND_##name = kind,
typedef enum __attribute__((packed)) tcp_opt_kind
{
        TCP_OPTIONS_FOREACH(TCP_OPTIONS_ENUM) //
        TCP_OPT_KIND_COUNT
} tcp_opt_kind_t;
#undef TCP_OPTIONS_ENUM

static_assert(sizeof(tcp_opt_kind_t) == 1);

#define TCP_OPTIONS_PUSH(kind, len, dtype, name, ...)                                                                              \
        static inline void tcp_opt_push_##__VA_ARGS__(iface_buffer_t a_iface_buffer, dtype const &val = {})                        \
        {                                                                                                                          \
                for (int i = 0; i < ALIGN(len, alignof(uint32_t)); ++i)                                                            \
                {                                                                                                                  \
                        a_iface_buffer->m_offset++;                                                                                \
                        if (TCP_OPT_KIND_##name == TCP_OPT_KIND_END_OF_LIST)                                                       \
                        {                                                                                                          \
                                *((tcp_opt_kind_t *)(TCP_IBUF_OUT_DATA(a_iface_buffer))) = TCP_OPT_KIND_END_OF_LIST;               \
                        }                                                                                                          \
                        else                                                                                                       \
                        {                                                                                                          \
                                *((tcp_opt_kind_t *)(TCP_IBUF_OUT_DATA(a_iface_buffer))) = TCP_OPT_KIND_NO_OPERATION;              \
                        }                                                                                                          \
                }                                                                                                                  \
                char *c = (char *)TCP_IBUF_OUT_DATA(a_iface_buffer);                                                               \
                c[0]    = TCP_OPT_KIND_##name;                                                                                     \
                if (len > 1)                                                                                                       \
                {                                                                                                                  \
                        c[1] = len;                                                                                                \
                        if (len > 2)                                                                                               \
                        {                                                                                                          \
                                *((dtype *)(c + 2)) = val;                                                                         \
                        }                                                                                                          \
                }                                                                                                                  \
        }

TCP_OPTIONS_FOREACH(TCP_OPTIONS_PUSH)

#undef TCP_OPTIONS_PUSH

// ********************************************************************************************************************************
void *tcp_syn_sent_output(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        log_info("Queue the data for transmission after entering ESTABLISHED state");
        tcb_t tcb                    = TCP_TCB(a_iface_buffer);
        a_iface_buffer->m_layer_curr = NETSTACK_LAYER_USER; // Reset so tcp_output gets called by protocol layer
        enqueue(&tcb->m_state_data.syn_sent.outq, &a_iface_buffer->m_dev_link);
        return NETSTACK_CALL(nullptr);
}

// ********************************************************************************************************************************
kern_return_t tcp_syn_sent_usr_request(tcb_t a_tcb, net_usr_req_op_t a_op, va_list ap)
// ********************************************************************************************************************************
{
        switch (a_op)
        {
        case NET_USR_REQ_RECV: {
                return KERN_FAIL(EAGAIN);
        }

        default:
                panic("IMP");
                break;
        }
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
void *tcp_listen_input(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        // rfc793 - pg.65

        tcphdr *tcph = TCP_HDR(a_iface_buffer);

        if (tcp_payload_size(a_iface_buffer) > 0)
        {
                log_warn("TCP: There is data in listen state, check it out!");
        }

        if (tcph->rst || tcph->fin)
        {
                /*
                   An incoming RST should be ignored.
                */
                log_debug("TCP: RST received in listen state");
                return NETSTACK_CALL(tcp_drop);
        }

        if (tcph->ack)
        {
                /*
                   Any acknowledgment is bad if it arrives on a connection still in the LISTEN state.
                   An acceptable reset segment should be formed for any arriving ACK-bearing segment.
                   The RST should be formatted as follows:
                   <SEQ=SEG.ACK><CTL=RST>
                */

                tcph->seq      = tcph->ack_seq;
                tcph->th_flags = TH_RST;

                return NETSTACK_CALL(tcp_reflect);
        }

        if (!tcph->syn)
        {
                /*
                   Any other control or text-bearing segment (not containing SYN) must have an ACK and thus would be discarded
                   by the ACK processing. An incoming RST segment could not be valid, since it could not have been sent in response
                   to anything sent by this incarnation of the connection. So you are unlikely to get here, but if you do, drop the
                   segment, and return.
                */
                log_debug("TCP: No syn in listen state");
                return NETSTACK_CALL(tcp_drop);
        }

        // Acceptable segment, further processing is done when the user accepts the connection
        tcb_t tcb = TCP_TCB(a_iface_buffer);

        for (queue_entry_t elt = queue_first(&tcb->m_state_data.listen.acceptq);
             queue_end(&tcb->m_state_data.listen.acceptq, elt) == false;
             elt = queue_next(elt))
        {
                iface_buffer_t ifbuf
                    = queue_containing_record(dequeue(&tcb->m_state_data.listen.acceptq), iface_buffer, m_dev_link);

                if (tcb->m_proto_ops->src_dest_equal(tcb, ifbuf, a_iface_buffer) == true)
                {
                        log_debug("SYN retransmission from same peer, dropping the old one");
                        remqueue(elt);
                        tcb->m_state_data.listen.backlog++;
                        iface_buffer_free(a_iface, ifbuf);
                        break;
                }
        }

        if (tcb->m_state_data.listen.backlog == 0)
        {
                log_debug("Dropping incoming connection, backlog == 0");
                return NETSTACK_CALL(tcp_drop);
        }

        enqueue(&TCP_TCB(a_iface_buffer)->m_state_data.listen.acceptq, &a_iface_buffer->m_dev_link);
        tcb->m_state_data.listen.backlog--;
        return nullptr;
}

// ********************************************************************************************************************************
void tcp_listen_pulse(net_proto_t, tcb_t a_tcb, natural_t a_ms_elapsed)
// ********************************************************************************************************************************
{
        a_tcb->m_proto_ops->accept_ready(a_tcb, queue_empty(&a_tcb->m_state_data.listen.acceptq) == false);
}

// ********************************************************************************************************************************
kern_return_t tcp_listen_usr_request(tcb_t a_tcb, net_usr_req_op_t a_op, va_list ap)
// ********************************************************************************************************************************
{
        switch (a_op)
        {
        case NET_USR_REQ_CLOSE: {
                while (queue_empty(&a_tcb->m_state_data.listen.acceptq) == false)
                {
                        iface_buffer_free(
                            nullptr,
                            queue_containing_record(dequeue(&a_tcb->m_state_data.listen.acceptq), iface_buffer, m_dev_link));
                }
                tcp_state_change(a_tcb, TCP_CONNECTION_STATE_CLOSED);

                break;
        }
        case NET_USR_REQ_ACCEPT: {
                if (queue_empty(&a_tcb->m_state_data.listen.acceptq))
                {
                        return KERN_FAIL(EAGAIN);
                }

                tcb_t newtcb = va_arg(ap, tcb_t);
                iface_buffer_t acceptbuf
                    = queue_containing_record(dequeue(&a_tcb->m_state_data.listen.acceptq), iface_buffer, m_dev_link);
                a_tcb->m_state_data.listen.backlog++;

                /*
                   Set RCV.NXT to SEG.SEQ+1, IRS is set to SEG.SEQ and any other
                   control or text should be queued for processing later.  ISS
                   should be selected and a SYN segment sent of the form:

                     <SEQ=ISS><ACK=RCV.NXT><CTL=SYN,ACK>

                   SND.NXT is set to ISS+1 and SND.UNA to ISS.  The connection
                   state should be changed to SYN-RECEIVED.  Note that any other
                   incoming control or data (combined with SYN) will be processed
                   in the SYN-RECEIVED state, but processing of SYN and ACK should
                   not be repeated.  If the listen was not fully specified (i.e.,
                   the foreign socket was not fully specified), then the
                   unspecified fields should be filled in now.
                */

                tcphdr *thdr = TCP_HDR(acceptbuf);

                newtcb->iss     = 300;
                newtcb->snd_una = newtcb->iss;
                newtcb->snd_nxt = newtcb->iss + 1;
                newtcb->snd_wnd = ntohs(thdr->th_win);

                newtcb->irs               = ntohl(thdr->th_seq);
                newtcb->rcv_nxt           = newtcb->irs + 1;
                newtcb->rcv_wnd           = 0x200; // 65536 with scale
                newtcb->rcv_wnd_shift_cnt = __builtin_ffs(128) - 1;
                newtcb->rcv_mss           = 1460;

                // Parse options

                for (uint8_t *o = (uint8_t *)(thdr + 1); o < (uint8_t *)tcp_payload_ptr(acceptbuf);)
                {
                        uint8_t len = o[1];
                        KASSERT(len > 0);
                        switch (o[0])
                        {
                        case 0: // End
                                o = (uint8_t *)tcp_payload_ptr(acceptbuf);
                                break;
                        case 1: // NOP
                                o++;
                                break;
                        case 2: // MSS (Maximum segment size) swap
                                KASSERT(len == 4);
                                newtcb->snd_mss = ntohs(*((uint16_t *)(o + 2)));
                                o += len;
                                break;
                        case 3: // Window scale option swap
                                KASSERT(len == 3);
                                newtcb->snd_wnd_shift_cnt = *(o + 2);
                                o += len;
                                break;
                        case 4: // SACK permitted
                                // Nop them
                                o[0] = 1;
                                o[1] = 1;
                                o += 2;
                                break;
                        case 8: // Timestamp
                                KASSERT(len == 10);
                                for (uint8_t i = 0; i < len; ++i)
                                {
                                        o[i] = 1; // NOP
                                }
                                o += len;
                                break;
                        default:
                                o += len;
                                break;
                        }
                }

                kern_return_t retval = newtcb->m_proto_ops->proto_connect(newtcb, acceptbuf);

                if (KERN_STATUS_FAILURE(retval))
                {
                        iface_buffer_free(nullptr, acceptbuf);
                        return KERN_FAIL(EAGAIN);
                }

                in_port_t inp[2] = {thdr->th_dport, thdr->th_sport};

                newtcb->m_proto_ops->enq_out_network(newtcb, acceptbuf); // Will reset the buffer

                KASSERT(IS_ALIGNED(acceptbuf->m_offset, sizeof(uint32_t)));

                natural_t obufoff = acceptbuf->m_offset;
                tcp_opt_push_eol(acceptbuf);
                tcp_opt_push_ws(acceptbuf, (tcp_opt_ws_t)newtcb->rcv_wnd_shift_cnt);
                tcp_opt_push_mss(acceptbuf, htons(newtcb->rcv_mss));

                acceptbuf->m_offset += sizeof(tcphdr);
                acceptbuf->m_layer_data[acceptbuf->m_layer_curr] = TCP_IBUF_OUT_DATA(acceptbuf);

                thdr           = TCP_HDR(acceptbuf);
                thdr->th_sport = inp[0];
                thdr->th_dport = inp[1];
                thdr->th_seq   = htonl(newtcb->iss);
                thdr->th_ack   = htonl(newtcb->rcv_nxt);
                thdr->th_x2    = 0;
                thdr->th_off   = ((acceptbuf->m_offset - obufoff) / sizeof(uint32_t)) & 0x0F; // header and options in 32bit words
                thdr->th_win   = 0x1000;
                thdr->th_flags = TH_ACK | TH_SYN;
                thdr->th_sum   = 0;
                thdr->th_urp   = 0;

                tcp_state_change(newtcb, TCP_CONNECTION_STATE_SYN_RCVD);

                break;
        }

        default:
                panic("IMP");
                break;
        }

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
void *tcp_closed_input(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        /*
        All data in the incoming segment is discarded.
        An incoming segment containing a RST is discarded.
        An incoming segment not containing a RST causes a RST to be sent in response.
        The acknowledgment and sequence field values are selected to make the reset sequence acceptable to the TCP endpoint that
        sent the offending segment.

        If the ACK bit is off, sequence number zero is used,
                <SEQ=0><ACK=SEG.SEQ+SEG.LEN><CTL=RST,ACK>
        If the ACK bit is on,
                <SEQ=SEG.ACK><CTL=RST>
        Return.
        */

        panic("IMP");
        return nullptr;
}

// ********************************************************************************************************************************
void tcp_closed_pulse(net_proto_t a_proto, tcb_t a_tcb, natural_t a_ms_elapsed)
// ********************************************************************************************************************************
{
}

// ********************************************************************************************************************************
kern_return_t tcp_closed_usr_request(tcb_t a_tcb, net_usr_req_op_t a_op, va_list ap)
// ********************************************************************************************************************************
{
        switch (a_op)
        {
        case NET_USR_REQ_CONNECT: {
                tcp_state_change(a_tcb, TCP_CONNECTION_STATE_SYN_SENT);

                a_tcb->iss               = 100;
                a_tcb->snd_una           = a_tcb->iss;
                a_tcb->snd_nxt           = a_tcb->iss + 1;
                a_tcb->rcv_wnd           = 0x200; // 65536 with scale
                a_tcb->rcv_wnd_shift_cnt = __builtin_ffs(128) - 1;
                a_tcb->rcv_mss           = 1460;

                iface_buffer_t ibuf = va_arg(ap, iface_buffer_t);
                in_port_t *inp      = va_arg(ap, in_port_t *);
                KASSERT(IS_ALIGNED(ibuf->m_offset, sizeof(uint32_t)));

                natural_t obufoff = ibuf->m_offset;
                tcp_opt_push_eol(ibuf);
                tcp_opt_push_ws(ibuf, (tcp_opt_ws_t)a_tcb->rcv_wnd_shift_cnt);
                tcp_opt_push_mss(ibuf, htons(a_tcb->rcv_mss));

                ibuf->m_offset += sizeof(tcphdr);
                ibuf->m_layer_data[ibuf->m_layer_curr] = TCP_IBUF_OUT_DATA(ibuf);
                //

                tcphdr *thdr   = TCP_HDR(ibuf);
                thdr->th_sport = inp[0];
                thdr->th_dport = inp[1];
                thdr->th_seq   = htonl(a_tcb->snd_una);
                thdr->th_ack   = htonl(a_tcb->rcv_nxt); // 0 at this point
                thdr->th_x2    = 0;
                thdr->th_off   = ((ibuf->m_offset - obufoff) / sizeof(uint32_t)) & 0x0F; // header and options in 32bit words
                thdr->th_win   = htons(536);
                thdr->th_flags = TH_SYN;
                thdr->th_sum   = 0;
                thdr->th_urp   = 0;
        }
        break;
        case NET_USR_REQ_LISTEN: {
                tcp_state_change(a_tcb, TCP_CONNECTION_STATE_LISTEN, va_arg(ap, integer_t));
                a_tcb->iss = 300;
        }
        break;

        default:
                panic("IMP");
                break;
        }
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
void tcp_tcb_init(tcb_t a_tcb, tcb_proto_ops_t a_tcb_proto_ops)
// ********************************************************************************************************************************
{
        a_tcb->m_state_ops = &s_closed_ops;
        a_tcb->m_proto_ops = a_tcb_proto_ops;
}

// ********************************************************************************************************************************
void tcp_pulse_noop(net_proto_t, tcb_t, natural_t)
// ********************************************************************************************************************************
{
}

// ********************************************************************************************************************************
void *tcp_input(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        return NETSTACK_CALL(TCP_TCB(a_iface_buffer)->m_state_ops->m_input);
}

// ********************************************************************************************************************************
void *tcp_output(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        return NETSTACK_CALL(TCP_TCB(a_iface_buffer)->m_state_ops->m_output);
}

// ********************************************************************************************************************************
void tcp_pulse(net_proto_t a_proto, tcb_t a_tcb, natural_t a_ms_elapsed)
// ********************************************************************************************************************************
{
        a_tcb->m_state_ops->m_pulse(a_proto, a_tcb, a_ms_elapsed);
}
// ********************************************************************************************************************************
kern_return_t tcp_usr_request(tcb_t a_tcb, net_usr_req_op_t a_op, ...)
// ********************************************************************************************************************************
{
        va_list args;
        va_start(args, a_op);
        kern_return_t retval = a_tcb->m_state_ops->m_user_request(a_tcb, a_op, args);
        va_end(args);
        return retval;
}

#if 0
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>

int main()
{
        int sfd = socket(AF_INET, SOCK_STREAM, 0);

        if (sfd == -1)
        {
                printf("Socket creation failed\n");
                exit(0);
        }

        struct sockaddr_in sa = {.sin_family = AF_INET, .sin_port = htons(80), .sin_addr = {.s_addr = htonl(INADDR_ANY)}};

        int err = bind(sfd, (struct sockaddr *)&sa, sizeof(sa));

        if (err)
        {
                printf("Socket binding failed\n");
                exit(0);
        }

        err = listen(sfd, 2);

        if (err)
        {
                printf("Socket listen failed\n");
                exit(0);
        }

        struct sockaddr_in sa_connaddr = {};
        socklen_t salen                = sizeof(sa_connaddr);
        int connsfd                    = accept(sfd, (struct sockaddr *)&sa_connaddr, &salen);

        close(sfd);

        if (connsfd < 0)
        {
                printf("Socket accept failed\n");
                exit(0);
        }

        char buf[0x200];

        inet_ntop(AF_INET, &sa_connaddr.sin_addr, buf, sizeof(buf));

        printf("Connection from %s on port %d\n", buf, sa_connaddr.sin_port);

        uintptr_t hash = 5381;
        for (err = recv(connsfd, buf, sizeof(buf), 0); err > 0; err = recv(connsfd, buf, sizeof(buf), 0))
        {
                for (int i = 0; i < err; ++i)
                {
                        hash = ((hash << 5) + hash) + buf[i];
                }
        }

        printf("HASH %p\n", (void *)hash);

        close(connsfd);

        return 0;
}
#endif