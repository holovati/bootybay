#pragma once

#include <arpa/inet.h>

#if defined(htons)
#undef htons
#endif

#if defined(htonl)
#undef htonl
#endif

#if defined(ntohs)
#undef ntohs
#endif

#if defined(ntohl)
#undef ntohl
#endif

#if __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
#define htons(v) (v)
#define htonl(v) (v)

#define ntohs(v) (v)
#define ntohl(v) (v)

#else
#define htons(v) __builtin_bswap16(((uint16_t)(v)))
#define htonl(v) __builtin_bswap32(((uint32_t)(v)))

#define ntohs(v) __builtin_bswap16(((uint16_t)(v)))
#define ntohl(v) __builtin_bswap32(((uint32_t)(v)))
#endif
