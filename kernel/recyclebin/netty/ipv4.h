#pragma once

#include <emerixx/inet/ip.h>
#include <emerixx/net/types.h>

void *ipv4_input(iface_t a_iface, iface_buffer_t a_iface_buffer);

void *ipv4_output(iface_t, iface_buffer_t);

void ipv4_get_addr(iface_buffer_t a_iface_buffer, in_addr a_in_addr[2]);

int ipv4_csum_set(iface_buffer_t a_iface_buffer);
