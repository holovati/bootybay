#pragma once

#define NETSTACK_CALL(x) ((void *)(x))

typedef struct iface *iface_t;

typedef struct iface_buffer *iface_buffer_t;

typedef struct protocol_ops *protocol_ops_t;

typedef struct protocol_ctx *protocol_ctx_t;

typedef struct socket *socket_t;

typedef void *(*protocol_op_t)(iface_t, iface_buffer_t);
