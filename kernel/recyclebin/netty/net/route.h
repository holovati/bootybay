#pragma once

#include <emerixx/ioctl_req.h>
#include <emerixx/net/if.h>

struct afswitch
{
        void (*af_hash)(struct sockaddr *sin, struct afhash *hp);
        int (*af_netmatch)(struct sockaddr *sin1, struct sockaddr *sin2);
        int (*af_equal)(struct sockaddr *sin1, struct sockaddr *sin2);
};

struct afhash
{
        uint32_t afh_hosthash;
        uint32_t afh_nethash;
};

extern struct afswitch afswitch[];

/*
 * Mach Operating System
 * Copyright (c) 1989 Carnegie-Mellon University
 * Copyright (c) 1988 Carnegie-Mellon University
 * All rights reserved.  The CMU software License Agreement specifies
 * the terms and conditions for use and redistribution.
 */

/*
 * Copyright (c) 1980, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)route.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Kernel resident routing tables.
 *
 * The routing tables are initialized when interface addresses
 * are set by making entries for all directly connected interfaces.
 */

/*
 * A route consists of a destination address and a reference
 * to a routing entry.  These are often held by protocols
 * in their control blocks, e.g. inpcb.
 */

typedef struct rtentry *rtentry_t;
typedef struct route *route_t;

struct route
{
        rtentry_t ro_rt;
        struct sockaddr ro_dst;
};

/*
 * We distinguish between routes to hosts and routes to networks,
 * preferring the former if available.  For each route we infer
 * the interface to use from the gateway address supplied when
 * the route was entered.  Routes that forward packets through
 * gateways are marked so that the output routines know to address the
 * gateway rather than the ultimate destination.
 */
struct rtentry
{
        queue_chain_t rt_bucket_link; /* to speed lookups */
        natural_t rt_hash;            /* to speed lookups */
        struct sockaddr rt_dst;       /* key */
        struct sockaddr rt_gateway;   /* value */
        natural_t rt_flags;           /* up/down?, host/net */
        natural_t rt_refcnt;          /* # held references */
        natural_t rt_use;             /* raw # packets forwarded */
        iface_t rt_ifp;               /* the answer: interface to use */
};

#define RTF_UP       0x1  /* route useable */
#define RTF_GATEWAY  0x2  /* destination is a gateway */
#define RTF_HOST     0x4  /* host entry (net otherwise) */
#define RTF_DYNAMIC  0x10 /* created dynamically (by redirect) */
#define RTF_MODIFIED 0x20 /* modified dynamically (by redirect)*/

/*
 * Routing statistics.
 */
struct rtstat
{
        short rts_badredirect; /* bogus redirect calls */
        short rts_dynamic;     /* routes created by redirects */
        short rts_newgateway;  /* routes modified by redirects */
        short rts_unreach;     /* lookups which failed */
        short rts_wildcard;    /* lookups satisfied by a wildcard */
};

void rtalloc(route_t ro);

void rtfree(rtentry_t rt);

void rtinit(sockaddr_t dst, sockaddr_t gateway, ioctl_req_t cmd, int32_t flags);
