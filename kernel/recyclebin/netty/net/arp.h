#pragma once

#include <emerixx/net/types.h>
#include <net/if_arp.h>

void arp_timer();

int arp_resolve(iface_t a_iface, iface_buffer_t a_iface_buffer);

void *arp_output(iface_t a_iface, iface_buffer_t a_iface_buffer);

int arp_resolve_ex(struct route *a_route, struct ether_header *a_ehdr);