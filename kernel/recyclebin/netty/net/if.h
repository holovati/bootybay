#pragma once

#include <net/if.h>

#include <etl/queue.h>

#include <emerixx/net/types.h>

typedef struct net_proto_data *net_proto_t;

typedef struct ifaddr *ifaddr_t;

typedef struct sockaddr *sockaddr_t;

struct iface
{
        void (*rx)(iface_t);
        void (*tx)(iface_t, iface_buffer_t);
        void *(*input)(iface_t, iface_buffer_t);
        void *(*output)(iface_t, iface_buffer_t);
        uint8_t mac_addr[IFHWADDRLEN];
        uint16_t if_flags;
};

#define IFACE_BUFFER_FL_DIR_OUT   BIT0
#define IFACE_BUFFER_FL_REFLECT   BIT1
#define IFACE_BUFFER_FL_PUSH_USER BIT2

#define NETSTACK_LAYER_UNUSED    0
#define NETSTACK_LAYER_LINK      1
#define NETSTACK_LAYER_NETWORK   2
#define NETSTACK_LAYER_TRANSPORT 3
#define NETSTACK_LAYER_USER      4
#define NETSTACK_LAYER_COUNT     5

struct iface_buffer
{
        natural_t m_phys_addr;
        vm_address_t m_virt_addr;
        queue_chain_t m_dev_link;

        natural_t m_size;
        natural_t m_packet_size;
        natural_t m_offset;
        natural_t m_flags;

        natural_t m_header_len[NETSTACK_LAYER_COUNT];
        natural_t m_payload_len[NETSTACK_LAYER_COUNT];
        natural_t m_footer_len[NETSTACK_LAYER_COUNT];
        natural_t m_layer_curr;

        natural_t m_layer_data[NETSTACK_LAYER_COUNT];
        void *m_layer_pcb[NETSTACK_LAYER_COUNT];

        net_proto_t m_proto;
        iface_t m_iface;
};

void iface_rx_sched(iface_t a_iface);

iface_t iface_alloc(natural_t a_private_size);

void *iface_private(iface_t a_iface);

void iface_network_wakeup();

struct pbuf *iface_buffer_get_pbuf(iface_buffer_t a_iface_buffer);

ifaddr_t ifa_ifwithaddr(sockaddr_t a_sa);
ifaddr_t ifa_ifwithdstaddr(sockaddr_t a_sa);
ifaddr_t ifa_ifwithnet(sockaddr_t a_sa);

void iface_register_address(ifaddr_t a_ifaddr);

kern_return_t iface_buffer_alloc(iface_t a_iface, iface_buffer_t *a_if_buffer_out);

void *iface_buffer_free(iface_t a_iface, iface_buffer_t a_iface_buffer);

void iface_buffer_recv(iface_t a_iface, iface_buffer_t a_iface_buffer);

void *iface_buffer_send(iface_t a_iface, iface_buffer_t a_iface_buffer);

void iface_buffer_send_pbuf(struct pbuf *a_pbuf);

natural_t iface_buffer_available_count();
