#include <emerixx/ioctl_req.h>
#include <emerixx/arpa/inet.h>
#include <emerixx/net/arp.h>
#include <emerixx/net/ethernet.h>
#include <emerixx/net/if.h>
#include <emerixx/net/route.h>

#include <etl/algorithm.hh>

#include "domain.h"
#include "ipv4.h"
#include "protocol.h"

#define ARP_HDR(buf) ((arphdr *)((buf)->m_virt_addr + (buf)->m_offset))

#define ARPTAB_BSIZ 9  /* bucket size */
#define ARPTAB_NB   19 /* number of buckets */
#define ARPTAB_SIZE (ARPTAB_BSIZ * ARPTAB_NB)

#define ATF_INUSE 0x01
#define ATF_DOASK 0x100

/*
 * Internet to ethernet address resolution table.
 */
struct arptab
{
        struct mbuf *at_hold;     /* last packet until resolved/timeout */
        struct ifnet *at_if;      /* interface */
        struct in_addr at_iaddr;  /* internet address */
        struct in_addr at_iaddr2; /* src internet address */
        natural_t at_flags;       /* flags */
        uint8_t at_enaddr[6];     /* ethernet address */
        uint8_t at_timer;         /* minutes since last reference */
        uint8_t pad;              /* pad */
};

static struct arptab s_arptab[ARPTAB_SIZE];
int arptab_size = ARPTAB_SIZE; /* for arp command */

/*
 * ARP trailer negotiation.  Trailer protocol is not IP specific,
 * but ARP request/response use IP addresses.
 */
#define ETHERTYPE_IPTRAILERS ETHERTYPE_TRAIL

#define ARPTAB_HASH(a) ((natural_t)(a) % ARPTAB_NB)

#define ARPTAB_LOOK(at, addr)                                                                                                      \
        {                                                                                                                          \
                int n;                                                                                                             \
                at = &s_arptab[ARPTAB_HASH(addr) * ARPTAB_BSIZ];                                                                   \
                for (n = 0; n < ARPTAB_BSIZ; n++, at++)                                                                            \
                {                                                                                                                  \
                        if (at->at_iaddr.s_addr == addr)                                                                           \
                        {                                                                                                          \
                                break;                                                                                             \
                        }                                                                                                          \
                }                                                                                                                  \
                                                                                                                                   \
                if (n >= ARPTAB_BSIZ)                                                                                              \
                {                                                                                                                  \
                        at = 0;                                                                                                    \
                }                                                                                                                  \
        }

/* timer values */
#define ARPT_AGE   (60 * 1) /* aging timer, 1 min. */
#define ARPT_KILLC 20       /* kill completed entry in 20 mins. */
#define ARPT_KILLI 3        /* kill incomplete entry in 3 minutes */

static uint8_t etherbroadcastaddr[6] = { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };

/*
 * Broadcast an ARP packet, asking who has addr on interface ac.
 */
// ********************************************************************************************************************************
static void arp_whohas(struct arptab *at)
// ********************************************************************************************************************************
{
        if ((at->at_flags & ATF_DOASK) == 0)
        {
                return;
        }

        iface_buffer_t ifbuf;
        kern_return_t retval = iface_buffer_alloc((iface_t)at->at_if, &ifbuf);
        if (KERN_STATUS_FAILURE(retval))
        {
                return;
        }

        at->at_flags &= ~ATF_DOASK;

        ifbuf->m_layer_curr = NETSTACK_LAYER_LINK;
        ether_header *eh    = (ether_header *)(ifbuf->m_layer_data[ifbuf->m_layer_curr] = ifbuf->m_virt_addr);
        ifbuf->m_offset += sizeof(ether_header);

        struct ether_arp *ea = (ether_arp *)ARP_HDR(ifbuf);
        ifbuf->m_offset += sizeof(struct ether_arp);

        ifbuf->m_packet_size = ifbuf->m_offset;

        *eh = {};

        etl::copy(etherbroadcastaddr, eh->ether_dhost);
        etl::copy(((iface_t)at->at_if)->mac_addr, eh->ether_shost);
        eh->ether_type = htons(ETHERTYPE_ARP);

        ea->arp_hrd = htons(ARPHRD_ETHER);
        ea->arp_pro = htons(ETHERTYPE_IP);
        ea->arp_hln = sizeof(ea->arp_sha); /* hardware address length */
        ea->arp_pln = sizeof(ea->arp_spa); /* protocol address length */
        ea->arp_op  = htons(ARPOP_REQUEST);

        etl::copy(eh->ether_shost, ea->arp_sha);
        *((struct in_addr *)&(ea->arp_spa)) = at->at_iaddr2;

        etl::clear(ea->arp_tha);
        *((struct in_addr *)&(ea->arp_tpa)) = at->at_iaddr;

        ((iface_t)at->at_if)->tx(((iface_t)at->at_if), ifbuf);
}

/*
 * Free an arptab entry.
 */
// ********************************************************************************************************************************
static void arp_tabfree(struct arptab *at)
// ********************************************************************************************************************************
{
        *at = {};
}

/*
 * Enter a new address in arptab, pushing out the oldest entry
 * from the bucket if there is no room.
 * This always succeeds since no bucket can be completely filled
 * with permanent entries (except from arpioctl when testing whether
 * another permanent entry will fit).
 */
// ********************************************************************************************************************************
struct arptab *arp_tabnew(struct in_addr *addr)
// ********************************************************************************************************************************
{
        int n;
        int oldest = -1;
        struct arptab *at, *ato = NULL;

        at = &s_arptab[ARPTAB_HASH(addr->s_addr) * ARPTAB_BSIZ];

        for (n = 0; n < ARPTAB_BSIZ; n++, at++)
        {
                if (at->at_flags == 0)
                {
                        goto out; /* found an empty entry */
                }

                if (at->at_flags & ATF_PERM)
                {
                        continue;
                }

                if (at->at_timer > oldest)
                {
                        oldest = at->at_timer;
                        ato    = at;
                }
        }

        if (ato == NULL)
        {
                return (NULL);
        }

        at = ato;
        arp_tabfree(at);
out:
        at->at_iaddr = *addr;
        at->at_flags = ATF_INUSE | ATF_DOASK;
        return (at);
}

/*
 * Timeout routine.  Age arp_tab entries once a minute.
 */
// ********************************************************************************************************************************
void arp_timer()
// ********************************************************************************************************************************
{
        for (arptab &at : s_arptab)
        {
                if (at.at_flags == 0 || (at.at_flags & ATF_PERM))
                {
                        continue;
                }

                if (at.at_flags & ATF_DOASK)
                {
                        arp_whohas(&at);
                        continue;
                }

                if (++at.at_timer < ((at.at_flags & ATF_COM) ? ARPT_KILLC : ARPT_KILLI))
                {
                        continue;
                }

                log_debug("arp_timer: deleting entry for %s", inet_ntoa(at.at_iaddr));

                /* timer has expired, clear entry */
                arp_tabfree(&at);
        }
}

// ********************************************************************************************************************************
static void *arp_drop(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        arphdr *arph = ARP_HDR(a_iface_buffer);
        // log_debug("ARP:dropping opcode %04X for proto %04X", ntohs(arph->ar_op), ntohs(arph->ar_pro));
        return NETSTACK_CALL(iface_buffer_free);
}

// ********************************************************************************************************************************
static void *arp_ethernet_ipv4(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        ether_arp *ea = (ether_arp *)ARP_HDR(a_iface_buffer);
        ethhdr *eh    = (ethhdr *)a_iface_buffer->m_layer_data[NETSTACK_LAYER_LINK];

        if (etl::equal(ea->arp_sha, a_iface->mac_addr))
        {
                log_debug("it's from me, ignore it.");
                return NETSTACK_CALL(arp_drop);
        }

        in_addr isaddr = { .s_addr = ((struct in_addr *)ea->arp_spa)->s_addr };
        in_addr itaddr = { .s_addr = ((struct in_addr *)ea->arp_tpa)->s_addr };

        if (etl::equal(ea->arp_sha, etherbroadcastaddr))
        {
                log_error("arp: ether address is broadcast for IP address %x!\n", ntohl(isaddr.s_addr));
                return NETSTACK_CALL(arp_drop);
        }

        struct arptab *at;
        ARPTAB_LOOK(at, isaddr.s_addr);

        switch (ntohs(ea->arp_op))
        {
                case ARPOP_REQUEST:
                {

                        sockaddr_in src_addr = { .sin_family = AF_INET, .sin_addr = itaddr };

                        ifaddr_t ifa = ifa_ifwithaddr((sockaddr *)&src_addr);

                        if (ifa == nullptr)
                        {
                                // log_debug("Arp request for unknown address");
                                break;
                        }

                        ea->arp_op = htons(ARPOP_REPLY);

                        // Fill in the correct MAC address from the interface
                        etl::copy(ifa->ifa_ifp->mac_addr, ea->arp_tha);

                        // Swap sender and target macs
                        etl::swap(ea->arp_sha, ea->arp_tha);

                        // Swap sender and target ip addresses
                        etl::swap(ea->arp_spa, ea->arp_tpa);

                        // Fill in our the correct mac address, eth layer will swap them while reflecting
                        etl::copy(ea->arp_sha, eh->h_dest);
                        a_iface_buffer->m_flags |= IFACE_BUFFER_FL_REFLECT;

                        // Add the address to the table
                        if (at == nullptr)
                        {
                                at = arp_tabnew(&isaddr);
                                at->at_flags &= ~ATF_DOASK;

                                at->at_if     = (struct ifnet *)a_iface;
                                at->at_iaddr  = isaddr;
                                at->at_iaddr2 = itaddr;
                        }

                        etl::copy(ea->arp_tha, at->at_enaddr);
                        at->at_flags |= ATF_COM;

                        log_debug("Arp request for address %s", inet_ntoa(itaddr));

                        return NETSTACK_CALL(a_iface_buffer->m_proto->m_output);
                }
                break;

                case ARPOP_REPLY:
                {
                        if (at == nullptr)
                        {
                                log_debug("Arp reply for unknown address");
                                break;
                        }

                        if (at->at_flags & ATF_COM)
                        {
                                log_debug("Arp reply for already completed address");
                                break;
                        }

                        etl::copy(ea->arp_sha, at->at_enaddr);
                        at->at_flags |= ATF_COM;

                        log_info("Arp reply for address %s", inet_ntoa(isaddr));
                }
                break;

                default:
                        log_debug("Arp unknown opcode %d", ntohs(ea->arp_op));
                        break;
        }

        return NETSTACK_CALL(arp_drop);
}

// ********************************************************************************************************************************
void *arp_input(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        arphdr *arph = ARP_HDR(a_iface_buffer);

        switch (ntohs(arph->ar_hrd))
        {
                case ARPHRD_ETHER:
                        if ((arph->ar_hln == 6) /*MAC addr len*/ && arph->ar_pln == 4 /* Ip4 addr len*/
                            && ntohs(arph->ar_pro) == ETHERTYPE_IP /* IPv4 protocol*/)
                        {
                                return NETSTACK_CALL(arp_ethernet_ipv4);
                        }
                        break;

                default:
                        break;
        }

        return NETSTACK_CALL(arp_drop);
}

// ********************************************************************************************************************************
void *arp_output(iface_t, iface_buffer_t)
// ********************************************************************************************************************************
{
        return nullptr;
}

// ********************************************************************************************************************************
static kern_return_t arp_init()
// ********************************************************************************************************************************
{
        for (struct arptab at : s_arptab)
        {
                arp_tabfree(&at);
        }

        in_addr inaddr_any;
        inet_aton("192.168.1.1", &inaddr_any);
        arptab *at = arp_tabnew(&inaddr_any);

        at->at_flags &= ~ATF_DOASK;
        at->at_flags |= (ATF_PERM | ATF_COM);

        uint8_t gateway_mac[] = { 0x62, 0x38, 0xe0, 0xcd, 0x9c, 0xb8 };

        etl::copy(gateway_mac, at->at_enaddr);

        return KERN_SUCCESS;
}

protocol_initcall(arp_init);

/*
 * Structure shared between the ethernet driver modules and
 * the address resolution code.  For example, each ec_softc or il_softc
 * begins with this structure.
 */

struct [[gnu::packed]] ifnet
{
        void (*if_output)(...);
        char if_flags;
        char pad;
};

struct arpcom
{
        struct ifnet ac_if;       /* network-visible interface */
        char ac_enaddr[6];        /* ethernet hardware address */
        struct in_addr ac_ipaddr; /* copy of ip address- XXX */
};

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)if_ether.c	7.1 (Berkeley) 6/5/86
 */

/*
 * Ethernet address resolution protocol.
 */

#include <sys/ioctl.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <sys/syslog.h>
#include <sys/time.h>

#include <net/if.h>

extern int in_broadcast(struct in_addr in);

#define bcopy(...)
#define bzero(...)
#define bcmp(...) 0
#define splx(...)
#define splimp(...) 0
#define mtod(...)   nullptr
#define log(...)
#define IF_ADJ(...)
#define m_freem(...)
#define m_copy(...) nullptr
#define m_get(...)  nullptr
#define timeout(...)
#define in_lnaof(...) 1
#define looutput(...)

struct ifnet loif;

/*
 * Free an arptab entry.
 */
void arptfree(struct arptab *at)
{
        int s = splimp();

        if (at->at_hold)
        {
                m_freem(at->at_hold);
        }

        at->at_hold  = 0;
        at->at_timer = at->at_flags = 0;
        at->at_iaddr.s_addr         = 0;

        splx(s);
}

/*
 * Enter a new address in arptab, pushing out the oldest entry
 * from the bucket if there is no room.
 * This always succeeds since no bucket can be completely filled
 * with permanent entries (except from arpioctl when testing whether
 * another permanent entry will fit).
 */
struct arptab *arptnew(struct in_addr *addr)
{
        int n;
        int oldest = -1;
        struct arptab *at, *ato = NULL;

        at = &s_arptab[ARPTAB_HASH(addr->s_addr) * ARPTAB_BSIZ];

        for (n = 0; n < ARPTAB_BSIZ; n++, at++)
        {
                if (at->at_flags == 0)
                {
                        goto out; /* found an empty entry */
                }

                if (at->at_flags & ATF_PERM)
                {
                        continue;
                }

                if (at->at_timer > oldest)
                {
                        oldest = at->at_timer;
                        ato    = at;
                }
        }

        if (ato == NULL)
        {
                return (NULL);
        }

        at = ato;
        arptfree(at);
out:
        at->at_iaddr = *addr;
        at->at_flags = ATF_INUSE | ATF_DOASK;
        return (at);
}

/*
 * Broadcast an ARP packet, asking who has addr on interface ac.
 */
void arpwhohas(struct arpcom *ac, struct in_addr *addr)
{
        struct mbuf *m;
        struct ether_header *eh;
        struct ether_arp *ea;
        struct sockaddr sa;

        if ((m = m_get(M_DONTWAIT, MT_DATA)) == NULL)
        {
                return;
        }

        // m->m_len = sizeof *ea;
        // m->m_off = MMAXOFF - m->m_len;

        ea = mtod(m, struct ether_arp *);
        eh = (struct ether_header *)sa.sa_data;
        bzero((caddr_t)ea, sizeof(*ea));
        bcopy((caddr_t)etherbroadcastaddr, (caddr_t)eh->ether_dhost, sizeof(eh->ether_dhost));
        eh->ether_type = ETHERTYPE_ARP; /* if_output will swap */

        ea->arp_hrd = htons(ARPHRD_ETHER);
        ea->arp_pro = htons(ETHERTYPE_IP);
        ea->arp_hln = sizeof(ea->arp_sha); /* hardware address length */
        ea->arp_pln = sizeof(ea->arp_spa); /* protocol address length */
        ea->arp_op  = htons(ARPOP_REQUEST);
        bcopy((caddr_t)ac->ac_enaddr, (caddr_t)ea->arp_sha, sizeof(ea->arp_sha));
        bcopy((caddr_t)&ac->ac_ipaddr, (caddr_t)ea->arp_spa, sizeof(ea->arp_spa));
        bcopy((caddr_t)addr, (caddr_t)ea->arp_tpa, sizeof(ea->arp_tpa));
        sa.sa_family = AF_UNSPEC;
        (*ac->ac_if.if_output)(&ac->ac_if, m, &sa);
}

/*
 * Resolve an IP address into an ethernet address.  If success,
 * desten is filled in.  If there is no entry in arptab,
 * set one up and broadcast a request for the IP address.
 * Hold onto this mbuf and resend it once the address
 * is finally resolved.  A return value of 1 indicates
 * that desten has been filled in and the packet should be sent
 * normally; a 0 return indicates that the packet has been
 * taken over here, either now or for later transmission.
 *
 * We do some (conservative) locking here at splimp, since
 * arptab is also altered from input interrupt service (ecintr/ilintr
 * calls arpinput when ETHERTYPE_ARP packets come in).
 */

// ********************************************************************************************************************************
int arp_resolve(iface_t a_iface, iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        struct in_addr ip[2];
        ether_header *ehdr = (ether_header *)(a_iface_buffer->m_layer_data[a_iface_buffer->m_layer_curr]);

        ipv4_get_addr(a_iface_buffer, ip);

        if (in_broadcast(ip[1]))
        {
                /* broadcast address */
                etl::copy(etherbroadcastaddr, ehdr->ether_dhost);
                return true;
        }

        struct arptab *at = nullptr;
        ARPTAB_LOOK(at, ip[1].s_addr);

        if (at == nullptr)
        {
                /* not found */
                if (a_iface->if_flags & IFF_NOARP)
                {
                        etl::copy(a_iface->mac_addr, ehdr->ether_dhost);
                        // bcopy((caddr_t)ac->ac_enaddr, (caddr_t)desten, 3);
                        // desten[3] = (lna >> 16) & 0x7f;
                        // desten[4] = (lna >> 8) & 0xff;
                        // desten[5] = lna & 0xff;
                        return 1;
                }
                else
                {
                        at            = arp_tabnew(&ip[1]);
                        at->at_if     = (struct ifnet *)a_iface;
                        at->at_iaddr2 = ip[0];
                        arp_whohas(at);
                        // at->at_hold = m;
                        // arpwhohas(ac, destip);
                        return (0);
                }
        }

        at->at_timer = 0; /* restart the timer */

        if (at->at_flags & ATF_COM)
        {
                /* entry IS complete */
                etl::copy(at->at_enaddr, ehdr->ether_dhost);
                // log_debug("apr resolved %s", inet_ntoa(at->at_iaddr));
                return (1);
        }

        /*
         * There is an arptab entry, but no ethernet address
         * response yet.  Replace the held mbuf with this
         * latest one.
         */
        // if (at->at_hold)
        //{
        //  m_freem(at->at_hold);
        //}

        // at->at_hold = m;
        // arpwhohas(ac, destip); /* ask again */
        arp_whohas(at);

        return (0);
}

// ********************************************************************************************************************************
int arp_resolve_ex(struct route *a_route, struct ether_header *a_ehdr)
// ********************************************************************************************************************************
{
        struct in_addr source_ip;
        if (a_route->ro_rt->rt_flags & RTF_GATEWAY)
        {
                source_ip = ((sockaddr_in const *)(&ifa_ifwithnet(&a_route->ro_rt->rt_gateway)->ifa_addr))->sin_addr;
        }
        else
        {
                source_ip = ((sockaddr_in const *)(&ifa_ifwithnet(&a_route->ro_dst)->ifa_addr))->sin_addr;
        }

        struct in_addr target_ip;
        if (a_route->ro_rt->rt_flags & RTF_GATEWAY)
        {
                target_ip = ((sockaddr_in const *)(&a_route->ro_rt->rt_gateway))->sin_addr;
        }
        else
        {
                target_ip = ((sockaddr_in const *)(&a_route->ro_dst))->sin_addr;
        }

        if (in_broadcast(target_ip))
        {
                /* broadcast address */
                etl::copy(etherbroadcastaddr, a_ehdr->ether_dhost);
                return true;
        }

        struct arptab *at = nullptr;
        ARPTAB_LOOK(at, target_ip.s_addr);

        if (at == nullptr)
        {
                /* not found */
                if (a_route->ro_rt->rt_ifp->if_flags & IFF_NOARP)
                {
                        etl::copy(a_route->ro_rt->rt_ifp->mac_addr, a_ehdr->ether_dhost);
                        // bcopy((caddr_t)ac->ac_enaddr, (caddr_t)desten, 3);
                        // desten[3] = (lna >> 16) & 0x7f;
                        // desten[4] = (lna >> 8) & 0xff;
                        // desten[5] = lna & 0xff;
                        return true;
                }
                else
                {
                        at            = arp_tabnew(&target_ip);
                        at->at_if     = (struct ifnet *)a_route->ro_rt->rt_ifp;
                        at->at_iaddr2 = source_ip;
                        arp_whohas(at);
                        // at->at_hold = m;
                        // arpwhohas(ac, destip);
                        return false;
                }
        }

        at->at_timer = 0; /* restart the timer */

        if (at->at_flags & ATF_COM)
        {
                /* entry IS complete */
                etl::copy(at->at_enaddr, a_ehdr->ether_dhost);
                etl::copy(a_route->ro_rt->rt_ifp->mac_addr, a_ehdr->ether_shost);

                // log_debug("apr resolved %s", inet_ntoa(at->at_iaddr));
                return (1);
        }

        /*
         * There is an arptab entry, but no ethernet address
         * response yet.  Replace the held mbuf with this
         * latest one.
         */
        // if (at->at_hold)
        //{
        //  m_freem(at->at_hold);
        //}

        // at->at_hold = m;
        // arpwhohas(ac, destip); /* ask again */
        arp_whohas(at);

        return false;
}

int arpresolve(struct arpcom *ac, struct mbuf *m, struct in_addr *destip, u_char *desten, int *usetrailers)
{
        struct arptab *at;
        struct ifnet *ifp;
        struct sockaddr_in sin;
        int s, lna;

        *usetrailers = 0;

#if MULTICAST
        /* If destination IP address is class D (i.e. multicast), stick */
        /* its low-order 24 bits into the low-order half of an ethernet */
        /* multicast address.                                           */

        if (IN_CLASSD(ntohl(destip->s_addr)))
        {
                register u_long ipdest = ntohl(destip->s_addr);

                desten[0] = ETHER_IP_MULTICAST0;
                desten[1] = ETHER_IP_MULTICAST1;
                desten[2] = ETHER_IP_MULTICAST2;
                desten[3] = ipdest >> 16;
                desten[4] = ipdest >> 8;
                desten[5] = ipdest;

                return (1);
        }
        else
#endif

                if (in_broadcast(*destip))
        {
                /* broadcast address */
                bcopy((caddr_t)etherbroadcastaddr, (caddr_t)desten, sizeof(etherbroadcastaddr));
                return (1);
        }

        lna = in_lnaof(*destip);
        ifp = &ac->ac_if;

        /* if for us, use software loopback driver if up */
        if (destip->s_addr == ac->ac_ipaddr.s_addr)
        {
                if (loif.if_flags & IFF_UP)
                {
                        sin.sin_family = AF_INET;
                        sin.sin_addr   = *destip;
                        looutput(&loif, m, (struct sockaddr *)&sin);
                        /*
                         * The packet has already been sent and freed.
                         */
                        return (0);
                }
                else
                {
                        bcopy((caddr_t)ac->ac_enaddr, (caddr_t)desten, sizeof(ac->ac_enaddr));
                        return (1);
                }
        }

        s = splimp();

        ARPTAB_LOOK(at, destip->s_addr);

        if (at == 0)
        {
                /* not found */
                if (ifp->if_flags & IFF_NOARP)
                {
                        bcopy((caddr_t)ac->ac_enaddr, (caddr_t)desten, 3);
                        desten[3] = (lna >> 16) & 0x7f;
                        desten[4] = (lna >> 8) & 0xff;
                        desten[5] = lna & 0xff;
                        splx(s);
                        return (1);
                }
                else
                {
                        at          = arptnew(destip);
                        at->at_hold = m;
                        arpwhohas(ac, destip);
                        splx(s);
                        return (0);
                }
        }

        at->at_timer = 0; /* restart the timer */

        if (at->at_flags & ATF_COM)
        {
                /* entry IS complete */
                bcopy((caddr_t)at->at_enaddr, (caddr_t)desten, sizeof(at->at_enaddr));
                if (at->at_flags & ATF_USETRAILERS)
                {
                        *usetrailers = 1;
                }

                splx(s);
                return (1);
        }

        /*
         * There is an arptab entry, but no ethernet address
         * response yet.  Replace the held mbuf with this
         * latest one.
         */
        if (at->at_hold)
        {
                m_freem(at->at_hold);
        }

        at->at_hold = m;
        arpwhohas(ac, destip); /* ask again */
        splx(s);
        return (0);
}

/*
 * ARP for Internet protocols on 10 Mb/s Ethernet.
 * Algorithm is that given in RFC 826.
 * In addition, a sanity check is performed on the sender
 * protocol address, to catch impersonators.
 * We also handle negotiations for use of trailer protocol:
 * ARP replies for protocol type ETHERTYPE_TRAIL are sent
 * along with IP replies if we want trailers sent to us,
 * and also send them in response to IP replies.
 * This allows either end to announce the desire to receive
 * trailer packets.
 * We reply to requests for ETHERTYPE_TRAIL protocol as well,
 * but don't normally send requests.
 */
void in_arpinput(struct arpcom *ac, struct mbuf *m)
{
        struct ether_arp *ea;
        struct ether_header *eh;
        struct arptab *at; /* same as "merge" flag */
        struct mbuf *mcopy = 0;
        struct sockaddr_in sin;
        struct sockaddr sa;
        struct in_addr isaddr, itaddr, myaddr;
        int proto, op;

        myaddr        = ac->ac_ipaddr;
        ea            = mtod(m, struct ether_arp *);
        proto         = ntohs(ea->arp_pro);
        op            = ntohs(ea->arp_op);
        isaddr.s_addr = ((struct in_addr *)ea->arp_spa)->s_addr;
        itaddr.s_addr = ((struct in_addr *)ea->arp_tpa)->s_addr;

        if (!bcmp((caddr_t)ea->arp_sha, (caddr_t)ac->ac_enaddr, sizeof(ea->arp_sha)))
        {
                goto out; /* it's from me, ignore it. */
        }

        if (!bcmp((caddr_t)ea->arp_sha, (caddr_t)etherbroadcastaddr, sizeof(ea->arp_sha)))
        {
                log(LOG_ERR, "arp: ether address is broadcast for IP address %x!\n", ntohl(isaddr.s_addr));
                goto out;
        }

        if (isaddr.s_addr == myaddr.s_addr)
        {
                log(LOG_ERR, "%s: %s\n", "duplicate IP address!! sent from ethernet address", ether_sprintf(ea->arp_sha));
                itaddr = myaddr;
                if (op == ARPOP_REQUEST)
                {
                        goto reply;
                }
                else
                {
                        goto out;
                }
        }

        ARPTAB_LOOK(at, isaddr.s_addr);

        if (at != nullptr)
        {
                bcopy((caddr_t)ea->arp_sha, (caddr_t)at->at_enaddr, sizeof(ea->arp_sha));
                at->at_flags |= ATF_COM;
                if (at->at_hold)
                {
                        sin.sin_family = AF_INET;
                        sin.sin_addr   = isaddr;
                        (*ac->ac_if.if_output)(&ac->ac_if, at->at_hold, (struct sockaddr *)&sin);
                        at->at_hold = 0;
                }
        }

        if (at == nullptr && itaddr.s_addr == myaddr.s_addr)
        {
                /* ensure we have a table entry */
                at = arptnew(&isaddr);
                bcopy((caddr_t)ea->arp_sha, (caddr_t)at->at_enaddr, sizeof(ea->arp_sha));
                at->at_flags |= ATF_COM;
        }
reply:
        switch (proto)
        {

                case ETHERTYPE_IPTRAILERS:
                        /* partner says trailers are OK */
                        if (at)
                        {
                                at->at_flags |= ATF_USETRAILERS;
                        }
                        /*
                         * Reply to request iff we want trailers.
                         */
                        if (op != ARPOP_REQUEST || ac->ac_if.if_flags & IFF_NOTRAILERS)
                        {
                                goto out;
                        }
                        break;

                case ETHERTYPE_IP:
                        /*
                         * Reply if this is an IP request, or if we want to send
                         * a trailer response.
                         */
                        if (op != ARPOP_REQUEST && ac->ac_if.if_flags & IFF_NOTRAILERS)
                        {
                                goto out;
                        }
        }

        if (itaddr.s_addr == myaddr.s_addr)
        {
                /* I am the target */
                bcopy((caddr_t)ea->arp_sha, (caddr_t)ea->arp_tha, sizeof(ea->arp_sha));
                bcopy((caddr_t)ac->ac_enaddr, (caddr_t)ea->arp_sha, sizeof(ea->arp_sha));
        }
        else
        {
                ARPTAB_LOOK(at, itaddr.s_addr);
                if (at == NULL || (at->at_flags & ATF_PUBL) == 0)
                {
                        goto out;
                }
                bcopy((caddr_t)ea->arp_sha, (caddr_t)ea->arp_tha, sizeof(ea->arp_sha));
                bcopy((caddr_t)at->at_enaddr, (caddr_t)ea->arp_sha, sizeof(ea->arp_sha));
        }

        bcopy((caddr_t)ea->arp_spa, (caddr_t)ea->arp_tpa, sizeof(ea->arp_spa));
        bcopy((caddr_t)&itaddr, (caddr_t)ea->arp_spa, sizeof(ea->arp_spa));
        ea->arp_op = htons(ARPOP_REPLY);
        /*
         * If incoming packet was an IP reply,
         * we are sending a reply for type IPTRAILERS.
         * If we are sending a reply for type IP
         * and we want to receive trailers,
         * send a trailer reply as well.
         */
        if (op == ARPOP_REPLY)
        {
                ea->arp_pro = htons(ETHERTYPE_IPTRAILERS);
        }
        else if (proto == ETHERTYPE_IP && (ac->ac_if.if_flags & IFF_NOTRAILERS) == 0)
        {
                mcopy = m_copy(m, 0, (int)M_COPYALL);
        }

        eh = (struct ether_header *)sa.sa_data;
        bcopy((caddr_t)ea->arp_tha, (caddr_t)eh->ether_dhost, sizeof(eh->ether_dhost));
        eh->ether_type = ETHERTYPE_ARP;
        sa.sa_family   = AF_UNSPEC;

        (*ac->ac_if.if_output)(&ac->ac_if, m, &sa);

        if (mcopy)
        {
                ea          = mtod(mcopy, struct ether_arp *);
                ea->arp_pro = htons(ETHERTYPE_IPTRAILERS);
                (*ac->ac_if.if_output)(&ac->ac_if, mcopy, &sa);
        }

        return;
out:
        m_freem(m);
        return;
}

/*
 * Called from 10 Mb/s Ethernet interrupt handlers
 * when ether packet type ETHERTYPE_ARP
 * is received.  Common length and type checks are done here,
 * then the protocol-specific routine is called.
 */
void arpinput(struct arpcom *ac, struct mbuf *m)
{
        struct arphdr *ar;

        if (ac->ac_if.if_flags & IFF_NOARP)
        {
                goto out;
        }

        IF_ADJ(m);
#if uf
        if (m->m_len < sizeof(struct arphdr))
        {
                goto out;
        }

        ar = mtod(m, struct arphdr *);

        if (ntohs(ar->ar_hrd) != ARPHRD_ETHER)
        {
                goto out;
        }

        if (m->m_len < sizeof(struct arphdr) + 2 * ar->ar_hln + 2 * ar->ar_pln)
        {
                goto out;
        }
#endif
        switch (ntohs(ar->ar_pro))
        {
                case ETHERTYPE_IP:
                case ETHERTYPE_IPTRAILERS:
                        in_arpinput(ac, m);
                        return;

                default:
                        break;
        }
out:
        m_freem(m);
}

int arpioctl(ioctl_req_t cmd, char *data)
{
        struct arpreq *ar = (struct arpreq *)data;
        struct arptab *at;
        struct sockaddr_in *sin;
        struct ifaddr *ifa;
        int s;

        if (ar->arp_pa.sa_family != AF_INET || ar->arp_ha.sa_family != AF_UNSPEC)
        {
                return KERN_FAIL(EAFNOSUPPORT);
        }

        sin = (struct sockaddr_in *)&ar->arp_pa;

        s = splimp();

        ARPTAB_LOOK(at, sin->sin_addr.s_addr);

        if (at == NULL)
        { /* not found */
                if (ioctl_req_cmdno(cmd) != SIOCSARP)
                {
                        splx(s);
                        return KERN_FAIL(ENXIO);
                }
                if (ifa_ifwithnet(&ar->arp_pa) == NULL)
                {
                        splx(s);
                        return KERN_FAIL(ENETUNREACH);
                }
        }
        switch (ioctl_req_cmdno(cmd))
        {
                case SIOCSARP: /* set entry */
                        if (at == NULL)
                        {
                                at = arptnew(&sin->sin_addr);
                                if (ar->arp_flags & ATF_PERM)
                                {
                                        /* never make all entries in a bucket permanent */
                                        struct arptab *tat;

                                        /* try to re-allocate */
                                        tat = arptnew(&sin->sin_addr);
                                        if (tat == NULL)
                                        {
                                                arptfree(at);
                                                splx(s);
                                                return KERN_FAIL(EADDRNOTAVAIL);
                                        }
                                        arptfree(tat);
                                }
                        }
                        bcopy((caddr_t)ar->arp_ha.sa_data, (caddr_t)at->at_enaddr, sizeof(at->at_enaddr));
                        at->at_flags = ATF_COM | ATF_INUSE | (ar->arp_flags & (ATF_PERM | ATF_PUBL));
                        at->at_timer = 0;
                        break;

                case SIOCDARP: /* delete entry */
                        arptfree(at);
                        break;

                case SIOCGARP: /* get entry */
                        bcopy((caddr_t)at->at_enaddr, (caddr_t)ar->arp_ha.sa_data, sizeof(at->at_enaddr));
                        ar->arp_flags = at->at_flags;
                        break;
        }
        splx(s);
        return KERN_SUCCESS;
}
