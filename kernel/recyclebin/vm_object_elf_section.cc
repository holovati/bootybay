#include <etl/algorithm.hh>

#include "vm_object_elf_section.hh"

ZONE_DEFINE(vm_object_elf_section, s_vm_object_elf_section_zone, 0x1000, 0x80, ZONE_FIXED, 32);

kern_return_t vm_object_elf_section::get_page(vm_offset_t a_offset, vm_prot_t a_prot, vm_page_t *a_vm_page_out)
{
        vnode::locked_ptr lll{m_vnode};

        KASSERT(m_vnode->islocked());

        // Check if we have the page at the given offset
        *a_vm_page_out = vm_page_map_lookup(&m_page_map, a_offset);

        // If we do, return it.
        if (*a_vm_page_out)
        {
                // We don't set COW on our own pages
                (*a_vm_page_out)->flags &= ~PG_COPYONWRITE;
                return KERN_SUCCESS;
        }

        // We don't have the page.

        // Is the offset within the vnode range
        kern_return_t result = KERN_FAILURE;
        if (m_pages_expected > 0)
        {
                if (a_offset < round_page(m_file_size + (m_file_offset & PAGE_MASK)))
                {
                        vm_offset_t copy_offset = 0;

                        if (a_offset == 0)
                        {
                                copy_offset = m_file_offset & PAGE_MASK;
                        }

                        vm_offset_t copy_size = etl::min(static_cast<Elf64_Xword>(PAGE_SIZE) - copy_offset,
                                                         m_file_size + (m_file_offset & PAGE_MASK) - a_offset - copy_offset);

                        result = m_vnode->get_page(trunc_page(m_file_offset) + a_offset, a_prot, a_vm_page_out);

                        if (result == KERN_SUCCESS)
                        {
                                if (copy_offset != 0 || copy_size != PAGE_SIZE)
                                {
                                        // Lastly clone the page
                                        vm_page_t vnode_page = *a_vm_page_out;

                                        *a_vm_page_out = vm_page_alloc(&m_page_map, a_offset);

                                        KASSERT(*a_vm_page_out != vnode_page);

                                        (*a_vm_page_out)->flags &= ~(PG_BUSY | PG_FAKE);

                                        (*a_vm_page_out)->zero_fill();

                                        vm_page_copy(*a_vm_page_out, vnode_page, copy_offset, copy_size);

                                        m_pages_expected--;
                                }
                        }
                }
                else
                {
                        KASSERT(a_offset < round_page(m_size));
                        // The offset is not in the file, so we return a zero page and set it to COW.(a_offset is guaranteed to be
                        // within the range of this object)

                        vm_page_t zero_page;
                        result = vm_object::get_zero_object()->get_page(0, a_prot, &zero_page);

                        KASSERT(result == KERN_SUCCESS && zero_page != nullptr);

                        *a_vm_page_out = vm_page_alloc(&m_page_map, a_offset);

                        (*a_vm_page_out)->flags &= ~(PG_BUSY | PG_FAKE);

                        vm_page_copy(*a_vm_page_out, zero_page, 0, PAGE_SIZE);

                        m_pages_expected--;
                }
        }
        else
        {
                result = m_vnode->get_page(trunc_page(m_file_offset) + a_offset, a_prot, a_vm_page_out);
        }

        KASSERT(result == KERN_SUCCESS);

        return result;
}

bool vm_object_elf_section::collapse(vm_object *a_destination, vm_object **a_backing_object, vm_offset_t *a_backing_object_offset)
{
        bool can_collapse
            = a_destination != nullptr && m_ref_count == 1 && 0 == m_pages_expected && a_destination->m_size == m_size;

        if (can_collapse)
        {
                // The pages we do not transfer to the designation object will be freed when er use_count drops to zero.
                // The caller has the responsibilty to call unreference on us.
                vm_page_t vmp_next = nullptr;
                for (vm_page_t vmp = vm_page_map_first(&m_page_map); vmp != nullptr; vmp = vmp_next)
                {
                        vmp_next = vm_page_map_next(vmp);
                        vm_page_rename(&m_page_map, vmp, &a_destination->m_page_map, vmp->offset);
                }

                if (a_backing_object)
                {
                        KASSERT(a_backing_object_offset != nullptr);
                        *a_backing_object = m_vnode.get_raw();
                        (*a_backing_object)->reference();
                        *a_backing_object_offset = trunc_page(m_file_offset);
                }
        }

        return can_collapse;
}

void vm_object_elf_section::do_unreference()
{
        m_vnode = nullptr;

        // No more references, deallocate the used pages and destroy the context
        vm_page_map_clear(&m_page_map);
        s_vm_object_elf_section_zone.free_delete(this);
}

vm_object_elf_section::vm_object_elf_section(Elf64_Phdr *a_phdr, vnode *a_vnode)
    : vm_object(round_page(a_phdr->p_memsz + (a_phdr->p_vaddr & PAGE_MASK)))
    , m_vnode(a_vnode)
    , m_ref_count(1)
    , m_file_size(a_phdr->p_filesz)
    , m_file_offset(a_phdr->p_offset)
    , m_pages_expected(0)
{
        // We need to fix the head
        if (m_file_size < PAGE_SIZE)
        {
                m_pages_expected += (m_file_offset & PAGE_MASK) != 0 || (m_file_size & PAGE_MASK);
        }
        else
        {
                // We need to fix the tail
                m_pages_expected += (m_file_offset & PAGE_MASK) != 0;
                m_pages_expected += (m_file_size & PAGE_MASK) != 0;
        }

        if (size_t anon_size = round_page(m_size) - round_page(m_file_size); anon_size > 0)
        {
                m_pages_expected += anon_size >> PAGE_SHIFT;
        }
}

bool vm_object_elf_section::do_extend(size_t)
{
        // We cannot be extended
        return false;
}

vm_object *vm_object_elf_section::create(Elf64_Phdr *a_phdr, vnode *a_vnode)
{
        vm_object *obj = s_vm_object_elf_section_zone.alloc_new(a_phdr, a_vnode);
        obj->reference(); // We need one ref
        return obj;
}
