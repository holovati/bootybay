#include <emerixx/file.h>
#include <fcntl.h>
#include <mach/errno.h>
#include <vfs/mount.h>

#include <vfs/vnode.h>

#if DAMIR
/*
 * Package up an I/O request on a vnode into a uio and do it.
 * Return memory but don't cache it.
 * Like vn_rdwr but doesn't do cache consistency calls here
 */
int vn_pageinout(enum uio_rw rw,
                 struct vnode::locked_ptr &vp,
                 char *base,
                 int len,
                 off_t offset,
                 enum uio_seg segflg,
                 int ioflg,
                 struct ucred *cred,
                 int *aresid,
                 pthread_t p)
{
        struct uio auio;
        struct iovec aiov;
        int error;

        auio.uio_iov    = &aiov;
        auio.uio_iovcnt = 1;
        aiov.iov_base   = base;
        aiov.iov_len    = len;
        auio.uio_resid  = len;
        auio.uio_offset = offset;
        auio.uio_segflg = segflg;
        auio.uio_rw     = rw;
        auio.uio_procp  = p;
        if (rw == UIO_READ)
        {
                error = vp->read(&auio, ioflg, cred);
        }
        else
        {
                error = vp->write(&auio, ioflg, cred);
        }
        if (aresid)
        {
                *aresid = auio.uio_resid;
        }
        else if (auio.uio_resid && error == 0)
        {
                error = -EIO;
        }

        return error;
}

/*
 * Pagein from file.
 * Return memory that the caller is expected to consume.
 * If the call fails, then no memory is returned.
 *
 * If less than asked for was read, the amount of memory originally
 * requested is still returned. XXX. This needs to be considered.
 *
 * It should be possible to return either more or less memory than
 * requested. This would make efficient read ahead possible in some
 * cases.  Also it might be nicer to let the file system push up data
 * on its own.
 *
 * This is a temporary implementation until a more suitable VFS
 * interface is added (the driver should provide the memory, not this
 * function).
 */

mach_error_t XXXvn_pagein(struct vnode *vp,
                          off_t offset,
                          int length,
                          struct ucred *cred,
                          pthread_t p,
                          vm_address_t *address, /* OUT */
                          vm_size_t *bytes_read) /* OUT */
{
        struct uio auio;
        struct iovec aiov;
        mach_error_t kr;
        vm_address_t addr;
        int residue;

        kr = vm_allocate(mach_task_self(), &addr, length, TRUE);
        assert(kr == KERN_SUCCESS);
        if (kr)
                return kr;

        VOP_LOCK(vp);
        auio.uio_iov    = &aiov;
        auio.uio_iovcnt = 1;
        aiov.iov_base   = (char *)addr;
        aiov.iov_len    = length;
        auio.uio_resid  = length;
        auio.uio_offset = offset;
        auio.uio_segflg = UIO_SYSSPACE;
        auio.uio_rw     = UIO_READ;
        auio.uio_procp  = p;

        kr      = VOP_READ(vp, &auio, IO_UNIT, cred);
        residue = auio.uio_resid;
        VOP_UNLOCK(vp);

        if (kr == KERN_SUCCESS)
        {
                *bytes_read = length - auio.uio_resid;
        }
        else
        {
                vm_deallocate(mach_task_self(), addr, length);
        }
        return kr;
}

/*
 * Pageout to file.
 * Consumes the memory (rounded up to page boundary).
 *
 * If the call fails, the memory is still consumed XXX needs thinking.
 *
 * This is a temporary implementation until a more suitable VFS
 * interface is added (the driver should consume the memory, not this
 * function).
 *
 * XXX remember that offset does not have to be aligned either.  This
 * is the case if a misaligned vm_mapping was established.
 */
mach_error_t XXXvn_pageout(struct vnode *vp,
                           off_t offset,
                           int length, /* does not have to be page rounded */
                           struct ucred *cred,
                           pthread_t p,
                           vm_address_t address,
                           vm_size_t *bytes_written) /* OUT */
{
        struct uio auio;
        struct iovec aiov;
        mach_error_t kr;
        vm_address_t addr;
        int residue;

        VOP_LOCK(vp);
        auio.uio_iov    = &aiov;
        auio.uio_iovcnt = 1;
        aiov.iov_base   = (char *)address;
        aiov.iov_len    = length;
        auio.uio_resid  = length;
        auio.uio_offset = offset;
        auio.uio_segflg = UIO_SYSSPACE;
        auio.uio_rw     = UIO_WRITE;
        auio.uio_procp  = p;

        kr      = VOP_WRITE(vp, &auio, IO_UNIT, cred);
        residue = auio.uio_resid;
        VOP_UNLOCK(vp);

        if (kr == KERN_SUCCESS)
        {
                *bytes_written = length - auio.uio_resid;
        }
        vm_deallocate(mach_task_self(), address, length);
        return kr;
}
#endif