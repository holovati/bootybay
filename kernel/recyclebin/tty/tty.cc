// clang-format off

/*-
 * Copyright (c) 1982, 1986, 1990, 1991, 1993
 *	The Regents of the University of California.  All rights reserved.
 * (c) UNIX System Laboratories, Inc.
 * All or some portions of this file are derived from material licensed
 * to the University of California by American Telephone and Telegraph
 * Co. or Unix System Laboratories, Inc. and are reproduced herein with
 * the permission of UNIX System Laboratories, Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)tty.c	8.8 (Berkeley) 1/21/94
 */
/* HISTORY
 * 12-Dec-94  Ian Dall (dall@hfrd.dsto.gov.au)
 *	Remove code for old commands in ttioctl(). ttcompat converts old form
 *	cmds to new form cmds and calls ttioctl again.
 *
 */
#include <mach/conf.h>
#include <mach/param.h>
// #include <sys/dkstat.h>
// #include <mach/kernel.h>
// #include <mach/sys/select.h>
// #include <sys/syslog.h>
// #include <sys/synch.h>

// #include <vm/vm.h>

#include <signal.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/ttydefaults.h>
#define TTYDEFCHARS
#define __NEED_struct_winsize
#include "tty.h"
#undef TTYDEFCHARS

#include <emerixx/vfs/vfs_file.h>
#include <aux/defer.hh>
#include <mach/machine/spl.h>
#include <emerixx/memory_rw.h>
#include <emerixx/ioctl_req.h>

#include "clist.h"
#include <emerixx/process.h>
#include <emerixx/sleep.h>

#include <etl/algorithm.hh>

#include <fcntl.h>

#define IO_NDELAY BIT0

#define _FREAD  00001 /* descriptor read/receive'able */
#define _FWRITE 00002 /* descriptor write/send'able */

static int scanc(size_t size, u8 *cp, u8 table[], int mask0)
{
        u8 *end;
        int mask;

        mask = mask0;
        for (end = &cp[size]; cp != end && (table[*cp] & mask) == 0; ++cp)
                ;
        return (end - cp);
}
/*
 * Ttyprintf displays a message on a tty; it should be used only by
 * the tty driver, or anything that knows the underlying tty will not
 * be revoke(2)'d away.  Other callers should use tprintf.
 */
void ttyprintf(struct tty *tp, const char *fmt, ...)
{
        panic("STUB %s\n", __PRETTY_FUNCTION__);

#if DAMIR
        va_list ap;

        va_start(ap, fmt);
        kprintf(fmt, TOTTY, tp, ap);
        va_end(ap);
#endif
}

#define VSTATUS 18 /* ICANON */

struct tty *constty; /* pointer to console "window" tty */

#define ALTWERASE 0x00000200 /* use alternate WERASE algorithm */

#define NOKERNINFO 0x02000000 /* no kernel output from VSTATUS */

#define OXTABS 0x00000004 /* expand tabs to spaces */

/*-------------*/

#define CP_USER   0
#define CP_NICE   1
#define CP_SYS    2
#define CP_INTR   3
#define CP_IDLE   4
#define CPUSTATES 5

#define DK_NDRIVE 8
long cp_time[CPUSTATES];
long dk_seek[DK_NDRIVE];
long dk_time[DK_NDRIVE];
long dk_wds[DK_NDRIVE];
long dk_wpms[DK_NDRIVE];
long dk_xfer[DK_NDRIVE];

int dk_busy;
int dk_ndrive;

long tk_cancc;
long tk_nin;
long tk_nout;
long tk_rawcc;

/*---------------*/

static int proc_compare(pthread_t p1, pthread_t p2);
static int ttnread(struct tty *);
static void ttyblock(struct tty *tp);
static void ttyecho(int, struct tty *tp);
static void ttyrubo(struct tty *, int);

/* Symbolic sleep message strings. */
char ttclos[] = "ttycls";
char ttopen[] = "ttyopn";
char ttybg[]  = "ttybg";
char ttybuf[] = "ttybuf";
char ttyin[]  = "ttyin";
char ttyout[] = "ttyout";

/*
 * Table with character classes and parity. The 8th bit indicates parity,
 * the 7th bit indicates the character is an alphameric or underscore (for
 * ALTWERASE), and the low 6 bits indicate delay type.  If the low 6 bits
 * are 0 then the character needs no special processing on output; classes
 * other than 0 might be translated or (not currently) require delays.
 */
#define E         0x00 /* Even parity. */
#define O         0x80 /* Odd parity. */
#define PARITY(c) (char_type[c] & O)

#define ALPHA      0x40 /* Alpha or underscore. */
#define ISALPHA(c) (char_type[(c)&TTY_CHARMASK] & ALPHA)

#define CCLASSMASK 0x3f
#define CCLASS(c)  (char_type[c] & CCLASSMASK)

#define BS BACKSPACE
#define CC CONTROL
#define CR RETURN
#define NA ORDINARY | ALPHA
#define NL NEWLINE
#define NO ORDINARY
#define TB TAB
#define VT VTAB

unsigned char const char_type[] = {
    E | CC,
    O | CC,
    O | CC,
    E | CC,
    O | CC,
    E | CC,
    E | CC,
    O | CC, /* nul - bel */
    O | BS,
    E | TB,
    E | NL,
    O | CC,
    E | VT,
    O | CR,
    O | CC,
    E | CC, /* bs - si */
    O | CC,
    E | CC,
    E | CC,
    O | CC,
    E | CC,
    O | CC,
    O | CC,
    E | CC, /* dle - etb */
    E | CC,
    O | CC,
    O | CC,
    E | CC,
    O | CC,
    E | CC,
    E | CC,
    O | CC, /* can - us */
    O | NO,
    E | NO,
    E | NO,
    O | NO,
    E | NO,
    O | NO,
    O | NO,
    E | NO, /* sp - ' */
    E | NO,
    O | NO,
    O | NO,
    E | NO,
    O | NO,
    E | NO,
    E | NO,
    O | NO, /* ( - / */
    E | NA,
    O | NA,
    O | NA,
    E | NA,
    O | NA,
    E | NA,
    E | NA,
    O | NA, /* 0 - 7 */
    O | NA,
    E | NA,
    E | NO,
    O | NO,
    E | NO,
    O | NO,
    O | NO,
    E | NO, /* 8 - ? */
    O | NO,
    E | NA,
    E | NA,
    O | NA,
    E | NA,
    O | NA,
    O | NA,
    E | NA, /* @ - G */
    E | NA,
    O | NA,
    O | NA,
    E | NA,
    O | NA,
    E | NA,
    E | NA,
    O | NA, /* H - O */
    E | NA,
    O | NA,
    O | NA,
    E | NA,
    O | NA,
    E | NA,
    E | NA,
    O | NA, /* P - W */
    O | NA,
    E | NA,
    E | NA,
    O | NO,
    E | NO,
    O | NO,
    O | NO,
    O | NA, /* X - _ */
    E | NO,
    O | NA,
    O | NA,
    E | NA,
    O | NA,
    E | NA,
    E | NA,
    O | NA, /* ` - g */
    O | NA,
    E | NA,
    E | NA,
    O | NA,
    E | NA,
    O | NA,
    O | NA,
    E | NA, /* h - o */
    O | NA,
    E | NA,
    E | NA,
    O | NA,
    E | NA,
    O | NA,
    O | NA,
    E | NA, /* p - w */
    E | NA,
    O | NA,
    O | NA,
    E | NO,
    O | NO,
    E | NO,
    E | NO,
    O | CC, /* x - del */
    /*
     * Meta chars; should be settable per character set;
     * for now, treat them all as normal characters.
     */
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
    NA,
};
#undef BS
#undef CC
#undef CR
#undef NA
#undef NL
#undef NO
#undef TB
#undef VT

/* Macros to clear/set/test flags. */
#define SET(t, f)   (t) |= (f)
#define CLR(t, f)   (t) &= ~(f)
#define ISSET(t, f) ((t) & (f))

/*
 * Initial open of tty, or (re)entry to standard tty line discipline.
 */
int ttyopen(dev_t device, struct tty *tp)
{
        int s;

        s         = spltty();
        tp->t_dev = device;
        if (!ISSET(tp->t_state, TS_ISOPEN))
        {
                SET(tp->t_state, TS_ISOPEN);
                bzero(&tp->t_winsize, sizeof(tp->t_winsize));
        }
        CLR(tp->t_state, TS_WOPEN);
        splx(s);
        return (0);
}

/*
 * Handle close() on a tty line: flush and set to initial state,
 * bumping generation number so that pending read/write calls
 * can detect recycling of the tty.
 */
int ttyclose(struct tty *tp)
{
        extern struct tty *constty; /* Temporary virtual console. */

        if (constty == tp)
                constty = NULL;

        ttyflush(tp, _FREAD | _FWRITE);

        tp->t_gen++;
        // tp->t_pgrp    = NULL;
        tp->t_foreground_group = 0;
        // tp->t_session          = NULL;
        tp->t_session_id     = 0;
        tp->t_session_leader = 0;
        tp->t_state          = 0;
        return (0);
}

#define FLUSHQ(q)                                                                                                                  \
        {                                                                                                                          \
                if ((q)->c_cc)                                                                                                     \
                        ndflush(q, (q)->c_cc);                                                                                     \
        }

/* Is 'c' a line delimiter ("break" character)? */
#define TTBREAKC(c) ((c) == '\n' || ((c) == cc[VEOF] || (c) == cc[VEOL] || (c) == cc[VEOL2]) && (c) != _POSIX_VDISABLE)

/*
 * Process input of a single character received on a tty.
 */
int ttyinput(int c, struct tty *tp)
{
        int iflag, lflag;
        u8 *cc;
        integer_t i, err;

        /*
         * If input is pending take it first.
         */
        lflag = tp->t_lflag;
        if (ISSET(lflag, PENDIN))
        {
                ttypend(tp);
        }

        /*
         * Gather stats.
         */
        if (ISSET(lflag, ICANON))
        {
                ++tk_cancc;
                ++tp->t_cancc;
        }
        else
        {
                ++tk_rawcc;
                ++tp->t_rawcc;
        }
        ++tk_nin;

        /* Handle exceptional conditions (break, parity, framing). */
        cc    = tp->t_cc;
        iflag = tp->t_iflag;
        if ((err = (ISSET(c, TTY_ERRORMASK))))
        {
                CLR(c, TTY_ERRORMASK);
                if (ISSET(err, TTY_FE) && !c)
                {
                        /* Break. */
                        if (ISSET(iflag, IGNBRK))
                        {
                                goto endcase;
                        }
#define _POSIX_VDISABLE 0
                        else if (ISSET(iflag, BRKINT) && ISSET(lflag, ISIG) && (cc[VINTR] != _POSIX_VDISABLE))
                        {
                                c = cc[VINTR];
                        }
                        else if (ISSET(iflag, PARMRK))
                        {
                                goto parmrk;
                        }
                }
                else if (ISSET(err, TTY_PE) && ISSET(iflag, INPCK) || ISSET(err, TTY_FE))
                {
                        if (ISSET(iflag, IGNPAR))
                        {
                                goto endcase;
                        }
                        else if (ISSET(iflag, PARMRK))
                        {
                        parmrk:
                                clist_putc(0377 | TTY_QUOTE, &tp->t_rawq);
                                clist_putc(0 | TTY_QUOTE, &tp->t_rawq);
                                clist_putc(c | TTY_QUOTE, &tp->t_rawq);
                                goto endcase;
                        }
                        else
                                c = 0;
                }
        }

        /*
         * In tandem mode, check high water mark.
         */
        if (ISSET(iflag, IXOFF))
        {
                ttyblock(tp);
        }

        if (!ISSET(tp->t_state, TS_TYPEN) && ISSET(iflag, ISTRIP))
        {
                CLR(c, 0x80);
        }

        if (!ISSET(lflag, EXTPROC))
        {
                /*
                 * Check for literal nexting very first
                 */
                if (ISSET(tp->t_state, TS_LNCH))
                {
                        SET(c, TTY_QUOTE);
                        CLR(tp->t_state, TS_LNCH);
                }

                /*
                 * Scan for special characters.  This code
                 * is really just a big case statement with
                 * non-constant cases.  The bottom of the
                 * case statement is labeled ``endcase'', so goto
                 * it after a case match, or similar.
                 */

                /*
                 * Control chars which aren't controlled
                 * by ICANON, ISIG, or IXON.
                 */
                if (ISSET(lflag, IEXTEN))
                {
                        if (CCEQ(cc[VLNEXT], c))
                        {
                                if (ISSET(lflag, ECHO))
                                {
                                        if (ISSET(lflag, ECHOE))
                                        {
                                                ttyoutput('^', tp);
                                                ttyoutput('\b', tp);
                                        }
                                        else
                                        {
                                                ttyecho(c, tp);
                                        }
                                }

                                SET(tp->t_state, TS_LNCH);

                                goto endcase;
                        }

                        if (CCEQ(cc[VDISCARD], c))
                        {

                                if (ISSET(lflag, FLUSHO))
                                {
                                        CLR(tp->t_lflag, FLUSHO);
                                }
                                else
                                {
                                        ttyflush(tp, _FWRITE);

                                        ttyecho(c, tp);

                                        if (tp->t_rawq.c_cc + tp->t_canq.c_cc)
                                        {
                                                ttyretype(tp);
                                        }

                                        SET(tp->t_lflag, FLUSHO);
                                }

                                goto startoutput;
                        }
                }

                /*
                 * Signals.
                 */
                if (ISSET(lflag, ISIG))
                {
                        if (CCEQ(cc[VINTR], c) || CCEQ(cc[VQUIT], c))
                        {

                                // process_group_signal(tp->t_pgrp, CCEQ(cc[VINTR], c) ? SIGINT : SIGQUIT);

                                // killpg(tp->t_foreground_group, CCEQ(cc[VINTR], c) ? SIGINT : SIGQUIT);

                                if (!ISSET(lflag, NOFLSH))
                                {
                                        ttyflush(tp, _FREAD | _FWRITE);
                                }

                                ttyecho(c, tp);

                                utask_group *utask_grp = utask_group_find(tp->t_foreground_group);
                                KASSERT(utask_grp != nullptr);
                                utask_group_signal(utask_grp, CCEQ(cc[VINTR], c) ? SIGINT : SIGQUIT);

                                // tp->t_pgrp->signal(CCEQ(cc[VINTR], c) ? SIGINT : SIGQUIT, 1);

                                goto endcase;
                        }
                        if (CCEQ(cc[VSUSP], c))
                        {
                                // process_group_signal(tp->t_pgrp, SIGTSTP);
                                // killpg(tp->t_foreground_group, SIGTSTP);
                                // tp->t_pgrp->signal(SIGTSTP, 1);

                                utask_group *utask_grp = utask_group_find(tp->t_foreground_group);
                                KASSERT(utask_grp != nullptr);
                                utask_group_signal(utask_grp, SIGTSTP);

                                if (!ISSET(lflag, NOFLSH))
                                {
                                        ttyflush(tp, _FREAD);
                                }

                                ttyecho(c, tp);

                                goto endcase;
                        }
                }

                /*
                 * Handle start/stop characters.
                 */
                if (ISSET(iflag, IXON))
                {
                        if (CCEQ(cc[VSTOP], c))
                        {
                                if (!ISSET(tp->t_state, TS_TTSTOP))
                                {
                                        SET(tp->t_state, TS_TTSTOP);
                                        tp->t_stop(nullptr, tp, 0);
                                        return (0);
                                }

                                if (!CCEQ(cc[VSTART], c))
                                {
                                        return (0);
                                }

                                /*
                                 * if VSTART == VSTOP then toggle
                                 */
                                goto endcase;
                        }
                        if (CCEQ(cc[VSTART], c))
                        {
                                goto restartoutput;
                        }
                }

                /*
                 * IGNCR, ICRNL, & INLCR
                 */
                if (c == '\r')
                {
                        if (ISSET(iflag, IGNCR))
                        {
                                goto endcase;
                        }
                        else if (ISSET(iflag, ICRNL))
                        {
                                c = '\n';
                        }
                }
                else if (c == '\n' && ISSET(iflag, INLCR))
                {
                        c = '\r';
                }
        }

        if (!ISSET(tp->t_lflag, EXTPROC) && ISSET(lflag, ICANON))
        {
                /*
                 * From here on down canonical mode character
                 * processing takes place.
                 */
                /*
                 * erase (^H / ^?)
                 */
                if (CCEQ(cc[VERASE], c))
                {
                        if (tp->t_rawq.c_cc)
                        {
                                ttyrub(unputc(&tp->t_rawq), tp);
                        }
                        goto endcase;
                }
                /*
                 * kill (^U)
                 */
                if (CCEQ(cc[VKILL], c))
                {
                        if (ISSET(lflag, ECHOKE) && tp->t_rawq.c_cc == tp->t_rocount && !ISSET(lflag, ECHOPRT))
                                while (tp->t_rawq.c_cc)
                                {
                                        ttyrub(unputc(&tp->t_rawq), tp);
                                }
                        else
                        {
                                ttyecho(c, tp);
                                if (ISSET(lflag, ECHOK) || ISSET(lflag, ECHOKE))
                                {
                                        ttyecho('\n', tp);
                                }
                                FLUSHQ(&tp->t_rawq);
                                tp->t_rocount = 0;
                        }
                        CLR(tp->t_state, TS_LOCAL);
                        goto endcase;
                }
                /*
                 * word erase (^W)
                 */
                if (CCEQ(cc[VWERASE], c))
                {
                        int alt = ISSET(lflag, ALTWERASE);
                        int ctype;

                        /*
                         * erase whitespace
                         */
                        while ((c = unputc(&tp->t_rawq)) == ' ' || c == '\t')
                        {
                                ttyrub(c, tp);
                        }

                        if (c == -1)
                        {
                                goto endcase;
                        }

                        /*
                         * erase last char of word and remember the
                         * next chars type (for ALTWERASE)
                         */

                        ttyrub(c, tp);

                        c = unputc(&tp->t_rawq);

                        if (c == -1)
                        {
                                goto endcase;
                        }

                        if (c == ' ' || c == '\t')
                        {
                                clist_putc(c, &tp->t_rawq);
                                goto endcase;
                        }

                        ctype = ISALPHA(c);
                        /*
                         * erase rest of word
                         */
                        do
                        {
                                ttyrub(c, tp);
                                c = unputc(&tp->t_rawq);
                                if (c == -1)
                                {
                                        goto endcase;
                                }

                        } while (c != ' ' && c != '\t' && (alt == 0 || ISALPHA(c) == ctype));

                        clist_putc(c, &tp->t_rawq);

                        goto endcase;
                }

                /*
                 * reprint line (^R)
                 */
                if (CCEQ(cc[VREPRINT], c))
                {
                        ttyretype(tp);
                        goto endcase;
                }

                /*
                 * ^T - kernel info and generate SIGINFO
                 */
                if (CCEQ(cc[VSTATUS], c))
                {
                        if (ISSET(lflag, ISIG))
                        {
                                /* Not linux or posix */

                                // tp->t_pgrp->signal(SA_SIGINFO, 1);
                        }

                        if (!ISSET(lflag, NOKERNINFO))
                        {
                                // ttyinfo(tp);
                        }

                        goto endcase;
                }
        }

        /*
         * Check for input buffer overflow
         */
        if (tp->t_rawq.c_cc + tp->t_canq.c_cc >= TTYHOG)
        {
                if (ISSET(iflag, IMAXBEL))
                {
                        if (tp->t_outq.c_cc < tp->t_hiwat)
                        {
                                ttyoutput(CTRL('g'), tp);
                        }
                }
                else
                {
                        ttyflush(tp, _FREAD | _FWRITE);
                }

                goto endcase;
        }

        /*
         * Put data char in q for user and
         * wakeup on seeing a line delimiter.
         */
        if (clist_putc(c, &tp->t_rawq) >= 0)
        {

                if (!ISSET(lflag, ICANON))
                {
                        ttwakeup(tp);
                        ttyecho(c, tp);
                        goto endcase;
                }

                if (TTBREAKC(c))
                {
                        tp->t_rocount = 0;
                        catq(&tp->t_rawq, &tp->t_canq);
                        ttwakeup(tp);
                }
                else if (tp->t_rocount++ == 0)
                {
                        tp->t_rocol = tp->t_column;
                }

                if (ISSET(tp->t_state, TS_ERASE))
                {
                        /*
                         * end of prterase \.../
                         */
                        CLR(tp->t_state, TS_ERASE);
                        ttyoutput('/', tp);
                }

                i = tp->t_column;

                ttyecho(c, tp);

                if (CCEQ(cc[VEOF], c) && ISSET(lflag, ECHO))
                {
                        /*
                         * Place the cursor over the '^' of the ^D.
                         */
                        i = etl::min(2l, tp->t_column - i);
                        while (i > 0)
                        {
                                ttyoutput('\b', tp);
                                i--;
                        }
                }
        }

endcase:
        /*
         * IXANY means allow any character to restart output.
         */
        if (ISSET(tp->t_state, TS_TTSTOP) && !ISSET(iflag, IXANY) && cc[VSTART] != cc[VSTOP])
        {
                return (0);
        }
restartoutput:
        CLR(tp->t_lflag, FLUSHO);
        CLR(tp->t_state, TS_TTSTOP);
startoutput:
        return (ttstart(tp));
}

/*
 * Output a single character on a tty, doing output processing
 * as needed (expanding tabs, newline processing, etc.).
 * Returns < 0 if succeeds, otherwise returns char to resend.
 * Must be recursive.
 */
int ttyoutput(int c, struct tty *tp)
{
        long oflag;
        int col, s;

        oflag = tp->t_oflag;
        if (!ISSET(oflag, OPOST))
        {
                if (ISSET(tp->t_lflag, FLUSHO))
                {
                        return (-1);
                }

                if (clist_putc(c, &tp->t_outq))
                {
                        return (c);
                }

                tk_nout++;
                tp->t_outcc++;
                return (-1);
        }
        /*
         * Do tab expansion if OXTABS is set.  Special case if we external
         * processing, we don't do the tab expansion because we'll probably
         * get it wrong.  If tab expansion needs to be done, let it happen
         * externally.
         */
        CLR(c, ~TTY_CHARMASK);
        if (c == '\t' && ISSET(oflag, OXTABS) && !ISSET(tp->t_lflag, EXTPROC))
        {
                c = 8 - (tp->t_column & 7);
                if (!ISSET(tp->t_lflag, FLUSHO))
                {
                        s = spltty(); /* Don't interrupt tabs. */
                        c -= b_to_q("        ", c, &tp->t_outq);
                        tk_nout += c;
                        tp->t_outcc += c;
                        splx(s);
                }
                tp->t_column += c;
                return (c ? -1 : '\t');
        }
#if DAMIR
        if (c == CEOT && ISSET(oflag, ONOEOT))
                return (-1);
#endif

        /*
         * Newline translation: if ONLCR is set,
         * translate newline into "\r\n".
         */
        if (c == '\n' && ISSET(tp->t_oflag, ONLCR))
        {
                tk_nout++;
                tp->t_outcc++;
                if (clist_putc('\r', &tp->t_outq))
                {
                        return (c);
                }
        }

        tk_nout++;

        tp->t_outcc++;

        if (!ISSET(tp->t_lflag, FLUSHO) && clist_putc(c, &tp->t_outq))
        {
                return (c);
        }

        col = tp->t_column;
        switch (CCLASS(c))
        {
        case BACKSPACE:
                if (col > 0)
                {
                        --col;
                }
                break;
        case CONTROL:
                break;
        case NEWLINE:
        case RETURN:
                col = 0;
                break;
        case ORDINARY:
                ++col;
                break;
        case TAB:
                col = (col + 8) & ~7;
                break;
        }
        tp->t_column = col;
        return (-1);
}

/*
 * Ioctls for all tty devices.  Called after line-discipline specific ioctl
 * has been called to do discipline-specific functions and/or reject any
 * of these ioctl commands.
 */
/* ARGSUSED */
int ttioctl(struct tty *tp, ioctl_req_t cmd, int flag)
{
        extern struct tty *constty; /* Temporary virtual console. */
        extern int nlinesw;
        process_t p;
        int s, error;

        // p = process::current(); /* XXX */

        /* If the ioctl involves modification, hang if in the background. */

        integer_t arg = ioctl_req_arg(cmd);

        kern_return_t retval = 0;

        switch (ioctl_req_cmdno(cmd))
        {
        case CFLUSH:
#if DAMIR
        case TIOCSETA:
        case TIOCSETD:
        case TIOCSETAF:
        case TIOCSETAW:
#endif
#ifdef notdef
        case TIOCSPGRP:
#endif
        case TIOCSTI:
                //    case TIOCSWINSZ:

#if DAMIR
                while (isbackground(p, tp) && p->p_pgrp->pg_jobc && (p->p_flag & P_PPWAIT) == 0 &&

                       (p->p_sigignore & sigmask(SIGTTOU)) == 0 && (p->p_sigmask & sigmask(SIGTTOU)) == 0)
                {
                        pgsignal(p->p_pgrp, SIGTTOU, 1);
                        if (error = ttysleep(tp, &lbolt, TTOPRI | PCATCH, ttybg, 0))
                                return (error);
                }

                break;
#endif
                break;
        }

        switch (ioctl_req_cmdno(cmd))
        {              /* Process the ioctl. */
        case FIOASYNC: /* set/clear async i/o */
                s = spltty();
                if (arg)
                {
                        SET(tp->t_state, TS_ASYNC);
                }
                else
                {
                        CLR(tp->t_state, TS_ASYNC);
                }
                splx(s);
                break;
        case FIONBIO:  /* set/clear non-blocking i/o */
                break; /* XXX: delete. */
        case FIONREAD: /* get # bytes to read */
                //*(int *)data = ttnread(tp);
                {
                        int br = ttnread(tp);
                        retval = ioctl_req_write(cmd, &br, sizeof(br));
                }
                break;
        case TIOCEXCL: /* set exclusive use of tty */
                s = spltty();
                SET(tp->t_state, TS_XCLUDE);
                splx(s);
                break;
        case CFLUSH: { /* flush buffers */
                //int flags = *(int *)data;
                int flags = (int)arg;
                if (flags == 0)
                {
                        flags = (_FREAD | _FWRITE);
                }
                else
                {
                        flags &= (_FREAD | _FWRITE);
                }

                ttyflush(tp, flags);
                break;
        }

        case TCFLSH: 
        {
                int flg = (int)arg;
                if (flg == TCIFLUSH) 
                {
                      //flushes data received but not read.
                } 
                else if (flg = TCOFLUSH)
                {
                     //flushes data written but not transmitted.
                }
                else if (flg == TCIOFLUSH) 
                {
                     //flushes both data received but not read, and data written but not transmitted.
                }
                else
                {
                        return KERN_FAIL(EINVAL);
                }
                break;
        }
        case TIOCCONS: /* become virtual console */
                if (arg)
                {
                        if (constty && constty != tp && ISSET(constty->t_state, TS_CARR_ON | TS_ISOPEN) == (TS_CARR_ON | TS_ISOPEN))
                        {
                                return KERN_FAIL(EBUSY);
                        }

#ifndef UCONSOLE
#if 0
                        if (error = suser(p->p_cred->pc_ucred, &p->p_acflag))
                        {
                                return (error);
                        }
#endif
#endif

                        constty = tp;
                }
                else if (tp == constty)
                {
                        constty = NULL;
                }

                break;
#if DAMIR
        case TIOCDRAIN: /* wait till output drained */
                if (error = ttywait(tp))
                        return (error);
                break;
#endif
        // case TIOCGETA: { /* get termios struct */
        case TCGETS: { /* get termios struct */

                //*(struct termios *)data = tp->t_termios;
                retval = ioctl_req_write(cmd, &tp->t_termios, sizeof(tp->t_termios));
                break;
        }

        case TCGETA: { /* get termio struct */
                panic("Not ready");
                //struct termios *t = (struct termios *)data;

                //bcopy(&tp->t_termios, t, sizeof(struct termios));
                break;
        }

        case TIOCGETD: /* get line discipline */
                //*(int *)data = tp->t_line;
                retval = ioctl_req_write(cmd, &tp->t_line, sizeof(int));
                break;
        case TIOCGWINSZ: /* get window size */
                //*(struct winsize *)data = tp->t_winsize;
                retval = ioctl_req_write(cmd, &tp->t_winsize, sizeof(tp->t_winsize));
                break;
        case TIOCGPGRP:

                /*
                TIOCGPGRP
                Argument: pid_t *argp

                When successful, equivalent to *argp = tcgetpgrp(fd).

                Get the process group ID of the foreground process group
                on this terminal.
                */

                if (!isctty(p, tp))
                {
                        return KERN_FAIL(ENOTTY);
                }

                //*(int *)data = tp->t_pgrp ? tp->t_pgrp->pg_id : NO_PID;

                //*(pid_t *)data = process_group_get_id(tp->t_pgrp);
                //*(pid_t *)data = tp->t_foreground_group;
                retval = ioctl_req_write(cmd, &tp->t_foreground_group, sizeof(pid_t));

                break;
#ifdef TIOCHPCL
        case TIOCHPCL: /* hang up on last close */
                s = spltty();
                SET(tp->t_cflag, HUPCL);
                splx(s);
                break;
#endif
        case TIOCNXCL: /* reset exclusive use of tty */
                s = spltty();
                CLR(tp->t_state, TS_XCLUDE);
                splx(s);
                break;
        case TIOCOUTQ: /* output queue size */
                //*(int *)data = tp->t_outq.c_cc;
                retval = ioctl_req_write(cmd, &tp->t_outq.c_cc, sizeof(int));                
                break;
        case TCSETS /*TIOCSETA*/:     /* set termios struct */
        case TCSETSW /*TIOCSETAW*/:   /* drain output, set */
        case TCSETSF /*TIOCSETAF*/: { /* drn out, fls in, set */
                //struct termios *t = (struct termios *)data;
                
                struct termios t;
                retval = ioctl_req_read(cmd, &t, sizeof(t));                

                if (KERN_STATUS_FAILURE(retval))
                {
                        break;
                }

                s = spltty();
                if (ioctl_req_cmdno(cmd) == /*TIOCSETAW*/ TCSETSW || ioctl_req_cmdno(cmd) == /*TIOCSETAF*/ TCSETSF)
                {
                        if (error = ttywait(tp))
                        {
                                splx(s);
                                return (error);
                        }

                        if (ioctl_req_cmdno(cmd) == /*TIOCSETAF*/ TCSETSF)
                        {
                                ttyflush(tp, _FREAD);
                        }
                }
#define CIGNORE 1
                if (!ISSET(t.c_cflag, CIGNORE))
                {
                        /*
                         * Set device hardware.
                         */
                        if (tp->t_param && (error = (*tp->t_param)(tp, &t)))
                        {
                                splx(s);
                                return (error);
                        }
                        else
                        {
                                if (!ISSET(tp->t_state, TS_CARR_ON) && ISSET(tp->t_cflag, CLOCAL) && !ISSET(t.c_cflag, CLOCAL))
                                {
                                        CLR(tp->t_state, TS_ISOPEN);
                                        SET(tp->t_state, TS_WOPEN);
                                        ttwakeup(tp);
                                }

                                tp->t_cflag  = t.c_cflag;
                                tp->t_ispeed = t.__c_ispeed;
                                tp->t_ospeed = t.__c_ospeed;
                        }
                        ttsetwater(tp);
                }

                if (ioctl_req_cmdno(cmd) != /*TIOCSETAF */ TCSETSF)
                {
                        if (ISSET(t.c_lflag, ICANON) != ISSET(tp->t_lflag, ICANON))
                                if (ISSET(t.c_lflag, ICANON))
                                {
                                        SET(tp->t_lflag, PENDIN);
                                        ttwakeup(tp);
                                }
                                else
                                {
                                        struct clist tq;

                                        catq(&tp->t_rawq, &tp->t_canq);
                                        tq         = tp->t_rawq;
                                        tp->t_rawq = tp->t_canq;
                                        tp->t_canq = tq;
                                        CLR(tp->t_lflag, PENDIN);
                                }
                }
                tp->t_iflag = t.c_iflag;
                tp->t_oflag = t.c_oflag;
                /*
                 * Make the EXTPROC bit read only.
                 */
                if (ISSET(tp->t_lflag, EXTPROC))
                {
                        SET(t.c_lflag, EXTPROC);
                }
                else
                {
                        CLR(t.c_lflag, EXTPROC);
                }

                tp->t_lflag = t.c_lflag | ISSET(tp->t_lflag, PENDIN);

                bcopy(t.c_cc, tp->t_cc, sizeof(t.c_cc));

                splx(s);
                break;
        }

        case TIOCSETD: { /* set line discipline */
                int t        = (int)arg;
                dev_t device = tp->t_dev;

                if ((u32)t >= nlinesw)
                        return -ENXIO;
                if (t != tp->t_line)
                {
                        s = spltty();
                        (*linesw[tp->t_line].l_close)(tp, flag);
                        error = (*linesw[t].l_open)(device, tp);
                        if (error)
                        {
                                (*linesw[tp->t_line].l_open)(device, tp);
                                splx(s);
                                return (error);
                        }
                        tp->t_line = t;
                        splx(s);
                }
                break;
        }

        case TCXONC: {
                // TCXONC
                // Start/stop control. The argument	is an  int  value.  If
                // the  argument  is 0, suspend output; if 1, restart sus-
                // pended output; if 2, suspend input; if 3, restart  sus-
                // pended input.

                s = spltty();

                switch (arg)
                {
                case 0: // Suspend output
                        if (!ISSET(tp->t_state, TS_TTSTOP))
                        {
                                SET(tp->t_state, TS_TTSTOP);
                                tp->t_stop(nullptr, tp, 0);
                        }
                        break;
                case 1: // Restart suspended output
                        if (ISSET(tp->t_state, TS_TTSTOP) || ISSET(tp->t_lflag, FLUSHO))
                        {
                                CLR(tp->t_lflag, FLUSHO);
                                CLR(tp->t_state, TS_TTSTOP);
                                ttstart(tp);
                        }
                        break;
                case 2: // Suspend input
                        /* code */
                        break;
                case 3: // Restart suspended input
                        /* code */
                        break;

                default:
                        splx(s);
                        return KERN_FAIL(EINVAL);
                        break;
                }

                splx(s);
                break;
        }

        case CSTART: /* start output, like ^Q */
                s = spltty();
                if (ISSET(tp->t_state, TS_TTSTOP) || ISSET(tp->t_lflag, FLUSHO))
                {
                        CLR(tp->t_lflag, FLUSHO);
                        CLR(tp->t_state, TS_TTSTOP);
                        ttstart(tp);
                }
                splx(s);
                break;
        case TIOCSTI: /* simulate terminal input */
                if (/*p->p_cred->pc_ucred->cr_uid &&  (flag & FREAD) == 0 */ 0)
                {
                        return KERN_FAIL(EPERM);
                }

                if (/*p->p_cred->pc_ucred->cr_uid && !isctty(p, tp) */ 0)
                {
                        return KERN_FAIL(EACCES);
                }

                {
                        uint8_t data;
                        retval = ioctl_req_read(cmd, &data, sizeof(data));
                        if (KERN_STATUS_SUCCESS(retval)) {
                                (*linesw[tp->t_line].l_rint)(data, tp);
                        }
                }
                break;
        case CSTOP: /* stop output, like ^S */
                s = spltty();
                if (!ISSET(tp->t_state, TS_TTSTOP))
                {
                        SET(tp->t_state, TS_TTSTOP);
                        tp->t_stop(nullptr, tp, 0);
                }
                splx(s);
                break;
        case TIOCSCTTY: /* become controlling tty */ {
                /* Session ctty vnode pointer set in vnode layer. */

                // int arg
                // Make the given terminal the controlling terminal of the calling process. The calling process must be a session
                // leader and not have a controlling terminal already. If this terminal is already the controlling terminal of a
                // different session group then the ioctl fails with EPERM, unless the caller is root (more precisely: has the
                // CAP_SYS_ADMIN capability) and arg equals 1, in which case the terminal is stolen, and all processes that had it
                // as controlling terminal lose it.

                if (tp->t_session_id != 0 /*if root, revoke it */)
                {
                        return KERN_FAIL(EPERM);
                }

                process_t utsk = utask_self();

                // Must be session leader
                if ((utsk->m_pid != utsk->m_sid))
                {
                        return KERN_FAIL(EPERM);
                }

                tp->t_session_id       = utsk->m_sid;
                tp->t_session_leader   = utsk->m_pid;
                tp->t_foreground_group = utsk->m_pgid;
                utsk->m_ctty           = tp->t_dev;

#if 0
                if (!p->is_session_leader() || (p->p_pgrp->pg_session->s_ttyvp || tp->t_session) && (tp->t_session != p->p_pgrp->pg_session))
                {
                        return KERN_FAIL(EPERM);
                }

                tp->t_session                 = p->p_pgrp->pg_session;
                tp->t_pgrp                    = p->p_pgrp;
                p->p_pgrp->pg_session->s_ttyp = tp;
                p->p_flag |= P_CONTROLT;
#endif
                break;
        }
        case TIOCSPGRP: { /* set pgrp of tty */
                          // struct pgrp *pgrp = proc_group_find(*(int *)data);
                          // Implement

                // tp  must be the controlling terminal of the calling process
                //                                if (!isctty(p, tp))
                //                                {
                //                                        return KERN_FAIL(ENOTTY);
                //                                }

                pid_t pgid;// = *(pid_t *)data;
                retval = ioctl_req_read(cmd, &pgid, sizeof(pgid));

                if (KERN_STATUS_FAILURE(retval))
                {
                        break;
                }

                if (tp->t_foreground_group == pgid)
                {
                        break;
                }

                utask_group *pg = utask_group_find(pgid);

                if (pg == nullptr)
                {
                        return KERN_FAIL(EINVAL);
                }

                process_t utask = utask_self();
                // pg must be a (nonempty) process group belonging to the same session as the calling process.
                if (pg->m_sid != utask->m_sid)
                {
                        return KERN_FAIL(EPERM);
                }

                sigset_t sigmask;
                uthread_sigprocmask(uthread_self(), 0, nullptr, &sigmask);

                ksigaction sigact;
                uthread_sigaction(uthread_self(), SIGTTOU, nullptr, &sigact);

                tp->t_foreground_group = pgid;

                if ((sigismember(&sigmask, SIGTTOU)) != 0 && (sigact.handler != SIG_IGN))
                {
                        utask_group_signal(pg, SIGTTOU);
                }

#if 0
                process_group_t pgrp;

                kern_return_t retval = process_group_find(*(pid_t *)data, &pgrp);

                if (retval != KERN_SUCCESS)
                {
                        return retval;
                }

                if (!isctty(p, tp))
                {
                        return KERN_FAIL(ENOTTY);
                }
                else if (pgrp == NULL || pgrp->pg_session != p->p_pgrp->pg_session)
                {
                        return KERN_FAIL(EPERM);
                }

                tp->t_pgrp = pgrp;
#endif
                break;
        }

        case TIOCSWINSZ: { /* set window size */
                //struct winsize *new_winsize = (struct winsize *)data;
                struct winsize new_winsize;
                retval = ioctl_req_read(cmd, &new_winsize, sizeof(new_winsize));

                if (KERN_STATUS_FAILURE(retval))
                {
                        break;
                }


                if (tp->t_winsize.ws_row != new_winsize.ws_row || tp->t_winsize.ws_col != new_winsize.ws_col
                    || tp->t_winsize.ws_xpixel != new_winsize.ws_xpixel || tp->t_winsize.ws_ypixel != new_winsize.ws_ypixel)
                {
                        tp->t_winsize = new_winsize;
                        // tp->t_pgrp->signal(SIGWINCH, 1);
                        // process_group_signal(tp->t_pgrp, SIGWINCH);
                        // killpg(tp->t_foreground_group, SIGWINCH);
                        utask_group *utask_grp = utask_group_find(tp->t_foreground_group);
                        KASSERT(utask_grp != nullptr);
                        utask_group_signal(utask_grp, SIGWINCH);
                }
                break;
        }
        default:
                return (-1);
        }
        return retval;
}

#if 0
int ttselect(dev_t device, int rw, pthread_t p)
{

        struct tty *tp;
        int nread, s;

        tp = (*cdevsw[major(device)].d_tty)(device);

        s = spltty();
        switch (rw)
        {
        case FREAD:
                nread = ttnread(tp);
                if (nread > 0 || !ISSET(tp->t_cflag, CLOCAL) && !ISSET(tp->t_state, TS_CARR_ON))
                        goto win;
                select_record(p, &tp->t_rsel);
                break;
        case FWRITE:
                if (tp->t_outq.c_cc <= tp->t_lowat)
                {
                win:
                        splx(s);
                        return (1);
                }
                select_record(p, &tp->t_wsel);
                break;
        }
        splx(s);

        panic("Should not run");
        return (0);
}
#endif

int ttpoll(struct tty *tp, pollfd **a_pfd)
{
        // spl_t s = spltty();
        // defer(splx(s));

        if ((*a_pfd)->events & (POLLIN | POLLRDNORM))
        {
                if (ttnread(tp) > 0 || (!ISSET(tp->t_cflag, CLOCAL) && !ISSET(tp->t_state, TS_CARR_ON)))
                {
                        (*a_pfd)->revents |= ((*a_pfd)->events & (POLLIN | POLLRDNORM));
                }
        }

        /* NOTE: POLLHUP and POLLOUT/POLLWRNORM are mutually exclusive */
        if (!ISSET(tp->t_cflag, CLOCAL) && !ISSET(tp->t_state, TS_CARR_ON))
        {
                (*a_pfd)->revents |= POLLHUP;
        }
        else if ((*a_pfd)->events & (POLLOUT | POLLWRNORM))
        {
                if (tp->t_outq.c_cc <= tp->t_lowat)
                {
                        (*a_pfd)->revents |= ((*a_pfd)->events & (POLLOUT | POLLWRNORM));
                }
        }

        return poll_record(&tp->t_pollfds, a_pfd);
}

int ttnread(struct tty *tp)
{
        size_t nread;

        if (ISSET(tp->t_lflag, PENDIN))
        {
                ttypend(tp);
        }

        nread = tp->t_canq.c_cc;

        if (!ISSET(tp->t_lflag, ICANON))
        {
                nread += tp->t_rawq.c_cc;
        }

        return (int)(nread);
}

/*
 * Wait for output to drain.
 */
int ttywait(struct tty *tp)
{
        int error, s;

        error = 0;
        s     = spltty();
        while ((tp->t_outq.c_cc || ISSET(tp->t_state, TS_BUSY)) && (ISSET(tp->t_state, TS_CARR_ON) || ISSET(tp->t_cflag, CLOCAL))
               && tp->t_oproc)
        {
                (*tp->t_oproc)(tp);
                SET(tp->t_state, TS_ASLEEP);
                if (error = ttysleep(tp, &tp->t_outq, TTOPRI | PCATCH, ttyout, 0))
                {
                        break;
                }
        }
        splx(s);
        return (error);
}

/*
 * Flush if successfully wait.
 */
int ttywflush(struct tty *tp)
{
        int error;

        if ((error = ttywait(tp)) == 0)
        {
                ttyflush(tp, _FREAD);
        }
        return (error);
}

/*
 * Flush tty read and/or write queues, notifying anyone waiting.
 */
void ttyflush(struct tty *tp, int rw)
{
        int s;

        s = spltty();
        if (rw & (_FREAD))
        {
                FLUSHQ(&tp->t_canq);
                FLUSHQ(&tp->t_rawq);
                tp->t_rocount = 0;
                tp->t_rocol   = 0;
                CLR(tp->t_state, TS_LOCAL);
                ttwakeup(tp);
        }
        if (rw & (_FWRITE))
        {
                CLR(tp->t_state, TS_TTSTOP);
                (*tp->t_stop)(nullptr, tp, rw);
                FLUSHQ(&tp->t_outq);
                // wakeup((char *)&tp->t_outq);
                uthread_wakeup(&tp->t_outq);
                poll_wakeup(&tp->t_pollfds, POLLOUT | POLLWRNORM);

                // select_wakeup(&tp->t_wsel);
        }
        splx(s);
}

/*
 * Copy in the default termios characters.
 */

static const cc_t ttydefchars[NCCS]{
    /*VINTR     0*/ CINTR,
    /*VQUIT     1*/ CQUIT,
    /*VERASE    2*/ CERASE,
    /*VKILL     3*/ CKILL,
    /*VEOF      4*/ CEOF,
    /*VTIME     5*/ CTIME,
    /*VMIN      6*/ CMIN,
    /*VSWTC     7*/ _POSIX_VDISABLE,
    /*VSTART    8*/ CSTART,
    /*VSTOP     9*/ CSTOP,
    /*VSUSP    10*/ CSUSP,
    /*VEOL     11*/ CEOL,
    /*VREPRINT 12*/ CREPRINT,
    /*VDISCARD 13*/ CDISCARD,
    /*VWERASE  14*/ VWERASE,
    /*VLNEXT   15*/ CLNEXT,
    /*VEOL2    16*/ CEOL,
    /* spare     */ _POSIX_VDISABLE,
    /* spare     */ _POSIX_VDISABLE,
    /* spare     */ _POSIX_VDISABLE,
    /* spare     */ _POSIX_VDISABLE,
    /* spare     */ _POSIX_VDISABLE,
    /* spare     */ _POSIX_VDISABLE,
    /* spare     */ _POSIX_VDISABLE,
    /* spare     */ _POSIX_VDISABLE,
    /* spare     */ _POSIX_VDISABLE,
    /* spare     */ _POSIX_VDISABLE,
    /* spare     */ _POSIX_VDISABLE,
    /* spare     */ _POSIX_VDISABLE,
    /* spare     */ _POSIX_VDISABLE,
    /* spare     */ _POSIX_VDISABLE,
    /* spare     */ _POSIX_VDISABLE
    /* NCCS */
};

#if 0
static const cc_t ttydefchars[NCCS] = {
    [VEOF] = CEOF,
    [VEOL] = CEOL,
    [VEOL2] = CEOL,
    [VERASE] = CERASE,
    [VWERASE] = CWERASE,
    [VKILL] = CKILL,
    [VREPRINT] = CREPRINT,
    [7] = _POSIX_VDISABLE, /* spare */
    [VINTR] = CINTR,
    [VQUIT] = CQUIT,
    [VSUSP] = CSUSP,
    [9]      = _POSIX_VDISABLE,
    // CDSUSP
    [VSTART] = CSTART,
    [VSTOP] = CSTOP,
    [VLNEXT] = CLNEXT,
    [VDISCARD] = CDISCARD,
    [VMIN] = CMIN,
    [VTIME] = CTIME,
    [VSTATUS] = CSTATUS,
    [19] = _POSIX_VDISABLE, /* spare */
};
#endif
void ttychars(struct tty *tp) { memcpy(&(tp->t_termios.c_cc[0]), &(ttydefchars[0]), sizeof(ttydefchars)); }

/*
 * Send stop character on input overflow.
 */
void ttyblock(struct tty *tp)
{
        int total;

        total = tp->t_rawq.c_cc + tp->t_canq.c_cc;
        if (tp->t_rawq.c_cc > TTYHOG)
        {
                ttyflush(tp, _FREAD | _FWRITE);
                CLR(tp->t_state, TS_TBLOCK);
        }
        /*
         * Block further input iff: current input > threshold
         * AND input is available to user program.
         */
        if (total >= TTYHOG / 2 && !ISSET(tp->t_state, TS_TBLOCK) && !ISSET(tp->t_lflag, ICANON)
            || tp->t_canq.c_cc > 0 && tp->t_cc[VSTOP] != _POSIX_VDISABLE)
        {
                if (clist_putc(tp->t_cc[VSTOP], &tp->t_outq) == 0)
                {
                        SET(tp->t_state, TS_TBLOCK);
                        ttstart(tp);
                }
        }
}

void ttrstrt(void *tp_arg)
{
        struct tty *tp;
        int s;

        /* #if DIAGNOSTIC */
        if (tp_arg == NULL)
        {
                panic("ttrstrt");
        }
        /* #endif */
        tp = (struct tty *)tp_arg;
        s  = spltty();

        CLR(tp->t_state, TS_TIMEOUT);
        ttstart(tp);

        splx(s);
}

int ttstart(struct tty *tp)
{
        if (tp->t_oproc != NULL) /* XXX: Kludge for pty. */
        {
                (*tp->t_oproc)(tp);
        }
        return (0);
}

/*
 * "close" a line discipline
 */
int ttylclose(struct tty *tp, int flag)
{
        if (flag & IO_NDELAY)
        {
                ttyflush(tp, _FREAD | _FWRITE);
        }
        else
        {
                ttywflush(tp);
        }
        return (0);
}

/*
 * Handle modem control transition on a tty.
 * Flag indicates new state of carrier.
 * Returns 0 if the line should be turned off, otherwise 1.
 */

int ttymodem(struct tty *tp, int flag)
{
        if (!ISSET(tp->t_state, TS_WOPEN) /*&& ISSET(tp->t_cflag, CCAR_OFLOW)*/)
        {
                /*
                 * MDMBUF: do flow control according to carrier flag
                 */
                if (flag)
                {
                        CLR(tp->t_state, TS_TTSTOP);
                        ttstart(tp);
                }
                else if (!ISSET(tp->t_state, TS_TTSTOP))
                {
                        SET(tp->t_state, TS_TTSTOP);
                        (*tp->t_stop)(nullptr, tp, 0);
                }
        }
        else if (flag == 0)
        {
                /*
                 * Lost carrier.
                 */
                CLR(tp->t_state, TS_CARR_ON);
                if (ISSET(tp->t_state, TS_ISOPEN) && !ISSET(tp->t_cflag, CLOCAL))
                {
                        panic("Not yet");
#if 0
                        if (tp->t_session && tp->t_session->s_leader)
                                psignal(tp->t_session->s_leader, SIGHUP);
                        ttyflush(tp, FREAD | FWRITE);
#endif
                        return (0);
                }
        }
        else
        {
                /*
                 * Carrier now on.
                 */
                SET(tp->t_state, TS_CARR_ON);
                ttwakeup(tp);
        }
        return (1);
}

/*
 * Default modem control routine (for other line disciplines).
 * Return argument flag, to turn off device on carrier drop.
 */
int nullmodem(struct tty *tp, int flag)
{
        if (flag)
        {
                SET(tp->t_state, TS_CARR_ON);
        }
        else
        {
                CLR(tp->t_state, TS_CARR_ON);
                if (!ISSET(tp->t_cflag, CLOCAL))
                {
                        if (tp->t_session_id != 0 && tp->t_session_leader != 0)
                        {
                                panic("Shit");
                                // if (kill(tp->t_session_leader, SIGHUP) != KERN_SUCCESS)
                                //{
                                //        tp->t_session_leader = 0;
                                // }
                        }
#if 0
                        if (tp->t_session && tp->t_session->s_leader)
                        {
                                tp->t_session->s_leader->signal(SIGHUP);
                        }
#endif
                        return (0);
                }
        }
        return (1);
}

/*
 * Reinput pending characters after state switch
 * call at spltty().
 */
void ttypend(struct tty *tp)
{
        struct clist tq;
        int c;

        CLR(tp->t_lflag, PENDIN);
        SET(tp->t_state, TS_TYPEN);
        tq              = tp->t_rawq;
        tp->t_rawq.c_cc = 0;
        tp->t_rawq.c_cf = tp->t_rawq.c_cl = 0;

        while ((c = clist_getc(&tq)) >= 0)
        {
                ttyinput(c, tp);
        }

        CLR(tp->t_state, TS_TYPEN);
}

/*
 * Process a read call on a tty device.
 */
int ttread(struct tty *tp, struct uio *uio, int flag)
{
        struct clist *qp;
        int c;
        long lflag;
        u8 *cc = tp->t_cc;
        int s, first, error = 0;

loop:
        lflag = tp->t_lflag;
        s     = spltty();
        /*
         * take pending input first
         */
        if (ISSET(lflag, PENDIN))
        {
                ttypend(tp);
        }
        splx(s);

        /*
         * Hang process if it's in the background.
         */
        if (/*isbackground(p, tp)*/ 0)
        {
#if DAMIR
                if ((p->p_sigignore & sigmask(SIGTTIN)) || (p->p_sigmask & sigmask(SIGTTIN)) || p->p_flag & P_PPWAIT
                    || p->p_pgrp->pg_jobc == 0)
                        return (-EIO);

                pgsignal(p->p_pgrp, SIGTTIN, 1);
                if (error = ttysleep(tp, &lbolt, TTIPRI | PCATCH, ttybg, 0))
                        return (error);

                goto loop;
#endif
        }

        /*
         * If canonical, use the canonical queue,
         * else use the raw queue.
         *
         * (should get rid of clists...)
         */
        qp = ISSET(lflag, ICANON) ? &tp->t_canq : &tp->t_rawq;

        /*
         * If there is no input, sleep on rawq
         * awaiting hardware receipt and notification.
         * If we have data, we don't need to check for carrier.
         */
        s = spltty();
        if (qp->c_cc <= 0)
        {
                int carrier;

                carrier = ISSET(tp->t_state, TS_CARR_ON) || ISSET(tp->t_cflag, CLOCAL);

                if (!carrier && ISSET(tp->t_state, TS_ISOPEN))
                {
                        splx(s);
                        return (0); /* EOF */
                }

                if (flag & IO_NDELAY)
                {
                        splx(s);
                        return -EWOULDBLOCK;
                }

                error = ttysleep(tp, &tp->t_rawq, TTIPRI | PCATCH, carrier ? ttyin : ttopen, 0);
                splx(s);

                if (error)
                {
                        return (error);
                }

                goto loop;
        }
        splx(s);

        /*
         * Input present, check for input mapping and processing.
         */
        first = 1;
        while ((c = clist_getc(qp)) >= 0)
        {
                /*
                 * delayed suspend (^Y)
                 */
                if (CCEQ(cc[VSUSP], c) && ISSET(lflag, ISIG))
                {
                        // tp->t_pgrp->signal(SIGTSTP, 1);
                        // process_group_signal(tp->t_pgrp, SIGTSTP);
                        //                        killpg(tp->t_foreground_group, SIGTSTP);

                        utask_group *utask_grp = utask_group_find(tp->t_foreground_group);
                        KASSERT(utask_grp != nullptr);
                        utask_group_signal(utask_grp, SIGTSTP);

                        if (first)
                        {
#if DAMIR
                                if (error = ttysleep(tp, &lbolt, TTIPRI | PCATCH, ttybg, 0))
                                        break
#endif
                                            goto loop;
                        }
                        break;
                }
                /*
                 * Interpret EOF only in canonical mode.
                 */
                if (CCEQ(cc[VEOF], c) && ISSET(lflag, ICANON))
                {
                        break;
                }
                /*
                 * Give user character.
                 */
                // error = ureadc(c, uio);
                error = uiomove(&c, sizeof(char), uio);

                if (error)
                {
                        break;
                }

                if (uio->uio_resid == 0)
                {
                        break;
                }

                /*
                 * In canonical mode check for a "break character"
                 * marking the end of a "line of input".
                 */
                if (ISSET(lflag, ICANON) && TTBREAKC(c))
                {
                        break;
                }

                first = 0;
        }
        /*
         * Look to unblock output now that (presumably)
         * the input queue has gone down.
         */
        s = spltty();

        if (ISSET(tp->t_state, TS_TBLOCK) && tp->t_rawq.c_cc < TTYHOG / 5)
        {
                if (cc[VSTART] != _POSIX_VDISABLE && clist_putc(cc[VSTART], &tp->t_outq) == 0)
                {
                        CLR(tp->t_state, TS_TBLOCK);
                        ttstart(tp);
                }
        }

        splx(s);

        return (error);
}

/*
 * Check the output queue on tp for space for a kernel message (from uprintf
 * or tprintf).  Allow some space over the normal hiwater mark so we don't
 * lose messages due to normal flow control, but don't let the tty run amok.
 * Sleeps here are not interruptible, but we return prematurely if new signals
 * arrive.
 */
#if 0
int ttycheckoutq(struct tty *tp, int wait)
{
        int hiwat, s, oldsig;
        process_t curproc = process::current();

        hiwat = tp->t_hiwat;
        s     = spltty();

        oldsig = wait ? curproc->p_siglist : 0;

        if (tp->t_outq.c_cc > hiwat + 200)
                while (tp->t_outq.c_cc > hiwat)
                {
                        ttstart(tp);

                        if (wait == 0 || curproc->p_siglist != oldsig)
                        {
                                splx(s);
                                return (0);
                        }

                        timeout(wakeup, &tp->t_outq, hz);

                        SET(tp->t_state, TS_ASLEEP);
                        sleep(&tp->t_outq, PZERO - 1, __PRETTY_FUNCTION__);
                }
        splx(s);
        return (1);
}
#endif
/*
 * Process a write call on a tty device.
 */
int ttwrite(struct tty *tp, struct uio *uio, int flag)
{
        char *cp;
        size_t cc, ce;
        size_t hiwat, cnt;
        int error, s;
        char obuf[OBUFSIZ];

        hiwat = tp->t_hiwat;
        cnt   = uio->uio_resid;
        error = 0;
        cc    = 0;
loop:
        s = spltty();
        if (!ISSET(tp->t_state, TS_CARR_ON) && !ISSET(tp->t_cflag, CLOCAL))
        {
                if (ISSET(tp->t_state, TS_ISOPEN))
                {
                        splx(s);
                        return (-EIO);
                }
                else if (flag & IO_NDELAY)
                {
                        splx(s);
                        error = -EWOULDBLOCK;
                        goto out;
                }
                else
                {
                        /* Sleep awaiting carrier. */
                        error = ttysleep(tp, &tp->t_rawq, TTIPRI | PCATCH, ttopen, 0);
                        splx(s);
                        if (error)
                                goto out;
                        goto loop;
                }
        }
        splx(s);
/*
 * Hang the process if it's in the background.
 */
#if DAMIR
        if (isbackground(p, tp) && ISSET(tp->t_lflag, TOSTOP) && (p->p_flag & P_PPWAIT) == 0
            && (p->p_sigignore & sigmask(SIGTTOU)) == 0 && (p->p_sigmask & sigmask(SIGTTOU)) == 0 && p->p_pgrp->pg_jobc)
        {
                pgsignal(p->p_pgrp, SIGTTOU, 1);
                if (error = ttysleep(tp, &lbolt, TTIPRI | PCATCH, ttybg, 0))
                        goto out;
                goto loop;
        }
#endif
        /*
         * Process the user's data in at most OBUFSIZ chunks.  Perform any
         * output translation.  Keep track of high water mark, sleep on
         * overflow awaiting device aid in acquiring new space.
         */
        while (uio->uio_resid > 0 || cc > 0)
        {
                if (ISSET(tp->t_lflag, FLUSHO))
                {
                        uio->uio_resid = 0;
                        return (0);
                }

                if (tp->t_outq.c_cc > hiwat)
                {
                        goto ovhiwat;
                }
                /*
                 * Grab a hunk of data from the user, unless we have some
                 * leftover from last time.
                 */
                if (cc == 0)
                {
                        cc    = etl::min(uio->uio_resid, OBUFSIZ);
                        cp    = obuf;
                        error = uiomove(cp, cc, uio);
                        if (error)
                        {
                                cc = 0;
                                break;
                        }
                }
                /*
                 * If nothing fancy need be done, grab those characters we
                 * can handle without any of ttyoutput's processing and
                 * just transfer them to the output q.  For those chars
                 * which require special processing (as indicated by the
                 * bits in char_type), call ttyoutput.  After processing
                 * a hunk of data, look for FLUSHO so ^O's will take effect
                 * immediately.
                 */
                while (cc > 0)
                {
                        if (!ISSET(tp->t_oflag, OPOST))
                        {
                                ce = cc;
                        }
                        else
                        {
                                ce = cc - scanc((u32)cc, (u8 *)cp, (u8 *)char_type, CCLASSMASK);
                                /*
                                 * If ce is zero, then we're processing
                                 * a special character through ttyoutput.
                                 */
                                if (ce == 0)
                                {
                                        tp->t_rocount = 0;
                                        if (ttyoutput(*cp, tp) >= 0)
                                        {
                                                /* No Clists, wait a bit. */
                                                ttstart(tp);
#ifdef DAMIR
                                                if (error = ttysleep(tp, &lbolt, TTOPRI | PCATCH, ttybuf, 0))
                                                        break;
#endif
                                                goto loop;
                                        }

                                        cp++;
                                        cc--;
                                        if (ISSET(tp->t_lflag, FLUSHO) || tp->t_outq.c_cc > hiwat)
                                        {
                                                goto ovhiwat;
                                        }

                                        continue;
                                }
                        }
                        /*
                         * A bunch of normal characters have been found.
                         * Transfer them en masse to the output queue and
                         * continue processing at the top of the loop.
                         * If there are any further characters in this
                         * <= OBUFSIZ chunk, the first should be a character
                         * requiring special handling by ttyoutput.
                         */
                        tp->t_rocount = 0;
                        int i         = b_to_q(cp, ce, &tp->t_outq);
                        ce -= i;
                        tp->t_column += ce;
                        cp += ce, cc -= ce, tk_nout += ce;
                        tp->t_outcc += ce;
                        if (i > 0)
                        {
                                /* No Clists, wait a bit. */
                                ttstart(tp);
#if DAMIR
                                if (error = ttysleep(tp, &lbolt, TTOPRI | PCATCH, ttybuf, 0))
                                        break
#endif
                                            goto loop;
                        }
                        if (ISSET(tp->t_lflag, FLUSHO) || tp->t_outq.c_cc > hiwat)
                                break;
                }
                ttstart(tp);
        }
out:
        /*
         * If cc is nonzero, we leave the uio structure inconsistent, as the
         * offset and iov pointers have moved forward, but it doesn't matter
         * (the call will either return short or restart with a new uio).
         */
        uio->uio_resid += cc;
        return (error);

ovhiwat:
        ttstart(tp);
        s = spltty();
        /*
         * This can only occur if FLUSHO is set in t_lflag,
         * or if ttstart/oproc is synchronous (or very fast).
         */
        if (tp->t_outq.c_cc <= hiwat)
        {
                splx(s);
                goto loop;
        }
        if (flag & IO_NDELAY)
        {
                splx(s);
                uio->uio_resid += cc;
                return (uio->uio_resid == cnt ? -EWOULDBLOCK : 0);
        }
        SET(tp->t_state, TS_ASLEEP);
        error = ttysleep(tp, &tp->t_outq, TTOPRI | PCATCH, ttyout, 0);
        splx(s);
        if (error)
        {
                goto out;
        }
        goto loop;
}

/*
 * Rubout one character from the rawq of tp
 * as cleanly as possible.
 */
void ttyrub(int c, struct tty *tp)
{
        char *cp;
        integer_t savecol;
        int tabc, s;

        if (!ISSET(tp->t_lflag, ECHO) || ISSET(tp->t_lflag, EXTPROC))
        {
                return;
        }

        CLR(tp->t_lflag, FLUSHO);

        if (ISSET(tp->t_lflag, ECHOE))
        {
                if (tp->t_rocount == 0)
                {
                        /*
                         * Screwed by ttwrite; retype
                         */
                        ttyretype(tp);
                        return;
                }
                if (c == ('\t' | TTY_QUOTE) || c == ('\n' | TTY_QUOTE))
                {
                        ttyrubo(tp, 2);
                }
                else
                {
                        CLR(c, ~TTY_CHARMASK);
                        switch (CCLASS(c))
                        {
                        case ORDINARY:
                                ttyrubo(tp, 1);
                                break;
                        case BACKSPACE:
                        case CONTROL:
                        case NEWLINE:
                        case RETURN:
                        case VTAB:
                                if (ISSET(tp->t_lflag, ECHOCTL))
                                {
                                        ttyrubo(tp, 2);
                                }
                                break;
                        case TAB:
                                if (tp->t_rocount < tp->t_rawq.c_cc)
                                {
                                        ttyretype(tp);
                                        return;
                                }
                                s       = spltty();
                                savecol = tp->t_column;
                                SET(tp->t_state, TS_CNTTB);
                                SET(tp->t_lflag, FLUSHO);
                                tp->t_column = tp->t_rocol;
                                cp           = tp->t_rawq.c_cf;

                                if (cp)
                                {
                                        tabc = *cp; /* XXX FIX NEXTC */
                                }

                                for (; cp; cp = nextc(&tp->t_rawq, cp, &tabc))
                                {
                                        ttyecho(tabc, tp);
                                }
                                CLR(tp->t_lflag, FLUSHO);
                                CLR(tp->t_state, TS_CNTTB);
                                splx(s);

                                /* savecol will now be length of the tab. */
                                savecol -= tp->t_column;
                                tp->t_column += savecol;
                                if (savecol > 8)
                                {
                                        savecol = 8; /* overflow screw */
                                }

                                while (--savecol >= 0)
                                {
                                        ttyoutput('\b', tp);
                                }

                                break;
                        default: /* XXX */
#define PANICSTR "ttyrub: would panic c = %d, val = %d\n"
                                panic(PANICSTR, c, CCLASS(c));
#ifdef notdef
                                panic(PANICSTR, c, CCLASS(c));
#endif
                        }
                }
        }
        else if (ISSET(tp->t_lflag, ECHOPRT))
        {
                if (!ISSET(tp->t_state, TS_ERASE))
                {
                        SET(tp->t_state, TS_ERASE);
                        ttyoutput('\\', tp);
                }
                ttyecho(c, tp);
        }
        else
        {
                ttyecho(tp->t_cc[VERASE], tp);
        }
        --tp->t_rocount;
}

/*
 * Back over cnt characters, erasing them.
 */
void ttyrubo(struct tty *tp, int cnt)
{
        while (cnt-- > 0)
        {
                ttyoutput('\b', tp);
                ttyoutput(' ', tp);
                ttyoutput('\b', tp);
        }
}

/*
 * ttyretype --
 *	Reprint the rawq line.  Note, it is assumed that c_cc has already
 *	been checked.
 */
void ttyretype(struct tty *tp)
{
        char *cp;
        int s, c;

        /* Echo the reprint character. */
        if (tp->t_cc[VREPRINT] != _POSIX_VDISABLE)
        {
                ttyecho(tp->t_cc[VREPRINT], tp);
        }

        ttyoutput('\n', tp);

        /*
         * XXX
         * FIX: NEXTC IS BROKEN - DOESN'T CHECK QUOTE
         * BIT OF FIRST CHAR.
         */
        s = spltty();
        for (cp = tp->t_canq.c_cf, c = (cp != NULL ? *cp : 0); cp != NULL; cp = nextc(&tp->t_canq, cp, &c))
        {
                ttyecho(c, tp);
        }

        for (cp = tp->t_rawq.c_cf, c = (cp != NULL ? *cp : 0); cp != NULL; cp = nextc(&tp->t_rawq, cp, &c))
        {
                ttyecho(c, tp);
        }

        CLR(tp->t_state, TS_ERASE);
        splx(s);

        tp->t_rocount = tp->t_rawq.c_cc;
        tp->t_rocol   = 0;
}

/*
 * Echo a typed character to the terminal.
 */
void ttyecho(int c, struct tty *tp)
{
        if (!ISSET(tp->t_state, TS_CNTTB))
        {
                CLR(tp->t_lflag, FLUSHO);
        }
        if ((!ISSET(tp->t_lflag, ECHO) && (!ISSET(tp->t_lflag, ECHONL) || c == '\n')) || ISSET(tp->t_lflag, EXTPROC))
        {
                return;
        }
        if (ISSET(tp->t_lflag, ECHOCTL)
            && (ISSET(c, TTY_CHARMASK) <= 037 && c != '\t' && c != '\n' || ISSET(c, TTY_CHARMASK) == 0177))
        {
                ttyoutput('^', tp);
                CLR(c, ~TTY_CHARMASK);
                if (c == 0177)
                {
                        c = '?';
                }
                else
                {
                        c += 'A' - 1;
                }
        }
        ttyoutput(c, tp);
}

/*
 * Wake up any readers on a tty.
 */
void ttwakeup(struct tty *tp)
{
        // select_wakeup(&tp->t_rsel);

        poll_wakeup(&tp->t_pollfds, POLLIN | POLLRDNORM);

        if (ISSET(tp->t_state, TS_ASYNC))
        {
                // tp->t_pgrp->signal(SIGIO, 1);
                // process_group_signal(tp->t_pgrp, SIGIO);
                // killpg(tp->t_foreground_group, SIGIO);
                utask_group *utask_grp = utask_group_find(tp->t_foreground_group);
                KASSERT(utask_grp != nullptr);
                utask_group_signal(utask_grp, SIGIO);
        }

        // wakeup((char *)&tp->t_rawq);
        uthread_wakeup(&tp->t_rawq);
}

/*
 * Look up a code for a specified speed in a conversion table;
 * used by drivers to map software speed values to hardware parameters.
 */
int ttspeedtab(int speed, struct speedtab *table)
{
        for (; table->sp_speed != -1; table++)
        {
                if (table->sp_speed == speed)
                {
                        return (table->sp_code);
                }
        }
        return (-1);
}

/*
 * Set tty hi and low water marks.
 *
 * Try to arrange the dynamics so there's about one second
 * from hi to low water.
 *
 */
void ttsetwater(struct tty *tp)
{
        size_t cps, x;

#define CLAMP(x, h, l) (size_t)((x) > h ? h : ((x) < l) ? l : (x))

        cps         = tp->t_ospeed / 10;
        tp->t_lowat = x = CLAMP(cps / 2, TTMAXLOWAT, TTMINLOWAT);
        x += cps;
        x           = CLAMP(x, TTMAXHIWAT, TTMINHIWAT);
        tp->t_hiwat = roundup(x, CBSIZE);
#undef CLAMP
}

/*
 * Report on state of foreground process group.
 */
#if DAMIR

void ttyinfo(struct tty *tp)
{
        process_t p, pick;
        struct timeval utime, stime;
        int tmp;

        if (ttycheckoutq(tp, 0) == 0)
                return;

/* Print load average. */
#if DAMIR
        tmp = (averunnable.ldavg[0] * 100 + FSCALE / 2) >> FSHIFT;
#else
        tmp = 1;
#endif
        ttyprintf(tp, "load: %d.%02d ", tmp / 100, tmp % 100);

        if (tp->t_session == NULL)
                ttyprintf(tp, "not a controlling terminal\n");
        else if (tp->t_pgrp == NULL)
                ttyprintf(tp, "no foreground process group\n");
        else if ((p = tp->t_pgrp->pg_members.lh_first) == NULL)
                ttyprintf(tp, "empty foreground process group\n");
        else
        {
/* Pick interesting process. */
#if DAMIR
                for (pick = NULL; p != NULL; p = p->p_pgrpnxt)
                        if (proc_compare(pick, p))
                                pick = p;

                ttyprintf(tp, " cmd: %s %d [%s] ", pick->p_comm, pick->p_pid, pick->p_stat == SRUN ? "running" : "waiting");
                /*		    pick->p_wmesg ? pick->p_wmesg : "iowait"); */

                calcru(pick, &utime, &stime, NULL);
#else
                utime = {0};
                stime = {0};

#endif

                /* Print user time. */
                ttyprintf(tp, "%d.%02du ", utime.tv_sec, (utime.tv_usec + 5000) / 10000);

                /* Print system time. */
                ttyprintf(tp, "%d.%02ds ", stime.tv_sec, (stime.tv_usec + 5000) / 10000);

#define pgtok(a) (((a)*NBPG) / 1024)
                /* Print percentage cpu, resident set size. */
#ifdef DAMIR
                tmp = pick->p_pctcpu * 10000 + FSCALE / 2 >> FSHIFT;

                ttyprintf(tp,
                          "%d%% %dk\n",
                          tmp / 100,
                          pick->p_stat == SIDL || pick->p_stat == SZOMB ? 0 :
#ifdef pmap_resident_count
                                                                        pgtok(pmap_resident_count(&pick->p_vmspace->vm_pmap))
#else
                                                                        pgtok(pick->p_vmspace->vm_rssize)
#endif

                );
#endif
        }
        tp->t_rocount = 0; /* so pending input will be retyped if BS */
}

/*
 * Returns 1 if p2 is "better" than p1
 *
 * The algorithm for picking the "interesting" process is thus:
 *
 *	1) Only foreground processes are eligible - implied.
 *	2) Runnable processes are favored over anything else.  The runner
 *	   with the highest cpu utilization is picked (p_estcpu).  Ties are
 *	   broken by picking the highest pid.
 *	3) The sleeper with the shortest sleep time is next.  With ties,
 *	   we pick out just "short-term" sleepers (P_SINTR == 0).
 *	4) Further ties are broken by picking the highest pid.
 */
#define ISRUN(p)     (((p)->p_stat == SRUN) || ((p)->p_stat == SIDL))
#define TESTAB(a, b) ((a) << 1 | (b))
#define ONLYA        2
#define ONLYB        1
#define BOTH         3

int proc_compare(process_t p1, process_t p2)
{
        if (p1 == NULL)
                return (1);
                /*
                 * see if at least one of them is runnable
                 */
#if DAMIR
        switch (TESTAB(ISRUN(p1), ISRUN(p2)))
        {
        case ONLYA:
                return (0);
        case ONLYB:
                return (1);
        case BOTH:
                /*
                 * tie - favor one with highest recent cpu utilization
                 */

                if (p2->p_estcpu > p1->p_estcpu)
                        return (1);
                if (p1->p_estcpu > p2->p_estcpu)
                        return (0);
                return (p2->p_pid > p1->p_pid); /* tie - return highest pid */
        }
#endif
        /*
         * weed out zombies
         */
        switch (TESTAB(p1->p_stat == SZOMB, p2->p_stat == SZOMB))
        {
        case ONLYA:
                return (1);
        case ONLYB:
                return (0);
        case BOTH:
                return (p2->p_pid > p1->p_pid); /* tie - return highest pid */
        }
#if DAMIR
        /*
         * pick the one with the smallest sleep time
         */
        if (p2->p_slptime > p1->p_slptime)
                return (0);
        if (p1->p_slptime > p2->p_slptime)
                return (1);
#endif
        /*
         * favor one sleeping in a non-interruptible sleep
         */
        if (p1->p_flag & P_SINTR && (p2->p_flag & P_SINTR) == 0)
                return (1);
        if (p2->p_flag & P_SINTR && (p1->p_flag & P_SINTR) == 0)
                return (0);
        return (p2->p_pid > p1->p_pid); /* tie - return highest pid */
}

#endif

/*
 * Output char to tty; console putchar style.
 */
int tputchar(int c, struct tty *tp)
{
        int s;

        s = spltty();
        if (ISSET(tp->t_state, TS_CARR_ON | TS_ISOPEN) != (TS_CARR_ON | TS_ISOPEN))
        {
                splx(s);
                return (-1);
        }

        if (c == '\n')
        {
                ttyoutput('\r', tp);
        }

        ttyoutput(c, tp);
        ttstart(tp);
        splx(s);
        return (0);
}

/*
 * Sleep on chan, returning ERESTART if tty changed while we napped and
 * returning any errors (e.g. EINTR/ETIMEDOUT) reported by tsleep.  If
 * the tty is revoked, restarting a pending call will redo validation done
 * at the start of the call.
 */

int ttysleep(struct tty *tp, void *chan, int pri, char *wmesg, int timeout)
{
        integer_t gen = tp->t_gen;
        KASSERT(timeout == 0);
        int error = uthread_sleep(chan); // tsleep(chan, pri, wmesg, timeout);

        if (error)
        {
                return -ERESTARTSYS;
        }

        return (tp->t_gen == gen ? 0 : -ERESTARTSYS);
}
