#include <aux/init.h>

#include <sys/ioctl.h>

#include <emerixx/ioctl_req.h>
#include <emerixx/chrdev.h>
#include <emerixx/process.h>
#include <emerixx/vfs/vfs_node.h>
#include <emerixx/vfs/vfs_node_data.h>
#include <emerixx/vfs/vfs_file_data.h>
#include <emerixx/vfs/vfs_file.h>

#include <sys/sysmacros.h>

#include "tty.h"

static chrdev_data s_cttydev;

#define TTY_CTTY    0
#define TTY_CONSOLE 1

#define cttyvp(p) ((p)->p_flag & P_CONTROLT ? (p)->p_pgrp->pg_session->s_ttyvp : NULL)
// ********************************************************************************************************************************
static kern_return_t ctty_open(vfs_node_t a_vfsn, vfs_file_t a_vfsf)
// ********************************************************************************************************************************
{
        kern_return_t retval = KERN_FAIL(ENXIO);

        switch (minor(a_vfsn->m_rdev))
        {
                case TTY_CTTY:
                {
                        chrdev_t cttydev;
                        retval = driver_chrdev_lookup(utask_self()->m_ctty, &cttydev);

                        if (KERN_STATUS_SUCCESS(retval))
                        {
                                a_vfsf->m_fops = cttydev->m_fops;
                                dev_t olddev   = a_vfsn->m_rdev;
                                a_vfsn->m_rdev = cttydev->m_beg;
                                retval         = a_vfsf->m_fops->fopen(a_vfsn, a_vfsf);
                                a_vfsn->m_rdev = olddev;
                                if (KERN_STATUS_SUCCESS(retval))
                                {
                                        struct ioctl_req_data ioctl_req;

                                        ioctl_req_create(TIOCSCTTY, 0, false, &ioctl_req);

                                        vfs_file_ioctl(a_vfsf, &ioctl_req);
                                        // utask_self()->m_ctty = makedev(0,0);
                                }
                        }
                }
                break;

                case TTY_CONSOLE:
                {
                        /* code */
                        panic("naynay");
                }
                break;

                default:
                        break;
        }

        return retval;

        // chrdev_t cttydev;
        // kern_return_t retval = driver_chrdev_lookup(utask_self()->m_ctty, &cttydev);

        // if (KERN_STATUS_FAILURE(retval))
        //{
        //         return retval;
        // }

#if 0
  vnode::ptr tty;
        kern_return_t retval = specfs_cdevvp(utask_self()->m_ctty, tty);
        // retval should return EIO, atleast in bsd
        if (KERN_STATUS_SUCCESS(retval))
        {
                tty->lock();
                tty->open(a_file, flag);
                tty->unlock();
        }

        return retval;

        panic("Open");

        struct vnode *ttyvp; // = cttyvp(pthread_self());
        int error;

        if (ttyvp == NULL)
                return -ENXIO;
        ttyvp->lock();
#ifdef PARANOID
        /*
         * Since group is tty and mode is 620 on most terminal lines
         * and since sessions protect terminals from processes outside
         * your session, this check is probably no longer necessary.
         * Since it inhibits setuid root programs that later switch
         * to another user from accessing /dev/tty, we have decided
         * to delete this test. (mckusick 5/93)
         */
        error = VOP_ACCESS(ttyvp, (flag & FREAD ? VREAD : 0) | (flag & FWRITE ? VWRITE : 0), p->p_ucred, p);
        if (!error)
#endif /* PARANOID */
                error = ttyvp->open(flag, nullptr);
        ttyvp->unlock();

        return (error);
#endif
}

// ********************************************************************************************************************************
static kern_return_t ctty_close(vfs_file_t a_file)
// ********************************************************************************************************************************
{
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t ctty_readwrite(vfs_file_t a_file, struct uio *a_uio)
// ********************************************************************************************************************************
{
        return KERN_SUCCESS;

#if 0
  vnode::ptr tty;
        kern_return_t retval = specfs_cdevvp(utask_self()->m_ctty, tty);
        // retval should return EIO, atleast in bsd
        if (KERN_STATUS_SUCCESS(retval))
        {
                tty->lock();
                retval = tty->uio_move(a_file, uio, flag);
                tty->unlock();
        }

        return retval;


        KASSERT("read" == nullptr);
        struct vnode *ttyvp; // = cttyvp(uio->uio_procp);
        int error;

        if (ttyvp == NULL)
                return -EIO;

        ttyvp->lock();
        error = ttyvp->uio_move(uio, flag);
        ttyvp->unlock();
        return (error);
#endif
}

// ********************************************************************************************************************************
static kern_return_t ctty_ioctl(vfs_file_t a_file, ioctl_req_t a_cmd, void *)
// ********************************************************************************************************************************
{
        return KERN_SUCCESS;
#if 0
  vnode::ptr tty;
        kern_return_t retval = specfs_cdevvp(utask_self()->m_ctty, tty);
        // retval should return EIO, atleast in bsd
        if (KERN_STATUS_SUCCESS(retval))
        {
                tty->lock();
                retval = tty->ioctl(a_file, cmd, addr, flag);
                tty->unlock();
        }

        return retval;


        struct vnode *ttyvp; // = cttyvp(p->process);

        if (ttyvp == NULL)
                return (-EIO);
        if (cmd == TIOCNOTTY)
        {
                if (!p->process->is_session_leader())
                {
                        p->process->p_flag &= ~P_CONTROLT;
                        return (0);
                }
                else
                        return -EINVAL;
        }
        return ttyvp->ioctl(cmd, addr, flag, NOCRED, p);
#endif
        return 0;
}

// ********************************************************************************************************************************
static kern_return_t cttypoll(vfs_file_t a_file, pollfd **a_pfd)
// ********************************************************************************************************************************
{
        return KERN_SUCCESS;
#if 0
        vnode::ptr tty;
        kern_return_t retval = specfs_cdevvp(utask_self()->m_ctty, tty);
        // retval should return EIO, atleast in bsd
        if (KERN_STATUS_SUCCESS(retval))
        {
                tty->lock();
                retval = tty->poll(a_file, a_pfd);
                tty->unlock();
        }

        return retval;
#endif
}

static struct vfs_file_operations_data s_cttydev_ops = {
        //
        .fopen = ctty_open,
        //
};

static kern_return_t ctty_device_init()
{
        // KASSERT(specfs_register_character_device(makedev(17, 0), 0x1, &s_ctty) == KERN_SUCCESS);
        kern_return_t retval = driver_chrdev_alloc(&s_cttydev, "tty", &s_cttydev_ops, makedev(17, 0), 1);

        return retval;
}

device_initcall(ctty_device_init);
