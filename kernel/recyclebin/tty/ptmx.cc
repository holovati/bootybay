#include <aux/init.h>

#include <etl/algorithm.hh>

#include <kernel/zalloc.h>

#include <emerixx/chrdev.h>
#include <emerixx/poll.h>
#include <emerixx/process.h>
#include <emerixx/sleep.h>
#include <emerixx/ioctl_req.h>
#include <emerixx/vfs/vfs_node.h>
#include <emerixx/vfs/vfs_file_data.h>

#include <emerixx/memory_rw.h>

#include <bits/signal.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/sysmacros.h>
#include <sys/ttydefaults.h>
#include <termios.h>

typedef struct pty_data *pty_t;

#define MAX_PTY 0x20

#define FTOPTY(f) (*(pty_t *)(&(f)->m_private))

#define PTY_MASTER_OPEN BIT0
#define PTY_SLAVE_OPEN  BIT1
#define PTY_PACKET_MODE BIT2

typedef struct tty_data *tty_t;

#define IOBUF_SIZE 0x800
#define IOBUF_MASK (IOBUF_SIZE - 1)

#define TTY_OUTPUT_RD_CNT(tty)          ((tty)->m_output.wpos - (tty)->m_output.rpos)
#define TTY_OUTPUT_WR_CNT(tty)          (IOBUF_SIZE - TTY_OUTPUT_RD_CNT(tty))
#define TTY_OUTPUT_RD_POS(tty)          ((tty)->m_output.rpos & IOBUF_MASK)
#define TTY_OUTPUT_WR_POS(tty)          ((tty)->m_output.wpos & IOBUF_MASK)
#define TTY_OUTPUT_FULL(tty)            (TTY_OUTPUT_WR_CNT(tty) < 4)
#define TTY_OUTPUT_EMPTY(tty)           (TTY_OUTPUT_RD_CNT(tty) == 0)
#define TTY_OUTPUT_RD_PTR(tty)          (&(tty)->m_output.buf[TTY_OUTPUT_RD_POS(tty)])
#define TTY_OUTPUT_WR_PTR(tty)          (&(tty)->m_output.buf[TTY_OUTPUT_WR_POS(tty)])
#define TTY_OUTPUT_RD_ADVANCE(tty, cnt) ((tty)->m_output.rpos += (cnt))
#define TTY_OUTPUT_WR_ADVANCE(tty, cnt) ((tty)->m_output.wpos += (cnt))
#define TTY_OUTPUT_WR_LEN(tty)          (etl::min(TTY_OUTPUT_WR_CNT(tty), IOBUF_SIZE - TTY_OUTPUT_WR_POS(tty)))
#define TTY_OUTPUT_RD_LEN(tty)          (etl::min(TTY_OUTPUT_RD_CNT(tty), IOBUF_SIZE - TTY_OUTPUT_RD_POS(tty)))
#define TTY_OUTPUT_PUSH_CHAR(tty, c)                                                                                               \
        do                                                                                                                         \
        {                                                                                                                          \
                TTY_OUTPUT_WR_PTR(tty)[0] = (c);                                                                                   \
                TTY_OUTPUT_WR_ADVANCE(tty, 1);                                                                                     \
        }                                                                                                                          \
        while (0)

#define TTY_INPUT_RD_CNT(tty)          ((tty)->m_input.wpos - (tty)->m_input.rpos)
#define TTY_INPUT_WR_CNT(tty)          (IOBUF_SIZE - TTY_INPUT_RD_CNT(tty))
#define TTY_INPUT_RD_POS(tty)          ((tty)->m_input.rpos & IOBUF_MASK)
#define TTY_INPUT_WR_POS(tty)          ((tty)->m_input.wpos & IOBUF_MASK)
#define TTY_INPUT_FULL(tty)            (TTY_INPUT_WR_CNT(tty) < 4)
#define TTY_INPUT_EMPTY(tty)           (TTY_INPUT_RD_CNT(tty) == 0)
#define TTY_INPUT_RD_PTR(tty)          (&(tty)->m_input.buf[TTY_INPUT_RD_POS(tty)])
#define TTY_INPUT_WR_PTR(tty)          (&(tty)->m_input.buf[TTY_INPUT_WR_POS(tty)])
#define TTY_INPUT_RD_ADVANCE(tty, cnt) ((tty)->m_input.rpos += (cnt))
#define TTY_INPUT_WR_ADVANCE(tty, cnt) ((tty)->m_input.wpos += (cnt))
#define TTY_INPUT_WR_LEN(tty)          (etl::min(TTY_INPUT_WR_CNT(tty), IOBUF_SIZE - TTY_INPUT_WR_POS(tty)))
#define TTY_INPUT_RD_LEN(tty)          (etl::min(TTY_INPUT_RD_CNT(tty), IOBUF_SIZE - TTY_INPUT_RD_POS(tty)))

#define TTY_INPUT_CLEAR(tty) (((tty)->m_input.rpos = (tty)->m_input.wpos))

#define TTY_INPUT_POP_CHAR(tty, c)                                                                                                 \
        do                                                                                                                         \
        {                                                                                                                          \
                (*(c)) = *TTY_INPUT_RD_PTR(tty);                                                                                   \
                TTY_INPUT_RD_ADVANCE(tty, 1);                                                                                      \
        }                                                                                                                          \
        while (0)

#define TTY_INPUT_PUSH_CHAR(tty, c)                                                                                                \
        do                                                                                                                         \
        {                                                                                                                          \
                TTY_INPUT_WR_PTR(tty)[0] = (c);                                                                                    \
                TTY_INPUT_WR_ADVANCE(tty, 1);                                                                                      \
        }                                                                                                                          \
        while (0)

#define TTY_LOCAL_FLAGS(tty)            ((tty)->m_tio.c_lflag)
#define TTY_LOCAL_FLAGS_IS_SET(tty, fl) ((TTY_LOCAL_FLAGS(tty) & (fl)) == (fl))

#define TTY_INPUT_FLAGS(tty)  ((tty)->m_tio.c_iflag)
#define TTY_OUTPUT_FLAGS(tty) ((tty)->m_tio.c_oflag)

#define TTY_CONTROL_CHAR(tty, idx) ((tty)->m_tio.c_cc[idx])

struct tty_data
{
        winsize m_ws;
        termios m_tio;
        pid_t m_fg_pgid;
        queue_head_t m_ptm_pollrecs; // Records of all poll requests on the master
        queue_head_t m_pts_pollrecs; // Records of all poll requests on the slave
        struct
        {
                natural_t rpos; // Read position (tail)
                natural_t wpos; // Write position (head)
                char buf[IOBUF_SIZE];
        } m_input, m_output;
        // char __tiopad[sizeof(m_fg_pgid) % sizeof(natural_t)];
};

struct pty_data
{
        chrdev_data m_slavedev;
        integer_t m_flags;
        integer_t m_tiockt_flags;
        integer_t m_slave_refs;
        tty_data m_tty;
};

ZONE_DEFINE(pty_data, s_pty_zone, MAX_PTY, MAX_PTY / 2, ZONE_EXHAUSTIBLE, MAX_PTY / 8);

static chrdev_data s_ptmxdev;

// ********************************************************************************************************************************
static kern_return_t pts_open(vfs_node_t a_vfsn, vfs_file_t a_file) // Slave open
// ********************************************************************************************************************************
{
        kern_return_t retval = driver_chrdev_lookup(vfs_node_device(a_vfsn), (chrdev_t *)&FTOPTY(a_file));

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        process_t proc = utask_self();

        if ((proc->m_ctty == 0xFFFF) && (proc->m_sid == proc->m_pid))
        {
                // Session  leader with no terminal, get this one.
                proc->m_ctty = vfs_node_device(a_vfsn);
        }

        // Record the pty structure in the slave file
        pty_t pty = FTOPTY(a_file); // = ((pty_t)((chrdev_t)a_file->m_vnode->v_un.vu_private));

        if (pty->m_slave_refs == 0)
        {
                pty->m_flags |= PTY_SLAVE_OPEN;
        }

        pty->m_slave_refs++;

        return retval;
}

// ********************************************************************************************************************************
static kern_return_t pts_read(vfs_file_t a_file, struct uio *a_uio) // Slave read
// ********************************************************************************************************************************
{
        pty_t pty = FTOPTY(a_file);
        tty_t tty = &pty->m_tty;

        kern_return_t retval = KERN_SUCCESS;

        while (TTY_INPUT_EMPTY(tty))
        {
                if (a_file->m_oflags & O_NONBLOCK)
                {
                        return KERN_FAIL(EWOULDBLOCK);
                }

                // Wait for data to be written to the slave
                retval = uthread_sleep(&tty->m_input);

                if (KERN_STATUS_FAILURE(retval))
                {
                        return retval;
                }
        }

        if (TTY_LOCAL_FLAGS(tty) & ICANON)
        {
                panic("The terminal is in canonical mode");
        }

        while ((a_uio->uio_resid > 0) && (TTY_INPUT_EMPTY(tty) == false) && KERN_STATUS_SUCCESS(retval))
        {
                char obuf[0x100];
                natural_t rdlen = etl::min(a_uio->uio_resid, sizeof(obuf));
                natural_t rdidx = 0;
                while ((rdidx < rdlen) && (TTY_INPUT_EMPTY(tty) == false))
                {
                        char curchar;

                        TTY_INPUT_POP_CHAR(tty, &curchar);

                        // Strip off eighth bit.
                        if (TTY_INPUT_FLAGS(tty) & ISTRIP)
                        {
                                curchar &= 0x7F;
                        }

                        if ((TTY_INPUT_FLAGS(tty) & INLCR) && (curchar == '\n'))
                        {
                                curchar = '\r';
                        }
                        else if (curchar == '\r')
                        {
                                if (TTY_INPUT_FLAGS(tty) & IGNCR)
                                {
                                        // Nothing
                                        continue;
                                }
                                else if (TTY_INPUT_FLAGS(tty) & ICRNL)
                                {
                                        curchar = '\r';
                                }
                        }

                        obuf[rdidx++] = curchar;
                }

                if (KERN_STATUS_SUCCESS(retval))
                {
                        retval = uiomove(obuf, rdidx, a_uio);
                }
        }

        if (TTY_INPUT_FULL(tty) == false)
        {
                // Wake up the master
                poll_wakeup(&pty->m_tty.m_ptm_pollrecs, POLLOUT | POLLWRNORM);
        }

        return retval;
}

// ********************************************************************************************************************************
static natural_t pts_opost_cb(char const *a_buf, natural_t a_count, void *a_arg) // Slave output post-processing
// ********************************************************************************************************************************
{
        pty_t pty = FTOPTY((vfs_file_t)(a_arg));
        tty_t tty = &pty->m_tty;

        natural_t i;

        for (i = 0; (i < a_count) && (TTY_OUTPUT_FULL(tty) == false); i++)
        {
                if ((TTY_OUTPUT_FLAGS(tty) & ONLCR) && (a_buf[i] == '\n'))
                {
                        TTY_OUTPUT_PUSH_CHAR(tty, '\r');
                        TTY_OUTPUT_PUSH_CHAR(tty, '\n');
                }
                else if ((TTY_OUTPUT_FLAGS(tty) & OCRNL) && (a_buf[i] == '\r'))
                {
                        TTY_OUTPUT_PUSH_CHAR(tty, '\n');
                }
                else if ((TTY_OUTPUT_FLAGS(tty) & ONOCR) && (a_buf[i] == '\r'))
                {
                        // Do nothing
                }
                else if ((TTY_OUTPUT_FLAGS(tty) & ONLRET) && (a_buf[i] == '\r'))
                {
                        // Do nothing
                }
                else
                {
                        TTY_OUTPUT_PUSH_CHAR(tty, a_buf[i]);
                }
        }
        return i;
}

// ******************************************* *************************************************************************************
static kern_return_t pts_write(vfs_file_t a_file, struct uio *a_uio) // Slave write
// ********************************************************************************************************************************
{
        pty_t pty = FTOPTY(a_file);
        tty_t tty = &pty->m_tty;

        kern_return_t retval = KERN_SUCCESS;
        natural_t oresid     = a_uio->uio_resid;

        while ((a_uio->uio_resid > 0) && KERN_STATUS_SUCCESS(retval))
        {
                if (TTY_OUTPUT_FULL(tty))
                {
                        if (a_uio->uio_resid < oresid)
                        {
                                // We have written some data, so return
                                break;
                        }

                        if (a_file->m_oflags & O_NONBLOCK)
                        {
                                retval = KERN_FAIL(EWOULDBLOCK);
                        }
                        else
                        {
                                // Wait for data to be read from the master
                                retval = uthread_sleep(&tty->m_output);
                        }
                        continue;
                }

                if ((TTY_OUTPUT_FLAGS(tty) & OPOST) == 0)
                {
                        // Copy the data directly to the output buffer
                        natural_t wrlen = TTY_OUTPUT_WR_LEN(tty);
                        char *wrptr     = TTY_OUTPUT_WR_PTR(tty);

                        retval = uiomove2(wrptr, &wrlen, a_uio);

                        if (KERN_STATUS_SUCCESS(retval))
                        {
                                TTY_OUTPUT_WR_ADVANCE(tty, wrlen);
                        }
                }
                else
                {
                        // Copy the data to a temporary buffer as we need to process it
                        retval = uiomove3(a_uio, pts_opost_cb, a_file);
                }
        }

        if (TTY_OUTPUT_EMPTY(tty) == false)
        {
                // Wake up the master
                poll_wakeup(&pty->m_tty.m_ptm_pollrecs, POLLIN | POLLRDNORM);
        }

        return retval;
}

// ********************************************************************************************************************************
static kern_return_t pts_ioctl(vfs_file_t a_file, ioctl_req_t a_cmd) // Slave ioctl
// ********************************************************************************************************************************
{
        pty_t pty            = FTOPTY(a_file);
        tty_t tty            = &pty->m_tty;
        integer_t cmdno      = ioctl_req_cmdno(a_cmd);
        kern_return_t retval = KERN_SUCCESS;

        switch (cmdno)
        {
                case TCGETS: // Get the current serial port settings
                        retval = ioctl_req_copyout(a_cmd, &tty->m_tio);
                        break;
                case TCSETS: // Set the current serial port settings (TCSANOW)
                        retval = ioctl_req_copyin(a_cmd, &tty->m_tio);
                        break;
                case TCSETSW: // Allow the output buffer to drain, and set the current serial port settings (TCSADRAIN)
                        retval = ioctl_req_read(a_cmd, &tty->m_tio, sizeof(tty->m_tio));
                        break;
                case TCSETSF: // Allow the output buffer to drain, discard pending input, and set the current serial port settings
                              // (TCSAFLUSH)
                        retval = ioctl_req_read(a_cmd, &tty->m_tio, sizeof(tty->m_tio));
                        break;
                case TCXONC:
                        // log_debug("TODO: Software flow control");
                        asm("nop");
                        break;

                case TIOCGPGRP: // Get the process group ID of the foreground process group on this terminal
                        retval = ioctl_req_write(a_cmd, &tty->m_fg_pgid, sizeof(tty->m_fg_pgid));
                        break;
                case TIOCSPGRP: // Set the foreground process group ID of this terminal
                {
                        if (tty->m_fg_pgid == 0)
                        {
                                retval = KERN_FAIL(EINVAL);
                                break;
                        }

                        retval = ioctl_req_read(a_cmd, &tty->m_fg_pgid, sizeof(tty->m_fg_pgid));
                }
                break;
                case TIOCSWINSZ: // Set terminal window size
                        retval = ioctl_req_read(a_cmd, &tty->m_ws, sizeof(tty->m_ws));
                        break;
                case TIOCGWINSZ: // Get terminal windows size
                        retval = ioctl_req_write(a_cmd, &tty->m_ws, sizeof(tty->m_ws));
                        break;
                case TIOCSCTTY:
                {
                        process_t utsk = utask_self();

                        // Must be session leader
                        if ((utsk->m_pid != utsk->m_sid))
                        {
                                return KERN_FAIL(EPERM);
                        }

                        // tp->t_session_id       = utsk->m_sid;
                        // tp->t_session_leader   = utsk->m_pid;
                        tty->m_fg_pgid       = utsk->m_pgid;
                        utask_self()->m_ctty = pty->m_slavedev.m_beg;

                        // log_debug("Make this a controlling terminal of the process");
                }
                break;
                case FIONBIO: // Activate non blocking io
                {
#if is_already_set_this_call_is_just_to_inform_the_device_about_it
                        int dontblock = ioctl_req_arg(a_cmd);

                        if (dontblock != 0)
                        {
                                a_file->m_oflags |= O_NONBLOCK;
                        }
                        else
                        {
                                a_file->m_oflags &= ~O_NONBLOCK;
                        }
#endif
                }
                break;
                default:
                        panic("UNHANDLED IOCTL %p", cmdno);
        }

        return retval;
}

// ********************************************************************************************************************************
static kern_return_t pts_close(vfs_file_t a_file) // Slave close
// ********************************************************************************************************************************
{
        pty_t pty = FTOPTY(a_file);

        if (--(pty->m_slave_refs))
        {
                return KERN_SUCCESS;
        }

        pty->m_flags &= ~PTY_SLAVE_OPEN;

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t pts_poll(vfs_file_t a_file, struct pollfd **a_pfd) // Slave poll
// ********************************************************************************************************************************
{
        pty_t pty = FTOPTY(a_file);
        tty_t tty = &pty->m_tty;

        if ((*a_pfd)->events & (POLLIN | POLLRDNORM))
        {
                if (TTY_INPUT_EMPTY(tty) == false)
                {
                        (*a_pfd)->revents |= (*a_pfd)->events & (short)(POLLIN | POLLRDNORM);
                }
        }

        if ((*a_pfd)->events & (POLLOUT | POLLWRNORM))
        {
                if (TTY_OUTPUT_FULL(tty) == false)
                {
                        (*a_pfd)->revents |= (*a_pfd)->events & (short)(POLLOUT | POLLWRNORM);
                }
        }

        return poll_record(&pty->m_tty.m_pts_pollrecs, a_pfd);
}

// ********************************************************************************************************************************
static kern_return_t pty_lseek(vfs_file_t, off_t a_offset, int a_whence) // Illegal seek for both ptm and pts
// ********************************************************************************************************************************
{
        return KERN_FAIL(ESPIPE);
}

static struct vfs_file_operations_data s_ptsdev_ops = {
        //
        .fopen  = pts_open,  //
        .fread  = pts_read,  //
        .fwrite = pts_write, //
        .flseek = pty_lseek, //
        .fioctl = pts_ioctl, //
        .fclose = pts_close, //
        .fpoll  = pts_poll
};

// ********************************************************************************************************************************
static kern_return_t ptmx_open(vfs_node_t, vfs_file_t a_file) // Master open
// ********************************************************************************************************************************
{
        pty_t pty = s_pty_zone.alloc();

        if (pty == nullptr)
        {
                return KERN_FAIL(ENOMEM);
        }

        // Find a free slot for for the slave
        kern_return_t retval;

        for (integer_t i = 0; i < MAX_PTY; ++i)
        {
                retval = driver_chrdev_alloc(&pty->m_slavedev, "pts", &s_ptsdev_ops, makedev(19, i), 1);

                if (retval != KERN_FAIL(EINVAL))
                {
                        break;
                }
        }

        if (KERN_STATUS_FAILURE(retval))
        {
                s_pty_zone.free(pty);
                return KERN_FAIL(ENOMEM);
        }

        pty->m_flags = PTY_MASTER_OPEN;

        pty->m_tiockt_flags = TIOCPKT_DATA;

        pty->m_slave_refs = 0;

        // Initialize the tty data
        tty_t tty = &pty->m_tty;

        *tty = {};

        // Input modes
        tty->m_tio.c_iflag = TTYDEF_IFLAG;

        // Output modes
        tty->m_tio.c_oflag = TTYDEF_OFLAG;

        // Control modes
        tty->m_tio.c_cflag = TTYDEF_CFLAG;

        // Local modes
        tty->m_tio.c_lflag = TTYDEF_LFLAG;

        // Line discipline
        tty->m_tio.c_line = N_TTY;

        // Control characters
        tty->m_tio.c_cc[VINTR]    = CINTR;
        tty->m_tio.c_cc[VQUIT]    = CQUIT;
        tty->m_tio.c_cc[VERASE]   = CERASE;
        tty->m_tio.c_cc[VKILL]    = CKILL;
        tty->m_tio.c_cc[VEOF]     = CEOF;
        tty->m_tio.c_cc[VTIME]    = CTIME;
        tty->m_tio.c_cc[VMIN]     = CMIN;
        tty->m_tio.c_cc[VSWTC]    = 0;
        tty->m_tio.c_cc[VSTART]   = CSTART;
        tty->m_tio.c_cc[VSTOP]    = CSTOP;
        tty->m_tio.c_cc[VSUSP]    = CSUSP;
        tty->m_tio.c_cc[VEOL]     = CEOL;
        tty->m_tio.c_cc[VREPRINT] = CREPRINT;
        tty->m_tio.c_cc[VDISCARD] = CDISCARD;
        tty->m_tio.c_cc[VWERASE]  = CWERASE;
        tty->m_tio.c_cc[VLNEXT]   = CLNEXT;
        tty->m_tio.c_cc[VEOL2]    = 0;

        tty->m_tio.__c_ispeed = TTYDEF_SPEED; // 9600 baud
        tty->m_tio.__c_ospeed = TTYDEF_SPEED; // 9600 baud

        queue_init(&tty->m_ptm_pollrecs);
        queue_init(&tty->m_pts_pollrecs);

        FTOPTY(a_file) = pty;

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t ptmx_read(vfs_file_t a_file, struct uio *a_uio) // Master read
// ********************************************************************************************************************************
{
        pty_t pty = FTOPTY(a_file);
        tty_t tty = &pty->m_tty;

        while (TTY_OUTPUT_EMPTY(tty))
        {
                if (a_file->m_oflags & O_NONBLOCK)
                {
                        return KERN_FAIL(EWOULDBLOCK);
                }

                panic("TODO: Sleep until data is available");
        }

        if (pty->m_flags & PTY_PACKET_MODE)
        {
                char ctrl_byte      = (char)(pty->m_tiockt_flags & 0xFF);
                pty->m_tiockt_flags = TIOCPKT_DATA; // Clears the flags

                kern_return_t retval = uiomove(&ctrl_byte, sizeof(ctrl_byte), a_uio);

                if (KERN_STATUS_FAILURE(retval))
                {
                        return retval;
                }
        }

        for (natural_t rdcnt = TTY_OUTPUT_RD_LEN(tty); (rdcnt > 0) && (a_uio->uio_resid > 0); rdcnt = TTY_OUTPUT_RD_LEN(tty))
        {
                kern_return_t retval = uiomove2(TTY_OUTPUT_RD_PTR(tty), &rdcnt, a_uio);

                if (KERN_STATUS_FAILURE(retval))
                {
                        return retval;
                }

                TTY_OUTPUT_RD_ADVANCE(tty, rdcnt);
        }

        if (TTY_OUTPUT_FULL(tty) == false)
        {
                // Wake up the slave
                poll_wakeup(&tty->m_pts_pollrecs, POLLOUT | POLLWRNORM);
                uthread_wakeup(&tty->m_output);
        }

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static inline void ptmx_echo(tty_t a_tty, char a_char)
// ********************************************************************************************************************************
{
        if (TTY_LOCAL_FLAGS_IS_SET(a_tty, ECHO) == false)
        {
                // If ICANON is also set, echo the NL character even if ECHO is not set.
                if (((a_char == '\n') && TTY_LOCAL_FLAGS_IS_SET(a_tty, ICANON | ECHONL)) == false)
                {
                        return;
                }
        }

        // Input processing
        if ((a_char == '\r') && (TTY_INPUT_FLAGS(a_tty) & IGNCR))
        {
                return;
        }
        else if ((a_char == '\r') && (TTY_INPUT_FLAGS(a_tty) & ICRNL))
        {
                a_char = '\n';
        }
        else if ((a_char == '\n') && (TTY_INPUT_FLAGS(a_tty) & INLCR))
        {
                a_char = '\r';
        }

        // Output processing
        if (a_char == '\n' && (TTY_OUTPUT_FLAGS(a_tty) & ONLCR))
        {
                if (TTY_OUTPUT_FULL(a_tty))
                {
                        TTY_OUTPUT_RD_ADVANCE(a_tty, 1);
                }

                TTY_OUTPUT_PUSH_CHAR(a_tty, '\r');
        }

        // Push on output queue
        if (TTY_OUTPUT_FULL(a_tty))
        {
                TTY_OUTPUT_RD_ADVANCE(a_tty, 1);
        }

        TTY_OUTPUT_PUSH_CHAR(a_tty, a_char);
}

// ********************************************************************************************************************************
static inline void ptmx_echo_ctl(tty_t a_tty, char a_char)
// ********************************************************************************************************************************
{
        if ((TTY_LOCAL_FLAGS(a_tty) & (ECHO | ECHOCTL)) == (ECHO | ECHOCTL))
        {
                ptmx_echo(a_tty, '^');
                ptmx_echo(a_tty, (char)('A' + a_char - 1));
        }
}

// ********************************************************************************************************************************
static natural_t ptmx_write_cb(char const *a_buf, natural_t a_count, void *a_arg) // Master write processing(ISIG set)
// ********************************************************************************************************************************
{
        pty_t pty = FTOPTY((vfs_file_t)(a_arg));
        tty_t tty = &pty->m_tty;
        natural_t i;
        for (i = 0; (i < a_count) && (TTY_INPUT_FULL(tty) == false); i++)
        {
                if (TTY_LOCAL_FLAGS(tty) & ISIG)
                {
                        if (a_buf[i] == TTY_CONTROL_CHAR(tty, VINTR))
                        {
                                /*
                                        debian strace:

                                        write(ptm, "ab\3cde", 6)                  = 6
                                        --- SIGINT {si_signo=SIGINT, si_code=SI_KERNEL} ---
                                        rt_sigreturn({mask=[]})                   = 6
                                        read(pts, "cde", 100)                     = 3
                                */

                                utask_group *utask_grp = utask_group_find(tty->m_fg_pgid);
                                KASSERT(utask_grp != nullptr);
                                utask_group_signal(utask_grp, SIGINT);
                                TTY_INPUT_CLEAR(tty);
                                ptmx_echo_ctl(tty, a_buf[i]);
                                continue;
                        }
                        else if (a_buf[i] == TTY_CONTROL_CHAR(tty, VQUIT))
                        {
                                //  Same as VINTR just send SIGQUIT instead
                                utask_group *utask_grp = utask_group_find(tty->m_fg_pgid);
                                KASSERT(utask_grp != nullptr);
                                utask_group_signal(utask_grp, SIGQUIT);
                                TTY_INPUT_CLEAR(tty);
                                ptmx_echo_ctl(tty, a_buf[i]);
                                continue;
                        }
                        else if (a_buf[i] == TTY_CONTROL_CHAR(tty, VSUSP))
                        {
                                utask_group *utask_grp = utask_group_find(tty->m_fg_pgid);
                                KASSERT(utask_grp != nullptr);
                                utask_group_signal(utask_grp, SIGTSTP);
                                TTY_INPUT_CLEAR(tty);
                                ptmx_echo_ctl(tty, a_buf[i]);
                                continue;
                        }
                }

                ptmx_echo(tty, a_buf[i]);

                TTY_INPUT_PUSH_CHAR(tty, a_buf[i]);
        }
        return i;
}

// ********************************************************************************************************************************
static kern_return_t ptmx_write(vfs_file_t a_file, struct uio *a_uio) // Master write
// ********************************************************************************************************************************
{
        pty_t pty = FTOPTY(a_file);
        tty_t tty = &pty->m_tty;

        while (TTY_INPUT_FULL(tty))
        {
                if (a_file->m_oflags & O_NONBLOCK)
                {
                        return KERN_FAIL(EWOULDBLOCK);
                }

                panic("TODO: Slave is not reading data fast enough. Sleep until data can be written.");
        }

        kern_return_t retval = KERN_SUCCESS;

        if (TTY_LOCAL_FLAGS(tty) & (ISIG | ICANON | ECHO))
        {
                // The master must check for signals on writes, and raise if necessary
                retval = uiomove3(a_uio, ptmx_write_cb, a_file);
        }
        else
        {
                // We are not checking for signals, just copy to input buffer.
                for (natural_t wrcnt = TTY_INPUT_WR_LEN(tty); (wrcnt > 0) && (a_uio->uio_resid > 0); wrcnt = TTY_INPUT_WR_LEN(tty))
                {
                        char *wrbuf = TTY_INPUT_WR_PTR(tty);

                        retval = uiomove2(wrbuf, &wrcnt, a_uio);

                        if (KERN_STATUS_FAILURE(retval))
                        {
                                break;
                        }

                        TTY_INPUT_WR_ADVANCE(tty, wrcnt);
                }
        }

        if (TTY_INPUT_EMPTY(tty) == false)
        {
                // Wake up the slave
                poll_wakeup(&tty->m_pts_pollrecs, POLLIN | POLLRDNORM);
                uthread_wakeup(&tty->m_input);
        }

        return retval;
}

// ********************************************************************************************************************************
static kern_return_t ptmx_ioctl(vfs_file_t a_file, ioctl_req_t a_cmd) // Master ioctl
// ********************************************************************************************************************************
{
        pty_t pty = FTOPTY(a_file);
        tty_t tty = &pty->m_tty;

        integer_t cmdno = ioctl_req_cmdno(a_cmd);

        kern_return_t retval = KERN_SUCCESS;

        switch (cmdno)
        {
                case TCGETS:
                {
                        // Get the current serial port settings
                        retval = ioctl_req_write(a_cmd, &tty->m_tio, sizeof(tty->m_tio));
                }
                break;
                case TCSETS:
                {
                        // Set the current serial port settings (TCSANOW)
                        retval = ioctl_req_read(a_cmd, &tty->m_tio, sizeof(tty->m_tio));
                }
                break;
                case TIOCPKT:
                {
                        int pkt_on = !!(ioctl_req_arg(a_cmd));

                        if (pkt_on != 0)
                        {
                                pty->m_flags |= PTY_PACKET_MODE;
                        }
                        else
                        {
                                pty->m_flags &= ~PTY_PACKET_MODE;
                        }
                }
                break;
                case FIONBIO: // Activate non blocking io
                {
#if is_already_set_this_call_is_just_to_inform_the_device_about_it
                        int dontblock = *((int *)a_data);

                        if (dontblock != 0)
                        {
                                a_file->m_oflags |= O_NONBLOCK;
                        }
                        else
                        {
                                a_file->m_oflags &= ~O_NONBLOCK;
                        }
#endif
                }
                break;
                case TIOCSWINSZ:
                {
                        // Master changing the window size, notify the foreground process group, anyone else need to be notified?
                        struct winsize ws;

                        retval = ioctl_req_read(a_cmd, &ws, sizeof(ws));

                        if (KERN_STATUS_SUCCESS(retval)
                            && ((tty->m_ws.ws_row != ws.ws_row) ||       //
                                (tty->m_ws.ws_col != ws.ws_col) ||       //
                                (tty->m_ws.ws_xpixel != ws.ws_xpixel) || //
                                (tty->m_ws.ws_ypixel != ws.ws_ypixel)))  //
                        {
                                tty->m_ws              = ws;
                                utask_group *utask_grp = utask_group_find(tty->m_fg_pgid);
                                if (utask_grp == nullptr)
                                {
                                        // KASSERT(utask_grp != nullptr);
                                        asm("nop");
                                }
                                else
                                {
                                        utask_group_signal(utask_grp, SIGWINCH);
                                }
                        }
                }
                break;
                case TIOCSPTLCK:
                {
                        boolean_t ls = !!(ioctl_req_arg(a_cmd));
                        if (ls == true)
                        {
                                log_debug("Lock slave");
                        }
                        else
                        {
                                log_debug("Unlock slave");
                        }
                        break;
                }
                case TIOCGPTN:
                {
                        int slave_minor = minor(pty->m_slavedev.m_beg);

                        retval = ioctl_req_write(a_cmd, &slave_minor, sizeof(slave_minor));
                        break;
                }
                default:
                        panic("UNHANDLED IOCTL %p", cmdno);
        }

        return retval;
}

// ********************************************************************************************************************************
static kern_return_t ptmx_close(vfs_file_t a_file) // Master close
// ********************************************************************************************************************************
{
        pty_t pty = FTOPTY(a_file);
        // The master has can only have one file open, when it's closed the pty is dead.

        pty->m_flags &= ~PTY_MASTER_OPEN;

        if (pty->m_slave_refs)
        {
                // The last slave must cleanup
                panic("TODO: PTY master closed. Linux sends a SIGHUP to the session leader when the master closes, examine "
                      "this "
                      "further.");
                return KERN_SUCCESS;
        }

        // No slaves, cleanup.
        driver_chrdev_free(&pty->m_slavedev);
        s_pty_zone.free(pty);
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t ptmx_poll(vfs_file_t a_file, struct pollfd **a_pfd) // Master poll
// ********************************************************************************************************************************
{
        pty_t pty = FTOPTY(a_file);
        tty_t tty = &pty->m_tty;

        if ((*a_pfd)->events & (POLLIN | POLLRDNORM))
        {
                if (TTY_OUTPUT_EMPTY(tty) == false)
                {
                        (*a_pfd)->revents |= (*a_pfd)->events & (short)(POLLIN | POLLRDNORM);
                }
        }

        if ((*a_pfd)->events & (POLLHUP))
        {
                if ((pty->m_flags & PTY_SLAVE_OPEN) == 0)
                {
                        (*a_pfd)->revents |= (short)(POLLHUP);
                }
        }
        else if ((*a_pfd)->events & (POLLOUT | POLLWRNORM))
        {
                if (TTY_INPUT_FULL(tty) == false)
                {
                        (*a_pfd)->revents |= (*a_pfd)->events & (short)(POLLOUT | POLLWRNORM);
                }
        }

        return poll_record(&tty->m_ptm_pollrecs, a_pfd);
}

static struct vfs_file_operations_data s_ptmxdev_ops = {
        .fopen  = ptmx_open,  //
        .fread  = ptmx_read,  //
        .fwrite = ptmx_write, //
        .flseek = pty_lseek,  //
        .fioctl = ptmx_ioctl, //
        .fclose = ptmx_close, //
        .fpoll  = ptmx_poll   //
};

// ********************************************************************************************************************************
static kern_return_t pt_init()
// ********************************************************************************************************************************
{
        kern_return_t retval = driver_chrdev_alloc(&s_ptmxdev, "ptmx", &s_ptmxdev_ops, makedev(18, 0), 1);

        return retval;
}

device_initcall(pt_init);
