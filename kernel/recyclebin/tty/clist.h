#pragma once
#include <mach/param.h>

struct cblock {
  struct cblock *c_next; /* next cblock in queue */
  char c_quote[CBQSIZE]; /* quoted characters */
  char c_info[CBSIZE];   /* characters */
};

#ifdef __cplusplus
extern "C" {
#endif  // __cplusplus

/*
 * Put a character into the output queue.
 */
int clist_putc(int c, struct clist *clp);

#ifdef __cplusplus
}
extern struct cblock *cfree, *cfreelist;
#endif  // __cplusplus
