/*
 * Mach Operating System
 * Copyright (c) 1992,1991,1990,1989,1988 Carnegie Mellon University
 * All Rights Reserved.
 *
 * Permission to use, copy, modify and distribute this software and its
 * documentation is hereby granted, provided that both the copyright
 * notice and this permission notice appear in all copies of the
 * software, derivative works or modified versions, and any portions
 * thereof, and that both notices appear in supporting documentation.
 *
 * CARNEGIE MELLON ALLOWS FREE USE OF THIS SOFTWARE IN ITS "AS IS"
 * CONDITION.  CARNEGIE MELLON DISCLAIMS ANY LIABILITY OF ANY KIND FOR
 * ANY DAMAGES WHATSOEVER RESULTING FROM THE USE OF THIS SOFTWARE.
 *
 * Carnegie Mellon requests users of this software to return to
 *
 *  Software Distribution Coordinator  or  Software.Distribution@CS.CMU.EDU
 *  School of Computer Science
 *  Carnegie Mellon University
 *  Pittsburgh PA 15213-3890
 *
 * any improvements or extensions that they make and grant Carnegie Mellon
 * the rights to redistribute these changes.
 */
/*
 *	File:	mach/mach_types.h
 *	Author:	Avadis Tevanian, Jr., Michael Wayne Young
 *	Date:	1986
 *
 *	Mach external interface definitions.
 *
 */

#ifndef _MACH_MACH_TYPES_H_
#define _MACH_MACH_TYPES_H_

#include <kernel/machine.h>
#include <kernel/processor.h> /* for processor_array_t*/
#include <kernel/processor_info.h>
#include <kernel/task_info.h>
#include <kernel/time_value.h>
#include <kernel/vm/vm_attributes.h>
#include <kernel/vm/vm_inherit.h>
#include <kernel/vm/vm_prot.h>
#include <kernel/vm/vm_statistics.h>
#include <mach/host_info.h>
#include <mach/machine/vm_types.h>
#include <mach/thread_info.h>
#include <mach/thread_status.h>
/* for thread_array_t processor_set_array_t, processor_set_name_array_t */
// #include <mach/syscall_emulation.h>
/* for emulation_vector_t */

/*
 *	Backwards compatibility, for those programs written
 *	before mach/{std,mach}_types.{defs,h} were set up.
 */
#include <kernel/std_types.h>

#endif /* _MACH_MACH_TYPES_H_ */
