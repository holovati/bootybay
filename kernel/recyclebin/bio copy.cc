#include <kernel/vm/vm_kern.h>
#include <kernel/vm/vm_map.h>
#include <kernel/vm/vm_meter.h>
#include <kernel/vm/vm_page.h>
#include <kernel/zalloc.h>
#include <mach/buf.h>
#include <mach/machine/spl.h>
#include <mach/thread.h>
#include <strings.h>
#include <vfs/vnode.h>

//  Map where buffer pages are mapped
static vm_map_t s_buffer_io_map;

#define IO_HEADER_PAGE_WANTED BIT(0)

struct io_header
{
        // Must be first
        buffer_cache::io_descriptor m_descriptors[PAGE_SIZE / DEV_BSIZE];

        vm_page_t m_vm_page;

        vm_address_t m_kernel_address;

        vm_map_entry m_map_entry;

        u64 m_descriptor_count : 4;
        u64 m_outstanding_io : 5;
        u64 m_flags : 4;

        u64 m_pad : 51;

        using link = intrusive::tailq::link<io_header>;
        link m_link;
        using list = link::head<&io_header::m_link>;

        static io_header *get(buffer_cache::io_descriptor *a_io_desc)
        {
                return reinterpret_cast<io_header *>(a_io_desc - a_io_desc->m_io_header_idx);
        }
};

ZONE_DEFINE(io_header, s_io_header_zone, 0x00, (PAGE_SIZE / sizeof(io_header)), ZONE_EXHAUSTIBLE, 0x10);

#define IO_HEADER_LIST_FREE  0
#define IO_HEADER_LIST_USED  1
#define IO_HEADER_LIST_COUNT 2

static io_header::list s_io_header_lists[IO_HEADER_LIST_COUNT];

buffer_cache::block_address_log_t buffer_cache::io_descriptor::get_logical_block_address()
{
        io_header *ioh = io_header::get(this);
        return ioh->m_vm_page->offset + (((PAGE_SIZE / (ioh->m_descriptor_count)) * m_io_header_idx));
}

vm_page_t buffer_cache::io_descriptor::get_vm_page()
{
        io_header *ioh = io_header::get(this);
        return ioh->m_vm_page;
}

vm_address_t buffer_cache::io_descriptor::get_phys_address()
{
        io_header *ioh = io_header::get(this);
        return (ioh->m_vm_page->phys_addr + ((PAGE_SIZE / (ioh->m_descriptor_count)) * m_io_header_idx));
}

size_t buffer_cache::io_descriptor::get_block_size()
{
        io_header *ioh = io_header::get(this);
        return (PAGE_SIZE / (ioh->m_descriptor_count));
}

void buffer_cache::io_descriptor::clear_data()
{
        char *buffer;
        size_t buffer_size;

        get_data(&buffer, 0, &buffer_size);

        bzero(buffer, buffer_size);
}

void buffer_cache::io_descriptor::get_data(void **a_buf_out, size_t a_buf_size, size_t a_block_offset, size_t *a_block_residue)
{
        io_header *ioh = io_header::get(this);

        size_t block_size = (PAGE_SIZE / (ioh->m_descriptor_count));

        KASSERT((a_block_offset + a_buf_size) <= block_size);

        *a_buf_out = reinterpret_cast<void *>((ioh->m_kernel_address + (block_size * m_io_header_idx)) + a_block_offset);

        if (a_block_residue)
        {
                *a_block_residue = block_size - a_block_offset;
        }
}

static FORCEINLINE void buffer_cache_wait(event_t a_event)
{
        assert_wait(a_event, false);
        thread_block(nullptr);
}

static FORCEINLINE void buffer_cache_wakeup(event_t a_event) { thread_wakeup(a_event); }

void buffer_cache::io_descriptor::io_done(kern_return_t a_result)
{
        io_header *ioh = io_header::get(this);

        spl_t pl = splbio();
        m_flags |= IO_DESCRIPTOR_IODONE;

        if (a_result != KERN_SUCCESS)
        {
                m_flags |= IO_DESCRIPTOR_IOERROR;
        }

        // if ((bp->b_flags & B_READ) == 0)
        //{
        //        vwakeup(bp);
        //}

        // If we just finished writing the buffer is not dirty anymore
        if ((m_flags & (IO_DESCRIPTOR_READ | IO_DESCRIPTOR_IOERROR)) == 0)
        {
                m_flags &= ~IO_DESCRIPTOR_DIRTY;
        }

        /*
         * For asynchronous completions, release the buffer now. The brelse
         *	checks for B_WANTED and will do the wakeup there if necessary -
         *	so no need to do a wakeup here in the async case.
         */

        if ((m_flags & IO_DESCRIPTOR_ASYNC))
        {
                buffer_cache::release(this);
        }
        else
        {
                buffer_cache_wakeup(this);

                if ((m_flags & IO_DESCRIPTOR_READ) == 0)
                {
                        buffer_cache::release(this);
                }
                else
                {
                        m_flags &= ~(IO_DESCRIPTOR_WANTED);
                }
        }

        --(ioh->m_outstanding_io);

        KASSERT((ioh->m_outstanding_io) <= ioh->m_descriptor_count);

        if (ioh->m_outstanding_io == 0)
        {
                if (ioh->m_flags & IO_HEADER_PAGE_WANTED)
                {
                        ioh->m_flags &= ~IO_HEADER_PAGE_WANTED;
                        buffer_cache_wakeup(ioh);
                }
        }

        splx(pl);
}

/////////////////////////////////////////////
/////////////////////////////////////////////
/////////////////////////////////////////////
static io_header *io_header_lookup(vnode *a_vnode, vm_offset_t a_logical_offset)
{
        KASSERT(a_vnode->islocked());
        // KASSERT(a_vnode->is_memory_mappable());
        KASSERT((a_logical_offset & PAGE_MASK) == 0);

        spl_t pl = splbio();

        io_header *ioh = nullptr;

        // Check if the block is already in memory
        vm_page_t vmpg = vm_page_map_lookup(&a_vnode->m_page_map, a_logical_offset);
        if (vmpg != nullptr)
        {
                KASSERT(vmpg->m_io_header != nullptr);

                ioh = vmpg->m_io_header;
        }

        splx(pl);

        return ioh;
}

static kern_return_t io_descriptor_wait(buffer_cache::io_descriptor *a_io_desc)
{
        spl_t pl = splbio();

        kern_return_t result = KERN_SUCCESS;

        if ((a_io_desc->m_flags & IO_DESCRIPTOR_ASYNC) == 0)
        {

                while ((a_io_desc->m_flags & IO_DESCRIPTOR_IODONE) == 0)
                {
                        // result = pthread_wait_event(a_io_desc, false);
                        buffer_cache_wait(a_io_desc);
                }

                if (a_io_desc->m_flags & IO_DESCRIPTOR_IOERROR)
                {
                        panic("Reset the descriptor to UNTOUCHED state and fail");
                        // if ((bp->b_flags & B_INVAL) == 0)
                        //{
                        //        bp->b_flags |= B_INVAL;
                        //        bp->b_dev = NODEV;
                        //        LIST_REMOVE(bp, b_hash);
                        //        LIST_INSERT_HEAD(&invalhash, bp, b_hash);
                        //}

                        result = KERN_FAIL(EIO);
                }
        }

        splx(pl);

        return result;
}
/////////////////////////////////////////////
/////////////////////////////////////////////
/////////////////////////////////////////////

buffer_cache::io_descriptor *buffer_cache::get(vnode *a_vnode, vm_offset_t a_logical_offset, size_t a_block_size)
{

        KASSERT(a_vnode->islocked());
        // KASSERT(a_vnode->is_memory_mappable());
        KASSERT((a_logical_offset & DEV_BMASK) == 0);
        KASSERT((a_block_size & DEV_BMASK) == 0);
        bool done = false;

        io_header *ioh;
        buffer_cache::io_descriptor *io_desc;

        vm_offset_t vnode_offset = a_logical_offset & ~PAGE_MASK;
        vm_offset_t page_offset  = a_logical_offset & PAGE_MASK;

        spl_t pl = splbio();
        do
        {
                if (ioh = io_header_lookup(a_vnode, vnode_offset); ioh != nullptr)
                {
                        io_desc = &(ioh->m_descriptors[page_offset / a_block_size]);

                        if (io_desc->m_flags & IO_DESCRIPTOR_BUSY)
                        {
                                io_desc->m_flags |= IO_DESCRIPTOR_WANTED;
                                buffer_cache_wait(io_desc);
                                continue;
                        }

                        // TODO(Damir): Remove it from the freelist

                        // Check for offset/size differences
                        if (io_desc->get_block_size() != a_block_size)
                        {
                                panic("Blocksize mismatch");
                        }

                        s_io_header_lists[IO_HEADER_LIST_USED].insert_head(s_io_header_lists[IO_HEADER_LIST_USED].remove(ioh));

                        done = true;
                        continue;
                }
                else
                {
                        if (s_io_header_lists[IO_HEADER_LIST_FREE].empty() == false)
                        {
                                ioh = s_io_header_lists[IO_HEADER_LIST_FREE].remove_back();

                                KASSERT(ioh->m_outstanding_io == 0);

                                vm_page_map_insert(&a_vnode->m_page_map, ioh->m_vm_page, vnode_offset);

                                KASSERT(ioh->m_map_entry.object.vm_object == nullptr);

                                ioh->m_map_entry.object.vm_object = a_vnode;
                        }
                        else if (ioh = s_io_header_zone.alloc(); ioh != nullptr)
                        {

                                ioh->m_outstanding_io = 0;
                                ioh->m_flags          = 0;
                                ioh->m_kernel_address = 0;

                                // Find space for the new page(The cluster should have it's own vm_entry_t)
                                if (vm_map_find(s_buffer_io_map,
                                                a_vnode,
                                                vnode_offset,
                                                &ioh->m_kernel_address,
                                                PAGE_SIZE,
                                                true,
                                                &(ioh->m_map_entry))
                                    != KERN_SUCCESS)
                                {
                                        panic("We should do more here, cleanup?");
                                        return nullptr;
                                }

                                // Allocate a page for the header and tie them together.
                                ioh->m_vm_page              = vm_page_alloc(&a_vnode->m_page_map, vnode_offset);
                                ioh->m_vm_page->m_io_header = ioh;
                        }
                        else
                        {
                                vnode *owner_vnode = static_cast<vnode *>(
                                    s_io_header_lists[IO_HEADER_LIST_USED].back()->m_map_entry.object.vm_object);
                                owner_vnode->reference();
                                owner_vnode->lock();
                                buffer_cache::page_out(owner_vnode);
                                owner_vnode->unlock();
                                owner_vnode->unreference();

                                continue;
                        }

                        for (ioh->m_descriptor_count = 0;                          //
                             ioh->m_descriptor_count < (PAGE_SIZE / a_block_size); //
                             ++(ioh->m_descriptor_count))
                        {
                                io_desc = &(ioh->m_descriptors[ioh->m_descriptor_count]);

                                io_desc->m_phys_block_address = vnode_offset + (ioh->m_descriptor_count * a_block_size);
                                io_desc->m_io_header_idx      = ioh->m_descriptor_count;
                                io_desc->m_flags              = 0;
                                io_desc->m_fs_flags           = 0;
                                io_desc->m_lba_offset         = 0;
                        }

                        pmap_enter(kernel_pmap, ioh->m_kernel_address, ioh->m_vm_page, VM_PROT_DEFAULT, true);

                        // ioh->m_vm_page->activate();
                        ioh->m_vm_page->wire();

                        // Get the right io_descriptor
                        io_desc = &(ioh->m_descriptors[page_offset / a_block_size]);

                        s_io_header_lists[IO_HEADER_LIST_USED].insert_head(ioh);

                        done = true;
                        continue;
                }

        } while (done == false);

        io_desc->m_flags |= IO_DESCRIPTOR_BUSY;

        splx(pl);

        return io_desc;
}

kern_return_t buffer_cache::read(vnode *a_vnode,
                                 vm_offset_t a_logical_offset,
                                 size_t a_block_size,
                                 buffer_cache::io_descriptor **a_io_desc)
{
        KASSERT(a_vnode->islocked());
        // KASSERT(a_vnode->is_memory_mappable());
        KASSERT((a_logical_offset & DEV_BMASK) == 0);
        KASSERT((a_block_size & DEV_BMASK) == 0);

        kern_return_t result = KERN_SUCCESS;

        spl_t pl = splbio();

        buffer_cache::io_descriptor *io_desc = buffer_cache::get(a_vnode, a_logical_offset, a_block_size);

        KASSERT(io_desc);

        if ((io_desc->m_flags & IO_DESCRIPTOR_ACCESSED) == 0)
        {
                io_desc->m_flags |= IO_DESCRIPTOR_READ;

                io_desc->m_flags &= ~(IO_DESCRIPTOR_IODONE);

                (io_header::get(io_desc)->m_outstanding_io)++;

                if (result = a_vnode->strategy(io_desc); result != KERN_SUCCESS)
                {
                        panic("Handle this");
                }
                else
                {
                        result = io_descriptor_wait(io_desc);

                        KASSERT(result == KERN_SUCCESS);
                }
        }

        *a_io_desc = io_desc;

        splx(pl);

        return result;
}

kern_return_t buffer_cache::read_page(
    vnode *a_vnode, vm_offset_t a_page_offset, size_t a_block_size, size_t a_read_size, io_descriptor **a_first_io_desc)
{
        KASSERT(a_vnode->islocked());
        // KASSERT(a_vnode->is_memory_mappable());
        KASSERT((a_page_offset & PAGE_MASK) == 0);
        KASSERT((a_block_size & DEV_BMASK) == 0);
        KASSERT((a_read_size >= a_block_size)); // Don't use this if reading a single fs block
        kern_return_t result = KERN_SUCCESS;

        spl_t pl = splbio();
        if (result = buffer_cache::read(a_vnode, a_page_offset, a_block_size, a_first_io_desc); result == KERN_SUCCESS)
        {
                io_header *ioh = io_header::get(*a_first_io_desc);

                for (vm_offset_t page_offset = a_page_offset + a_block_size; //
                     page_offset < (a_page_offset + a_read_size);            //
                     page_offset += a_block_size)                            //
                {
                        buffer_cache::io_descriptor *io_desc = buffer_cache::get(a_vnode, page_offset, a_block_size);

                        if ((io_desc->m_flags & IO_DESCRIPTOR_ACCESSED) == 0)
                        {
                                io_desc->m_flags |= IO_DESCRIPTOR_READ | IO_DESCRIPTOR_ASYNC;

                                io_desc->m_flags &= ~(IO_DESCRIPTOR_IODONE);

                                (ioh->m_outstanding_io)++;

                                if (result = a_vnode->strategy(io_desc); result != KERN_SUCCESS)
                                {
                                        panic("Handle this");
                                }
                        }
                        else
                        {
                                buffer_cache::release(io_desc);
                        }
                }

                if (ioh->m_outstanding_io)
                {
                        ioh->m_flags |= IO_HEADER_PAGE_WANTED;

                        while ((ioh->m_flags & IO_HEADER_PAGE_WANTED))
                        {
                                buffer_cache_wait(ioh);
                        }
                }
        }

        KASSERT((*a_first_io_desc)->m_flags & IO_DESCRIPTOR_BUSY);

        splx(pl);
        return result;
}

kern_return_t buffer_cache::write(io_descriptor *a_io_desc, bool a_delay)
{
        spl_t pl = splbio();
        KASSERT(a_io_desc->m_flags & IO_DESCRIPTOR_BUSY);

        // if (bp->b_flags & B_INVAL)
        //{
        //        brelse(bp);
        //        return;
        //}

        kern_return_t result = KERN_SUCCESS;

        a_io_desc->m_flags &= ~IO_DESCRIPTOR_READ;

        if (a_delay)
        {
                a_io_desc->m_flags |= IO_DESCRIPTOR_DIRTY;
                buffer_cache::release(a_io_desc);
        }
        else
        {
                a_io_desc->m_flags &= ~(IO_DESCRIPTOR_IODONE);

                io_header *ioh = io_header::get(a_io_desc);

                (ioh->m_outstanding_io)++;

                vnode *vn = static_cast<vnode *>(ioh->m_map_entry.object.vm_object);

                KASSERT(vn);
                KASSERT(vn->islocked());

                if (result = vn->strategy(a_io_desc); result != KERN_SUCCESS)
                {
                        panic("Handle this");
                }
                else
                {
                        result = io_descriptor_wait(a_io_desc);

                        KASSERT(result == KERN_SUCCESS);
                }
        }

        splx(pl);
        return result;
}

kern_return_t buffer_cache::write_async(buffer_cache::io_descriptor *a_io_desc)
{
        a_io_desc->m_flags |= IO_DESCRIPTOR_ASYNC;
        return buffer_cache::write(a_io_desc, false);
}

void buffer_cache::release(buffer_cache::io_descriptor *a_io_desc)
{

        spl_t pl = splbio();

        KASSERT((a_io_desc->m_flags) & IO_DESCRIPTOR_BUSY);

        io_header *ioh = io_header::get(a_io_desc);

        a_io_desc->m_flags |= IO_DESCRIPTOR_ACCESSED;

        /* anyone need a "free" block? */

        /* anyone need this block? */
        if (a_io_desc->m_flags & IO_DESCRIPTOR_WANTED)
        {
                a_io_desc->m_flags &= ~(IO_DESCRIPTOR_WANTED);
                buffer_cache_wakeup(a_io_desc);
        }

        // if ((bp->b_flags & (B_NOCACHE | B_INVAL | B_ERROR)) || (bp->b_bufsize <= 0))
        //{
        //        bp->b_flags |= B_INVAL;
        //        bp->b_flags &= ~(B_DELWRI | B_CACHE);
        //        if (bp->b_vp)
        //        {
        //                // brelvp(bp);
        //                bp->b_vp->on_buffer_detach(bp);
        //        }
        //}

        /* unlock */
        a_io_desc->m_flags &= ~(IO_DESCRIPTOR_WANTED | IO_DESCRIPTOR_BUSY | IO_DESCRIPTOR_ASYNC /*| B_NOCACHE | B_AGE*/);

        splx(pl);
}

bool buffer_cache::invalidate(vnode *a_vnode, vm_offset_t a_logical_offset, size_t a_block_size)
{
        KASSERT(a_vnode->islocked());
        KASSERT((a_logical_offset & DEV_BMASK) == 0);
        KASSERT((a_block_size & DEV_BMASK) == 0);
        spl_t pl = splbio();

        bool descriptor_invalidated = false;

        if (io_header *ioh = io_header_lookup(a_vnode, a_logical_offset & ~PAGE_MASK); ioh != nullptr)
        {
                // There is a header containing this descriptor, get it and invalidate it.
                io_descriptor *io_desc = buffer_cache::get(a_vnode, a_logical_offset, a_block_size);

                KASSERT(io_desc);

                descriptor_invalidated = (io_desc->m_flags & IO_DESCRIPTOR_ACCESSED) != 0;

                // Reset the descriptor to unused state
                io_desc->m_phys_block_address = a_logical_offset;
                io_desc->m_flags              = 0;
        }

        // Don't release the io descriptor as it will set IO_DESCRIPTOR_ACCESSED flag

        splx(pl);

        return descriptor_invalidated;
}

void buffer_cache::page_out(vnode *a_vnode)
{
        KASSERT(a_vnode->islocked());

        spl_t pl = splbio();

        vm_page_t vmp_next = nullptr;
        for (vm_page_t vmp = vm_page_map_first(&a_vnode->m_page_map); vmp != nullptr; vmp = vmp_next)
        {
                io_header *ioh = vmp->m_io_header;

                // KASSERT(ioh != nullptr);
                if (ioh == nullptr)
                {
                        // We have a hack going on where a character device vnode holds the pages for the frame buffer
                        // We cant page that out. Skip it for now.
                        KASSERT(S_ISCHR(a_vnode->m_fstat.st_mode)); // Just to limit the hack a bit
                        return;
                }

                size_t block_size = ioh->m_descriptors->get_block_size();

                for (u32 i = 0; i < ioh->m_descriptor_count; ++i)
                {
                        io_descriptor *io_desc = buffer_cache::get(a_vnode, vmp->offset + (block_size * i), block_size);

                        if (io_desc->m_flags & IO_DESCRIPTOR_DIRTY)
                        {
                                buffer_cache::write_async(io_desc);
                        }
                }

                if (ioh->m_outstanding_io)
                {
                        ioh->m_flags |= IO_HEADER_PAGE_WANTED;

                        while ((ioh->m_flags & IO_HEADER_PAGE_WANTED))
                        {
                                buffer_cache_wait(ioh);
                        }
                }

                // Disassociate the page from the object
                vmp_next                          = vm_page_map_remove(&a_vnode->m_page_map, vmp);
                ioh->m_map_entry.object.vm_object = nullptr;
                // a_vm_page->deactivate();
                vmp->unwire();
                pmap_change_wiring(kernel_pmap, ioh->m_kernel_address, false);
                pmap_page_protect(vmp->phys_addr, VM_PROT_NONE);

                s_io_header_lists[IO_HEADER_LIST_USED].remove(ioh);

                s_io_header_lists[IO_HEADER_LIST_FREE].insert_head(ioh);
        }

        splx(pl);
}

/*
 * Initialize buffer headers and related structures.
 */
static kern_return_t bufinit()
{
        vm_address_t buf_area_start_address;
        vm_address_t buf_area_end_address;

        if (s_buffer_io_map
            = kmem_suballoc(kernel_map, &buf_area_start_address, &buf_area_end_address, (cnt.v_free_count * PAGE_SIZE), false);
            s_buffer_io_map == nullptr)
        {
                return KERN_FAILURE;
        }

        s_io_header_zone.m_max_elements = cnt.v_free_count / 2;

        for (io_header::list &list : s_io_header_lists)
        {
                list.init();
        }

        return KERN_SUCCESS;
}

//subsys_initcall(bufinit);