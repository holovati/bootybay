#pragma once

typedef struct vnodeops vnodeops_t;

/*
 * This structure defines the information maintained about
 * special devices. It is allocated in checkalias and freed
 * in vgone.
 */
struct specinfo {
  struct vnode **si_hashchain;
  struct vnode *si_specnext;
  natural_t si_flags;
  dev_t si_rdev;
};

/*
 * Flags for specinfo
 */
#define SI_MOUNTEDON 0x0001 /* block special device is mounted on */

/*
 * Special device management
 */
#define SPECHSZ 64
#if ((SPECHSZ & (SPECHSZ - 1)) == 0)
#  define SPECHASH(rdev) (((rdev >> 5) + (rdev)) & (SPECHSZ - 1))
#else
#  define SPECHASH(rdev) (((unsigned)((rdev >> 5) + (rdev))) % SPECHSZ)
#endif

/*
 * Prototypes for special file operations on vnodes.
 */
extern vnodeops_t spec_vnodeop;
