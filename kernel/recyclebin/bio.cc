/*
 * Copyright (c) 1994 John S. Dyson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice immediately at the beginning of the file, without modification,
 *    this list of conditions, and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Absolutely no warranty of function or purpose is made by the author
 *    John S. Dyson.
 * 4. Modifications may be freely made to this file if the above conditions
 *    are met.
 *
 * $Id: vfs_bio.c,v 1.1.1.2 1995/03/23 01:16:39 law Exp $
 */

extern "C" {
#include <errno.h>
#include <mach/conf.h>
#include <mach/machine/spl.h>
#include <mach/queue.h>
#include <mach/specdev.h>
#include <mach/thread.h>
#include <mach/vm/pmap.h>
#include <mach/vm_param.h>
#include <strings.h>
}
#include "vm/vm_meter.h"
#define REMOVE_SOON
#include <mach/buf.h>
#include <mach/init.h>
#include <mach/mount.h>
#include <mach/proc.h>
#include <mach/ucred.h>
#include <mach/vm/vm_kern.h>
#include <mach/vm/vm_map.h>
#include <mach/vm/vm_page.h>
#include <mach/vnode.h>

/*
 * Copyright (c) 1982, 1986, 1989, 1993
 *	The Regents of the University of California.  All rights reserved.
 * (c) UNIX System Laboratories, Inc.
 * All or some portions of this file are derived from material licensed
 * to the University of California by American Telephone and Telegraph
 * Co. or Unix System Laboratories, Inc. and are reproduced herein with
 * the permission of UNIX System Laboratories, Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)buf.h	8.7 (Berkeley) 1/21/94
 * $Id: buf.h,v 1.1.1.2 1995/03/23 01:15:54 law Exp $
 */

#include <mach/param.h>
struct buf;
typedef struct buf buf_t, *bufp_t;
struct vnode;
struct ucred;

/*
 * These flags are kept in b_flags.
 */
#define B_AGE         0x00000001 /* Move to age queue when I/O done. */
#define B_APPENDWRITE 0x00000002 /* Append-write in progress. */
#define B_ASYNC       0x00000004 /* Start I/O, do not wait. */
#define B_BAD         0x00000008 /* Bad block revectoring in progress. */
#define B_BUSY        0x00000010 /* I/O in progress. */
#define B_CACHE       0x00000020 /* Bread found us in the cache. */
#define B_CALL        0x00000040 /* Call b_iodone from biodone. */
#define B_DELWRI      0x00000080 /* Delay I/O until buffer reused. */
#define B_DIRTY       0x00000100 /* Dirty page to be pushed out async. */
#define B_DONE        0x00000200 /* I/O completed. */
#define B_EINTR       0x00000400 /* I/O was interrupted */
#define B_ERROR       0x00000800 /* I/O error occurred. */
#define B_GATHERED    0x00001000 /* LFS: already in a segment. */
#define B_INVAL       0x00002000 /* Does not contain valid info. */
#define B_LOCKED      0x00004000 /* Locked in core (not reusable). */
#define B_NOCACHE     0x00008000 /* Do not cache block after use. */
#define B_PAGET       0x00010000 /* Page in/out of page table space. */
#define B_PGIN        0x00020000 /* Pagein op, so swap() can count it. */
#define B_PHYS        0x00040000 /* I/O to user memory. */
#define B_RAW         0x00080000 /* Set by physio for raw transfers. */
#define B_READ        0x00100000 /* Read buffer. */
#define B_TAPE        0x00200000 /* Magnetic tape I/O. */
#define B_UAREA       0x00400000 /* Buffer describes Uarea I/O. */
#define B_WANTED      0x00800000 /* Process wants this buffer. */
#define B_WRITE       0x00000000 /* Write buffer (pseudo flag). */
#define B_WRITEINPROG 0x01000000 /* Write in progress. */
#define B_XXX         0x02000000 /* Debugging flag. */
#define B_VMIO        0x20000000 /* VMIO flag */
#define B_CLUSTER     0x40000000 /* pagein op, so swap() can count it */
#define B_BOUNCE      0x80000000 /* bounce buffer flag */

#ifdef __cplusplus
#include <mach/queue.h>

#include <mach/intrusive/tailq.hh>

#define NOLIST ((struct buf *)0x87654321)

typedef void (*buf_iodonefp_t)(bufp_t);

/*
 * The buffer header describes an I/O operation in the kernel.
 */
struct buf {
  using list_link = intrusive::list::link<buf>;

  list_link b_vnbufs; /* Buffer's associated vnode. */
  using vnbufs_list = list_link::head<&buf::b_vnbufs>;

  LIST_ENTRY(buf) b_hash;       /* Hash chain. */
  TAILQ_ENTRY(buf) b_freelist;  /* Free list position if not active. */
  struct buf *b_actf, **b_actb; /* Device driver queue when active. */
  struct proc *b_proc;          /* Associated proc; NULL if kernel. */
  i64 volatile b_flags;         /* B_* flags. */
  size_t b_qindex;              /* buffer queue index */
  size_t b_bufsize;             /* Allocated buffer size. */
  size_t b_bcount;              /* Valid bytes in buffer. */
  size_t b_resid;               /* Remaining I/O. */
  dev_t b_dev;                  /* Device associated with buffer. */
  void *b_addr;                 /* Memory, superblocks, indirect etc. */
  // void *b_saveaddr;             /* Original b_addr for physio. */
  size_t b_lblkno;         /* Logical block number. */
  size_t b_blkno;          /* Underlying physical block number. */
  buf_iodonefp_t b_iodone; /* Function to call upon completion. */
  struct vnode *b_vp;      /* Device vnode. */
  // i32 b_pfcent;            /* Center page when swapping cluster. */
  i32 b_error, pad; /* Errno value. */
  // size_t b_dirtyoff;            /* Offset in buffer of dirty region. */
  // size_t b_dirtyend;            /* Offset of end of dirty region. */
  struct ucred *b_rcred; /* Read credentials reference. */
  struct ucred *b_wcred; /* Write credentials reference. */
  // int b_validoff;        /* Offset in buffer of valid region. */
  // int b_validend;        /* Offset of end of valid region. */
  size_t b_pblkno; /* physical block number */
  void *b_savekva; /* saved kva for transfer while bouncing */
  void *b_driver1; /* for private use by the driver */
  void *b_driver2; /* for private use by the driver */
  // void *b_spc;

  vm_page_t m_page;                 // Page containing the data
  vm_address_t m_page_map_address;  // Page containing the data

  // vm_page_t b_pages[(MAXBSIZE + PAGE_SIZE - 1) / PAGE_SIZE];
  // size_t b_npages;
};

/* Device driver compatibility definitions. */
#if 0
#define b_active b_bcount /* Driver queue head: drive active. */
#define b_data   b_un.b_addr /* b_un.b_addr is not changeable. */
#define b_errcnt b_resid /* Retry count while I/O in progress. */
#define iodone   biodone /* Old name for biodone. */
#define iowait   biowait /* Old name for biowait. */
#endif

/*
 * This structure describes a clustered I/O.  It is stored in the b_saveaddr
 * field of the buffer on which I/O is done.  At I/O completion, cluster
 * callback uses the structure to parcel I/O's to individual buffers, and
 * then free's this structure.
 */
struct cluster_save {
  size_t bs_bcount;         /* Saved b_bcount. */
  size_t bs_bufsize;        /* Saved b_bufsize. */
  void *bs_saveaddr;        /* Saved b_addr. */
  size_t bs_nchildren;      /* Number of associated buffers. */
  struct buf **bs_children; /* List of associated buffers. */
};

extern "C" {
#ifdef REMOVE_SOON
void bremfree(struct buf *);
int bread(struct vnode *, size_t, int, struct ucred *, struct buf **);
int breadn(struct vnode *,
           size_t,
           int,
           size_t *,
           int *,
           int,
           struct ucred *,
           struct buf **);
int bwrite(struct buf *);
void bdwrite(struct buf *);
void bawrite(struct buf *);
void brelse(struct buf *);
struct buf *getnewbuf(int slpflag, int slptimeo);
struct buf *getpbuf(void);
struct buf *incore(struct vnode *, size_t);
struct buf *getblk(struct vnode *, size_t, int, int, int);
struct buf *geteblk(int);
void allocbuf(struct buf *, int);
int biowait(struct buf *);
void biodone(struct buf *);
void clrbuf(bufp_t bp);
void cluster_callback(struct buf *);
int cluster_read(
    struct vnode *, size_t, size_t, long, struct ucred *, struct buf **);
void cluster_write(struct buf *, size_t);
u32 minphys(struct buf *);
void vwakeup(struct buf *);
void vmapbuf(struct buf *);
void vunmapbuf(struct buf *);
void relpbuf(struct buf *);
// void brelvp(struct buf *);
// void bgetvp(struct vnode *, struct buf *);
void reassignbuf(struct buf *, struct vnode *);
void buf_setdev(struct buf *bp, dev_t dev);
dev_t buf_getdev(struct buf *bp);
int buf_get_flags(struct buf *bp);
void buf_setblkno(struct buf *bp, size_t blkno);
size_t buf_getblkno(struct buf *bp);
void buf_setflags(struct buf *bp, int flags);
void buf_set_flags(struct buf *bp, int flags);
void *buf_get_baddr(struct buf *bp);
size_t buf_get_bcount(struct buf *bp);
void buf_set_driver1(struct buf *a_bp, void *p);
void *buf_get_driver1(struct buf *a_bp);
vm_page_t buf_get_vm_page(struct buf *bp);
#endif
#ifdef __cplusplus
}
#endif

/*
 * number of buffer hash entries
 */
#define BUFHSZ 512

/*
 * buffer hash table calculation, originally by David Greenman
 */
#define BUFHASH(vnp, bn)                                                       \
  (&bufhashtbl[(((vm_offset_t)(vnp) / sizeof(struct vnode))                    \
                + (vm_offset_t)(bn))                                           \
               % BUFHSZ])

/*
 * Definitions for the buffer free lists.
 */
#define BUFFER_QUEUES 5 /* number of free buffer queues */

LIST_HEAD(bufhashhdr, buf);
extern struct bufhashhdr bufhashtbl[BUFHSZ];
extern struct bufhashhdr invalhash;

TAILQ_HEAD(bqueues, buf);
extern struct bqueues bufqueues[BUFFER_QUEUES]; /* XXX extern! */

#define QUEUE_NONE   0 /* on no queue */
#define QUEUE_LOCKED 1 /* locked buffers */
#define QUEUE_LRU    2 /* useful buffers */
#define QUEUE_AGE    3 /* less useful buffers */
#define QUEUE_EMPTY  4 /* empty buffer headers*/

/*
 * Zero out the buffer's data area.
 */

/* Flags to low-level allocation routines. */
#define B_CLRBUF 0x01 /* Request allocated buffer be cleared. */
#define B_SYNC   0x02 /* Do all allocations synchronously. */

// extern int nbuf;          /* The number of buffer headers */
extern struct buf *buf;   /* The buffer headers. */
extern char *buffers;     /* The buffer contents. */
extern int bufpages;      /* Number of memory pages in the buffer pool. */
extern struct buf *swbuf; /* Swap I/O buffer headers. */
extern int nswbuf;        /* Number of swap I/O buffer headers. */
extern TAILQ_HEAD(swqueue, buf) bswlist;

#endif  // __cplusplus

struct bufhashhdr bufhashtbl[BUFHSZ];
struct bufhashhdr invalhash;
struct bqueues bufqueues[BUFFER_QUEUES];

struct buf *buf; /* buffer header pool */
int nbuf;        /* number of buffer headers calculated elsewhere */
struct swqueue bswlist;

void vm_hold_free_pages(vm_offset_t from, vm_offset_t to);

void vm_hold_load_pages(vm_offset_t from, vm_offset_t to);

int needsbuffer;

/*
 * Internal update daemon, process 3
 *	The variable vfs_update_wakeup allows for internal syncs.
 */
int vfs_update_wakeup;

// Hack until we vm/bio integration system is complete
static vm_map_entry_t s_blocks_map_entry;

void reassignbuf(struct buf *bp, struct vnode *newvp) {}

/*
 * remove the buffer from the appropriate free list
 */
void bremfree(struct buf *bp) {
  int s = splbio();

  if (bp->b_qindex != QUEUE_NONE) {
    TAILQ_REMOVE(&bufqueues[bp->b_qindex], bp, b_freelist);
    bp->b_qindex = QUEUE_NONE;
  } else {
    panic("bremfree: removing a buffer when not on a queue");
  }

  splx(s);
}

/*
 * Get a buffer with the specified data.  Look in the cache first.
 */
int bread(struct vnode *vp,
          size_t blkno,
          int size,
          struct ucred *cred,
          struct buf **bpp) {
  struct buf *bp;
  struct proc *p = proc::current();
  int s, result = 0;

  bp = getblk(vp, blkno, size, 0, 0);

  *bpp = bp;

  s = splbio(); /* avoid losing events */

  /* if not found in cache, do some I/O */
  if ((bp->b_flags & B_CACHE) == 0) {
    if (p) {
      p->resource_usage(proc::resource::in_block);
    }

    bp->b_flags |= B_READ;

    bp->b_flags &= ~(B_DONE | B_ERROR | B_INVAL);

    if (bp->b_rcred == NOCRED) {
      if (cred != NOCRED) {
        crhold(cred);
      }

      bp->b_rcred = cred;
    }

    // vnode::strategy(bp);

    // bp->b_vp->strategy(bp);

    result = biowait(bp);
  }

  splx(s);

  return result;
}

/*
 * Operates like bread, but also starts asynchronous I/O on
 * read-ahead blocks.
 */
int breadn(struct vnode *vp,
           size_t blkno,
           int size,
           size_t *rablkno,
           int *rabsize,
           int cnt,
           struct ucred *cred,
           struct buf **bpp) {
  struct buf *bp, *rabp;
  int i;
  int rv = 0, readwait = 0;
  struct proc *p = proc::current();

  *bpp = bp = getblk(vp, blkno, size, 0, 0);

  /* if not found in cache, do some I/O */
  if ((bp->b_flags & B_CACHE) == 0) {
    if (p) { /* count block I/O */
      p->resource_usage(proc::resource::in_block);
    }

    bp->b_flags |= B_READ;

    bp->b_flags &= ~(B_DONE | B_ERROR | B_INVAL);

    if (bp->b_rcred == NOCRED) {
      if (cred != NOCRED) {
        crhold(cred);
      }

      bp->b_rcred = cred;
    }
    // vnode::strategy(bp);
    // bp->b_vp->strategy(bp);
    ++readwait;
  }

  for (i = 0; i < cnt; i++, rablkno++, rabsize++) {
    if (incore(vp, *rablkno)) {
      continue;
    }

    rabp = getblk(vp, *rablkno, *rabsize, 0, 0);

    if ((rabp->b_flags & B_CACHE) == 0) {
      if (p) {
        p->resource_usage(proc::resource::in_block);
      }

      rabp->b_flags |= B_READ | B_ASYNC;

      rabp->b_flags &= ~(B_DONE | B_ERROR | B_INVAL);

      if (rabp->b_rcred == NOCRED) {
        if (cred != NOCRED) {
          crhold(cred);
        }

        rabp->b_rcred = cred;
      }
      // vnode::strategy(rabp);
      // rabp->b_vp->strategy(bp);
    } else {
      brelse(rabp);
    }
  }

  if (readwait) {
    rv = biowait(bp);
  }

  return rv;
}

/*
 * Write, release buffer on completion.  (Done by iodone
 * if async.)
 */
int bwrite(struct buf *bp) {
  int oldflags   = bp->b_flags;
  struct proc *p = proc::current();

  if (bp->b_flags & B_INVAL) {
    brelse(bp);
    return (0);
  }

  if (!(bp->b_flags & B_BUSY)) {
    panic("bwrite: buffer is not busy???");
  }

  bp->b_flags &= ~(B_READ | B_DONE | B_ERROR | B_DELWRI);

  bp->b_flags |= B_WRITEINPROG;

  if (oldflags & B_ASYNC) {
    if (oldflags & B_DELWRI) {
      reassignbuf(bp, bp->b_vp);
    } else if (p) {
      p->resource_usage(proc::resource::out_block);
    }
  }

  bp->b_vp->v_numoutput++;
  // vnode::strategy(bp);
  // bp->b_vp->strategy(bp);

  if ((oldflags & B_ASYNC) == 0) {
    int rtval = biowait(bp);
    if (oldflags & B_DELWRI) {
      reassignbuf(bp, bp->b_vp);
    } else if (p) {
      p->resource_usage(proc::resource::out_block);
    }
    brelse(bp);

    return rtval;
  }

  return 0;
}

int vn_bwrite(bufp_t a_bp) { return (bwrite(a_bp)); }

/*
 * Delayed write. (Buffer is marked dirty).
 */
void bdwrite(struct buf *bp) {
  struct proc *p = proc::current();

  if ((bp->b_flags & B_BUSY) == 0) {
    panic("bdwrite: buffer is not busy");
  }

  if (bp->b_flags & B_INVAL) {
    brelse(bp);
    return;
  }

  if (bp->b_flags & B_TAPE) {
    bawrite(bp);
    return;
  }

  bp->b_flags &= ~B_READ;

  if ((bp->b_flags & B_DELWRI) == 0) {
    if (p) {
      p->resource_usage(proc::resource::out_block);
    }

    bp->b_flags |= B_DONE | B_DELWRI;

    reassignbuf(bp, bp->b_vp);
  }

  brelse(bp);

  return;
}

/*
 * Asynchronous write.
 * Start output on a buffer, but do not wait for it to complete.
 * The buffer is released when the output completes.
 */
void bawrite(struct buf *bp) {
  bp->b_flags |= B_ASYNC;
  bwrite(bp);
}

/*
 * Release a buffer.
 */
void brelse(struct buf *bp) {
  int x;

  /* anyone need a "free" block? */
  x = splbio();
  if (needsbuffer) {
    needsbuffer = 0;
    wakeup(&needsbuffer);
  }

  /* anyone need this block? */
  if (bp->b_flags & B_WANTED) {
    bp->b_flags &= ~(B_WANTED | B_AGE);
    wakeup(bp);
  }

  if (bp->b_flags & B_LOCKED) {
    bp->b_flags &= ~B_ERROR;
  }

  if ((bp->b_flags & (B_NOCACHE | B_INVAL | B_ERROR)) || (bp->b_bufsize <= 0)) {
    bp->b_flags |= B_INVAL;
    bp->b_flags &= ~(B_DELWRI | B_CACHE);
    if (bp->b_vp) {
      // brelvp(bp);
      // bp->b_vp->on_buffer_detach(bp);
    }
  }

  if (bp->b_qindex != QUEUE_NONE) {
    panic("brelse: free buffer onto another queue???");
  }

  /* enqueue */
  /* buffers with no memory */
  if (bp->b_bufsize == 0) {
    bp->b_qindex = QUEUE_EMPTY;
    TAILQ_INSERT_HEAD(&bufqueues[QUEUE_EMPTY], bp, b_freelist);
    LIST_REMOVE(bp, b_hash);
    LIST_INSERT_HEAD(&invalhash, bp, b_hash);
    bp->b_dev = NODEV;
    /* buffers with junk contents */
  } else if (bp->b_flags & (B_ERROR | B_INVAL | B_NOCACHE)) {
    bp->b_qindex = QUEUE_AGE;
    TAILQ_INSERT_HEAD(&bufqueues[QUEUE_AGE], bp, b_freelist);
    LIST_REMOVE(bp, b_hash);
    LIST_INSERT_HEAD(&invalhash, bp, b_hash);
    bp->b_dev = NODEV;
    /* buffers that are locked */
  } else if (bp->b_flags & B_LOCKED) {
    bp->b_qindex = QUEUE_LOCKED;
    TAILQ_INSERT_TAIL(&bufqueues[QUEUE_LOCKED], bp, b_freelist);
    /* buffers with stale but valid contents */
  } else if (bp->b_flags & B_AGE) {
    bp->b_qindex = QUEUE_AGE;
    TAILQ_INSERT_TAIL(&bufqueues[QUEUE_AGE], bp, b_freelist);
    /* buffers with valid and quite potentially reuseable contents */
  } else {
    bp->b_qindex = QUEUE_LRU;
    TAILQ_INSERT_TAIL(&bufqueues[QUEUE_LRU], bp, b_freelist);
  }

  /* unlock */
  bp->b_flags &= ~(B_WANTED | B_BUSY | B_ASYNC | B_NOCACHE | B_AGE);

  splx(x);
}

int freebufspace;
int allocbufspace;

/*
 * Find a buffer header which is available for use.
 */
struct buf *getnewbuf(int slpflag, int slptimeo) {
  struct buf *bp;
  int s;
  s = splbio();
start:
  /* can we constitute a new buffer? */
  if ((bp = bufqueues[QUEUE_EMPTY].tqh_first)) {
    if (bp->b_qindex != QUEUE_EMPTY) {
      panic("getnewbuf: inconsistent EMPTY queue");
    }

    bremfree(bp);

    goto fillbuf;
  }

  if ((bp = bufqueues[QUEUE_AGE].tqh_first)) {
    if (bp->b_qindex != QUEUE_AGE) {
      panic("getnewbuf: inconsistent AGE queue");
    }

    bremfree(bp);
  } else if ((bp = bufqueues[QUEUE_LRU].tqh_first)) {
    if (bp->b_qindex != QUEUE_LRU) {
      panic("getnewbuf: inconsistent LRU queue");
      bremfree(bp);
    }
  } else {
    /* wait for a free buffer of any kind */
    needsbuffer = 1;

    tsleep(&needsbuffer, PRIBIO, "newbuf", 0);

    splx(s);

    return 0;
  }

  /* if we are a delayed write, convert to an async write */
  if (bp->b_flags & B_DELWRI) {
    bp->b_flags |= B_BUSY;

    bawrite(bp);

    goto start;
  }

  if (bp->b_vp) {
    // brelvp(bp);
    // bp->b_vp->on_buffer_detach(bp);
  }

  /* we are not free, nor do we contain interesting data */
  if (bp->b_rcred != NOCRED) {
    crfree(bp->b_rcred);
  }

  if (bp->b_wcred != NOCRED) {
    crfree(bp->b_wcred);
  }

fillbuf:
  bp->b_flags = B_BUSY;
  LIST_REMOVE(bp, b_hash);
  LIST_INSERT_HEAD(&invalhash, bp, b_hash);
  splx(s);
  bp->b_dev   = NODEV;
  bp->b_vp    = NULL;
  bp->b_blkno = bp->b_lblkno = 0;
  bp->b_iodone               = 0;
  bp->b_error                = 0;
  bp->b_resid                = 0;
  bp->b_bcount               = 0;
  bp->b_wcred = bp->b_rcred = NOCRED;
  bp->m_page_map_address    = (vm_address_t)bp->b_addr;
  // bp->b_dirtyoff = bp->b_dirtyend = 0;
  // bp->b_validoff = bp->b_validend = 0;
  return bp;
}

/*
 * Check to see if a block is currently memory resident.
 */
struct buf *incore(struct vnode *vp, size_t blkno) {
  struct buf *bp;
  struct bufhashhdr *bh;

  int s = splbio();

  bh = BUFHASH(vp, blkno);
  bp = bh->lh_first;

  /* Search hash chain */
  while (bp) {
    if ((bp < buf) || (bp >= buf + nbuf)) {
      log_debug(
          "incore: buf out of range: %p, hash: %d\n", bp, bh - bufhashtbl);
      panic("incore: buf fault");
    }

    /* hit */
    if (bp->b_lblkno == blkno && bp->b_vp == vp
        && (bp->b_flags & B_INVAL) == 0) {
      splx(s);
      return bp;
    }

    bp = bp->b_hash.le_next;
  }

  splx(s);

  return 0;
}

/*
 * Get a block given a specified block and offset into a file/device.
 */
struct buf *getblk(
    struct vnode *vp, size_t blkno, int size, int slpflag, int slptimeo) {
  struct buf *bp;
  int s;
  struct bufhashhdr *bh;

  // The byte offset of blkno
  size_t byte_offset = blkno << vp->m_block_shift;
  // The block number of PAGE_SIZE'd blocks
  size_t sys_blkno = byte_offset >> PAGE_SHIFT;
  // The offset into the PAGE_SIZED block that represents blkno
  size_t sys_blk_offset = byte_offset & PAGE_MASK;

  blkno = blkno - (sys_blk_offset >> vp->m_block_shift);

  s = splbio();
loop:
  if ((bp = incore(vp, blkno))) {
    if (bp->b_flags & B_BUSY) {
      bp->b_flags |= B_WANTED;
      tsleep(bp, PRIBIO, "getblk", 0);
      goto loop;
    }

    bp->b_flags |= B_BUSY | B_CACHE;

    bremfree(bp);

    /*
     * check for size inconsistancies
     */
    if (bp->b_bcount != size) {
      log_debug("getblk: invalid buffer size: %ld\n", bp->b_bcount);
      bp->b_flags |= B_INVAL;
      bwrite(bp);
      goto loop;
    }
  } else {
    if ((bp = getnewbuf(0, 0)) == 0) {
      goto loop;
    }

    bp->b_blkno = bp->b_lblkno = blkno;
    // bgetvp(vp, bp);
    // vp->on_buffer_attach(bp);
    LIST_REMOVE(bp, b_hash);
    bh = BUFHASH(vp, blkno);
    LIST_INSERT_HEAD(bh, bp, b_hash);
    allocbuf(bp, size);

    // ----- HACK
    size_t object_offset = ((bp)->m_page_map_address)
                           - s_blocks_map_entry->links.start
                           + s_blocks_map_entry->offset;

    bp->m_page
        = vm_page_lookup(s_blocks_map_entry->object.vm_object, object_offset);
    // -----------
  }

  bp->b_addr = (void *)(bp->m_page_map_address + sys_blk_offset);

  splx(s);
  return bp;
}

/*
 * Get an empty, disassociated buffer of given size.
 */
struct buf *geteblk(int size) {
  struct buf *bp;

  while ((bp = getnewbuf(0, 0)) == 0)
    ;

  allocbuf(bp, size);

  bp->b_flags |= B_INVAL;

  return bp;
}

/*
 * Modify the length of a buffer's underlying buffer storage without
 * destroying information (unless, of course the buffer is shrinking).
 */
void allocbuf(struct buf *bp, int size) {
  int newbsize = round_page(size);

  if (newbsize == bp->b_bufsize) {
    bp->b_bcount = size;
    return;
  } else if (newbsize < bp->b_bufsize) {
    vm_hold_free_pages((vm_offset_t)bp->b_addr + newbsize,
                       (vm_offset_t)bp->b_addr + bp->b_bufsize);
  } else if (newbsize > bp->b_bufsize) {
    vm_hold_load_pages((vm_offset_t)bp->b_addr + bp->b_bufsize,
                       (vm_offset_t)bp->b_addr + newbsize);
  }

  panic("Should not be here, yet!\n");
  /* adjust buffer cache's idea of memory allocated to buffer contents */
  freebufspace -= newbsize - bp->b_bufsize;
  allocbufspace += newbsize - bp->b_bufsize;

  bp->b_bufsize = newbsize;
  bp->b_bcount  = size;
}

/*
 * Wait for buffer I/O completion, returning error status.
 */
int biowait(struct buf *bp) {
  int s;

  s = splbio();

  while ((bp->b_flags & B_DONE) == 0) {
    tsleep(bp, PRIBIO, "biowait", 0);
  }

  if ((bp->b_flags & B_ERROR) || bp->b_error) {
    if ((bp->b_flags & B_INVAL) == 0) {
      bp->b_flags |= B_INVAL;
      bp->b_dev = NODEV;
      LIST_REMOVE(bp, b_hash);
      LIST_INSERT_HEAD(&invalhash, bp, b_hash);
    }

    if (!bp->b_error) {
      bp->b_error = -EIO;
    } else {
      bp->b_flags |= B_ERROR;
    }

    splx(s);

    return bp->b_error;
  } else {
    splx(s);

    return 0;
  }
}

/*
 * Finish I/O on a buffer, calling an optional function.
 * This is usually called from interrupt level, so process blocking
 * is not *a good idea*.
 */

void vwakeup(struct buf *) {}
void biodone(struct buf *bp) {
  int s;
  s = splbio();
  bp->b_flags |= B_DONE;

  if ((bp->b_flags & B_READ) == 0) {
    vwakeup(bp);
  }

#ifdef BOUNCE_BUFFERS
  if (bp->b_flags & B_BOUNCE)
    vm_bounce_free(bp);
#endif

  /* call optional completion function if requested */
  if (bp->b_flags & B_CALL) {
    bp->b_flags &= ~B_CALL;

    (*bp->b_iodone)(bp);

    splx(s);

    return;
  }

  /*
   * For asynchronous completions, release the buffer now. The brelse
   *	checks for B_WANTED and will do the wakeup there if necessary -
   *	so no need to do a wakeup here in the async case.
   */

  if (bp->b_flags & B_ASYNC) {
    brelse(bp);
  } else {
    bp->b_flags &= ~B_WANTED;
    wakeup(bp);
  }

  splx(s);
}

void clrbuf(bufp_t bp) {
  bzero((bp)->b_addr, (bp)->b_bcount);
  (bp)->b_resid = 0;
}

int count_lock_queue() {
  int count;
  struct buf *bp;

  count = 0;
  for (bp = bufqueues[QUEUE_LOCKED].tqh_first; bp != NULL;
       bp = bp->b_freelist.tqe_next)
    count++;
  return (count);
}

int vfs_update_interval = 30;

void vfs_update() {
  spl0();
  while (1) {
    tsleep(&vfs_update_wakeup, PRIBIO, "update", hz * vfs_update_interval);
    vfs_update_wakeup = 0;
#if DAMIR
    sync(proc::current(), NULL, NULL);
#endif
  }
}

#if 0
#define MAXFREEBP   128
#define LDFREE_BUSY 1
#define LDFREE_WANT 2
int loadfreeing;
struct buf *freebp[MAXFREEBP];
#endif
/*
 * these routines are not in the correct place (yet)
 * also they work *ONLY* for kernel_pmap!!!
 */
void vm_hold_load_pages(vm_offset_t froma, vm_offset_t toa) {
#ifndef LITES
  vm_offset_t pg;
  vm_page_t p;
  vm_offset_t from = round_page(froma);
  vm_offset_t to   = round_page(toa);
#if 0
  for (pg = from; pg < to; pg += PAGE_SIZE) {
  tryagain:

/*
 * don't allow buffer cache to cause VM paging
 */
		if ( cnt.v_free_count < cnt.v_free_min) {
			if( !loadfreeing ) {
				int n=0;
				struct buf *bp;
				loadfreeing = LDFREE_BUSY;
				while( (cnt.v_free_count <= cnt.v_free_min) &&
					(n < MAXFREEBP)) {
					bp = geteblk(0);
					if( bp)
						freebp[n++] = bp;
					else
						break;
				}
				while(--n >= 0) {
					brelse(freebp[n]);
				}
				if( loadfreeing & LDFREE_WANT)
					wakeup((caddr_t) &loadfreeing);
				loadfreeing = 0;
			} else {
				loadfreeing |= LDFREE_WANT;
				tsleep(&loadfreeing, PRIBIO, "biofree", 0);
			}
		}

    if ((curproc != pageproc) &&
        (cnt.v_free_count <= cnt.v_free_reserved + (toa - froma) / PAGE_SIZE)) {
      VM_WAIT;
      goto tryagain;
    }

    p = vm_page_alloc(kernel_object, pg - VM_MIN_KERNEL_ADDRESS);
    if (!p) {
      VM_WAIT;
      goto tryagain;
    }

    vm_page_wire(p);
    pmap_kenter(pg, VM_PAGE_TO_PHYS(p));

  }
#endif
#endif /* !LITES */
}

void vm_hold_free_pages(vm_offset_t froma, vm_offset_t toa) {
  vm_offset_t pg;
  vm_page_t p;
  vm_offset_t from = round_page(froma);
  vm_offset_t to   = round_page(toa);
  { panic("Not implemented, %s\n", __PRETTY_FUNCTION__); }

  for (pg = from; pg < to; pg += PAGE_SIZE) {
    p = vm_page_get_page(pmap_extract(kernel_map->pmap, pg));
    pmap_remove(kernel_map->pmap, pg, PAGE_SIZE);
    vm_page_free(p);
  }
}

void bufstats() {}

void vfs_bufstats() {}
