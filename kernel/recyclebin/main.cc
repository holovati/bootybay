#include <mach/clist.h>
#include <mach/conf.h>

#include <fs/ext2.h>
#include <fs/specfs.h>
#include <mach/fcntl.h>
#include <mach/file.h>
#include <mach/filedesc.h>
#include <mach/mount.h>

#include <mach/stat.h>
#include <mach/syslog/syslog.h>
#include <mach/tty.h>
#include <mach/ucred.h>
#include <mach/vfs.h>

/*
 * Initialization code.
 * Called from cold start routine as
 * soon as a stack and segmentation
 * have been established.
 * Functions:
 *	clear and free user core
 *	turn on clock
 *	hand craft 0th process
 *	call all initialization routines
 *	fork - process 0 to schedule
 *	     - process 1 execute bootstrap
 *
 * loop at low address in user mode -- /etc/init
 *	cannot be executed.
 */
void main(vm_address_t **pid1_ssize, vm_address_t **pid1_maxsaddr)
{
        /*startup();*/

        /* Create the init process */
        process_t p = proc_init();

        dev_t consdev = makedev(0, 0);

        vnode::ptr ttyvn; // Hack
        specfs_cdevvp(consdev, ttyvn);
        ttyvn->lock();
        p->p_pgrp->pg_session->s_ttyvp = ttyvn.get_raw();
        p->p_pgrp->pg_session->s_ttyvp->reference();

        p->p_pgrp->pg_session->s_ttyvp->open(FREAD | FWRITE, NOCRED, &p->thread);
        p->p_pgrp->pg_session->s_ttyp            = cdevsw[consdev].d_tty(consdev);
        p->p_pgrp->pg_session->s_ttyp->t_session = p->p_pgrp->pg_session;
        p->p_pgrp->pg_session->s_ttyp->t_pgrp    = p->p_pgrp;
        p->p_flag |= P_CONTROLT;
        ttyvn->unlock();

        log_info("Hello from %s", "RS64");

        int fd;
        fdalloc(p, 0, &fd);

        ttyvn->bind_file(p->p_cred->pc_ucred, FREAD | FWRITE, &(p->p_fd->fd_ofiles[fd]));

        fdesc_dup(&p->thread, fd);
        fdesc_dup(&p->thread, fd);

        dev_t rootdev = makedev(13, 2);

        vnode::ptr vn_rootdev;
        specfs_bdevvp(rootdev, vn_rootdev);

        KASSERT(ext2_mountroot(&p->thread, vn_rootdev, false, rootdev) == 0);

        /* Get the vnode for '/'.  Set fdp->fd_fd.fd_cdir to reference it. */
        if (mount::mountlist.front()->root(vnode::root))
        {
                panic("cannot find root vnode");
        }

        p->p_fd->fd_cdir = vnode::root.get_raw();
        p->p_fd->fd_cdir->reference();

        p->p_fd->fd_rdir = vnode::root.get_raw();
        p->p_fd->fd_rdir->reference();

        *pid1_maxsaddr = &p->vm_maxsaddr;
        *pid1_ssize    = &p->vm_ssize;
}
