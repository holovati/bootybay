/*
 * Mach Operating System
 * Copyright (c) 1988 Carnegie-Mellon University
 * All rights reserved.  The CMU software License Agreement specifies
 * the terms and conditions for use and redistribution.
 */

#ifndef _MACRO_HELP_H_
#define _MACRO_HELP_H_ 1

#include <mach/machine/macro_help.h>

#define MACRO_BEGIN                                                                                                                \
        do                                                                                                                         \
        {
#define MACRO_END                                                                                                                  \
        }                                                                                                                          \
        while (0)

#define MACRO_RETURN                                                                                                               \
        if (1)                                                                                                                     \
        return

#if !defined(MACRO_BEGIN)

#include <mach/boolean.h>

#ifdef lint
boolean_t NEVER;
boolean_t ALWAYS;
#else
#define NEVER  FALSE
#define ALWAYS TRUE
#endif

#define MACRO_BEGIN                                                                                                                \
        do                                                                                                                         \
        {
#define MACRO_END                                                                                                                  \
        }                                                                                                                          \
        while (NEVER)

#define MACRO_RETURN                                                                                                               \
        if (ALWAYS)                                                                                                                \
        return

#endif

#ifdef __cplusplus
#define SA_SIZE_MULTIPLE_OF(type, s) static_assert((sizeof(type) % (s)) == 0, #type " must be a multiple of " #s "")
#endif

#define DISALLOW_COPY_AND_ASSIGN(cls_name)                                                                                         \
        cls_name(const cls_name &other) = delete;                                                                                  \
        cls_name(cls_name &&cls_name)   = delete;                                                                                  \
        cls_name &operator=(const cls_name &other) = delete;                                                                       \
        cls_name &operator=(cls_name &&other) = delete

#endif
