/*
 *  modified for Lites 1.1
 *
 *  Aug 1995, Godmar Back (gback@cs.utah.edu)
 *  University of Utah, Department of Computer Science
 */
/*
 * Copyright (c) 1982, 1986, 1989, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)ext2_inode.c	8.5 (Berkeley) 12/30/93
 */
extern "C" {
//#include "diagnostic.h"
#include <sys/file.h>

#include "quota.h"
//#include <sys/kernel.h>
#include <mach/param.h>
#include <malloc.h>
//#include <sys/resourcevar.h>
#include <mach/time_value.h>
#include <mach/vfs_cache.h>
//#include <sys/trace.h>
#define trace(a, b, c) /* a b c */
//#include <vm/vm.h>

#include <errno.h>
#include <string.h>

#include "quota.h"
#include "ufs_extern.h"
// STUBS(DAMIR)
}
#include <mach/buf.h>
#include <mach/mount.h>
#include <mach/proc.h>
#include <mach/ucred.h>
#include <mach/uio.h>
#include <mach/vnode.h>
#include <mach/vnode_pager.h>

#include <iterator>
#include <mach/span.hh>

#include "ext2_fs.h"
#include "inode.h"

template <typename T>
class reverse_range {
  T &x;

 public:
  reverse_range(T &x)
      : x(x) {}

  auto begin() const -> decltype(this->x.rbegin()) { return x.rbegin(); }

  auto end() const -> decltype(this->x.rend()) { return x.rend(); }
};

template <typename T>
reverse_range<T> reverse_iterate(T &x) {
  return reverse_range<T>(x);
}

#include "ext2_extern.h"
#include "ext2_mount.h"

extern "C" {
int print_time() {
  panic("%s: Not implemeted", __PRETTY_FUNCTION__);
  return 0;
}
int get_time(struct timeval
                 *val) { /*panic("%s: Not implemeted", __PRETTY_FUNCTION__); */
  return 0;
}

int doforce() {
  panic("%s: Not implemeted", __PRETTY_FUNCTION__);
  return 0;
}
int vn_pager_revoke_write_and_wait() {
  panic("%s: Not implemeted", __PRETTY_FUNCTION__);
  return 0;
}

int ufs_dirbad() { return -1; }
int vn_cache_state_buf_read() { return -1; }
int vn_cache_state_buf_write() { return -1; }
int vn_pager_sync() { return -1; }
void dqinit() { log_debug("Not implemeted"); }

int ufs_check_export(struct mount *a,
                     struct ufid *b,
                     struct mbuf *c,
                     struct vnode **d,
                     int *exflagsp,
                     struct ucred **f) {
  panic("%s: Not implemeted", __PRETTY_FUNCTION__);
  return 0;
}

int ufs_start(struct mount *a, int b, struct proc *c) {
  panic("%s: Not implemeted", __PRETTY_FUNCTION__);
  return 0;
}
}

vnodeops_t dead_vnodeop_p;

int ufs_quotactl(struct mount *e, int d, uid_t c, char *b, struct proc *a) {
  panic("%s: Not implemeted", __PRETTY_FUNCTION__);
  return 0;
}

static int ext2_indirtrunc(struct inode *, i32, i32, i32, int, long *);

/*
 * Update the access, modified, and inode change times as specified by the
 * IACCESS, IUPDATE, and ICHANGE flags respectively. The IMODIFIED flag is
 * used to specify that the inode needs to be updated but that the times have
 * already been set. The access and modified times are taken from the second
 * and third parameters; the inode change time is always taken from the current
 * time. If waitfor is set, then wait for the disk write of the inode to
 * complete.
 */
int ext2_update(struct vnode *a_vp,
                struct timeval *a_access,
                struct timeval *a_modify,
                int a_waitfor) {
  panic(" NONO");
#if 0
  struct ext2_sb_info *fs;
  struct buf *bp;
  struct inode *ip;
  int error;
  struct timeval time;

  ip = VTOI(a_vp);
  if (a_vp->v_mount->mnt_flag & MNT_RDONLY) {
    ip->i_flag &= ~(IN_ACCESS | IN_CHANGE | IN_MODIFIED | IN_UPDATE);
    return (0);
  }
  if ((ip->i_flag & (IN_ACCESS | IN_CHANGE | IN_MODIFIED | IN_UPDATE)) == 0)
    return (0);
  if (ip->i_flag & IN_ACCESS)
    ip->i_din.i_atime = a_access->tv_sec;
  if (ip->i_flag & IN_UPDATE) {
    ip->i_din.i_mtime = a_modify->tv_sec;
    ip->i_modrev++;
  }
  if (ip->i_flag & IN_CHANGE) {
    get_time(&time);
    ip->i_din.i_ctime = time.tv_sec;
  }
  ip->i_flag &= ~(IN_ACCESS | IN_CHANGE | IN_MODIFIED | IN_UPDATE);
  fs = ip->i_e2fs;
  if (error = bread(ip->i_devvp,
                    fsbtodb(fs, ino_to_fsba(fs, ip->i_number)),
                    (int)fs->s_blocksize,
                    NOCRED,
                    &bp)) {
    brelse(bp);
    return (error);
  }

  ((struct ext2_inode *)bp->b_addr)[ino_to_fsbo(fs, ip->i_number)] = ip->i_din;

  //  ext2_di2ei(
  //     &ip->i_din,
  //     (struct ext2_inode *)((vm_offset_t)bp->b_addr
  //                           + EXT2_INODE_SIZE * ino_to_fsbo(fs,
  //                           ip->i_number)));

  if (a_waitfor)
    return (bwrite(bp));
  else {
    bdwrite(bp);
    return (0);
  }
#endif
}

static int ext2_free_blocks(
    int depth, int max_depth, u32 blk, u32 blk_ptrs_cnt, struct inode *ip) {
  panic("Nono");
#if 0
  int error = 0;
  struct buf *bp;
  if (depth < max_depth) {
    error = bread(ip->i_devvp,
                  fsbtodb(ip->i_e2fs, blk),
                  ip->i_e2fs->s_blocksize,
                  NOCRED,
                  &bp);

    emerixx::span blkspan{((u32 *)bp->b_addr), blk_ptrs_cnt};

    for (u32 &b : reverse_range(blkspan)) {
      if (b) {
        ext2_free_blocks(depth + 1, max_depth, b, blk_ptrs_cnt, ip);
        b = 0;
      }
    }

    bwrite(bp);  // check aflags
  }

  ext2_blkfree(ip, blk, ip->i_e2fs->s_blocksize);

  return error;
#endif
}
#if 0
#define SINGLE 0 /* index of single indirect block */
#define DOUBLE 1 /* index of double indirect block */
#define TRIPLE 2 /* index of triple indirect block */
/*
 * Truncate the inode oip to at most length size, freeing the
 * disk blocks.
 */
int ext2_truncate(struct vnode *a_vp,
                  off_t a_length,
                  int a_flags,
                  struct ucred *a_cred,
                  struct proc *a_p) {
  struct vnode *ovp = a_vp;
  i32 lastblock;
  struct inode *oip;
  i32 bn, lbn;
  i32 lastiblock[EXT2_NIDIR_BLOCKS], indir_lbn[EXT2_NIDIR_BLOCKS];
  i32 oldblks[EXT2_N_BLOCKS], newblks[EXT2_N_BLOCKS];
  off_t length = a_length;
  struct ext2_sb_info *fs;
  struct buf *bp;
  int offset, size, level;
  long count, nblocks, vflags, blocksreleased = 0;
  struct timeval tv;
  int i;
  int aflags, error, allerror;
  off_t osize;
  /*
  printf("ext2_truncate called %d to %d\n", VTOI(ovp)->i_number, ap->a_length);
  */	/*
	 * negative file sizes will totally break the code below and
	 * are not meaningful anyways.
	 */
  if (length < 0)
    return -EFBIG;

  if (length != 0) {
    panic("untested");
  }

  oip = VTOI(ovp);
  get_time(&tv);
  if (ovp->v_type == VLNK
      && oip->i_din.i_size < ovp->v_mount->mnt_maxsymlinklen) {
    panic("untested");
#if DIAGNOSTIC
    if (length != 0)
      panic("ext2_truncate: partial truncate of symlink");
#endif
    bzero(&oip->i_din.i_block, (size_t)oip->i_din.i_size);
    oip->i_din.i_size = 0;
    oip->i_flag |= IN_CHANGE | IN_UPDATE;
    return ovp->update(&tv, &tv, 1);
  }
  if (oip->i_din.i_size == length) {
    oip->i_flag |= IN_CHANGE | IN_UPDATE;
    return ovp->update(&tv, &tv, 0);
  }
#if QUOTA
  if (error = getinoquota(oip))
    return (error);
#endif
  vnode_pager_setsize(ovp, (size_t)length);
  fs    = oip->i_e2fs;
  osize = oip->i_din.i_size;
  ext2_discard_prealloc(oip);
  /*
   * Lengthen the size of the file. We must ensure that the
   * last byte of the file is allocated. Since the smallest
   * value of oszie is 0, length will be at least 1.
   */
  if (osize < length) {
    panic("untested");
    offset = blkoff(fs, length - 1);
    lbn    = lblkno(fs, length - 1);
    aflags = B_CLRBUF;
    if (a_flags & IO_SYNC)
      aflags |= B_SYNC;
    if (error = ext2_balloc(oip, lbn, offset + 1, a_cred, &bp, aflags))
      return (error);
    oip->i_din.i_size = length;
    vnode_pager_uncache(ovp);
    if (aflags & IO_SYNC)
      bwrite(bp);
    else
      bawrite(bp);
    oip->i_flag |= IN_CHANGE | IN_UPDATE;
    return ovp->update(&tv, &tv, 1);
  }
  /*
   * Shorten the size of the file. If the file is not being
   * truncated to a block boundry, the contents of the
   * partial block following the end of the file must be
   * zero'ed in case it ever become accessable again because
   * of subsequent file growth.
   */
  /* I don't understand the comment above */
  offset = blkoff(fs, length);
  if (offset == 0) {
    oip->i_din.i_size = length;
  } else {
    panic("untested");
    lbn    = lblkno(fs, length);
    aflags = B_CLRBUF;
    if (a_flags & IO_SYNC)
      aflags |= B_SYNC;
    if (error = ext2_balloc(oip, lbn, offset, a_cred, &bp, aflags))
      return (error);
    oip->i_din.i_size = length;
    size              = blksize(fs, oip, lbn);
    vnode_pager_uncache(ovp);
    bzero((void *)((vm_offset_t)bp->b_addr + offset), (size_t)(size - offset));
    allocbuf(bp, size);
    if (aflags & IO_SYNC)
      bwrite(bp);
    else
      bawrite(bp);
  }
  /*
   * Calculate index into inode's block list of
   * last direct and indirect blocks (if any)
   * which we want to keep.  Lastblock is -1 when
   * the file is truncated to 0.
   */
  lastblock          = lblkno(fs, length + fs->s_blocksize - 1) - 1;
  lastiblock[SINGLE] = lastblock - EXT2_NDIR_BLOCKS;
  lastiblock[DOUBLE] = lastiblock[SINGLE] - NINDIR(fs);
  lastiblock[TRIPLE] = lastiblock[DOUBLE] - NINDIR(fs) * NINDIR(fs);
  nblocks            = btodb(fs->s_blocksize);
  /*
   * Update file and block pointers on disk before we start freeing
   * blocks.  If we crash before free'ing blocks below, the blocks
   * will be returned to the free list.  lastiblock values are also
   * normalized to -1 for calls to ext2_indirtrunc below.
   */
  bcopy(&oip->i_din.i_block[0], oldblks, sizeof oldblks);

  for (level = TRIPLE; level >= SINGLE; level--)
    if (lastiblock[level] < 0) {
      oip->i_din.i_block[EXT2_NDIR_BLOCKS + level] = 0;
      lastiblock[level]                            = -1;
    }

  for (i = EXT2_NDIR_BLOCKS - 1; i > lastblock; i--) {
    oip->i_din.i_block[i] = 0;
  }

  oip->i_flag |= IN_CHANGE | IN_UPDATE;

  if (error = ovp->update(&tv, &tv, MNT_WAIT)) {
    allerror = error;
  }

  /*
   * Having written the new inode to disk, save its new configuration
   * and put back the old block pointers long enough to process them.
   * Note that we save the new block configuration so we can check it
   * when we are done.
   */

  bcopy(&oip->i_din.i_block[0], newblks, sizeof newblks);
  bcopy(oldblks, &oip->i_din.i_block[0], sizeof oldblks);

  oip->i_din.i_size = osize;

  vflags   = ((length > 0) ? V_SAVE : 0) | V_SAVEMETA;
  allerror = vinvalbuf(ovp, vflags, a_cred, a_p, 0, 0);

  emerixx::span blocks{&oip->i_din.i_block[EXT2_IND_BLOCK], EXT2_NIDIR_BLOCKS};
  int max_depth = EXT2_NIDIR_BLOCKS;
  for (u32 &blk : reverse_range(blocks)) {
    if (blk) {
      ext2_free_blocks(0, max_depth, blk, EXT2_ADDR_PER_BLOCK(fs), oip);
      blk = 0;
    }
    max_depth--;
  }

  blocks = emerixx::span{oip->i_din.i_block, EXT2_NDIR_BLOCKS};
  for (u32 &blk : reverse_range(blocks)) {
    if (blk) {
      ext2_free_blocks(0, 0, blk, EXT2_ADDR_PER_BLOCK(fs), oip);
      blk = 0;
    }
  }

  oip->i_din.i_blocks = btodb(fs->s_blocksize);

#if 0
  /*
   * Indirect blocks first.
   */
  indir_lbn[SINGLE] = -EXT2_NDIR_BLOCKS;
  indir_lbn[DOUBLE] = indir_lbn[SINGLE] - NINDIR(fs) - 1;
  indir_lbn[TRIPLE] = indir_lbn[DOUBLE] - NINDIR(fs) * NINDIR(fs) - 1;
  for (level = TRIPLE; level >= SINGLE; level--) {
    bn = oip->i_din.i_block[EXT2_NDIR_BLOCKS + level];
    if (bn != 0) {
      error = ext2_indirtrunc(oip,
                              indir_lbn[level],
                              fsbtodb(fs, bn),
                              lastiblock[level],
                              level,
                              &count);
      if (error)
        allerror = error;
      blocksreleased += count;
      if (lastiblock[level] < 0) {
        oip->i_din.i_block[EXT2_NDIR_BLOCKS + level] = 0;
        ext2_blkfree(oip, bn, fs->s_frag_size);
        blocksreleased += nblocks;
      }
    }
    if (lastiblock[level] >= 0)
      goto done;
  }

  /*
   * All whole direct blocks or frags.
   */
  for (i = EXT2_NDIR_BLOCKS - 1; i > lastblock; i--) {
    long bsize;

    bn = oip->i_din.i_block[i];
    if (bn == 0)
      continue;
    oip->i_din.i_block[i] = 0;
    bsize                 = blksize(fs, oip, i);
    ext2_blkfree(oip, bn, bsize);
    blocksreleased += btodb(bsize);
  }
  if (lastblock < 0)
    goto done;

  /*
   * Finally, look for a change in size of the
   * last direct block; release any frags.
   */
  bn = oip->i_din.i_block[lastblock];
  if (bn != 0) {
    long oldspace, newspace;

    /*
     * Calculate amount of space we're giving
     * back as old block size minus new block size.
     */
    oldspace          = blksize(fs, oip, lastblock);
    oip->i_din.i_size = length;
    newspace          = blksize(fs, oip, lastblock);
    if (newspace == 0)
      panic("itrunc: newspace");
    if (oldspace - newspace > 0) {
      /*
       * Block number of space to be free'd is
       * the old block # plus the number of frags
       * required for the storage we're keeping.
       */
      bn += numfrags(fs, newspace);
      ext2_blkfree(oip, bn, oldspace - newspace);
      blocksreleased += btodb(oldspace - newspace);
    }
  }
done:
#if DIAGNOSTIC
  for (level = SINGLE; level <= TRIPLE; level++)
    if (newblks[NDADDR + level] != oip->i_ib[EXT2_NDIR_BLOCKS + level])
      panic("itrunc1");
  for (i = 0; i < NDADDR; i++)
    if (newblks[i] != oip->i_db[i])
      panic("itrunc2");
  if (length == 0 && (ovp->v_dirtyblkhd.lh_first || ovp->v_cleanblkhd.lh_first))
    panic("itrunc3");
#endif /* DIAGNOSTIC */
  /*
   * Put back the real size.
   */
#endif  // 0
  oip->i_din.i_size = length;
  // oip->i_din.i_blocks -= blocksreleased;
  if (oip->i_din.i_blocks < 0) /* sanity */
    oip->i_din.i_blocks = 0;
  oip->i_flag |= IN_CHANGE;
#if QUOTA
  (void)chkdq(oip, -blocksreleased, NOCRED, 0);
#endif
  return (allerror);
}

/*
 * Release blocks associated with the inode ip and stored in the indirect
 * block bn.  Blocks are free'd in LIFO order up to (but not including)
 * lastbn.  If level is greater than SINGLE, the block is an indirect block
 * and recursive calls to indirtrunc must be used to cleanse other indirect
 * blocks.
 *
 * NB: triple indirect blocks are untested.
 */

int ext2_indirtrunc(
    struct inode *ip, i32 lbn, i32 lastbn, i32 dbn, int level, long *countp) {
  int i;
  struct buf *bp;
  struct ext2_sb_info *fs = ip->i_e2fs;
  i32 *bap;
  struct vnode *vp;
  i32 *copy, nb, nlbn, last;
  long blkcount, factor;
  int nblocks, blocksreleased = 0;
  int error = 0, allerror = 0;

  /*
   * Calculate index in current block of last
   * block to be kept.  -1 indicates the entire
   * block so we need not calculate the index.
   */
  factor = 1;
  for (i = SINGLE; i < level; i++) {
    factor *= NINDIR(fs);
  }
  last = lastbn;
  if (lastbn > 0)
    last /= factor;
  nblocks = btodb(fs->s_blocksize);
  /*
   * Get buffer of block pointers, zero those entries corresponding
   * to blocks to be free'd, and update on disk copy first.  Since
   * double(triple) indirect before single(double) indirect, calls
   * to bmap on these blocks will fail.  However, we already have
   * the on disk address, so we have to set the b_blkno field
   * explicitly instead of letting bread do everything for us.
   */
  vp = ITOV(ip);
  bp = getblk(vp, lbn, (int)fs->s_blocksize, 0, 0);
  if (bp->b_flags & (B_DONE | B_DELWRI)) {
    /* Braces must be here in case trace evaluates to nothing. */
    trace(TR_BREADHIT, pack(vp, fs->s_blocksize), lbn);
  } else {
    trace(TR_BREADMISS, pack(vp, fs->s_blocksize), lbn);
#ifdef DAMIR
    get_proc()->p_stats->p_ru.ru_inblock++; /* pay for read */
#endif
    bp->b_flags |= B_READ;
    if (bp->b_bcount > bp->b_bufsize)
      panic("ext2_indirtrunc: bad buffer size");
    bp->b_blkno = dbn;
    vnode::strategy(bp);
    error = biowait(bp);
  }
  if (error) {
    brelse(bp);
    *countp = 0;
    return (error);
  }

  bap = (i32 *)bp->b_addr;
#ifdef DAMIR
  MALLOC(copy, size_t *, fs->s_blocksize, M_TEMP, M_WAITOK);
#else
  copy = (i32 *)malloc(fs->s_blocksize);
#endif
  bcopy(bap, copy, (size_t)fs->s_blocksize);
  bzero(&bap[last + 1], (size_t)(NINDIR(fs) - (last + 1)) * sizeof(i32));

  if (last == -1) {
    bp->b_flags |= B_INVAL;
  }

  error = bwrite(bp);

  if (error) {
    allerror = error;
  }

  bap = copy;

  /*
   * Recursively free totally unused blocks.
   */
  for (i = NINDIR(fs) - 1, nlbn = lbn + 1 - i * factor; i > last;
       i--, nlbn += factor) {
    nb = bap[i];
    if (nb == 0)
      continue;
    if (level > SINGLE) {
      if (error = ext2_indirtrunc(
              ip, nlbn, fsbtodb(fs, nb), -1, level - 1, &blkcount))
        allerror = error;
      blocksreleased += blkcount;
    }
    ext2_blkfree(ip, nb, fs->s_blocksize);
    blocksreleased += nblocks;
  }

  /*
   * Recursively free last partial block.
   */
  if (level > SINGLE && lastbn >= 0) {
    last = lastbn % factor;
    nb   = bap[i];
    if (nb != 0) {
      if (error = ext2_indirtrunc(
              ip, nlbn, fsbtodb(fs, nb), last, level - 1, &blkcount))
        allerror = error;
      blocksreleased += blkcount;
    }
  }
#ifdef DAMIR
  FREE(copy, M_TEMP);
#else
  free(copy);
#endif
  *countp = blocksreleased;
  return (allerror);
}
#endif
/*
 *	discard preallocated blocks
 */
int ext2_inactive(struct vnode *a_vp) {
  ext2_discard_prealloc(VTOI(a_vp));
  return ufs_inactive(a_vp);
}

/*
 * Reclaim an inode so that it can be used for other purposes.
 */
#if 0
int ext2_reclaim(struct vnode *a_vp) {
  struct vnode *vp = a_vp;
  struct inode *ip;
  int i, type;

#ifdef DAMIR /* prtactive prints out reclamation of active nodes */
  if (prtactive && vp->v_usecount != 0)
    log_warn("ufs_reclaim: pushing active");
#endif

  /*
   * Remove the inode from its hash chain.
   */
  ip = VTOI(vp);
  ufs_ihashrem(ip);
  /*
   * Purge old data structures associated with the inode.
   */
  vfs_cache_purge(vp);
  if (ip->i_devvp) {
    vnode::unreference(ip->i_devvp);
    ip->i_devvp = 0;
  }
#if QUOTA
  for (i = 0; i < MAXQUOTAS; i++) {
    if (ip->i_dquot[i] != NODQUOT) {
      dqrele(vp, ip->i_dquot[i]);
      ip->i_dquot[i] = NODQUOT;
    }
  }
#endif
  switch (vp->v_mount->mnt_stat.f_type) {
    case MOUNT_UFS:
    case MOUNT_EXT2FS:
      type = 45; /* FFS_NODE XXX */
      break;
#ifdef DAMIR
    case MOUNT_MFS:
      type = M_MFSNODE;
      break;
    case MOUNT_LFS:
      type = M_LFSNODE;
      break;
#endif
    default:
      panic("ufs_reclaim: not ufs file");
  }
#ifdef DAMIR
  FREE(vp->v_data, type);
#else
  /* We have allocated the inode using malloc */
  free(vp->v_data);
#endif
  vp->v_data = NULL;
  return (0);
}
#endif