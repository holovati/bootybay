/*
 *  modified for Lites 1.1
 *
 *  Aug 1995, Godmar Back (gback@cs.utah.edu)
 *  University of Utah, Department of Computer Science
 */
/*
 *  linux/fs/ext2/balloc.c
 *
 * Copyright (C) 1992, 1993, 1994, 1995
 * Remy Card (card@masi.ibp.fr)
 * Laboratoire MASI - Institut Blaise Pascal
 * Universite Pierre et Marie Curie (Paris VI)
 *
 *  Enhanced block allocation by Stephen Tweedie (sct@dcs.ed.ac.uk), 1993
 */

/*
 * The free blocks are managed by bitmaps.  A file system contains several
 * blocks groups.  Each group contains 1 bitmap block for blocks, 1 bitmap
 * block for inodes, N blocks for the inode table and data blocks.
 *
 * The file system contains group descriptors which are located after the
 * super block.  Each descriptor contains the number of the bitmap block and
 * the free blocks count in the block.  The descriptors are loaded in memory
 * when a file system is mounted (see ext2_read_super).
 */
extern "C"
{
#include <mach/param.h>
#include <mach/ucred.h>

#include "quota.h"
}
#include <mach/bitops.h>
#include <mach/buf.h>
#include <mach/mount.h>
#include <mach/proc.h>
#include <mach/vnode.h>
#include <string.h>

#include "ext2_extern.h"
#include "ext2_fs.h"
#include "ext2_mount.h"

unsigned long ext2_count_free(struct buffer_head *, unsigned int);

#define in_range(b, first, len) ((b) >= (first) && (b) <= (first) + (len)-1)

static inline char *memscan(void *addr, int c, size_t size)
{
        char *res = (char *)memchr(addr, (int)c, size);
        return res ? res : (char *)(((vm_offset_t)addr) + size);
}

/* got rid of get_group_desc since it can already be found in
 * ext2_linux_ialloc.c
 */
static void read_block_bitmap(struct mount *mp, unsigned int block_group, unsigned long bitmap_nr)
{
        struct ext2_sb_info *sb = &VFSTOEXT2(mp)->e2fs;
        struct ext2_group_desc *gdp;
        struct buf *bh;
        int error;
        panic("not ready");
#if 0
        gdp = get_group_desc(mp, block_group, NULL);
        if (error = bread(VFSTOEXT2(mp)->um_devvp.get_raw(), fsbtodb(sb, gdp->bg_block_bitmap), sb->s_blocksize, NOCRED, &bh))
                panic("read_block_bitmap: "
                      "Cannot read block bitmap - "
                      "block_group = %d, block_bitmap = %lu",
                      block_group,
                      (unsigned long)gdp->bg_block_bitmap);
        sb->s_block_bitmap_number[bitmap_nr] = block_group;
        memcpy(sb->s_block_bitmap[bitmap_nr], bh->b_addr, sb->s_blocksize);

        brelse(bh);
#endif
}

/*
 * load_block_bitmap loads the block bitmap for a blocks group
 *
 * It maintains a cache for the last bitmaps loaded.  This cache is managed
 * with a LRU algorithm.
 *
 * Notes:
 * 1/ There is one cache per mounted file system.
 * 2/ If the file system contains less than EXT2_MAX_GROUP_LOADED groups,
 *    this function reads the bitmap without maintaining a LRU cache.
 */
static int load__block_bitmap(struct mount *mp, unsigned int block_group)
{
        int i, j;
        struct ext2_sb_info *sb = &VFSTOEXT2(mp)->e2fs;
        unsigned long block_bitmap_number;
        // struct buffer_head *block_bitmap;
        char *block_bitmap;
        int error;

        if (block_group >= sb->s_groups_count)
                panic("load_block_bitmap: "
                      "block_group >= groups_count - "
                      "block_group = %d, groups_count = %lu",
                      block_group,
                      sb->s_groups_count);

        if (sb->s_groups_count <= EXT2_MAX_GROUP_LOADED)
        {
                if (sb->s_block_bitmap[block_group])
                {
                        if (sb->s_block_bitmap_number[block_group] != block_group)
                                panic("load_block_bitmap: "
                                      "block_group != block_bitmap_number");
                        else
                                return block_group;
                }
                else
                {
                        read_block_bitmap(mp, block_group, block_group);
                        return block_group;
                }
        }

        for (i = 0; i < sb->s_loaded_block_bitmaps && sb->s_block_bitmap_number[i] != block_group; i++)
                ;
        if (i < sb->s_loaded_block_bitmaps && sb->s_block_bitmap_number[i] == block_group)
        {
                block_bitmap_number = sb->s_block_bitmap_number[i];
                block_bitmap        = sb->s_block_bitmap[i];
                for (j = i; j > 0; j--)
                {
                        sb->s_block_bitmap_number[j] = sb->s_block_bitmap_number[j - 1];
                        sb->s_block_bitmap[j]        = sb->s_block_bitmap[j - 1];
                }
                sb->s_block_bitmap_number[0] = block_bitmap_number;
                sb->s_block_bitmap[0]        = block_bitmap;
        }
        else
        {
                if (sb->s_loaded_block_bitmaps < EXT2_MAX_GROUP_LOADED)
                        sb->s_loaded_block_bitmaps++;
                else
                {
                        // brelse(sb->s_block_bitmap[EXT2_MAX_GROUP_LOADED - 1]);
                }
                for (j = sb->s_loaded_block_bitmaps - 1; j > 0; j--)
                {
                        sb->s_block_bitmap_number[j] = sb->s_block_bitmap_number[j - 1];
                        sb->s_block_bitmap[j]        = sb->s_block_bitmap[j - 1];
                }
                read_block_bitmap(mp, block_group, 0);
        }
        return 0;
}

static inline int load_block_bitmap(struct mount *mp, unsigned int block_group)
{
        struct ext2_sb_info *sb = &VFSTOEXT2(mp)->e2fs;
        if (sb->s_loaded_block_bitmaps > 0 && sb->s_block_bitmap_number[0] == block_group)
                return 0;

        if (sb->s_groups_count <= EXT2_MAX_GROUP_LOADED && sb->s_block_bitmap_number[block_group] == block_group
            && sb->s_block_bitmap[block_group])
                return block_group;

        return load__block_bitmap(mp, block_group);
}

void ext2_free_blocks(struct mount *mp, unsigned long block, unsigned long count)
{
        struct ext2_sb_info *sb = &VFSTOEXT2(mp)->e2fs;
        // struct buffer_head *bh;
        // struct buffer_head *bh2;
        char *bh, *bh2;
        unsigned long block_group;
        unsigned long bit;
        unsigned long i;
        int bitmap_nr;
        struct ext2_group_desc *gdp;
        struct ext2_super_block *es = &sb->s_es;

        if (!sb)
        {
                log_warn("ext2_free_blocks: nonexistent device");
                return;
        }
        // lock_super(VFSTOEXT2(mp)->um_devvp);
        vnode::locked_ptr devvp{VFSTOEXT2(mp)->um_devvp};

        if (block < es->s_first_data_block || (block + count) > es->s_blocks_count)
        {
                log_debug("ext2_free_blocks: "
                          "Freeing blocks not in datazone - "
                          "block = %lu, count = %lu",
                          block,
                          count);
                return;
        }

        ext2_debug("freeing blocks %lu to %lu\n", block, block + count - 1);

        block_group = (block - es->s_first_data_block) / EXT2_BLOCKS_PER_GROUP(sb);
        bit         = (block - es->s_first_data_block) % EXT2_BLOCKS_PER_GROUP(sb);
        if (bit + count > EXT2_BLOCKS_PER_GROUP(sb))
                panic("ext2_free_blocks: "
                      "Freeing blocks across group boundary - "
                      "Block = %lu, count = %lu",
                      block,
                      count);
        bitmap_nr = load_block_bitmap(mp, block_group);
        bh        = sb->s_block_bitmap[bitmap_nr];
        gdp       = get_group_desc(mp, block_group, /*&bh2*/ nullptr);

        if (/* test_opt (sb, CHECK_STRICT) && 	assume always strict ! */
            (in_range(gdp->bg_block_bitmap, block, count) || in_range(gdp->bg_inode_bitmap, block, count)
             || in_range(block, gdp->bg_inode_table, sb->s_itb_per_group)
             || in_range(block + count - 1, gdp->bg_inode_table, sb->s_itb_per_group)))
                panic("ext2_free_blocks: "
                      "Freeing blocks in system zones - "
                      "Block = %lu, count = %lu",
                      block,
                      count);

        for (i = 0; i < count; i++)
        {
                if (!emerixx::bitops::clear_bit((u8 *)bh, bit + i))
                        log_debug("ext2_free_blocks: "
                                  "bit already cleared for block %lu",
                                  block);
                else
                {
                        gdp->bg_free_blocks_count++;
                        es->s_free_blocks_count++;
                }
        }

        // mark_buffer_dirty(bh2);
        // mark_buffer_dirty(bh /*, 1*/);
        /****
                if (sb->s_flags & MS_SYNCHRONOUS) {
                        ll_rw_block (WRITE, 1, &bh);
                        wait_on_buffer (bh);
                }
        ****/
        sb->s_dirt = 1;
        return;
}

/*
 * ext2_new_block uses a goal block to assist allocation.  If the goal is
 * free, or there is a free block within 32 blocks of the goal, that block
 * is allocated.  Otherwise a forward search is made for a free block; within
 * each block group the search first looks for an entire free byte in the block
 * bitmap, and then for any free bit if that fails.
 */
int ext2_new_block(struct mount *mp, unsigned long goal, u32 *prealloc_count, u32 *prealloc_block)
{
        struct ext2_sb_info *sb = &VFSTOEXT2(mp)->e2fs;
        char *bh, bh2;
        char *p, *r;
        int i, j, k, tmp;
        int bitmap_nr;
        struct ext2_group_desc *gdp;
        struct ext2_super_block *es = &sb->s_es;

        static int goal_hits = 0, goal_attempts = 0;

        if (!sb)
        {
                log_debug("ext2_new_block: nonexistent device");
                return 0;
        }
        vnode::locked_ptr devvp{VFSTOEXT2(mp)->um_devvp}

        ext2_debug("goal=%lu.\n", goal);

repeat:
        /*
         * First, test whether the goal block is free.
         */
        if (goal < es->s_first_data_block || goal >= es->s_blocks_count)
                goal = es->s_first_data_block;
        i   = (goal - es->s_first_data_block) / EXT2_BLOCKS_PER_GROUP(sb);
        gdp = get_group_desc(mp, i, /*&bh2*/ nullptr);
        if (gdp->bg_free_blocks_count > 0)
        {
                j = ((goal - es->s_first_data_block) % EXT2_BLOCKS_PER_GROUP(sb));

                if (j)
                        goal_attempts++;

                bitmap_nr = load_block_bitmap(mp, i);
                bh        = sb->s_block_bitmap[bitmap_nr];

                ext2_debug("goal is at %d:%d.\n", i, j);

                if (!emerixx::bitops::test_bit((u8 *)bh, j))
                {
                        goal_hits++;
                        ext2_debug("goal bit allocated.\n");
                        goto got_block;
                }
                if (j)
                {
                        /*
                         * The goal was occupied; search forward for a free
                         * block within the next XX blocks.
                         *
                         * end_goal is more or less random, but it has to be
                         * less than EXT2_BLOCKS_PER_GROUP. Aligning up to the
                         * next 64-bit boundary is simple..
                         */
                        int end_goal = (j + 63) & ~63;
                        j            = emerixx::bitops::find_next_zero_bit(bh, end_goal, j);
                        if (j < end_goal)
                                goto got_block;
                }

                log_debug("Bit not found near goal");

                /*
                 * There has been no free block found in the near vicinity
                 * of the goal: do a search forward through the block groups,
                 * searching in each group first for an entire free byte in
                 * the bitmap and then for any free bit.
                 *
                 * Search first in the remainder of the current group; then,
                 * cyclicly search through the rest of the groups.
                 */
                p = (bh) + (j >> 3);
                r = memscan(p, 0, (EXT2_BLOCKS_PER_GROUP(sb) - j + 7) >> 3);
                k = (r - (bh)) << 3;
                if (k < EXT2_BLOCKS_PER_GROUP(sb))
                {
                        j = k;
                        goto search_back;
                }
                k = emerixx::bitops::find_next_zero_bit((u8 *)bh, EXT2_BLOCKS_PER_GROUP(sb), j);
                if (k < EXT2_BLOCKS_PER_GROUP(sb))
                {
                        j = k;
                        goto got_block;
                }
        }

        log_debug("Bit not found in block group %d.", i);

        /*
         * Now search the rest of the groups.  We assume that
         * i and gdp correctly point to the last group visited.
         */
        for (k = 0; k < sb->s_groups_count; k++)
        {
                i++;
                if (i >= sb->s_groups_count)
                        i = 0;
                gdp = get_group_desc(mp, i, nullptr);
                if (gdp->bg_free_blocks_count > 0)
                        break;
        }
        if (k >= sb->s_groups_count)
        {
                return 0;
        }
        bitmap_nr = load_block_bitmap(mp, i);
        bh        = sb->s_block_bitmap[bitmap_nr];
        r         = memscan(bh, 0, EXT2_BLOCKS_PER_GROUP(sb) >> 3);
        j         = (r - bh) << 3;

        if (j < EXT2_BLOCKS_PER_GROUP(sb))
                goto search_back;
        else
        {
                j = emerixx::bitops::find_first_zero_bit((vm_offset_t *)bh, EXT2_BLOCKS_PER_GROUP(sb));
        }

        if (j >= EXT2_BLOCKS_PER_GROUP(sb))
        {
                log_info("ext2_new_block: "
                         "Free blocks count corrupted for block group %d",
                         i);
                return 0;
        }

search_back:
        /*
         * We have succeeded in finding a free byte in the block
         * bitmap.  Now search backwards up to 7 bits to find the
         * start of this group of free blocks.
         */
        for (k = 0; k < 7 && j > 0 && !emerixx::bitops::test_bit((u8 *)bh, j - 1); k++, j--)
                ;

got_block:

        log_debug("using block group %d(%d)\n", i, gdp->bg_free_blocks_count);

        tmp = j + i * EXT2_BLOCKS_PER_GROUP(sb) + es->s_first_data_block;

        if (/* test_opt (sb, CHECK_STRICT) && we are always strict. */
            (tmp == gdp->bg_block_bitmap || tmp == gdp->bg_inode_bitmap || in_range(tmp, gdp->bg_inode_table, sb->s_itb_per_group)))
                panic("ext2_new_block: "
                      "Allocating block in system zone - "
                      "%dth block = %u in group %u",
                      j,
                      tmp,
                      i);

        if (emerixx::bitops::set_bit((u8 *)bh, j))
        {
                log_info("ext2_new_block: "
                         "bit already set for block %d",
                         j);
                goto repeat;
        }

        log_debug("found bit %d", j);

        /*
         * Do block preallocation now if required.
         */
#ifdef EXT2_PREALLOCATE
        if (prealloc_block)
        {
                *prealloc_count = 0;
                *prealloc_block = tmp + 1;
                for (k = 1; k < 8 && (j + k) < EXT2_BLOCKS_PER_GROUP(sb); k++)
                {
                        if (emerixx::bitops::set_bit((u8 *)bh, j + k))
                                break;
                        (*prealloc_count)++;
                }
                gdp->bg_free_blocks_count -= *prealloc_count;
                es->s_free_blocks_count -= *prealloc_count;
                log_debug("Preallocated a further %lu bits.", *prealloc_count);
        }
#endif

        j = tmp;

        // mark_buffer_dirty(bh);
        /****
                if (sb->s_flags & MS_SYNCHRONOUS) {
                        ll_rw_block (WRITE, 1, &bh);
                        wait_on_buffer (bh);
                }
        ****/
        if (j >= es->s_blocks_count)
        {
                log_info("ext2_new_block: "
                         "block >= blocks count - "
                         "block_group = %d, block=%d",
                         i,
                         j);
                return 0;
        }

        log_debug("allocating block %d. "
                  "Goal hits %d of %d.\n",
                  j,
                  goal_hits,
                  goal_attempts);

        gdp->bg_free_blocks_count--;
        // mark_buffer_dirty(bh2 /*, 1*/);
        es->s_free_blocks_count--;
        sb->s_dirt = 1;
        return j;
}

unsigned long ext2_count_free_blocks(struct mount *mp)
{
        struct ext2_sb_info *sb = &VFSTOEXT2(mp)->e2fs;
#ifdef EXT2FS_DEBUG
        struct ext2_super_block *es;
        unsigned long desc_count, bitmap_count, x;
        int bitmap_nr;
        struct ext2_group_desc *gdp;
        int i;

        lock_super(VFSTOEXT2(mp)->um_devvp);
        es           = sb->s_es;
        desc_count   = 0;
        bitmap_count = 0;
        gdp          = NULL;
        for (i = 0; i < sb->s_groups_count; i++)
        {
                gdp = get_group_desc(mp, i, NULL);
                desc_count += gdp->bg_free_blocks_count;
                bitmap_nr = load_block_bitmap(mp, i);
                x         = ext2_count_free(sb->s_block_bitmap[bitmap_nr], sb->s_blocksize);
                ext2_debug("group %d: stored = %d, counted = %lu\n", i, gdp->bg_free_blocks_count, x);
                bitmap_count += x;
        }
        ext2_debug("stored = %lu, computed = %lu, %lu\n", es->s_free_blocks_count, desc_count, bitmap_count);
        unlock_super(VFSTOEXT2(mp)->um_devvp);
        return bitmap_count;
#else
        return sb->s_es.s_free_blocks_count;
#endif
}

static inline int block_in_use(unsigned long block, struct ext2_sb_info *sb, unsigned char *map)
{
        return emerixx::bitops::test_bit((u8 *)map, (block - sb->s_es.s_first_data_block) % EXT2_BLOCKS_PER_GROUP(sb));
}

/*
 *  this function is taken from
 *  linux/fs/ext2/bitmap.c
 */

static int nibblemap[] = {4, 3, 3, 2, 3, 2, 2, 1, 3, 2, 2, 1, 2, 1, 1, 0};

unsigned long ext2_count_free(struct buffer_head *map, unsigned int numchars)
{
        unsigned int i;
        unsigned long sum = 0;

        if (!map)
                return (0);
        for (i = 0; i < numchars; i++)
                sum += nibblemap[((char *)map->b_addr)[i] & 0xf] + nibblemap[(((char *)map->b_addr)[i] >> 4) & 0xf];
        return (sum);
}
