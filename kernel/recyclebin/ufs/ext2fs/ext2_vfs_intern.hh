
#pragma once
#include <mach/vnode.h>
struct vnode;
struct ext2mount;
struct proc;
struct ucred;
struct vnodeops;
typedef struct vnodeops vnodeops_t;

/*
 * Common code for mount and mountroot
 */
int ext2_mountfs(vnode::ptr &devvp, struct ext2mount *mp, struct proc *p);

int ext2_sbupdate(struct ext2mount *, int);
int ext2_flushfiles(struct ext2mount *mp, int flags, struct proc *p);
int ext2_reload(struct ext2mount *mountp, struct ucred *cred, struct proc *p);
int ufs_vinit(struct ext2mount *mntp, vnodeops_t *specops, int (**fifoops)(), struct vnode **vpp);