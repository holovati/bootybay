#include "inode/inode_common.hh"
#include "ext2_fs.h"
#include "ext2_mount.h"
#include "inode/inode_base.hh"
#include <mach/errno.h>
#include <mach/range.hh>
#include <mach/span.hh>
#include <mach/stat.h>
#include <mach/ucred.h>
#include <mach/uio.h>
#include <mach/vnode_pager.h>
#include <string.h>

extern size_t ext2_blkpref(struct inode *ip, size_t lbn, int indx, u32 *bap, size_t blocknr);
extern int ext2_alloc(struct inode *ip, size_t lbn, size_t bpref, int size, struct ucred *cred, size_t *bnp);

static int ext2_balloc(
    inode *a_inodep, size_t a_lbn, int a_size, struct ucred *a_cred, buffer_cache::io_descriptor **a_io_desc, int a_flags)
{
        ext2mount *e2mount = static_cast<ext2mount *>(a_inodep->v_mount);

        size_t i_block_idx;
        lbn_path_t lbn_path;
        size_t lbn_path_len;

        // Used to check if we should clear the buffer or not
        bool alloc_performed = false;

        // Generate the path to a_lbn
        int error = e2mount->generate_lbn_path(a_lbn, lbn_path, &lbn_path_len, &i_block_idx);

        if (error)
        {
                return error;
        }

        u32 new_block = a_inodep->i_din.i_block_u.i_block[i_block_idx];

        // If we are missing a block from the i_block array, allocate it
        if (new_block == 0)
        {
                // size_t pref = ext2_blkpref(a_inodep, a_lbn, i_block_idx, &a_inodep->i_din.i_block[0], 0);

                // error = ext2_alloc(a_inodep, a_lbn, pref, e2mount->e2fs.s_blocksize, a_cred, &new_block);

                error = e2mount->block_alloc(a_inodep, &new_block);

                if (error)
                {
                        return error;
                }

                a_inodep->i_din.i_block_u.i_block[i_block_idx] = new_block;

                // If we allocated a pointer block we must clear it
                if (lbn_path_len)
                {
                        // struct buf *new_block_buf = getblk(
                        //    e2mount->um_devvp.get_raw(), fsbtodb(&e2mount->e2fs, new_block), e2mount->e2fs.s_blocksize, 0, 0);
                        // clrbuf(new_block_buf);

                        // error = bwrite(new_block_buf);
                        buffer_cache::io_descriptor *io_desc;

                        error = e2mount->fsio_get(new_block, &io_desc);

                        if (error)
                        {
                                return error;
                        }

                        char *data;
                        io_desc->get_data(&data);

                        memset(data, 0, io_desc->get_block_size());

                        buffer_cache::release(io_desc);
                }
                else
                {
                        // if it's a data block we respect the CLR_BUF flag.
                        alloc_performed = true;
                }
        }

        // Make sure the path(block inclusive) to a_lbn is valid. Allocating if needed
        for (size_t idx = 0; idx < lbn_path_len; ++idx)
        {
                panic("not ready");
#if 0
                struct buf *indir_buf;
                error = bread(
                    e2mount->um_devvp.get_raw(), fsbtodb(&e2mount->e2fs, new_block), e2mount->e2fs.s_blocksize, a_cred, &indir_buf);

                if (error)
                {
                        brelse(indir_buf);
                        return error;
                }

                u32 *block_ptr_array = (u32 *)(indir_buf->b_addr);

                // Does the block already exist
                if ((new_block = block_ptr_array[lbn_path[idx]]))
                {
                        brelse(indir_buf);
                        continue;
                }
                else
                {
                        alloc_performed = true;
                }

                // Nope, create alloc new
                size_t pref = ext2_blkpref(a_inodep, a_lbn, i_block_idx, block_ptr_array, lbn_path[idx]);

                error = ext2_alloc(a_inodep, a_lbn, pref, e2mount->e2fs.s_blocksize, a_cred, &new_block);

                // If we allocated a pointer block make sure it's cleared, and record its number indir block
                if (idx < (lbn_path_len - 1))
                {
                        struct buf *new_block_buf = getblk(
                            e2mount->um_devvp.get_raw(), fsbtodb(&e2mount->e2fs, new_block), e2mount->e2fs.s_blocksize, 0, 0);

                        clrbuf(new_block_buf);

                        error = bwrite(new_block_buf);

                        if (error)
                        {
                                brelse(indir_buf);
                                return error;
                        }
                }

                // Update the indirect block at the correct index to point to the new block
                block_ptr_array[lbn_path[idx]] = new_block;

                if ((error = bwrite(indir_buf)))
                {
                        return error;
                }

        }

        // Now we should be able to read the block pointed to by a_lbn
        // error = a_inodep->lblk_read(a_lbn, a_bpp, a_cred);
        // if (error)
        //{
        // brelse(*a_bpp);
        // return error;
        //}

        *a_io_desc = buffer_cache::get(a_inodep, lblktosize(&(e2mount->e2fs), a_lbn), e2mount->e2fs.s_blocksize);

        //*a_bpp = getblk(a_inodep, a_lbn, e2mount->e2fs.s_blocksize, 0, 0);

        // Clear the buffer if requested
        if (alloc_performed && (a_flags & B_CLRBUF))
        {
                char *data;
                (*a_io_desc)->get_data(&data);
                memset(data, 0, (*a_io_desc)->get_block_size());
        }

        a_inodep->i_flag |= IN_CHANGE | IN_UPDATE;

                return error;
        }

        void ext2_inode_zero_unused_iblocks(u32 * iblock_arr, size_t inode_size, size_t fsblock_size)
        {
                int used_blocks = (inode_size + fsblock_size - 1) / fsblock_size;

                for (int i = used_blocks; i < EXT2_NDIR_BLOCKS; i++)
                {
                        iblock_arr[i] = 0;
                }
        }
}

int ext2_inode_write(inode *a_inode, uio *a_uio, int a_ioflag, ucred *a_cred)
{
        KASSERT(a_uio->uio_rw == UIO_WRITE);

        size_t resid = a_uio->uio_resid;
        size_t osize = a_inode->i_din.i_size;
        int flags    = a_ioflag & IO_SYNC ? B_SYNC : 0;

        ext2_sb_info *sb_info = &static_cast<ext2mount *>(a_inode->v_mount)->e2fs;

        int error;
        for (error = 0; a_uio->uio_resid > 0;)
        {
                size_t lbn       = lblkno(sb_info, a_uio->uio_offset);
                size_t blkoffset = blkoff(sb_info, a_uio->uio_offset);
                size_t xfersize  = sb_info->s_frag_size - blkoffset;

                if (a_uio->uio_resid < xfersize)
                {
                        xfersize = a_uio->uio_resid;
                }

                if (sb_info->s_frag_size > xfersize)
                {
                        flags |= B_CLRBUF;
                }
                else
                {
                        flags &= ~B_CLRBUF;
                }

                // buf_t *bp;
                buffer_io::io_header *ioh;
                error = ext2_balloc(a_inode, lbn, blkoffset + xfersize, a_cred, &ioh, flags);

                if (error)
                {
                        break;
                }

                if (a_uio->uio_offset + xfersize > a_inode->i_din.i_size)
                {
                        a_inode->i_din.i_size = a_uio->uio_offset + xfersize;
                        // vnode_pager_setsize(vp, (size_t)ip->i_din.i_size);
                }

                // vnode_pager_uncache(vp);

                size_t size = blksize(sb_info, ip, lbn); // - bp->b_resid;

                if (size < xfersize)
                {
                        xfersize = size;
                }

                char *block_data;
                KASSERT(ioh->get_data(&block_data, blkoffset) == KERN_SUCCESS);

                error = uiomove(block_data, (int)xfersize, a_uio);

                if (a_ioflag & IO_SYNC)
                {
                        buffer_io::block_write(ioh, false);
                        //::bwrite(bp);
                }
                else if (xfersize + blkoffset == sb_info->s_frag_size)
                        if (/*doclusterwrite*/ 0)
                        {
                                panic("Not ready");
                                // cluster_write(bp, ip->i_size);
                        }
                        else
                        {
                                // bp->b_flags |= B_AGE;
                                // bawrite(bp);
                                buffer_io::block_write_async(ioh);
                        }
                else
                {
                        // bdwrite(bp);
                        buffer_io::block_write(ioh);
                }

                if (error || xfersize == 0)
                {
                        break;
                }
                a_inode->i_flag |= IN_CHANGE | IN_UPDATE;
        }

        /*
         * If we successfully wrote any data, and we are not the superuser
         * we clear the setuid and setgid bits as a precaution against
         * tampering.
         */
        if (resid > a_uio->uio_resid && a_cred && a_cred->cr_uid != 0)
        {
                a_inode->i_din.i_mode &= ~(ISUID | ISGID);
        }

        if (error)
        {
                if (a_ioflag & IO_UNIT)
                {
                        a_inode->truncate(osize, a_ioflag & IO_SYNC, a_cred, a_uio->uio_procp);
                        a_uio->uio_offset -= resid - a_uio->uio_resid;
                        a_uio->uio_resid = resid;
                }
        }
        else if (resid > a_uio->uio_resid && (a_ioflag & IO_SYNC))
        {
                struct timeval time;
                // get_time(&time);
                error = a_inode->update(&time, &time, 1);
        }

        return error;
}

int ext2_inode_write_buffer(inode *a_inode, int a_ioflag, ucred *a_ucred, void *a_buffer, size_t a_buffer_size)
{
        iovec aiov{.iov_base = a_buffer, .iov_len = a_buffer_size};

        uio u{.uio_iov    = &aiov,
              .uio_iovcnt = 1,
              .uio_offset = 0,
              .uio_resid  = a_buffer_size,
              .uio_procp  = nullptr,
              .uio_segflg = UIO_SYSSPACE,
              .uio_rw     = UIO_WRITE};

        return ext2_inode_write(a_inode, &u, a_ioflag, a_ucred);
}
                extern "C" void ext2_free_blocks(mount * mp, unsigned long block, unsigned long count);

                static int ext2_free_blocks(int depth, int max_depth, u32 blk, struct inode *ip)
                {
                        int error = 0;
                        struct buf *bp;
                        ext2mount *e2mount = static_cast<ext2mount *>(ip->v_mount);
                        if (depth < max_depth)
                        {
                                panic("Not ready");
#if 0
                error = bread(e2mount->um_devvp.get_raw(), fsbtodb(ip->i_e2fs, blk), ip->i_e2fs->s_blocksize, NOCRED, &bp);

                emerixx::span blkspan{((u32 *)bp->b_addr), e2mount->um_nindir};

                for (u32 &b : emerixx::range::reverse(blkspan))
                {
                        if (b)
                        {
                                ext2_free_blocks(depth + 1, max_depth, b, ip);
                                b = 0;
                        }
                }

                bwrite(bp); // check aflags
#endif
                        }

                        // A dirty girl, rewrite! (naming between this function and the one below is kinda unfortunate)
                        ext2_free_blocks(ip->v_mount, blk, ip->i_e2fs->s_blocksize / e2mount->e2fs.s_frag_size);
                        (ip->i_din.i_blocks) -= btodb(ip->i_e2fs->s_blocksize);

                        return error;
                }

                extern "C" void ext2_discard_prealloc(inode * a_inode);
                int ext2_inode_truncate(inode * a_inode, off_t a_length, int a_flags, ucred *a_cred, proc *a_proc)
                {

                        panic("not ready");

                        if (a_length < 0)
                        {
                                return -EFBIG;
                        }

                        if (a_inode->i_din.i_size == a_length)
                        {
                                a_inode->i_flag |= IN_CHANGE | IN_UPDATE;
                                struct timeval tv;
                                // get_time
                                return a_inode->update(&tv, &tv, 0);
                        }

                        // vnode_pager_setsize(a_inode, (size_t)a_length);

                        a_inode->notify_truncate(a_length);

                        struct ext2_sb_info *sb_info = a_inode->i_e2fs;

                        off_t old_size = a_inode->i_din.i_size;

                        ext2_discard_prealloc(a_inode);
                        /*
                         * Lengthen the size of the file. We must ensure that the
                         * last byte of the file is allocated. Since the smallest
                         * value of old_szie is 0, length will be at least 1.
                         */

#if 0
        if (old_size < a_length)
        {
                for (int start_lbn = lblkno(sb_info, old_size); start_lbn < lblkno(sb_info, a_length); ++start_lbn)
                {
                        int offset = blkoff(sb_info, a_length - 1);
                        i32 lbn    = lblkno(sb_info, a_length - 1);
                        int aflags = B_CLRBUF;

                        if (a_flags & IO_SYNC)
                        {
                                aflags |= B_SYNC;
                        }

                        buf_t *bp;
                        if (int error = ext2_balloc(a_inode, start_lbn, offset + 1, a_cred, &bp, aflags); error != 0)
                        {
                                return (error);
                        }

                        a_inode->i_din.i_size += sb_info->s_blocksize;

                        // vnode_pager_uncache(a_inode);

                        if (aflags & IO_SYNC)
                        {
                                bwrite(bp);
                        }
                        else
                        {
                                bawrite(bp);
                        }

                }
                a_inode->i_flag |= IN_CHANGE | IN_UPDATE;
                struct timeval tv;
                // get_time
                return a_inode->update(&tv, &tv, 1);
        }
        /*
         * Shorten the size of the file. If the file is not being
         * truncated to a block boundry, the contents of the
         * partial block following the end of the file must be
         * zero'ed in case it ever become accessable again because
         * of subsequent file growth.
         */
        /* I don't understand the comment above */
        int offset = blkoff(sb_info, a_length);
        if (offset == 0)
        {
                a_inode->i_din.i_size = a_length;
        }
        else
        {
                panic("untested");

                int lbn = lblkno(sb_info, a_length);

                int aflags = B_CLRBUF;

                if (a_flags & IO_SYNC)
                {
                        aflags |= B_SYNC;
                }

                buf_t *bp;
                if (int error = ext2_balloc(a_inode, lbn, offset, a_cred, &bp, aflags); error != 0)
                {
                        return (error);
                }

                a_inode->i_din.i_size = a_length;

                int size = blksize(sb_info, a_inode, lbn);

                // vnode_pager_uncache(a_inode);

                bzero((void *)((vm_offset_t)bp->b_addr + offset), (size_t)(size - offset));

                allocbuf(bp, size);

                if (aflags & IO_SYNC)
                {
                        bwrite(bp);
                }
                else
                {
                        bawrite(bp);
                }
        }
        /*
         * Update file and block pointers on disk before we start freeing
         * blocks.  If we crash before free'ing blocks below, the blocks
         * will be returned to the free list.  lastiblock values are also
         * normalized to -1 for calls to ext2_indirtrunc below.
         */
        i32 oldblks[EXT2_N_BLOCKS];
        i32 newblks[EXT2_N_BLOCKS];
        bcopy(&a_inode->i_din.i_block[0], oldblks, sizeof oldblks);

        int lastblock = lblkno(sb_info, a_length + sb_info->s_blocksize - 1);
        for (int i = EXT2_NDIR_BLOCKS - 1; i > lastblock; i--)
        {
                a_inode->i_din.i_block[i] = 0;
        }

        a_inode->i_flag |= IN_CHANGE | IN_UPDATE;

        timeval tv;
        // GetTime
        int allerror;
        if (int error = a_inode->update(&tv, &tv, MNT_WAIT); error != 0)
        {
                allerror = error;
        }

        /*
         * Having written the new inode to disk, save its new configuration
         * and put back the old block pointers long enough to process them.
         * Note that we save the new block configuration so we can check it
         * when we are done.
         */
        bcopy(&a_inode->i_din.i_block[0], newblks, sizeof newblks);
        bcopy(oldblks, &a_inode->i_din.i_block[0], sizeof oldblks);

        a_inode->i_din.i_size = old_size;

        int vflags = ((a_length > 0) ? V_SAVE : 0) | V_SAVEMETA;
        allerror   = vinvalbuf(a_inode, vflags, a_cred, a_proc, 0, 0);


        // WE MUST INVALIDATE ALL BUFFERS ASSOCIATED WITH THIS VNODE BEFORE TRUNCING IT, OR ELSE IF WE ALLOC A NEW BLOCK AND PERFORM
        // A BREAD ON IT'S LBN, WE WILL GET THIS OLD ONE IF ITS STILL CACHED BY BIO
        KASSERT(vinvalbuf(a_inode, a_flags, a_cred, a_proc, 0, 0) == 0);
        emerixx::span blocks{&a_inode->i_din.i_block[EXT2_IND_BLOCK], EXT2_NIDIR_BLOCKS};
        int max_depth = EXT2_NIDIR_BLOCKS;

        for (u32 &blk : emerixx::range::reverse(blocks))
        {
                if (blk)
                {
                        ext2_free_blocks(0, max_depth, blk, a_inode);
                        blk = 0;
                }
                max_depth--;
        }

        blocks = emerixx::span{a_inode->i_din.i_block, EXT2_NDIR_BLOCKS};
        for (u32 &blk : emerixx::range::reverse(blocks))
        {
                if (blk)
                {
                        ext2_free_blocks(0, 0, blk, a_inode);
                        blk = 0;
                }
        }

#endif

                        a_inode->i_din.i_size = a_length;

                        // Adjusted in ext2_free_blocks
                        // oip->i_din.i_blocks -= blocksreleased;

                        a_inode->i_flag |= IN_CHANGE;

                        return 0;
                }

                extern "C" void ext2_free_inode(inode * inode);
                int ext2_inode_free_disk_inode(inode * a_inode)
                {
                        struct ext2_sb_info *sb_info = &static_cast<ext2mount *>(a_inode->v_mount)->e2fs;

                        if ((size_t)a_inode->i_number >= sb_info->s_inodes_per_group * sb_info->s_groups_count)
                        {
                                panic("ifree: range: dev = 0x%x, ino = %d, fs = %s\n",
                                      a_inode->get_device_id(),
                                      a_inode->i_number,
                                      sb_info->fs_fsmnt);
                        }
                        /* ext2_debug("ext2_vfree (%d, %d) called\n", pip->i_number, ap->a_mode);
                         */
                        ext2_discard_prealloc(a_inode);

                        ext2_free_inode(a_inode);

                        return 0;
                }
