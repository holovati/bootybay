#pragma once

struct uio;
struct inode;
struct ucred;
struct proc;

void ext2_inode_zero_unused_iblocks(u32 *iblock_arr, size_t inode_size, size_t fsblock_size);

int ext2_inode_write(inode *a_inode, uio *a_uio, int a_ioflag, ucred *a_cred);

int ext2_inode_write_buffer(inode *a_inode, int a_ioflag, ucred *a_ucred, void *a_buffer, size_t a_buffer_size);

int ext2_inode_truncate(inode *a_inode, off_t a_length, int a_flags, ucred *a_cred, proc *a_proc);

int ext2_inode_free_disk_inode(inode *a_inode);