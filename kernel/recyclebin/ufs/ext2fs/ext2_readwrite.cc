/*
 *  modified for Lites 1.1
 *
 *  Aug 1995, Godmar Back (gback@cs.utah.edu)
 *  University of Utah, Department of Computer Science
 */
/*
 * Copyright (c) 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)ufs_readwrite.c	8.7 (Berkeley) 1/21/94
 */
extern "C" {
//#include "diagnostic.h"
#include <errno.h>
#include <mach/buf.h>
#include <mach/proc.h>
#include <mach/stat.h>
#include <mach/ucred.h>
#include <mach/uio.h>
#include <mach/vnode_pager.h>

#include "ext2_extern.h"

int get_time(struct timeval *time);  // A stub
}
#include "ext2_fs.h"
#include "ext2_mount.h"
#include "inode.h"
/*
 * Vnode op for reading.
 */
/* ARGSUSED */
int ext2_read(struct vnode *a_vp,
              struct uio *a_uio,
              int a_ioflag,
              struct ucred *a_cred) {
  struct vnode *vp;
  struct inode *ip;
  struct uio *uio;
  struct ext2_sb_info *fs;
  struct buf *bp;
  size_t lbn, nextlbn;
  off_t bytesinfile;
  long size, xfersize, blkoffset;
  int error;
  u16 mode;

  vp   = a_vp;
  ip   = VTOI(vp);
  mode = ip->i_din.i_mode;
  uio  = a_uio;

#if DIAGNOSTIC
  if (uio->uio_rw != UIO_READ)
    panic("%s: mode", READ_S);

  if (vp->v_type == VLNK) {
    if ((int)ip->i_size < vp->v_mount->mnt_maxsymlinklen)
      panic("%s: short symlink", READ_S);
  } else if (vp->v_type != VREG && vp->v_type != VDIR)
    panic("%s: type %d", READ_S, vp->v_type);
#endif
  fs = ip->i_e2fs;
#if 0
	if ((u_quad_t)uio->uio_offset > fs->fs_maxfilesize)
		return -EFBIG;
#endif

  for (error = 0, bp = NULL; uio->uio_resid > 0; bp = NULL) {
    if ((bytesinfile = ip->i_din.i_size - uio->uio_offset) <= 0)
      break;
    lbn       = lblkno(fs, uio->uio_offset);
    nextlbn   = lbn + 1;
    size      = blksize(fs, ip, lbn);
    blkoffset = blkoff(fs, uio->uio_offset);
    xfersize  = fs->s_frag_size - blkoffset;
    if (uio->uio_resid < xfersize)
      xfersize = uio->uio_resid;
    if (bytesinfile < xfersize)
      xfersize = bytesinfile;

    if (lblktosize(fs, nextlbn) > ip->i_din.i_size)
      error = bread(vp, lbn, size, NOCRED, &bp);
#if 0
#define doclusterread 1 /*DAMIR*/
    else if (doclusterread)
      error = cluster_read(vp, ip->i_size, lbn, size, NOCRED, &bp);

    else if (lbn - 1 == vp->v_lastr) {
      int nextsize = BLKSIZE(fs, ip, nextlbn);
      error        = breadn(vp, lbn, size, &nextlbn, &nextsize, 1, NOCRED, &bp);

  }
#endif
    else
      error = bread(vp, lbn, size, NOCRED, &bp);
    if (error)
      break;
    vp->v_lastr = lbn;

    /*
     * We should only get non-zero b_resid when an I/O error
     * has occurred, which should cause us to break above.
     * However, if the short read did not cause an error,
     * then we want to ensure that we do not uiomove bad
     * or uninitialized data.
     */
    size -= bp->b_resid;
    if (size < xfersize) {
      if (size == 0)
        break;
      xfersize = size;
    }
    if (error = uiomove((char *)bp->b_addr + blkoffset, (int)xfersize, uio))
      break;

    if (S_ISREG(mode)
        && (xfersize + blkoffset == fs->s_frag_size
            || uio->uio_offset == ip->i_din.i_size))
      bp->b_flags |= B_AGE;
    brelse(bp);
  }
  if (bp != NULL)
    brelse(bp);
  ip->i_flag |= IN_ACCESS;
  return (error);
}
#if 0
/*
 * Vnode op for writing.
 */
int ext2_write(struct vnode *a_vp,
               struct uio *a_uio,
               int a_ioflag,
               struct ucred *a_cred) {
  struct vnode *vp;
  struct uio *uio;
  struct inode *ip;
  struct ext2_sb_info *fs;
  struct buf *bp;
  struct proc *p;
  size_t lbn;
  off_t osize;
  int blkoffset, error, flags, ioflag, resid, size, xfersize;

  ioflag = a_ioflag;
  uio    = a_uio;
  vp     = a_vp;
  ip     = VTOI(vp);

#if DIAGNOSTIC
  if (uio->uio_rw != UIO_WRITE)
    panic("%s: mode", WRITE_S);
#endif

  switch (vp->v_type) {
    case VREG:
      if (ioflag & IO_APPEND)
        uio->uio_offset = ip->i_din.i_size;
      if ((ip->i_din.i_flags & APPEND) && uio->uio_offset != ip->i_din.i_size)
        return -EPERM;
      /* FALLTHROUGH */
    case VLNK:
      break;
    case VDIR:
      if ((ioflag & IO_SYNC) == 0)
        panic("nonsync dir write");
      break;
    default:
      panic("ext2_write");
  }

  fs = ip->i_e2fs;
#if 0
	if (uio->uio_offset < 0 ||
	    (u_quad_t)uio->uio_offset + uio->uio_resid > fs->fs_maxfilesize)
		return -EFBIG;
#endif

/*
 * Maybe this should be above the vnode op call, but so long as
 * file servers have no limits, I don't think it matters.
 */
#if DAMIR
  p = uio->uio_procp;
  if (vp->v_type == VREG && p
      && uio->uio_offset + uio->uio_resid
             > p->p_rlimit[RLIMIT_FSIZE].rlim_cur) {
    psignal(p, SIGXFSZ);
    return -EFBIG;
  }
#endif

  resid = uio->uio_resid;
  osize = ip->i_din.i_size;
  flags = ioflag & IO_SYNC ? B_SYNC : 0;

  for (error = 0; uio->uio_resid > 0;) {
    lbn       = lblkno(fs, uio->uio_offset);
    blkoffset = blkoff(fs, uio->uio_offset);
    xfersize  = fs->s_frag_size - blkoffset;
    if (uio->uio_resid < xfersize)
      xfersize = uio->uio_resid;

    if (fs->s_frag_size > xfersize)
      flags |= B_CLRBUF;
    else
      flags &= ~B_CLRBUF;

    error = ext2_balloc(ip, lbn, blkoffset + xfersize, a_cred, &bp, flags);

    if (error)
      break;
    if (uio->uio_offset + xfersize > ip->i_din.i_size) {
      ip->i_din.i_size = uio->uio_offset + xfersize;
      vnode_pager_setsize(vp, (size_t)ip->i_din.i_size);
    }

    vnode_pager_uncache(vp);

    size = blksize(fs, ip, lbn) - bp->b_resid;
    if (size < xfersize)
      xfersize = size;

    error = uiomove((char *)bp->b_addr + blkoffset, (int)xfersize, uio);

    if (ioflag & IO_SYNC) {
      bwrite(bp);
    } else if (xfersize + blkoffset == fs->s_frag_size)

      if (/*doclusterwrite*/ 0) {
        panic("Not ready");
        // cluster_write(bp, ip->i_size);
      } else {
        bp->b_flags |= B_AGE;
        bawrite(bp);
      }
    else {
      bdwrite(bp);
    }

    if (error || xfersize == 0) {
      break;
    }

    ip->i_flag |= IN_CHANGE | IN_UPDATE;
  }
  /*
   * If we successfully wrote any data, and we are not the superuser
   * we clear the setuid and setgid bits as a precaution against
   * tampering.
   */
  if (resid > uio->uio_resid && a_cred && a_cred->cr_uid != 0)
    ip->i_din.i_mode &= ~(ISUID | ISGID);
  if (error) {
    if (ioflag & IO_UNIT) {
      vp->truncate(osize, ioflag & IO_SYNC, a_cred, uio->uio_procp);
      uio->uio_offset -= resid - uio->uio_resid;
      uio->uio_resid = resid;
    }
  } else if (resid > uio->uio_resid && (ioflag & IO_SYNC)) {
    struct timeval time;
    get_time(&time);
    error = vp->update(&time, &time, 1);
  }
  return (error);
}
#endif