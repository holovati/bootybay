/*
 * Copyright (c) 1991, 1993
 *	The Regents of the University of California.  All rights reserved.
 * (c) UNIX System Laboratories, Inc.
 * All or some portions of this file are derived from material licensed
 * to the University of California by American Telephone and Telegraph
 * Co. or Unix System Laboratories, Inc. and are reproduced herein with
 * the permission of UNIX System Laboratories, Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)ufs_inode.c	8.4 (Berkeley) 1/21/94
 */
extern "C" {
#include "quota.h"
//#include "diagnostic.h"

//#include <mach/param.h>
//#include <sys/systm.h>
#include <mach/ucred.h>
#include <mach/vfs_cache.h>
//#include <sys/malloc.h>
#include "quota.h"
#include "ufs_extern.h"
}
#include <mach/mount.h>
#include <mach/proc.h>
#include <mach/vnode.h>

#include "ext2_extern.h"
#include "ext2_fs.h"
#include "ext2_mount.h"
#include "inode.h"

/*
 * Last reference to an inode.  If necessary, write or delete it.
 */
int ufs_inactive(struct vnode *a_vp) {
  struct vnode *vp = a_vp;
  struct inode *ip = VTOI(a_vp);
  struct timeval tv;
  int mode, error;
  extern int prtactive;
#ifdef DAMIR
  proc_invocation_t pk = get_proc_invocation();
#endif
  // if (prtactive && vp->v_usecount != 0)
  //   log_info("ffs_inactive: pushing active", vp);

  /* Get rid of inodes related to stale file handles. */
  if (ip->i_din.i_mode == 0) {
    // if ((vp->v_flag & VXLOCK) == 0)
    //  vgone(vp);
    return (0);
  }

  error = 0;
#if DIAGNOSTIC
  if (VOP_ISLOCKED(vp))
    panic("ffs_inactive: locked inode");
  ip->i_lockholder = pk;
#endif
  ip->i_flag |= IN_LOCKED;
  if (ip->i_din.i_links_count <= 0
      && (vp->v_mount->mnt_flag & MNT_RDONLY) == 0) {
#if QUOTA
    if (!getinoquota(ip))
      (void)chkiq(ip, -1, NOCRED, 0);
#endif
    error                = vp->truncate((off_t)0, 0, NOCRED, NULL);
    ip->i_din.i_block[0] = 0;
    mode                 = ip->i_din.i_mode;
    ip->i_din.i_mode     = 0;
    ip->i_flag |= IN_CHANGE | IN_UPDATE;
    vp->vfree(ip->i_number, mode);
  }
  if (ip->i_flag & (IN_ACCESS | IN_CHANGE | IN_MODIFIED | IN_UPDATE)) {
    get_time(&tv);
    vp->update(&tv, &tv, 0);
  }
  vp->unlock();
  /*
   * If we are done with the inode, reclaim it
   * so that it can be reused immediately.
   */
  if (vp->v_usecount == 0 && ip->i_din.i_mode == 0) {
    // vgone(vp);
  }
  return (error);
}
