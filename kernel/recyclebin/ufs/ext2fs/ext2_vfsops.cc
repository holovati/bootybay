// clang-format off
/*
 *  modified for EXT2FS support in Lites 1.1
 *
 *  Aug 1995, Godmar Back (gback@cs.utah.edu)
 *  University of Utah, Department of Computer Science
 */
/*
 * Copyright (c) 1989, 1991, 1993, 1994
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)ffs_vfsops.c	8.8 (Berkeley) 4/18/94
 */
extern "C" {
#include <mach/specdev.h>
#include <mach/stat.h>
#include <mach/ucred.h>

#include "quota.h"
}
#include <mach/errno.h>
#include <mach/file.h>
#include <mach/vfs.h>
#include <malloc.h>
#include <string.h>

#include "ext2_extern.h"
#include "ext2_fs.h"
#include "ext2_mount.h"
#include "ext2_vfs_intern.hh"
#include "inode.h"

extern size_t nextgennumber;

int ext2mount::_mount(char *path,
                      char *data /* this is actually a (struct ufs_args *) */,
                      struct nameidata *ndp,
                      struct proc *p) {
                        panic("broken");
  struct vnode *devvp;
#ifdef DAMIR
  struct ufs_args args;
#endif
  // struct ext2mount *ump = VFSTOEXT2(mp);
  size_t size;
  int error, flags;

#ifdef DAMIR
  if (error = copyin(data, &args, sizeof(struct ufs_args))) {
    return error;
  }
#endif

  /*
   * If updating, check whether changing from read-only to
   * read/write; if there is no device name, that's all we do.
   */
  if (mnt_flag & MNT_UPDATE) {
    // ump   = VFSTOEXT2(mp);
    error = 0;
    if (e2fs.s_rd_only == 0 && (mnt_flag & MNT_RDONLY)) {
      flags = WRITECLOSE;

      if (mnt_flag & MNT_FORCE) {
        flags |= FORCECLOSE;
      }

      if (this->busy()) {
        return -EBUSY;
      }

      error = ext2_flushfiles(this, flags, p);
      this->unbusy();
    }

    if (!error && (mnt_flag & MNT_RELOAD)) {
      panic("Not ready");
      //error = ext2_reload(this, ndp->ni_cnd.cn_cred, p);
    }

    if (error) {
      return error;
    }

    if (e2fs.s_rd_only && (mnt_flag & MNT_WANTRDWR)) {
      e2fs.s_rd_only = 0;
    }

    if (e2fs.s_rd_only == 0) {
      /* don't say it's clean */
      e2fs.s_es.s_state &= ~EXT2_VALID_FS;
      ext2_sbupdate(this, MNT_WAIT);
    }
#ifdef DAMIR
    if (args.fspec == 0) {
      /*
       * Process export requests.
       */
      return (vfs_export(mp, &ump->um_export, &args.export));
    }
#endif
  }
  /*
   * Not an update, or updating the name: look up the name
   * and verify that it refers to a sensible block device.
   */
#ifdef DAMIR
  NDINIT(ndp, LOOKUP, FOLLOW, UIO_USERSPACE, args.fspec, p);

  if (error = namei(ndp)) {
    return (error)
  }

  devvp = ndp->ni_vp;

  if (devvp->v_type != VBLK) {
    vrele(devvp);
    return -ENOTBLK;
  }

  if (major(devvp->v_rdev) >= nblkdev) {
    vrele(devvp);
    return (-ENXIO);
  }
#endif
  if ((mnt_flag & MNT_UPDATE) == 0) {
    //error = ext2_mountfs(devvp, this, p);
  } else {
  //  if (devvp != um_devvp) {
   //   error = -EINVAL; /* needs translation */
  //  } else {
  //    vnode::unreference(devvp);
  //  }
  }

  if (error) {
    devvp->unreference();
    return (error);
  }

  copyinstr(path, e2fs.fs_fsmnt, sizeof(e2fs.fs_fsmnt) - 1, &size);
  //bzero(e2fs.fs_fsmnt + size, sizeof(e2fs.fs_fsmnt) - size);
  //bcopy(e2fs.fs_fsmnt, mnt_stat.f_mntonname, MNAMELEN);
#ifdef DAMIR
  copyinstr(args.fspec, mp->mnt_stat.f_mntfromname, MNAMELEN - 1, &size);
#endif
  //bzero(mnt_stat.f_mntfromname + size, MNAMELEN - size);
  statfs(&mnt_stat, p);
  return 0;
}

int ext2mount::unmount(int mntflags, struct proc *p) {
  int flags = 0;

  if (mntflags & MNT_FORCE) {
    if (mnt_flag & MNT_ROOTFS) {
      return -EINVAL;
    }
    flags |= FORCECLOSE;
  }

  if (int error; error = ext2_flushfiles(this, flags, p)) {
    return (error);
  }

  if (!e2fs.s_rd_only) {
    e2fs.s_es.s_state |= EXT2_VALID_FS; /* was fs_clean = 1 */
    ext2_sbupdate(this, MNT_WAIT);
  }

  /* release buffers containing group descriptors */
  for (int i = 0; i < e2fs.s_db_per_group; i++) {
    //brelse(e2fs.s_group_desc[i]);
  }

  /* release cached inode/block bitmaps */
  for (char *b : e2fs.s_inode_bitmap) {
    if (b) {
      free(b);
      //brelse(b);
    }
  }

  for (char *b : e2fs.s_block_bitmap) {
    if (b) {
        free(b);
      //brelse(b);
    }
  }

  um_devvp->v_un.vu_specinfo->si_flags &= ~SI_MOUNTEDON;

  int r = um_devvp->close(e2fs.s_rd_only ? FREAD : FREAD | FWRITE, NOCRED, p);

  //vnode::unreference(um_devvp);

  mnt_flag &= ~MNT_LOCAL;
  return r;
}

#ifdef OLD_VNODE_CODE
int ext2mount::vget(ino_t ino, struct vnode **vpp) {
  struct inode *ip;
  struct buf *bp;
  struct vnode *vp;
  int i, type, error;
  int used_blocks;

  if ((*vpp = ufs_ihashget(m_dev, ino)) != NULL) {
    return (0);
  }

  /* Allocate a new vnode/inode. */
  if (error = getnewvnode(VT_UFS, this, &ext2_vnodeop, &vp)) {
    *vpp = NULL;
    return (error);
  }
  /* I don't really know what this 'type' does. I suppose it's some kind
   * of memory accounting. Let's just book this memory on FFS's account
   * If I'm not mistaken, this stuff isn't implemented anyway in Lites
   */
#ifdef DAMIR
  type = ump->um_devvp->v_tag == VT_MFS ? M_MFSNODE : M_FFSNODE; /* XXX */
  MALLOC(ip, struct inode *, sizeof(struct inode), type, M_WAITOK);
#else
  type = 45; /* FFS_NODE XXX */
  ip   = (struct inode *)malloc(sizeof(struct inode));
#endif

  insmntque(vp, this);
  bzero((char *)ip, sizeof(struct inode));
  vp->v_data   = ip;
  ip->i_vnode  = vp;
  ip->i_e2fs   = &e2fs;
  ip->i_dev    = m_dev;
  ip->i_number = ino;
#if QUOTA
  for (i = 0; i < MAXQUOTAS; i++) ip->i_dquot[i] = NODQUOT;
#endif
  /*
   * Put it onto its hash chain and lock it so that other requests for
   * this inode will block if they arrive while we are sleeping waiting
   * for old data structures to be purged or for the contents of the
   * disk portion of this inode to be read.
   */
  ufs_ihashins(ip);

  /* Read in the disk contents for the inode, copy into the inode. */
  log_debug("ext2_vget(%d) dbn= %d ",
            ino,
            fsbtodb((&e2fs), ino_to_fsba((&e2fs), ino)));
  if (error = bread(um_devvp,
                    fsbtodb((&e2fs), ino_to_fsba((&e2fs), ino)),
                    (int)e2fs.s_blocksize,
                    NOCRED,
                    &bp)) {
    /*
     * The inode does not contain anything useful, so it would
     * be misleading to leave it on its hash chain. With mode
     * still zero, it will be unlinked and returned to the free
     * list by vput().
     */
    vput(vp);
    brelse(bp);
    *vpp = NULL;
    return (error);
  }

  ip->i_din
      = *(struct ext2_inode *)(((vm_offset_t)bp->b_addr)
                               + EXT2_INODE_SIZE * ino_to_fsbo((&e2fs), ino));

  /* convert ext2 inode to dinode */
  // ext2_ei2di(
  //    (struct ext2_inode *)(((vm_offset_t)bp->b_addr)
  //                          + EXT2_INODE_SIZE * ino_to_fsbo((&e2fs), ino)),
  //    &ip->i_din);

  ip->i_block_group      = ino_to_cg((&e2fs), ino);
  ip->i_next_alloc_block = 0;
  ip->i_next_alloc_goal  = 0;
  ip->i_prealloc_count   = 0;
  ip->i_prealloc_block   = 0;

  /* now we want to make sure that block pointers for unused
     blocks are zeroed out - ext2_balloc depends on this
     although for regular files and directories only
  */
  if (S_ISDIR(ip->i_din.i_mode) || S_ISREG(ip->i_din.i_mode)) {
    used_blocks = (ip->i_din.i_size + e2fs.s_blocksize - 1) / e2fs.s_blocksize;
    for (i = used_blocks; i < EXT2_NDIR_BLOCKS; i++) {
      ip->i_din.i_block[i] = 0;
    }
  }

  // Clear time values
  ip->i_din.i_atime = 0;
  ip->i_din.i_ctime = 0;
  ip->i_din.i_mtime = 0;
  ip->i_din.i_dtime = 0;

  /*
          ext2_print_inode(ip);
  */
  brelse(bp);

  /*
   * Initialize the vnode from the inode, check for aliases.
   * Note that the underlying vnode may have changed.
   */
  if (error
      = ufs_vinit(this, /*&ext2_specop*/ &spec_vnodeop, EXT2_FIFOOPS, &vp)) {
    vput(vp);
    *vpp = NULL;
    return (error);
  }

  /*
   * Finish inode initialization now that aliasing has been resolved.
   */
  ip->i_devvp = um_devvp;
  VREF(ip->i_devvp);
/*
 * Set up a generation number for this inode if it does not
 * already have one. This should only happen on old filesystems.
 */
#if DAMIR
  if (ip->i_gen == 0) {
    struct timeval time;
    get_time(&time);  // Does not work
    if (++nextgennumber < (size_t)time.tv_sec)
      nextgennumber = time.tv_sec;
    ip->i_gen = nextgennumber;
    if ((vp->v_mount->mnt_flag & MNT_RDONLY) == 0)
      ip->i_flag |= IN_MODIFIED;
  }
#endif
  *vpp = vp;
  return (0);
}
#endif  // OLD_VNODE_CODE

#define ITIMES(ip, t1, t2)                                                     \
  {                                                                            \
    if ((ip)->i_flag & (IN_ACCESS | IN_CHANGE | IN_UPDATE)) {                  \
      (ip)->i_flag |= IN_MODIFIED;                                             \
      if ((ip)->i_flag & IN_ACCESS)                                            \
        (ip)->i_atime.ts_sec = (t1)->tv_sec;                                   \
      if ((ip)->i_flag & IN_UPDATE) {                                          \
        (ip)->i_mtime.ts_sec = (t2)->tv_sec;                                   \
        (ip)->i_modrev++;                                                      \
      }                                                                        \
      if ((ip)->i_flag & IN_CHANGE)                                            \
        (ip)->i_ctime.ts_sec = time.tv_sec;                                    \
      (ip)->i_flag &= ~(IN_ACCESS | IN_CHANGE | IN_UPDATE);                    \
    }                                                                          \
  }

/* This overlays the fid structure (see mount.h). */
struct ufid {
  u16 ufid_len;   /* Length of structure. */
  u16 ufid_pad;   /* Force long alignment. */
  ino_t ufid_ino; /* File number (ino). */
  long ufid_gen;  /* Generation number. */
};

/*
 * File handle to vnode
 *
 * Have to be really careful about stale file handles:
 * - check that the inode number is valid
 * - call ext2_vget() to get the locked inode
 * - check for an unallocated inode (i_mode == 0)
 * - check that the given client host has export rights and return
 *   those rights via. exflagsp and credanonp
 */
int ext2mount::fhtovp(fid *fhp,
                      struct mbuf *nam,
                      struct vnode **vpp,
                      int *exflagsp,
                      struct ucred **credanonp) {
  struct ufid *ufhp;

  ufhp = (struct ufid *)fhp;

  if (ufhp->ufid_ino < ROOTINO
      || ufhp->ufid_ino >= e2fs.s_groups_count * e2fs.s_es.s_inodes_per_group) {
    return -ESTALE;
  }
  KASSERT(!"Not Ready");
  return 0;
  // return (ufs_check_export(this, ufhp, nam, vpp, exflagsp, credanonp));
}

#if 0
struct vfsops ext2fs_vfsops = {
    "ext2fs",
    ext2_mount,
    ufs_start, /* empty function */
    ext2_unmount,
    ext2_root,    /* root inode via vget */
    ufs_quotactl, /* does operations associated with quotas */
    ext2_statfs,
    ext2_sync,
    ext2_vget,
    ext2_fhtovp,
    ext2_vptofh,
    ext2_init,
};
#endif

/*
 * unmount system call
 */

/*
 * Get file system statistics.
 * taken from ext2/super.c ext2_statfs
 */

/*
 * Initialize the vnode associated with a new inode, handle aliased
 * vnodes.
 */
#define _QUAD_HIGHWORD 1
#define _QUAD_LOWWORD  0

union _qcvt {
  i64 qcvt;
  long val[2];
};
#define SETHIGH(q, h)                                                          \
  {                                                                            \
    union _qcvt tmp;                                                           \
    tmp.qcvt                = (q);                                             \
    tmp.val[_QUAD_HIGHWORD] = (h);                                             \
    (q)                     = tmp.qcvt;                                        \
  }
#define SETLOW(q, l)                                                           \
  {                                                                            \
    union _qcvt tmp;                                                           \
    tmp.qcvt               = (q);                                              \
    tmp.val[_QUAD_LOWWORD] = (l);                                              \
    (q)                    = tmp.qcvt;                                         \
  }

int ufs_vinit(struct ext2mount *mntp,
              vnodeops_t *specops,
              int (**fifoops)(),
              struct vnode **vpp) {
  struct inode *ip;
  struct vnode *vp, *nvp;

  vp = *vpp;
  ip = static_cast<inode *>(vp);
  switch (vp->v_type = IFTOVT(ip->i_din.i_mode)) {
    case VCHR:
    case VBLK:
   //   vp->vnops = specops;
#if 0
      if (nvp = checkalias(vp, *(dev_t *)ip->i_din.i_block, mntp)) {
        /*
         * Discard unneeded vnode, but save its inode.
         */
        ufs_ihashrem(ip);
        vp->unlock();
        nvp->v_data = vp->v_data;
        vp->v_data  = NULL;
        vp->vnops   = specops;
        vnode::unreference(vp);
        vgone(vp);
        /*
         * Reinitialize aliased inode.
         */
        vp          = nvp;
        ip->i_vnode = vp;
        ufs_ihashins(ip);
      }
      break;
#endif
    case VFIFO:
#if FIFO
      vp->v_op = fifoops;
      break;
#else
      return -EOPNOTSUPP;
#endif
  }
  if (ip->i_number == ROOTINO)
    vp->v_flag |= VROOT;
  /*
   * Initialize modrev times
   */
  {
    struct timeval mono_time;
#ifdef DAMIR
    get_time(&mono_time);  // Does not work
#endif
    SETHIGH(ip->i_modrev, mono_time.tv_sec);
    SETLOW(ip->i_modrev, mono_time.tv_usec * 4294);
  }
  *vpp = vp;
  return (0);
}

/*
 * Vnode pointer to File handle
 */
/* ARGSUSED */
int ext2_vptofh(struct vnode *vp, struct fid *fhp) {
  struct inode *ip;
  struct ufid *ufhp;

  ip             = static_cast<inode *>(vp);
  ufhp           = (struct ufid *)fhp;
  ufhp->ufid_len = sizeof(struct ufid);
  ufhp->ufid_ino = ip->i_number;
  // ufhp->ufid_gen = ip->i_gen;
  return (0);
}
