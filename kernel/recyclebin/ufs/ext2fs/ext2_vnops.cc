/*
 *  modified for EXT2FS support in Lites 1.1
 *
 *  Aug 1995, Godmar Back (gback@cs.utah.edu)
 *  University of Utah, Department of Computer Science
 */
/*
 * Copyright (c) 1982, 1986, 1989, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)ext2_vnops.c	8.7 (Berkeley) 2/3/94
 */

//#include "fifo.h"
//#include "diagnostic.h"
extern "C" {
#include <mach/fcntl.h>
#include <mach/param.h>
#include <mach/specdev.h>
#include <mach/thread.h>
#include <mach/vfs_cache.h>

#include "lockf.h"
#include "quota.h"
#include "ufs_extern.h"
}
#include <mach/buf.h>
#include <mach/file.h>
#include <mach/mount.h>
#include <mach/namei.h>
#include <mach/proc.h>
#include <mach/vnode.h>
#include <malloc.h>
#include <string.h>
#include <strings.h>

#include "ext2_extern.h"
#include "ext2_fs.h"
#include "ext2_utils.hh"
#include "inode.h"

/*
 * Enabling cluster read/write operations.
 */

/* doclusterwrite seems to work only as long as doreallocblks is turned
   off in vfs_cluster.c, but it's slower - so we turn it off
*/
#define doclusterwrite 0
/* doclusterread should work with new pagemove */
#define doclusterread (1)

#include "ext2_readwrite.cc"

/* Take from ufs, change when ext2 -> ufs conversion of inodes is ripped out */
int ext2_open(struct vnode *a_vp,
              mode_t a_mode,
              struct ucred *a_cred,
              struct proc *a_p) {
  /*
   * Files marked append-only must be opened for appending.
   */
  if ((VTOI(a_vp)->i_din.i_flags & APPEND)
      && (a_mode & (FWRITE | O_APPEND)) == FWRITE)
    return -EPERM;
  return (0);
}

/*
 * Close called.
 *
 * Update the times on the inode.
 */
int ext2_close(struct vnode *a_vp,
               int a_fflag,
               struct ucred *a_cred,
               struct proc *a_p) {
  struct inode *ip = VTOI(a_vp);

  if (a_vp->v_usecount > 1 && !(ip->i_flag & IN_LOCKED)) {
    struct timeval time;
    get_time(&time);

    // ITIMES(ip, &time, &time);
  }
  return (0);
}

int ext2_lock(struct vnode *a_vp) {
  struct vnode *vp = a_vp;
  struct inode *ip;
  // proc_invocation_t *pk = get_proc_invocation();

start:
  while (vp->v_flag & VXLOCK) {
    vp->v_flag |= VXWANT;
    sleep(vp, PINOD);
  }

  if (vp->v_tag == VT_NON) {
    return -ENOENT;
  }

  ip = VTOI(vp);
  if (ip->i_flag & IN_LOCKED) {
    ip->i_flag |= IN_WANTED;

#if DIAGNOSTIC
    if (pk == ip->i_lockholder) {
      panic("locking against myself");
      return -EIO;
    }
    ip->i_lockwaiter = pk;
#endif

    sleep(ip, PINOD);
    goto start;
  }

#if DIAGNOSTIC
  ip->i_lockwaiter = 0;
  if (ip->i_lockholder != 0)
    panic("lockholder (x%x) != 0", ip->i_lockholder);
#ifndef LITES
  if (p && p->p_pid == 0)
    printf("locking by process 0\n");
#endif
  ip->i_lockholder = pk;
#endif

  ip->i_flag |= IN_LOCKED;

  return (0);
}

int lockcount = 90;
static int ext2_unlock(struct vnode *a_vp) {
  struct inode *ip = VTOI(a_vp);
  // proc_invocation_t pk      = get_proc_invocation();

#if DIAGNOSTIC
  if ((ip->i_flag & IN_LOCKED) == 0) {
    vprint("ufs_unlock: unlocked inode", ap->a_vp);
    panic("ufs_unlock NOT LOCKED");
  }
  if (pk != ip->i_lockholder) {
    panic("ufs_unlock");
  }
  ip->i_lockholder = 0;
#endif

  ip->i_flag &= ~IN_LOCKED;
  if (ip->i_flag & IN_WANTED) {
    ip->i_flag &= ~IN_WANTED;
    wakeup(ip);
  }
  return (0);
}

/*
 * Synch an open file.
 */
int ext2_fsync(struct vnode *a_vp,
               struct ucred *a_cred,
               int a_waitfor,
               struct proc *a_p) {
  struct vnode *vp = a_vp;
  struct buf *bp;
  struct timeval tv;
  struct buf *nbp;
  int s;

/*
 * Clean memory object.
 * XXX add this to all file systems.
 * XXX why is all this fs specific?
 */
#ifdef DAMIR
  vn_pager_sync(vp, a_waitfor);
#endif
  /*
   * Flush all dirty buffers associated with a vnode.
   */
  ext2_discard_prealloc(VTOI(vp));

loop:
  s = splbio();
  for (bp = vp->v_dirtyblkhd.lh_first; bp; bp = nbp) {
    nbp = bp->b_vnbufs.le_next;
    if ((bp->b_flags & B_BUSY))
      continue;
    if ((bp->b_flags & B_DELWRI) == 0)
      panic("ext2_fsync: not dirty");
    bremfree(bp);
    bp->b_flags |= B_BUSY;
    splx(s);
    /*
     * Wait for I/O associated with indirect blocks to complete,
     * since there is no way to quickly wait for them below.
     */
    if (bp->b_vp == vp || a_waitfor == MNT_NOWAIT)
      bawrite(bp);
    else
      bwrite(bp);
    goto loop;
  }
  if (a_waitfor == MNT_WAIT) {
    while (vp->v_numoutput) {
      vp->v_flag |= VBWAIT;
      sleep(&vp->v_numoutput, PRIBIO + 1);
    }
#if DIAGNOSTIC
    if (vp->v_dirtyblkhd.lh_first) {
      vprint("ext2_fsync: dirty", vp);
      goto loop;
    }
#endif
  }
  splx(s);
  get_time(&tv);
  return a_vp->update(&tv, &tv, a_waitfor == MNT_WAIT);
}

static int ext2_access(struct vnode *a_vp,
                       mode_t a_mode,
                       struct ucred *a_cred,
                       struct proc *a_p) {
  struct vnode *vp   = a_vp;
  struct inode *ip   = VTOI(vp);
  struct ucred *cred = a_cred;
  mode_t mask, mode = a_mode;
  gid_t *gp;
  int i, error;

#if DIAGNOSTIC
  if (!VOP_ISLOCKED(vp)) {
    vprint("ufs_access: not locked", vp);
    panic("ufs_access: not locked");
  }
#endif
#if QUOTA
  if (mode & VWRITE)
    switch (vp->v_type) {
      case VDIR:
      case VLNK:
      case VREG:
        if (error = getinoquota(ip))
          return (error);
        break;
    }
#endif

  /* If immutable bit set, nobody gets to write it. */
  if ((mode & VWRITE) && (ip->i_din.i_flags & IMMUTABLE)) {
    return -EPERM;
  }

  /* Otherwise, user id 0 always gets access. */
  if (cred->cr_uid == 0) {
    return (0);
  }

  mask = 0;

  /* Otherwise, check the owner. */
  if (cred->cr_uid == ip->i_din.i_uid) {
    if (mode & VEXEC) {
      mask |= S_IXUSR;
    }

    if (mode & VREAD) {
      mask |= S_IRUSR;
    }

    if (mode & VWRITE) {
      mask |= S_IWUSR;
    }

    return ((ip->i_din.i_mode & mask) == mask ? 0 : -EACCES);
  }

  /* Otherwise, check the groups. */
  for (i = 0, gp = cred->cr_groups; i < cred->cr_ngroups; i++, gp++)
    if (ip->i_din.i_gid == *gp) {
      if (mode & VEXEC) {
        mask |= S_IXGRP;
      }

      if (mode & VREAD) {
        mask |= S_IRGRP;
      }

      if (mode & VWRITE) {
        mask |= S_IWGRP;
      }

      return ((ip->i_din.i_mode & mask) == mask ? 0 : -EACCES);
    }

  /* Otherwise, check everyone else. */
  if (mode & VEXEC) {
    mask |= S_IXOTH;
  }
  if (mode & VREAD) {
    mask |= S_IROTH;
  }
  if (mode & VWRITE) {
    mask |= S_IWOTH;
  }
  return ((ip->i_din.i_mode & mask) == mask ? 0 : -EACCES);
}

static int ext2_strategy(struct buf *a_bp) {
  struct buf *bp   = a_bp;
  struct vnode *vp = bp->b_vp;
  struct inode *ip;
  int error;
  panic("NONO");
#if 0
  ip = VTOI(vp);

  if (vp->v_type == VBLK || vp->v_type == VCHR) {
    panic("ufs_strategy: spec");
  }

  if (bp->b_blkno == bp->b_lblkno) {
    if (error = vp->bmap(bp->b_lblkno, NULL, &bp->b_blkno, NULL)) {
      bp->b_error = error;
      bp->b_flags |= B_ERROR;
      biodone(bp);
      return (error);
    }
    if ((long)bp->b_blkno == -1) {
      clrbuf(bp);
    }
  }

  if ((long)bp->b_blkno == -1) {
    biodone(bp);
    return (0);
  }

  vp        = ip->i_devvp;
  bp->b_dev = vp->v_un.vu_specinfo->si_rdev;
  // VOCALL(vp->vnops, VOFFSET(vop_strategy), ap);
  // VOP_STRATEGY(vp, a_bp);
  vp->vnops->strategy(a_bp);
  return (0);
#endif
}
/*
              Convert the logical block number bn of a file specified by vnode
              vp to its physical block number on the disk.  The physical block
              is returned in bnp.  In case the logical block is not allocated,
              -1 is used.

              If vpp is not NULL, the vnode of the device vnode for the file
              system is returned in the address specified by vpp.  If runp is
              not NULL, the number of contiguous blocks starting from the next
              block after the queried block will be returned in runp.
*/

#include "ext2_mount.h"
#define blkptrtodb(ump, b) ((b) << (ump)->um_bptrtodb)
void ext2_getindir(struct vnode *a_dvp,
                   struct ext2mount *ump,
                   size_t blksize,
                   size_t indircnt,
                   size_t indirdblkno,
                   size_t *blkno,
                   int *err) {
  if (!(*err)) {
    struct buf *indbp;
    if ((*err = bread(a_dvp, indirdblkno, blksize, NOCRED, &indbp)) == 0) {
      /* If indiridx > 0, we have read a double indirect block */
      size_t indiridx = (*blkno) / indircnt;
      size_t indiroff = (*blkno) % indircnt;

      /* If we overflow the double indirect table means we are dealing with
       * triple indirection, so scale accordingly */
      if (!(indiridx < indircnt)) {
        indiridx = (*blkno) / (indircnt * indircnt);
        indiroff = (*blkno) % (indircnt * indircnt);
      }

      if (!(indiridx < indircnt)) {
        panic("Should never happen!");
      }

      u32 *indirarr = (u32 *)(indbp->b_addr);
      if (indiridx) {
        indirdblkno = (size_t)blkptrtodb(ump, indirarr[indiridx - 1]);
        *blkno      = indiroff;
        brelse(indbp);
        ext2_getindir(a_dvp, ump, blksize, indircnt, indirdblkno, blkno, err);
      } else {
        *blkno = blkptrtodb(ump, indirarr[indiroff]);
        brelse(indbp);
      }
    }
  }
}

static int ext2_bmap(struct vnode *a_vp,
                     size_t a_bn,
                     struct vnode **a_vpp,
                     size_t *a_bnp,
                     int *a_runp) {
  panic("NONO");
#if 0
  if (a_vpp || a_runp) {
    panic("%s: Not supported!");
  }

  struct inode *ip      = VTOI(a_vp);
  struct ext2mount *ump = VFSTOEXT2(a_vp->v_mount);

  /* First 13 blocks are direct */
  if (a_bn < EXT2_NDIR_BLOCKS) {
    /* TODO(DAMIR) Remove this ufs stuff and work directly with inodes */
    *a_bnp = blkptrtodb(ump, ip->i_din.i_block[a_bn]);
    return 0;
  }

  /* lblkno after direct blocks */
  *a_bnp = a_bn - EXT2_NDIR_BLOCKS;

  u32 indircnt = EXT2_ADDR_PER_BLOCK(ip->i_e2fs);

  u32 indiridx = !(*a_bnp < indircnt) + !(*a_bnp < (indircnt * indircnt))
                 + !(*a_bnp < (indircnt * indircnt * indircnt));

  u32 indirdblkno
      = blkptrtodb(ump, ip->i_din.i_block[EXT2_NDIR_BLOCKS + indiridx]);

  /* Number of pointers in a inderect block*/

  /* Vnode for the device the filesystem resides on */
  struct vnode *dvp = ip->i_devvp;

  /* The buffer that will contain the indirect block */
  struct buf *indbp;

  /* We record any errors here and break out if necessary */
  int err = 0;

  ext2_getindir(dvp,
                ump,
                EXT2_BLOCK_SIZE(ip->i_e2fs),
                indircnt,
                indirdblkno,
                a_bnp,
                &err);

  return err;
#endif
}

int ext2_mkdir(struct vnode *a_dvp,
               struct vnode **a_vpp,
               struct componentname *a_cnp,
               struct vattr *a_vap) {
  struct vnode *dvp         = a_dvp;
  struct vattr *vap         = a_vap;
  struct componentname *cnp = a_cnp;
  struct inode *ip, *dp;
  struct vnode *tvp;
  struct timeval tv;
  int error, dmode;

#if DIAGNOSTIC
  if ((cnp->cn_flags & HASBUF) == 0)
    panic("ufs_mkdir: no name");
#endif
  dp = VTOI(dvp);
  if ((nlink_t)dp->i_din.i_links_count >= LINK_MAX) {
    error = -EMLINK;
    goto out;
  }
  dmode = vap->va_mode & 0777;
  dmode |= IFDIR;
  /*
   * Must simulate part of ufs_makeinode here to acquire the inode,
   * but not have it entered in the parent directory. The entry is
   * made later after writing "." and ".." entries.
   */
  if (error = dvp->valloc(dmode, cnp->cn_cred, &tvp))
    goto out;
  ip              = VTOI(tvp);
  ip->i_din.i_uid = cnp->cn_cred->cr_uid;
  ip->i_din.i_gid = dp->i_din.i_gid;
#if QUOTA
  if ((error = getinoquota(ip)) || (error = chkiq(ip, 1, cnp->cn_cred, 0))) {
    free(cnp->cn_pnbuf, M_NAMEI);
    VOP_VFREE(tvp, ip->i_number, dmode);
    vput(tvp);
    vput(dvp);
    return (error);
  }
#endif
  ip->i_flag |= IN_ACCESS | IN_CHANGE | IN_UPDATE;
  ip->i_din.i_mode        = dmode;
  tvp->v_type             = VDIR; /* Rest init'd in getnewvnode(). */
  ip->i_din.i_links_count = 2;
  get_time(&tv);
  error = tvp->update(&tv, &tv, 1);

  /*
   * Bump link count in parent directory
   * to reflect work done below.  Should
   * be done before reference is created
   * so reparation is possible if we crash.
   */
  dp->i_din.i_links_count++;
  dp->i_flag |= IN_CHANGE;
  if (error = dvp->update(&tv, &tv, 1))
    goto bad;
#if DAMIR
  /* Initialize directory with "." and ".." from static template. */
  dtp = (struct dirtemplate *)&omastertemplate;

  dirtemplate            = *dtp;
  dirtemplate.dot_ino    = ip->i_number;
  dirtemplate.dotdot_ino = dp->i_number;
  /* note that in ext2 DIRBLKSIZ == blocksize, not DEV_BSIZE
   * so let's just redefine it - for this function only
   */

  dirtemplate.dotdot_reclen = VTOI(dvp)->i_e2fs->s_blocksize - 12;
#endif

  // Initialize directory with "." and ".."
  {
    ext2_empty_dir edir{ip->i_number, dp->i_number, ip->i_e2fs->s_blocksize};

    error = vn_rdwr(UIO_WRITE,
                    tvp,
                    (char *)&edir,
                    sizeof(edir),
                    (off_t)0,
                    UIO_SYSSPACE,
                    IO_NODELOCKED | IO_SYNC,
                    cnp->cn_cred,
                    (int *)0,
                    (struct proc *)0);
  }
  if (error) {
    dp->i_din.i_links_count--;
    dp->i_flag |= IN_CHANGE;
    goto bad;
  }
  if (VTOI(dvp)->i_e2fs->s_blocksize
      > VFSTOEXT2(dvp->v_mount)->mnt_stat.f_bsize)
    panic("ufs_mkdir: blksize"); /* XXX should grow with balloc() */
  else {
    ip->i_din.i_size = VTOI(dvp)->i_e2fs->s_blocksize;
    ip->i_flag |= IN_CHANGE;
  }

  /* Directory set up, now install it's entry in the parent directory. */
  if (error = ext2_direnter(ip, dvp, cnp)) {
    dp->i_din.i_links_count--;
    dp->i_flag |= IN_CHANGE;
  }
bad:
  /*
   * No need to do an explicit VOP_TRUNCATE here, vrele will do this
   * for us because we set the link count to 0.
   */
  if (error) {
    ip->i_din.i_links_count = 0;
    ip->i_flag |= IN_CHANGE;
    tvp->unlock();
    vnode::unreference(tvp);
  } else {
    *a_vpp = tvp;
  }
out:
#if DAMIR
  FREE(cnp->cn_pnbuf, M_NAMEI);
#else
  free(cnp->cn_pnbuf);
#endif
  dvp->unlock();
  vnode::unreference(dvp);
  return (error);
}

/*
 * Rmdir system call.
 */
int ext2_rmdir(struct vnode *a_dvp,
               struct vnode *a_vp,
               struct componentname *a_cnp) {
  panic("NONO");
#if 0
  struct vnode *vp          = a_vp;
  struct vnode *dvp         = a_dvp;
  struct componentname *cnp = a_cnp;
  struct inode *ip, *dp;
  int error;

  ip = VTOI(vp);
  dp = VTOI(dvp);
  /*
   * No rmdir "." please.
   */
  if (dp == ip) {
    // vrele(dvp);
    vnode::unreference(dvp);
    // vput(vp);
    vp->unlock();
    vnode::unreference(vp);
    return -EINVAL;
  }
  /*
   * Verify the directory is empty (and valid).
   * (Rmdir ".." won't be valid since
   *  ".." will contain a reference to
   *  the current directory and thus be
   *  non-empty.)
   */
  error = 0;
  if (ip->i_din.i_links_count != 2
      || !ext2_dirempty(ip, dp->i_number, cnp->cn_cred)) {
    error = -ENOTEMPTY;
    goto out;
  }
  if ((dp->i_din.i_flags & APPEND)
      || (ip->i_din.i_flags & (IMMUTABLE | APPEND))) {
    error = -EPERM;
    goto out;
  }
  /*
   * Delete reference to directory before purging
   * inode.  If we crash in between, the directory
   * will be reattached to lost+found,
   */
  if (error = ext2_dirremove(dvp, cnp))
    goto out;
  dp->i_din.i_links_count--;
  dp->i_flag |= IN_CHANGE;
  vfs_cache_purge(dvp);
  dvp->unlock();
  vnode::unreference(dvp);
  // vput(dvp);
  dvp = NULL;
  /*
   * Truncate inode.  The only stuff left
   * in the directory is "." and "..".  The
   * "." reference is inconsequential since
   * we're quashing it.  The ".." reference
   * has already been adjusted above.  We've
   * removed the "." reference and the reference
   * in the parent directory, but there may be
   * other hard links so decrement by 2 and
   * worry about them later.
   */
  ip->i_din.i_links_count -= 2;
  error = vp->truncate((off_t)0, IO_SYNC, cnp->cn_cred, cnp->cn_proc);
  vfs_cache_purge(ITOV(ip));
out:
  if (dvp) {
    dvp->unlock();
    vnode::unreference(dvp);
    // vput(dvp);
  }
  vp->unlock();
  vnode::unreference(vp);
  // vput(vp);
  return (error);
#endif
}

/* Taken from ufs, needs to be rewritten once ufs<->ext2 conversion is ripped
 * out */
int ext2_getattr(struct vnode *a_vp,
                 struct vattr *a_vap,
                 struct ucred *a_cred,
                 struct proc *a_p) {
  struct inode *ip = VTOI(a_vp);
  struct timeval time;
  get_time(&time);

  // ITIMES(ip, &time, &time);
  /*
   * Copy from inode table
   */
  a_vap->va_fsid         = ip->i_dev;
  a_vap->va_fileid       = ip->i_number;
  a_vap->va_mode         = ip->i_din.i_mode & ~IFMT;
  a_vap->va_nlink        = ip->i_din.i_links_count;
  a_vap->va_uid          = ip->i_din.i_uid;
  a_vap->va_gid          = ip->i_din.i_gid;
  a_vap->va_rdev         = (dev_t)ip->i_din.i_block[0];
  a_vap->va_size         = ip->i_din.i_size;
  a_vap->va_atime.tv_sec = ip->i_din.i_atime;
  a_vap->va_mtime.tv_sec = ip->i_din.i_mtime;
  a_vap->va_ctime.tv_sec = ip->i_din.i_ctime;

  a_vap->va_flags = ip->i_din.i_flags;
  // a_vap->va_gen   = ip->i_gen;
  /* this doesn't belong here */
  if (a_vp->v_type == VBLK) {
    a_vap->va_blocksize = BLKDEV_IOSIZE;
  } else if (a_vp->v_type == VCHR) {
    a_vap->va_blocksize = MAXBSIZE;
  } else {
    a_vap->va_blocksize = a_vp->v_mount->mnt_stat.f_iosize;
  }
  a_vap->va_bytes   = dbtob(ip->i_din.i_blocks);
  a_vap->va_type    = a_vp->v_type;
  a_vap->va_filerev = ip->i_modrev;
  return (0);
}

/*
 * Return target name of a symbolic link
 */
static int ext2_readlink(struct vnode *a_vp,
                         struct uio *a_uio,
                         struct ucred *a_cred) {
  struct vnode *vp = a_vp;
  struct inode *ip = VTOI(vp);
  int isize;
  isize = ip->i_din.i_size;
#ifdef IC_FASTLINK
  if (isize < vp->v_mount->mnt_maxsymlinklen
      || ((ip->i_flags & IC_FASTLINK) && (isize <= MAXSYMLINKLEN)))
#else
  if (isize < vp->v_mount->mnt_maxsymlinklen)
#endif /* IC_FASTLINK */
  {
    uiomove((char *)ip->i_din.i_block, isize, a_uio);
    return (0);
  }
  return vp->read(a_uio, 0, a_cred);
}

/*
 * Allocate a new inode.
 */
static int ufs_makeinode(int mode,
                         struct vnode *dvp,
                         struct vnode **vpp,
                         struct componentname *cnp) {
  struct inode *ip, *pdir;
  struct timeval tv;
  struct vnode *tvp;
  int error;

  pdir = VTOI(dvp);
#if DIAGNOSTIC
  if ((cnp->cn_flags & HASBUF) == 0)
    panic("ufs_makeinode: no name");
#endif
  *vpp = NULL;
  if ((mode & IFMT) == 0)
    mode |= IFREG;

  if (error = dvp->valloc(mode, cnp->cn_cred, &tvp)) {
    free(cnp->cn_pnbuf /*, M_NAMEI*/);
    dvp->unlock();
    vnode::unreference(dvp);
    // vput(dvp);
    return (error);
  }
  ip              = VTOI(tvp);
  ip->i_din.i_gid = pdir->i_din.i_gid;
  if ((mode & IFMT) == IFLNK)
    ip->i_din.i_uid = pdir->i_din.i_uid;
  else
    ip->i_din.i_uid = cnp->cn_cred->cr_uid;
#if QUOTA
  if ((error = getinoquota(ip)) || (error = chkiq(ip, 1, cnp->cn_cred, 0))) {
    free(cnp->cn_pnbuf, M_NAMEI);
    VOP_VFREE(tvp, ip->i_number, mode);
    vput(tvp);
    vput(dvp);
    return (error);
  }
#endif
  ip->i_flag |= IN_ACCESS | IN_CHANGE | IN_UPDATE;
  ip->i_din.i_mode        = mode;
  tvp->v_type             = IFTOVT(mode); /* Rest init'd in getnewvnode(). */
  ip->i_din.i_links_count = 1;
  if ((ip->i_din.i_mode & ISGID) && !groupmember(ip->i_din.i_gid, cnp->cn_cred)
      && suser(cnp->cn_cred, NULL))
    ip->i_din.i_mode &= ~ISGID;

  /*
   * Make sure inode goes to disk before directory entry.
   */
  get_time(&tv);
  if (error = tvp->update(&tv, &tv, 1))
    goto bad;
  error = ext2_direnter(ip, dvp, cnp);

  if (error) {
    goto bad;
  }

  if ((cnp->cn_flags & SAVESTART) == 0)
#if DAMIR
    FREE(cnp->cn_pnbuf, M_NAMEI);
#else
    free(cnp->cn_pnbuf);
#endif
  dvp->unlock();
  vnode::unreference(dvp);
  // vput(dvp);
  *vpp = tvp;
  return (0);

bad:
  /*
   * Write error occurred trying to update the inode
   * or the directory so must deallocate the inode.
   */
  free(cnp->cn_pnbuf /*, M_NAMEI*/);
  dvp->unlock();
  vnode::unreference(dvp);
  // vput(dvp);
  ip->i_din.i_links_count = 0;
  ip->i_flag |= IN_CHANGE;
  // vput(tvp);
  tvp->unlock();
  vnode::unreference(tvp);
  return (error);
}

/*
 * Create a regular file
 */
static int ext2_create(struct vnode *a_dvp,
                       struct vnode **a_vpp,
                       struct componentname *a_cnp,
                       struct vattr *a_vap) {
  int error;
  if (error = ufs_makeinode(
          MAKEIMODE(a_vap->va_type, a_vap->va_mode), a_dvp, a_vpp, a_cnp))
    return (error);
  return (0);
}

/*
 * Mknod vnode call
 */
/* ARGSUSED */
int ext2_mknod(struct vnode *a_dvp,
               struct vnode **a_vpp,
               struct componentname *a_cnp,
               struct vattr *a_vap) {
  struct vattr *vap  = a_vap;
  struct vnode **vpp = a_vpp;
  struct inode *ip;
  int error;

  if (error = ufs_makeinode(
          MAKEIMODE(vap->va_type, vap->va_mode), a_dvp, vpp, a_cnp)) {
    return (error);
  }

  ip = VTOI(*vpp);
  ip->i_flag |= IN_ACCESS | IN_CHANGE | IN_UPDATE;
  if (vap->va_rdev != VNOVAL) {
    /*
     * Want to be able to use this to make badblock
     * inodes, so don't truncate the dev number.
     */
    *(dev_t *)(ip->i_din.i_block) = vap->va_rdev;
  }
  /*
   * Remove inode so that it will be reloaded by VFS_VGET and
   * checked to see if it is an alias of an existing entry in
   * the inode cache.
   */
  (*vpp)->unlock();
  vnode::unreference(*vpp);
  (*vpp)->v_type = VNON;
  vgone(*vpp);
  *vpp = 0;
  return (0);
}

/*
 * Check for a locked inode.
 */
static int ext2_islocked(struct vnode *a_vp) {
  if (VTOI(a_vp)->i_flag & IN_LOCKED)
    return (1);
  return (0);
}

/*
 * Ufs abort op, called after namei() when a CREATE/DELETE isn't actually
 * done. If a buffer has been saved in anticipation of a CREATE, delete it.
 */
/* ARGSUSED */
static int ext2_abortop(struct vnode *a_dvp, struct componentname *a_cnp) {
  if ((a_cnp->cn_flags & (HASBUF | SAVESTART)) == HASBUF) {
#ifdef DAMIR
    FREE(a_cnp->cn_pnbuf, M_NAMEI);
#else
    free(a_cnp->cn_pnbuf);
#endif
  }
  return 0;
}

#define ALLPERMS                                                               \
  (S_ISUID | S_ISGID | S_ISVTX | S_IRUSR | S_IWUSR | S_IXUSR | S_IRWXU         \
   | S_IRGRP | S_IWGRP | S_IXGRP | S_IRWXG | S_IROTH | S_IWOTH | S_IXOTH       \
   | S_IRWXO)

/*
 * Change the mode on a file.
 * Inode must be locked before calling.
 */
static int ufs_chmod(struct vnode *vp,
                     int mode,
                     struct ucred *cred,
                     struct proc *p) {
  struct inode *ip = VTOI(vp);
  int error;

  if (cred->cr_uid != ip->i_din.i_uid && (error = suser(cred, &p->p_acflag))) {
    return (error);
  }

  if (cred->cr_uid) {
#if DAMIR
    if (vp->v_type != VDIR && (mode & S_ISTXT)) {
      return -EFTYPE;
    }
#endif
    if (!groupmember(ip->i_din.i_gid, cred) && (mode & ISGID))
      return -EPERM;
  }

  ip->i_din.i_mode &= ~ALLPERMS;
  ip->i_din.i_mode |= (mode & ALLPERMS);
  ip->i_flag |= IN_CHANGE;

  if ((vp->v_flag & VTEXT) && (ip->i_din.i_mode) == 0) {
    vnode_pager_uncache(vp);
  }

  return (0);
}

/*
 * Perform chown operation on inode ip;
 * inode must be locked prior to call.
 */
static int ext2_chown(struct vnode *vp,
                      uid_t uid,
                      gid_t gid,
                      struct ucred *cred,
                      struct proc *p) {
  struct inode *ip = VTOI(vp);
  uid_t ouid;
  gid_t ogid;
  int error = 0;
#if QUOTA
  register int i;
  long change;
#endif

  if (uid == (uid_t)VNOVAL)
    uid = ip->i_din.i_uid;
  if (gid == (gid_t)VNOVAL)
    gid = ip->i_din.i_gid;
  /*
   * If we don't own the file, are trying to change the owner
   * of the file, or are not a member of the target group,
   * the caller must be superuser or the call fails.
   */
  if ((cred->cr_uid != ip->i_din.i_uid || uid != ip->i_din.i_uid
       || !groupmember((gid_t)gid, cred))
      && (error = suser(cred, &p->p_acflag))) {
    return (error);
  }

  ogid = ip->i_din.i_gid;
  ouid = ip->i_din.i_uid;
#if QUOTA
  if (error = getinoquota(ip))
    return (error);
  if (ouid == uid) {
    dqrele(vp, ip->i_dquot[USRQUOTA]);
    ip->i_dquot[USRQUOTA] = NODQUOT;
  }
  if (ogid == gid) {
    dqrele(vp, ip->i_dquot[GRPQUOTA]);
    ip->i_dquot[GRPQUOTA] = NODQUOT;
  }
  change = ip->i_blocks;
  (void)chkdq(ip, -change, cred, CHOWN);
  (void)chkiq(ip, -1, cred, CHOWN);
  for (i = 0; i < MAXQUOTAS; i++) {
    dqrele(vp, ip->i_dquot[i]);
    ip->i_dquot[i] = NODQUOT;
  }
#endif
  ip->i_din.i_gid = gid;
  ip->i_din.i_uid = uid;
#if QUOTA
  if ((error = getinoquota(ip)) == 0) {
    if (ouid == uid) {
      dqrele(vp, ip->i_dquot[USRQUOTA]);
      ip->i_dquot[USRQUOTA] = NODQUOT;
    }
    if (ogid == gid) {
      dqrele(vp, ip->i_dquot[GRPQUOTA]);
      ip->i_dquot[GRPQUOTA] = NODQUOT;
    }
    if ((error = chkdq(ip, change, cred, CHOWN)) == 0) {
      if ((error = chkiq(ip, 1, cred, CHOWN)) == 0)
        goto good;
      else
        (void)chkdq(ip, -change, cred, CHOWN | FORCE);
    }
    for (i = 0; i < MAXQUOTAS; i++) {
      dqrele(vp, ip->i_dquot[i]);
      ip->i_dquot[i] = NODQUOT;
    }
  }
  ip->i_gid = ogid;
  ip->i_uid = ouid;
  if (getinoquota(ip) == 0) {
    if (ouid == uid) {
      dqrele(vp, ip->i_dquot[USRQUOTA]);
      ip->i_dquot[USRQUOTA] = NODQUOT;
    }
    if (ogid == gid) {
      dqrele(vp, ip->i_dquot[GRPQUOTA]);
      ip->i_dquot[GRPQUOTA] = NODQUOT;
    }
    (void)chkdq(ip, change, cred, FORCE | CHOWN);
    (void)chkiq(ip, 1, cred, FORCE | CHOWN);
    (void)getinoquota(ip);
  }
  return (error);
good:
  if (getinoquota(ip))
    panic("chown: lost quota");
#endif /* QUOTA */
  if (ouid != uid || ogid != gid) {
    ip->i_flag |= IN_CHANGE;
  }

  if (ouid != uid && cred->cr_uid != 0) {
    ip->i_din.i_mode &= ~ISUID;
  }

  if (ogid != gid && cred->cr_uid != 0) {
    ip->i_din.i_mode &= ~ISGID;
  }
  return (0);
}

#define UF_SETTABLE 0x0000ffff /* mask of owner changeable flags */

/*
 * Set attribute vnode op. called from several syscalls
 */
static int securelevel = 0;
static int ext2_setattr(struct vnode *a_vp,
                        struct vattr *a_vap,
                        struct ucred *a_cred,
                        struct proc *a_p) {
  struct vattr *vap  = a_vap;
  struct vnode *vp   = a_vp;
  struct inode *ip   = VTOI(vp);
  struct ucred *cred = a_cred;
  struct proc *p     = a_p;
  struct timeval atimeval, mtimeval;
  int error;

  /*
   * Check for unsettable attributes.
   */
  if ((vap->va_type != VNON) || ((int)vap->va_nlink != VNOVAL)
      || ((int)vap->va_fsid != VNOVAL) || ((int)vap->va_fileid != VNOVAL)
      || ((int)vap->va_blocksize != VNOVAL) || ((int)vap->va_rdev != VNOVAL)
      || ((int)vap->va_bytes != VNOVAL) || ((int)vap->va_gen != VNOVAL)) {
    return -EINVAL;
  }

  if (vap->va_flags != VNOVAL) {
    if (cred->cr_uid != ip->i_din.i_uid
        && (error = suser(cred, &p->p_acflag))) {
      return (error);
    }

    if (cred->cr_uid == 0) {
      if ((ip->i_din.i_flags & (SF_IMMUTABLE | SF_APPEND)) && securelevel > 0) {
        return -EPERM;
      }

      ip->i_din.i_flags = vap->va_flags;
    } else {
      if (ip->i_din.i_flags & (SF_IMMUTABLE | SF_APPEND)) {
        return -EPERM;
      }

      ip->i_din.i_flags &= SF_SETTABLE;
      ip->i_din.i_flags |= (vap->va_flags & UF_SETTABLE);
    }

    ip->i_flag |= IN_CHANGE;
    if (vap->va_flags & (IMMUTABLE | APPEND))
      return (0);
  }

  if (ip->i_din.i_flags & (IMMUTABLE | APPEND)) {
    return -EPERM;
  }
  /*
   * Go through the fields and update iff not VNOVAL.
   */
  if (vap->va_uid != (uid_t)VNOVAL || vap->va_gid != (gid_t)VNOVAL)
    if (error = ext2_chown(vp, vap->va_uid, vap->va_gid, cred, p))
      return (error);
  if (vap->va_size != VNOVAL) {
    if (vp->v_type == VDIR)
      return -EISDIR;
    if (error = vp->truncate(vap->va_size, 0, cred, p))
      return (error);
  }
  ip = VTOI(vp);
  if (vap->va_atime.tv_sec != VNOVAL || vap->va_mtime.tv_sec != VNOVAL) {
    if (cred->cr_uid != ip->i_din.i_uid && (error = suser(cred, &p->p_acflag))
        && ((vap->va_vaflags & VA_UTIMES_NULL) == 0
            || (error = vp->access(VWRITE, cred, p)))) {
      return (error);
    }

    if (vap->va_atime.tv_sec != VNOVAL)
      ip->i_flag |= IN_ACCESS;
    if (vap->va_mtime.tv_sec != VNOVAL)
      ip->i_flag |= IN_CHANGE | IN_UPDATE;
    atimeval.tv_sec  = vap->va_atime.tv_sec;
    atimeval.tv_usec = vap->va_atime.tv_nsec / 1000;
    mtimeval.tv_sec  = vap->va_mtime.tv_sec;
    mtimeval.tv_usec = vap->va_mtime.tv_nsec / 1000;
    if (error = vp->update(&atimeval, &mtimeval, 1)) {
      return (error);
    }
  }

  error = 0;

  if (vap->va_mode != (mode_t)VNOVAL)
    error = ufs_chmod(vp, (int)vap->va_mode, cred, p);

  return (error);
}

/*
 * Remove a directory entry after a call to namei, using
 * the parameters which it left in nameidata. The entry
 * dp->i_offset contains the offset into the directory of the
 * entry to be eliminated.  The dp->i_count field contains the
 * size of the previous record in the directory.  If this
 * is 0, the first entry is being deleted, so we need only
 * zero the inode number to mark the entry as free.  If the
 * entry is not the first in the directory, we must reclaim
 * the space of the now empty record by adding the record size
 * to the size of the previous entry.
 */
int ext2_dirremove(struct vnode *dvp, struct componentname *cnp) {
  struct inode *dp;
  struct ext2_dir_entry *ep;
  struct buf *bp;
  int error;
  int DIRBLKSIZ = VTOI(dvp)->i_e2fs->s_blocksize;

  dp = VTOI(dvp);

  if (dp->i_count == 0) {
    /*
     * First entry in block: set d_ino to zero.
     */
    if (error = dvp->blkatooff((off_t)dp->i_offset, (char **)&ep, &bp))
      return (error);
    ep->inode = 0;
    // error     = vnode::bwrite(bp);
    error = ::bwrite(bp);
    dp->i_flag |= IN_CHANGE | IN_UPDATE;
    return (error);
  }
  /*
   * Collapse new free space into previous entry.
   */
  if (error = dvp->blkatooff(
          (off_t)(dp->i_offset - dp->i_count), (char **)&ep, &bp)) {
    return (error);
  }

  ep->rec_len += dp->i_reclen;
  // error = vnode::bwrite(bp);
  // bp->b_vp->bwrite();
  bwrite(bp);
  dp->i_flag |= IN_CHANGE | IN_UPDATE;
  return (error);
}

static int ext2_remove(struct vnode *a_dvp,
                       struct vnode *a_vp,
                       struct componentname *a_cnp) {
  struct inode *ip;
  struct vnode *vp  = a_vp;
  struct vnode *dvp = a_dvp;
  int error;

  ip = VTOI(vp);
  if ((ip->i_din.i_flags & (IMMUTABLE | APPEND))
      || (VTOI(dvp)->i_din.i_flags & APPEND)) {
    error = -EPERM;
    goto out;
  }
  if ((error = ext2_dirremove(dvp, a_cnp)) == 0) {
    ip->i_din.i_links_count--;
    ip->i_flag |= IN_CHANGE;
  }
out:
  if (dvp == vp) {
    vnode::unreference(vp);
    // vrele(vp);
  } else {
    vp->unlock();
    vnode::unreference(vp);
    // vput(vp);
  }
  // vput(dvp);
  dvp->unlock();
  vnode::unreference(dvp);
  return (error);
}

/*
 * relookup - lookup a path name component
 *    Used by lookup to re-aquire things.
 */
int relookup(struct vnode *dvp, struct vnode **vpp, struct componentname *cnp) {
  struct vnode *dp = 0; /* the directory we are searching */
  int docache;          /* == 0 do not cache last component */
  int wantparent;       /* 1 => wantparent or lockparent flag */
  int rdonly;           /* lookup read-only flag bit */
  int error = 0;
#ifdef NAMEI_DIAGNOSTIC
  int newhash; /* DEBUG: check name hash */
  char *cp;    /* DEBUG: check name ptr/len */
#endif

  /*
   * Setup: break out flag bits into variables.
   */
  wantparent = cnp->cn_flags & (LOCKPARENT | WANTPARENT);
  docache    = (cnp->cn_flags & NOCACHE) ^ NOCACHE;
  if (cnp->cn_nameiop == DELETE || (wantparent && cnp->cn_nameiop != CREATE))
    docache = 0;
  rdonly = cnp->cn_flags & RDONLY;
  cnp->cn_flags &= ~ISSYMLINK;
  dp = dvp;
  dp->lock();

  /* dirloop: */
  /*
   * Search a new directory.
   *
   * The cn_hash value is for use by vfs_cache.
   * The last component of the filename is left accessible via
   * cnp->cn_nameptr for callers that need the name. Callers needing
   * the name set the SAVENAME flag. When done, they assume
   * responsibility for freeing the pathname buffer.
   */
#ifdef NAMEI_DIAGNOSTIC
  for (newhash = 0, cp = cnp->cn_nameptr; *cp != 0 && *cp != '/'; cp++)
    newhash += (unsigned char)*cp;
  if (newhash != cnp->cn_hash)
    panic("relookup: bad hash");
  if (cnp->cn_namelen != cp - cnp->cn_nameptr)
    panic("relookup: bad len");
  if (*cp != 0)
    panic("relookup: not last component");
  printf("{%s}: ", cnp->cn_nameptr);
#endif

  /*
   * Check for degenerate name (e.g. / or "")
   * which is a way of talking about a directory,
   * e.g. like "/." or ".".
   */
  if (cnp->cn_nameptr[0] == '\0') {
    if (cnp->cn_nameiop != LOOKUP || wantparent) {
      error = -EISDIR;
      goto bad;
    }
    if (dp->v_type != VDIR) {
      error = -ENOTDIR;
      goto bad;
    }
    if (!(cnp->cn_flags & LOCKLEAF))
      dp->unlock();
    *vpp = dp;
    if (cnp->cn_flags & SAVESTART)
      panic("lookup: SAVESTART");
    return (0);
  }

  if (cnp->cn_flags & ISDOTDOT)
    panic("relookup: lookup on dot-dot");

  /*
   * We now have a segment name to search for, and a directory to search.
   */
  if (error = dp->lookup(vpp, cnp)) {
#if DIAGNOSTIC
    if (*vpp != NULL)
      panic("leaf should be empty");
#endif
    if (error != ENOENT /*was EJUSTRETURN*/)
      goto bad;
    /*
     * If creating and at end of pathname, then can consider
     * allowing file to be created.
     */
    if (rdonly || (dvp->v_mount->mnt_flag & MNT_RDONLY)) {
      error = -EROFS;
      goto bad;
    }
    /* ASSERT(dvp == ndp->ni_startdir) */
    if (cnp->cn_flags & SAVESTART)
      vnode::refernce(dvp);
    /*
     * We return with ni_vp NULL to indicate that the entry
     * doesn't currently exist, leaving a pointer to the
     * (possibly locked) directory inode in ndp->ni_dvp.
     */
    return (0);
  }
  dp = *vpp;

#if DIAGNOSTIC
  /*
   * Check for symbolic link
   */
  if (dp->v_type == VLNK && (cnp->cn_flags & FOLLOW))
    panic("relookup: symlink found.\n");
#endif

  /*
   * Check for read-only file systems.
   */
  if (cnp->cn_nameiop == DELETE || cnp->cn_nameiop == RENAME) {
    /*
     * Disallow directory write attempts on read-only
     * file systems.
     */
    if (rdonly || (dp->v_mount->mnt_flag & MNT_RDONLY)
        || (wantparent && (dvp->v_mount->mnt_flag & MNT_RDONLY))) {
      error = -EROFS;
      goto bad2;
    }
  }
  /* ASSERT(dvp == ndp->ni_startdir) */
  if (cnp->cn_flags & SAVESTART)
    vnode::refernce(dvp);

  if (!wantparent) {
    dvp->unlock();
    vnode::unreference(dvp);
    // vrele(dvp);
  }
  if ((cnp->cn_flags & LOCKLEAF) == 0)
    dp->unlock();
  return (0);

bad2:
  if ((cnp->cn_flags & LOCKPARENT) && (cnp->cn_flags & ISLASTCN)) {
    dvp->unlock();
  }
  vnode::unreference(dvp);
bad:
  dp->unlock();
  vnode::unreference(dp);
  *vpp = NULL;
  return (error);
}

/*
 * Rename system call.
 * 	rename("foo", "bar");
 * is essentially
 *	unlink("bar");
 *	link("foo", "bar");
 *	unlink("foo");
 * but ``atomically''.  Can't do full commit without saving state in the
 * inode on disk which isn't feasible at this time.  Best we can do is
 * always guarantee the target exists.
 *
 * Basic algorithm is:
 *
 * 1) Bump link count on source while we're linking it to the
 *    target.  This also ensure the inode won't be deleted out
 *    from underneath us while we work (it may be truncated by
 *    a concurrent `trunc' or `open' for creation).
 * 2) Link source to destination.  If destination already exists,
 *    delete it first.
 * 3) Unlink source reference to inode if still around. If a
 *    directory was moved and the parent of the destination
 *    is different from the source, patch the ".." entry in the
 *    directory.
 */
static int ext2_rename(struct vnode *a_fdvp,
                       struct vnode *a_fvp,
                       struct componentname *a_fcnp,
                       struct vnode *a_tdvp,
                       struct vnode *a_tvp,
                       struct componentname *a_tcnp) {
  struct vnode *tvp          = a_tvp;
  struct vnode *tdvp         = a_tdvp;
  struct vnode *fvp          = a_fvp;
  struct vnode *fdvp         = a_fdvp;
  struct componentname *tcnp = a_tcnp;
  struct componentname *fcnp = a_fcnp;
  struct inode *ip, *xp, *dp;
  // struct dirtemplate dirbuf;
  struct timeval tv;
  int doingdirectory = 0, oldparent = 0, newparent = 0;
  int error = 0;
  char namlen;

#if DIAGNOSTIC
  if ((tcnp->cn_flags & HASBUF) == 0 || (fcnp->cn_flags & HASBUF) == 0)
    panic("ufs_rename: no name");
#endif
  /*
   * Check for cross-device rename.
   */
  if ((fvp->v_mount != tdvp->v_mount)
      || (tvp && (fvp->v_mount != tvp->v_mount))) {
    error = -EXDEV;
  abortit:
    // tdvp->abortop(tcnp); /* XXX, why not in NFS? */
    if (tdvp == tvp) {
      // vrele(tdvp);
      vnode::unreference(tdvp);
    } else {
      tdvp->unlock();
      vnode::unreference(tdvp);
      // vput(tdvp);
    }

    if (tvp) {
      tvp->unlock();
      vnode::unreference(tvp);
      // vput(tvp);
    }

    // fdvp->abortop(fcnp); /* XXX, why not in NFS? */
    // vrele(fdvp);
    vnode::unreference(fdvp);
    // vrele(fvp);
    vnode::unreference(fvp);
    return (error);
  }

  /*
   * Check if just deleting a link name.
   */
  if (tvp
      && ((VTOI(tvp)->i_din.i_flags & (IMMUTABLE | APPEND))
          || (VTOI(tdvp)->i_din.i_flags & APPEND))) {
    error = -EPERM;
    goto abortit;
  }

  if (fvp == tvp) {
    if (fvp->v_type == VDIR) {
      error = -EINVAL;
      goto abortit;
    }

    // fdvp->abortop(fcnp);

    // vrele(fdvp);
    vnode::unreference(fdvp);
    // vrele(fvp);
    vnode::unreference(fvp);
    // vput(tdvp);
    tdvp->unlock();
    vnode::unreference(tdvp);
    // vput(tvp);
    tvp->unlock();
    vnode::unreference(tvp);

    tcnp->cn_flags &= ~MODMASK;
    tcnp->cn_flags |= LOCKPARENT | LOCKLEAF;

    if ((tcnp->cn_flags & SAVESTART) == 0)
      panic("ufs_rename: lost from startdir");

    tcnp->cn_nameiop = DELETE;

    relookup(tdvp, &tvp, tcnp);
    return tdvp->remove(tvp, tcnp);
  }
  if (error = fvp->lock()) {
    goto abortit;
  }

  dp = VTOI(fdvp);
  ip = VTOI(fvp);
  if ((ip->i_din.i_flags & (IMMUTABLE | APPEND))
      || (dp->i_din.i_flags & APPEND)) {
    fvp->unlock();
    error = -EPERM;
    goto abortit;
  }

  if ((ip->i_din.i_mode & IFMT) == IFDIR) {
    /*
     * Avoid ".", "..", and aliases of "." for obvious reasons.
     */
    if ((fcnp->cn_namelen == 1 && fcnp->cn_nameptr[0] == '.') || dp == ip
        || (fcnp->cn_flags & ISDOTDOT) || (ip->i_flag & IN_RENAME)) {
      fvp->unlock();
      error = -EINVAL;
      goto abortit;
    }
    ip->i_flag |= IN_RENAME;
    oldparent = dp->i_number;
    doingdirectory++;
  }
  // vrele(fdvp);
  vnode::unreference(fdvp);
  /*
   * When the target exists, both the directory
   * and target vnodes are returned locked.
   */
  dp = VTOI(tdvp);
  xp = NULL;
  if (tvp) {
    xp = VTOI(tvp);
  }
  /*
   * 1) Bump link count while we're moving stuff
   *    around.  If we crash somewhere before
   *    completing our work, the link count
   *    may be wrong, but correctable.
   */
  ip->i_din.i_links_count++;
  ip->i_flag |= IN_CHANGE;
  get_time(&tv);
  if (error = fvp->update(&tv, &tv, 1)) {
    fvp->unlock();
    goto bad;
  }

  /*
   * If ".." must be changed (ie the directory gets a new
   * parent) then the source directory must not be in the
   * directory heirarchy above the target, as this would
   * orphan everything below the source directory. Also
   * the user must have write permission in the source so
   * as to be able to change "..". We must repeat the call
   * to namei, as the parent directory is unlocked by the
   * call to checkpath().
   */
  error = fvp->access(VWRITE, tcnp->cn_cred, tcnp->cn_proc);
  fvp->unlock();

  if (oldparent != dp->i_number) {
    newparent = dp->i_number;
  }
  if (doingdirectory && newparent) {
    if (error) /* write access check above */
      goto bad;
    if (xp != NULL) {
      // vput(tvp);
      tvp->unlock();
      vnode::unreference(tvp);
    }
    if (error = ext2_checkpath(ip, dp, tcnp->cn_cred)) {
      goto out;
    }

    if ((tcnp->cn_flags & SAVESTART) == 0)
      panic("ufs_rename: lost to startdir");
    if (error = relookup(tdvp, &tvp, tcnp)) {
      goto out;
    }

    dp = VTOI(tdvp);
    xp = NULL;
    if (tvp) {
      xp = VTOI(tvp);
    }
  }
  /*
   * 2) If target doesn't exist, link the target
   *    to the source and unlink the source.
   *    Otherwise, rewrite the target directory
   *    entry to reference the source inode and
   *    expunge the original entry's existence.
   */
  if (xp == NULL) {
    if (dp->i_dev != ip->i_dev) {
      panic("rename: EXDEV");
    }
    /*
     * Account for ".." in new directory.
     * When source and destination have the same
     * parent we don't fool with the link count.
     */
    if (doingdirectory && newparent) {
      if ((nlink_t)dp->i_din.i_links_count >= LINK_MAX) {
        error = -EMLINK;
        goto bad;
      }

      dp->i_din.i_links_count++;
      dp->i_flag |= IN_CHANGE;
      if (error = tdvp->update(&tv, &tv, 1))
        goto bad;
    }

    if (error = ext2_direnter(ip, tdvp, tcnp)) {
      if (doingdirectory && newparent) {
        dp->i_din.i_links_count--;
        dp->i_flag |= IN_CHANGE;
        tdvp->update(&tv, &tv, 1);
      }
      goto bad;
    }
    // vput(tdvp);
    tdvp->unlock();
    vnode::unreference(tdvp);
  } else {
    if (xp->i_dev != dp->i_dev || xp->i_dev != ip->i_dev)
      panic("rename: EXDEV");
    /*
     * Short circuit rename(foo, foo).
     */
    if (xp->i_number == ip->i_number)
      panic("rename: same file");
#if DAMIR
    /*
     * If the parent directory is "sticky", then the user must
     * own the parent directory, or the destination of the rename,
     * otherwise the destination may not be changed (except by
     * root). This implements append-only directories.
     */
    if ((dp->i_mode & S_ISTXT) && tcnp->cn_cred->cr_uid != 0
        && tcnp->cn_cred->cr_uid != dp->i_uid
        && xp->i_uid != tcnp->cn_cred->cr_uid) {
      error = EPERM;
      goto bad;
    }
#endif
    /*
     * Target must be empty if a directory and have no links
     * to it. Also, ensure source and target are compatible
     * (both directories, or both not directories).
     */
    if ((xp->i_din.i_mode & IFMT) == IFDIR) {
      if (!ext2_dirempty(xp, dp->i_number, tcnp->cn_cred)
          || xp->i_din.i_links_count > 2) {
        error = -ENOTEMPTY;
        goto bad;
      }
      if (!doingdirectory) {
        error = -ENOTDIR;
        goto bad;
      }

      vfs_cache_purge(tdvp);
    } else if (doingdirectory) {
      error = -EISDIR;
      goto bad;
    }

    if (error = ext2_dirrewrite(dp, ip, tcnp))
      goto bad;
    /*
     * If the target directory is in the same
     * directory as the source directory,
     * decrement the link count on the parent
     * of the target directory.
     */
    if (doingdirectory && !newparent) {
      dp->i_din.i_links_count--;
      dp->i_flag |= IN_CHANGE;
    }
    // vput(tdvp);
    tdvp->unlock();
    vnode::unreference(tdvp);

    /*
     * Adjust the link count of the target to
     * reflect the dirrewrite above.  If this is
     * a directory it is empty and there are
     * no links to it, so we can squash the inode and
     * any space associated with it.  We disallowed
     * renaming over top of a directory with links to
     * it above, as the remaining link would point to
     * a directory without "." or ".." entries.
     */
    xp->i_din.i_links_count--;
    if (doingdirectory) {
      if (--xp->i_din.i_links_count != 0)
        panic("rename: linked directory");
      error = tvp->truncate((off_t)0, IO_SYNC, tcnp->cn_cred, tcnp->cn_proc);
    }
    xp->i_flag |= IN_CHANGE;
    // vput(tvp);
    tvp->unlock();
    vnode::unreference(tvp);
    xp = NULL;
  }

  /*
   * 3) Unlink the source.
   */
  fcnp->cn_flags &= ~MODMASK;
  fcnp->cn_flags |= LOCKPARENT | LOCKLEAF;
  if ((fcnp->cn_flags & SAVESTART) == 0)
    panic("ufs_rename: lost from startdir");

  relookup(fdvp, &fvp, fcnp);

  if (fvp != NULL) {
    xp = VTOI(fvp);
    dp = VTOI(fdvp);
  } else {
    /*
     * From name has disappeared.
     */
    if (doingdirectory)
      panic("rename: lost dir entry");
    // vrele(a_fvp);
    vnode::unreference(a_fvp);
    return (0);
  }
  /*
   * Ensure that the directory entry still exists and has not
   * changed while the new name has been entered. If the source is
   * a file then the entry may have been unlinked or renamed. In
   * either case there is no further work to be done. If the source
   * is a directory then it cannot have been rmdir'ed; its link
   * count of three would cause a rmdir to fail with ENOTEMPTY.
   * The IRENAME flag ensures that it cannot be moved by another
   * rename.
   */
  if (xp != ip) {
    if (doingdirectory)
      panic("rename: lost dir entry");
  } else {
    panic("Not ready");
#if DAMIR
    /*
     * If the source is a directory with a
     * new parent, the link count of the old
     * parent directory must be decremented
     * and ".." set to point to the new parent.
     */
    if (doingdirectory && newparent) {
      dp->i_din.i_links_count--;
      dp->i_flag |= IN_CHANGE;
      error = vn_rdwr(UIO_READ,
                      fvp,
                      (char *)&dirbuf,
                      sizeof(struct dirtemplate),
                      (off_t)0,
                      UIO_SYSSPACE,
                      IO_NODELOCKED,
                      tcnp->cn_cred,
                      (int *)0,
                      (struct proc *)0);
      if (error == 0) {
        if (fvp->v_mount->mnt_maxsymlinklen <= 0)
          namlen = dirbuf.dotdot_type;
        else
          namlen = dirbuf.dotdot_namlen;

        namlen = ((struct odirtemplate *)&dirbuf)->dotdot_namlen;

        if (namlen != 2 || dirbuf.dotdot_name[0] != '.'
            || dirbuf.dotdot_name[1] != '.') {
          ufs_dirbad(xp, (doff_t)12, "rename: mangled dir");
        } else {
          dirbuf.dotdot_ino = newparent;
          (void)vn_rdwr(UIO_WRITE,
                        fvp,
                        (char *)&dirbuf,
                        sizeof(struct dirtemplate),
                        (off_t)0,
                        UIO_SYSSPACE,
                        IO_NODELOCKED | IO_SYNC,
                        tcnp->cn_cred,
                        (int *)0,
                        (struct proc *)0);
          vfs_cache_purge(fdvp);
        }
      }
    }
#endif
    error = ext2_dirremove(fdvp, fcnp);
    if (!error) {
      xp->i_din.i_links_count--;
      xp->i_flag |= IN_CHANGE;
    }
    xp->i_flag &= ~IN_RENAME;
  }
  if (dp) {
    // vput(fdvp);
    fdvp->unlock();
    vnode::unreference(fdvp);
  }

  if (xp) {
    xp->unlock();
    vnode::unreference(xp);
    // vput(fvp);
  }

  // vrele(a_fvp);
  vnode::unreference(a_fvp);
  return (error);

bad:
  if (xp) {
    // vput(ITOV(xp));
    xp->unlock();
    vnode::unreference(xp);
  }

  // vput(ITOV(dp));
  dp->unlock();
  vnode::unreference(dp);
out:
  if (fvp->lock() == 0) {
    ip->i_din.i_links_count--;
    ip->i_flag |= IN_CHANGE;
    // vput(fvp);
    fvp->unlock();
    vnode::unreference(fvp);
  } else {
    // vrele(fvp);
    vnode::unreference(fvp);
  }
  return (error);
}

/*
 * symlink -- make a symbolic link
 */
static int ext2_symlink(struct vnode *a_dvp,
                        struct vnode **a_vpp,
                        struct componentname *a_cnp,
                        struct vattr *a_vap,
                        char *a_target) {
  struct vnode *vp, **vpp = a_vpp;
  struct inode *ip;
  int len, error;

  if (error = ufs_makeinode(IFLNK | a_vap->va_mode, a_dvp, vpp, a_cnp))
    return (error);
  vp  = *vpp;
  len = strlen(a_target);
  if (len < vp->v_mount->mnt_maxsymlinklen) {
    ip = VTOI(vp);
    bcopy(a_target, ip->i_din.i_block, len);
    ip->i_din.i_size = len;
    ip->i_flag |= IN_CHANGE | IN_UPDATE;
  } else
    error = vn_rdwr(UIO_WRITE,
                    vp,
                    a_target,
                    len,
                    (off_t)0,
                    UIO_SYSSPACE,
                    IO_NODELOCKED,
                    a_cnp->cn_cred,
                    (int *)0,
                    (struct proc *)0);
  // vput(vp);
  vp->unlock();
  vnode::unreference(vp);
  return (error);
}

/*
 * Mmap a file
 *
 * NB Currently unsupported.
 */
/* ARGSUSED */
static int ext2_mmap(struct vnode *a_vp,
                     int a_fflags,
                     struct ucred *a_cred,
                     struct proc *a_p) {
  return -EINVAL;
}

/*
 * Seek on a file
 *
 * Nothing to do, so just return.
 */
/* ARGSUSED */
static int ext2_seek(struct vnode *a_vp,
                     off_t a_oldoff,
                     off_t a_newoff,
                     struct ucred *a_cred) {
  return (0);
}

/* ARGSUSED */
static int ext2_ioctl(struct vnode *a_vp,
                      int a_command,
                      char *a_data,
                      int a_fflag,
                      struct ucred *a_cred,
                      struct proc *a_p) {
  return -ENOTTY;
}

/* ARGSUSED */
static int ext2_select(struct vnode *a_vp,
                       int a_which,
                       int a_fflags,
                       struct ucred *a_cred,
                       struct proc *a_p) {
  /*
   * We should really check to see if I/O is possible.
   */
  return (1);
}

/*
 * link vnode call
 */
int ext2_link(struct vnode *a_vp,
              struct vnode *a_tdvp,
              struct componentname *a_cnp) {
  struct vnode *vp          = a_vp;
  struct vnode *tdvp        = a_tdvp;
  struct componentname *cnp = a_cnp;
  struct inode *ip;
  struct timeval tv;
  int error;

#if DIAGNOSTIC
  if ((cnp->cn_flags & HASBUF) == 0)
    panic("ufs_link: no name");
#endif
  if (vp->v_mount != tdvp->v_mount) {
    // vp->abortop(cnp);
    error = -EXDEV;
    goto out2;
  }
  if (vp != tdvp && (error = tdvp->lock())) {
    // vp->abortop(cnp);
    goto out2;
  }

  ip = VTOI(tdvp);

  if ((nlink_t)ip->i_din.i_links_count >= LINK_MAX) {
    // vp->abortop(cnp);
    error = -EMLINK;
    goto out1;
  }

  if (ip->i_din.i_flags & (IMMUTABLE | APPEND)) {
    // vp->abortop(cnp);
    error = -EPERM;
    goto out1;
  }

  ip->i_din.i_links_count++;
  ip->i_flag |= IN_CHANGE;
  get_time(&tv);
  error = tdvp->update(&tv, &tv, 1);
  if (!error)
    error = ext2_direnter(ip, vp, cnp);

  if (error) {
    ip->i_din.i_links_count--;
    ip->i_flag |= IN_CHANGE;
  }
#ifdef DAMIR
  FREE(cnp->cn_pnbuf, M_NAMEI);
#else
  free(cnp->cn_pnbuf);
#endif
out1:
  if (vp != tdvp)
    tdvp->unlock();
out2:
  // vput(vp);
  vp->unlock();
  vnode::unreference(vp);
  return (error);
}

/*
 * Return POSIX pathconf information applicable to ufs filesystems.
 */
static int ext2_pathconf(struct vnode *a_vp, int a_name, int *a_retval) {
#if DAMIR
  switch (a_name) {
    case _PC_LINK_MAX:
      *ap->a_retval = LINK_MAX;
      return (0);
    case _PC_NAME_MAX:
      *ap->a_retval = NAME_MAX;
      return (0);
    case _PC_PATH_MAX:
      *ap->a_retval = PATH_MAX;
      return (0);
    case _PC_PIPE_BUF:
      *ap->a_retval = PIPE_BUF;
      return (0);
    case _PC_CHOWN_RESTRICTED:
      *ap->a_retval = 1;
      return (0);
    case _PC_NO_TRUNC:
      *ap->a_retval = 1;
      return (0);
    default:
      return -EINVAL;
  }
/* NOTREACHED */
#endif
  return -EINVAL;
}

/*
 * Advisory record locking support
 */
static int ext2_advlock(
    struct vnode *a_vp, char *a_id, int a_op, struct flock *a_fl, int a_flags) {
  struct inode *ip = VTOI(a_vp);
  struct flock *fl = a_fl;
  struct lockf *lock;
  off_t start, end;
  int error;

  /*
   * Avoid the common case of unlocking when inode has no locks.
   */
  if (ip->i_lockf == (struct lockf *)0) {
    if (a_op != F_SETLK) {
      fl->l_type = F_UNLCK;
      return (0);
    }
  }
  /*
   * Convert the flock structure into a start and end.
   */
  switch (fl->l_whence) {
    case SEEK_SET:
    case SEEK_CUR:
      /*
       * Caller is responsible for adding any necessary offset
       * when SEEK_CUR is used.
       */
      start = fl->l_start;
      break;

    case SEEK_END:
      start = ip->i_din.i_size + fl->l_start;
      break;

    default:
      return -EINVAL;
  }
  if (start < 0)
    return -EINVAL;
  if (fl->l_len == 0)
    end = -1;
  else
    end = start + fl->l_len - 1;
/*
 * Create the lockf structure
 */
#if DAMIR
  MALLOC(lock, struct lockf *, sizeof *lock, M_LOCKF, M_WAITOK);
#else
  panic("not ready");
  // lock = (struct lockf *)malloc(sizeof(struct lockf));
#endif
  lock->lf_start = start;
  lock->lf_end   = end;
  lock->lf_id    = a_id;
  lock->lf_inode = ip;
  lock->lf_type  = fl->l_type;
  lock->lf_next  = (struct lockf *)0;
  lock->lf_block = (struct lockf *)0;
  lock->lf_flags = a_flags;
  /*
   * Do the requested operation.
   */
  switch (a_op) {
    case F_SETLK:
      //      return (lf_setlock(lock));

    case F_UNLCK:
      // error = lf_clearlock(lock);
#ifdef DAMIR
      FREE(lock, M_LOCKF);
#else
      free(lock);
#endif
      return (error);

    case F_GETLK:
      // error = lf_getlock(lock, fl);
#ifdef DAMIR
      FREE(lock, M_LOCKF);
#else
      free(lock);
#endif
      return (error);

    default:
#ifdef DAMIR
      free(lock, M_LOCKF);
#else
      free(lock);
#endif
      return -EINVAL;
  }
  /* NOTREACHED */
}

static int ext2_print(struct vnode *a_vp) { return 0; }

/* Global vfs data structures for ufs. */
vnodeops_t ext2_vnodeop = {
    .open     = ext2_open,
    .close    = ext2_close,
    .read     = ext2_read,
    .write    = ext2_write,
    .ioctl    = ext2_ioctl,
    .getattr  = ext2_getattr,
    .setattr  = ext2_setattr,
    .access   = ext2_access,
    .lookup   = ext2_lookup,
    .create   = ext2_create,
    .remove   = ext2_remove,
    .rename   = ext2_rename,
    .mkdir    = ext2_mkdir,
    .rmdir    = ext2_rmdir,
    .readdir  = ext2_readdir,
    .symlink  = ext2_symlink,
    .readlink = ext2_readlink,
    .fsync    = ext2_fsync,
    .inactive = ext2_inactive,
    .seek     = ext2_seek,
    .mknod    = ext2_mknod,
    .select   = ext2_select,
    .mmap     = ext2_mmap,
    .abortop  = ext2_abortop,
    .reclaim  = ext2_reclaim,
    .lock     = ext2_lock,
    .unlock   = ext2_unlock,
    .bmap     = ext2_bmap,
    .strategy = ext2_strategy,
    .print    = ext2_print,
    .islocked = ext2_islocked,
    .pathconf = ext2_pathconf,
    .advlock  = ext2_advlock,
    .blkatoff = ext2_blkatoff,
    .valloc   = ext2_valloc,
    .vfree    = ext2_vfree,
    .truncate = ext2_truncate,
    .update   = ext2_update,
    .bwrite   = vn_bwrite,
};
