#pragma once
/*
 *  modified for EXT2FS support in Lites 1.1
 *
 *  Aug 1995, Godmar Back (gback@cs.utah.edu)
 *  University of Utah, Department of Computer Science
 */
/*-
 * Copyright (c) 1991, 1993, 1994
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)ffs_extern.h	8.3 (Berkeley) 4/16/94
 */

struct buf;
struct fid;
struct fs;
struct inode;
struct mount;
struct nameidata;
struct proc;
struct statfs;
struct timeval;
struct ucred;
struct uio;
struct vnode;
struct mbuf;
struct dinode;
struct ext2_group_desc;
struct ext2_inode;
struct ext2mount;

struct cluster_save;

#ifdef __cplusplus
extern "C" {
#endif

int ext2_blkatoff(struct vnode *a_vp,
                  off_t a_offset,
                  char **a_res,
                  struct buf **a_bpp);
void ext2_blkfree(struct inode *, size_t, long);

int ext2_fsync(struct vnode *a_vp,
               struct ucred *a_cred,
               int a_waitfor,
               struct proc *a_p);

int ext2_read(struct vnode *a_vp,
              struct uio *a_uio,
              int a_ioflag,
              struct ucred *a_cred);
int ext2_reallocblks(struct vnode *a_vp, struct cluster_save *a_buflist);
int ext2_reclaim(struct vnode *a_vp);
int ext2_truncate(struct vnode *a_vp,
                  off_t a_length,
                  int a_flags,
                  struct ucred *a_cred,
                  struct proc *a_p);
int ext2_update(struct vnode *a_vp,
                struct timeval *a_access,
                struct timeval *a_modify,
                int a_waitfor);
int ext2_valloc(struct vnode *a_pvp,
                int a_mode,
                struct ucred *a_cred,
                struct vnode **a_vpp);
int ext2_vfree(struct vnode *a_pvp, ino_t a_ino, int a_mode);
int ext2_vptofh(struct vnode *, struct fid *);
int ext2_write(struct vnode *a_vp,
               struct uio *a_uio,
               int a_ioflag,
               struct ucred *a_cred);
int ext2_lookup(struct vnode *a_dvp,
                struct vnode **a_vpp,
                struct componentname *a_cnp);
int ext2_readdir(struct vnode *a_vp,
                 struct uio *a_uio,
                 struct ucred *a_cred,
                 int *a_eofflag,
                 u32 *a_cookies,
                 int a_ncookies);
void ext2_print_dinode(struct dinode *);
void ext2_print_inode(struct inode *);
int ext2_direnter(struct inode *, struct vnode *, struct componentname *);
int ext2_dirremove(struct vnode *, struct componentname *);
int ext2_dirrewrite(struct inode *, struct inode *, struct componentname *);
int ext2_dirempty(struct inode *, ino_t, struct ucred *);
int ext2_checkpath(struct inode *, struct inode *, struct ucred *);
struct ext2_group_desc *get_group_desc(struct mount *,
                                       unsigned int,
                                       struct buf **);
void ext2_discard_prealloc(struct inode *);
int ext2_inactive(struct vnode *a_vp);
int ll_w_block(struct buf *, int);

void ext2_free_blocks(struct mount *mp,
                      unsigned long block,
                      unsigned long count);

int ext2_new_block(struct mount *mp,
                   unsigned long goal,
                   u32 *prealloc_count,
                   u32 *prealloc_block);

void mark_buffer_dirty(struct buf *bh);

ino_t ext2_new_inode(const struct inode *dir, int mode);

void ext2_free_inode(struct inode *inode);

int get_time(struct timeval *time);  // A stub

#ifdef __cplusplus
}
#endif

/* this macros allows some of the ufs code to distinguish between
 * an EXT2 and a non-ext2(FFS/LFS) vnode.
 */
#define IS_EXT2_VNODE(vp) (vp->v_mount->mnt_stat.f_type == MOUNT_EXT2FS)

#ifdef DIAGNOSTIC
void ext2_checkoverlap(struct buf *, struct inode *);
#endif

extern vnodeops_t ext2_vnodeop;
extern vnodeops_t ext2_specop;

// extern int (**ext2_specop_p)();
#ifdef FIFO
#define EXT2_FIFOOPS ext2_fifoop_p
#else
#define EXT2_FIFOOPS NULL
#endif
