#pragma once
#define __USE_MISC
#include <sys/stat.h>
#undef __USE_MISC

#define ACCESSPERMS (S_IRWXU | S_IRWXG | S_IRWXO)                               /* 0777 */
#define ALLPERMS    (S_ISUID | S_ISGID | S_ISVTX | S_IRWXU | S_IRWXG | S_IRWXO) /* 07777 */

#if 0
/* Machine specific */

/*Definitions of flags stored in file flags word.**Super -user and owner
changeable flags.*/

#define UF_SETTABLE  0x0000ffff /* mask of owner changeable flags */
#define UF_NODUMP    0x00000001 /* do not dump file */
#define UF_IMMUTABLE 0x00000002 /* file may not be changed */
#define UF_APPEND    0x00000004 /* writes to file may only append */

/*
 * Super-user changeable flags.
 */
#define SF_SETTABLE  0xffff0000 /* mask of superuser changeable flags */
#define SF_ARCHIVED  0x00010000 /* file is archived */
#define SF_IMMUTABLE 0x00020000 /* file may not be changed */
#define SF_APPEND    0x00040000 /* writes to file may only append */
/*
 * Shorthand abbreviations of above.
 */
#define APPEND       (UF_APPEND | SF_APPEND)
#define IMMUTABLE    (UF_IMMUTABLE | SF_IMMUTABLE)
#endif
typedef struct stat fstat_t;