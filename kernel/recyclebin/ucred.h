#pragma once
#include <mach/syslimits.h>
/*
 * One structure allocated per active
 * process. It contains all data needed
 * about the process while the
 * process may be swapped out.
 * Other per process data (user.h)
 * is swapped with the process.
 */
#define NOCRED ((struct ucred *)-1)
#define FSCRED ((struct ucred *)-2) /* filesystem credential */

typedef struct ucred *ucred_t;

/*
 * Shareable process credentials (always resident).  This includes a reference
 * to the current user credentials as well as real and saved ids that may be
 * used to change ids.
 */
struct pcred
{
        ucred_t pc_ucred; /* Current credentials. */
        uid_t p_ruid;     /* Real user id. */
        uid_t p_svuid;    /* Saved effective user id. */
        gid_t p_rgid;     /* Real group id. */
        gid_t p_svgid;    /* Saved effective group id. */
        size_t p_refcnt;  /* Number of references. */
};

struct ucred
{
        u32 cr_ref;
        uid_t cr_uid;
        size_t cr_ngroups;        /* number of groups */
        gid_t cr_groups[NGROUPS]; /* groups */
};

int suser(struct ucred *cred, integer_t *acflag);

/*
 * Copy cred structure to a new one and free the old one.
 */
struct ucred *crcopy(struct ucred *cr);

/*
 * Allocate a zeroed cred structure.
 */
struct ucred *crget();

/*
 * Free a cred structure.
 * Throws away space when ref count gets to 0.
 */
void crfree(struct ucred *cr);

static inline void crhold(struct ucred *cr) { (cr)->cr_ref++; }

int groupmember(gid_t gid, struct ucred *cred);
