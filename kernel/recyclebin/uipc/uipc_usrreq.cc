/*
 * Copyright (c) 1982, 1986, 1989, 1991, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)uipc_usrreq.c	8.9 (Berkeley) 5/14/95
 */
#include "mbuf/mbuf.h"
#include <mach/file.h>
#include <mach/filedesc.h>
#include <mach/param.h>

#include <mach/sys/domain.h>
#include <mach/sys/protosw.h>
#include <mach/vnode.h>
#include <malloc.h>
#undef _GNU_SOURCE
#include <sys/socket.h>
#define _GNU_SOURCE
//#include <sys/socketvar.h>
#include "un.h"
#include <fcntl.h>
#include <mach/errno.h>
#include <mach/socketvar.h>
#include <mach/stat.h>
#include <mach/sys/unpcb.h>
#include <pthread.h>
#include <strings.h>

/*Forward*/
static int unp_internalize(struct mbuf *control, pthread_t p);

/*
 * Unix communications domain.
 *
 * TODO:
 *	SEQPACKET, RDM
 *	rethink name space problems
 *	need a proper out-of-band
 */
struct sockaddr sun_noname = {sizeof(sun_noname), AF_UNIX};
ino_t unp_ino; /* prototype for fake inode numbers */

/*ARGSUSED*/
int uipc_usrreq(struct socket *so, int req, struct mbuf *m, struct mbuf *nam, struct mbuf *control)
{
        struct unpcb *unp = sotounpcb(so);
        struct socket *so2;
        int error = 0;

        if (req == PRU_CONTROL)
                return -EOPNOTSUPP;
        if (req != PRU_SEND && control && control->m_len)
        {
                error = -EOPNOTSUPP;
                goto release;
        }
        if (unp == 0 && req != PRU_ATTACH)
        {
                error = -EINVAL;
                goto release;
        }
        switch (req)
        {
        case PRU_ATTACH:
                if (unp)
                {
                        error = -EISCONN;
                        break;
                }
                error = unp_attach(so);
                break;

        case PRU_DETACH:
                unp_detach(unp);
                break;

        case PRU_BIND:
                error = unp_bind(unp, nam, /*pthread_self()*/ nullptr);
                break;

        case PRU_LISTEN:
                if (unp->unp_vnode == 0)
                        error = -EINVAL;
                break;

        case PRU_CONNECT:
                error = unp_connect(so, nam, /*pthread_self()*/ nullptr);
                break;

        case PRU_CONNECT2:
                error = unp_connect2(so, (struct socket *)nam);
                break;

        case PRU_DISCONNECT:
                unp_disconnect(unp);
                break;

        case PRU_ACCEPT:
                /*
                 * Pass back name of connected socket,
                 * if it was bound and we are still connected
                 * (our peer may have closed already!).
                 */
                if (unp->unp_conn && unp->unp_conn->unp_addr)
                {
                        nam->m_len = unp->unp_conn->unp_addr->m_len;
                        bcopy(mtod(unp->unp_conn->unp_addr, void *), mtod(nam, void *), (unsigned)nam->m_len);
                }
                else
                {
                        nam->m_len                      = sizeof(sun_noname);
                        *(mtod(nam, struct sockaddr *)) = sun_noname;
                }
                break;

        case PRU_SHUTDOWN:
                socantsendmore(so);
                unp_shutdown(unp);
                break;

        case PRU_RCVD:
                switch (so->so_type)
                {
                case SOCK_DGRAM:
                        panic("uipc 1");
                        /*NOTREACHED*/

                case SOCK_STREAM:
#define rcv (&so->so_rcv)
#define snd (&so2->so_snd)
                        if (unp->unp_conn == 0)
                                break;
                        so2 = unp->unp_conn->unp_socket;
                        /*
                         * Adjust backpressure on sender
                         * and wakeup any waiting to write.
                         */
                        snd->sb_mbmax += unp->unp_mbcnt - rcv->sb_mbcnt;
                        unp->unp_mbcnt = rcv->sb_mbcnt;
                        snd->sb_hiwat += unp->unp_cc - rcv->sb_cc;
                        unp->unp_cc = rcv->sb_cc;
                        sowwakeup(so2);
#undef snd
#undef rcv
                        break;

                default:
                        panic("uipc 2");
                }
                break;

        case PRU_SEND:
                if (control && (error = unp_internalize(control, /*pthread_self()*/ nullptr)))
                        break;
                switch (so->so_type)
                {
                case SOCK_DGRAM: {
                        struct sockaddr *from;

                        if (nam)
                        {
                                if (unp->unp_conn)
                                {
                                        error = -EISCONN;
                                        break;
                                }
                                error = unp_connect(so, nam, /*pthread_self()*/ nullptr);
                                if (error)
                                        break;
                        }
                        else
                        {
                                if (unp->unp_conn == 0)
                                {
                                        error = -ENOTCONN;
                                        break;
                                }
                        }

                        so2 = unp->unp_conn->unp_socket;
                        if (unp->unp_addr)
                                from = mtod(unp->unp_addr, struct sockaddr *);
                        else
                                from = &sun_noname;
                        if (sbappendaddr(&so2->so_rcv, from, m, control))
                        {
                                sorwakeup(so2);
                                m       = 0;
                                control = 0;
                        }
                        else
                                error = -ENOBUFS;
                        if (nam)
                                unp_disconnect(unp);
                        break;
                }

                case SOCK_STREAM:
#define rcv (&so2->so_rcv)
#define snd (&so->so_snd)
                        if (so->so_state & SS_CANTSENDMORE)
                        {
                                error = -EPIPE;
                                break;
                        }
                        if (unp->unp_conn == 0)
                                panic("uipc 3");
                        so2 = unp->unp_conn->unp_socket;
                        /*
                         * Send to paired receive port, and then reduce
                         * send buffer hiwater marks to maintain backpressure.
                         * Wake up readers.
                         */
                        if (control)
                        {
                                if (sbappendcontrol(rcv, m, control))
                                        control = 0;
                        }
                        else
                                sbappend(rcv, m);
                        snd->sb_mbmax -= rcv->sb_mbcnt - unp->unp_conn->unp_mbcnt;
                        unp->unp_conn->unp_mbcnt = rcv->sb_mbcnt;
                        snd->sb_hiwat -= rcv->sb_cc - unp->unp_conn->unp_cc;
                        unp->unp_conn->unp_cc = rcv->sb_cc;
                        sorwakeup(so2);
                        m = 0;
#undef snd
#undef rcv
                        break;

                default:
                        panic("uipc 4");
                }
                break;

        case PRU_ABORT:
                panic("NOREADY");
                // DAMIR unp_drop(unp, ECONNABORTED);
                break;

        case PRU_SENSE:
                ((struct stat *)m)->st_blksize = so->so_snd.sb_hiwat;
                if (so->so_type == SOCK_STREAM && unp->unp_conn != 0)
                {
                        so2 = unp->unp_conn->unp_socket;
                        ((struct stat *)m)->st_blksize += so2->so_rcv.sb_cc;
                }
                ((struct stat *)m)->st_dev = NODEV;
                if (unp->unp_ino == 0)
                {
                        unp->unp_ino = unp_ino++;
                }
                ((struct stat *)m)->st_ino = unp->unp_ino;
                return (0);

        case PRU_RCVOOB:
                return -EOPNOTSUPP;

        case PRU_SENDOOB:
                error = -EOPNOTSUPP;
                break;

        case PRU_SOCKADDR:
                if (unp->unp_addr)
                {
                        nam->m_len = unp->unp_addr->m_len;
                        bcopy(mtod(unp->unp_addr, void *), mtod(nam, void *), (unsigned)nam->m_len);
                }
                else
                        nam->m_len = 0;
                break;

        case PRU_PEERADDR:
                if (unp->unp_conn && unp->unp_conn->unp_addr)
                {
                        nam->m_len = unp->unp_conn->unp_addr->m_len;
                        bcopy(mtod(unp->unp_conn->unp_addr, void *), mtod(nam, void *), (unsigned)nam->m_len);
                }
                else
                        nam->m_len = 0;
                break;

        case PRU_SLOWTIMO:
                break;

        default:
                panic("piusrreq");
        }
release:
        if (control)
                m_freem(control);
        if (m)
                m_freem(m);
        return (error);
}

/*
 * Both send and receive buffers are allocated PIPSIZ bytes of buffering
 * for stream sockets, although the total for sender and receiver is
 * actually only PIPSIZ.
 * Datagram sockets really use the sendspace as the maximum datagram size,
 * and don't really want to reserve the sendspace.  Their recvspace should
 * be large enough for at least one max-size datagram plus address.
 */
#define PIPSIZ 4096
size_t unpst_sendspace = PIPSIZ;
size_t unpst_recvspace = PIPSIZ;
size_t unpdg_sendspace = 2 * 1024; /* really max datagram size */
size_t unpdg_recvspace = 4 * 1024;

int unp_rights; /* file descriptors in flight */

int unp_attach(struct socket *so)
{
        struct mbuf *m;
        struct unpcb *unp;
        int error;

        if (so->so_snd.sb_hiwat == 0 || so->so_rcv.sb_hiwat == 0)
        {
                switch (so->so_type)
                {
                case SOCK_STREAM:
                        error = soreserve(so, unpst_sendspace, unpst_recvspace);
                        break;

                case SOCK_DGRAM:
                        error = soreserve(so, unpdg_sendspace, unpdg_recvspace);
                        break;

                default:
                        error = -1;
                        panic("unp_attach");
                }
                if (error)
                        return (error);
        }
        m = m_getclr(M_DONTWAIT, MT_PCB);
        if (m == NULL)
                return -ENOBUFS;
        unp             = mtod(m, struct unpcb *);
        so->so_pcb      = unp;
        unp->unp_socket = so;
        return (0);
}

void unp_detach(struct unpcb *unp)
{
        if (unp->unp_vnode)
        {
                // unp->unp_vnode->v_un.vu_socket = 0;
                unp->unp_vnode->unreference();
                unp->unp_vnode = 0;
        }
        if (unp->unp_conn)
        {
                unp_disconnect(unp);
        }
        while (unp->unp_refs)
        {
                panic("NOTREADY");
                // unp_drop(unp->unp_refs, ECONNRESET);
        }

        soisdisconnected(unp->unp_socket);

        unp->unp_socket->so_pcb = 0;

        m_freem(unp->unp_addr);

        mbuf_free(dtom(unp));

        if (unp_rights)
        {
                /*
                 * Normally the receive buffer is flushed later,
                 * in sofree, but if our receive buffer holds references
                 * to descriptors that are now garbage, we will dispose
                 * of those descriptor references after the garbage collector
                 * gets them (resulting in a "panic: closef: count < 0").
                 */
                sorflush(unp->unp_socket);

                unp_gc();
        }
}

int unp_bind(struct unpcb *unp, struct mbuf *nam, pthread_t p)
{
        panic("Uncommented because of namei has been removed");
#if 0
        struct sockaddr_un *soun = mtod(nam, struct sockaddr_un *);
        struct vnode *vp;
        vnode::attributes vattr;
        int error;
        struct nameidata nd;

        NDINIT(&nd, CREATE, FOLLOW | LOCKPARENT, UIO_SYSSPACE, soun->sun_path, p);
        if (unp->unp_vnode != NULL)
                return (-EINVAL);
        if (nam->m_len == MLEN)
        {
                if (*((char *)(mtod(nam, vm_offset_t) + nam->m_len - 1)) != 0)
                        return (-EINVAL);
        }
        else
                *((char *)(mtod(nam, vm_offset_t) + nam->m_len)) = 0;
        /* SHOULD BE ABLE TO ADOPT EXISTING AND wakeup() ALA FIFO's */
        if (error = namei(&nd))
                return (error);
        vp = nd.ni_vp;
        if (vp != NULL)
        {
                // nd.ni_dvp->abortop(&nd.ni_cnd);
                free(nd.ni_cnd.cn_pnbuf);
                if (nd.ni_dvp == vp)
                        vnode::unreference(nd.ni_dvp);
                else
                {
                        nd.ni_dvp->unlock();
                        vnode::unreference(nd.ni_dvp);
                }
                vnode::unreference(vp);
                return -EADDRINUSE;
        }
        vattr.va_type = VSOCK;
        // vattr.va_mode = ACCESSPERMS;
        // VOP_LEASE(nd.ni_dvp, p, p->p_cred, LEASE_WRITE);
        panic("Use new create");
#if 0
        if (error = nd.ni_dvp->create(&nd.ni_vp, &nd.ni_cnd, &vattr))
        {
                return (error);
        }
#endif
        vp                 = nd.ni_vp;
        vp->v_un.vu_socket = unp->unp_socket;
        unp->unp_vnode     = vp;
        unp->unp_addr      = m_copy(nam, 0, (int)M_COPYALL);
        vp->unlock(/*, 0, p*/);
#endif
        return (0);
}

int unp_connect(struct socket *so, struct mbuf *nam, pthread_t p)
{
        panic("Uncommented because of namei");
#if 0
        struct sockaddr_un *soun = mtod(nam, struct sockaddr_un *);
        struct vnode *vp;
        struct socket *so2, *so3;
        struct unpcb *unp2, *unp3;
        int error;
        struct nameidata nd;

        NDINIT(&nd, LOOKUP, FOLLOW | LOCKLEAF, UIO_SYSSPACE, soun->sun_path, p);
        if (((vm_offset_t)nam->m_data) + nam->m_len == ((vm_offset_t)&nam->m_dat[MLEN]))
        { /* XXX */
                if (*(mtod(nam, char *) + nam->m_len - 1) != 0)
                {
                        return (-EMSGSIZE);
                }
        }
        else
        {
                *(mtod(nam, char *) + nam->m_len) = 0;
        }

        if (error = namei(&nd))
        {
                return (error);
        }
        vp = nd.ni_vp;
        if (vp->v_type != VSOCK)
        {
                error = -ENOTSOCK;
                goto bad;
        }

        if (error = vp->access(VWRITE, p->p_cred->pc_ucred, p))
        {
                goto bad;
        }

        so2 = vp->v_un.vu_socket;

        if (so2 == 0)
        {
                error = -ECONNREFUSED;
                goto bad;
        }
        if (so->so_type != so2->so_type)
        {
                error = -EPROTOTYPE;
                goto bad;
        }
        if (so->so_proto->pr_flags & PR_CONNREQUIRED)
        {
                if ((so2->so_options & SO_ACCEPTCONN) == 0 || (so3 = sonewconn(so2, 0)) == 0)
                {
                        error = -ECONNREFUSED;
                        goto bad;
                }
                unp2 = sotounpcb(so2);
                unp3 = sotounpcb(so3);
                if (unp2->unp_addr)
                        unp3->unp_addr = m_copy(unp2->unp_addr, 0, (int)M_COPYALL);
                so2 = so3;
        }
        error = unp_connect2(so, so2);
bad:
        vp->unlock();
        vnode::unreference(vp);

        return (error);
#endif
        return KERN_SUCCESS;
}

int unp_connect2(struct socket *so, struct socket *so2)
{
        struct unpcb *unp = sotounpcb(so);
        struct unpcb *unp2;

        if (so2->so_type != so->so_type)
                return -EPROTOTYPE;
        unp2          = sotounpcb(so2);
        unp->unp_conn = unp2;
        switch (so->so_type)
        {
        case SOCK_DGRAM:
                unp->unp_nextref = unp2->unp_refs;
                unp2->unp_refs   = unp;
                soisconnected(so);
                break;

        case SOCK_STREAM:
                unp2->unp_conn = unp;
                soisconnected(so);
                soisconnected(so2);
                break;

        default:
                panic("unp_connect2");
        }
        return (0);
}

void unp_disconnect(struct unpcb *unp)
{
        struct unpcb *unp2 = unp->unp_conn;

        if (unp2 == 0)
                return;
        unp->unp_conn = 0;
        switch (unp->unp_socket->so_type)
        {
        case SOCK_DGRAM:
                if (unp2->unp_refs == unp)
                {
                        unp2->unp_refs = unp->unp_nextref;
                }
                else
                {
                        unp2 = unp2->unp_refs;
                        for (;;)
                        {
                                if (unp2 == 0)
                                {
                                        panic("unp_disconnect");
                                }

                                if (unp2->unp_nextref == unp)
                                {
                                        break;
                                }

                                unp2 = unp2->unp_nextref;
                        }
                        unp2->unp_nextref = unp->unp_nextref;
                }
                unp->unp_nextref = 0;
                unp->unp_socket->so_state &= ~SS_ISCONNECTED;
                break;

        case SOCK_STREAM:
                soisdisconnected(unp->unp_socket);
                unp2->unp_conn = 0;
                soisdisconnected(unp2->unp_socket);
                break;
        }
}

#ifdef notdef
void unp_abort(unp) struct unpcb *unp;
{
        unp_detach(unp);
}
#endif

void unp_shutdown(struct unpcb *unp)
{
        struct socket *so;

        if (unp->unp_socket->so_type == SOCK_STREAM && unp->unp_conn && (so = unp->unp_conn->unp_socket))
        {
                socantrcvmore(so);
        }
}

void unp_drop(struct unpcb *unp, int errno)
{
        struct socket *so = unp->unp_socket;

        so->so_error = errno;
        unp_disconnect(unp);
        if (so->so_head)
        {
                so->so_pcb = (void *)0;
                m_freem(unp->unp_addr);
                mbuf_free(dtom(unp));
                sofree(so);
        }
}

#ifdef notdef
unp_drain() {}
#endif

int unp_externalize(struct mbuf *rights)
{
        int i;
        struct cmsghdr *cm = mtod(rights, struct cmsghdr *);
        struct file **rp   = (struct file **)(cm + 1);
        struct file *fp;
        int newfds = (cm->cmsg_len - sizeof(*cm)) / sizeof(int);
        int f;
        panic("no");
#if DAMIR
        if (!fdavail(&p->thread, newfds))
        {
                for (i = 0; i < newfds; i++)
                {
                        fp = *rp;
                        unp_discard(fp);
                        *rp++ = 0;
                }
                return (EMSGSIZE);
        }
        for (i = 0; i < newfds; i++)
        {
                if (fdalloc(p, 0, &f))
                        panic("unp_externalize");
                fp                    = *rp;
                p->p_fd->fd_ofiles[f] = fp;

                fp->f_msgcount--;

                unp_rights--;
                *(int *)rp++ = f;
        }
#endif
        return (0);
}

#define FMARK    0x1000 /* mark during gc() */
#define FDEFER   0x2000 /* defer for next gc pass */
#define FHASLOCK 0x4000 /* descriptor holds advisory lock */

int unp_internalize(struct mbuf *control, pthread_t p)
{
        panic("shit");
#if 0
        struct filedesc *fdp = p->process->p_fd;
        struct cmsghdr *cm   = mtod(control, struct cmsghdr *);
        struct file **rp;
        struct file *fp;
        int i, fd;
        int oldfds;

        if (cm->cmsg_type != SCM_RIGHTS || cm->cmsg_level != SOL_SOCKET || cm->cmsg_len != control->m_len)
                return (-EINVAL);
        oldfds = (cm->cmsg_len - sizeof(*cm)) / sizeof(int);
        rp     = (struct file **)(cm + 1);
        for (i = 0; i < oldfds; i++)
        {
                fd = *(int *)rp++;
                if ((unsigned)fd >= fdp->fd_nfiles || fdp->fd_ofiles[fd] == NULL)
                        return (-EBADF);
        }
        rp = (struct file **)(cm + 1);
        for (i = 0; i < oldfds; i++)
        {
                fp    = fdp->fd_ofiles[*(int *)rp];
                *rp++ = fp;
                fp->f_count++;
#ifdef DAMIR
                fp->f_msgcount++;
#endif
                unp_rights++;
        }
#endif
        return (0);
}

int unp_defer, unp_gcing;
extern struct domain unixdomain;

#define malloc(a, b, c) malloc(a)
#define free(a, b)      free(a)

void unp_gc()
{
        struct file *fp, *nextfp;
        struct socket *so;
        struct file **extra_ref, **fpp;
        int nunref, i;

        if (unp_gcing)
                return;
        unp_gcing = 1;
        unp_defer = 0;
        for (fp = file::filehead.lh_first; fp != 0; fp = fp->f_list_link.le_next)
                fp->f_flag &= ~(FMARK | FDEFER);
        do
        {
                for (fp = file::filehead.lh_first; fp != 0; fp = fp->f_list_link.le_next)
                {
                        if (fp->f_count == 0)
                                continue;
                        if (fp->f_flag & FDEFER)
                        {
                                fp->f_flag &= ~FDEFER;
                                unp_defer--;
                        }
                        else
                        {
                                if (fp->f_flag & FMARK)
                                        continue;
#ifdef DAMIR
                                if (fp->f_count == fp->f_msgcount)
                                        continue;
#endif
                                fp->f_flag |= FMARK;
                        }
                        so = nullptr;
                        panic("sdsd");
                        // if (fp->f_type != DTYPE_SOCKET || (so = (struct socket *)fp->f_data) == 0)
                        //        continue;
                        if (so->so_proto->pr_domain != &unixdomain || (so->so_proto->pr_flags & PR_RIGHTS) == 0)
                                continue;
#ifdef notdef
                        if (so->so_rcv.sb_flags & SB_LOCK)
                        {
                                /*
                                 * This is problematical; it's not clear
                                 * we need to wait for the sockbuf to be
                                 * unlocked (on a uniprocessor, at least),
                                 * and it's also not clear what to do
                                 * if sbwait returns an error due to receipt
                                 * of a signal.  If sbwait does return
                                 * an error, we'll go into an infinite
                                 * loop.  Delete all of this for now.
                                 */
                                sbwait(&so->so_rcv);
                                goto restart;
                        }
#endif
                        unp_scan(so->so_rcv.sb_mb, unp_mark);
                }
        } while (unp_defer);
        /*
         * We grab an extra reference to each of the file table entries
         * that are not otherwise accessible and then free the rights
         * that are stored in messages on them.
         *
         * The bug in the orginal code is a little tricky, so I'll describe
         * what's wrong with it here.
         *
         * It is incorrect to simply unp_discard each entry for f_msgcount
         * times -- consider the case of sockets A and B that contain
         * references to each other.  On a last close of some other socket,
         * we trigger a gc since the number of outstanding rights (unp_rights)
         * is non-zero.  If during the sweep phase the gc code un_discards,
         * we end up doing a (full) closef on the descriptor.  A closef on A
         * results in the following chain.  Closef calls soo_close, which
         * calls soclose.   Soclose calls first (through the switch
         * uipc_usrreq) unp_detach, which re-invokes unp_gc.  Unp_gc simply
         * returns because the previous instance had set unp_gcing, and
         * we return all the way back to soclose, which marks the socket
         * with SS_NOFDREF, and then calls sofree.  Sofree calls sorflush
         * to free up the rights that are queued in messages on the socket A,
         * i.e., the reference on B.  The sorflush calls via the dom_dispose
         * switch unp_dispose, which unp_scans with unp_discard.  This second
         * instance of unp_discard just calls closef on B.
         *
         * Well, a similar chain occurs on B, resulting in a sorflush on B,
         * which results in another closef on A.  Unfortunately, A is already
         * being closed, and the descriptor has already been marked with
         * SS_NOFDREF, and soclose panics at this point.
         *
         * Here, we first take an extra reference to each inaccessible
         * descriptor.  Then, we call sorflush ourself, since we know
         * it is a Unix domain socket anyhow.  After we destroy all the
         * rights carried in messages, we do a last closef to get rid
         * of our extra reference.  This is the last close, and the
         * unp_detach etc will shut down the socket.
         *
         * 91/09/19, bsy@cs.cmu.edu
         */
        extra_ref = (struct file **)malloc(file::nfiles * sizeof(struct file *), M_FILE, M_WAITOK);
        for (nunref = 0, fp = file::filehead.lh_first, fpp = extra_ref; fp != 0; fp = nextfp)
        {
                nextfp = fp->f_list_link.le_next;
                if (fp->f_count == 0)
                {
                        continue;
                }

                panic("NOTREADY");

#ifdef DAMIR
                if (fp->f_count == fp->f_msgcount && !(fp->f_flag & FMARK))
                {
                        *fpp++ = fp;
                        nunref++;
                        fp->f_count++;
                }
#endif
        }
        for (i = nunref, fpp = extra_ref; --i >= 0; ++fpp)
                panic("sdsd");
        // sorflush((struct socket *)(*fpp)->f_data);

        panic("NOTREADY");
#ifdef DAMIR
        for (i = nunref, fpp = extra_ref; --i >= 0; ++fpp)
                closef(*fpp, (pthread_t)NULL);
#endif
        free(extra_ref, M_FILE);
        unp_gcing = 0;
}

void unp_dispose(struct mbuf *m)
{
        if (m)
        {
                unp_scan(m, unp_discard);
        }
}

void unp_scan(struct mbuf *m0, void (*op)(struct file *))
{
        struct mbuf *m;
        struct file **rp;
        struct cmsghdr *cm;
        int i;
        int qfds;

        while (m0)
        {
                for (m = m0; m; m = m->m_next)
                        if (m->m_type == MT_CONTROL && m->m_len >= sizeof(*cm))
                        {
                                cm = mtod(m, struct cmsghdr *);
                                if (cm->cmsg_level != SOL_SOCKET || cm->cmsg_type != SCM_RIGHTS)
                                        continue;
                                qfds = (cm->cmsg_len - sizeof *cm) / sizeof(struct file *);
                                rp   = (struct file **)(cm + 1);
                                for (i = 0; i < qfds; i++)
                                        (*op)(*rp++);
                                break; /* XXX, but saves time */
                        }
                m0 = m0->m_act;
        }
}

void unp_mark(struct file *fp)
{
        if (fp->f_flag & FMARK)
        {
                return;
        }
        unp_defer++;
        fp->f_flag |= (FMARK | FDEFER);
}

void unp_discard(struct file *fp)
{
        panic("NOT READY");
#ifdef DAMIR
        fp->f_msgcount--;
        unp_rights--;
        closef(fp, (pthread_t)NULL);
#endif
}
