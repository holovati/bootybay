/*
 * Copyright (c) 1982, 1986, 1988, 1990, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)uipc_socket2.c	8.2 (Berkeley) 2/14/95
 */

#include "mbuf/mbuf.h"
#include <mach/socketvar.h>
#include <mach/sys/protosw.h>
#include <malloc.h>
#include <strings.h>
#include <sys/socket.h>

#include <mach/buf.h>
#include <mach/errno.h>
#include <mach/file.h>
#include <mach/param.h>

#include <mach/thread.h>

#include <algorithm>
#include <emerixx/sleep.h>

//#include <sys/systm.h>

/*
 * Primitive routines for operating on sockets and socket buffers
 */

/* strings for sleep message: */
const char netio[]  = "netio";
const char netcon[] = "netcon";
const char netcls[] = "netcls";

u32 sb_max = SB_MAX; /* patchable */

/*
 * Procedures to manipulate state flags of socket
 * and do appropriate wakeups.  Normal sequence from the
 * active (originating) side is that soisconnecting() is
 * called during processing of connect() call,
 * resulting in an eventual call to soisconnected() if/when the
 * connection is established.  When the connection is torn down
 * soisdisconnecting() is called during processing of disconnect() call,
 * and soisdisconnected() is called when the connection to the peer
 * is totally severed.  The semantics of these routines are such that
 * connectionless protocols can call soisconnected() and soisdisconnected()
 * only, bypassing the in-progress calls when setting up a ``connection''
 * takes no time.
 *
 * From the passive side, a socket is created with
 * two queues of sockets: so_q0 for connections in progress
 * and so_q for connections already made and awaiting user acceptance.
 * As a protocol is preparing incoming connections, it creates a socket
 * structure queued on so_q0 by calling sonewconn().  When the connection
 * is established, soisconnected() is called, and transfers the
 * socket structure to so_q, making it available to accept().
 *
 * If a socket is closed with sockets on either
 * so_q0 or so_q, these sockets are dropped.
 *
 * If higher level protocols are implemented in
 * the kernel, the wakeups done here will sometimes
 * cause software-interrupt process scheduling.
 */

void soisconnecting(struct socket *so)
{
        so->so_state &= ~(SS_ISCONNECTED | SS_ISDISCONNECTING);
        so->so_state |= SS_ISCONNECTING;
}

void soisconnected(struct socket *so)
{
        struct socket *head = so->so_head;

        so->so_state &= ~(SS_ISCONNECTING | SS_ISDISCONNECTING | SS_ISCONFIRMING);
        so->so_state |= SS_ISCONNECTED;
        if (head && soqremque(so, 0))
        {
                soqinsque(head, so, 1);
                sorwakeup(head);
                uthread_wakeup(&head->so_timeo);
        }
        else
        {
                uthread_wakeup(&so->so_timeo);
                sorwakeup(so);
                sowwakeup(so);
        }
}

void soisdisconnecting(struct socket *so)
{
        so->so_state &= ~SS_ISCONNECTING;
        so->so_state |= (SS_ISDISCONNECTING | SS_CANTRCVMORE | SS_CANTSENDMORE);
        uthread_wakeup(&so->so_timeo);
        sowwakeup(so);
        sorwakeup(so);
}

void soisdisconnected(struct socket *so)
{
        so->so_state &= ~(SS_ISCONNECTING | SS_ISCONNECTED | SS_ISDISCONNECTING);
        so->so_state |= (SS_CANTRCVMORE | SS_CANTSENDMORE | SS_ISDISCONNECTED);
        uthread_wakeup(&so->so_timeo);
        sowwakeup(so);
        sorwakeup(so);
}

/*
 * When an attempt at a new connection is noted on a socket
 * which accepts connections, sonewconn is called.  If the
 * connection is possible (subject to space constraints, etc.)
 * then we allocate a new structure, propoerly linked into the
 * data structure of the original socket, and return this.
 * Connstatus may be 0, or SO_ISCONFIRMING, or SO_ISCONNECTED.
 *
 * Currently, sonewconn() is defined as sonewconn1() in socketvar.h
 * to catch calls that are missing the (new) second parameter.
 */
struct socket *sonewconn1(struct socket *head, int connstatus)
{
        struct socket *so = nullptr;
        int soqueue       = connstatus ? 1 : 0;

        if (head->so_qlen + head->so_q0len > 3 * head->so_qlimit / 2)
                return ((struct socket *)0);
#ifdef DAMIR
        MALLOC(so, struct socket *, sizeof(*so), M_SOCKET, M_DONTWAIT);
#else
        panic("Convert to zone");
        // so = (struct socket *)malloc(sizeof(*so));
#endif

        if (so == NULL)
                return ((struct socket *)0);

        bzero(so, sizeof(*so));
        so->so_type    = head->so_type;
        so->so_options = head->so_options & ~SO_ACCEPTCONN;
        so->so_linger  = head->so_linger;
        so->so_state   = head->so_state | SS_NOFDREF;
        so->so_proto   = head->so_proto;
        so->so_timeo   = head->so_timeo;
        so->so_pgid    = head->so_pgid;
        soreserve(so, head->so_snd.sb_hiwat, head->so_rcv.sb_hiwat);
        soqinsque(head, so, soqueue);
        if ((*so->so_proto->pr_usrreq)(so, PRU_ATTACH, (struct mbuf *)0, (struct mbuf *)0, (struct mbuf *)0))
        {
                soqremque(so, soqueue);
#if DAMIR
                free((caddr_t)so, M_SOCKET);
#else
                panic("Convert to zone");
                // free(so);
#endif
                return nullptr;
        }
        if (connstatus)
        {
                sorwakeup(head);
                uthread_wakeup(&head->so_timeo);
                so->so_state |= connstatus;
        }
        return (so);
}

void soqinsque(struct socket *head, struct socket *so, int q)
{
        struct socket **prev;
        so->so_head = head;
        if (q == 0)
        {
                head->so_q0len++;
                so->so_q0 = 0;
                for (prev = &(head->so_q0); *prev;)
                        prev = &((*prev)->so_q0);
        }
        else
        {
                head->so_qlen++;
                so->so_q = 0;
                for (prev = &(head->so_q); *prev;)
                        prev = &((*prev)->so_q);
        }
        *prev = so;
}

int soqremque(struct socket *so, int q)
{
        struct socket *head, *prev, *next;

        head = so->so_head;
        prev = head;
        for (;;)
        {
                next = q ? prev->so_q : prev->so_q0;
                if (next == so)
                        break;
                if (next == 0)
                        return (0);
                prev = next;
        }
        if (q == 0)
        {
                prev->so_q0 = next->so_q0;
                head->so_q0len--;
        }
        else
        {
                prev->so_q = next->so_q;
                head->so_qlen--;
        }
        next->so_q0 = next->so_q = 0;
        next->so_head            = 0;
        return (1);
}

/*
 * Socantsendmore indicates that no more data will be sent on the
 * socket; it would normally be applied to a socket when the user
 * informs the system that no more data is to be sent, by the protocol
 * code (in case PRU_SHUTDOWN).  Socantrcvmore indicates that no more data
 * will be received, and will normally be applied to the socket by a
 * protocol when it detects that the peer will send no more data.
 * Data queued for reading in the socket may yet be read.
 */

void socantsendmore(struct socket *so)
{
        so->so_state |= SS_CANTSENDMORE;
        sowwakeup(so);
}

void socantrcvmore(struct socket *so)
{
        so->so_state |= SS_CANTRCVMORE;
        sorwakeup(so);
}

/*
 * Wait for data to arrive at/drain from a socket buffer.
 */
int sbwait(struct sockbuf *sb)
{
        sb->sb_flags |= SB_WAIT;
        return uthread_sleep_timeout(&sb->sb_cc /*, (sb->sb_flags & SB_NOINTR) ? PSOCK : PSOCK | PCATCH, netio*/, sb->sb_timeo);
}

/*
 * Lock a sockbuf already known to be locked;
 * return any error returned from sleep (EINTR).
 */
int sb_lock(struct sockbuf *sb)
{
        int error;

        while (sb->sb_flags & SB_LOCK)
        {
                sb->sb_flags |= SB_WANT;

                if (error = uthread_sleep(&sb->sb_flags /*, (sb->sb_flags & SB_NOINTR) ? PSOCK : PSOCK | PCATCH, netio, 0)*/))
                {
                        return (error);
                }

                if (sb->sb_flags & SB_NOINTR)
                {
                        if (error
                            = uthread_sleep(&sb->sb_flags /*,(sb->sb_flags & SB_NOINTR) ? PSOCK : PSOCK | PCATCH, netio, 0)*/))
                        {
                                return (error);
                        }
                        else
                        {
                                assert_wait(&sb->sb_flags, false);
                                thread_block(nullptr);
                        }
                }
        }
        sb->sb_flags |= SB_LOCK;
        return (0);
}
/*
 * Wakeup processes waiting on a socket buffer.
 * Do asynchronous notification via SIGIO
 * if the socket has the SS_ASYNC flag set.
 */
void sowakeup(struct socket *so, struct sockbuf *sb, int revents)
{
        process_t p;

        // select_wakeup(&sb->sb_sel);

        poll_wakeup(&so->so_pollfds, revents);

        sb->sb_flags &= ~SB_SEL;

        if (sb->sb_flags & SB_WAIT)
        {
                sb->sb_flags &= ~SB_WAIT;
                uthread_wakeup(&sb->sb_cc);
        }

        if (so->so_state & SS_ASYNC)
        {
                if (so->so_pgid < 0)
                {
                        panic("not ready");
                        // gsignal(-so->so_pgid, SIGIO);
                }
#if 0
                else if (so->so_pgid > 0 && (p = process::find(so->so_pgid)) != 0)
                {
                        p->signal(SIGIO);
                }
#endif
        }
}

/*
 * Socket buffer (struct sockbuf) utility routines.
 *
 * Each socket contains two socket buffers: one for sending data and
 * one for receiving data.  Each buffer contains a queue of mbufs,
 * information about the number of mbufs and amount of data in the
 * queue, and other fields allowing select() statements and notification
 * on data availability to be implemented.
 *
 * Data stored in a socket buffer is maintained as a list of records.
 * Each record is a list of mbufs chained together with the m_next
 * field.  Records are chained together with the m_nextpkt field. The upper
 * level routine soreceive() expects the following conventions to be
 * observed when placing information in the receive buffer:
 *
 * 1. If the protocol requires each message be preceded by the sender's
 *    name, then a record containing that name must be present before
 *    any associated data (mbuf's must be of type MT_SONAME).
 * 2. If the protocol supports the exchange of ``access rights'' (really
 *    just additional data associated with the message), and there are
 *    ``rights'' to be received, then a record containing this data
 *    should be present (mbuf's must be of type MT_RIGHTS).
 * 3. If a name or rights record exists, then it must be followed by
 *    a data record, perhaps of zero length.
 *
 * Before using a new socket structure it is first necessary to reserve
 * buffer space to the socket, by calling sbreserve().  This should commit
 * some of the available buffer space in the system buffer pool for the
 * socket (currently, it does nothing but enforce limits).  The space
 * should be released by calling sbrelease() when the socket is destroyed.
 */

int soreserve(struct socket *so, u32 sndcc, u32 rcvcc)
{
        if (sbreserve(&so->so_snd, sndcc) == 0)
        {
                goto bad;
        }
        if (sbreserve(&so->so_rcv, rcvcc) == 0)
        {
                goto bad2;
        }
        if (so->so_rcv.sb_lowat == 0)
        {
                so->so_rcv.sb_lowat = 1;
        }
        if (so->so_snd.sb_lowat == 0)
        {
                so->so_snd.sb_lowat = MCLBYTES;
        }
        if (so->so_snd.sb_lowat > so->so_snd.sb_hiwat)
        {
                so->so_snd.sb_lowat = so->so_snd.sb_hiwat;
        }
        return (0);
bad2:
        sbrelease(&so->so_snd);
bad:
        return -ENOBUFS;
}

/*
 * Allot mbufs to a sockbuf.
 * Attempt to scale mbmax so that mbcnt doesn't become limiting
 * if buffering efficiency is near the normal case.
 */
int sbreserve(struct sockbuf *sb, u32 cc)
{
        if (cc > sb_max * MCLBYTES / (MSIZE + MCLBYTES))
        {
                return (0);
        }

        sb->sb_hiwat = cc;
        sb->sb_mbmax = std::min(cc * 2, sb_max);

        if (sb->sb_lowat > sb->sb_hiwat)
        {
                sb->sb_lowat = sb->sb_hiwat;
        }

        return (1);
}

/*
 * Free mbufs held by a socket, and reserved mbuf space.
 */
void sbrelease(struct sockbuf *sb)
{
        sbflush(sb);
        sb->sb_hiwat = sb->sb_mbmax = 0;
}

/*
 * Routines to add and remove
 * data from an mbuf queue.
 *
 * The routines sbappend() or sbappendrecord() are normally called to
 * append new mbufs to a socket buffer, after checking that adequate
 * space is available, comparing the function sbspace() with the amount
 * of data to be added.  sbappendrecord() differs from sbappend() in
 * that data supplied is treated as the beginning of a new record.
 * To place a sender's address, optional access rights, and data in a
 * socket receive buffer, sbappendaddr() should be used.  To place
 * access rights and data in a socket receive buffer, sbappendrights()
 * should be used.  In either case, the new data begins a new record.
 * Note that unlike sbappend() and sbappendrecord(), these routines check
 * for the caller that there will be enough space to store the data.
 * Each fails if there is not enough space, or if it cannot find mbufs
 * to store additional information in.
 *
 * Reliable protocols may use the socket send buffer to hold data
 * awaiting acknowledgement.  Data is normally copied from a socket
 * send buffer in a protocol with m_copy for output to a peer,
 * and then removing the data from the socket buffer with sbdrop()
 * or sbdroprecord() when the data is acknowledged by the peer.
 */

/*
 * Append mbuf chain m to the last record in the
 * socket buffer sb.  The additional space associated
 * the mbuf chain is recorded in sb.  Empty mbufs are
 * discarded and mbufs are compacted where possible.
 */
void sbappend(struct sockbuf *sb, struct mbuf *m)
{
        struct mbuf *n;

        if (m == 0)
        {
                return;
        }

        if (n = sb->sb_mb)
        {
                while (n->m_nextpkt)
                {
                        n = n->m_nextpkt;
                }
                do
                {
                        if (n->__m_flags & M_EOR)
                        {
                                sbappendrecord(sb, m); /* XXXXXX!!!! */
                                return;
                        }
                } while (n->m_next && (n = n->m_next));
        }
        sbcompress(sb, m, n);
}

#ifdef SOCKBUF_DEBUG
void sbcheck(sb) register struct sockbuf *sb;
{
        register struct mbuf *m;
        register int len = 0, mbcnt = 0;

        for (m = sb->sb_mb; m; m = m->m_next)
        {
                len += m->m_len;
                mbcnt += MSIZE;
                if (m->__m_flags & M_EXT)
                        mbcnt += m->m_ext.ext_size;
                if (m->m_nextpkt)
                        panic("sbcheck nextpkt");
        }
        if (len != sb->sb_cc || mbcnt != sb->sb_mbcnt)
        {
                printf("cc %d != %d || mbcnt %d != %d\n", len, sb->sb_cc, mbcnt, sb->sb_mbcnt);
                panic("sbcheck");
        }
}
#endif

/*
 * As above, except the mbuf chain
 * begins a new record.
 */
void sbappendrecord(struct sockbuf *sb, struct mbuf *m0)
{
        struct mbuf *m;

        if (m0 == 0)
        {
                return;
        }

        if (m = sb->sb_mb)
        {
                while (m->m_nextpkt)
                {
                        m = m->m_nextpkt;
                }
        }
        /*
         * Put the first mbuf on the queue.
         * Note this permits zero length records.
         */
        sballoc(sb, m0);
        if (m)
        {
                m->m_nextpkt = m0;
        }
        else
        {
                sb->sb_mb = m0;
        }
        m          = m0->m_next;
        m0->m_next = 0;
        if (m && (m0->__m_flags & M_EOR))
        {
                m0->__m_flags &= ~M_EOR;
                m->__m_flags |= M_EOR;
        }
        sbcompress(sb, m, m0);
}

/*
 * As above except that OOB data
 * is inserted at the beginning of the sockbuf,
 * but after any other OOB data.
 */
void sbinsertoob(struct sockbuf *sb, struct mbuf *m0)
{
        struct mbuf *m;
        struct mbuf **mp;

        if (m0 == 0)
        {
                return;
        }

        for (mp = &sb->sb_mb; m = *mp; mp = &((*mp)->m_nextpkt))
        {
        again:
                switch (m->m_type)
                {
                case MT_OOBDATA:
                        continue; /* WANT next train */

                case MT_CONTROL:
                        if (m = m->m_next)
                        {
                                goto again; /* inspect THIS train further */
                        }
                }
                break;
        }
        /*
         * Put the first mbuf on the queue.
         * Note this permits zero length records.
         */
        sballoc(sb, m0);
        m0->m_nextpkt = *mp;
        *mp           = m0;
        m             = m0->m_next;
        m0->m_next    = 0;
        if (m && (m0->__m_flags & M_EOR))
        {
                m0->__m_flags &= ~M_EOR;
                m->__m_flags |= M_EOR;
        }
        sbcompress(sb, m, m0);
}

/*
 * Append address and data, and optionally, control (ancillary) data
 * to the receive queue of a socket.  If present,
 * m0 must include a packet header with total length.
 * Returns 0 if no space in sockbuf or insufficient mbufs.
 */
int sbappendaddr(struct sockbuf *sb, struct sockaddr *asa, struct mbuf *m0, struct mbuf *control)
{
        panic("NOT READY");
#if DAMIR
        struct mbuf *m, *n;
        int space = asa->sa_len;

        if (m0 && (m0->__m_flags & M_PKTHDR) == 0)
                panic("sbappendaddr");
        if (m0)
                space += m0->m_pkthdr.len;
        for (n = control; n; n = n->m_next)
        {
                space += n->m_len;
                if (n->m_next == 0) /* keep pointer to last control buf */
                        break;
        }
        if (space > sbspace(sb))
                return (0);
        if (asa->sa_len > MLEN)
                return (0);
        MGET(m, M_DONTWAIT, MT_SONAME);
        if (m == 0)
                return (0);
        m->m_len = asa->sa_len;
        bcopy(asa, mtod(m, void *), asa->sa_len);
        if (n)
                n->m_next = m0; /* concatenate data to control */
        else
                control = m0;
        m->m_next = control;
        for (n = m; n; n = n->m_next)
                sballoc(sb, n);
        if (n = sb->sb_mb)
        {
                while (n->m_nextpkt)
                        n = n->m_nextpkt;
                n->m_nextpkt = m;
        }
        else
                sb->sb_mb = m;
#endif
        return (1);
}

int sbappendcontrol(struct sockbuf *sb, struct mbuf *m0, struct mbuf *control)
{
        struct mbuf *m, *n;
        int space = 0;

        if (control == 0)
        {
                panic("sbappendcontrol");
        }

        for (m = control;; m = m->m_next)
        {
                space += m->m_len;
                if (m->m_next == 0)
                {
                        break;
                }
        }

        n = m; /* save pointer to last control buffer */

        for (m = m0; m; m = m->m_next)
        {
                space += m->m_len;
        }

        if (space > sbspace(sb))
        {
                return (0);
        }

        n->m_next = m0; /* concatenate data to control */

        for (m = control; m; m = m->m_next)
        {
                sballoc(sb, m);
        }

        if (n = sb->sb_mb)
        {
                while (n->m_nextpkt)
                {
                        n = n->m_nextpkt;
                }
                n->m_nextpkt = control;
        }
        else
        {
                sb->sb_mb = control;
        }

        return (1);
}

/*
 * Compress mbuf chain m into the socket
 * buffer sb following mbuf n.  If n
 * is null, the buffer is presumed empty.
 */
void sbcompress(struct sockbuf *sb, struct mbuf *m, struct mbuf *n)
{
        int eor = 0;
        struct mbuf *o;

        while (m)
        {
                eor |= m->__m_flags & M_EOR;
                if (m->m_len == 0 && (eor == 0 || (((o = m->m_next) || (o = n)) && o->m_type == m->m_type)))
                {
                        m = mbuf_free(m);
                        continue;
                }
                if (n && (n->__m_flags & (M_EXT | M_EOR)) == 0
                    && (((vm_offset_t)n->m_data) + n->m_len + m->m_len) < (vm_offset_t)(&n->m_dat[MLEN]) && n->m_type == m->m_type)
                {
                        bcopy(mtod(m, void *), (void *)(mtod(n, vm_offset_t) + n->m_len), (unsigned)m->m_len);
                        n->m_len += m->m_len;
                        sb->sb_cc += m->m_len;
                        m = mbuf_free(m);
                        continue;
                }
                if (n)
                        n->m_next = m;
                else
                        sb->sb_mb = m;
                sballoc(sb, m);
                n = m;
                m->__m_flags &= ~M_EOR;
                m         = m->m_next;
                n->m_next = 0;
        }
        if (eor)
        {
                if (n)
                        n->__m_flags |= eor;
                else
                        log_fatal("semi-panic: sbcompress");
        }
}

/*
 * Free all mbufs in a sockbuf.
 * Check that all resources are reclaimed.
 */
void sbflush(struct sockbuf *sb)
{
        if (sb->sb_flags & SB_LOCK)
                panic("sbflush");
        while (sb->sb_mbcnt)
                sbdrop(sb, (int)sb->sb_cc);
        if (sb->sb_cc || sb->sb_mb)
                panic("sbflush 2");
}

/*
 * Drop data from (the front of) a sockbuf.
 */
void sbdrop(struct sockbuf *sb, int len)
{
        struct mbuf *m, *mn;
        struct mbuf *next;

        next = (m = sb->sb_mb) ? m->m_nextpkt : 0;
        while (len > 0)
        {
                if (m == 0)
                {
                        if (next == 0)
                                panic("sbdrop");
                        m    = next;
                        next = m->m_nextpkt;
                        continue;
                }
                if (m->m_len > len)
                {
                        m->m_len -= len;
                        m->m_data = (void *)(((vm_offset_t)m->m_data) + len);
                        sb->sb_cc -= len;
                        break;
                }
                len -= m->m_len;
                sbfree(sb, m);
                mn = mbuf_free(m);
                m  = mn;
        }
        while (m && m->m_len == 0)
        {
                sbfree(sb, m);
                mn = mbuf_free(m);
                m  = mn;
        }
        if (m)
        {
                sb->sb_mb    = m;
                m->m_nextpkt = next;
        }
        else
                sb->sb_mb = next;
}

/*
 * Drop a record off the front of a sockbuf
 * and move the next record to the front.
 */
void sbdroprecord(struct sockbuf *sb)
{
        struct mbuf *m, *mn;

        m = sb->sb_mb;
        if (m)
        {
                sb->sb_mb = m->m_nextpkt;
                do
                {
                        sbfree(sb, m);
                        mn = mbuf_free(m);
                } while (m = mn);
        }
}
