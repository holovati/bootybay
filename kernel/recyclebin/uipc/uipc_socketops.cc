/*
 * Copyright (c) 1982, 1986, 1990, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)sys_socket.c	8.3 (Berkeley) 2/14/95
 */

#include "mbuf/mbuf.h"
#include <mach/file.h>

#include <mach/socketvar.h>
#include <mach/stat.h>
#include <mach/sys/protosw.h>
#include <sys/ioctl.h>
#include <sys/param.h>
#include <sys/statfs.h>
#undef _GNU_SOURCE
#include <sys/socket.h>
#define _GNU_SOURCE
//#include <sys/systm.h>
#include <algorithm>
#include <kernel/zalloc.h>
#include <mach/init.h>
#include <mach/machine/spl.h>
#include <strings.h>
//#include <net/if.h>
//#include <net/route.h>

/* XXX */
#include "../emerixx/src/fcntl/fdesc.hh"

//-------------------------------------------------------------------------------

struct socket_file final : file
{
        int read(struct uio *uio, struct ucred *cred) override;

        int write(struct uio *uio, struct ucred *cred) override;

        int ioctl(ioctl_cmd_t com, void *data, pthread_t p) override;

        int stat(struct stat *a_statbuf) override;

        int statfs(struct statfs *a_statfsbuf) override;

        int close(pthread_t p) override;

        kern_return_t poll(emerixx::pollinfo *a_pollinfo) override;

        void do_file_free() override;

        socket_file(struct socket *a_so, int a_flag)
            : file(DTYPE_SOCKET, a_flag)
            , m_socket(a_so)
        {
        }

        struct socket *m_socket;
};

ZONE_DEFINE(socket_file, s_socket_file_zone, file::maxfiles, 0, ZONE_FIXED, 0);

template <> int file::open<struct socket *>(pthread_t a_pthread, struct socket *a_socket, mode_t a_open_flags, file **a_file_out)
{
        socket_file *f = s_socket_file_zone.alloc_new(a_socket, a_open_flags);

        KASSERT(f != nullptr);

        kern_return_t retval = fd_context_fdalloc(a_pthread, f, a_open_flags);

        if (KERN_STATUS_SUCCESS(retval))
        {
                if (a_file_out != nullptr)
                {
                        *a_file_out = f;
                }
                else
                {
                        f->f_count--;
                }
        }
        else
        {
                s_socket_file_zone.free_delete(f);
        }

        return retval;
}

int socket_file::read(struct uio *uio, struct ucred *cred)
{
        return (soreceive(m_socket, (struct mbuf **)0, uio, (struct mbuf **)0, (struct mbuf **)0, (int *)0));
}
int socket_file::write(struct uio *uio, struct ucred *cred)
{
        return (sosend(m_socket, (struct mbuf *)0, uio, (struct mbuf *)0, (struct mbuf *)0, 0));
}

int socket_file::ioctl(ioctl_cmd_t com, void *data, pthread_t p)
{
        switch (com)
        {
        case FIONBIO:
                if (*(int *)data)
                        m_socket->so_state |= SS_NBIO;
                else
                        m_socket->so_state &= ~SS_NBIO;
                return (0);

        case FIOASYNC:
                if (*(int *)data)
                {
                        m_socket->so_state |= SS_ASYNC;
                        m_socket->so_rcv.sb_flags |= SB_ASYNC;
                        m_socket->so_snd.sb_flags |= SB_ASYNC;
                }
                else
                {
                        m_socket->so_state &= ~SS_ASYNC;
                        m_socket->so_rcv.sb_flags &= ~SB_ASYNC;
                        m_socket->so_snd.sb_flags &= ~SB_ASYNC;
                }
                return (0);

        case FIONREAD:
                *(int *)data = m_socket->so_rcv.sb_cc;
                return (0);

        case SIOCSPGRP:
                m_socket->so_pgid = *(int *)data;
                return (0);

        case SIOCGPGRP:
                *(int *)data = m_socket->so_pgid;
                return (0);

        case SIOCATMARK:
                *(int *)data = (m_socket->so_state & SS_RCVATMARK) != 0;
                return (0);
        }
#define IOCGROUP(x) (((x) >> 8) & 0xff)
        /*
         * Interface/routing/protocol specific ioctls:
         * interface and routing ioctls should have a
         * different entry since a socket's unnecessary
         */
        if (IOCGROUP(com) == 'i')
        {
                panic("not ready");
                // return (ifioctl(so, cmd, data, p));
        }

        if (IOCGROUP(com) == 'r')
        {
                panic("not ready");
                // return (rtioctl(cmd, data, p));
        }

        return ((*m_socket->so_proto->pr_usrreq)(m_socket, PRU_CONTROL, (struct mbuf *)com, (struct mbuf *)data, (struct mbuf *)0));
}

#if 0
int socket_file::select(int which, pthread_t p)
{

        struct socket *so = m_socket;
        int s             = splnet();

        switch (which)
        {
        case FREAD:
                if (soreadable(so))
                {
                        splx(s);
                        return (1);
                }
                select_record(p, &so->so_rcv.sb_sel);
                so->so_rcv.sb_flags |= SB_SEL;
                break;

        case FWRITE:
                if (sowriteable(so))
                {
                        splx(s);
                        return (1);
                }
                select_record(p, &so->so_snd.sb_sel);
                so->so_snd.sb_flags |= SB_SEL;
                break;

        case 0:
                if (so->so_oobmark || (so->so_state & SS_RCVATMARK))
                {
                        splx(s);
                        return (1);
                }
                select_record(p, &so->so_rcv.sb_sel);
                so->so_rcv.sb_flags |= SB_SEL;
                break;
        }
        splx(s);

        panic("Should not run");

        return (0);
}
#endif
int socket_file::stat(struct stat *a_statbuf)
{
        bzero(a_statbuf, sizeof(*a_statbuf));
        a_statbuf->st_mode = S_IFSOCK;
        return (
            (*m_socket->so_proto->pr_usrreq)(m_socket, PRU_SENSE, (struct mbuf *)a_statbuf, (struct mbuf *)0, (struct mbuf *)0));
}

int socket_file::statfs(struct statfs *a_statfsbuf)
{
#define PIPEFS_MAGIC 0x50495045

        bzero(a_statfsbuf, sizeof(*a_statfsbuf));
        a_statfsbuf->f_type    = PIPEFS_MAGIC;
        a_statfsbuf->f_bsize   = 0x1000;
        a_statfsbuf->f_namelen = 0xFF;
        a_statfsbuf->f_frsize  = 0x1000;
        a_statfsbuf->f_type    = 0x20; // ST_VALID;

        return 0;
}

int socket_file::close(pthread_t p)
{
        int error = 0;

        if (m_socket)
        {
                error = soclose(m_socket);
        }

        m_socket = nullptr;
        return (error);
}

kern_return_t socket_file::poll(emerixx::pollinfo *a_pollinfo)
{
        struct socket *so = m_socket;
        // int revents       = 0;
        // int s;

        // s = solock(so);
        // spl_t s = splnet();
        if (a_pollinfo->m_fd->events & (POLLIN | POLLRDNORM))
        {
                if (soreadable(so))
                {
                        a_pollinfo->m_fd->revents |= a_pollinfo->m_fd->events & (POLLIN | POLLRDNORM);
                }
        }
        /* NOTE: POLLHUP and POLLOUT/POLLWRNORM are mutually exclusive */
        if (so->so_state & SS_ISDISCONNECTED)
        {
                a_pollinfo->m_fd->revents |= POLLHUP;
        }
        else if (a_pollinfo->m_fd->events & (POLLOUT | POLLWRNORM))
        {
                if (sowriteable(so))
                {
                        a_pollinfo->m_fd->revents |= a_pollinfo->m_fd->events & (POLLOUT | POLLWRNORM);
                }
        }
        if (a_pollinfo->m_fd->events & (POLLPRI | POLLRDBAND))
        {
                if (so->so_oobmark || (so->so_state & SS_RCVATMARK))
                {
                        a_pollinfo->m_fd->revents |= a_pollinfo->m_fd->events & (POLLPRI | POLLRDBAND);
                }
        }

        a_pollinfo->m_dev_polls = &so->so_pollfds;
        poll_record(a_pollinfo);
#if 0
        if (a_pollinfo->m_fd->revents == 0)
        {

                if (a_pollinfo->m_fd->events & (POLLIN | POLLPRI | POLLRDNORM | POLLRDBAND))
                {
                        // selrecord(p, &so->so_rcv.sb_sel);

                        so->so_rcv.sb_flags |= SB_SEL;
                }
                if (a_pollinfo->m_fd->events & (POLLOUT | POLLWRNORM))
                {
                        // selrecord(p, &so->so_snd.sb_sel);
                        so->so_snd.sb_flags |= SB_SEL;
                }
        }
#endif
        if (a_pollinfo->m_fd->revents)
        {
                poll_wakeup(&so->so_pollfds, a_pollinfo->m_fd->revents);
        }
        // sounlock(so, s);
        // splx(s);
        return KERN_SUCCESS;
}

void socket_file::do_file_free() { s_socket_file_zone.free_delete(this); }

//-------------------------------------------------------------------------------