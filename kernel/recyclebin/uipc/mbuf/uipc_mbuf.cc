/*
 * Copyright (c) 1982, 1986, 1988, 1991, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)uipc_mbuf.c	8.4 (Berkeley) 2/14/95
 */

#include <malloc.h>
//#include <mach/map.h>
//#include <sys/param.h>

//#include <sys/systm.h>
#define MBTYPES
#include <mach/init.h>
#include <mach/sys/domain.h>
#include <mach/vm/vm_kern.h>

#include <strings.h>
//#include </kernel.h>
/*
 * Types of memory to be allocated
 */
#define M_FREE      0  /* should be on free list */
#define M_MBUF      1  /* mbuf */
#define M_DEVBUF    2  /* device driver memory */
#define M_SOCKET    3  /* socket structure */
#define M_PCB       4  /* protocol control block */
#define M_RTABLE    5  /* routing tables */
#define M_HTABLE    6  /* IMP host tables */
#define M_FTABLE    7  /* fragment reassembly header */
#define M_ZOMBIE    8  /* zombie proc status */
#define M_IFADDR    9  /* interface address */
#define M_SOOPTS    10 /* socket options */
#define M_SONAME    11 /* socket name */
#define M_NAMEI     12 /* namei path name buffer */
#define M_GPROF     13 /* kernel profiling buffer */
#define M_IOCTLOPS  14 /* ioctl data buffer */
#define M_MAPMEM    15 /* mapped memory descriptors */
#define M_CRED      16 /* credentials */
#define M_PGRP      17 /* process group header */
#define M_SESSION   18 /* session header */
#define M_IOV       19 /* large iov's */
#define M_MOUNT     20 /* vfs mount struct */
#define M_FHANDLE   21 /* network file handle */
#define M_NFSREQ    22 /* NFS request header */
#define M_NFSMNT    23 /* NFS mount structure */
#define M_NFSNODE   24 /* NFS vnode private part */
#define M_VNODE     25 /* Dynamically allocated vnodes */
#define M_CACHE     26 /* Dynamically allocated cache entries */
#define M_DQUOT     27 /* UFS quota entries */
#define M_UFSMNT    28 /* UFS mount structure */
#define M_SHM       29 /* SVID compatible shared memory segments */
#define M_VMMAP     30 /* VM map structures */
#define M_VMMAPENT  31 /* VM map entry structures */
#define M_VMOBJ     32 /* VM object structure */
#define M_VMOBJHASH 33 /* VM object hash structure */
#define M_VMPMAP    34 /* VM pmap */
#define M_VMPVENT   35 /* VM phys-virt mapping entry */
#define M_VMPAGER   36 /* XXX: VM pager struct */
#define M_VMPGDATA  37 /* XXX: VM pager private data */
#define M_FILE      38 /* Open file structure */
#define M_FILEDESC  39 /* Open file descriptor table */
#define M_LOCKF     40 /* Byte-range locking structures */
#define M_PROC      41 /* Proc structures */
#define M_SUBPROC   42 /* Proc sub-structures */
#define M_SEGMENT   43 /* Segment for LFS */
#define M_LFSNODE   44 /* LFS vnode private part */
#define M_FFSNODE   45 /* FFS vnode private part */
#define M_MFSNODE   46 /* MFS vnode private part */
#define M_NQLEASE   47 /* Nqnfs lease */
#define M_NQMHOST   48 /* Nqnfs host address table */
#define M_NETADDR   49 /* Export host address structure */
#define M_NFSSVC    50 /* Nfs server structure */
#define M_NFSUID    51 /* Nfs uid mapping structure */
#define M_NFSD      52 /* Nfs server daemon structure */
#define M_IPMOPTS   53 /* internet multicast options */
#define M_IPMADDR   54 /* internet multicast address */
#define M_IFMADDR   55 /* link-level multicast address */
#define M_MRTABLE   56 /* multicast routing tables */
#define M_ISOFSMNT  57 /* ISOFS mount structure */
#define M_ISOFSNODE 58 /* ISOFS vnode private part */
#define M_NFSRVDESC 59 /* NFS server socket descriptor */
#define M_NFSDIROFF 60 /* NFS directory offset data */
#define M_NFSBIGFH  61 /* NFS version 3 file handle */
#define M_TEMP      74 /* misc temporary data buffers */
#define M_LAST      75 /* Must be last type + 1 */

#include "mbuf/mbuf.h"
#include <mach/machine/spl.h>
#include <mach/ptr_arith.hh>
#include <mach/sys/protosw.h>
#include <mach/thread.h>
//#include <sys/syslog.h>

#include <algorithm>

#include <kernel/lock.h>
#include <mach/vm/vm_kern.h>
#define CLSIZE  1
#define CLBYTES (CLSIZE * PGBYTES)
#define CLOFSET ((CLSIZE * PGBYTES) - 1) /* for clusters, like PGOFSET */

struct mbstat mbstat;
union mcluster *mclfree;
static vm_map_t mb_map;
struct mbuf *mbutl;
char *mclrefcnt;

int max_linkhdr;  /* largest link-level header */
int max_protohdr; /* largest protocol header */
int max_hdr;      /* largest link+protocol header */
int max_datalen;  /* MHLEN - max_hdr */

//#undef MGET

/*
 * Allocate some number of mbuf clusters
 * and place on cluster free list.
 * Must be called at splimp.
 */
/* ARGSUSED */

static int inited = 0;
static lock_data_t lock_;
static bool mbuf_cluster_alloc(int ncl, bool nowait)
{
        if (inited == 0)
        {
                lock_init(&lock_, false);
                inited++;
        }

        if (nowait)
        {
                if (lock_try_write(&lock_) == false)
                {
                        return false;
                }
        }
        else
        {
                lock_write(&lock_);
        }

        static int logged;
        union mcluster *p;

        int npg = ncl * CLSIZE;

        if (kmem_alloc_aligned(mb_map, (vm_offset_t *)&p, npg * PGBYTES) != KERN_SUCCESS)
        {
                if (logged == 0)
                {
                        logged++;
                        log_error("mb_map full\n");
                }
                lock_write_done(&lock_);
                return false;
        }

        ncl = (ncl * CLBYTES) / MCLBYTES;

        for (int i = 0; i < ncl; i++)
        {
                ((union mcluster *)p)->mcl_next = mclfree;

                mclfree = p;

                p = emerixx::add_ptr<union mcluster>(p, MCLBYTES);

                mbstat.m_clfree++;
        }

        mbstat.m_clusters += ncl;

        lock_write_done(&lock_);

        return true;
}

void *mbuf_cluster_allocate(int how)
{
        int ms = splimp();

        if (mclfree == 0)
        {
                mbuf_cluster_alloc(1, (how));
        }

        void *p;
        if (((p) = (void *)mclfree) != 0)
        {
                size_t idx = mtocl(p);
                KASSERT(idx < 0x100);
                ++mclrefcnt[idx];
                mbstat.m_clfree--;
                mclfree = ((union mcluster *)(p))->mcl_next;
        };

        splx(ms);
        return p;
}

void mbuf_cluster_get(struct mbuf *m, int how)
{
        m->m_ext.ext_buf = mbuf_cluster_allocate(how);

        if ((m)->m_ext.ext_buf != nullptr)
        {
                (m)->m_data = (m)->m_ext.ext_buf;
                (m)->__m_flags |= M_EXT;
                (m)->m_ext.ext_size = MCLBYTES;
        }
}

void mbuf_cluster_free(void *p)
{
        int ms = splimp();

        size_t idx = mtocl(p);
        KASSERT(idx < 0x100);

        if (--mclrefcnt[idx] == 0)
        {
                ((union mcluster *)(p))->mcl_next = mclfree;
                mclfree                           = (union mcluster *)(p);
                mbstat.m_clfree++;
        };

        splx(ms);
}
/*
 * When MGET failes, ask protocols to free space when short of memory,
 * then re-attempt to allocate an mbuf.
 */
struct mbuf *m_retry(int i, int t)
{
        struct mbuf *m;

        m_reclaim();
#define m_retry(i, t) (struct mbuf *)0
        m = mbuf_get(i, t);
#undef m_retry
        return (m);
}

/*
 * As above; retry an MGETHDR.
 */
struct mbuf *m_retryhdr(int i, int t)
{
        struct mbuf *m;

        m_reclaim();
#define m_retryhdr(i, t) (struct mbuf *)0
        m = mbuf_get_hdr(i, t);
#undef m_retryhdr
        return (m);
}

void m_reclaim()
{
        struct domain *dp;
        struct protosw *pr;
        int s = splimp();

        for (dp = domains; dp; dp = dp->dom_next)
        {
                for (pr = dp->dom_protosw; pr < dp->dom_protoswNPROTOSW; pr++)
                {
                        if (pr->pr_drain)
                        {
                                (*pr->pr_drain)();
                        }
                }
        }
        splx(s);
        mbstat.m_drain++;
}

/*
 * Space allocation routines.
 * These are also available as macros
 * for critical paths.
 */
struct mbuf *mbuf_get(int how, int type)
{
        /*MALLOC((m), struct mbuf *, MSIZE, mbtypes[type], (how));*/
        struct mbuf *m = (struct mbuf *)aligned_alloc(MSIZE, MSIZE);
        if (m)
        {
                (m)->m_type = (type);

                int ms = splimp();
                mbstat.m_mtypes[type]++;
                splx(ms);

                (m)->m_next    = (struct mbuf *)NULL;
                (m)->m_nextpkt = (struct mbuf *)NULL;
                (m)->m_data    = (m)->m_dat;
                (m)->__m_flags = 0;
        }
        else
        {
                panic("Will overflow the stack as is");
                (m) = m_retry((how), (type));
        }

        return m;
}

struct mbuf *mbuf_get_hdr(int how, int type)
{
        /*MALLOC((m), struct mbuf *, MSIZE, mbtypes[type], (how));*/
        struct mbuf *m = (struct mbuf *)aligned_alloc(MSIZE, MSIZE);
        if (m)
        {
                (m)->m_type = (type);

                int ms = splimp();
                mbstat.m_mtypes[type]++;
                splx(ms);

                (m)->m_next    = (struct mbuf *)NULL;
                (m)->m_nextpkt = (struct mbuf *)NULL;
                (m)->m_data    = (m)->m_pktdat;
                (m)->__m_flags = M_PKTHDR;
        }
        else
        {
                panic("Will overflow the stack as is");
                (m) = m_retryhdr((how), (type));
        }
        return m;
}

struct mbuf *mbuf_free(struct mbuf *m)
{
        int ms = splimp();
        mbstat.m_mtypes[(m)->m_type]--;
        splx(ms);

        if ((m)->__m_flags & M_EXT)
        {
                mbuf_cluster_free((m)->m_ext.ext_buf);
        }
        struct mbuf *nn = (m)->m_next; /*FREE((m), mbtypes[(m)->m_type]);*/
        free(m);

        return nn;
}

struct mbuf *m_getclr(int nowait, int type)
{
        struct mbuf *m = mbuf_get(nowait, type);

        if (m)
        {
                bzero(mtod(m, void *), MLEN);
        }

        return (m);
}

void m_freem(struct mbuf *m)
{
        while (m != nullptr && (m = mbuf_free(m)))
                ;
}

/*
 * Mbuffer utility routines.
 */

/*
 * Lesser-used path for M_PREPEND:
 * allocate new mbuf to prepend to chain,
 * copy junk along.
 */
struct mbuf *m_prepend(struct mbuf *m, int len, int how)
{
        struct mbuf *mn = mbuf_get(how, m->m_type);

        if (mn == nullptr)
        {
                m_freem(m);
                return nullptr;
        }

        if (m->__m_flags & M_PKTHDR)
        {
                mn->pkthdr_copy(m);
                m->__m_flags &= ~M_PKTHDR;
        }

        mn->m_next = m;
        m          = mn;

        if (len < MHLEN)
        {
                m->hdr_align(len);
        }

        m->m_len = len;
        return (m);
}

/*
 * Make a copy of an mbuf chain starting "off0" bytes from the beginning,
 * continuing for "len" bytes.  If len is M_COPYALL, copy to end of mbuf.
 * The wait parameter is a choice of M_WAIT/M_DONTWAIT from caller.
 */
int MCFail;

struct mbuf *m_copym(struct mbuf *m, int off0, int wait, int len)
{
        struct mbuf *n, **np;
        int off = off0;
        struct mbuf *top;
        int copyhdr = 0;

        if (off < 0 || len < 0)
        {
                panic("m_copym");
        }

        if (off == 0 && m->__m_flags & M_PKTHDR)
        {
                copyhdr = 1;
        }

        while (off > 0)
        {
                if (m == 0)
                {
                        panic("m_copym");
                }

                if (off < m->m_len)
                {
                        break;
                }

                off -= m->m_len;
                m = m->m_next;
        }
        np  = &top;
        top = 0;
        while (len > 0)
        {
                if (m == 0)
                {
                        if (len != M_COPYALL)
                                panic("m_copym");
                        break;
                }

                n = mbuf_get(wait, m->m_type);

                *np = n;

                if (n == 0)
                {
                        goto nospace;
                }

                if (copyhdr)
                {
                        n->pkthdr_copy(m);
                        if (len == M_COPYALL)
                        {
                                n->m_pkthdr.len -= off0;
                        }
                        else
                        {
                                n->m_pkthdr.len = len;
                        }
                        copyhdr = 0;
                }
                n->m_len = std::min(len, m->m_len - off);
                if (m->__m_flags & M_EXT)
                {
                        n->m_data = (void *)(((vm_offset_t)m->m_data) + off);
                        mclrefcnt[mtocl(m->m_ext.ext_buf)]++;
                        n->m_ext = m->m_ext;
                        n->__m_flags |= M_EXT;
                }
                else
                {
                        bcopy((void *)(mtod(m, vm_offset_t) + off), mtod(n, void *), (unsigned)n->m_len);
                }

                if (len != M_COPYALL)
                {
                        len -= n->m_len;
                }

                off = 0;
                m   = m->m_next;
                np  = &n->m_next;
        }

        if (top == 0)
        {
                MCFail++;
        }
        return (top);
nospace:
        m_freem(top);
        MCFail++;
        return (0);
}

/*
 * Copy data from an mbuf chain starting "off" bytes from the beginning,
 * continuing for "len" bytes, into the indicated buffer.
 */
void m_copydata(struct mbuf *m, int off, int len, void *cp)
{
        unsigned count;

        if (off < 0 || len < 0)
        {
                panic("m_copydata");
        }
        while (off > 0)
        {
                if (m == 0)
                {
                        panic("m_copydata");
                }
                if (off < m->m_len)
                {
                        break;
                }
                off -= m->m_len;
                m = m->m_next;
        }
        while (len > 0)
        {
                if (m == 0)
                {
                        panic("m_copydata");
                }

                count = std::min(m->m_len - off, len);
                bcopy((void *)(mtod(m, vm_offset_t) + off), cp, count);
                len -= count;
                cp  = (void *)((vm_offset_t)cp + count);
                off = 0;
                m   = m->m_next;
        }
}

/*
 * Concatenate mbuf chain n to m.
 * Both chains must be of the same type (e.g. MT_DATA).
 * Any m_pkthdr is not updated.
 */
void m_cat(struct mbuf *m, struct mbuf *n)
{
        while (m->m_next)
        {
                m = m->m_next;
        }

        while (n)
        {
                if (m->__m_flags & M_EXT || ((vm_offset_t)m->m_data) + m->m_len + n->m_len >= ((vm_offset_t)&m->m_dat[MLEN]))
                {
                        /* just join the two chains */
                        m->m_next = n;
                        return;
                }
                /* splat the data from one into the other */
                bcopy(mtod(n, void *), (void *)(mtod(m, vm_offset_t) + m->m_len), (size_t)n->m_len);
                m->m_len += n->m_len;
                n = mbuf_free(n);
        }
}

void m_adj(struct mbuf *mp, int req_len)
{
        int len = req_len;
        struct mbuf *m;
        int count;

        if ((m = mp) == NULL)
        {
                return;
        }

        if (len >= 0)
        {
                /*
                 * Trim from head.
                 */
                while (m != NULL && len > 0)
                {
                        if (m->m_len <= len)
                        {
                                len -= m->m_len;
                                m->m_len = 0;
                                m        = m->m_next;
                        }
                        else
                        {
                                m->m_len -= len;
                                m->m_data = (void *)((vm_offset_t)m->m_data + len);
                                len       = 0;
                        }
                }
                m = mp;

                if (mp->__m_flags & M_PKTHDR)
                {
                        m->m_pkthdr.len -= (req_len - len);
                }
        }
        else
        {
                /*
                 * Trim from tail.  Scan the mbuf chain,
                 * calculating its length and finding the last mbuf.
                 * If the adjustment only affects this mbuf, then just
                 * adjust and return.  Otherwise, rescan and truncate
                 * after the remaining size.
                 */
                len   = -len;
                count = 0;
                for (;;)
                {
                        count += m->m_len;
                        if (m->m_next == (struct mbuf *)0)
                        {
                                break;
                        }
                        m = m->m_next;
                }
                if (m->m_len >= len)
                {
                        m->m_len -= len;

                        if (mp->__m_flags & M_PKTHDR)
                        {
                                mp->m_pkthdr.len -= len;
                        }
                        return;
                }

                count -= len;

                if (count < 0)
                {
                        count = 0;
                }
                /*
                 * Correct length for chain is "count".
                 * Find the mbuf with last data, adjust its length,
                 * and toss data from remaining mbufs on chain.
                 */
                m = mp;

                if (m->__m_flags & M_PKTHDR)
                {
                        m->m_pkthdr.len = count;
                }

                for (; m; m = m->m_next)
                {
                        if (m->m_len >= count)
                        {
                                m->m_len = count;
                                break;
                        }
                        count -= m->m_len;
                }

                while (m = m->m_next)
                {
                        m->m_len = 0;
                }
        }
}

/*
 * Rearange an mbuf chain so that len bytes are contiguous
 * and in the data area of an mbuf (so that mtod and dtom
 * will work for a structure of size len).  Returns the resulting
 * mbuf chain on success, frees it and returns null on failure.
 * If there is room, it will add up to max_protohdr-len extra bytes to the
 * contiguous region in an attempt to avoid being called next time.
 */
int MPFail;

struct mbuf *m_pullup(struct mbuf *n, int len)
{
        struct mbuf *m;
        int count;
        int space;

        /*
         * If first mbuf has no cluster, and has room for len bytes
         * without shifting current data, pullup into it,
         * otherwise allocate a new mbuf to prepend to the chain.
         */
        if ((n->__m_flags & M_EXT) == 0 && ((vm_offset_t)n->m_data) + len < ((vm_offset_t)&n->m_dat[MLEN])
            && ((vm_offset_t)n->m_next))
        {
                if (n->m_len >= len)
                {
                        return (n);
                }
                m = n;
                n = n->m_next;
                len -= m->m_len;
        }
        else
        {
                if (len > MHLEN)
                {
                        goto bad;
                }

                m = mbuf_get(M_DONTWAIT, n->m_type);

                if (m == 0)
                {
                        goto bad;
                }

                m->m_len = 0;

                if (n->__m_flags & M_PKTHDR)
                {
                        m->pkthdr_copy(n);
                        n->__m_flags &= ~M_PKTHDR;
                }
        }
        space = ((vm_offset_t)&m->m_dat[MLEN]) - (((vm_offset_t)m->m_data) + m->m_len);
        do
        {
                count = std::min(std::min(std::max(len, max_protohdr), space), n->m_len);
                bcopy(mtod(n, void *), (void *)(mtod(m, vm_offset_t) + m->m_len), (unsigned)count);
                len -= count;
                m->m_len += count;
                n->m_len -= count;
                space -= count;

                if (n->m_len)
                {
                        n->m_data = (void *)(((vm_offset_t)n->m_data) + count);
                }
                else
                {
                        n = mbuf_free(n);
                }
        } while (len > 0 && n);

        if (len > 0)
        {
                mbuf_free(m);
                goto bad;
        }

        m->m_next = n;

        return (m);
bad:
        m_freem(n);
        MPFail++;
        return (0);
}

/*
 * Partition an mbuf chain in two pieces, returning the tail --
 * all but the first len0 bytes.  In case of failure, it returns NULL and
 * attempts to restore the chain to its original state.
 */
struct mbuf *m_split(struct mbuf *m0, int len0, int wait)
{
        struct mbuf *m, *n;
        unsigned len = len0, remain;

        for (m = m0; m && len > m->m_len; m = m->m_next)
        {
                len -= m->m_len;
        }

        if (m == 0)
        {
                return 0;
        }

        remain = m->m_len - len;

        if (m0->__m_flags & M_PKTHDR)
        {
                n = mbuf_get_hdr(wait, m0->m_type);
                if (n == 0)
                {
                        return 0;
                }

                n->m_pkthdr.rcvif = m0->m_pkthdr.rcvif;
                n->m_pkthdr.len   = m0->m_pkthdr.len - len0;
                m0->m_pkthdr.len  = len0;

                if (m->__m_flags & M_EXT)
                {
                        goto extpacket;
                }

                if (remain > MHLEN)
                {
                        /* m can't be the lead packet */
                        n->hdr_align(0);

                        n->m_next = m_split(m, len, wait);

                        if (n->m_next == 0)
                        {
                                mbuf_free(n);
                                return (0);
                        }
                        else
                        {
                                return (n);
                        }
                }
                else
                {
                        n->hdr_align(remain);
                }
        }
        else if (remain == 0)
        {
                n         = m->m_next;
                m->m_next = 0;
                return (n);
        }
        else
        {
                n = mbuf_get(wait, m->m_type);
                if (n == 0)
                        return (0);
                n->align(remain);
        }
extpacket:
        if (m->__m_flags & M_EXT)
        {
                n->__m_flags |= M_EXT;
                n->m_ext = m->m_ext;
                mclrefcnt[mtocl(m->m_ext.ext_buf)]++;
                m->m_ext.ext_size = 0; /* For Accounting XXXXXX danger */
                n->m_data         = ((void *)(((vm_offset_t)m->m_data) + len));
        }
        else
        {
                bcopy((void *)(mtod(m, vm_offset_t) + len), mtod(n, void *), remain);
        }
        n->m_len  = remain;
        m->m_len  = len;
        n->m_next = m->m_next;
        m->m_next = 0;
        return (n);
}
/*
 * Routine to copy from device local memory into mbufs.
 */
struct mbuf *m_devget(char *buf, int totlen, int off0, struct ifnet *ifp, void (*copy)(void *, void *, size_t))
{
        struct mbuf *m;
        struct mbuf *top = 0, **mp = &top;
        int off = off0, len;
        char *cp;
        char *epkt;

        cp   = buf;
        epkt = cp + totlen;
        if (off)
        {
                /*
                 * If 'off' is non-zero, packet is trailer-encapsulated,
                 * so we have to skip the type and length fields.
                 */
                cp += off + 2 * sizeof(u16);
                totlen -= 2 * sizeof(u16);
        }
        m = mbuf_get_hdr(M_DONTWAIT, MT_DATA);

        if (m == 0)
        {
                return 0;
        }
        m->m_pkthdr.rcvif = ifp;
        m->m_pkthdr.len   = totlen;
        m->m_len          = MHLEN;

        while (totlen > 0)
        {
                if (top)
                {
                        m = mbuf_get(M_DONTWAIT, MT_DATA);
                        if (m == 0)
                        {
                                m_freem(top);
                                return (0);
                        }
                        m->m_len = MLEN;
                }

                len = std::min(totlen, (int)(epkt - cp));

                if (len >= MINCLSIZE)
                {
                        mbuf_cluster_get(m, M_DONTWAIT);
                        if (m->__m_flags & M_EXT)
                        {
                                m->m_len = len = std::min(len, MCLBYTES);
                        }
                        else
                        {
                                len = m->m_len;
                        }
                }
                else
                {
                        /*
                         * Place initial small packet/header at end of mbuf.
                         */
                        if (len < m->m_len)
                        {
                                if (top == 0 && len + max_linkhdr <= m->m_len)
                                {
                                        m->m_data = (void *)(((vm_offset_t)m->m_data) + max_linkhdr);
                                }
                                m->m_len = len;
                        }
                        else
                        {
                                len = m->m_len;
                        }
                }
                if (copy)
                {
                        copy(cp, mtod(m, void *), (unsigned)len);
                }
                else
                {
                        bcopy(cp, mtod(m, void *), (unsigned)len);
                }
                cp += len;
                *mp = m;
                mp  = &m->m_next;
                totlen -= len;
                if (cp == epkt)
                {
                        cp = buf;
                }
        }
        return top;
}

/* change mbuf to new type */
void mbuf::change_type(int t)
{
        int ms = splimp();
        mbstat.m_mtypes[m_hdr.mh_type]--;
        mbstat.m_mtypes[t]++;
        splx(ms);
        m_hdr.mh_type = t;
}

static int mbinit()
{
#define VM_MBUF_SIZE ((NMBCLUSTERS * MCLBYTES))
        /*
         * Finally, allocate mbuf pool.  Since mclrefcnt is an off-size
         * we use the more space efficient malloc in place of kmem_alloc.
         */
        mclrefcnt = (char *)malloc(NMBCLUSTERS + CLBYTES / MCLBYTES /*,  M_MBUF, M_NOWAIT*/);
        bzero(mclrefcnt, NMBCLUSTERS + CLBYTES / MCLBYTES);

        vm_address_t maxaddr;
        mb_map = kmem_suballoc(kernel_map, (vm_offset_t *)&mbutl, &maxaddr, VM_MBUF_SIZE, FALSE);

        int s = splimp();

        int error = mbuf_cluster_alloc(std::max(4096 / CLBYTES, 1), M_DONTWAIT) == false;

        splx(s);

        return error;
}

subsys_initcall(mbinit);