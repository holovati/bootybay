/*
 * Copyright (c) 1982, 1986, 1988, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)mbuf.h	8.5 (Berkeley) 2/19/95
 */
#include <mach/vm_param.h>
/*
 * Mbufs are of a single size, MSIZE (machine/machparam.h), which
 * includes overhead.  An mbuf may add a single "mbuf cluster" of size
 * MCLBYTES (also in machine/machparam.h), which has no additional overhead
 * and is used instead of the internal data area; this is done when
 * at least MINCLSIZE of data must be stored.
 */

#define MLEN  (MSIZE - sizeof(struct m_hdr)) /* normal data len */
#define MHLEN (MLEN - sizeof(struct pkthdr)) /* data len w/pkthdr */

#define MINCLSIZE     (MHLEN + MLEN) /* smallest amount to put in cluster */
#define M_MAXCOMPRESS (MHLEN / 2)    /* max amount to copy for compression */

/*
 * Macros for type conversion
 * mtod(m,t) -	convert mbuf pointer to data pointer of correct type
 * dtom(x) -	convert data pointer within mbuf to mbuf pointer (XXX)
 * mtocl(x) -	convert pointer within cluster to cluster index #
 * cltom(x) -	convert cluster # to ptr to beginning of cluster
 */
#define mtod(m, t) ((t)((m)->m_data))
#define dtom(x)    ((struct mbuf *)((long)(x) & ~(MSIZE - 1)))
#define mtocl(x)   (((vm_offset_t)(x) - (vm_offset_t)mbutl) >> MCLSHIFT)
#define cltom(x)   ((void *)((vm_offset_t)mbutl + ((vm_offset_t)(x) << MCLSHIFT)))

/* header at beginning of each mbuf: */
struct m_hdr
{
        struct mbuf *mh_next;    /* next buffer in chain */
        struct mbuf *mh_nextpkt; /* next chain in queue/record */
        void *mh_data;           /* location of data */
        int mh_len;              /* amount of data in this mbuf */
        short mh_type;           /* type of data in this mbuf */
        short mh_flags;          /* flags; see below */
};

/* record/packet header in first mbuf of chain; valid if M_PKTHDR set */
struct pkthdr
{
        struct ifnet *rcvif; /* rcv interface */
        int len;             /* total packet length */
};

/* description of external storage mapped into mbuf, valid if M_EXT set */
struct m_ext
{
        void *ext_buf;      /* start of buffer */
        void (*ext_free)(); /* free routine if not the usual */
        size_t ext_size;    /* size of buffer, for ext_free */
};

/* mbuf flags */
#define M_EXT    0x0001 /* has associated external storage */
#define M_PKTHDR 0x0002 /* start of record */
#define M_EOR    0x0004 /* end of record */

/* mbuf pkthdr flags, also in m_flags */
#define M_BCAST 0x0100 /* send/received as link-level broadcast */
#define M_MCAST 0x0200 /* send/received as link-level multicast */

/* flags copied when copying m_pkthdr */
#define M_COPYFLAGS (M_PKTHDR | M_EOR | M_BCAST | M_MCAST)

/* mbuf types */
#define MT_FREE    0 /* should be on free list */
#define MT_DATA    1 /* dynamic (data) allocation */
#define MT_HEADER  2 /* packet header */
#define MT_SOCKET  3 /* socket structure */
#define MT_PCB     4 /* protocol control block */
#define MT_RTABLE  5 /* routing tables */
#define MT_HTABLE  6 /* IMP host tables */
#define MT_ATABLE  7 /* address resolution tables */
#define MT_SONAME  8 /* socket name */
#define MT_SOOPTS  10 /* socket options */
#define MT_FTABLE  11 /* fragment reassembly header */
#define MT_RIGHTS  12 /* access rights */
#define MT_IFADDR  13 /* interface address */
#define MT_CONTROL 14 /* extra-data protocol message */
#define MT_OOBDATA 15 /* expedited data  */

/* flags to m_get/MGET */
#define M_DONTWAIT /*M_NOWAIT*/ 0
#define M_WAIT     /*M_WAITOK*/ 1

struct mbuf
{
        struct m_hdr m_hdr;
        union {
                struct
                {
                        struct pkthdr MH_pkthdr; /* M_PKTHDR set */
                        union {
                                struct m_ext MH_ext; /* M_EXT set */
                                char MH_databuf[MHLEN];
                        } MH_dat;
                } MH;
                char M_databuf[MLEN]; /* !M_PKTHDR, !M_EXT */
        } M_dat;

        /*
         * Copy mbuf pkthdr from from to to. from must have M_PKTHDR set, and to must be empty.
         */
        void pkthdr_copy(struct mbuf *from)
        {
                M_dat.MH.MH_pkthdr = from->M_dat.MH.MH_pkthdr;
                m_hdr.mh_flags     = from->m_hdr.mh_flags & M_COPYFLAGS;
                m_hdr.mh_data      = M_dat.MH.MH_dat.MH_databuf;
        }

        /*
         * Set the m_data pointer of a newly-allocated mbuf (m_get/MGET) to place an object of the specified size at the end of the
         * mbuf, longword aligned.
         */
        void align(size_t len) { m_hdr.mh_data = (void *)(((vm_offset_t)m_hdr.mh_data) + (MLEN - (len)) & ~(sizeof(size_t) - 1)); }

        /*
         * As above, for mbufs allocated with m_gethdr/MGETHDR or initialized by M_COPY_PKTHDR.
         */
        void hdr_align(size_t len)
        {
                m_hdr.mh_data = (void *)(((vm_offset_t)m_hdr.mh_data) + (MHLEN - (len)) & ~(sizeof(long) - 1));
        }

        /*
         * Compute the amount of space available before the current start of data in an mbuf.
         */
        size_t get_leadingspace()
        {
                size_t result;
                if (m_hdr.mh_flags & M_EXT)
                {
                        /* (m)->m_data - (m)->m_ext.ext_buf */
                        result = 0;
                }
                else if (m_hdr.mh_flags & M_PKTHDR)
                {
                        result = (vm_address_t)(m_hdr.mh_data) - (vm_address_t)(M_dat.MH.MH_dat.MH_databuf);
                }
                else
                {
                        result = (vm_address_t)(m_hdr.mh_data) - (vm_address_t)(M_dat.M_databuf);
                }

                return result;
        }
        /*
         * Compute the amount of space available after the end of data in an mbuf.
         */
        size_t get_trailingspace()
        {
                size_t result;
                if (m_hdr.mh_flags & M_EXT)
                {
                        result = ((vm_address_t)M_dat.MH.MH_dat.MH_ext.ext_buf) + ((vm_address_t)M_dat.MH.MH_dat.MH_ext.ext_size)
                                 - ((vm_address_t)m_hdr.mh_data + m_hdr.mh_len);
                }
                else
                {
                        result = (((vm_address_t)&M_dat.M_databuf[MLEN]) - (((vm_address_t)m_hdr.mh_data) + m_hdr.mh_len));
                }
                return result;
        }

        /* change mbuf to new type */
        void change_type(int t);
};

#define m_next    m_hdr.mh_next
#define m_len     m_hdr.mh_len
#define m_data    m_hdr.mh_data
#define m_type    m_hdr.mh_type
#define __m_flags m_hdr.mh_flags
#define m_nextpkt m_hdr.mh_nextpkt
#define m_act     m_nextpkt
#define m_pkthdr  M_dat.MH.MH_pkthdr
#define m_ext     M_dat.MH.MH_dat.MH_ext
#define m_pktdat  M_dat.MH.MH_dat.MH_databuf
#define m_dat     M_dat.M_databuf

/*
 * mbuf allocation/deallocation macros:
 *
 *	MGET(struct mbuf *m, int how, int type)
 * allocates an mbuf and initializes it to contain internal data.
 *
 *	MGETHDR(struct mbuf *m, int how, int type)
 * allocates an mbuf and initializes it to contain a packet header
 * and internal data.
 */
struct mbuf *mbuf_get(int how, int type);
struct mbuf *mbuf_get_hdr(int how, int type);

/*
 * Mbuf cluster macros.
 * MCLALLOC(void* p, int how) allocates an mbuf cluster.
 * MCLGET adds such clusters to a normal mbuf; the flag M_EXT is set upon success.
 * MCLFREE releases a reference to a cluster allocated by MCLALLOC,
 * freeing the cluster if the reference count has reached 0.
 *
 * Normal mbuf clusters are normally treated as character arrays
 * after allocation, but use the first word of the buffer as a free list
 * pointer while on the free list.
 */
union mcluster {
        union mcluster *mcl_next;
        char mcl_buf[MCLBYTES];
};

void *mbuf_cluster_allocate(int how);
void mbuf_cluster_get(struct mbuf *m, int how);
void mbuf_cluster_free(void *p);

/*
 * MFREE(struct mbuf *m, struct mbuf *n)
 * Free a single mbuf and associated external storage.
 * Place the successor, if any, in n.
 */
struct mbuf *mbuf_free(struct mbuf *m);

/*
 * Arrange to prepend space of size plen to mbuf m.
 * If a new mbuf must be allocated, how specifies whether to wait.
 * If how is M_DONTWAIT and allocation fails, the original mbuf chain
 * is freed and m is set to NULL.
 */
#define M_PREPEND(m, plen, how)                                                                                                    \
        {                                                                                                                          \
                if (M_LEADINGSPACE(m) >= (plen))                                                                                   \
                {                                                                                                                  \
                        (m)->m_data -= (plen);                                                                                     \
                        (m)->m_len += (plen);                                                                                      \
                }                                                                                                                  \
                else                                                                                                               \
                        (m) = m_prepend((m), (plen), (how));                                                                       \
                if ((m) && (m)->__m_flags & M_PKTHDR)                                                                              \
                        (m)->m_pkthdr.len += (plen);                                                                               \
        }

/* length to m_copy to copy all */
#define M_COPYALL 1000000000

/* compatiblity with 4.3 */
#define m_copy(m, o, l) m_copym((m), (o), (l), M_DONTWAIT)

/*
 * Mbuf statistics.
 */
struct mbstat
{
        u32 m_mbufs;       /* mbufs obtained from page pool */
        u32 m_clusters;    /* clusters obtained from page pool */
        u32 m_spare;       /* spare field */
        u32 m_clfree;      /* free clusters */
        u32 m_drops;       /* times failed to find space */
        u32 m_wait;        /* times waited for space */
        u32 m_drain;       /* times drained protocols for space */
        u16 m_mtypes[256]; /* type specific mbuf allocations */
};

#ifdef KERNEL
extern struct mbuf *mbutl; /* virtual address of mclusters */
extern char *mclrefcnt;    /* cluster reference counts */
extern struct mbstat mbstat;
extern int nmbclusters;
extern union mcluster *mclfree;
extern int max_linkhdr;  /* largest link-level header */
extern int max_protohdr; /* largest protocol header */
extern int max_hdr;      /* largest link+protocol header */
extern int max_datalen;  /* MHLEN - max_hdr */
extern int mbtypes[];    /* XXX */

struct mbuf *m_copym(struct mbuf *, int, int, int);
struct mbuf *m_getclr(int, int);
struct mbuf *m_prepend(struct mbuf *, int, int);
struct mbuf *m_pullup(struct mbuf *, int);
struct mbuf *m_retry(int, int);
struct mbuf *m_retryhdr(int, int);
void m_adj(struct mbuf *, int);
void m_copyback(struct mbuf *, int, int, void *);
void m_freem(struct mbuf *);
void m_reclaim();

#ifdef MBTYPES
int mbtypes[] = {
    /* XXX */
    M_FREE,   /* MT_FREE	0	   should be on free list */
    M_MBUF,   /* MT_DATA	1	   dynamic (data) allocation */
    M_MBUF,   /* MT_HEADER	2	   packet header */
    M_SOCKET, /* MT_SOCKET	3	   socket structure */
    M_PCB,    /* MT_PCB	4	   protocol control block */
    M_RTABLE, /* MT_RTABLE	5	   routing tables */
    M_HTABLE, /* MT_HTABLE	6	   IMP host tables */
    0,        /* MT_ATABLE	7	   address resolution tables */
    M_MBUF,   /* MT_SONAME	8	   socket name */
    0,        /* 		9 */
    M_SOOPTS, /* MT_SOOPTS	10	   socket options */
    M_FTABLE, /* MT_FTABLE	11	   fragment reassembly header */
    M_MBUF,   /* MT_RIGHTS	12	   access rights */
    M_IFADDR, /* MT_IFADDR	13	   interface address */
    M_MBUF,   /* MT_CONTROL	14	   extra-data protocol message */
    M_MBUF,   /* MT_OOBDATA	15	   expedited data  */
#ifdef DATAKIT
    25,       26, 27, 28, 29, 30, 31, 32 /* datakit ugliness */
#endif
};
#endif
#endif
