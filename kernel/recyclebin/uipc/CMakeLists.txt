# ##############################################################################
# Export objects
# ##############################################################################
#add_library(uipc OBJECT uipc_syscalls.cc uipc_socket.cc uipc_socket2.cc uipc_domain.cc uipc_mbuf.cc uipc_socketops.cc uipc_usrreq.cc uipc_proto.cc)

add_library(
  uipc STATIC
  uipc_syscalls.cc
  uipc_socket.cc
  uipc_socket2.cc
  uipc_domain.cc
  mbuf/uipc_mbuf.cc
  uipc_socketops.cc
  uipc_usrreq.cc
  uipc_proto.cc
)

target_include_directories(uipc
  PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
  PRIVATE
  ${CMAKE_CURRENT_SOURCE_DIR}
  $<TARGET_PROPERTY:emerixx,INTERFACE_INCLUDE_DIRECTORIES>
)