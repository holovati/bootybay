
#include <emerixx/file.h>
#include <fcntl.h>
#include <mach/dirent.h>
#include <mach/filedesc.h>
#include <vfs/mount.h>

#include <mach/ucred.h>
#include <string.h>
#include <sys/stat.h>
#include <vector>
#include <vfs/vfs.h>
#include <vfs/vfs_cache.h>
#include <vfs/vnode.h>

/*
 * Set the mode mask for creation of filesystem nodes.
 */
int vfs_umask(pthread_t a_procp, mode_t a_newmask)
{
        struct filedesc *fdp;
        fdp            = a_procp->process->p_fd;
        mode_t oldmask = fdp->fd_cmask;
        fdp->fd_cmask  = a_newmask & ALLPERMS;
        return oldmask;
}
