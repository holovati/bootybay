#pragma once

#define EXT2_HTREE_EOF 0x7FFFFFFF

enum ext2_htree_hash_version : u8
{
        EXT2_HTREE_LEGACY = 0,
        EXT2_HTREE_HALF_MD4,
        EXT2_HTREE_TEA,
        EXT2_HTREE_LEGACY_UNSIGNED,
        EXT2_HTREE_HALF_MD4_UNSIGNED,
        EXT2_HTREE_TEA_UNSIGNED,
};

kern_return_t ext2_htree_hash(const char *name,
                              int len,
                              uint32_t *hash_seed,
                              ext2_htree_hash_version hash_version,
                              uint32_t *hash_major,
                              uint32_t *hash_minor);

struct ext2_dx_root_info
{
        u32 reserved_zero;                    // Zero.
        ext2_htree_hash_version hash_version; // Hash type.
        u8 length;                            // Length of the tree information, 0x8.
        u8 info_levels; // Depth of the htree. Cannot be larger than 3 if the INCOMPAT_LARGEDIR feature is set; cannot be larger
                        // than 2 otherwise.
        u8 unused_flags;
};
static_assert(sizeof(ext2_dx_root_info) == 0x08);

struct ext2_dx_tail
{
        u32 dt_reserved; // Zero
        u32 dt_checksum; // Checksum of the htree directory block.
};
static_assert(sizeof(ext2_dx_tail) == 0x08);

struct ext2_dx_entry
{
        u32 hash;  // Hash code.
        u32 block; // Block number (within the directory file, not filesystem blocks) of the next node in the htree.
};
static_assert(sizeof(ext2_dx_entry) == 0x08);

struct ext2_dx_root
{
        // The . direntry
        ext2_dir_entry dot;
        i8 dot_name[4]; // “.\0\0\0”

        // The .. direntry
        ext2_dir_entry dotdot;
        i8 dotdot_name[4]; // “..\0\0”

        ext2_dx_root_info root_info;

        u16 limit; // Maximum number of ext2_dx_entries that can follow this header, plus 1 for the header itself.
        u16 count; // Actual number of ext2_dx_entries that follow this header, plus 1 for the header itself.
        u32 block; // The block number (within the directory file) that goes with hash=0.

        ext2_dx_entry entries[0]; // As many 8-byte struct ext2_dx_entry as fits in the rest of the data block.
};
static_assert(sizeof(ext2_dx_root) == 0x28);

struct ext2_dx_node
{
        ext2_dir_entry fake;

        u16 limit; // Maximum number of ext2_dx_entries that can follow this header, plus 1 for the header itself.
        u16 count; // Actual number of ext2_dx_entries that follow this header, plus 1 for the header itself.
        u32 block; // The block number (within the directory file) that goes with the lowest hash value of this block. This value is
                   // stored in the parent block.

        ext2_dx_entry entries[0]; // As many 8-byte struct ext2_dx_entry as fits in the rest of the data block.
};
static_assert(sizeof(ext2_dx_node) == 0x10);
