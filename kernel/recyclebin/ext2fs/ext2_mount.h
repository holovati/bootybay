/*
 * Copyright (c) 1982, 1986, 1989, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)ext2mount.h	8.2 (Berkeley) 1/12/94
 */
#pragma once
#include <vfs/mount.h>

#include "ext2_fs.h"
#include "ext2_inode.hh"
/*
 * Structure of the super block
 */
struct ext2_super_block
{
        u32 s_inodes_count;      /* Inodes count */
        u32 s_blocks_count;      /* Blocks count */
        u32 s_r_blocks_count;    /* Reserved blocks count */
        u32 s_free_blocks_count; /* Free blocks count */
        u32 s_free_inodes_count; /* Free inodes count */
        u32 s_first_data_block;  /* First Data Block */
        u32 s_log_block_size;    /* Block size */
        i32 s_log_frag_size;     /* Fragment size */
        u32 s_blocks_per_group;  /* # Blocks per group */
        u32 s_frags_per_group;   /* # Fragments per group */
        u32 s_inodes_per_group;  /* # Inodes per group */
        u32 s_mtime;             /* Mount time */
        u32 s_wtime;             /* Write time */
        u16 s_mnt_count;         /* Mount count */
        i16 s_max_mnt_count;     /* Maximal mount count */
        u16 s_magic;             /* Magic signature */
        u16 s_state;             /* File system state */
        u16 s_errors;            /* Behaviour when detecting errors */
        u16 s_pad;
        u32 s_lastcheck;     /* time of last check */
        u32 s_checkinterval; /* max. time between checks */
        u32 s_creator_os;    /* OS */
        u32 s_rev_level;     /* Revision level */
        u16 s_def_resuid;    /* Default uid for reserved blocks */
        u16 s_def_resgid;    /* Default gid for reserved blocks */
        // -- EXT2_DYNAMIC_REV Specific --
        u32 s_first_ino;      /* First non-reserved inode */
        u16 s_inode_size;     /* Size of inode structure, in bytes */
        u16 s_block_group_nr; /* Block group # of this superblock */
        u32 s_feature_compat; /* Compatible feature set flags. Kernel can still read/write this fs even if it doesn’t understand a
                                flag; */

        u32 s_feature_incompat; /* Incompatible feature set. If the kernel or fsck doesn’t understand one of these bits, it should
                                   stop */

        u32 s_feature_ro_compat; /* Readonly-compatible feature set. If the kernel doesn’t understand one of these bits, it can
                                    still mount read-only */

        u8 s_uuid[16]; /* 128-bit UUID for volume */

        i8 s_volume_name[16]; /* Volume label */

        i8 s_last_mounted[64]; /*Directory where filesystem was last mounted */

        u32 s_algorithm_usage_bitmap; /* For compression (Not used in e2fsprogs/Linux) */

        // -- Performance hints. Directory preallocation should only happen if the EXT4_FEATURE_COMPAT_DIR_PREALLOC flag is on. --
        u8 s_prealloc_blocks;      /*#. of blocks to try to preallocate for … files? (Not used in e2fsprogs/Linux) */
        u8 s_prealloc_dir_blocks;  /* #. of blocks to preallocate for directories. (Not used in e2fsprogs/Linux) */
        u16 s_reserved_gdt_blocks; /* Number of reserved GDT entries for future filesystem expansion. */

        // -- Journalling support is valid only if EXT4_FEATURE_COMPAT_HAS_JOURNAL is set
        u8 s_journal_uuid[16]; /* UUID of journal superblock */
        u32 s_journal_inum;    /* inode number of journal file */
        u32 s_journal_dev;     /* Device number of journal file, if the external journal feature flag is set. */
        u32 s_last_orphan;     /* Start of list of orphaned inodes to delete. */
        u32 s_hash_seed[4];    /*HTREE hash seed. */

        u8 s_reserved[772]; /* Padding to the end of the block */
};

static_assert(sizeof(ext2_super_block) == 1024, "superblock wrong size");

/*
 * The following is not needed anymore since the descriptors buffer
 * heads are now dynamically allocated
 */
/* #define EXT2_MAX_GROUP_DESC	8 */

//#define EXT2_MAX_GROUP_LOADED 8

//#define buffer_head buf
#define MAXMNTLEN 512

/*
 * second extended-fs super-block data in memory
 */
struct ext2_sb_info
{
        u32 s_frag_size;        /* Size of a fragment in bytes */
        u32 s_frags_per_block;  /* Number of fragments per block */
        u32 s_inodes_per_block; /* Number of inodes per block */
        u32 s_frags_per_group;  /* Number of fragments in a group */
        u32 s_blocks_per_group; /* Number of blocks in a group */
        u32 s_inodes_per_group; /* Number of inodes in a group */
        u32 s_itb_per_group;    /* Number of inode table blocks per group */
        u32 s_db_per_group;     /* Number of descriptor blocks per group */
        u32 s_desc_per_block;   /* Number of group descriptors per block */
        u32 s_groups_count;     /* Number of groups in the fs */
        u32 s_mount_opt;
        i32 s_rename_lock;
        u32 s_blocksize;
        u32 s_blocksize_bits;
        u32 s_bshift;  /* = log2(s_blocksize) */
        u32 s_fsbtodb; /* shift to get disk block */
        u32 s_loaded_inode_bitmaps;
        u32 s_loaded_block_bitmaps;
        u32 s_resuid;
        u32 s_resgid;
        u32 s_mount_state;
        u32 s_rd_only; /* read-only 		*/
        u32 s_dirt;    /* fs modified flag */
        // u32 s_inode_bitmap_number[EXT2_MAX_GROUP_LOADED];
        // u32 s_block_bitmap_number[EXT2_MAX_GROUP_LOADED];
        u32 s_qbmask; /* = s_blocksize - 1 */

        struct ext2_super_block s_es; /* Pointer to the super block in the buffer */

        struct ext2_group_desc *s_group_desc;

        // char *s_inode_bitmap[EXT2_MAX_GROUP_LOADED];
        // char *s_block_bitmap[EXT2_MAX_GROUP_LOADED];

        // struct buffer_head *s_sbh; /* Buffer containing the super block */

        // struct buffer_head **s_group_desc;
        // struct buffer_head *s_inode_bitmap[EXT2_MAX_GROUP_LOADED];
        // struct buffer_head *s_block_bitmap[EXT2_MAX_GROUP_LOADED];

        char fs_fsmnt[MAXMNTLEN]; /* name mounted on */
};

/* This structure describes the UFS specific mount structure data. */
struct ext2mount final : mount
{
        // struct mount *um_mountp;            /* filesystem vfs structure */
        struct ext2_sb_info e2fs; /* pointer to superblock */
        vnode::ptr um_devvp;      /* block device mounted vnode */
        // struct vnode *um_quotas[MAXQUOTAS]; /* pointer to quota files */
        // struct ucred *um_cred[MAXQUOTAS];   /* quota file access cred */
        size_t um_nindir;             /* indirect ptrs per block */
        size_t um_nindir_pow_2;       /* indirect ptrs 2nd lvl indirect block */
        size_t um_first_indir_count;  /* Number of pointers addressable by the single indirect block */
        size_t um_second_indir_count; /* Number of pointers addressable by the double indirect block */
        size_t um_triple_indir_count; /* Number of pointers addressable by the triple indirect block */

        // size_t um_bptrtodb;         /* indir ptr to disk block */
        size_t um_seqinc; /* inc between seq blocks */
        // time_t um_btime[MAXQUOTAS]; /* block quota time limit */
        // time_t um_itime[MAXQUOTAS]; /* inode quota time limit */
        // char um_qflags[MAXQUOTAS];  /* quota specific flags */
        // struct netexport um_export; /* export information */
        inode::cache_map_t inode_cache;

        ext2mount(vnode::ptr &a_devvp, vnode::ptr &vncovered, integer_t flags);
        ~ext2mount();

        int start(int flags, pthread_t p) override;

        int unmount(int, pthread_t) override
        {
                panic("NI");
                return KERN_SUCCESS;
        };

        int root(struct vnode::ptr &vpp) override;

        int quotactl(int cmds, uid_t uid, char *arg, pthread_t p) override;

        int statfs(struct statfs *sbp) override;

        int sync(int waitfor) override;

        int vget(emerixx::dirent_t *a_dirent, vnode *a_parent, vnode **a_vnode_out) override;

        void vnode_reclaim(vnode *vp) override;

        int generate_lbn_path(size_t a_lbn, lbn_path_t a_lbn_path, size_t *a_lbn_path_len, size_t *a_start_idx);

        kern_return_t fsio_read(size_t a_lbn, buffer_cache::io_descriptor **a_io_desc_out);

        kern_return_t fsio_get(size_t a_lbn, buffer_cache::io_descriptor **a_io_desc_out);

        kern_return_t read_group_descriptor(size_t a_block_group_number,
                                            ext2_group_desc **a_group_desc_out,
                                            buffer_cache::io_descriptor **a_io_descder_out);

        kern_return_t write_group_descriptor(size_t a_block_group_number, ext2_group_desc *a_group_desc);

        kern_return_t block_alloc(inode *a_inode, u32 *a_fs_blkno_out);

        kern_return_t block_free(vnode *a_vnode, vm_offset_t a_offset, size_t a_count) override;

        kern_return_t inode_free(inode *a_inode);

        kern_return_t inode_alloc(inode *a_parent, mode_t a_mode, ino_t *a_inode_num_out);

        static ext2mount *get(inode *a_inode) { return static_cast<ext2mount *>(a_inode->v_mount); }

        static void initialize();
};
