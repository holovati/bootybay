#pragma once

struct ext2mount;
struct ext2_sb_info;
struct ext2_group_desc;

#define INODE_FREE(e, m, f) bitmap_free<&ext2_sb_info::s_inodes_per_group, &ext2_group_desc::bg_inode_bitmap>(e, m, f)
#define BLOCK_FREE(e, m, f) bitmap_free<&ext2_sb_info::s_blocks_per_group, &ext2_group_desc::bg_block_bitmap>(e, m, f)

#define INODE_ALLOC(e, m, f) bitmap_alloc<&ext2_sb_info::s_inodes_per_group, &ext2_group_desc::bg_inode_bitmap>(e, m, f)
#define BLOCK_ALLOC(e, m, f) bitmap_alloc<&ext2_sb_info::s_blocks_per_group, &ext2_group_desc::bg_block_bitmap>(e, m, f)

template <u32 ext2_sb_info::*X_PER_GROUP, u32 ext2_group_desc::*X_BITMAP, typename FUNCTOR>
static inline kern_return_t bitmap_free(u32 a_element_num, ext2mount *a_mnt, FUNCTOR f)
{
        kern_return_t ret;

        u32 block_group = (a_element_num - 1) / a_mnt->e2fs.*X_PER_GROUP;
        u32 bitmap_bit  = (a_element_num - 1) % a_mnt->e2fs.*X_PER_GROUP;

        ext2_group_desc *gr_desc = &a_mnt->e2fs.s_group_desc[block_group];

        buffer_cache::io_descriptor *io_desc;

        ret = a_mnt->fsio_read(gr_desc->*X_BITMAP, &io_desc);

        KASSERT(ret == KERN_SUCCESS);

        u32 *bitmap;
        io_desc->get_data(&bitmap);

        u32 bitmap_offset     = static_cast<u32>(bitmap_bit / (sizeof(u32) * CHAR_BIT));
        u32 bitmap_offset_bit = static_cast<u32>(bitmap_bit % (sizeof(u32) * CHAR_BIT));

        KASSERT(bitmap[bitmap_offset] & BIT(bitmap_offset_bit));

        bitmap[bitmap_offset] &= ~BIT(bitmap_offset_bit);

        buffer_cache::write(io_desc);

        f(a_mnt, gr_desc);

        return ret;
}

template <typename FUNCTOR> static inline kern_return_t bitmap_search(u32 a_start_group, ext2mount *a_mnt, FUNCTOR f)
{
        u32 back         = a_start_group - 1;
        u32 forward      = a_start_group + 1;
        u32 grdesc_count = a_mnt->e2fs.s_groups_count;

        KASSERT(a_start_group < grdesc_count);

        kern_return_t retval;

        if (retval = f(a_start_group); retval == KERN_SUCCESS)
        {
                do
                {
                        if (back < grdesc_count)
                        {
                                if (retval = f(back); retval != KERN_SUCCESS)
                                {
                                        break;
                                }
                                back--;
                        }

                        if (forward < grdesc_count)
                        {
                                if (retval = f(forward); retval != KERN_SUCCESS)
                                {
                                        break;
                                }
                                forward++;
                        }

                } while (forward < grdesc_count || back < grdesc_count);
        }

        return retval == KERN_SUCCESS ? KERN_FAIL(ENOSPC) : retval;
}

template <u32 ext2_sb_info::*X_PER_GROUP, u32 ext2_group_desc::*X_BITMAP, typename FUNCTOR>
static inline kern_return_t bitmap_alloc(u32 a_start_elem_num, ext2mount *a_mnt, FUNCTOR f)
{
        auto bitmap_lookup = [=](u32 a_grnum) -> kern_return_t {
                ext2_group_desc *grdesc = &a_mnt->e2fs.s_group_desc[a_grnum];

                buffer_cache::io_descriptor *io_desc;
                kern_return_t retval = a_mnt->fsio_read(grdesc->*X_BITMAP, &io_desc);

                if (retval != KERN_SUCCESS)
                {
                        return retval;
                }

                u32 *bitmap;
                io_desc->get_data(&bitmap);

                u32 i;
                u32 bit = 0;
                for (i = 0; i < (a_mnt->e2fs.s_blocksize / sizeof(u32)); ++i)
                {
                        if (bitmap[i] == UINT32_MAX)
                        {
                                continue;
                        }

                        if (bitmap[i] == 0)
                        {

                                bitmap[i] = 1;
                                bit       = 0;
                        }
                        else
                        {
                                for (bit = 0; bit < (sizeof(u32) * CHAR_BIT); ++bit)
                                {
                                        if ((bitmap[i] & BIT(bit)) == 0)
                                        {
                                                break;
                                        }
                                }

                                KASSERT((bitmap[i] & BIT(bit)) == 0);
                                bitmap[i] |= BIT(bit);
                        }

                        break;
                }

                if (i == (a_mnt->e2fs.s_blocksize / sizeof(u32)))
                {
                        // Nothing usable here, get next block
                        buffer_cache::release(io_desc);
                }
                else
                {
                        // Set the reval to the found element
                        retval = static_cast<kern_return_t>(a_grnum * a_mnt->e2fs.*X_PER_GROUP
                                                            + ((i * (sizeof(u32) * CHAR_BIT)) + bit) + 1);

                        // Make adjustements to the group descriptor
                        f(grdesc);

                        // Write out the modified bitmap
                        buffer_cache::write(io_desc);
                }

                return retval;
        };

        return bitmap_search((a_start_elem_num - 1) / a_mnt->e2fs.*X_PER_GROUP, a_mnt, bitmap_lookup);
}