
#include "ext2_mount.h"

#include <kernel/mach_clock.h>
#include <strings.h>
#include <sys/stat.h>
#include <vfs/vfs_cache.h>
// Alloc/Free functions
#include "ext2_alloc.hh"
#include "ext2_inode.hh"

namespace
{
// Storage for our inodes
template <> vnode::storage<inode>::type vnode::storage<inode>::mem{};

} // namespace

inline void *operator new(size_t, inode *p) { return p; }

static void ext2_inode_to_fstat(ext2_disk_inode *a_inode, fstat_t *a_fstat)
{
        // a_fstat->st_dev   = 0;
        // a_fstat->st_ino   = 0;
        a_fstat->st_mode  = a_inode->i_mode;
        a_fstat->st_nlink = a_inode->i_links_count;
        a_fstat->st_uid   = a_inode->i_uid;
        a_fstat->st_gid   = a_inode->i_gid;

        if (S_ISBLK(a_inode->i_mode) || S_ISCHR(a_inode->i_mode))
        {
                a_fstat->st_rdev = a_inode->i_block_u.i_block_dev;
        }
        else
        {
                a_fstat->st_rdev = 0; // If special file
        }
        a_fstat->st_size = static_cast<off_t>(a_inode->get_isize());
        // a_fstat->st_blksize = 0;
        a_fstat->st_blocks = a_inode->i_blocks;
        a_fstat->st_atime  = a_inode->i_atime;
        a_fstat->st_mtime  = a_inode->i_mtime;
        a_fstat->st_ctime  = a_inode->i_ctime;
}

static void fstat_to_ext2_inode(ext2_disk_inode *a_inode, fstat_t *a_fstat)
{
        // a_fstat->st_dev   = 0;
        // a_fstat->st_ino   = 0;
        a_inode->i_mode        = static_cast<u16>(a_fstat->st_mode);
        a_inode->i_links_count = static_cast<u16>(a_fstat->st_nlink);
        a_inode->i_uid         = static_cast<u16>(a_fstat->st_uid);
        a_inode->i_gid         = static_cast<u16>(a_fstat->st_gid);

        if (S_ISBLK(a_inode->i_mode) || S_ISCHR(a_inode->i_mode))
        {
                a_inode->i_block_u.i_block_dev = a_fstat->st_rdev;
        }

        a_inode->i_size = static_cast<u32>(a_fstat->st_size);
        // a_fstat->st_blksize = 0;
        a_inode->i_blocks = static_cast<u32>(a_fstat->st_blocks);
        a_inode->i_atime  = static_cast<u32>(a_fstat->st_atime);
        a_inode->i_mtime  = static_cast<u32>(a_fstat->st_mtime);
        a_inode->i_ctime  = static_cast<u32>(a_fstat->st_ctime);
}

template <> struct intrusive::static_unordered::hash<ino_t>
{
        size_t operator()(ino_t a_ino) { return a_ino; }
};

void ext2mount::initialize() { vnode::storage<inode>::init(); }

ext2mount::ext2mount(vnode::ptr &a_devvp, vnode::ptr &vncovered, integer_t flags)
    : mount(a_devvp->m_fstat.st_rdev, vncovered, "ext2", flags)
    , um_devvp(a_devvp)
{
        inode_cache.init();
        bzero(e2fs.fs_fsmnt, sizeof(e2fs.fs_fsmnt));
}

ext2mount::~ext2mount() {}

// One of our inodes has become unrefrenced(refcount 0), inactivate has been
// called on it. It is our responsibilty to delete it
void ext2mount::vnode_reclaim(vnode *a_vnode)
{

        KASSERT(a_vnode->v_mount == this);

        inode *ip = static_cast<inode *>(a_vnode);

        // vfs_cache_purge(ip);

        // panic("Inode not it out cache")

        buffer_cache::io_descriptor *io_desc;
        kern_return_t retval = fsio_read(ino_to_fsba((&e2fs), ip->m_fstat.st_ino), &io_desc);
        if (retval == KERN_SUCCESS)
        {
                ext2_disk_inode *bufinode;
                io_desc->get_data(&bufinode, ((size_t)ino_to_fsbo((&e2fs), ip->m_fstat.st_ino)) * e2fs.s_es.s_inode_size);

                fstat_to_ext2_inode(bufinode, &ip->m_fstat);
                bufinode->i_block_u = ip->i_block_u;
                bufinode->i_flags   = ip->m_flags;

                if (ip->m_fstat.st_nlink == 0)
                {
                        if (ip->m_fstat.st_size)
                        {
                                panic("isize left %d", ip->m_fstat.st_size);
                        }

                        if (ip->m_fstat.st_blocks)
                        {
                                panic("iblocks left %d", ip->m_fstat.st_blocks);
                        }

                        timespec delete_time;
                        mach_get_timespec(&delete_time);

                        bufinode->i_dtime = static_cast<u32>(delete_time.tv_sec);
                }
                else
                {
                        bufinode->i_dtime = 0;
                }

                buffer_cache::write(io_desc);
        }
        else
        {
                panic("Failed to sync inode");
        }

        if (ip->m_fstat.st_nlink == 0)
        {
                ext2mount::inode_free(ip);
        }

        inode_cache.remove(ip);

        KASSERT(inode_cache.find(ip->m_fstat.st_ino) == nullptr);

        mount::delete_vnode(ip);
}

kern_return_t ext2mount::fsio_read(size_t a_lbn, buffer_cache::io_descriptor **a_io_desc_out)
{
        um_devvp->lock();
        kern_return_t result = buffer_cache::read(um_devvp.get_raw(), a_lbn << e2fs.s_bshift, e2fs.s_blocksize, a_io_desc_out);
        um_devvp->unlock();
        return result;
}

kern_return_t ext2mount::fsio_get(size_t a_lbn, buffer_cache::io_descriptor **a_io_desc_out)
{
        um_devvp->lock();
        *a_io_desc_out = buffer_cache::get(um_devvp.get_raw(), a_lbn << e2fs.s_bshift, e2fs.s_blocksize);
        um_devvp->unlock();

        kern_return_t result = KERN_SUCCESS;

        if (*a_io_desc_out == nullptr)
        {
                result = KERN_FAIL(EIO);
        }

        return result;
}

kern_return_t ext2mount::read_group_descriptor(size_t a_block_group_number,
                                               ext2_group_desc **a_group_desc_out,
                                               buffer_cache::io_descriptor **a_io_desc_out)
{
        KASSERT(a_block_group_number < e2fs.s_groups_count);

        u32 group_descriptor_block     = static_cast<u32>(a_block_group_number) / e2fs.s_desc_per_block;
        u32 group_descriptor_block_idx = static_cast<u32>(a_block_group_number) % e2fs.s_desc_per_block;

        *a_io_desc_out = nullptr;

        kern_return_t result;
        if (result = fsio_read(ROOTINO + group_descriptor_block, a_io_desc_out); result == KERN_SUCCESS)
        {
                (*a_io_desc_out)->get_data(a_group_desc_out);
                *a_group_desc_out = &(*a_group_desc_out)[group_descriptor_block_idx];
        }

        if (result != KERN_SUCCESS && *a_io_desc_out)
        {
                buffer_cache::release(*a_io_desc_out);
        }

        return result;
}

kern_return_t ext2mount::write_group_descriptor(size_t a_block_group_number, ext2_group_desc *a_group_desc)
{
        buffer_cache::io_descriptor *io_desc;
        ext2_group_desc *old_grdesc;

        kern_return_t retval = ext2mount::read_group_descriptor(a_block_group_number, &old_grdesc, &io_desc);

        if (retval == KERN_SUCCESS)
        {
                *old_grdesc = *a_group_desc;
                buffer_cache::write(io_desc);
        }

        return retval;
}

static kern_return_t free_blocks_recursive(inode *a_inode, u32 *a_blk, u32 *a_path, u32 a_path_len, u32 a_depth)
{
        kern_return_t ret = KERN_SUCCESS;

        bool free_block = true;

        if (a_depth < a_path_len)
        {
                buffer_cache::io_descriptor *io_desc;
                ret = ext2mount::get(a_inode)->fsio_read(*a_blk, &io_desc);

                KASSERT(ret == KERN_SUCCESS);

                u32 *blkno;
                io_desc->get_data(&blkno);

                ret = free_blocks_recursive(a_inode, &blkno[a_path[a_depth]], a_path, a_path_len, a_depth + 1);
                KASSERT(ret == KERN_SUCCESS);

                // blkno[a_path[a_depth]] = 0;

                u32 block_size = ext2mount::get(a_inode)->e2fs.s_blocksize;
                u32 i;

                for (i = 0; i < (block_size / sizeof(u32)); ++i)
                {
                        if (blkno[i])
                        {
                                free_block = false;
                                break;
                        }
                }

                if (free_block)
                {
                        buffer_cache::release(io_desc);
                }
                else
                {
                        buffer_cache::write(io_desc);
                }
        }

        if (free_block)
        {

                auto block_free_callback = [](ext2mount *a_mnt, ext2_group_desc *a_grdesc) {
                        (a_mnt->e2fs.s_es.s_free_blocks_count)++;
                        (a_grdesc->bg_free_blocks_count)++;
                };

                ret = BLOCK_FREE(*a_blk, ext2mount::get(a_inode), block_free_callback);

                KASSERT(ret == KERN_SUCCESS);

                *a_blk = 0;

                a_inode->m_fstat.st_blocks -= btodb(a_inode->m_fstat.st_blksize);
        }

        return ret;
}

kern_return_t ext2mount::block_free(vnode *a_vnode, vm_offset_t a_blk_offset, size_t a_blk_count)
{
        if ((S_ISLNK(a_vnode->m_fstat.st_mode)) && (static_cast<size_t>(a_vnode->m_fstat.st_size) < sizeof(ext2_i_block)))
        {
                return KERN_SUCCESS;
        }

        vm_offset_t end_offset = a_blk_count * e2fs.s_blocksize;
        kern_return_t ret      = KERN_SUCCESS;
        lbn_path_t lbn_path;
        size_t lbn_path_len;
        size_t i_block_idx;

        inode *in = static_cast<inode *>(a_vnode);

        for (vm_offset_t current_offset = a_blk_offset; current_offset < end_offset; current_offset += e2fs.s_blocksize)
        {
                size_t phys_offset = 0;

                if (ret = in->bmap(current_offset, &phys_offset, lbn_path, &lbn_path_len, &i_block_idx); ret == KERN_SUCCESS)
                {
                        buffer_cache::invalidate(in, current_offset, e2fs.s_blocksize);

                        if (phys_offset == 0)
                        {
                                // A file has gained blocks in memory(a new file) and is deleted before they are written to
                                // disk.
                                continue;
                        }

                        u32 _lbn_path[] = {lbn_path[0], lbn_path[1], lbn_path[2]};

                        ret = free_blocks_recursive(in, &(in->i_block_u.i_block[i_block_idx]), _lbn_path, lbn_path_len, 0);
                }
        }

        return ret;
}

kern_return_t ext2mount::block_alloc(inode *a_inode, u32 *a_fs_blkno_out)
{
        KASSERT(e2fs.s_es.s_free_blocks_count);

        auto block_alloc_callback = [=](ext2_group_desc *a_grdesc) {
                (e2fs.s_es.s_free_blocks_count)--;
                (a_grdesc->bg_free_blocks_count)--;
        };

        kern_return_t retval = BLOCK_ALLOC(static_cast<u32>(a_inode->m_dirent.d_ino), this, block_alloc_callback);

        if (retval > 0)
        {
                *a_fs_blkno_out = static_cast<u32>(retval);
                retval          = KERN_SUCCESS;
        }

        return retval;
}

kern_return_t ext2mount::inode_free(inode *a_inode)
{
        auto inode_free_callback = [=](ext2mount *a_mnt, ext2_group_desc *a_grdesc) {
                if (S_ISDIR(a_inode->m_fstat.st_mode))
                {
                        (a_grdesc->bg_used_dirs_count)--;
                }

                (a_grdesc->bg_free_inodes_count)++;
                (a_mnt->e2fs.s_es.s_free_inodes_count)++;
        };

        return INODE_FREE(static_cast<u32>(a_inode->m_fstat.st_ino), this, inode_free_callback);
}

kern_return_t ext2mount::inode_alloc(inode *a_parent, mode_t a_mode, ino_t *a_inode_num_out)
{
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpadded"

        KASSERT(e2fs.s_es.s_free_inodes_count);
        auto inode_alloc_callback = [=](ext2_group_desc *a_grdesc) {
                e2fs.s_es.s_free_inodes_count--;
                (a_grdesc->bg_free_inodes_count)--;

                if (S_ISDIR(a_mode))
                {
                        (a_grdesc->bg_used_dirs_count)++;
                }
        };

#pragma GCC diagnostic pop
        kern_return_t retval = INODE_ALLOC(static_cast<u32>(a_parent->m_fstat.st_ino), this, inode_alloc_callback);

        if (retval > 0)
        {
                *a_inode_num_out = static_cast<ino_t>(retval);
                retval           = KERN_SUCCESS;
        }

        return retval;
}

/*
 * Look up a EXT2FS dinode number to find its incore vnode, otherwise read
 * it in from disk.  If it is in core, wait for the lock bit to clear, then
 * return the inode locked.  Detection and handling of mount points must be
 * done by the calling routine.
 */
int ext2mount::vget(emerixx::dirent_t *a_dirent, vnode *a_parent, vnode **a_vnode_out)
{
        KASSERT(e2fs.s_es.s_inode_size <= sizeof(ext2_disk_inode));

        int error        = 0;
        inode *inode_out = nullptr;
        // fstat_t fst              = {};

        // Fill in the fstat information we know at this point
        // fst.st_ino     = a_dirent->d_ino;
        // fst.st_dev     = m_dev;
        // fst.st_blksize = e2fs.s_blocksize;

        mode_t mode = static_cast<mode_t>(DTTOIF(a_dirent->d_type));

        // If ino is set we are looking for a existing inode, which might be in memory
        if (a_dirent->d_ino != 0)
        {
                // Check the cache for the ino, assignment may sleep if the found inode is locked
                *a_vnode_out = inode_cache.find(a_dirent->d_ino);

                if (*a_vnode_out != nullptr)
                {
                        if ((*a_vnode_out)->m_usecount == 0)
                        {
                                // The vnode is being reclaimed
                                (*a_vnode_out) = nullptr;
                                return KERN_FAIL(EAGAIN);
                        }

                        return 0;
                }

                error = mount::new_vnode(&inode_out, this);

                if (error != KERN_SUCCESS)
                {
                        return error;
                }

                buffer_cache::io_descriptor *io_desc;
                error = fsio_read(ino_to_fsba((&e2fs), a_dirent->d_ino), &io_desc);
                if (error != KERN_SUCCESS)
                {
                        mount::delete_vnode(inode_out);
                        return error;
                }

                ext2_disk_inode *bufinode;
                io_desc->get_data(&bufinode, ((size_t)ino_to_fsbo((&e2fs), a_dirent->d_ino)) * e2fs.s_es.s_inode_size);
                KASSERT((bufinode->i_mode & S_IFMT) == mode);

                ext2_inode_to_fstat(bufinode, &inode_out->m_fstat);

                inode_out->m_flags = bufinode->i_flags;

                inode_out->i_block_u = bufinode->i_block_u;

                buffer_cache::release(io_desc);
        }
        else
        {
                KASSERT(a_parent);

                ino_t ino;
                error = ext2mount::inode_alloc(static_cast<inode *>(a_parent), mode, &ino);

                if (error != KERN_SUCCESS)
                {
                        return error;
                }

                error = mount::new_vnode(&inode_out, this);

                KASSERT(error == KERN_SUCCESS);

                // fst.st_ino = ino;

                a_dirent->d_ino = ino;

                inode_out->m_fstat.st_mode = mode;

                // New vnode patch up the dirent ino
                // a_dirent->d_ino = fst.st_ino;
                // bufinode.i_mode = static_cast<u16>(mode);
        }

        inode_out->m_size = round_page(inode_out->m_fstat.st_size);

        inode_out->i_number = a_dirent->d_ino;

        inode_out->m_fstat.st_ino = a_dirent->d_ino;

        inode_out->m_dirent = *a_dirent;

        inode_out->m_fstat.st_dev = m_dev;

        inode_out->m_fstat.st_blksize = e2fs.s_blocksize;

        inode_cache.insert(inode_out);

        *a_vnode_out = inode_out;

        return error;
}

/*
 * Go through the disk queues to initiate sandbagged IO;
 * go through the inodes to write those that have been modified;
 * initiate the writing of the super block if it has been modified.
 *
 * Note: we are always called with the filesystem marked `MPBUSY'.
 */
extern int ext2_sbupdate(struct ext2mount *mp, int waitfor);

int ext2mount::sync(int waitfor)
{
        ext2mount::lock();

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpadded"

        mnt_vnodelist.for_each([=](vnode *a_vnode) {
                a_vnode->lock();

                a_vnode->fsync(waitfor);

                buffer_cache::io_descriptor *io_desc;

                kern_return_t retval = fsio_read(ino_to_fsba((&e2fs), a_vnode->m_fstat.st_ino), &io_desc);

                KASSERT(retval == KERN_SUCCESS);

                ext2_disk_inode *bufinode;
                io_desc->get_data(&bufinode, ((size_t)ino_to_fsbo((&e2fs), a_vnode->m_fstat.st_ino)) * e2fs.s_es.s_inode_size);

                fstat_to_ext2_inode(bufinode, &a_vnode->m_fstat);
                bufinode->i_block_u = static_cast<inode *>(a_vnode)->i_block_u;
                bufinode->i_flags   = static_cast<inode *>(a_vnode)->m_flags;

                if (bufinode->i_links_count != 0)
                {
                        bufinode->i_dtime = 0;
                }
                else
                {
                        log_debug("LORIS");
                }

                buffer_cache::write(io_desc);

                a_vnode->unlock();
        });
#pragma GCC diagnostic pop

        for (u32 i = 0; i < e2fs.s_groups_count; ++i)
        {
                ext2mount::write_group_descriptor(i, &(e2fs.s_group_desc[i]));
        }

        buffer_cache::io_descriptor *io_desc;

        KASSERT(ext2mount::fsio_read(1, &io_desc) == KERN_SUCCESS);

        ext2_super_block *superblock;

        io_desc->get_data(&superblock);

        *superblock = e2fs.s_es;

        um_devvp->lock();
        buffer_cache::write(io_desc, false);

        buffer_cache::page_out(um_devvp.get_raw());
        um_devvp->unlock();

        ext2mount::unlock();

        return KERN_SUCCESS;
#if 0
        int allerror = 0;

        /*
         * Write back modified superblock.
         * Consistency check that the superblock
         * is still in the buffer cache.
         */
        if (e2fs.s_dirt)
        {
                struct timeval time;

                if (e2fs.s_rd_only != 0)
                { /* XXX */
                        log_error("fs = %s\n", e2fs.fs_fsmnt);
                        panic("update: rofs mod");
                }
                e2fs.s_dirt = 0;

                // get_time(&time);  // Does not work

                e2fs.s_es.s_wtime = time.tv_sec;
                allerror          = ext2_sbupdate(this, waitfor);
        }
        /*
         * Write back each (modified) inode.
         */
        struct inode *ip;
        int error;
        for (vnode::locked_ptr vp : mnt_vnodelist)
        {
                /*
                 * If the vnode that we are about to sync is no longer
                 * associated with this mount point, start over.
                 */
                // if (vp->v_mount != this)
                //{
                //    repeat = true;
                //    break;
                //}

                // if (vp->islocked())
                //{
                //        continue;
                //}

                ip = vp.to_raw<inode>();
                if ((ip->i_flag & (IN_ACCESS | IN_CHANGE | IN_MODIFIED | IN_UPDATE)) == 0 && vp->v_dirtyblkhd.empty())
                {
                        continue;
                }

      if (::vget(vp, 1)) {
        repeat = true;
        break;
      }

                if (error = vp->fsync(cred, waitfor, p))
                {
                        allerror = error;
                }
        }

        /*
         * Force stale file system control information to be flushed.
         */
        if (error = um_devvp->fsync(cred, waitfor, p))
        {
                allerror = error;
        }

#if QUOTA
        qsync(mp);
#endif

        return (allerror);
#endif
        return KERN_SUCCESS;
}

int ext2mount::generate_lbn_path(size_t a_lbn, lbn_path_t a_lbn_path, size_t *a_lbn_path_len, size_t *a_start_idx)
{
        int error = 0;

        if (a_lbn < EXT2_NDIR_BLOCKS)
        {
                *a_start_idx    = a_lbn;
                *a_lbn_path_len = 0;
        }
        else
        {
                if (a_lbn < um_first_indir_count)
                {
                        *a_start_idx = EXT2_NDIR_BLOCKS;
                        a_lbn -= EXT2_NDIR_BLOCKS;

                        // LVL 1
                        a_lbn_path[0]   = a_lbn;
                        *a_lbn_path_len = 1;
                        // (b/4)2+(b/4)+11
                }
                else if (a_lbn < um_second_indir_count)
                {
                        *a_start_idx = EXT2_NDIR_BLOCKS + 1;
                        a_lbn -= um_first_indir_count;

                        // LVL 2
                        a_lbn_path[0] = a_lbn / um_nindir;
                        a_lbn         = a_lbn % um_nindir;
                        // LVL 1
                        a_lbn_path[1] = a_lbn;

                        *a_lbn_path_len = 2;
                        // (b/4)3+(b/4)2+(b/4)+11
                }
                else if (a_lbn < um_triple_indir_count)
                {
                        a_lbn -= um_second_indir_count;
                        *a_start_idx = EXT2_NDIR_BLOCKS + 2;

                        // LVL 3
                        a_lbn_path[0] = a_lbn / um_nindir_pow_2;
                        a_lbn         = a_lbn % um_nindir_pow_2;
                        // LVL 2
                        a_lbn_path[1] = a_lbn / um_nindir;
                        a_lbn         = a_lbn % um_nindir;
                        // LVL 1
                        a_lbn_path[2] = a_lbn;

                        *a_lbn_path_len = 3;
                }
                else
                {
                        *a_lbn_path_len = 0;
                        error           = -EFBIG;
                }
        }

        return error;
}

/*
 * Return the root of a filesystem.
 */
int ext2mount::root(struct vnode::ptr &vpp)
{
        int error;
        vnode *lvpp;

        emerixx::dirent_t dent = {};

        dent.d_ino  = EXT2_ROOT_INO;
        dent.d_type = IFTODT(S_IFDIR);

        error = vget(&dent, nullptr, &lvpp);

        if (error)
        {
                vpp = nullptr;
                return error;
        }
        vpp = lvpp;
        return 0;
}

int ext2mount::quotactl(int UNUSED cmds, uid_t UNUSED uid, char UNUSED *arg, pthread_t p UNUSED)
{
        panic("NI");
        return 0;
}

int ext2mount::statfs(struct statfs *sbp)
{
        unsigned long overhead;
        unsigned long overhead_per_group;

        struct ext2_super_block *es = &e2fs.s_es;

        if (es->s_magic != EXT2_SUPER_MAGIC)
        {
                panic("ext2_statfs - magic number spoiled");
        }

        /*
         * Compute the overhead (FS structures)
         */
        overhead_per_group
            = 1 /* super block */ + e2fs.s_db_per_group + 1 /* block bitmap */ + 1 /* inode bitmap */ + e2fs.s_itb_per_group;
        overhead = es->s_first_data_block + e2fs.s_groups_count * overhead_per_group;

        sbp->f_type = MOUNT_EXT2FS;
        // sbp->f_bsize  = EXT2_FRAG_SIZE(&e2fs);
        sbp->f_bsize  = EXT2_BLOCK_SIZE(&e2fs);
        sbp->f_blocks = es->s_blocks_count - overhead;
        sbp->f_bfree  = es->s_free_blocks_count;
        sbp->f_bavail = sbp->f_bfree - es->s_r_blocks_count;
        sbp->f_files  = es->s_inodes_count;
        sbp->f_ffree  = es->s_free_inodes_count;

        if (sbp != &mnt_stat)
        {
                // bcopy(mnt_stat.f_mntonname, &sbp->f_mntonname[0], MNAMELEN);
                // bcopy(mnt_stat.f_mntfromname, &sbp->f_mntfromname[0], MNAMELEN);
        }

        // strncpy(sbp->f_fstypename, fs_name, MFSNAMELEN);

        return 0;
}

int ext2mount::start(int UNUSED flags, pthread_t)
{
        panic("NI");
        return flags;
}
