#pragma once
#include "ext2_fs.h"
#include <mach/intrusive/hash_map.hh>
#include <vfs/vnode.h>

// Forward declarations
struct ext2mount;
struct ext2_dir_inode;

#define ROOTINO ((ino_t)2)

/* File types. */
#define IFMT   0170000 /* Mask of file type. */
#define IFIFO  0010000 /* Named pipe (fifo). */
#define IFCHR  0020000 /* Character device. */
#define IFDIR  0040000 /* Directory file. */
#define IFBLK  0060000 /* Block device. */
#define IFREG  0100000 /* Regular file. */
#define IFLNK  0120000 /* Symbolic link. */
#define IFSOCK 0140000 /* UNIX domain socket. */

/* File modes. */
#define IEXEC  0000100 /* Executable. */
#define IWRITE 0000200 /* Writeable. */
#define IREAD  0000400 /* Readable. */
#define ISVTX  0001000 /* Sticky bit. */
#define ISGID  0002000 /* Set-gid. */
#define ISUID  0004000 /* Set-uid. */

#pragma pack(push, 1)
union ext2_i_block {
        u32 i_block[/*EXT2_N_BLOCKS*/ 15]; /* Pointers to blocks */
        struct
        {
                u32 i_block_dir[12];
                u32 i_block_indir[3];

        } i_block_split;

        i8 i_block_bytes[sizeof(i_block)];
        dev_t i_block_dev;
};
#pragma pack(pop)

/*
 * Structure of an inode on the disk
 */
struct ext2_disk_inode
{
        u16 i_mode;        /* File mode */
        u16 i_uid;         /* Owner Uid */
        u32 i_size;        /* Size in bytes */
        u32 i_atime;       /* Access time */
        u32 i_ctime;       /* Creation time */
        u32 i_mtime;       /* Modification time */
        u32 i_dtime;       /* Deletion Time */
        u16 i_gid;         /* Group Id */
        u16 i_links_count; /* Links count */
        u32 i_blocks;      /* Blocks count */
        u32 i_flags;       /* File flags */
        union {
                struct
                {
                        u32 l_i_reserved1;
                } linux1;
                struct
                {
                        u32 h_i_translator;
                } hurd1;
                struct
                {
                        u32 m_i_reserved1;
                } masix1;
        } osd1; /* OS dependent 1 */

        ext2_i_block i_block_u;

        static_assert(sizeof(i_block_u) == sizeof(u32[15]));

        u32 i_version;  /* File version (for NFS) */
        u32 i_file_acl; /* File ACL */
        u32 i_dir_acl;  /* Directory ACL */
        u32 i_faddr;    /* Fragment address */
        union {
                struct
                {
                        u8 l_i_frag;  /* Fragment number */
                        u8 l_i_fsize; /* Fragment size */
                        u16 i_pad1;
                        u32 l_i_reserved2[2];
                } linux2;
                struct
                {
                        u8 h_i_frag;  /* Fragment number */
                        u8 h_i_fsize; /* Fragment size */
                        u16 h_i_mode_high;
                        u16 h_i_uid_high;
                        u16 h_i_gid_high;
                        u32 h_i_author;
                } hurd2;
                struct
                {
                        u8 m_i_frag;  /* Fragment number */
                        u8 m_i_fsize; /* Fragment size */
                        u16 m_pad1;
                        u32 m_i_reserved2[2];
                } masix2;
        } osd2; /* OS dependent 2 */

        u64 get_isize()
        {
                u64 inode_size = i_size;
                if ((i_mode & IFMT) & IFREG)
                {
                        inode_size |= (static_cast<u64>(i_dir_acl) << 32);
                }
                return inode_size;
        }

        u8 s[128];
};

/* These flags are kept in i_flag. */
#define IN_ACCESS   0x0001 /* Access time update request. */
#define IN_CHANGE   0x0002 /* Inode change time update request. */
#define IN_EXLOCK   0x0004 /* File has exclusive lock. */
#define IN_LOCKED   0x0008 /* Inode lock. */
#define IN_LWAIT    0x0010 /* Process waiting on file lock. */
#define IN_MODIFIED 0x0020 /* Inode has been modified. */
#define IN_RENAME   0x0040 /* Inode is being renamed. */
#define IN_SHLOCK   0x0080 /* File has shared lock. */
#define IN_UPDATE   0x0100 /* Modification time update request. */
#define IN_WANTED   0x0200 /* Inode is wanted by a process. */

/*
 * The inode is used to describe each active (or recently active)
 * file in the UFS filesystem. It is composed of two types of
 * information. The first part is the information that is needed
 * only while the file is active (such as the identity of the file
 * and linkage to speed its lookup). The second part is the
 * permannent meta-data associated with the file which is read
 * in from the permanent dinode from long term storage when the
 * file becomes active, and is put back when the file is no longer
 * being used.
 */
struct inode final : vnode
{
        using hash_link = intrusive::static_unordered::link<inode>;

        inode(ext2mount *mp, ext2_disk_inode *disk_inode);

        inode(ext2mount *mp);

        ~inode();

        kern_return_t bmap(
            size_t a_bn, size_t *a_bnp, lbn_path_t a_lbn_path_out, size_t *a_lbn_path_len_out, size_t *a_i_block_idx_out);

        kern_return_t fs_readdir(fs_readdir_cb_t a_cb, fs_readdir_ctx_t *a_ctx) override;

        kern_return_t fs_dir_initialize(fstat_t *a_parent_stat) override;

        kern_return_t fs_dir_reparent(fstat_t *a_parent_stat) override;

        kern_return_t fs_dir_empty() override;

        kern_return_t fs_readwrite_link(uio *a_uio) override;

        kern_return_t fs_readwrite_spec(file *a_file, uio *a_uio, int a_ioflag) override;

        kern_return_t fs_strategy(buffer_cache::io_descriptor *a_iodesc) override;

        kern_return_t fs_ioctl(file *a_file, int a_cmd, void *a_data, int a_fflag) override;

        kern_return_t fs_open(file *a_file, mode_t a_mode) override;

        kern_return_t fs_close(file *a_file) override;

        kern_return_t fs_poll(file *a_file, pollfd **a_pfd) override;

        kern_return_t fs_remove_dirent(emerixx::dirent_t *a_dent) override;

        kern_return_t fs_insert_dirent(emerixx::dirent_t *a_dent) override;

        kern_return_t fs_get_page(size_t a_offset, vm_prot_t a_prot, vm_page_t *a_vm_page_out) override;

        // u32 i_flag;                             /* I* flags. */
        ino_t i_number;                         /* The identity of the inode. */
        struct ext2_sb_info *i_e2fs;            /* Associated filesystem. */
        struct dquot *i_dquot[/*MAXQUOTAS*/ 2]; /* Dquot structures. */

        hash_link i_cache_link;
        using cache_map_t = intrusive::static_unordered::map<inode, &inode::i_cache_link, ino_t, &inode::i_number>;

        // ext2
        // long i_block_group;
        // long i_next_alloc_block;
        // long i_next_alloc_goal;
        // long i_prealloc_block;
        // long i_prealloc_count;
        // size_t i_modrev;                        /* Revision level for lease. */
        // struct lockf *i_lockf;                  /* Head of byte-level lock list. */
        // void *i_lockholder;                     /* DEBUG: holder of inode lock. */
        // void *i_lockwaiter;                     /* DEBUG: latest blocked for inode lock. */

        /*
         * The on-disk dinode itself.
         */
        u32 m_flags;
        ext2_i_block i_block_u; /* 128 bytes of the on-disk dinode. */
};