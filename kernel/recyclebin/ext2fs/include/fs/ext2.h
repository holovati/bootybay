#pragma once
#include <vfs/vnode.h>
struct vnode;

int ext2_mountroot(pthread_t p, vnode::ptr &rootvp, int rdonly, dev_t a_mount_dev);