
#include <aux/init.h>
#include <fs/ext2.h>
#include <strings.h>

#include "ext2_mount.h"

#define ROOTNAME "root_device"
static int copystr(const char *fromaddr, void *a_toaddr, int maxlength, size_t *lencopied)
{
        int tally    = 0;
        char *toaddr = (char *)a_toaddr;
        while (maxlength--)
        {
                *(char *)toaddr = *(char *)fromaddr++;
                tally++;
                if (*(char *)toaddr++ == 0)
                {
                        if (lencopied)
                        {
                                *lencopied = tally;
                        }
                        return (0);
                }
        }

        if (lencopied)
        {
                *lencopied = tally;
        }

        return -ENAMETOOLONG;
}

/*
 * Called by main() when ext2 is going to be mounted as root.
 *
 * Name is updated by mount(8) after booting.
 */
int ext2_mountfs(vnode::ptr &devvp, struct ext2mount *mp, pthread_t p);

int ext2_mountroot(pthread_t p, vnode::ptr &a_rootvp, int rdonly, dev_t a_mount_dev)
{
        /*
         * Get vnodes for swapdev and rootdev.
         */

        //  if (/* bdevvp(swapdev, &swapdev_vp) || */ bdevvp(rootdev, &rootvp)) {
        //    panic("ext2_mountroot: can't setup bdevvp's");
        //  }

        mount::lock_shared();

        integer_t mnt_flag = MNT_ROOTFS;

        if (rdonly)
        {
                mnt_flag |= MNT_RDONLY;
        }

        vnode::ptr nullvnptr; // HACK
        ext2mount *mp = new ext2mount{a_rootvp, nullvnptr, mnt_flag};

        mp->m_dev = a_mount_dev;

        if (int error; error = ext2_mountfs(a_rootvp, mp, p))
        {
                delete mp;
                mount::unlock_shared();
                return error;
        }

        if (int error; error = mp->lock())
        {
                mp->unmount(0, p);
                delete mp;
                mount::unlock_shared();
                return error;
        }

        mp->e2fs.fs_fsmnt[0] = '/';
        // bcopy(mp->e2fs.fs_fsmnt, mp->mnt_stat.f_mntonname, MNAMELEN);

        size_t size = 0;
        // copystr(ROOTNAME, mp->mnt_stat.f_mntfromname, MNAMELEN - 1, &size);

        // bzero(mp->mnt_stat.f_mntfromname + size, MNAMELEN - size);
        mp->statfs(&mp->mnt_stat);
        mp->unlock();

#ifdef DAMIR
        // This does not work
        inittodr(fs->s_es->s_wtime); /* this helps to set the time */
#endif
        mount::unlock_shared();
        return 0;
}

static int ext2_init()
{
        ext2mount::initialize();
        return 0;
}

fs_initcall(ext2_init);
