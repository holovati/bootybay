
#include <fs/specfs.h>
#include <mach/disklabel.h>

#include <fcntl.h>
#include <kernel/vm/vm_kern.h>
#include <malloc.h>
#include <string.h>
#include <strings.h>
#include <vfs/vfs.h>

#include "ext2_fs.h"
#include "ext2_mount.h"

/*
 * checks that the data in the descriptor blocks make sense
 * this is taken from ext2/super.c
 */
static int ext2_check_descriptors(struct ext2_sb_info *sb)
{
        // int desc_block              = 0;
        unsigned long block         = sb->s_es.s_first_data_block;
        struct ext2_group_desc *gdp = NULL;

        /* ext2_debug ("Checking group descriptors"); */

        for (int i = 0; i < sb->s_groups_count; i++)
        {
                gdp = &sb->s_group_desc[i];

                if (gdp->bg_block_bitmap < block || gdp->bg_block_bitmap >= block + EXT2_BLOCKS_PER_GROUP(sb))
                {
                        log_debug("ext2_check_descriptors: "
                                  "Block bitmap for group %d"
                                  " not in group (block %lu)!",
                                  i,
                                  (unsigned long)gdp->bg_block_bitmap);
                        return 0;
                }
                if (gdp->bg_inode_bitmap < block || gdp->bg_inode_bitmap >= block + EXT2_BLOCKS_PER_GROUP(sb))
                {
                        log_debug("ext2_check_descriptors: "
                                  "Inode bitmap for group %d"
                                  " not in group (block %lu)!",
                                  i,
                                  (unsigned long)gdp->bg_inode_bitmap);
                        return 0;
                }
                if (gdp->bg_inode_table < block || gdp->bg_inode_table + sb->s_itb_per_group >= block + EXT2_BLOCKS_PER_GROUP(sb))
                {
                        log_debug("ext2_check_descriptors: "
                                  "Inode table for group %d"
                                  " not in group (block %lu)!",
                                  i,
                                  (unsigned long)gdp->bg_inode_table);
                        return 0;
                }
                block += EXT2_BLOCKS_PER_GROUP(sb);
                gdp++;
        }
        return 1;
}

/*
 * this computes the fields of the  ext2_sb_info structure from the
 * data in the ext2_super_block structure read in
 */
static int compute_sb_data(ext2mount *mp)
{
        int db_count, error;
        int j;
        int logic_sb_block = 1; /* XXX for now */
        kern_return_t retval;

        mp->e2fs.s_blocksize      = EXT2_MIN_BLOCK_SIZE << mp->e2fs.s_es.s_log_block_size;
        mp->e2fs.s_bshift         = EXT2_MIN_BLOCK_LOG_SIZE + mp->e2fs.s_es.s_log_block_size;
        mp->e2fs.s_fsbtodb        = mp->e2fs.s_es.s_log_block_size + 1;
        mp->e2fs.s_qbmask         = mp->e2fs.s_blocksize - 1;
        mp->e2fs.s_blocksize_bits = EXT2_BLOCK_SIZE_BITS(&(mp->e2fs.s_es));
        mp->e2fs.s_frag_size      = EXT2_MIN_FRAG_SIZE << mp->e2fs.s_es.s_log_frag_size;

        if (mp->e2fs.s_frag_size)
        {
                mp->e2fs.s_frags_per_block = mp->e2fs.s_blocksize / mp->e2fs.s_frag_size;
        }

        mp->e2fs.s_blocks_per_group = mp->e2fs.s_es.s_blocks_per_group;
        mp->e2fs.s_frags_per_group  = mp->e2fs.s_es.s_frags_per_group;
        mp->e2fs.s_inodes_per_group = mp->e2fs.s_es.s_inodes_per_group;
        mp->e2fs.s_inodes_per_block = mp->e2fs.s_blocksize / EXT2_INODE_SIZE(&(mp->e2fs));
        mp->e2fs.s_itb_per_group    = mp->e2fs.s_inodes_per_group / mp->e2fs.s_inodes_per_block;
        mp->e2fs.s_desc_per_block   = mp->e2fs.s_blocksize / sizeof(struct ext2_group_desc);
        /* s_resuid / s_resgid ? */
        mp->e2fs.s_groups_count
            = (mp->e2fs.s_es.s_blocks_count - mp->e2fs.s_es.s_first_data_block + EXT2_BLOCKS_PER_GROUP(&(mp->e2fs)) - 1)
              / EXT2_BLOCKS_PER_GROUP(&(mp->e2fs));

        db_count = (mp->e2fs.s_groups_count + EXT2_DESC_PER_BLOCK(&(mp->e2fs)) - 1) / EXT2_DESC_PER_BLOCK(&(mp->e2fs));
        mp->e2fs.s_db_per_group = db_count;

#ifdef DAMIR
        fs->s_group_desc = bsd_malloc(db_count * sizeof(struct buf *), M_UFSMNT, M_WAITOK);
#endif
        // fs->s_group_desc = (ext2_group_desc *);

        retval = kmem_alloc_wired(kernel_map,
                                  reinterpret_cast<vm_offset_t *>(&(mp->e2fs.s_group_desc)),
                                  round_page(sizeof(ext2_group_desc) * mp->e2fs.s_groups_count));

        KASSERT(retval == KERN_SUCCESS);

        buffer_cache::io_descriptor *io_desc;
        ext2_group_desc *grdesc;
        for (u32 i = 0; i < mp->e2fs.s_groups_count; ++i)
        {
                retval = mp->read_group_descriptor(i, &grdesc, &io_desc);

                KASSERT(retval == KERN_SUCCESS);

                mp->e2fs.s_group_desc[i] = *grdesc;

                buffer_cache::release(io_desc);
        }

        /* adjust logic_sb_block */
        if (mp->e2fs.s_blocksize > SBSIZE)
        {
                /* Godmar thinks: if the blocksize is greater than 1024, then
                   the superblock is logically part of block zero.
                 */
                logic_sb_block = 0;
        }

        if (!ext2_check_descriptors(&(mp->e2fs)))
        {
#ifdef DAMIR
                for (j = 0; j < db_count; j++)
                        brelse(fs->s_group_desc[j]);

                bsd_free(fs->s_group_desc, M_UFSMNT);
#endif

                // free(fs->s_group_desc);

                log_error("EXT2-fs: (ext2_check_descriptors failure) "
                          "unable to read group descriptors\n");
                panic("Damir!!!! fix this");

                return -EIO;
        }

#if 0
        for (i = 0; i < EXT2_MAX_GROUP_LOADED; i++)
        {
                fs->s_inode_bitmap_number[i] = 0;
                fs->s_inode_bitmap[i]        = (char *)malloc(fs->s_blocksize);
                fs->s_block_bitmap_number[i] = 0;
                fs->s_block_bitmap[i]        = (char *)malloc(fs->s_blocksize);
        }

        fs->s_loaded_inode_bitmaps = 0;
        fs->s_loaded_block_bitmaps = 0;
#endif
        return retval;
}
/*
 * Write a superblock and associated information back to disk.
 */
int ext2_sbupdate(struct ext2mount *mp, int waitfor)
{
        struct ext2_sb_info *fs     = &mp->e2fs;
        struct ext2_super_block *es = &fs->s_es;

        int blks;
        char *space;
        int i, size, error = 0;

        log_info("updating superblock, waitfor=%s", waitfor == MNT_WAIT ? "yes" : "no");

#if 0
        struct buf *bp = getblk(mp->um_devvp.get_raw(), SBLOCK, SBSIZE, 0, 0);

        bcopy((char *)es, bp->b_addr, sizeof(struct ext2_super_block));
#endif

        buffer_cache::io_descriptor *io_desc;
        if (io_desc = buffer_cache::get(mp->um_devvp.get_raw(), 1024, fs->s_blocksize); io_desc == nullptr)
        {
                return KERN_FAIL(EIO);
        }

        ext2_super_block *super_block;
        io_desc->get_data(&super_block);

        *super_block = *es;

        if (waitfor == MNT_WAIT)
        {

                if (kern_return_t result = buffer_cache::write(io_desc, false); result != KERN_SUCCESS)
                {
                        return result;
                }

#if 0
error = bwrite(bp);
#endif
        }
        else
        {

                if (kern_return_t result = buffer_cache::write_async(io_desc); result != KERN_SUCCESS)
                {
                        return result;
                }

#if 0
bawrite(bp);
#endif
        }

        return KERN_SUCCESS;
#if 0
        /* write group descriptors back on disk */
        for (i = 0; i < fs->s_db_per_group; i++)
                /* Godmar thinks: we must avoid using any of the b*write
                 * functions here: we want to keep the buffer locked
                 * so we use my 'housemade' write routine:
                 */
                // error |= ll_w_block(&fs->s_group_desc[i], waitfor == MNT_WAIT);

                for (i = 0; i < EXT2_MAX_GROUP_LOADED; i++)
                        // if (fs->s_inode_bitmap[i])
                        //        ll_w_block(fs->s_inode_bitmap[i], 1);
                        for (i = 0; i < EXT2_MAX_GROUP_LOADED; i++)
                                // if (fs->s_block_bitmap[i])
                                //        ll_w_block(fs->s_block_bitmap[i], 1);

                                return (error);

        return error;
#endif
}

/*
 * Reload all incore data for a filesystem (used after running fsck on
 * the root filesystem and finding things to fix). The filesystem must
 * be mounted read-only.
 *
 * Things to do to update the mount:
 *	1) invalidate all cached meta-data.
 *	2) re-read superblock from disk.
 *	3) re-read summary information from disk.
 *	4) invalidate all inactive vnodes.
 *	5) invalidate all cached file data.
 *	6) re-read inode data for all active vnodes.
 */
int ext2_reload(struct ext2mount *mountp)
{
        struct vnode *nvp;
        struct inode *ip;

        int i, size, error;

        if ((mountp->mnt_flag & MNT_RDONLY) == 0)
        {
                return -EINVAL;
        }
        panic("not ready");
#if 0

        /*
         * Step 1: invalidate all cached meta-data.
         */
        if (vinvalbuf(mountp->um_devvp.get_raw(), 0, cred, p, 0, 0))
        {
                panic("ext2_reload: dirty1");
        }
        /*
         * Step 2: re-read superblock from disk.
         * constants have been adjusted for ext2
         */
        struct buf *bp;
        if (error = bread(mountp->um_devvp.get_raw(), SBLOCK, SBSIZE, NOCRED, &bp))
        {
                return error;
        }

        struct ext2_super_block *es = (struct ext2_super_block *)bp->b_addr;
        if (es->s_magic != EXT2_SUPER_MAGIC)
        {
                if (es->s_magic == EXT2_PRE_02B_MAGIC)
                {
                        log_debug("This filesystem bears the magic number of a pre "
                                  "0.2b version of ext2. This is not supported by "
                                  "Lites.\n");
                }
                else
                {
                        log_debug("Wrong magic number: %x (expected %x for ext2 fs\n", es->s_magic, EXT2_SUPER_MAGIC);
                }
                brelse(bp);
                return -EIO; /* XXX needs translation */
        }

        struct ext2_sb_info *fs = &mountp->e2fs;
        bcopy(bp->b_addr, &fs->s_es, sizeof(struct ext2_super_block));

        if (error = compute_sb_data(mountp->um_devvp.get_raw(), es, fs))
        {
                brelse(bp);
                return error;
        }
#ifdef UNKLAR
        if (fs->fs_sbsize < SBSIZE)
        {
                bp->b_flags |= B_INVAL;
        }
#endif
        brelse(bp);

loop:
        for (struct vnode *vp = mountp->mnt_vnodelist.lh_first; vp != NULL; vp = nvp)
        {
                nvp = vp->v_mntvnodes.le_next;
                /*
                 * Step 4: invalidate all inactive vnodes.
                 */
                if (vp->v_usecount == 0)
                {
                        // vgone(vp);
                        continue;
                }
                /*
                 * Step 5: invalidate all cached file data.
                 */
                // if (::vget(vp, 1)) {
                //  goto loop;
                //}
                if (vinvalbuf(vp, 0, cred, p, 0, 0))
                {
                        panic("ext2_reload: dirty2");
                }
                /*
                 * Step 6: re-read inode data for all active vnodes.
                 */
                ip = static_cast<inode *>(vp);
                if (error = bread(
                        mountp->um_devvp.get_raw(), fsbtodb(fs, ino_to_fsba(fs, ip->i_number)), (int)fs->s_blocksize, NOCRED, &bp))
                {
                        vp->unlock();
                        vp->unreference();
                        return (error);
                }

                ip->i_din = *(ext2_inode *)((char *)bp->b_addr + EXT2_INODE_SIZE(fs) * ino_to_fsbo(fs, ip->i_number));

                // ext2_ei2di((struct ext2_inode *)((char *)bp->b_addr
                //                                 + EXT2_INODE_SIZE
                //                                       * ino_to_fsbo(fs, ip->i_number)),
                //           &ip->i_din);

                brelse(bp);
                vp->unlock();
                vp->unreference();
                if (vp->v_mount != mountp)
                {
                        goto loop;
                }
        }
#endif
        return (0);
}

#if 0
/*
 * Flag to permit forcible unmounting.
 */
int doforce = 1;

/*
 * Flush out all the files in a filesystem.
 */
int ext2_flushfiles(struct ext2mount *mp, int flags, pthread_t p)
{
        int i, error;

        if (!doforce)
        {
                flags &= ~FORCECLOSE;
        }

#if QUOTA
        if (mp->mnt_flag & MNT_QUOTA)
        {
                if (error = vflush(mp, NULLVP, SKIPSYSTEM | flags))
                {
                        return (error);
                }
                for (i = 0; i < MAXQUOTAS; i++)
                {
                        if (ump->um_quotas[i] == NULLVP)
                        {
                                continue;
                        }
                        quotaoff(p, mp, i);
                }
                /*
                 * Here we fall through to vflush again to ensure
                 * that we have gotten rid of all the system vnodes.
                 */
        }
#endif
        error = mp->vflush(nullptr, flags);
        return (error);
}
#endif
/*
 * Common code for mount and mountroot
 */
int ext2_mountfs(struct vnode::ptr &devvp, struct ext2mount *mp, pthread_t p)
{
        dev_t dev = mp->m_dev; // devvp->v_un.vu_specinfo->si_rdev;
        char *base;
        // int havepart = 0;
        int error, i; //, size;
        int ronly;
        struct ext2_sb_info *fs = nullptr;
        // struct ext2mount *ump       = nullptr;
        struct ext2_super_block *es = nullptr;
        extern struct vnode *rootvp;
        /*
         * Disallow multiple mounts of the same device.
         * Disallow mounting of a device that is currently in use
         * (except for root, which might share swap device for miniroot).
         * Flush out any old buffers remaining from a previous use.
         */

        vnode::locked_ptr dev_locked_vn{devvp};

        if (error = specfs_is_mounted(devvp) /*vfs_mountedon(devvp)*/)
        {
                return error;
        }

        // vcount should be replaced with specfs_alias_count(...)
        // if (/*(vcount(devvp) > 1) && */ /*(vnode::root != dev_locked_vn)*/)
        //{
        //        return -EBUSY;
        //}

        // if (error = vinvalbuf(dev_locked_vn.get_raw(), V_SAVE, p->p_cred->pc_ucred, p, 0, 0))
        //{
        //        return error;
        //}

#ifdef READONLY
        /* turn on this to force it to be read-only */
        mp->mnt_flag |= MNT_RDONLY;
#endif

        ronly = (mp->mnt_flag & MNT_RDONLY) != 0;

        if (error = devvp->open(nullptr, ronly ? O_RDONLY : O_RDWR, nullptr) < 0)
        {
                return error;
        }

        // struct partinfo dpart;
        // if (devvp->ioctl(DIOCGPART, &dpart, FREAD, NOCRED, p) != 0)
        //{
        //       size = DEV_BSIZE;
        // }
        // else
        //{
        //        havepart = 1;
        //        size     = dpart.disklab->d_secsize;
        //}

        // The code above has cached buffers of BSIZE, flush them so we can use 1024 blocks
        // buffer_cache::page_out(devvp.get_raw());

        buffer_cache::io_descriptor *io_desc;
        if (kern_return_t result = buffer_cache::read(dev_locked_vn.get_raw(), 1024, 1024, &io_desc); result != KERN_SUCCESS)
        {
                goto out;
        }

        io_desc->get_data(&es);

#if 0
        struct buf *bp = NULL;
        if (error = bread(dev_locked_vn.get_raw(), SBLOCK, SBSIZE, NOCRED, &bp))
        {
                log_info("out");
                goto out;
        }

        es = (struct ext2_super_block *)bp->b_addr;
#endif
        if (es->s_magic != EXT2_SUPER_MAGIC)
        {
                if (es->s_magic == EXT2_PRE_02B_MAGIC)
                {
                        log_error("This filesystem bears the magic number of a pre "
                                  "0.2b version of ext2. This is not supported by "
                                  "Lites.\n");
                }
                else
                {
                        log_error("Wrong magic number: %x (expected %x for EXT2FS)\n", es->s_magic, EXT2_SUPER_MAGIC);
                }

                error = -EINVAL; /* XXX needs translation */
                goto out;
        }
#ifdef DAMIR
        struct ext2mount *ump = bsd_malloc(sizeof *ump, M_UFSMNT, M_WAITOK);
#else
        // ump = (struct ext2mount *)malloc(sizeof *ump);
#endif
        // bzero(ump, sizeof *ump);
/* I don't know whether this is the right strategy. Note that
   we dynamically allocate both a ext2_sb_info and a ext2_super_block
   while Linux keeps the super block in a locked buffer
*/
#ifdef DAMIR
        ump->e2fs = bsd_malloc(sizeof(struct ext2_sb_info), M_UFSMNT, M_WAITOK);
#else
        // mp->e2fs = (struct ext2_sb_info *)malloc(sizeof(struct ext2_sb_info));
#endif
#ifdef DAMIR
        ump->e2fs->s_es = bsd_malloc(sizeof(struct ext2_super_block), M_UFSMNT, M_WAITOK);
#else
        // mp->e2fs.s_es
        //    = (struct ext2_super_block *)malloc(sizeof(struct ext2_super_block));
#endif
        // bcopy(es, &mp->e2fs.s_es, sizeof(struct ext2_super_block));

        mp->e2fs.s_es = *es;

        // brelse(bp);
        buffer_cache::release(io_desc);

        io_desc = nullptr;
        // bp = NULL;

        dev_locked_vn->unlock();
        if (error = compute_sb_data(mp))
        {
                dev_locked_vn->lock();
                log_info("comp sb data");
                buffer_cache::release(io_desc);
                // brelse(bp);
                return error;
        }

        dev_locked_vn->lock();

        fs = &mp->e2fs;

        fs->s_rd_only = ronly; /* ronly is set according to mnt_flags */
        if (!(fs->s_es.s_state & EXT2_VALID_FS))
        {
                log_warn("%s was not properly dismounted\n", fs->fs_fsmnt);
        }
        /* if the fs is not mounted read-only, make sure the super block is
           always written back on a sync()
         */
        if (ronly == 0)
        {
                fs->s_dirt = 1;                     /* mark it modified */
                fs->s_es.s_state &= ~EXT2_VALID_FS; /* set fs invalid */
        }
        // mp->mnt_data               = (void *)ump;
        mp->mnt_stat.f_fsid.__val[0] = (long)dev;
        mp->mnt_stat.f_fsid.__val[1] = MOUNT_EXT2FS;
        mp->mnt_maxsymlinklen        = EXT2_MAXSYMLINKLEN;
        mp->mnt_flag |= MNT_LOCAL;
        // ump->um_mountp = mp;

        mp->m_dev = dev;
        // mp->um_devvp = devvp;
        /* setting those two parameters allows us to use
           ufs_bmap w/o changse !
        */
        mp->um_nindir = EXT2_ADDR_PER_BLOCK(fs);

        mp->um_nindir_pow_2 = mp->um_nindir * mp->um_nindir;

        mp->um_first_indir_count = mp->um_nindir + EXT2_NDIR_BLOCKS;

        mp->um_second_indir_count = (mp->um_nindir * mp->um_nindir) + mp->um_nindir + EXT2_NDIR_BLOCKS;

        mp->um_triple_indir_count
            = (mp->um_nindir * mp->um_nindir * mp->um_nindir) + (mp->um_nindir * mp->um_nindir) + mp->um_nindir + EXT2_NDIR_BLOCKS;

        // mp->um_bptrtodb = fs->s_es.s_log_block_size + 1;
        mp->um_seqinc = EXT2_FRAGS_PER_BLOCK(fs);

        // for (i = 0; i < MAXQUOTAS; i++)
        //{
        //        mp->um_quotas[i] = nullptr;
        //}

        // devvp->v_un.vu_specinfo->si_flags |= SI_MOUNTEDON;

        if (ronly == 0)
        {
                ext2_sbupdate(mp, MNT_WAIT);
        }

        return 0;
out:
        if (/*bp*/ io_desc)
        {
                // brelse(bp);
                buffer_cache::release(io_desc);
        }

        devvp->close(nullptr, ronly ? O_RDONLY : O_RDWR);

        if (mp)
        {
#ifdef DAMIR
                bsd_free(ump->um_fs, M_UFSMNT);
                bsd_free(ump, M_UFSMNT);
#else
                // free(mp->e2fs);
                // free(ump);
#endif
                // mp->mnt_data = NULL;
        }

        return error;
}
