#include "ext2_fs.h"
#include "ext2_mount.h"

#include <fcntl.h>
#include <fs/specfs.h>

#include <string.h>

#include "ext2_direntry.hh"
#include "ext2_htree.hh"

struct ext2_empty_dir
{
        ext2_dir_entry dot;
        char dot_name_n_pad[EXT2_DIR_REC_LEN(sizeof(".") - 1) - sizeof dot];
        ext2_dir_entry dot_dot;
        char dot_dot_name_n_pad[EXT2_DIR_REC_LEN(sizeof("..") - 1) - sizeof dot_dot];

        ext2_empty_dir() = default;

        ext2_empty_dir(size_t dot_ino, size_t dot_dot_ino, size_t blksize)
            : dot({.inode     = (u32)dot_ino,
                   .rec_len   = EXT2_DIR_REC_LEN(sizeof(".") - 1),
                   .name_len  = sizeof(".") - 1,
                   .file_type = EXT2_FT_DIR})
            , dot_name_n_pad{'.'}
            , dot_dot({.inode     = (u32)dot_dot_ino,
                       .rec_len   = (u16)(blksize - EXT2_DIR_REC_LEN(sizeof("..") - 1)),
                       .name_len  = sizeof("..") - 1,
                       .file_type = EXT2_FT_DIR})
            , dot_dot_name_n_pad{'.', '.'}
        {
        }
};

static u8 const ext2_sys_dt_tab[] = {
    DT_UNKNOWN, /* (0) EXT2_FT_UNKNOWN - Unknown File Type */
    DT_REG,     /* (1) EXT2_FT_REG_FILE - Regular File */
    DT_DIR,     /* (2) EXT2_FT_DIR - Directory File */
    DT_CHR,     /* (3) EXT2_FT_CHRDEV - Character Device */
    DT_BLK,     /* (4) EXT2_FT_BLKDEV - Block Device */
    DT_FIFO,    /* (5) EXT2_FT_FIFO - Buffer File */
    DT_SOCK,    /* (6) EXT2_FT_SOCK - Socket File */
    DT_LNK      /* (7) EXT2_FT_SYMLINK - Symbolic Link */
};

static u8 const sys_dt_ext2_tab[] = {
    EXT2_FT_UNKNOWN,  /* (0) DT_UNKNOWN - Unknown File Type */
    EXT2_FT_FIFO,     /* (1) DT_FIFO - Buffer File */
    EXT2_FT_CHRDEV,   /* (2) DT_CHR - Character Device */
    EXT2_FT_UNKNOWN,  /* (3) DT_UNKNOWN - Unknown File Type */
    EXT2_FT_DIR,      /* (4) DT_DIR  - Directory File */
    EXT2_FT_UNKNOWN,  /* (5) DT_UNKNOWN - Unknown File Type */
    EXT2_FT_BLKDEV,   /* (6) DT_BLK  - Block Device */
    EXT2_FT_UNKNOWN,  /* (7) DT_UNKNOWN - Unknown File Type */
    EXT2_FT_REG_FILE, /* (8) DT_REG  - Regular File */
    EXT2_FT_UNKNOWN,  /* (9) DT_UNKNOWN - Unknown File Type */
    EXT2_FT_SYMLINK,  /* (10) DT_LNK - Symbolic Link */
    EXT2_FT_UNKNOWN,  /* (11) DT_UNKNOWN - Unknown File Type */
    EXT2_FT_SOCK,     /* (12) DT_SOCK - Socket File */
    EXT2_FT_UNKNOWN,  /* (13) DT_UNKNOWN - Unknown File Type */
    EXT2_FT_UNKNOWN,  /* (14) DT_WHT - Unknown File Type */
};

// ********************************************************************************************************************************
static void ext2dent_to_sysdent(ext2_dir_entry const *a_ext2_dirent, emerixx::dirent_t *a_sys_dirent)
// ********************************************************************************************************************************
{
        KASSERT(a_ext2_dirent->file_type < 8);
        a_sys_dirent->d_type = ext2_sys_dt_tab[a_ext2_dirent->file_type];

        a_sys_dirent->d_ino = a_ext2_dirent->inode;
        // a_sys_dirent->d_off    = 0; // Put something usefull here
        a_sys_dirent->d_reclen = ROUNDUP(offsetof(emerixx::dirent_t, d_name) + (a_ext2_dirent->name_len) + 2, sizeof(long));

        strncpy(&(a_sys_dirent->d_name[0]), &a_ext2_dirent->name[0], a_ext2_dirent->name_len);

        if (a_ext2_dirent->name_len >= EXT2_NAME_LEN)
        {
                log_error("e2dir name_len !(%d <  EXT2_NAME_LEN)", a_ext2_dirent->name_len);
                a_sys_dirent->d_name[EXT2_NAME_LEN - 1] = 0;

                a_sys_dirent->d_reclen = ROUNDUP(offsetof(emerixx::dirent_t, d_name) + EXT2_NAME_LEN + 2, sizeof(long));
        }
        else
        {
                a_sys_dirent->d_name[a_ext2_dirent->name_len] = 0;
        }

        a_sys_dirent->d_namlen = a_ext2_dirent->name_len;
}

// ********************************************************************************************************************************
static ext2_dir_entry *ext2_dirent_next(ext2_dir_entry *a_current, size_t block_mask, bool loop = false)
// ********************************************************************************************************************************
{
        ext2_dir_entry *result = nullptr;
        if ((((vm_address_t)a_current) + a_current->rec_len) & block_mask)
        {
                result = emerixx::add_ptr<ext2_dir_entry>(a_current, a_current->rec_len);
        }
        return (loop && !result) ? a_current : result;
}

// ********************************************************************************************************************************
inode::inode(ext2mount *mp, ext2_disk_inode *disk_inode)
    : vnode(mp)
    //, i_flag(0)
    , i_e2fs(&mp->e2fs)
    , i_dquot{0, 0}
    , i_number(0)
    , m_flags(disk_inode->i_flags)
    , i_block_u(disk_inode->i_block_u)
// ********************************************************************************************************************************
{
}

// ********************************************************************************************************************************
inode::inode(ext2mount *mp)
    : vnode(mp)
    //, i_flag(0)
    , i_e2fs(&mp->e2fs)
    , i_dquot{0, 0}
    , i_number(0)
    , m_flags(0)
    , i_block_u({})
// ********************************************************************************************************************************
{
}

// ********************************************************************************************************************************
inode::~inode()
// ********************************************************************************************************************************
{
}

// ********************************************************************************************************************************
kern_return_t inode::fs_ioctl(file *a_file, int a_cmd, void *a_data, int a_fflag)
// ********************************************************************************************************************************
{
        KASSERT(this->islocked());

        return KERN_FAIL(ENOTTY);
}

// ********************************************************************************************************************************
kern_return_t inode::fs_open(file *a_file, mode_t a_mode)
// ********************************************************************************************************************************
{
        KASSERT(islocked());
        kern_return_t retval = KERN_SUCCESS;

        if ((m_flags & O_APPEND) && (a_mode & (O_RDWR | O_APPEND)) == O_RDWR)
        {
                retval = KERN_FAIL(EPERM);
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t inode::fs_close(file *a_file)
// ********************************************************************************************************************************
{
        KASSERT(islocked());
        kern_return_t retval = KERN_SUCCESS;

        if (m_usecount > 1 && !(m_flags & IN_LOCKED))
        {
                struct timeval time;
                // get_time(&time);

                // ITIMES(ip, &time, &time);
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t inode::fs_poll(file *a_file, pollfd **)
// ********************************************************************************************************************************
{
        KASSERT(islocked());

        return KERN_FAIL(ENOTSUP);
}

// ********************************************************************************************************************************
static u32 htree_find_block(u32 a_hash, ext2_dx_root const *a_dx_root)
// ********************************************************************************************************************************
{
        i32 low  = 0;
        i32 high = a_dx_root->count - 1;

        i32 mid = 0;

        while (low < high)
        {
                mid = (low + high) / 2;

                if (a_hash < a_dx_root->entries[mid].hash)
                {
                        high = mid;
                }
                else
                {
                        low = mid + 1;
                }
        }

        return a_dx_root->entries[high - 1].block;
}

// ********************************************************************************************************************************
kern_return_t inode::fs_remove_dirent(emerixx::dirent_t *a_dent)
// ********************************************************************************************************************************
{
        KASSERT(islocked());

        buffer_cache::io_descriptor *dirent_iodesc;
        ext2_dir_entry *ext2_dirent;

        kern_return_t retval = vnode::read_fs_type(a_dent->d_off, &ext2_dirent, &dirent_iodesc);

        if (retval != KERN_SUCCESS)
        {
                return retval;
        }

        if (retval == KERN_SUCCESS)
        {
                // if (rminode->m_fstat.st_nlink == 0)
                //{
                ext2_dirent->inode = 0;
                //}

                buffer_cache::write(dirent_iodesc);
        }
        else
        {
                buffer_cache::release(dirent_iodesc);
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t inode::fs_insert_dirent(emerixx::dirent_t *a_dent)
// ********************************************************************************************************************************
{
        KASSERT(islocked());
        KASSERT(S_ISDIR(m_fstat.st_mode));

        vm_offset_t diroff  = 0;
        vm_offset_t dirsize = m_fstat.st_size;

        KASSERT(dirsize > 0);

        buffer_cache::io_descriptor *iodesc = nullptr;
        if (m_flags & EXT2_INDEX_FL)
        {
                log_warn("braah");
                ext2_dx_root *dx_root;
                kern_return_t retval = vnode::read_fs_type(0, &dx_root, &iodesc);

                if (retval != KERN_SUCCESS)
                {
                        return retval;
                }

                u32 hash_major;
                u32 hash_minor;
                KASSERT(dx_root->root_info.info_levels == 0);

                retval = ext2_htree_hash(a_dent->d_name,
                                         a_dent->d_namlen,
                                         ext2mount::get(this)->e2fs.s_es.s_hash_seed,
                                         dx_root->root_info.hash_version,
                                         &hash_major,
                                         &hash_minor);

                KASSERT(hash_major);

                if (retval != KERN_SUCCESS)
                {
                        buffer_cache::release(iodesc);
                        return retval;
                }

                diroff  = htree_find_block(hash_major, dx_root) * m_fstat.st_blksize;
                dirsize = diroff + m_fstat.st_blksize;

                buffer_cache::release(iodesc);
        }

        ext2_dir_entry *ext2_dent = nullptr;
        while (diroff < dirsize)
        {
                KASSERT((diroff & (m_fstat.st_blksize - 1)) == 0);

                kern_return_t retval = vnode::read_fs_type(diroff, &ext2_dent, &iodesc);

                if (retval != KERN_SUCCESS)
                {
                        return retval;
                }

                do
                {
                        if ((ext2_dent->inode == 0) && (ext2_dent->rec_len >= EXT2_DIR_REC_LEN(a_dent->d_namlen)))
                        {
                                break;
                        }
                        else if ((ext2_dent->inode != 0) && (ext2_dent->free_space() >= EXT2_DIR_REC_LEN(a_dent->d_namlen)))
                        {
                                diroff += ext2_dent->trimmed_reclen();

                                u16 new_reclen = ext2_dent->rec_len - ext2_dent->trimmed_reclen();

                                ext2_dent->trim();

                                ext2_dent = ext2_dirent_next(ext2_dent, m_fstat.st_blksize - 1);

                                *ext2_dent = {};

                                ext2_dent->rec_len = new_reclen;

                                KASSERT(ext2_dent->rec_len > 0);

                                break;
                        }

                        diroff += ext2_dent->rec_len;
                        ext2_dent = ext2_dirent_next(ext2_dent, m_fstat.st_blksize - 1);

                } while (ext2_dent != nullptr);

                if (ext2_dent != nullptr)
                {
                        break;
                }

                buffer_cache::release(iodesc);
                iodesc = nullptr;
        }

        // No space found
        if (ext2_dent == nullptr)
        {
                if (m_flags & EXT2_INDEX_FL)
                {
                        panic("Not ready");
                }

                KASSERT(iodesc == nullptr);
                KASSERT((diroff & (m_fstat.st_blksize - 1)) == 0);
                KASSERT(m_fstat.st_size == diroff);

                vnode::truncate(m_fstat.st_size + m_fstat.st_blksize, 0);

                kern_return_t retval = vnode::read_fs_type(diroff, &ext2_dent, &iodesc);

                if (retval != KERN_SUCCESS)
                {
                        return retval;
                }

                ext2_dent->rec_len = m_fstat.st_blksize;
                KASSERT(ext2_dent->rec_len > 0);
        }

        a_dent->d_off = diroff;

        ext2_dent->file_type = sys_dt_ext2_tab[a_dent->d_type];
        ext2_dent->inode     = a_dent->d_ino;
        ext2_dent->name_len  = a_dent->d_namlen;

        strncpy(ext2_dent->name, a_dent->d_name, a_dent->d_namlen);

        buffer_cache::write(iodesc);

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t inode::fs_get_page(size_t a_offset, vm_prot_t a_prot, vm_page_t *a_vm_page_out)
// ********************************************************************************************************************************
{
        panic("Should not be here");
        return KERN_FAILURE;
}

// ********************************************************************************************************************************
kern_return_t inode::fs_dir_initialize(fstat_t *a_parent_stat)
// ********************************************************************************************************************************
{
        KASSERT(this->islocked());
        KASSERT(m_fstat.st_size == 0);
        KASSERT(m_fstat.st_nlink == 0);

        kern_return_t retval;

        retval = vnode::truncate(m_fstat.st_blksize, 0);

        if (retval != KERN_SUCCESS)
        {
                return retval;
        }

        ext2_empty_dir *edir;
        buffer_cache::io_descriptor *iodesc;

        retval = vnode::read_fs_type(0, &edir, &iodesc);

        if (retval != KERN_SUCCESS)
        {
                return retval;
        }

        *edir = {m_fstat.st_ino, a_parent_stat->st_ino, m_fstat.st_blksize};

        buffer_cache::write(iodesc);

        return retval;
}

// ********************************************************************************************************************************
kern_return_t inode::fs_dir_reparent(fstat_t *a_parent_stat)
// ********************************************************************************************************************************
{
        KASSERT(this->islocked());
        KASSERT(m_fstat.st_size > 0);

        ext2_empty_dir *edir;
        buffer_cache::io_descriptor *iodesc;

        kern_return_t retval = vnode::read_fs_type(0, &edir, &iodesc);

        if (retval != KERN_SUCCESS)
        {
                return retval;
        }

        *edir = {m_fstat.st_ino, a_parent_stat->st_ino, m_fstat.st_blksize};

        edir->dot_dot.inode = a_parent_stat->st_ino;

        buffer_cache::write(iodesc);

        return retval;
}

// ********************************************************************************************************************************
kern_return_t inode::fs_dir_empty()
// ********************************************************************************************************************************
{
        KASSERT(this->islocked());

        if (m_fstat.st_nlink <= 2)
        {
                return KERN_SUCCESS;
        }

        return KERN_FAIL(ENOTEMPTY);
}

// ********************************************************************************************************************************
kern_return_t inode::fs_readdir(fs_readdir_cb_t a_cb, fs_readdir_ctx_t *a_ctx)
// ********************************************************************************************************************************
{
        KASSERT(this->islocked());

        off_t dirsize   = m_fstat.st_size;
        off_t diroffset = a_ctx->fs_offset;
        off_t blocksize = m_fstat.st_blksize;
        off_t blockmask = blocksize - 1;

        buffer_cache::io_descriptor *io_desc;
        kern_return_t retval = KERN_SUCCESS;

        emerixx::dirent_t dirent;

        // If we have a HTREE and a_name and no start offset, we try to determine it ourselves
        if ((m_flags & EXT2_INDEX_FL) && (a_ctx->name != nullptr) && (diroffset == 0))
        {
                ext2_dx_root *dx_root;
                retval = vnode::read_fs_type(0, &dx_root, &io_desc);

                if (retval != KERN_SUCCESS)
                {
                        return retval;
                }

                if ((a_ctx->namelen == 1) && a_ctx->name[0] == '.')
                {
                        ext2dent_to_sysdent(&dx_root->dot, &dirent);
                        dirent.d_off = 0;
                        retval       = (this->*a_cb)(&dirent, a_ctx);
                }
                else if ((a_ctx->namelen == 2) && (a_ctx->name[0] == '.') && (a_ctx->name[1] == '.'))
                {
                        ext2dent_to_sysdent(&dx_root->dotdot, &dirent);
                        dirent.d_off = dx_root->dot.rec_len;
                        retval       = (this->*a_cb)(&dirent, a_ctx);
                }
                else
                {

                        u32 hash_major;
                        u32 hash_minor;
                        KASSERT(dx_root->root_info.info_levels == 0);

                        retval = ext2_htree_hash(a_ctx->name,
                                                 a_ctx->namelen,
                                                 ext2mount::get(this)->e2fs.s_es.s_hash_seed,
                                                 dx_root->root_info.hash_version,
                                                 &hash_major,
                                                 &hash_minor);

                        KASSERT(hash_major);

                        if (retval == KERN_SUCCESS)
                        {
                                diroffset = htree_find_block(hash_major, dx_root) * blocksize;
                                dirsize   = diroffset + blocksize;
                        }
                }

                buffer_cache::release(io_desc);

                if (a_ctx->exit || (retval != KERN_SUCCESS))
                {
                        return retval;
                }
        }

        ext2_dir_entry *ext2_dirent;
        while (diroffset < dirsize)
        {
                retval = vnode::read_fs_type(diroffset, &ext2_dirent, &io_desc);

                if (retval != KERN_SUCCESS)
                {
                        break;
                }

                do
                {
                        if (ext2_dirent->rec_len == 0)
                        {
                                retval = KERN_FAIL(EINVAL);
                                break;
                        }

                        if (ext2_dirent->inode != 0)
                        {
                                ext2dent_to_sysdent(ext2_dirent, &dirent);
                                dirent.d_off = diroffset;
                                retval       = (this->*a_cb)(&dirent, a_ctx);

                                if (a_ctx->exit)
                                {
                                        break;
                                }
                        }

                        diroffset += ext2_dirent->rec_len;

                        ext2_dirent = ext2_dirent_next(ext2_dirent, blockmask);

                        if ((retval != KERN_SUCCESS) || (a_ctx->exit))
                        {
                                break;
                        }

                } while (ext2_dirent != nullptr);

                buffer_cache::release(io_desc);

                if ((retval != KERN_SUCCESS) || (a_ctx->exit))
                {
                        break;
                }
        }

        a_ctx->fs_offset = diroffset;

        return retval;
}

// ********************************************************************************************************************************
kern_return_t inode::fs_readwrite_link(uio *a_uio)
// ********************************************************************************************************************************
{
        KASSERT(this->islocked());
        kern_return_t retval;

        if (a_uio->uio_rw == UIO_READ)
        {
                if (m_fstat.st_size < sizeof(i_block_u.i_block_bytes))
                {
                        retval = uiomove(i_block_u.i_block_bytes, m_fstat.st_size, a_uio);
                }
                else
                {
                        char *buffer;
                        buffer_cache::io_descriptor *iodesc;
                        retval = vnode::read_fs_type(0, &buffer, &iodesc);

                        if (retval == KERN_SUCCESS)
                        {
                                retval = uiomove(buffer, m_fstat.st_blksize, a_uio);
                                buffer_cache::release(iodesc);
                        }
                }
        }
        else
        {
                if (a_uio->uio_resid < sizeof(i_block_u.i_block_bytes))
                {
                        m_fstat.st_size = a_uio->uio_resid;
                        retval          = uiomove(i_block_u.i_block_bytes, m_fstat.st_size, a_uio);
                }
                else
                {
                        char *buffer;
                        buffer_cache::io_descriptor *iodesc;
                        retval = vnode::read_fs_type(0, &buffer, &iodesc);
                        if (retval == KERN_SUCCESS)
                        {
                                retval = uiomove(buffer, m_fstat.st_blksize, a_uio);
                                buffer_cache::write(iodesc);
                        }
                }
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t inode::fs_readwrite_spec(file *a_file, uio *a_uio, int a_ioflag)
// ********************************************************************************************************************************
{
        KASSERT(this->islocked());
        return KERN_FAIL(ENODEV);
}

// ********************************************************************************************************************************
kern_return_t inode::bmap(
    size_t a_bn, size_t *a_bnp, lbn_path_t a_lbn_path_out, size_t *a_lbn_path_len_out, size_t *a_i_block_idx_out)
// ********************************************************************************************************************************
{
        KASSERT(this->islocked());

        ext2mount *e2mount = static_cast<ext2mount *>(v_mount);

        size_t i_block_idx;
        lbn_path_t lbn_path = {0, 0, 0};
        size_t lbn_path_len;

        kern_return_t result
            = e2mount->generate_lbn_path(lblkno(&(e2mount->e2fs), a_bn), a_lbn_path_out, a_lbn_path_len_out, a_i_block_idx_out);

        if (result == KERN_SUCCESS)
        {
                *a_bnp = i_block_u.i_block[*a_i_block_idx_out];

                // struct buf *indbp;
                buffer_cache::io_descriptor *io_desc;
                for (size_t idx = 0; idx < (*a_lbn_path_len_out) && (*a_bnp) > 0 && result == KERN_SUCCESS; ++idx)
                {

                        // panic("not ready");
                        if (result = e2mount->fsio_read(*a_bnp, &io_desc); result == KERN_SUCCESS)
                        {
                                u32 *bns;
                                io_desc->get_data(&bns);

                                *a_bnp = bns[a_lbn_path_out[idx]];

                                // Replace the logical offsets with physical
                                // a_lbn_path_out[idx] = *a_bnp;

                                buffer_cache::release(io_desc);
                        }
                }
        }

        if (result == KERN_SUCCESS && (*a_bnp) > 0)
        {
                *a_bnp = lblktosize(&(e2mount->e2fs), *a_bnp);
        }

        return result;
}

// ********************************************************************************************************************************
kern_return_t inode::fs_strategy(buffer_cache::io_descriptor *a_io_desc)
// ********************************************************************************************************************************
{
        KASSERT(this->islocked());
        ext2mount *mnt = static_cast<ext2mount *>(v_mount);

        vm_offset_t vn_offset = a_io_desc->get_logical_block_address();
        size_t block_size     = a_io_desc->get_block_size();
        vm_offset_t vn_size   = ROUNDUP(m_fstat.st_size, m_fstat.st_blksize);
        lbn_path_t lbn_path;
        size_t lbn_path_len;
        size_t i_block_idx;

        KASSERT(vn_size != 0);

        kern_return_t result = KERN_SUCCESS;

        if (a_io_desc->m_phys_block_address == vn_offset)
        {
                if (vn_offset < vn_size)
                {
                        if (result = bmap(a_io_desc->m_phys_block_address,
                                          &(a_io_desc->m_phys_block_address),
                                          &lbn_path[0],
                                          &lbn_path_len,
                                          &i_block_idx);
                            result == KERN_SUCCESS)
                        {
                                if (a_io_desc->m_phys_block_address == 0)
                                {
                                        if (a_io_desc->m_flags & IO_DESCRIPTOR_READ)
                                        {
                                                a_io_desc->clear_data();
                                                // a_io_desc->m_flags |= IO_DESCRIPTOR_IODONE;
                                                a_io_desc->m_phys_block_address = vn_offset;
                                                a_io_desc->io_done(KERN_SUCCESS);
                                                // i_din.i_blocks += btodb(block_size);
                                                return KERN_SUCCESS;
                                        }
                                        else
                                        {
                                                // Test
                                                u32 _lbn_path[] = {i_block_idx, lbn_path[0], lbn_path[1], lbn_path[2]};
                                                lbn_path_len++;
                                                // Teste end

                                                u32 *block_ptrs    = &i_block_u.i_block[0];
                                                u32 *block_ptr_idx = &_lbn_path[0];

                                                buffer_cache::io_descriptor *io_desc = nullptr;

                                                for (size_t i = 0; i < lbn_path_len - 1; ++i)
                                                {
                                                        if (io_desc != nullptr)
                                                        {
                                                                buffer_cache::write(io_desc);
                                                        }

                                                        if (block_ptrs[(*block_ptr_idx)] == 0)
                                                        {
                                                                // Goes to device vnode
                                                                KASSERT(mnt->block_alloc(this, &block_ptrs[(*block_ptr_idx)])
                                                                        == KERN_SUCCESS);
                                                                m_fstat.st_blocks += btodb(block_size);

                                                                result = mnt->fsio_get(block_ptrs[(*block_ptr_idx)], &io_desc);

                                                                KASSERT(result == KERN_SUCCESS);

                                                                io_desc->clear_data();
                                                        }
                                                        else
                                                        {
                                                                result = mnt->fsio_read(block_ptrs[(*block_ptr_idx)], &io_desc);

                                                                KASSERT(result == KERN_SUCCESS);
                                                        }

                                                        io_desc->get_data(&block_ptrs);
                                                        block_ptr_idx++;
                                                }

                                                // Goes to this vnode
                                                KASSERT(mnt->block_alloc(this, &block_ptrs[(*block_ptr_idx)]) == KERN_SUCCESS);
                                                // i_blocks are counted then "read" was performed above

                                                a_io_desc->m_phys_block_address
                                                    = lblktosize(&(mnt->e2fs), block_ptrs[(*block_ptr_idx)]);

                                                m_fstat.st_blocks += btodb(block_size);

                                                if (io_desc != nullptr)
                                                {
                                                        buffer_cache::write(io_desc);
                                                }
                                        }
                                }
                        }
                }
                else
                {
                        log_debug("IO beyond vnode size");
                        result = KERN_FAIL(EIO);
                }
        }

        if (result == KERN_SUCCESS)
        {
                result = vnode::locked_ptr { ext2mount::get(this)->um_devvp }
                ->strategy(a_io_desc);
        }

        return result;
}