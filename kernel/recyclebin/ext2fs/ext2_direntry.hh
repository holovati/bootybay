#pragma once

/*
 * EXT2_DIR_PAD defines the directory entries boundaries
 *
 * NOTE: It must be a multiple of 4
 */
#define EXT2_DIR_PAD               4
#define EXT2_DIR_ROUND             (EXT2_DIR_PAD - 1)
#define EXT2_DIR_REC_LEN(name_len) ((u16)(((name_len) + 8 + EXT2_DIR_ROUND) & ~EXT2_DIR_ROUND))

struct ext2_dir_entry
{
        /**
         * @brief
         * 32bit inode number of the file entry. A value of 0 indicate that the entry
         * is not used.
         */
        u32 inode;

        /**
         * @brief
         * 16bit unsigned displacement to the next directory entry from the start of
         * the current directory entry. This field must have a value at least equal to
         * the length of the current record.
         *
         * The directory entries must be aligned on 4 bytes boundaries and there
         * cannot be any directory entry spanning multiple data blocks. If an entry
         * cannot completely fit in one block, it must be pushed to the next data
         * block and the rec_len of the previous entry properly adjusted.
         *
         * Note
         * Since this value cannot be negative, when a file is removed the previous
         * record within the block has to be modified to point to the next valid
         * record within the block or to the end of the block when no other directory
         * entry is present.
         *
         * If the first entry within the block is removed, a blank record will be
         * created and point to the next directory entry or to the end of the block.
         */
        u16 rec_len;

        /**
         * @brief
         * 8bit unsigned value indicating how many bytes of character data are
         * contained in the name.
         *
         * Note
         * This value must never be larger than rec_len - 8. If the directory entry
         * name is updated and cannot fit in the existing directory entry, the entry
         * may have to be relocated in a new directory entry of sufficient size and
         * possibly stored in a new data block.
         */
        u8 name_len;

        /**
         * @brief
         * file_type
         * 8bit unsigned value used to indicate file type.
         *
         * Note
         * In revision 0, this field was the upper 8-bit of the then 16-bit name_len.
         * Since all implementations still limited the file names to 255 characters
         * this 8-bit value was always 0.
         *
         * This value must match the inode type defined in the related inode entry.
         */
        u8 file_type;

        /**
         * @brief
         * Name of the entry. The ISO-Latin-1 character set is expected in most
         * system. The name must be no longer than 255 bytes after encoding.
         */
        char name[0];

        bool is_dot() { return name_len == 1 && name[0] == '.'; }

        bool is_dotdot() { return name_len == 2 && name[0] == '.' && name[1] == '.'; }

        void trim() { rec_len = name_len ? EXT2_DIR_REC_LEN(name_len) : 0; }

        u16 trimmed_reclen() { return name_len ? EXT2_DIR_REC_LEN(name_len) : 0; }

        u16 free_space() { return rec_len - trimmed_reclen(); }
};
