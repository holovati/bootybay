/*
 *  modified for EXT2FS support in Lites 1.1
 *
 *  Aug 1995, Godmar Back (gback@cs.utah.edu)
 *  University of Utah, Department of Computer Science
 */
/*
 *  linux/include/linux/ext2_fs.h
 *
 * Copyright (C) 1992, 1993, 1994, 1995
 * Remy Card (card@masi.ibp.fr)
 * Laboratoire MASI - Institut Blaise Pascal
 * Universite Pierre et Marie Curie (Paris VI)
 *
 *  from
 *
 *  linux/include/linux/minix_fs.h
 *
 *  Copyright (C) 1991, 1992  Linus Torvalds
 */
#ifndef _LINUX_EXT2_FS_H
#define _LINUX_EXT2_FS_H

/* the Linux implementation of EXT2 stores some information about
 * an inode in a ext2_inode_info structure which is part of the incore
 * inode in Linux
 * I decided to use the i_spare[11] fields instead - we'll see how this
 * works out
 */

/*
 * The second extended filesystem constants/structures
 */

/*
 * Define EXT2FS_DEBUG to produce debug messages
 */
#undef EXT2FS_DEBUG

/*
 * Define EXT2FS_DEBUG_CACHE to produce cache debug messages
 */
#undef EXT2FS_DEBUG_CACHE

/*
 * Define EXT2FS_CHECK_CACHE to add some checks to the name cache code
 */
#undef EXT2FS_CHECK_CACHE

/*
 * Define EXT2FS_PRE_02B_COMPAT to convert ext 2 fs prior to 0.2b
 */
#undef EXT2FS_PRE_02B_COMPAT

/*
 * Define EXT2_PREALLOCATE to preallocate data blocks for expanding files
 */
#define EXT2_PREALLOCATE

/*
 * The second extended file system version
 */
#define EXT2FS_DATE    "95/03/19"
#define EXT2FS_VERSION "0.5a"

/*
 * Debug code
 */
#ifdef EXT2FS_DEBUG
#define ext2_debug(f, a...)                                                                                                        \
        {                                                                                                                          \
                printf("EXT2-fs DEBUG (%s, %d): %s:", __FILE__, __LINE__, __FUNCTION__);                                           \
                printf(f, ##a);                                                                                                    \
        }
#else
#define ext2_debug(f, ...) /**/
#endif

/*
 * Special inodes numbers
 */
#define EXT2_BAD_INO         1                      /* Bad blocks inode */
#define EXT2_ROOT_INO        2                      /* Root inode */
#define EXT2_ACL_IDX_INO     3                      /* ACL inode */
#define EXT2_ACL_DATA_INO    4                      /* ACL inode */
#define EXT2_BOOT_LOADER_INO 5                      /* Boot loader inode */
#define EXT2_UNDEL_DIR_INO   6                      /* Undelete directory inode */
#define EXT2_FIRST_INO(sb)   (sb)->s_es.s_first_ino /* First non reserved inode */

/*
 * The second extended file system magic number
 */
#define EXT2_PRE_02B_MAGIC 0xEF51
#define EXT2_SUPER_MAGIC   0xEF53

/*
 * Maximal count of links to a file
 */
#define EXT2_LINK_MAX 32000

/*
 * Macro-instructions used to manage several block sizes
 */
#define EXT2_MIN_BLOCK_SIZE     1024
#define EXT2_MAX_BLOCK_SIZE     4096
#define EXT2_MIN_BLOCK_LOG_SIZE 10

#define EXT2_BLOCK_SIZE(s)      ((s)->s_blocksize)
#define EXT2_ACLE_PER_BLOCK(s)  (EXT2_BLOCK_SIZE(s) / sizeof(struct ext2_acl_entry))
#define EXT2_ADDR_PER_BLOCK(s)  (EXT2_BLOCK_SIZE(s) / sizeof(u32))
#define EXT2_BLOCK_SIZE_BITS(s) ((s)->s_log_block_size + 10)

#define EXT2_INODE_SIZE(s) (s)->s_es.s_inode_size
/* ought to be  sizeof (struct ext2_inode)) */
#define EXT2_INODES_PER_BLOCK(s) ((s)->s_inodes_per_block)

/*
 * Macro-instructions used to manage fragments
 */
#define EXT2_MIN_FRAG_SIZE      1024
#define EXT2_MAX_FRAG_SIZE      4096
#define EXT2_MIN_FRAG_LOG_SIZE  10
#define EXT2_FRAG_SIZE(s)       ((s)->s_frag_size)
#define EXT2_FRAGS_PER_BLOCK(s) (EXT2_BLOCK_SIZE(s) / EXT2_FRAG_SIZE(s))

/*
 * ACL structures
 */
struct ext2_acl_header /* Header of Access Control Lists */
{
        u32 aclh_size;
        u32 aclh_file_count;
        u32 aclh_acle_count;
        u32 aclh_first_acle;
};

struct ext2_acl_entry /* Access Control List Entry */
{
        u32 acle_size;
        u16 acle_perms; /* Access permissions */
        u16 acle_type;  /* Type of entry */
        u16 acle_tag;   /* User or group identity */
        u16 acle_pad1;
        u32 acle_next; /* Pointer on next entry for the */
                       /* same inode or on next free entry */
};

struct ext2_group_desc
{
        u32 bg_block_bitmap;      /* Blocks bitmap block */
        u32 bg_inode_bitmap;      /* Inodes bitmap block */
        u32 bg_inode_table;       /* Inodes table block */
        u16 bg_free_blocks_count; /* Free blocks count */
        u16 bg_free_inodes_count; /* Free inodes count */
        u16 bg_used_dirs_count;   /* Directories count */
        u16 bg_pad;               /* Alignment to word */
        u32 bg_reserved[3];       /* Nulls to pad out 24 bytes */
};

static_assert(sizeof(ext2_group_desc) == 32, "ext2_group_desc wrong size");

/*
 * Macro-instructions used to manage group descriptors
 */
#define EXT2_INODES_PER_GROUP(s) ((s)->s_inodes_per_group)
#define EXT2_DESC_PER_BLOCK(s)   (EXT2_BLOCK_SIZE(s) / sizeof(struct ext2_group_desc))
#define EXT2_BLOCKS_PER_GROUP(s) ((s)->s_blocks_per_group)

/*
 * Constants relative to the data blocks
 */
#define EXT2_NDIR_BLOCKS   12
#define EXT2_IND_BLOCK     EXT2_NDIR_BLOCKS
#define EXT2_DIND_BLOCK    (EXT2_IND_BLOCK + 1)
#define EXT2_TIND_BLOCK    (EXT2_DIND_BLOCK + 1)
#define EXT2_N_BLOCKS      (EXT2_TIND_BLOCK + 1)
#define EXT2_NIDIR_BLOCKS  (EXT2_N_BLOCKS - EXT2_NDIR_BLOCKS)
#define EXT2_MAXSYMLINKLEN (EXT2_N_BLOCKS * sizeof(u32))

/*
 * Inode flags
 */
#define EXT2_SECRM_FL        0x00000001 //	secure deletion
#define EXT2_UNRM_FL         0x00000002 //	record for undelete
#define EXT2_COMPR_FL        0x00000004 //	compressed file
#define EXT2_SYNC_FL         0x00000008 //	synchronous updates
#define EXT2_IMMUTABLE_FL    0x00000010 //	immutable file
#define EXT2_APPEND_FL       0x00000020 //	append only
#define EXT2_NODUMP_FL       0x00000040 //	do not dump/delete file
#define EXT2_NOATIME_FL      0x00000080 //	do not update .i_atime
#define EXT2_DIRTY_FL        0x00000100 //	dirty (file is in use?)
#define EXT2_COMPRBLK_FL     0x00000200 //	compressed blocks
#define EXT2_NOCOMPR_FL      0x00000400 //	access raw compressed data
#define EXT2_ECOMPR_FL       0x00000800 //	compression error
#define EXT2_BTREE_FL        0x00001000 //	b-tree format directory
#define EXT2_INDEX_FL        0x00001000 //	Hash indexed directory
#define EXT2_IMAGIC_FL       0x00002000 //	?
#define EXT3_JOURNAL_DATA_FL 0x00004000 //	journal file data
#define EXT2_RESERVED_FL     0x80000000 //	reserved for ext2 implementation

/*
 * ioctl commands
 */
#define EXT2_IOC_GETFLAGS   _IOR('f', 1, long)
#define EXT2_IOC_SETFLAGS   _IOW('f', 2, long)
#define EXT2_IOC_GETVERSION _IOR('v', 1, long)
#define EXT2_IOC_SETVERSION _IOW('v', 2, long)

/*
 * File system states
 */
#define EXT2_VALID_FS 0x0001 /* Unmounted cleanly */
#define EXT2_ERROR_FS 0x0002 /* Errors detected */

/*
 * Mount flags
 */
#define EXT2_MOUNT_CHECK_NORMAL 0x0001 /* Do some more checks */
#define EXT2_MOUNT_CHECK_STRICT 0x0002 /* Do again more checks */
#define EXT2_MOUNT_CHECK        (EXT2_MOUNT_CHECK_NORMAL | EXT2_MOUNT_CHECK_STRICT)
#define EXT2_MOUNT_GRPID        0x0004 /* Create files with directory's group */
#define EXT2_MOUNT_DEBUG        0x0008 /* Some debugging messages */
#define EXT2_MOUNT_ERRORS_CONT  0x0010 /* Continue on errors */
#define EXT2_MOUNT_ERRORS_RO    0x0020 /* Remount fs ro on errors */
#define EXT2_MOUNT_ERRORS_PANIC 0x0040 /* Panic on errors */
#define EXT2_MOUNT_MINIX_DF     0x0080 /* Mimics the Minix statfs */

#define clear_opt(o, opt) o &= ~EXT2_MOUNT_##opt
#define set_opt(o, opt)   o |= EXT2_MOUNT_##opt
#define test_opt(sb, opt) ((sb)->u.ext2_sb.s_mount_opt & EXT2_MOUNT_##opt)
/*
 * Maximal mount counts between two filesystem checks
 */
#define EXT2_DFL_MAX_MNT_COUNT 20 /* Allow 20 mounts */
#define EXT2_DFL_CHECKINTERVAL 0  /* Don't use interval check */

/*
 * Behaviour when detecting errors
 */
#define EXT2_ERRORS_CONTINUE 1 /* Continue execution */
#define EXT2_ERRORS_RO       2 /* Remount fs read-only */
#define EXT2_ERRORS_PANIC    3 /* Panic */
#define EXT2_ERRORS_DEFAULT  EXT2_ERRORS_CONTINUE

#define EXT2_OS_LINUX 0
#define EXT2_OS_HURD  1
#define EXT2_OS_MASIX 2

#define EXT2_CURRENT_REV 0

#define EXT2_DEF_RESUID 0
#define EXT2_DEF_RESGID 0

/*
 * Structure of a directory entry
 */
#define EXT2_NAME_LEN    255
#define EXT2_FT_UNKNOWN  0 /* Unknown File Type */
#define EXT2_FT_REG_FILE 1 /* Regular File */
#define EXT2_FT_DIR      2 /* Directory File */
#define EXT2_FT_CHRDEV   3 /* Character Device */
#define EXT2_FT_BLKDEV   4 /* Block Device */
#define EXT2_FT_FIFO     5 /* Buffer File */
#define EXT2_FT_SOCK     6 /* Socket File */
#define EXT2_FT_SYMLINK  7 /* Symbolic Link */

/*
 * Each disk drive contains some number of file systems.
 * A file system consists of a number of cylinder groups.
 * Each cylinder group has inodes and data.
 *
 * A file system is described by its super-block, which in turn
 * describes the cylinder groups.  The super-block is critical
 * data and is replicated in each cylinder group to protect against
 * catastrophic loss.  This is done at `newfs' time and the critical
 * super-block data does not change, so the copies need not be
 * referenced further unless disaster strikes.
 *
 * The first boot and super blocks are given in absolute disk addresses.
 * The byte-offset forms are preferred, as they don't imply a sector size.
 */
#define BBSIZE 1024
#define SBSIZE 1024
#define BBOFF  ((off_t)(0))
#define SBOFF  ((off_t)(BBOFF + BBSIZE))
#define BBLOCK ((size_t)(0))
#define SBLOCK ((size_t)(BBLOCK + BBSIZE / DEV_BSIZE))

/*
 * The path name on which the file system is mounted is maintained
 * in fs_fsmnt. MAXMNTLEN defines the amount of space allocated in
 * the super block for this name.
 */
#define MAXMNTLEN 512

/*
 * Macros for access to superblock array structures
 */

/*
 * Convert cylinder group to base address of its global summary info.
 */
#define fs_cs(fs, cgindx) ((fs->s_group_desc[cgindx]))

/*
 * Turn file system block numbers into disk block addresses.
 * This maps file system blocks to device size blocks.
 */
#define fsbtodb(fs, b) ((b) << ((fs)->s_fsbtodb))
#define dbtofsb(fs, b) ((b) >> ((fs)->s_fsbtodb))

/* get group containing inode */
#define ino_to_cg(fs, x) (((x)-1) / EXT2_INODES_PER_GROUP(fs))

/* get block containing inode from its number x */
#define ino_to_fsba(fs, x)                                                                                                         \
        fs_cs(fs, ino_to_cg(fs, x)).bg_inode_table + (((x)-1) % EXT2_INODES_PER_GROUP(fs)) / EXT2_INODES_PER_BLOCK(fs)

/* get offset for inode in block */
#define ino_to_fsbo(fs, x) ((x - 1) % EXT2_INODES_PER_BLOCK(fs))

/*
 * Give cylinder group number for a file system block.
 * Give cylinder group block number for a file system block.
 */
#define dtog(fs, d)  (((d)-fs->s_es->s_first_data_block) / EXT2_BLOCKS_PER_GROUP(fs))
#define dtogd(fs, d) (((d)-fs->s_es->s_first_data_block) % EXT2_BLOCKS_PER_GROUP(fs))

/*
 * The following macros optimize certain frequently calculated
 * quantities by using shifts and masks in place of divisions
 * modulos and multiplications.
 */
#define blkoff(fs, loc) /* calculates (loc % fs->fs_bsize) */ ((loc) & (fs)->s_qbmask)

#define lblktosize(fs, blk) /* calculates (blk * fs->fs_bsize) */ ((blk) << ((fs)->s_bshift))

#define lblkno(fs, loc) /* calculates (loc / fs->fs_bsize) */ ((loc) >> ((fs)->s_bshift))

/* no fragments -> logical block number equal # of frags */
#define numfrags(fs, loc) /* calculates (loc / fs->fs_fsize) */ ((loc) >> ((fs)->s_bshift))

#define fragroundup(fs, size) /* calculates roundup(size, fs->fs_fsize) */ roundup(size, fs->s_frag_size)
/* was (((size) + (fs)->fs_qfmask) & (fs)->fs_fmask) */

/*
 * Determining the size of a file block in the file system.
 * easy w/o fragments
 */
#define blksize(fs, ip, lbn) ((fs)->s_frag_size)

/*
 * INOPB is the number of inodes in a secondary storage block.
 */
#define INOPB(fs) EXT2_INODES_PER_BLOCK(fs)

/*
 * NINDIR is the number of indirects in a file system block.
 */
#define NINDIR(fs) (EXT2_ADDR_PER_BLOCK(fs))

/* a few remarks about superblock locking/unlocking
 * Linux provides special routines for doing so
 * I haven't figured out yet what BSD does
 * I think I'll try a VOP_LOCK/VOP_UNLOCK on the device vnode
 */

struct ext2_inode;

using lbn_path_t = size_t[3];

#endif /* _LINUX_EXT2_FS_H */
