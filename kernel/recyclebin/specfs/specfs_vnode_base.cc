#include "specfs_vnode_base.hh"

#include <mach/conf.h>
#include <vfs/mount.h>

// ********************************************************************************************************************************
kern_return_t specfs_vnode_base::fs_ioctl(file *a_file, int a_cmd, void *a_data, int a_fflag)
// ********************************************************************************************************************************
{
        KASSERT(islocked());

        kern_return_t retval;
        switch (m_fstat.st_mode & S_IFMT)
        {
        case S_IFBLK:
                retval = (*bdevsw[major(m_fstat.st_rdev)].d_ioctl)(m_fstat.st_rdev, a_cmd, (char *)a_data, a_fflag);
                break;

        default:
                retval = KERN_FAIL(ENXIO);
                break;
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t specfs_vnode_base::fs_open(file *a_file, mode_t a_mode)
// ********************************************************************************************************************************
{
        KASSERT(islocked());

        kern_return_t retval;

        switch (m_fstat.st_mode & S_IFMT)
        {
        case S_IFBLK:
                retval = (*bdevsw[major(m_fstat.st_rdev)].d_open)(m_fstat.st_rdev, m_fstat.st_mode, S_IFBLK);
                break;
        default:
                retval = KERN_FAIL(ENXIO);
                break;
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t specfs_vnode_base::fs_close(file *a_file)
// ********************************************************************************************************************************
{
        KASSERT(islocked());

        kern_return_t retval;

        switch (m_fstat.st_mode & S_IFMT)
        {
        case S_IFBLK:
                retval = (*bdevsw[major(m_fstat.st_rdev)].d_close)(m_fstat.st_rdev);
                break;

        default:
                retval = KERN_FAIL(ENXIO);
                break;
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t specfs_vnode_base::fs_poll(file *a_file, pollfd **a_pfd)
// ********************************************************************************************************************************
{
        KASSERT(islocked());

        kern_return_t retval;

        retval = KERN_FAIL(ENOTSUP);
        // retval = (*bdevsw[major(m_fstat.st_rdev)].d_poll)(m_fstat.st_rdev, a_pollfd);

        return retval;
}

// ********************************************************************************************************************************
kern_return_t specfs_vnode_base::fs_remove_dirent(emerixx::dirent_t *a_dirent)
// ********************************************************************************************************************************
{
        return KERN_FAIL(ENOENT);
}

// ********************************************************************************************************************************
kern_return_t specfs_vnode_base::fs_insert_dirent(emerixx::dirent_t *a_dirent)
// ********************************************************************************************************************************
{
        return KERN_FAIL(EINVAL);
}

// ********************************************************************************************************************************
kern_return_t specfs_vnode_base::fs_get_page(size_t a_offset, vm_prot_t a_prot, vm_page_t *a_vm_page_out)
// ********************************************************************************************************************************
{
        kern_return_t retval;

        if (S_ISCHR(m_fstat.st_mode))
        {
                dev_t dev_major = major(m_fstat.st_rdev);
                // Check if we have the page at the given offset
                *a_vm_page_out = vm_page_map_lookup(&m_page_map, a_offset);

                // If we do, return it.
                if (*a_vm_page_out)
                {
                        retval = KERN_SUCCESS;
                }
                else
                {
                        retval = KERN_FAILURE;
                }
        }
        else
        {
                retval = KERN_FAILURE;
                panic("Should not be here");
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t specfs_vnode_base::fs_readdir(fs_readdir_cb_t a_cb, fs_readdir_ctx_t *a_ctx)
// ********************************************************************************************************************************
{
        return KERN_FAIL(ENOTDIR);
}

// ********************************************************************************************************************************
kern_return_t specfs_vnode_base::fs_dir_initialize(fstat_t *a_parent_stat)
// ********************************************************************************************************************************
{
        return KERN_FAIL(ENOTDIR);
}

// ********************************************************************************************************************************
kern_return_t specfs_vnode_base::fs_dir_reparent(fstat_t *a_parent_stat)
// ********************************************************************************************************************************
{
        return KERN_FAIL(ENOTDIR);
}

// ********************************************************************************************************************************
kern_return_t specfs_vnode_base::fs_dir_empty()
// ********************************************************************************************************************************
{
        return KERN_FAIL(ENOTDIR);
}

// ********************************************************************************************************************************
kern_return_t specfs_vnode_base::fs_readwrite_link(uio *a_uio)
// ********************************************************************************************************************************

{
        return KERN_FAIL(EINVAL);
}

// ********************************************************************************************************************************
kern_return_t specfs_vnode_base::fs_readwrite_spec(file *a_file, uio *a_uio, int a_ioflag)
// ********************************************************************************************************************************
{
        kern_return_t retval = KERN_SUCCESS;

        size_t iosize = static_cast<size_t>(a_uio->uio_offset + a_uio->uio_resid);
        if (iosize > m_fstat.st_size)
        {
                retval = KERN_FAIL(ENOSPC);
        }
        else
        {
                retval = buffered_uio_move(a_uio);
        }

        return retval;
}

// ********************************************************************************************************************************
specfs_vnode_base::specfs_vnode_base(mount *mp)
    : vnode(mp)
// ********************************************************************************************************************************
{
}

// ********************************************************************************************************************************
specfs_vnode_base::~specfs_vnode_base()
// ********************************************************************************************************************************
{
}
