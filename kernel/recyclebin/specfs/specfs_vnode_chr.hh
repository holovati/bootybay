#pragma once

#include "specfs_vnode_base.hh"

struct specfs_vnode_chr final : specfs_vnode_base
{
        kern_return_t fs_strategy(buffer_cache::io_descriptor *) override;

        specfs_vnode_chr(mount *mp);

        ~specfs_vnode_chr();
};