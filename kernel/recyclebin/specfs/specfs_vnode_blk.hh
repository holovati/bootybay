#pragma once
#include "specfs_vnode_base.hh"

/*
 * Flags for specinfo
 */
#define SI_MOUNTEDON 0x0001 /* block special device is mounted on */

struct specfs_vnode_blk final : specfs_vnode_base
{
        kern_return_t fs_strategy(buffer_cache::io_descriptor *a_io_desc) override;

        specfs_vnode_blk(mount *mp);

        ~specfs_vnode_blk();

        integer_t m_flags;
        vm_offset_t m_phys_offset;
};