#include "specfs_vnode_blk.hh"
#include "specfs_mount.hh"

#include <fcntl.h>
#include <mach/buf.h>
#include <mach/conf.h> // bdevsw
#include <mach/disklabel.h>
#include <mach/machine/spl.h> // XXX

#include <sys/stat.h> // S_IFBLK

#include <string.h>

// In the future, we should move bdevsw here
extern u32 nblkdev;
#if 0
int specfs_vnode_blk::open(mode_t a_mode, ucred *a_cred, pthread_t a_p)
{
        /*
         * Don't allow open if fs is mounted -nodev.
         */
        if (v_mount && (v_mount->mnt_flag & MNT_NODEV))
        {
                return (-ENXIO);
        }

#if 0
        /*
         * When running in very secure mode, do not allow
         * opens for writing of any disk block devices.
         */

      /* (Damir) Disable for now. What does FWRITE do */
      if (securelevel >= 2 && ap->a_cred != FSCRED && (ap->a_mode & FWRITE) &&
          isdisk(dev, VBLK)) {
        return (EPERM);
      }
#endif
        /*
         * Do not allow opens of block devices that are
         * currently mounted.
         */
        if (m_flags & SI_MOUNTEDON)
        {
                return -EBUSY;
        }

        return ((*bdevsw[major(m_fstat.st_rdev)].d_open)(m_fstat.st_rdev, a_mode, S_IFBLK, a_p));
}

int specfs_vnode_blk::close(int a_fflag, ucred *a_cred, pthread_t a_p)
{
        panic("not ready");
#if DAMIR // Figure out a smarter way to do this
        /*
         * On last close of a block device (that isn't mounted)
         * we must invalidate any in core blocks, so that
         * we can, for instance, change floppy disks.
         */
        if (int error = vinvalbuf(this, V_SAVE, a_cred, a_p, 0, 0); error != 0)
        {
                return (error);
        }
        /*
         * We do not want to really close the device if it
         * is still in use unless we are trying to close it
         * forcibly. Since every use (buffer, vnode, swap, cmap)
         * holds a reference to the vnode, and because we mark
         * any other vnodes that alias this device, when the
         * sum of the reference counts on all the aliased
         * vnodes descends to one, we are on last close.
         */

        if (vcount(this) > 1 && (v_flag & VXLOCK) == 0)
        {
                return (0);
        }
#endif
        bdevsw[major(m_fstat.st_rdev)].d_close(m_fstat.st_rdev);

        return 0;
}
kern_return_t specfs_vnode_blk::read(uio *a_uio, int a_ioflag)
{
        if (a_uio->uio_rw != UIO_READ)
        {
                panic("uio not set to read");
        }

        if (a_uio->uio_segflg == UIO_USERSPACE && a_uio->uio_procp != proc::current())
        {
                panic("proc");
        }

        if (a_uio->uio_resid == 0)
        {
                return 0;
        }

        if (a_uio->uio_offset < 0)
        {
                return KERN_FAIL(EINVAL);
        }

        buffer_cache::io_descriptor *io_desc;
        kern_return_t result = KERN_SUCCESS;

        while ((a_uio->uio_offset < DEV_BSIZE) && a_uio->uio_resid)
        {
                size_t inode_offset = a_uio->uio_offset & ~(DEV_BMASK);
                size_t block_offset = a_uio->uio_offset & DEV_BMASK;

                if (result = buffer_cache::read(this, inode_offset, DEV_BSIZE, &io_desc); result == KERN_SUCCESS)
                {
                        char *data;
                        size_t block_residue;

                        io_desc->get_data(&data, block_offset, &block_residue);

                        size_t move_size = std::min(a_uio->uio_resid, block_residue);

                        move_size = std::min(move_size, static_cast<size_t>(DEV_BSIZE - a_uio->uio_offset));

                        result = uiomove(data, move_size, a_uio);

                        buffer_cache::release(io_desc);
                }
                else
                {
                        break;
                }
        }

        return result;
#if 0
        int error;
        KASSERT(error == KERN_SUCCESS);

        size_t bsize  = BLKDEV_IOSIZE;
        size_t bscale = bsize / DEV_BSIZE;
        size_t n      = 0;
        // Perform io in chunks the device supports

        do
        {
                size_t bn = (a_uio->uio_offset / DEV_BSIZE) & ~(bscale - 1);
                size_t on = a_uio->uio_offset % bsize;

                n = std::min((size_t)(bsize - on), (size_t)(a_uio->uio_resid));
                buf_t *bp;
                if (v_lastr + bscale == bn)
                {
                        size_t nextbn = bn + bscale;

                        error = breadn(this, bn, (int)bsize, &nextbn, (int *)&bsize, 1, NOCRED, &bp);
                }
                else
                {
                        error = bread(this, bn, (int)bsize, NOCRED, &bp);
                }

                v_lastr = bn;

                n = std::min(n, (size_t)(bsize - bp->b_resid));

                if (error)
                {
                        brelse(bp);
                        return (error);
                }

                error = uiomove((char *)bp->b_addr + on, n, a_uio);

                if (n + on == bsize)
                {
                        bp->b_flags |= B_AGE;
                }

                brelse(bp);

        } while (error == 0 && a_uio->uio_resid > 0 && n != 0);

        return error;
#endif
}

int specfs_vnode_blk::write(uio *a_uio, int a_ioflag)
{
        if (a_uio->uio_rw != UIO_WRITE)
        {
                panic("spec_write mode");
        }

        if (a_uio->uio_segflg == UIO_USERSPACE && a_uio->uio_procp != proc::current())
        {
                panic("spec_write proc");
        }

        if (a_uio->uio_resid == 0)
        {
                return 0;
        }

        if (a_uio->uio_offset < 0)
        {
                return -EINVAL;
        }

        panic("Not ready");
        return KERN_SUCCESS;
#if 0
        size_t bsize   = BLKDEV_IOSIZE;
        size_t blkmask = (bsize / DEV_BSIZE) - 1;
        size_t n       = 0;
        int error      = 0;
        do
        {
                size_t bn = (a_uio->uio_offset / DEV_BSIZE) & ~blkmask;
                size_t on = a_uio->uio_offset % bsize;

                n = std::min((size_t)(bsize - on), (size_t)(a_uio->uio_resid));

                buf_t *bp;

                if (n == bsize)
                {
                        bp = getblk(this, bn, bsize, 0, 0);
                }
                else
                {
                        error = bread(this, bn, bsize, NOCRED, &bp);
                }

                n = std::min(n, (size_t)(bsize - bp->b_resid));

                if (error)
                {
                        brelse(bp);
                        return (error);
                }

                error = uiomove(((char *)bp->b_addr) + on, n, a_uio);

                if (n + on == bsize)
                {
                        bp->b_flags |= B_AGE;
                        bawrite(bp);
                }
                else
                {
                        bdwrite(bp);
                }
        } while (error == 0 && a_uio->uio_resid > 0 && n != 0);

        return error;
#endif
}


int specfs_vnode_blk::ioctl(int a_cmd, void *a_data, int a_fflag, ucred *a_cred, pthread_t a_p)
{
#if 0
      /* (Damir) We don't support tape drives for now */
      if (a_command == 0 && (natural_t)ap->a_data == B_TAPE) {
        return !((bdevsw[major(dev)].d_flags & B_TAPE) > 0);
      }
#endif

        kern_return_t result = (*bdevsw[major(m_fstat.st_rdev)].d_ioctl)(m_fstat.st_rdev, a_cmd, (char *)a_data, a_fflag, a_p);
        return result;
}

int specfs_vnode_blk::fsync(ucred *a_cred, int a_waitfor, pthread_ta_p)
{
        panic("Not ready");
#if 0
loop:
        spl_t s = splbio();
        buf_t *nbp;
        for (buf_t *bp = v_dirtyblkhd.lh_first; bp; bp = nbp)
        {
                nbp = bp->b_vnbufs.le_next;

                if ((bp->b_flags & B_BUSY))
                {
                        continue;
                }

                if ((bp->b_flags & B_DELWRI) == 0)
                {
                        panic("spec_fsync: not dirty");
                }

                bremfree(bp);
                bp->b_flags |= B_BUSY;
                splx(s);
                bawrite(bp);
                goto loop;
        }

        if (a_waitfor == MNT_WAIT)
        {
                while (v_numoutput)
                {
                        v_flag |= VBWAIT;
                        sleep(&v_numoutput, PRIBIO + 1);
                }
                if (v_dirtyblkhd.lh_first)
                {
                        log_info("spec_fsync: dirty");
                        goto loop;
                }
        }

        splx(s);
#endif
        return 0;
}
int specfs_vnode_blk::stat(struct stat *a_statbuf)
{
        a_statbuf->st_dev     = m_device_num;
        a_statbuf->st_blksize = PAGE_SIZE;
        a_statbuf->st_dev     = m_device_num;
        a_statbuf->st_ino     = major(m_device_num);

        // a_statbuf->st_size   = static_cast<off_t>(i_din.i_size);
        // a_statbuf->st_blocks = i_din.;

        return 0;
}

kern_return_t specfs_vnode_blk::mknod(
    const char *a_name, size_t a_name_len, dev_t a_dev, mode_t a_mode, vnode::locked_ptr &a_vnode_out)
{
        return KERN_FAIL(ENOTDIR);
}

kern_return_t specfs_vnode_blk::symlink(const char *a_name, size_t a_name_len, uio *a_uio, vnode::locked_ptr &a_vnode_out)
{
        return KERN_FAIL(ENOTDIR);
}
#endif
kern_return_t specfs_vnode_blk::fs_strategy(buffer_cache::io_descriptor *a_io_desc)
{
        // if (a_block_cluster->m_dev == NODEV)
        //{
        //        a_block_cluster->m_dev = m_device_num;
        //}
        if (a_io_desc->m_lba_offset != m_phys_offset)
        {
                a_io_desc->m_lba_offset = m_phys_offset;
        }

        return (*bdevsw[major(m_fstat.st_rdev)].d_strategy2)(m_fstat.st_rdev, a_io_desc);
}

specfs_vnode_blk::specfs_vnode_blk(mount *mp)
    : specfs_vnode_base(mp)
    , m_flags(0)
    , m_phys_offset(0)
{
}

specfs_vnode_blk::~specfs_vnode_blk() {}
