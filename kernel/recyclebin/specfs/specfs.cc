#include "specfs_mount.hh"
#include "specfs_vnode_blk.hh"
#include "specfs_vnode_chr.hh"

#include <aux/init.h>
#include <emerixx/fdesc.hh>
#include <emerixx/file.h>
#include <emerixx/process.h>
#include <fcntl.h>
#include <fs/specfs.h>
#include <kernel/zalloc.h>
#include <mach/conf.h>
#include <mach/disklabel.h>

static kern_return_t spc_fread(file_t a_so_file, struct uio *a_uio);
static kern_return_t spc_fwrite(file_t a_so_file, struct uio *a_uio);
static kern_return_t spc_flseek(file_t, off_t a_offset, int a_whence);
static kern_return_t spc_fioctl(file_t a_so_file, ioctl_cmd_t a_cmd, void *a_data);
static kern_return_t spc_fstat(file_t, struct stat *a_statbuf);
static kern_return_t spc_fstatfs(file_t a_so_file, struct statfs *a_statfsbuf);
static kern_return_t spc_fclose(file_t a_so_file);
static kern_return_t spc_getdents(file_t, struct uio *) { return KERN_FAIL(ENOTDIR); }
static kern_return_t spc_fmmap(file_t a_so_file, void *);
static kern_return_t spc_fpoll(file_t a_so_file, struct pollfd **a_pfd);
static kern_return_t spc_fsplice(
    file_t a_so_file, off_t *a_off, file_t a_file_out, off_t *a_off_out, size_t a_len, unsigned a_flags)
{
        return KERN_FAIL(ENOSYS);
}

static file_ops_data s_spc_fops = {
    //
    .fread     = spc_fread,
    .fwrite    = spc_fwrite,
    .flseek    = spc_flseek,
    .fioctl    = spc_fioctl,
    .fclose    = spc_fclose,
    .fgetdents = spc_getdents,
    .fmmap     = spc_fmmap,
    .fpoll     = spc_fpoll,
    .fsplice   = spc_fsplice
    //
};

struct spc_file
{
        vnode::ptr m_vnode;
        integer_t m_offset;
};

ZONE_DEFINE(spc_file, s_vn_file_zone, FILEMAX, 0x10, ZONE_FIXED, 0x100);

#define FTOSPC(f) ((spc_file *)((f)->m_private))

// ********************************************************************************************************************************
kern_return_t spc_fread(file_t a_file, struct uio *a_uio)
// ********************************************************************************************************************************
{
        spc_file *f = FTOSPC(a_file);

        vnode::locked_ptr vp{f->m_vnode};

        a_uio->uio_offset    = f->m_offset;
        int count            = a_uio->uio_resid;
        kern_return_t retval = vp->uio_move(a_file, a_uio, (a_file->m_oflags & O_NONBLOCK) ? IO_NDELAY : 0);
        f->m_offset          = f->m_offset + (count - a_uio->uio_resid);
        return retval;
}

// ********************************************************************************************************************************
kern_return_t spc_fwrite(file_t a_file, struct uio *a_uio)
// ********************************************************************************************************************************
{
        spc_file *f = FTOSPC(a_file);

        vnode::locked_ptr vp{f->m_vnode};

        int ioflag = 0;

        if (a_file->m_oflags & O_NONBLOCK)
        {
                ioflag |= IO_NDELAY;
        }

        a_uio->uio_offset    = f->m_offset;
        int count            = a_uio->uio_resid;
        kern_return_t retval = vp->uio_move(a_file, a_uio, ioflag);
        f->m_offset += count - a_uio->uio_resid;

        return retval;
}

// ********************************************************************************************************************************
kern_return_t spc_flseek(file_t a_file, off_t a_offset, int a_whence)
// ********************************************************************************************************************************
{
        spc_file *f = FTOSPC(a_file);
        vnode::locked_ptr vp{f->m_vnode};

        switch (vp->m_fstat.st_mode & S_IFMT)
        {
        case S_IFBLK:
                break;

        default:
                return KERN_FAIL(ESPIPE);
        }

        switch (a_whence)
        {
        case SEEK_CUR:
                f->m_offset += a_offset;
                break;
        case SEEK_END:
                f->m_offset = a_offset + vp->m_fstat.st_size;
                break;
        case SEEK_SET:
                f->m_offset = a_offset;
                break;
        default:
                return KERN_FAIL(EINVAL);
        }

        return (kern_return_t)f->m_offset;
}

// ********************************************************************************************************************************
kern_return_t spc_fioctl(file_t a_file, ioctl_cmd_t a_cmd, void *a_data)
// ********************************************************************************************************************************
{
        spc_file *f = FTOSPC(a_file);
        vnode::locked_ptr vp{f->m_vnode};
        switch (vp->m_fstat.st_mode & S_IFMT)
        {

        case S_IFCHR:
        case S_IFBLK: {
                kern_return_t retval = vp->ioctl(a_file, a_cmd, a_data, a_file->m_oflags);
                if (KERN_STATUS_SUCCESS(retval) && a_cmd == TIOCSCTTY)
                {
                        // is set by the tty layer
                        // panic("picnic");
                        // p->process->p_pgrp->pg_session->s_ttyvp = vp.get_raw();
                        // vp->reference();
                }
                return retval;
        }
        default:
                return KERN_FAIL(ENOTTY);
        }

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t spc_fstat(file_t a_file, struct stat *a_statbuf)
// ********************************************************************************************************************************
{
        spc_file *f          = FTOSPC(a_file);
        kern_return_t retval = vnode::locked_ptr(f->m_vnode)->stat(a_statbuf);
        return retval;
}

// ********************************************************************************************************************************
kern_return_t spc_fstatfs(file_t a_file, struct statfs *a_statfsbuf)
// ********************************************************************************************************************************
{
        spc_file *f          = FTOSPC(a_file);
        kern_return_t retval = vnode::locked_ptr(f->m_vnode)->v_mount->statfs(a_statfsbuf);
        return retval;
}

// ********************************************************************************************************************************
kern_return_t spc_fclose(file_t a_file)
// ********************************************************************************************************************************
{
        spc_file *f = FTOSPC(a_file);
        f->m_vnode  = nullptr;
        s_vn_file_zone.free_delete(f);
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t spc_fmmap(file_t a_file, void *)
// ********************************************************************************************************************************
{
        panic("noop");
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t spc_fpoll(file_t a_file, struct pollfd **a_pfd)
// ********************************************************************************************************************************
{
        spc_file *f          = FTOSPC(a_file);
        kern_return_t retval = vnode::locked_ptr(f->m_vnode)->poll(a_file, a_pfd);
        return retval;
}

extern int nblkdev;
int specfs_bdevvp(dev_t dev, vnode::ptr &vpp)
{
        vpp = nullptr;

        if ((major(dev) < nblkdev) && (minor(dev) < 0x100))
        {
                vpp = specfs_mount::instance->m_block_devices[major(dev)].minors[minor(dev)];
        }

        return vpp.is_null() ? KERN_FAIL(ENODEV) : KERN_SUCCESS;
}

int specfs_is_mounted(vnode::ptr &a_vp)
{
        if (S_ISBLK(a_vp->m_fstat.st_mode) == false || a_vp->v_mount != specfs_mount::instance)
        {
                panic("Wrong vnode type/owner");
        }

        return (((specfs_vnode_blk *)a_vp.get_raw())->m_flags & SI_MOUNTEDON) != 0;
}

kern_return_t specfs_file_alloc(dev_t a_dev, mode_t a_mode, integer_t a_oflags)
{
        kern_return_t retval = KERN_SUCCESS;
        vnode::ptr vnp;

        if (S_ISBLK(a_mode))
        {
                retval = specfs_bdevvp(a_dev, vnp);
        }
        else if (S_ISCHR(a_mode))
        {
                panic("no");
        }

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        spc_file *spcf = s_vn_file_zone.alloc_new();

        if (spcf == nullptr)
        {
                return KERN_FAIL(ENOMEM);
        }

        spcf->m_vnode = vnp;

        file_t f;
        int fd = fd_context_fdalloc(uthread_self()->m_fdctx, a_oflags, &s_spc_fops, vnp.get_raw(), (natural_t)spcf, &f);

        if (vnp->islocked() == false)
        {
                spcf->m_vnode->lock();
                retval = spcf->m_vnode->open(f, a_oflags);
                spcf->m_vnode->unlock();
        }
        else
        {
                retval = spcf->m_vnode->open(f, a_oflags);
        }
        KASSERT(KERN_STATUS_SUCCESS(retval));

        retval = fd;

        return retval;
}

kern_return_t specfs_register_block_device(dev_t a_device, struct bdevsw *a_dev_ops)
{
        /* Register us in the bdevsw, should not be here */
        bdevsw[major(a_device)] = *a_dev_ops;

        vnode::locked_ptr device_vnode;

        emerixx::dirent_t dent = {};
        dent.d_type            = IFTODT(S_IFBLK);
        dent.d_ino             = a_device;

        vnode *vn;
        kern_return_t retval = specfs_mount::instance->vget(&dent, nullptr, &vn);

        if (retval == KERN_SUCCESS)
        {
                device_vnode                  = vn;
                device_vnode->m_dirent        = dent;
                device_vnode->m_fstat.st_rdev = a_device;
                device_vnode->m_fstat.st_mode = S_IFBLK;
                // We need to set the currect vnode size.
                disklabel di;
                retval = device_vnode->ioctl(nullptr, DIOCGDINFO, &di, 0);
                if (retval == KERN_SUCCESS)
                {
                        // Set object size in pages
                        device_vnode->m_size = round_page(di.d_nsectors * di.d_secsize);

                        // Fill in stat struct
                        device_vnode->m_fstat.st_dev     = a_device;
                        device_vnode->m_fstat.st_ino     = a_device;
                        device_vnode->m_fstat.st_mode    = S_IFBLK;
                        device_vnode->m_fstat.st_nlink   = 1;
                        device_vnode->m_fstat.st_uid     = 0;
                        device_vnode->m_fstat.st_gid     = 0;
                        device_vnode->m_fstat.st_rdev    = a_device;
                        device_vnode->m_fstat.st_size    = di.d_nsectors * di.d_secsize;
                        device_vnode->m_fstat.st_blksize = di.d_secsize;
                        device_vnode->m_fstat.st_blocks  = di.d_nsectors;
                        device_vnode->m_fstat.st_atim    = {0};
                        device_vnode->m_fstat.st_mtim    = {0};
                        device_vnode->m_fstat.st_ctim    = {0};

                        retval
                            = partition_scan(device_vnode.get_raw(), di.d_nsectors, &di.part[0], MAXPARTITIONS, &di.d_npartitions);

                        if (retval == KERN_SUCCESS)
                        {
                                // Write back the partition info to the driver. The driver should not have any knowledge of
                                // partations ???
                                retval = device_vnode->ioctl(nullptr, DIOCSDINFO, &di, 0);

                                if (retval == KERN_SUCCESS)
                                {
                                        // Create partition devices
                                        fstat_t tmp = device_vnode->m_fstat;

                                        for (int i = 0; i < di.d_npartitions; ++i)
                                        {
                                                device_vnode      = nullptr;
                                                dev_t part_device = makedev(major(a_device), i + 1);

                                                dent.d_ino = part_device;

                                                retval = specfs_mount::instance->vget(&dent, nullptr, &vn);
                                                if (retval != KERN_SUCCESS)
                                                {
                                                        break;
                                                }
                                                device_vnode = vn;

                                                device_vnode.to_raw<specfs_vnode_blk>()->m_phys_offset = di.part[i].p_offset;
                                                device_vnode->m_size = round_page(di.part[i].p_size);

                                                device_vnode->m_fstat = tmp;

                                                device_vnode->m_fstat.st_dev    = a_device;
                                                device_vnode->m_fstat.st_rdev   = part_device;
                                                device_vnode->m_fstat.st_ino    = part_device;
                                                device_vnode->m_fstat.st_size   = di.part[i].p_size * di.d_secsize;
                                                device_vnode->m_fstat.st_blocks = di.part[i].p_size;
                                        }
                                }
                        }
                }
        }
        return retval;
}

static int specfs_init()
{
        specfs_mount::initialize();

        return 0;
}

fs_initcall(specfs_init);
