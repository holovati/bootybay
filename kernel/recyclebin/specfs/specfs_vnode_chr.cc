#include "specfs_vnode_chr.hh"
#include <mach/conf.h>

#include <string.h>
#include <sys/stat.h>
#if 0
int specfs_vnode_chr::open(mode_t a_mode, ucred *a_cred, pthread_t a_p)
{
        // unlock();
        int error = (*cdevsw[m_fstat.st_rdev].d_open)(m_fstat.st_rdev, a_mode, S_IFCHR, a_p);
        // lock();
        return error;
}

int specfs_vnode_chr::close(int a_fflag, ucred *a_cred, pthread_t a_p)
{
        /*
         * Hack: a tty device that is a controlling terminal
         * has a reference from the session structure.
         * We cannot easily tell that a character device is
         * a controlling terminal, unless it is the closing
         * process' controlling terminal.  In that case,
         * if the reference count is 2 (this last descriptor
         * plus the session), release the reference from the session.
         */
#if DAMIR // Figure out a smarter way to do this
        if (vcount(this) == 2 && a_p && this == a_p->p_pgrp->pg_session->s_ttyvp)
        {
                vnode::unreference(this);
                a_p->p_pgrp->pg_session->s_ttyvp = NULL;
        }
        /*
         * If the vnode is locked, then we are in the midst
         * of forcably closing the device, otherwise we only
         * close on last reference.
         */
        if (vcount(this) > 1 && (this->v_flag & VXLOCK) == 0)
        {
                return (0);
        }
#endif
        cdevsw[major(m_fstat.st_rdev)].d_close(m_fstat.st_rdev);
        return 0;
}

int specfs_vnode_chr::read(uio *a_uio, int a_ioflag)
{
        if (a_uio->uio_rw != UIO_READ)
        {
                panic("uio not set to read");
        }

        if (a_uio->uio_segflg == UIO_USERSPACE && a_uio->uio_procp != proc::current())
        {
                panic("proc");
        }

        if (a_uio->uio_resid == 0)
        {
                return 0;
        }

        // unlock();
        int error = (*cdevsw[major(m_device_num)].d_read)(m_device_num, a_uio, a_ioflag);
        // lock();

        return error;
}

int specfs_vnode_chr::write(uio *a_uio, int a_ioflag)
{
        if (a_uio->uio_rw != UIO_WRITE)
        {
                panic("spec_write mode");
        }

        if (a_uio->uio_segflg == UIO_USERSPACE && a_uio->uio_procp != proc::current())
        {
                panic("spec_write proc");
        }

        // unlock();
        int error = (*cdevsw[major(m_device_num)].d_write)(m_device_num, a_uio, a_ioflag);
        // lock();
        return error;
}

int specfs_vnode_chr::ioctl(int a_cmd, void *a_data, int a_fflag, ucred *a_cred, pthread_t a_p)
{
        return (*cdevsw[major(m_fstat.st_rdev)].d_ioctl)(m_fstat.st_rdev, a_cmd, (char *)a_data, a_fflag, a_p);
}

int specfs_vnode_chr::fsync(ucred *a_cred, int a_waitfor, pthread_ta_p) { return 0; }

int specfs_vnode_chr::stat(struct stat *a_statbuf)
{
        a_statbuf->st_mode    = S_IFCHR;
        a_statbuf->st_dev     = m_device_num;
        a_statbuf->st_blksize = 1024;
        a_statbuf->st_size    = 0;
        return 0;
}



kern_return_t specfs_vnode_chr::mknod(
    const char *a_name, size_t a_name_len, dev_t a_dev, mode_t a_mode, vnode::locked_ptr &a_vnode_out)
{
        return KERN_FAIL(ENOTDIR);
}

kern_return_t specfs_vnode_chr::symlink(const char *a_name, size_t a_name_len, uio *a_uio, vnode::locked_ptr &a_vnode_out)
{
        return KERN_FAIL(ENOTDIR);
}
#endif

kern_return_t specfs_vnode_chr::fs_strategy(buffer_cache::io_descriptor *) { return KERN_FAIL(ENOTSUP); }

specfs_vnode_chr::specfs_vnode_chr(mount *mp)
    : specfs_vnode_base(mp)
{
}

specfs_vnode_chr::~specfs_vnode_chr() {}
