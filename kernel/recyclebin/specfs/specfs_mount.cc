#include "specfs_mount.hh"
#include "specfs_vnode_blk.hh"
#include "specfs_vnode_chr.hh"

#include <aux/init.h>
#include <kernel/vm/vm_kern.h>
#include <string.h>

namespace
{
// Storage for our vnode types
template <> vnode::storage<specfs_vnode_blk>::type vnode::storage<specfs_vnode_blk>::mem{};
} // namespace

specfs_mount *specfs_mount::instance;

vnode::ptr nullvn; // hack

specfs_mount::specfs_mount()
    : mount(0, nullvn, "specfs", 0)
{
}

specfs_mount::~specfs_mount() {}

int specfs_mount::start(int flags, pthread_t p) { return KERN_SUCCESS; }

int specfs_mount::unmount(int mntflags, pthread_t p) { return KERN_SUCCESS; }

int specfs_mount::root(vnode::ptr &vpp) { return KERN_SUCCESS; }

int specfs_mount::quotactl(int cmds, uid_t uid, char *arg, pthread_t p) { return KERN_SUCCESS; }

int specfs_mount::statfs(struct statfs *sbp) { return KERN_SUCCESS; }

int specfs_mount::sync(int waitfor) { return KERN_SUCCESS; }

inline void *operator new(size_t, specfs_vnode_blk *p) { return p; }
inline void *operator new(size_t, specfs_vnode_chr *p) { return p; }

int specfs_mount::vget(emerixx::dirent_t *a_dirent, vnode *a_parent, vnode **a_vnode_out)
{
        kern_return_t retval = KERN_SUCCESS;

        fstat_t fst = {};
        fst.st_ino  = a_dirent->d_ino;
        fst.st_rdev = a_dirent->d_ino;
        fst.st_mode = DTTOIF(a_dirent->d_type);

        if (S_ISBLK(fst.st_mode))
        {
                specfs_vnode_blk **blk_device = &(m_block_devices[major(a_dirent->d_ino)].minors[minor(a_dirent->d_ino)]);

                if (*blk_device == nullptr)
                {

                        retval       = new_vnode<specfs_vnode_blk>(blk_device, this);
                        *a_vnode_out = *blk_device;
                        (*a_vnode_out)->reference(); // Will be unrefed by specfs_unregister_block_device
                }
                else
                {
                        *a_vnode_out = *blk_device;
                }
        }
        else if (S_ISCHR(fst.st_mode))
        {
                panic("no");
        }
        else
        {
                retval = KERN_FAIL(EINVAL);
        }

        return retval;
}

void specfs_mount::vnode_reclaim(vnode *vp) { panic("Check this out doc"); }

extern int nblkdev;

void specfs_mount::initialize()
{
        vnode::storage<specfs_vnode_blk>::init();

        mount::lock_shared();

        instance = new specfs_mount;

        size_t devsw_size       = ((nblkdev * sizeof(specfs_mount::block_devices)));
        vm_address_t devsw_addr = 0;

        KASSERT(kmem_alloc_wired(kernel_map, &devsw_addr, round_page(devsw_size)) == KERN_SUCCESS);

        memset(reinterpret_cast<void *>(devsw_addr), 0, devsw_size);

        instance->m_block_devices = reinterpret_cast<specfs_mount::block_devices *>(devsw_addr);

        instance->mnt_stat.f_fsid.__val[0] = 0;
        instance->mnt_stat.f_fsid.__val[1] = MOUNT_SPECFS;

        // Remove us from the mountlist as we are not really mounted
        mount::mountlist.remove(instance);

        mount::unlock_shared();
}
