#pragma once

#include <vfs/vnode.h>

struct specfs_vnode_base : vnode
{
        kern_return_t fs_readdir(fs_readdir_cb_t a_cb, fs_readdir_ctx_t *a_ctx) override;

        kern_return_t fs_dir_initialize(fstat_t *a_parent_stat);

        kern_return_t fs_dir_reparent(fstat_t *a_parent_stat);

        kern_return_t fs_dir_empty();

        kern_return_t fs_readwrite_link(uio *a_uio) override;

        kern_return_t fs_readwrite_spec(file *a_file, uio *a_uio, int a_ioflag) override;

        kern_return_t fs_ioctl(file *a_file, int a_cmd, void *a_data, int a_fflag) override;

        kern_return_t fs_open(file *a_file, mode_t a_mode) override;

        kern_return_t fs_close(file *a_file) override;

        kern_return_t fs_poll(file *a_file, pollfd **a_pfd) override;

        kern_return_t fs_remove_dirent(emerixx::dirent_t *a_dirent) override;

        kern_return_t fs_insert_dirent(emerixx::dirent_t *a_dirent) override;

        kern_return_t fs_get_page(size_t a_offset, vm_prot_t a_prot, vm_page_t *a_vm_page_out) override;

        specfs_vnode_base(mount *mp);

        virtual ~specfs_vnode_base();
};