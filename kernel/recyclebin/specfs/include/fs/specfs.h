#pragma once
#include <vfs/vnode.h>

kern_return_t specfs_bdevvp(dev_t dev, vnode::ptr &vpp);

kern_return_t specfs_cdevvp(dev_t dev, vnode::ptr &vpp);

kern_return_t specfs_is_mounted(vnode::ptr &vpp);

kern_return_t specfs_file_alloc(dev_t a_dev, mode_t a_mode, integer_t a_oflags);

kern_return_t specfs_register_block_device(dev_t a_device, struct bdevsw *a_dev_ops);

kern_return_t specfs_register_character_device(dev_t a_device, size_t a_minor_count, struct cdevsw *a_dev_ops);