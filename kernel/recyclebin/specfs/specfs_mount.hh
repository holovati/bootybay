#pragma once

#include <vfs/mount.h>

struct specfs_vnode_blk;
struct specfs_vnode_chr;

struct specfs_mount final : mount
{
        specfs_mount();
        ~specfs_mount();

        int start(int flags, pthread_t p) override;

        int unmount(int mntflags, pthread_t p) override;

        int root(vnode::ptr &vpp) override;

        int quotactl(int cmds, uid_t uid, char *arg, pthread_t p) override;

        int statfs(struct statfs *sbp) override;

        int sync(int waitfor) override;

        kern_return_t block_free(vnode *a_vnode, vm_offset_t a_offset, size_t a_count) override { return KERN_FAIL(EINVAL); }

        int vget(emerixx::dirent_t *a_dirent, vnode *a_parent, vnode **a_vnode_out) override;

        void vnode_reclaim(vnode *vp) override;

        static void initialize();

        struct block_devices
        {
                specfs_vnode_blk *minors[0x100];
        } * m_block_devices;

        struct char_devices
        {
                specfs_vnode_chr *minors[0x100];
        } * m_char_devices;

        static specfs_mount *instance;
};