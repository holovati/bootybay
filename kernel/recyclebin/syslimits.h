#pragma once
#include <limits.h>

#define CHILD_MAX      40          /* max simultaneous processes */
#define LINK_MAX       32767       /* max file link count */
#define MAX_CANON      255         /* max bytes in term canon input line */
#define MAX_INPUT      255         /* max bytes in terminal input */
#define OPEN_MAX       64          /* max open files per process */
#define BC_BASE_MAX    99          /* max ibase/obase values in bc(1) */
#define BC_DIM_MAX     2048        /* max array elements in bc(1) */
#define BC_SCALE_MAX   99          /* max scale value in bc(1) */
#define BC_STRING_MAX  1000        /* max const string length in bc(1) */
#define EXPR_NEST_MAX  32          /* max expressions nested in expr(1n) */
#define RE_DUP_MAX     255         /* max RE's in interval notation */
#define MAXCOMLEN      128         /* max command name remembered */
#define MAXINTERP      128         /* max interpreter file name length */
#define MAXLOGNAME     16          /* max login name length */
#define NOGROUP        65535       /* marker for empty group set member */
#define MAXHOSTNAMELEN 256         /* max hostname size */
#define NCARGS         ARG_MAX     /* max bytes for an exec function */
#define NGROUPS        NGROUPS_MAX /* max number groups */
#define NOFILE         OPEN_MAX    /* max open files per process */
