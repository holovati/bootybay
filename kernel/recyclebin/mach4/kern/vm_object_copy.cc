
#include <kernel/vm/vm_object.h>
#include <kernel/zalloc.h>

ZONE_DEFINE(vm_object_copy, s_ocopy_zone, 0x1000, 0x80, ZONE_FIXED, 32);

void vm_object_copy::do_unreference()
{
        panic("nr");

        s_ocopy_zone.free_delete(this);
}

bool vm_object_copy::do_extend(size_t)
{
        panic("nr");
        return false;
}

vm_object_copy *vm_object_copy::create(vm_object *a_source_object)
{
        vm_object_copy *obj = s_ocopy_zone.alloc_new(a_source_object);
        return obj;
}

kern_return_t vm_object_copy::get_page(vm_offset_t a_offset, vm_prot_t a_prot, vm_page_t *a_vm_page_out)
{
        //
        return -ENOMEM;
}

vm_object_copy::vm_object_copy(vm_object *a_source)
    : vm_object(a_source->m_size)
{
}

vm_object_copy::~vm_object_copy() {}