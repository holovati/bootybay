#include <kernel/lock.h>
#include <kernel/sched_prim.h> /* definitions of wait/wakeup */
#include <kernel/vm/vm_fault.h>
#include <kernel/vm/vm_param.h>
#include <mach/machine/vm_param.h>

#include "vm_pageout.h"

#include <kernel/vm/vm_map.h>
#include <kernel/vm/vm_object.h>
#include <mach/thread.h>
#include <vfs/vfs.h>
// #include <kernel/vm/vm_pager.h>
#include <kernel/zalloc.h>

/* list of objects persisting */
// static vm_object::cached_list vm_object_cached_list;
/* size of cached list */
// static int vm_object_cached;
/* lock for object cache */
// static simple_lock_data_t vm_cache_lock;

/* list of allocated objects */
// static vm_object::object_list vm_object_list;
/* count of all objects */
static size_t vm_object_count;
/* lock for object list and count */
// static simple_lock_data_t vm_object_list_lock;

// zone_t vm_object_zone; /* vm backing store zone */
// using vm_object_zone_t = zone<vm_object, 1024, 1024>;
// static vm_object_zone_t vm_object_zone;
// static zone<vm_object>::zone_t vm_object_zone;

// vm_object_t kernel_object;

/*
 *	Virtual memory objects maintain the actual data
 *	associated with allocated virtual memory.  A given
 *	page of memory exists within exactly one object.
 *
 *	An object is only deallocated when all "references"
 *	are given up.  Only one "reference" to a given
 *	region of an object should be writeable.
 *
 *	Associated with each object is a list of all resident
 *	memory pages belonging to that object; this list is
 *	maintained by the "vm_page" module, and locked by the object's
 *	lock.
 *
 *	Each object also records a "pager" routine which is
 *	used to retrieve (and store) pages to the proper backing
 *	storage.  In addition, objects may be backed by other
 *	objects from which they were virtual-copied.
 *
 *	The only items within the object structure which are
 *	modified after time of creation are:
 *		reference count		locked by object's lock
 *		pager routine		locked by object's lock
 *
 */
// #define VM_OBJECT_HASH_COUNT 157

// struct vm_object_hash_entry
//{
//        typedef intrusive::tailq::link<vm_object_hash_entry> tailq_link;
//        tailq_link hash_links; /* hash chain links */
//        typedef tailq_link::head<&vm_object_hash_entry::hash_links> head;
//
//        vm_object_t object; /* object represented */
//};

// using vm_object_hash_entry_zone_t = zone<vm_object_hash_entry, 1024, 512>::type;

// static vm_object_hash_entry_zone_t s_vm_object_hash_entry_zone;

// typedef struct vm_object_hash_entry *vm_object_hash_entry_t;

// static int vm_cache_max = 0; /* can patch if necessary */
// static vm_object_hash_entry::head vm_object_hashtable[VM_OBJECT_HASH_COUNT];

static long object_collapses = 0;
static long object_bypasses  = 0;

// static void vm_object_deactivate_pages(vm_object_t object);

static void bootstrap_vm_object_new(size_t a_kernel_object_size);
/*
 *	vm_object_bootstrap:
 *
 *	Initialize the VM objects module.
 */
void vm_object_bootstrap(void)
{
        // vm_object_zone = zinit((vm_size_t)sizeof(struct vm_object),
        //                       round_page(512 * 1024),
        //                       round_page(12 * 1024),
        //                       0,
        //                       "objects");

        // vm_object_zone_t::init(&vm_object_zone, "vm object");
        // vm_object_hash_entry_zone_t::init(&s_vm_object_hash_entry_zone, "vm object hash entry");

        /*
         *	Initialize the "submap object".  Make it as large as the
         *	kernel object so that no limit is imposed on submap sizes.
         */

        // vm_object_cached_list.init();
        // vm_object_list.init();
        vm_object_count = 0;

        // simple_lock_init(&vm_cache_lock);
        // simple_lock_init(&vm_object_list_lock);

        // for (int i = 0; i < VM_OBJECT_HASH_COUNT; i++)
        //        TAILQ_INIT(&vm_object_hashtable[i]);

        /*
         *	Initialize the "kernel object"
         */
        // kernel_object = vm_object_zone.alloc_new();
        extern vm_offset_t kernel_virtual_start;
        extern vm_offset_t kernel_virtual_end;
        size_t kvmsz = kernel_virtual_end - kernel_virtual_start;
        // vm_object_initialize(kvmsz, kernel_object);
        // vm_submap_object = vm_object_zone.alloc_new();
        // vm_object_initialize(kvmsz, vm_submap_object);

        bootstrap_vm_object_new(kvmsz);
}

/*
 *	vm_object_allocate:
 *
 *	Returns a new object with the given size.
 */
vm_object_t vm_object_allocate(vm_size_t size)
{
        panic("NONO");
        // vm_object_t result = vm_object_zone.alloc_new();

        // result = (vm_object_t)zalloc(vm_object_zone);

        // vm_object_initialize(size, result);

        // return (result);
        return KERN_SUCCESS;
}
#if 0
void vm_object_initialize(vm_size_t size, vm_object_t object)
{
        // TAILQ_INIT(&object->memq);
        // object->memq.init();
        // simple_lock_init(&(object)->m_lock);
        // object->ref_count           = 1;
        // object->resident_page_count = 0;
        // object->size                = size;
        // object->flags               = VM_OBJ_INTERNAL; /* vm_allocate_with_pager will reset */
        // object->paging_in_progress = 0;
        // object->copy = NULL;

        /*
         *	Object starts out read-write, with no pager.
         */

        // object->pager         = NULL;
        // object->paging_offset = 0;
        // object->shadow        = NULL;
        // object->shadow_offset = (vm_offset_t)0;

        // simple_lock(&vm_object_list_lock);
        //  TAILQ_INSERT_TAIL(&vm_object_list, object, object_list);
        //  vm_object_list.insert_tail(object);
        vm_object_count++;
        cnt.v_nzfod += atop(size);
        // simple_unlock(&vm_object_list_lock);
}
static inline void vm_object_sleep(event_t event, vm_object_t object, boolean_t interruptible)
{
        // thread_sleep(event, &(object->m_lock), interruptible);
}

/*
 *	vm_object_reference:
 *
 *	Gets another reference to the given object.
 */
void vm_object::reference() {
  if (this == NULL) /* WTF? */ {
    log_warn("Called with nullptr as this");
    return;
  }

  lock();
  ref_count++;
  unlock();
}
#endif

/*
 *	vm_object_deallocate:
 *
 *	Release a reference to the specified object,
 *	gained either through a vm_object_allocate
 *	or a vm_object_reference call.  When all references
 *	are gone, storage associated with this object
 *	may be relinquished.
 *
 *	No object may be locked.
 */
void vm_object_terminate(vm_object_t);
void vm_object_remove(vm_pager_t);
void vm_object_cache_lock();
void vm_object_cache_unlock();
void vm_object_cache_trim(void);
void vm_object_deallocate(vm_object_t object)
{
        vm_object_t temp;
        panic("NONO");
#if 0
        while (object != NULL)
        {
                /*
                 *	The cache holds a reference (uncounted) to
                 *	the object; we must lock it before removing
                 *	the object.
                 */

                vm_object_cache_lock();

                /*
                 *	Lose the reference
                 */
                object->lock();

    if (--(object->ref_count) != 0) {
      /*
       *	If there are still references, then
       *	we are done.
       */
      object->unlock();
      vm_object_cache_unlock();
      return;
    }

                /*
                 *	See if this object can persist.  If so, enter
                 *	it in the cache, then deactivate all of its
                 *	pages.
                 */

                if (object->flags & VM_OBJ_CANPERSIST)
                {
                        TAILQ_INSERT_TAIL(&vm_object_cached_list, object, cached_list_link);
                        vm_object_cached++;
                        vm_object_cache_unlock();

                        vm_object_deactivate_pages(object);
                        object->unlock();
                        vm_object_cache_trim();
                        return;
                }

                /*
                 *	Make sure no one can look us up now.
                 */
                // vm_object_remove(object->pager);
                vm_object_cache_unlock();

                // temp = object->shadow;
                panic("VM_OBJECT");
                // vm_object_terminate(object);
                /* unlocks and deallocates object */
                object = temp;
        }
#endif
}
#if 0
/*
 *	vm_object_terminate actually destroys the specified object, freeing
 *	up all previously used resources.
 *
 *	The object must be locked.
 */

static boolean_t vm_object_page_clean(
    vm_object_t object, vm_offset_t, vm_offset_t, boolean_t, boolean_t);
void vm_object_terminate(vm_object_t object) {
  vm_page_t p;
  vm_object_t shadow_object;

  /*
   *	Detach the object from its shadow if we are the shadow's
   *	copy.
   */
  if ((shadow_object = object->shadow) != NULL) {
    shadow_object->lock();
    if (shadow_object->copy == object)
      shadow_object->copy = NULL;
#if 0
		else if (shadow_object->copy != NULL)
			panic("vm_object_terminate: copy/shadow inconsistency");
#endif
    shadow_object->unlock();
  }

  /*
   * Wait until the pageout daemon is through with the object.
   */
  while (object->paging_in_progress) {
    vm_object_sleep(object, object, FALSE);
    object->lock();
  }

  /*
   * If not an internal object clean all the pages, removing them
   * from paging queues as we go.
   *
   * XXX need to do something in the event of a cleaning error.
   */
  if ((object->flags & VM_OBJ_INTERNAL) == 0)
    vm_object_page_clean(object, 0, 0, TRUE, TRUE);

  /*
   * Now free the pages.
   * For internal objects, this also removes them from paging queues.
   */
  while ((p = object->memq.tqh_first) != NULL) {
    VM_PAGE_CHECK(p);
    vm_page_lock_queues();
    vm_page_free(p);
    cnt.v_pfree++;
    vm_page_unlock_queues();
  }
  object->unlock();

  /*
   * Let the pager know object is dead.
   */
  if (object->pager != NULL)
    vm_pager_deallocate(object->pager);

  simple_lock(&vm_object_list_lock);
  // TAILQ_REMOVE(&vm_object_list, object, object_list);
  vm_object_list.remove(object);
  vm_object_count--;
  simple_unlock(&vm_object_list_lock);

  /*
   * Free the space for the object.
   */
  // zfree(vm_object_zone, (vm_offset_t)object);
  if (!(object->flags & VM_OBJ_NEW_TYPE)) {
    vm_object_zone.free_delete(object);
  }
}

/*
 *	vm_object_page_clean
 *
 *	Clean all dirty pages in the specified range of object.
 *	If syncio is TRUE, page cleaning is done synchronously.
 *	If de_queue is TRUE, pages are removed from any paging queue
 *	they were on, otherwise they are left on whatever queue they
 *	were on before the cleaning operation began.
 *
 *	Odd semantics: if start == end, we clean everything.
 *
 *	The object must be locked.
 *
 *	Returns TRUE if all was well, FALSE if there was a pager error
 *	somewhere.  We attempt to clean (and dequeue) all pages regardless
 *	of where an error occurs.
 */
boolean_t vm_object_page_clean(vm_object_t object,
                               vm_offset_t start,
                               vm_offset_t end,
                               boolean_t syncio,
                               boolean_t de_queue) {
  vm_page_t p;
  int onqueue;
  boolean_t noerror = TRUE;

  if (object == NULL)
    return (TRUE);

  /*
   * If it is an internal object and there is no pager, attempt to
   * allocate one.  Note that vm_object_collapse may relocate one
   * from a collapsed object so we must recheck afterward.
   */
  if ((object->flags & VM_OBJ_INTERNAL) && object->pager == NULL) {
    object->collapse();
    if (object->pager == NULL) {
      vm_pager_t pager;

      object->unlock();
      pager = vm_pager_allocate(
          /*PG_DFLT*/ NULL, 0, object->size, VM_PROT_ALL, (vm_offset_t)0);
      if (pager)
        object->setpager(pager, 0, FALSE);
      object->lock();
    }
  }
  if (object->pager == NULL)
    return (FALSE);

again:
  /*
   * Wait until the pageout daemon is through with the object.
   */
  while (object->paging_in_progress) {
    vm_object_sleep(object, object, FALSE);
    object->lock();
  }
  /*
   * Loop through the object page list cleaning as necessary.
   */
  for (p = object->memq.tqh_first; p != NULL; p = p->listq.tqe_next) {
    if ((start == end || p->offset >= start && p->offset < end)
        && !(p->flags & PG_FICTITIOUS)) {
      if ((p->flags & PG_CLEAN) && pmap_is_modified(p->phys_addr))
        p->flags &= ~PG_CLEAN;
      /*
       * Remove the page from any paging queue.
       * This needs to be done if either we have been
       * explicitly asked to do so or it is about to
       * be cleaned (see comment below).
       */
      if (de_queue || !(p->flags & PG_CLEAN)) {
        vm_page_lock_queues();
        if (p->flags & PG_ACTIVE) {
          vm_page_remove_active(p);
          onqueue = 1;
        } else if (p->flags & PG_INACTIVE) {
          vm_page_remove_inactive(p);
          onqueue = -1;
        } else
          onqueue = 0;
        vm_page_unlock_queues();
      }
      /*
       * To ensure the state of the page doesn't change
       * during the clean operation we do two things.
       * First we set the busy bit and write-protect all
       * mappings to ensure that write accesses to the
       * page block (in vm_fault).  Second, we remove
       * the page from any paging queue to foil the
       * pageout daemon (vm_pageout_scan).
       */
      pmap_page_protect(p->phys_addr, VM_PROT_READ);
      if (!(p->flags & PG_CLEAN)) {
        p->flags |= PG_BUSY;
        object->paging_in_progress++;
        object->unlock();
        /*
         * XXX if put fails we mark the page as
         * clean to avoid an infinite loop.
         * Will loose changes to the page.
         */
        if (vm_pager_put_pages(object->pager, &p, 1, syncio)) {
          log_error("%s: pager_put error\n", "vm_object_page_clean");
          p->flags |= PG_CLEAN;
          noerror = FALSE;
        }
        object->lock();
        object->paging_in_progress--;
        if (!de_queue && onqueue) {
          vm_page_lock_queues();
          if (onqueue > 0)
            p->activate();
          else
            p->deactivate();
          vm_page_unlock_queues();
        }
        p->flags &= ~PG_BUSY;
        p->_wakeup();
        goto again;
      }
    }
  }
  return (noerror);
}
#endif
/*
 *	vm_object_deactivate_pages
 *
 *	Deactivate all pages in the specified object.  (Keep its pages
 *	in memory even though it is no longer referenced.)
 *
 *	The object must be locked.
 */
void vm_object_deactivate_pages(vm_object_t object)
{
        panic("NONO");
#if 0
        vm_page_t p, next;

        for (p = object->memq.tqh_first; p != NULL; p = next)
        {
                next = p->listq.tqe_next;
                vm_page_lock_queues();
                p->deactivate();
                vm_page_unlock_queues();
        }
#endif
}
#if 0
/*
 *	Trim the object cache to size.
 */
vm_object_t vm_object_lookup(vm_pager_t);
void vm_object_cache_trim()
{
        vm_object_t object;

        vm_object_cache_lock();
        while (vm_object_cached > vm_cache_max)
        {
                object = vm_object_cached_list.tqh_first;
                vm_object_cache_unlock();

                panic("VM_OBJECT");
                // if (object != vm_object_lookup(object->pager)) {
                //  panic("vm_object_deactivate: I'm sooo confused.");
                //}

                // vm_pager_cache(object, FALSE);

                vm_object_cache_lock();
        }
        vm_object_cache_unlock();
}


/*
 *	vm_object_pmap_copy:
 *
 *	Makes all physical pages in the specified
 *	object range copy-on-write.  No writeable
 *	references to these pages should remain.
 *
 *	The object must *not* be locked.
 */
void vm_object::opmap_copy(vm_offset_t start, vm_offset_t end) {
  vm_page_t p;

  if (this == NULL)
    return;

  this->lock();
  for (p = this->memq.tqh_first; p != NULL; p = p->listq.tqe_next) {
    if ((start <= p->offset) && (p->offset < end)) {
      pmap_page_protect(p->phys_addr, VM_PROT_READ);
      p->flags |= PG_COPYONWRITE;
    }
  }
  this->unlock();
}


/*
 *	vm_object_pmap_remove:
 *
 *	Removes all physical pages in the specified
 *	object range from all physical maps.
 *
 *	The object must *not* be locked.
 */
void vm_object::opmap_remove(vm_offset_t start, vm_offset_t end) {
  vm_page_t p;

  if (this == NULL)
    return;

  this->lock();
  for (p = this->memq.tqh_first; p != NULL; p = p->listq.tqe_next) {
    if ((start <= p->offset) && (p->offset < end)) {
      pmap_page_protect(p->phys_addr, VM_PROT_NONE);
    }
  }
  this->unlock();
}

/*
 *	vm_object_copy:
 *
 *	Create a new object which is a copy of an existing
 *	object, and mark all of the pages in the existing
 *	object 'copy-on-write'.  The new object has one reference.
 *	Returns the new object.
 *
 *	May defer the copy until later if the object is not backed
 *	up by a non-default pager.
 */
void vm_object::ocopy(vm_offset_t src_offset,
                      vm_size_t size,
                      vm_object_t *dst_object, /* OUT */
                      vm_offset_t *dst_offset, /* OUT */
                      boolean_t *src_needs_copy /* OUT */) {
  vm_object_t new_copy;
  vm_object_t old_copy;
  vm_offset_t new_start, new_end;

  vm_page_t p;

  if (this == NULL) {
    /*
     *	Nothing to copy
     */
    *dst_object     = NULL;
    *dst_offset     = 0;
    *src_needs_copy = FALSE;
    return;
  }

  /*
   *	If the object's pager is null_pager or the
   *	default pager, we don't have to make a copy
   *	of it.  Instead, we set the needs copy flag and
   *	make a shadow later.
   */

  this->lock();
  if (this->pager == NULL || (this->flags & VM_OBJ_INTERNAL)) {
    /*
     *	Make another reference to the object
     */
    this->ref_count++;

    /*
     *	Mark all of the pages copy-on-write.
     */
    for (p = this->memq.tqh_first; p; p = p->listq.tqe_next)
      if (src_offset <= p->offset && p->offset < src_offset + size)
        p->flags |= PG_COPYONWRITE;
    this->unlock();

    *dst_object = this;
    *dst_offset = src_offset;

    /*
     *	Must make a shadow when write is desired
     */
    *src_needs_copy = TRUE;
    return;
  }

  /*
   *	Try to collapse the object before copying it.
   */
  this->collapse();

  /*
   *	If the object has a pager, the pager wants to
   *	see all of the changes.  We need a copy-object
   *	for the changed pages.
   *
   *	If there is a copy-object, and it is empty,
   *	no changes have been made to the object since the
   *	copy-object was made.  We can use the same copy-
   *	object.
   */

Retry1:
  old_copy = this->copy;
  if (old_copy != NULL) {
    /*
     *	Try to get the locks (out of order)
     */
    if (!old_copy->lock_try()) {
      this->unlock();

      /* should spin a bit here... */
      this->lock();
      goto Retry1;
    }

    if (old_copy->resident_page_count == 0 && old_copy->pager == NULL) {
      /*
       *	Return another reference to
       *	the existing copy-object.
       */
      old_copy->ref_count++;
      old_copy->unlock();
      this->unlock();
      *dst_object     = old_copy;
      *dst_offset     = src_offset;
      *src_needs_copy = FALSE;
      return;
    }
    old_copy->unlock();
  }
  this->unlock();

  /*
   *	If the object has a pager, the pager wants
   *	to see all of the changes.  We must make
   *	a copy-object and put the changed pages there.
   *
   *	The copy-object is always made large enough to
   *	completely shadow the original object, since
   *	it may have several users who want to shadow
   *	the original object at different points.
   */

  new_copy = vm_object_allocate(this->size);

Retry2:
  this->lock();
  /*
   *	Copy object may have changed while we were unlocked
   */
  old_copy = this->copy;
  if (old_copy != NULL) {
    /*
     *	Try to get the locks (out of order)
     */
    if (!old_copy->lock_try()) {
      this->unlock();
      goto Retry2;
    }

    /*
     *	Consistency check
     */
    if (old_copy->shadow != this || old_copy->shadow_offset != (vm_offset_t)0)
      panic("vm_object_copy: copy/shadow inconsistency");

    /*
     *	Make the old copy-object shadow the new one.
     *	It will receive no more pages from the original
     *	object.
     */

    this->ref_count--; /* remove ref. from old_copy */
    old_copy->shadow = new_copy;
    new_copy->ref_count++; /* locking not needed - we
                              have the only pointer */
    old_copy->unlock();    /* done with old_copy */
  }

  new_start = (vm_offset_t)0;              /* always shadow original at 0 */
  new_end   = (vm_offset_t)new_copy->size; /* for the whole object */

  /*
   *	Point the new copy at the existing object.
   */

  new_copy->shadow        = this;
  new_copy->shadow_offset = new_start;
  this->ref_count++;
  this->copy = new_copy;

  /*
   *	Mark all the affected pages of the existing object
   *	copy-on-write.
   */
  for (p = this->memq.tqh_first; p != NULL; p = p->listq.tqe_next)
    if ((new_start <= p->offset) && (p->offset < new_end))
      p->flags |= PG_COPYONWRITE;

  this->unlock();

  *dst_object     = new_copy;
  *dst_offset     = src_offset - new_start;
  *src_needs_copy = FALSE;
}

/*
 *	vm_object_shadow:
 *
 *	Create a new object which is backed by the
 *	specified existing object range.  The source
 *	object reference is deallocated.
 *
 *	The new object and offset into that object
 *	are returned in the source parameters.
 */

vm_object_t vm_object::oshadow(vm_offset_t *offset, /* IN/OUT */
                               vm_size_t length) {
  vm_object_t result = nullptr;

  /*
   *	Allocate a new object with the given length
   */

  if ((result = vm_object_allocate(length)) == NULL)
    panic("vm_object_shadow: no object for shadowing");

  /*
   *	The new object shadows the source object, adding
   *	a reference to it.  Our caller changes his reference
   *	to point to the new object, removing a reference to
   *	the source object.  Net result: no change of reference
   *	count.
   */
  result->shadow = this;

  /*
   *	Store the offset into the source object,
   *	and fix up the offset into the new object.
   */

  result->shadow_offset = *offset;

  /*
   *	Return the new things
   */

  *offset = 0;
  return result;
}

/*
 *	Set the specified object's pager to the specified pager.
 */

void vm_object::setpager(vm_pager_t a_pager,
                         vm_offset_t a_off,
                         boolean_t read_only) {
  lock(); /* XXX ? */
  pager         = a_pager;
  paging_offset = a_off;
  unlock(); /* XXX ? */
}
#endif
/*
 *	vm_object_hash hashes the pager/id pair.
 */

#define vm_object_hash(pager) (((unsigned long)pager) % VM_OBJECT_HASH_COUNT)

/*
 *	vm_object_lookup looks in the object cache for an object with the
 *	specified pager and paging id.
 */
#if 0
vm_object_t vm_object_lookup(vm_pager_t pager)
{
        panic("NONO");
#if 0
        vm_object_hash_entry_t entry;
        vm_object_t object;

        vm_object_cache_lock();

        for (entry = vm_object_hashtable[vm_object_hash(pager)].tqh_first; entry != NULL; entry = entry->hash_links.tqe_next)
        {
                object = entry->object;
                panic("VM_OBJECT");

    if (object->pager == pager) {
      object->lock();
      if (object->ref_count == 0) {
        TAILQ_REMOVE(&vm_object_cached_list, object, cached_list_link);
        vm_object_cached--;
      }
      object->ref_count++;
      object->unlock();
      vm_object_cache_unlock();
      return (object);
    }

        }

        vm_object_cache_unlock();
#endif
        return (NULL);
}
#endif
/*
 *	vm_object_enter enters the specified object/pager/id into
 *	the hash table.
 */

void vm_object_enter(vm_object_t object, vm_pager_t pager)
{
        panic("NONO");
#if 0
        vm_object_hash_entry_t entry;
        vm_object_hash_entry::head *bucket;
        /*
         *	We don't cache null objects, and we can't cache
         *	objects with the null pager.
         */

        if (object == NULL)
                return;
        if (pager == NULL)
                return;

        bucket        = &vm_object_hashtable[vm_object_hash(pager)];
        entry         = s_vm_object_hash_entry_zone.alloc();
        entry->object = object;
        object->flags |= VM_OBJ_CANPERSIST;

        vm_object_cache_lock();
        TAILQ_INSERT_TAIL(bucket, entry, hash_links);
        vm_object_cache_unlock();
#endif
}

/*
 *	vm_object_remove:
 *
 *	Remove the pager from the hash table.
 *	Note:  This assumes that the object cache
 *	is locked.  XXX this should be fixed
 *	by reorganizing vm_object_deallocate.
 */
void vm_object_remove(vm_pager_t pager)
{
        panic("NONO");
#if 0
        vm_object_hash_entry::head *bucket;
        vm_object_hash_entry_t entry;
        vm_object_t object;

        bucket = &vm_object_hashtable[vm_object_hash(pager)];

        for (entry = bucket->tqh_first; entry != NULL; entry = entry->hash_links.tqe_next)
        {
                object = entry->object;
                panic("VM_OBJECT");

    if (object->pager == pager) {
      TAILQ_REMOVE(bucket, entry, hash_links);
      // free(entry /*, M_VMOBJHASH*/);
      s_vm_object_hash_entry_zone.free(entry);
      break;
    }
}
#endif
}
#if 0
/*
 *	vm_object_cache_clear removes all objects from the cache.
 *
 */
void vm_object_cache_clear()
{
        vm_object_t object;

        /*
         *	Remove each object in the cache by scanning down the
         *	list of cached objects.
         */
        vm_object_cache_lock();
        while ((object = vm_object_cached_list.tqh_first) != NULL)
        {
                vm_object_cache_unlock();

                /*
                 * Note: it is important that we use vm_object_lookup
                 * to gain a reference, and not vm_object_reference, because
                 * the logic for removing an object from the cache lies in
                 * lookup.
                 */
                panic("VM_OBJECT");
                // if (object != vm_object_lookup(object->pager)) {
                //  panic("vm_object_cache_clear: I'm sooo confused.");
                //}
                // vm_pager_cache(object, FALSE);

                vm_object_cache_lock();
        }
        vm_object_cache_unlock();
}

boolean_t vm_object_collapse_allowed = TRUE;
/*
 *	vm_object_collapse:
 *
 *	Collapse an object with the object backing it.
 *	Pages in the backing object are moved into the
 *	parent, and the backing object is deallocated.
 *
 *	Requires that the object be locked and the page
 *	queues be unlocked.
 *
 */
void vm_object::collapse() {
  vm_object_t backing_object;
  vm_offset_t backing_offset;
  vm_size_t size;
  vm_offset_t new_offset;

  if (!vm_object_collapse_allowed)
    return;

  while (TRUE) {
    /*
     *	Verify that the conditions are right for collapse:
     *
     *	The object exists and no pages in it are currently
     *	being paged out (or have ever been paged out).
     */
    if (this == NULL || this->paging_in_progress != 0 || this->pager != NULL)
      return;

    /*
     *		There is a backing object, and
     */

    if ((backing_object = this->shadow) == NULL)
      return;

    backing_object->lock();
    /*
     *	...
     *		The backing object is not read_only,
     *		and no pages in the backing object are
     *		currently being paged out.
     *		The backing object is internal.
     */

    if ((backing_object->flags & VM_OBJ_INTERNAL) == 0
        || backing_object->paging_in_progress != 0) {
      backing_object->unlock();
      return;
    }

    /*
     *	The backing object can't be a copy-object:
     *	the shadow_offset for the copy-object must stay
     *	as 0.  Furthermore (for the 'we have all the
     *	pages' case), if we bypass backing_object and
     *	just shadow the next object in the chain, old
     *	pages from that object would then have to be copied
     *	BOTH into the (former) backing_object and into the
     *	parent object.
     */
    if (backing_object->shadow != NULL
        && backing_object->shadow->copy != NULL) {
      backing_object->unlock();
      return;
    }

    /*
     *	We know that we can either collapse the backing
     *	object (if the parent is the only reference to
     *	it) or (perhaps) remove the parent's reference
     *	to it.
     */

    backing_offset = this->shadow_offset;
    size           = this->size;

    /*
     *	If there is exactly one reference to the backing
     *	object, we can collapse it into the parent.
     */

    if (backing_object->ref_count == 1) {
      /*
       *	We can collapse the backing object.
       *
       *	Move all in-memory pages from backing_object
       *	to the parent.  Pages that have been paged out
       *	will be overwritten by any of the parent's
       *	pages that shadow them.
       */
      vm_page_t p, pp;
      while ((p = backing_object->memq.tqh_first) != NULL) {
        new_offset = (p->offset - backing_offset);

        /*
         *	If the parent has a page here, or if
         *	this page falls outside the parent,
         *	dispose of it.
         *
         *	Otherwise, move it as planned.
         */

        if (p->offset < backing_offset || new_offset >= size) {
          vm_page_lock_queues();
          vm_page_free(p);
          vm_page_unlock_queues();
        } else {
          pp = vm_page_lookup(this, new_offset);
          if (pp != NULL && !(pp->flags & PG_FAKE)) {
            vm_page_lock_queues();
            vm_page_free(p);
            vm_page_unlock_queues();
          } else {
            if (pp) {
              /* may be someone waiting for it */
              pp->_wakeup();
              vm_page_lock_queues();
              vm_page_free(pp);
              vm_page_unlock_queues();
            }
            p->rename(this, new_offset);
          }
        }
      }

      /*
       *	Move the pager from backing_object to object.
       *
       *	XXX We're only using part of the paging space
       *	for keeps now... we ought to discard the
       *	unused portion.
       */

      if (backing_object->pager) {
        this->pager           = backing_object->pager;
        this->paging_offset   = backing_offset + backing_object->paging_offset;
        backing_object->pager = NULL;
      }

      /*
       *	Object now shadows whatever backing_object did.
       *	Note that the reference to backing_object->shadow
       *	moves from within backing_object to within object.
       */

      this->shadow = backing_object->shadow;
      this->shadow_offset += backing_object->shadow_offset;
      if (this->shadow != NULL && this->shadow->copy != NULL) {
        panic("vm_object_collapse: we collapsed a copy-object!");
      }
      /*
       *	Discard backing_object.
       *
       *	Since the backing object has no pages, no
       *	pager left, and no object references within it,
       *	all that is necessary is to dispose of it.
       */

      backing_object->unlock();

      simple_lock(&vm_object_list_lock);
      // TAILQ_REMOVE(&vm_object_list, backing_object, object_list);
      vm_object_list.remove(backing_object);
      vm_object_count--;
      simple_unlock(&vm_object_list_lock);

      /// zfree(vm_object_zone, (vm_offset_t)backing_object);
      vm_object_zone.free_delete(backing_object);
      object_collapses++;
    } else {
      /*
       *	If all of the pages in the backing object are
       *	shadowed by the parent object, the parent
       *	object no longer has to shadow the backing
       *	object; it can shadow the next one in the
       *	chain.
       *
       *	The backing object must not be paged out - we'd
       *	have to check all of the paged-out pages, as
       *	well.
       */

      if (backing_object->pager != NULL) {
        backing_object->unlock();
        return;
      }

      /*
       *	Should have a check for a 'small' number
       *	of pages here.
       */

      // for (p = backing_object->memq.tqh_first; p != NULL;
      //     p = p->listq.tqe_next) {
      for (vm_page_t p : (backing_object->memq)) {
        new_offset = (p->offset - backing_offset);

        /*
         *	If the parent has a page here, or if
         *	this page falls outside the parent,
         *	keep going.
         *
         *	Otherwise, the backing_object must be
         *	left in the chain.
         */
        vm_page_t pp;
        if (p->offset >= backing_offset && new_offset < size
            && ((pp = vm_page_lookup(this, new_offset)) == NULL
                || (pp->flags & PG_FAKE))) {
          /*
           *	Page still needed.
           *	Can't go any further.
           */
          backing_object->unlock();
          return;
        }
      }

      /*
       *	Make the parent shadow the next object
       *	in the chain.  Deallocating backing_object
       *	will not remove it, since its reference
       *	count is at least 2.
       */

      this->shadow = backing_object->shadow;
      if (this->shadow) {
        this->shadow->reference();
      }
      this->shadow_offset += backing_object->shadow_offset;

      /*
       *	Backing object might have had a copy pointer
       *	to us.  If it did, clear it.
       */
      if (backing_object->copy == this) {
        backing_object->copy = NULL;
      }

      /*	Drop the reference count on backing_object.
       *	Since its ref_count was at least 2, it
       *	will not vanish; so we don't need to call
       *	vm_object_deallocate.
       */
      backing_object->ref_count--;
      backing_object->unlock();

      object_bypasses++;
    }

    /*
     *	Try again with this object's new backing object.
     */
  }
}

/*
 *	vm_object_page_remove: [internal]
 *
 *	Removes all physical pages in the specified
 *	object range from the object's list of pages.
 *
 *	The object must be locked.
 */
void vm_object::page_remove(vm_offset_t start, vm_offset_t end) {
  vm_page_t p, next;

  if (this == NULL)
    return;

  for (p = this->memq.tqh_first; p != NULL; p = next) {
    next = p->listq.tqe_next;
    if ((start <= p->offset) && (p->offset < end)) {
      pmap_page_protect(p->phys_addr, VM_PROT_NONE);
      vm_page_lock_queues();
      vm_page_free(p);
      vm_page_unlock_queues();
    }
  }
}
#endif

#if 0
/*
 *	Routine:	vm_object_coalesce
 *	Function:	Coalesces two objects backing up adjoining
 *			regions of memory into a single object.
 *
 *	returns TRUE if objects were combined.
 *
 *	NOTE:	Only works at the moment if the second object is NULL -
 *		if it's not, which object do we lock first?
 *
 *	Parameters:
 *		prev_object	First object to coalesce
 *		prev_offset	Offset into prev_object
 *		next_object	Second object into coalesce
 *		next_offset	Offset into next_object
 *
 *		prev_size	Size of reference to prev_object
 *		next_size	Size of reference to next_object
 *
 *	Conditions:
 *	The object must *not* be locked.
 */
boolean_t vm_object::coalesce(
    vm_object_t next_object, vm_offset_t prev_offset, vm_offset_t next_offset, vm_size_t prev_size, vm_size_t next_size)
{
        if (next_object != NULL)
        {
                return (FALSE);
        }

        if (this == nullptr)
        {
                return (TRUE);
        }

        this->lock();

        /*
         *	Try to collapse the object first
         */
        panic("VM_OBJECT");
        // this->collapse();

        /*
         *	Can't coalesce if:
         *	. more than one reference
         *	. paged out
         *	. shadows another object
         *	. has a copy elsewhere
         *	(any of which mean that the pages not mapped to
         *	prev_entry may be in use anyway)
         */

        // if (this->ref_count > 1 || this->pager != NULL || this->shadow != NULL
        //    || this->copy != NULL) {
        //  this->unlock();
        //  return (FALSE);
        //}

        /*
         *	Remove any pages that may still be in the object from
         *	a previous deallocation.
         */

        panic("VM_OBJECT");
        // this->page_remove(prev_offset + prev_size,
        //                  prev_offset + prev_size + next_size);

        /*
         *	Extend the object if necessary.
         */
        vm_size_t newsize = prev_offset + prev_size + next_size;

        if (newsize > this->m_size)
        {
                this->m_size = newsize;
        }

        this->unlock();
        return (TRUE);
}
#endif
// void vm_object_cache_lock() { simple_lock(&vm_cache_lock); }
// void vm_object_cache_unlock() { simple_unlock(&vm_cache_lock); }

//////////////////////////////////////////////////////////////////////////////////////
#include "vm_object_zero.hh"

static vm_object_zero s_vm_object_zero;

vm_object *vm_object::kernel;

vm_object_shadow *vm_object::create_shadow(vm_object *a_source, vm_offset_t a_source_offset, size_t a_shadow_size)
{
        a_source->reference();
        return vm_object_shadow::create(a_source, a_source_offset, a_shadow_size);
}

// vm_object_copy *vm_object::create_copy()
//{
//    return vm_object_copy::create(this);
//}

kern_return_t vm_object::lock()
{
#if 0
start:
        // does not belong here
        //  if (v_tag == VT_NON) {
        //    panic("WTF");
        // return -ENOENT;
        //  }

        if (m_flag & VXLOCK)
        {
                m_flag |= VXWANT;
                sleep(this, PINOD, __PRETTY_FUNCTION__);
                goto start;
        }

        m_flag |= VXLOCK;
#endif
        lock_write(&m_lock);
        return 0;
}

kern_return_t vm_object::try_lock()
{
#if 0
        if (m_flag & VXLOCK)
        {
                return -EAGAIN;
        }

        m_flag |= VXLOCK;
#endif
        return lock_try_write(&m_lock) ? KERN_SUCCESS : KERN_FAIL(EAGAIN);
}

kern_return_t vm_object::unlock()
{
#if 0
        if ((m_flag & VXLOCK) == 0)
        {
                panic("Not locked");
        }

        m_flag &= ~VXLOCK;

        if (m_flag & VXWANT)
        {
                m_flag &= ~VXWANT;
                wakeup(this);
        }
#endif
        lock_write_done(&m_lock);
        return 0;
}

/*
 * vm_object reference.
 * Increment use count.
 */
void vm_object::reference()
{
        if (m_usecount < 0)
        {
                panic("vref used where vget required");
        }

        m_usecount++;
}

void vm_object::unreference()
{
        m_usecount--;

        KASSERT(m_usecount >= 0);

        if (m_usecount > 0)
        {
#if 0
                if (0 && m_usecount == 1)
                {

                        if (islocked())
                        {
                                panic("should not be");
                        }
                }
#endif
                return;
        }

        if (islocked())
        {
                panic("should not be");
        }

        vm_object::lock();
        do_unreference();
        // We are dead after this
}

kern_return_t vm_object::islocked() { return m_lock.want_write > 0; }

bool vm_object::extend(size_t a_size)
{
        KASSERT(a_size > m_size);
        // Do some pre processing here
        return do_extend(a_size - m_size);
}

void vm_object::map_protection_change(vm_prot_t a_prot)
{
        for (vm_page_t vmp = vm_page_map_first(&m_page_map); vmp != nullptr; vmp = vm_page_map_next(vmp))
        {
                pmap_page_protect(vmp->phys_addr, a_prot);
        }
}

vm_object *vm_object::create_unnamed(size_t a_size) { return vm_object::create_shadow(&s_vm_object_zero, 0, a_size); }

vm_object *vm_object::get_zero_object() { return &s_vm_object_zero; }

u8 const *vm_object::zero_page_bytes() { return reinterpret_cast<u8 const *>(s_vm_object_zero.m_zero_page_address); }

inline void *operator new(size_t, vm_object *a_ptr) { return a_ptr; }
void bootstrap_vm_object_new(size_t a_kernel_object_size)
{
        new (&s_vm_object_zero) vm_object_zero{0};
        vm_object::kernel = vm_object::create_shadow(&s_vm_object_zero, 0, a_kernel_object_size);
}

vm_object::vm_object(size_t a_size)
    : m_size(a_size)
    // , m_copy(nullptr)
    // , m_flag(0)
    , m_usecount(0)
//, m_resident_page_count(0)
{
        // m_resident_pages.init();
        vm_page_map_init(&m_page_map);
        // simple_lock(&vm_object_list_lock);
        //  vm_object_list.insert_tail(this);
        vm_object_count++;
        // cnt.v_nzfod += atop(m_size);
        //  simple_unlock(&vm_object_list_lock);
        lock_init(&m_lock, true);
}

vm_object::~vm_object()
{
        /// simple_lock(&vm_object_list_lock);
        // vm_object_list.remove(this);
        vm_object_count--;
        // simple_unlock(&vm_object_list_lock);
}