#pragma once

struct vm_object_zero final : vm_object
{
        kern_return_t get_page(vm_offset_t a_offset, vm_prot_t a_prot, vm_page_t *a_vm_page_out) override;

        bool collapse(vm_object *, vm_object **, vm_offset_t *) override { return false; }

        vm_object_zero(int);

        vm_object_zero();

        vm_page_t m_zero_page;

        vm_address_t m_zero_page_address;

      private:
        void do_unreference() override;
        bool do_extend(size_t a_size) override;
};