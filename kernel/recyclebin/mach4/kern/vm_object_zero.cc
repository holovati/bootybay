#include <kernel/vm/vm_object.h>

#include "vm_object_zero.hh"
#include <aux/init.h>
#include <kernel/vm/vm_kern.h>
#include <kernel/vm/vm_map.h>
#include <kernel/vm/vm_param.h>

void vm_object_zero::do_unreference() { panic("Should never happen"); }

bool vm_object_zero::do_extend(size_t)
{
        // We have "unlimited" amount of zero pages with COW flag set
        return true;
}

kern_return_t vm_object_zero::get_page(vm_offset_t, vm_prot_t, vm_page_t *a_vm_page_out)
{
        *a_vm_page_out = m_zero_page;
        return KERN_SUCCESS;
}

vm_object_zero::vm_object_zero(int)
    : vm_object(VM_MAX_ADDRESS)
{
        m_zero_page_address = 0;

        vm_page_alloc(&m_page_map, 0);
        m_zero_page = vm_page_map_lookup(&m_page_map, 0);
        m_zero_page->zero_fill();
        reference(); // Keep us alive;
}

vm_object_zero::vm_object_zero() {}

static kern_return_t init_object_zero()
{
        vm_address_t *zero_page_bytes = &static_cast<vm_object_zero *>(vm_object::get_zero_object())->m_zero_page_address;

        kern_return_t retval = vm_map_find(kernel_map, vm_object::get_zero_object(), 0, zero_page_bytes, PAGE_SIZE, true, nullptr);

        if (retval == KERN_SUCCESS)
        {
                vm_page_t zero_page;
                vm_object::get_zero_object()->get_page(0, VM_PROT_READ, &zero_page);

                pmap_enter(kernel_pmap, *zero_page_bytes, zero_page, VM_PROT_READ, TRUE);
        }

        return retval;
}

subsys_initcall(init_object_zero);