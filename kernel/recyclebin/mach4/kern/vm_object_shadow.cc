
#include <kernel/vm/pmap.h>
#include <kernel/vm/vm_object.h>
#include <kernel/zalloc.h>

ZONE_DEFINE(vm_object_shadow, s_vm_object_shadow_zone, 0x1000, 0x80, ZONE_FIXED, 32);

void vm_object_shadow::do_unreference()
{
        KASSERT(islocked());

        // Are we shadowing anyone, if so drop the refrence
        if (m_shadow)
        {
                m_shadow->unreference();
        }

        vm_page_map_clear(&m_page_map);

        s_vm_object_shadow_zone.free_delete(this);
}

bool vm_object_shadow::do_extend(size_t a_size)
{
        // Do we need to call extend on the object we are shadowing?
        size_t new_size = m_size + a_size;
        if ((m_shadow_offset + new_size) < m_shadow->m_size)
        {
                // All good.
                m_size = new_size;
                return true;
        }

        bool extended = m_shadow->extend(new_size);

        if (extended)
        {
                m_size = new_size;
        }

        return extended;
}

kern_return_t vm_object_shadow::get_page(vm_offset_t a_offset, vm_prot_t a_prot, vm_page_t *a_vm_page_out)
{
        // Check if we have the page at the given offset
        *a_vm_page_out = vm_page_map_lookup(&m_page_map, a_offset);

        // If we do, return it.
        if (*a_vm_page_out)
        {
                // We don't set COW on our own pages
                (*a_vm_page_out)->flags &= ~PG_COPYONWRITE;
                return KERN_SUCCESS;
        }

        // We don't have the requested page, so must have a object to forward the request to.
        KASSERT(m_shadow);

        m_shadow->lock();

        // We don't, request it from the object we are shadowing
        kern_return_t result = m_shadow->get_page(m_shadow_offset + a_offset, a_prot, a_vm_page_out);

        // If the shadowed object does'nt have the page, bail!
        if (result != KERN_SUCCESS)
        {
                m_shadow->unlock();
                return result;
        }

        if ((a_prot & VM_PROT_WRITE) == VM_PROT_NONE)
        {
                // Since the page we are returning does not belong to us, ensure COW is set
                (*a_vm_page_out)->flags |= PG_COPYONWRITE;
                m_shadow->unlock();
                return KERN_SUCCESS;
        }

        vm_page_t shadow_page = *a_vm_page_out;

        // Don't try to copy fictitious pages.
        if (shadow_page->flags & PG_FICTITIOUS)
        {
                (*a_vm_page_out)->flags |= PG_COPYONWRITE;
                m_shadow->unlock();
                return KERN_SUCCESS;
        }

        *a_vm_page_out = vm_page_alloc(&m_page_map, a_offset);

        KASSERT(*a_vm_page_out != shadow_page);

        (*a_vm_page_out)->flags &= ~(PG_BUSY | PG_FAKE);

        vm_page_copy(*a_vm_page_out, shadow_page, 0, PAGE_SIZE);

        m_shadow->unlock();
        return KERN_SUCCESS;
}

bool vm_object_shadow::collapse(vm_object *a_destination, vm_object **a_backing_object, vm_offset_t *a_backing_object_offset)
{

        // First lets check if the our shadow can be collapsed into us
        if (m_shadow != nullptr)
        {
                m_shadow->lock();
                vm_object *new_shadow = nullptr;
                bool collapsed        = m_shadow->collapse(this, &new_shadow, &m_shadow_offset);
                m_shadow->unlock();

                if (collapsed)
                {
                        // We now have all the pages we need from the object we were shadowing, loose it.
                        m_shadow->unreference();
                        m_shadow = new_shadow;
                }
        }

        // Did we get a destination object object for our pages? And can we be collapsed?
        bool can_collapse = a_destination != nullptr && m_usecount == 1 && a_destination->m_size == m_size;

        if (can_collapse)
        {
                // The pages we do not transfer to the destination object will be freed when er use_count drops to zero.
                // The caller has the responsibilty to call unreference on us.
                vm_page_t vmp_next = nullptr;
                for (vm_page_t vmp = vm_page_map_first(&m_page_map); vmp != nullptr; vmp = vmp_next)
                {
                        vmp_next = vm_page_map_next(vmp);
                        vm_page_rename(&m_page_map, vmp, &a_destination->m_page_map, vmp->offset);
                }

                if (a_backing_object)
                {
                        *a_backing_object          = m_shadow;
                        m_shadow                   = nullptr;
                        (*a_backing_object_offset) = m_shadow_offset;
                }
        }

        return can_collapse;
}

static int n_shadow_objs = 0;

vm_object_shadow::vm_object_shadow(vm_object *a_source, vm_offset_t a_source_offset, size_t a_source_size)
    : vm_object(a_source_size)
    , m_shadow(a_source)
    , m_shadow_offset(a_source_offset)
{
        n_shadow_objs++;
}

vm_object_shadow::~vm_object_shadow() { n_shadow_objs--; }

vm_object_shadow *vm_object_shadow::create(vm_object *a_source_object, vm_offset_t a_source_offset, size_t a_size)
{
        vm_object_shadow *obj = s_vm_object_shadow_zone.alloc_new(a_source_object, a_source_offset, a_size);
        obj->reference();
        return obj;
}