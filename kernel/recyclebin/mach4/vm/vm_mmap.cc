/*
 * Copyright (c) 1988 University of Utah.
 * Copyright (c) 1991 The Regents of the University of California.
 * All rights reserved.
 *
 * This code is derived from software contributed to Berkeley by
 * the Systems Programming Group of the University of Utah Computer
 * Science Department.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	$Id: mmap.c,v 1.1 94/10/19 17:37:17 bill Exp $
 */

/*
 * Mapped file (mmap) interface to VM
 */
extern "C" {
#include <mach/filedesc.h>
#include <mach/param.h>
#include <mach/specdev.h>
#include <mach/sys/mman.h>
#include <mach/thread.h>
#include <mach/vm/vm_user.h>
#include <mach/vm_param.h>
}
#include <mach/errno.h>
#include <mach/proc.h>
#include <mach/vm/vm_map.h>
#include <mach/vm/vm_mmap.h>
#include <mach/vm/vm_object.h>
#include <mach/vm/vm_pager.h>
#include <mach/vnode.h>
#include <mach/vnode_pager.h>
#if 0
extern vm_object_t vm_object_lookup(vm_pager_t);
/*
 * Internal version of mmap.
 * Currently used by mmap, exec, and sys5 shared memory.
 * Handle is either a vnode pointer or NULL for MAP_ANON.
 */
int vm_kmmap(vm_map_t map,
             vm_offset_t *addr,
             vm_size_t size,
             vm_prot_t prot,
             vm_prot_t maxprot,
             int flags,
             struct vnode *handle,
             vm_offset_t foff) {
  vm_pager_t pager;
  boolean_t fitit;
  vm_object_t object;
  struct vnode *vp = NULL;
  struct pagerops const *pops;
  int rv = KERN_SUCCESS;

  if (size == 0) {
    return 0;
  }

  if ((flags & MAP_FIXED) == 0) {
    fitit = TRUE;
    *addr = round_page(*addr);
  } else {
    fitit = FALSE;
    vm_deallocate(map, *addr, size);
  }

  /*
   * Lookup/allocate pager.  All except an unnamed anonymous lookup
   * gain a reference to ensure continued existance of the object.
   * (XXX the exception is to appease the pageout daemon)
   */
  if (flags & MAP_ANON) {
    UNUSED(pops);
    // type = PG_DFLT;
    pops = NULL;
  } else {
    vp = handle;
    KASSERT(vp != nullptr);
    if (vp->v_type == VCHR) {
      UNUSED(pops);
      // type   = PG_DEVICE;
      // handle = (struct vnode *)vp->v_un.vu_specinfo->si_rdev;
    } else {
      pops = vnode_pager_getops();
    }
  }

  pager = vm_pager_allocate(handle, pops, size, prot, foff);

  if (pager == NULL) {
    UNUSED(pops);
    // return (type == PG_DEVICE ? -EINVAL : -ENOMEM);
  }

  /*
   * Find object and release extra reference gained by lookup
   */

  object = vm_object_lookup(pager);

  vm_object_deallocate(object);

  /*
   * Anonymous memory.
   */
  if (flags & MAP_ANON) {
    rv = vm_allocate_with_pager(map, addr, size, fitit, pager, foff, TRUE);

    if (rv != KERN_SUCCESS) {
      if (handle == NULL) {
        vm_pager_deallocate(pager);
      } else {
        vm_object_deallocate(object);
      }
      goto out;
    }
    /*
     * Don't cache anonymous objects.
     * Loses the reference gained by vm_pager_allocate.
     * Note that object will be NULL when handle == NULL,
     * this is ok since vm_allocate_with_pager has made
     * sure that these objects are uncached.
     */
    vm_pager_cache(object, FALSE);
#ifdef DEBUG
    if (mmapdebug & MDB_MAPIT)
      printf("vm_mmap(%d): ANON *addr %x size %x pager %x\n",
             curproc->p_pid,
             *addr,
             size,
             pager);
#endif
  }
  /*
   * Must be a mapped file.
   * Distinguish between character special and regular files.
   */
  else if (vp->v_type == VCHR) {
    rv = vm_allocate_with_pager(map, addr, size, fitit, pager, foff, FALSE);
    /*
     * Uncache the object and lose the reference gained
     * by vm_pager_allocate().  If the call to
     * vm_allocate_with_pager() was sucessful, then we
     * gained an additional reference ensuring the object
     * will continue to exist.  If the call failed then
     * the deallocate call below will terminate the
     * object which is fine.
     */
    vm_pager_cache(object, FALSE);
    if (rv != KERN_SUCCESS)
      goto out;
  }
  /*
   * A regular file
   */
  else {
#ifdef DEBUG
    if (object == NULL)
      printf("vm_mmap: no object: vp %x, pager %x\n", vp, pager);
#endif
    /*
     * Map it directly.
     * Allows modifications to go out to the vnode.
     */
    if (flags & MAP_SHARED) {
      rv = vm_allocate_with_pager(map, addr, size, fitit, pager, foff, FALSE);
      if (rv != KERN_SUCCESS) {
        vm_object_deallocate(object);
        goto out;
      }
      /*
       * Don't cache the object.  This is the easiest way
       * of ensuring that data gets back to the filesystem
       * because vnode_pager_deallocate() will fsync the
       * vnode.  pager_cache() will lose the extra ref.
       */
      if (prot & VM_PROT_WRITE) {
        vm_pager_cache(object, FALSE);
      } else {
        vm_object_deallocate(object);
      }
    }
    /*
     * Copy-on-write of file.  Two flavors.
     * MAP_COPY is true COW, you essentially get a snapshot of
     * the region at the time of mapping.  MAP_PRIVATE means only
     * that your changes are not reflected back to the object.
     * Changes made by others will be seen.
     */
    else {
      vm_map_t tmap;
      vm_offset_t off;

      /* locate and allocate the target address space */
      rv = vm_map_find(map, NULL, (vm_offset_t)0, addr, size, fitit);
      if (rv != KERN_SUCCESS) {
        vm_object_deallocate(object);
        goto out;
      }
      tmap = vm_map_create(
          pmap_create(size), VM_MIN_ADDRESS, VM_MIN_ADDRESS + size, TRUE);
      off = VM_MIN_ADDRESS;
      rv  = vm_allocate_with_pager(tmap, &off, size, TRUE, pager, foff, FALSE);
      if (rv != KERN_SUCCESS) {
        vm_object_deallocate(object);
        vm_map_deallocate(tmap);
        goto out;
      }
      /*
       * (XXX)
       * MAP_PRIVATE implies that we see changes made by
       * others.  To ensure that we need to guarentee that
       * no copy object is created (otherwise original
       * pages would be pushed to the copy object and we
       * would never see changes made by others).  We
       * totally sleeze it right now by marking the object
       * internal temporarily.
       */
      if ((flags & /*MAP_COPY*/ MAP_PRIVATE) == 0) {
        object->flags |= VM_OBJ_INTERNAL;
      }

      rv = vm_map_copy(map, tmap, *addr, size, off, FALSE, FALSE);

      object->flags &= ~VM_OBJ_INTERNAL;
      /*
       * (XXX)
       * My oh my, this only gets worse...
       * Force creation of a shadow object so that
       * vm_map_fork will do the right thing.
       */
      if ((flags & MAP_PRIVATE /*MAP_COPY*/) == 0) {
        vm_map_t tmap;
        vm_map_entry_t tentry;
        vm_object_t tobject;
        vm_offset_t toffset;
        vm_prot_t tprot;
        boolean_t twired, tsu;

        tmap = map;
        vm_map_lookup(&tmap,
                      *addr,
                      VM_PROT_WRITE,
                      &tentry,
                      &tobject,
                      &toffset,
                      &tprot,
                      &twired,
                      &tsu);
        vm_map_lookup_done(tmap, tentry);
      }
      /*
       * (XXX)
       * Map copy code cannot detect sharing unless a
       * sharing map is involved.  So we cheat and write
       * protect everything ourselves.
       */
      object->opmap_copy(foff, foff + size);
      vm_object_deallocate(object);
      vm_map_deallocate(tmap);
      if (rv != KERN_SUCCESS) {
        goto out;
      }
    }
#ifdef DEBUG
    if (mmapdebug & MDB_MAPIT)
      printf("vm_mmap(%d): FILE *addr %x size %x pager %x\n",
             curproc->p_pid,
             *addr,
             size,
             pager);
#endif
  }
  /*
   * Correct protection (default is VM_PROT_ALL).
   * If maxprot is different than prot, we must set both explicitly.
   */
  rv = KERN_SUCCESS;

  if (maxprot != VM_PROT_ALL) {
    rv = vm_map_protect(map, *addr, *addr + size, maxprot, TRUE);
  }

  if (rv == KERN_SUCCESS && prot != maxprot) {
    rv = vm_map_protect(
        map, *addr, *addr + size, (prot & ~VM_PROT_EXECUTE), FALSE);
  }

  if (rv != KERN_SUCCESS) {
    vm_deallocate(map, *addr, size);
    goto out;
  }
  /*
   * Shared memory is also shared with children.
   */
  if (flags & MAP_SHARED) {
    rv = vm_map_inherit(map, *addr, *addr + size, VM_INHERIT_SHARE);
    if (rv != KERN_SUCCESS) {
      vm_deallocate(map, *addr, size);
      goto out;
    }
  }
out:
#ifdef DEBUG
  if (mmapdebug & MDB_MAPIT)
    printf("vm_mmap: rv %d\n", rv);
#endif
  switch (rv) {
    case KERN_SUCCESS:
      return (0);
    case KERN_INVALID_ADDRESS:
    case KERN_NO_SPACE:
      return -ENOMEM;
    case KERN_PROTECTION_FAILURE:
      return -EACCES;
    default:
      return -EINVAL;
  }
}
#endif