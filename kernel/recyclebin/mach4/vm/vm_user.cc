/*
 * Mach Operating System
 * Copyright (c) 1991,1990,1989,1988 Carnegie Mellon University
 * All Rights Reserved.
 *
 * Permission to use, copy, modify and distribute this software and its
 * documentation is hereby granted, provided that both the copyright
 * notice and this permission notice appear in all copies of the
 * software, derivative works or modified versions, and any portions
 * thereof, and that both notices appear in supporting documentation.
 *
 * CARNEGIE MELLON ALLOWS FREE USE OF THIS SOFTWARE IN ITS "AS IS"
 * CONDITION.  CARNEGIE MELLON DISCLAIMS ANY LIABILITY OF ANY KIND FOR
 * ANY DAMAGES WHATSOEVER RESULTING FROM THE USE OF THIS SOFTWARE.
 *
 * Carnegie Mellon requests users of this software to return to
 *
 *  Software Distribution Coordinator  or  Software.Distribution@CS.CMU.EDU
 *  School of Computer Science
 *  Carnegie Mellon University
 *  Pittsburgh PA 15213-3890
 *
 * any improvements or extensions that they make and grant Carnegie Mellon
 * the rights to redistribute these changes.
 */
/*
 *	File:	vm/vm_user.c
 *	Author:	Avadis Tevanian, Jr., Michael Wayne Young
 *
 *	User-exported virtual memory functions.
 */
#include <kernel/vm/vm_attributes.h>
#include <kernel/vm/vm_fault.h>
#include <kernel/vm/vm_param.h>
#include <kernel/vm/vm_statistics.h>
#include <mach/task.h>
#include <sys/mman.h>

#include <kernel/vm/vm_map.h>
#include <kernel/vm/vm_object.h>
#include <kernel/vm/vm_user.h>

vm_statistics_data_t vm_stat;

/*
 *	vm_allocate allocates "zero fill" memory in the specfied
 *	map.
 */
kern_return_t vm_allocate(vm_map_t map, vm_offset_t *addr, vm_size_t size, boolean_t anywhere)

{
        if (map == NULL)
                return (KERN_INVALID_ARGUMENT);
        if (size == 0)
        {
                *addr = 0;
                return (KERN_SUCCESS);
        }

        if (anywhere)
                *addr = map->min_offset();
        else
                *addr = trunc_page(*addr);
        size = round_page(size);

        kern_return_t result = vm_map_find(map, NULL, (vm_offset_t)0, addr, size, anywhere, nullptr);

        return (result);
}
#if 0
extern vm_object_t vm_object_lookup(vm_pager_t);
extern void vm_object_enter(vm_object_t, vm_pager_t);
/*
 * Similar to vm_allocate but assigns an explicit pager.
 */
int vm_allocate_with_pager(
    vm_map_t map, vm_offset_t *addr, vm_size_t size, boolean_t anywhere, vm_pager_t pager, vm_offset_t poffset, boolean_t internal)
{
        panic("NONO");
        vm_object_t object;
        int result;

        if (map == NULL)
        {
                return (KERN_INVALID_ARGUMENT);
        }

        *addr = trunc_page(*addr);
        size  = round_page(size);

        /*
         *	Lookup the pager/paging-space in the object cache.
         *	If it's not there, then create a new object and cache
         *	it.
         */
        object = vm_object_lookup(pager);

        cnt.v_lookups++;

        if (object == NULL)
        {
                // object = vm_object_allocate(size);
                /*
                 * From Mike Hibler: "unnamed anonymous objects should never
                 * be on the hash list ... For now you can just change
                 * vm_allocate_with_pager to not do vm_object_enter if this
                 * is an internal object ..."
                 */
                if (!internal)
                {
                        vm_object_enter(object, pager);
                }
        }
        else
        {
                cnt.v_hits++;
        }

        if (internal)
        {
                // object->flags |= VM_OBJ_INTERNAL;
        }
        else
        {
                // object->flags &= ~VM_OBJ_INTERNAL;
                cnt.v_nzfod -= atop(size);
        }

        result = vm_map_find(map, object, poffset, addr, size, anywhere, nullptr);

        if (result != KERN_SUCCESS)
        {
                panic("old shit below");
                // vm_object_deallocate(object);
        }
        else if (pager != NULL)
        {
                panic("VM_OBJECT");
                // object->setpager(pager, (vm_offset_t)0, TRUE);
        }

        return (result);
}
#endif
/*
 *	vm_deallocate deallocates the specified range of addresses in the
 *	specified address map.
 */
kern_return_t vm_deallocate(vm_map_t map, vm_offset_t start, vm_size_t size)
{
        if (map == VM_MAP_NULL)
        {
                return (KERN_INVALID_ARGUMENT);
        }

        if (size == (vm_offset_t)0)
        {
                return (KERN_SUCCESS);
        }
        return (vm_map_remove(map, trunc_page(start), round_page(start + size)));
}
#if 0
/*
 *	vm_inherit sets the inheritence of the specified range in the
 *	specified map.
 */
static int vm_inherit(vm_map_t map, vm_offset_t start, vm_size_t size, vm_inherit_t new_inheritance)
{
        if (map == NULL)
                return (KERN_INVALID_ARGUMENT);

        return (vm_map_inherit(map, trunc_page(start), round_page(start + size), new_inheritance));
}
#endif
/*
 *	vm_protect sets the protection of the specified range in the
 *	specified map.
 */

kern_return_t vm_protect(vm_map_t map, vm_offset_t start, vm_size_t size, boolean_t set_maximum, vm_prot_t new_protection)
{
        if (map == NULL)
                return (KERN_INVALID_ARGUMENT);

        return (vm_map_protect(map, trunc_page(start), round_page(start + size), new_protection, set_maximum));
}
#if 0
static kern_return_t _vm_statistics(vm_map_t map, vm_statistics_data_t *stat)
{
        if (map == VM_MAP_NULL)
        {
                return (KERN_INVALID_ARGUMENT);
        }

        *stat = vm_stat;

        stat->pagesize       = PAGE_SIZE;
        stat->free_count     = cnt.v_free_count;
        stat->active_count   = cnt.v_active_count;
        stat->inactive_count = cnt.v_inactive_count;
        stat->wire_count     = cnt.v_wire_count;

        return (KERN_SUCCESS);
}
#endif