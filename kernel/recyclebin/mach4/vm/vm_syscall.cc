extern "C" {

#include <kernel/vm/vm_fault.h>
#include <kernel/vm/vm_mmap.h>
#include <kernel/vm/vm_param.h>
#include <kernel/vm/vm_user.h>
#include <mach/filedesc.h>
#include <mach/task.h>
#include <mach/thread.h>
#include <sys/mman.h>
}
#include <emerixx/file.h>

#include <kernel/vm/vm_map.h>
#include <kernel/vm/vm_object.h>
#include <vfs/vnode.h>

vm_address_t vm_mmap(pthread_t a_procp, vm_address_t a_addr, size_t a_len, int a_prot, int a_flags, int a_fd, off_t a_pgoff)
{
        panic("CALLED");
        struct filedesc *fdp = a_procp->p_fd;
        struct file *fp;
        vm_offset_t addr;
        vm_offset_t pos;
        vm_size_t size;
        vm_prot_t prot, maxprot;

        int flags, error;

        vm_map_t map = a_procp->p_thread->task->map;
        vnode *vnp;
        vnode::ptr vp;

        prot  = a_prot & VM_PROT_ALL;
        flags = a_flags;
        pos   = a_pgoff;

#ifdef DEBUG
        if (mmapdebug & MDB_FOLLOW)
                printf("mmap(%d): addr %x len %x pro %x flg %x fd %d pos %x\n",
                       p->p_pid,
                       SCARG(uap, addr),
                       SCARG(uap, len),
                       prot,
                       flags,
                       SCARG(uap, fd),
                       pos);
#endif
        /*
         * Address (if FIXED) must be page aligned.
         * Size is implicitly rounded to a page boundary.
         *
         * XXX most (all?) vendors require that the file offset be
         * page aligned as well.  However, we already have applications
         * (e.g. nlist) that rely on unrestricted alignment.  Since we
         * support it, let it happen.
         */
        addr = a_addr;
        if (((flags & MAP_FIXED) && (addr & PAGE_MASK)) ||
#if 0
	    ((flags & MAP_ANON) == 0 && (pos & PAGE_MASK)) ||
#endif
            (ssize_t)a_len < 0 || ((flags & MAP_ANON) && a_fd != -1))
        {
                return -EINVAL;
        }

        size = (vm_size_t)round_page(a_len);
        /*
         * Check for illegal addresses.  Watch out for address wrap...
         * Note that VM_*_ADDRESS are not constants due to casts (argh).
         */
        if (flags & MAP_FIXED)
        {
                if (VM_MAX_ADDRESS > 0 && addr + size >= VM_MAX_ADDRESS)
                {
                        return -EINVAL;
                }

                if (VM_MIN_ADDRESS > 0 && addr < VM_MIN_ADDRESS)
                {
                        return -EINVAL;
                }

                if (addr > addr + size)
                {
                        return -EINVAL;
                }
        }

        /*
         * XXX for non-fixed mappings where no hint is provided or
         * the hint would fall in the potential heap space,
         * place it after the end of the largest possible heap.
         *
         * There should really be a pmap call to determine a reasonable
         * location.
         */
        else if (!addr && (addr < round_page(a_procp->vm_daddr + MAXDSIZ)))
        {
                addr = round_page(a_procp->vm_daddr + MAXDSIZ);
        }

        if (flags & MAP_ANON)
        {
                /*
                 * Mapping blank space is trivial.
                 */
                vnp     = NULL;
                maxprot = VM_PROT_ALL;
                pos     = 0;
        }
        else
        {
                /*
                 * Mapping file, get fp for validation.
                 * Obtain vnode and make sure it is of appropriate type.
                 */
                if (((unsigned)a_fd) >= fdp->fd_nfiles || (fp = fdp->fd_ofiles[a_fd]) == NULL)
                {
                        return -EBADF;
                }
                /*
                    if (fp->f_type != DTYPE_VNODE) {
                      return -EINVAL;
                    }

                    vp = (struct vnode *)fp->f_data;
                */
                vp = vnode::get_from_file(fp).get_raw();

                if (vp.is_null())
                {
                        return -EINVAL;
                }

                if (vp->v_type != VREG && vp->v_type != VCHR)
                {
                        return -EINVAL;
                }
                /*
                 * XXX hack to handle use of /dev/zero to map anon
                 * memory (ala SunOS).
                 */
                if (vp->v_type == VCHR /*&& iszerodev(vp->v_rdev)*/)
                {
                        vnp     = NULL;
                        maxprot = VM_PROT_ALL;
                        flags |= MAP_ANON;
                }
                else
                {
                        /*
                         * Ensure that file and memory protections are
                         * compatible.  Note that we only worry about
                         * writability if mapping is shared; in this case,
                         * current and max prot are dictated by the open file.
                         * XXX use the vnode instead?  Problem is: what
                         * credentials do we use for determination?
                         * What if proc does a setuid?
                         */
                        maxprot = VM_PROT_EXECUTE; /* ??? */
                        if (fp->f_flag & FREAD)
                        {
                                maxprot |= VM_PROT_READ;
                        }
                        else if (prot & PROT_READ)
                        {
                                return -EACCES;
                        }
                        if (flags & MAP_SHARED)
                        {
                                if (fp->f_flag & FWRITE)
                                {
                                        maxprot |= VM_PROT_WRITE;
                                }
                                else if (prot & PROT_WRITE)
                                {
                                        return -EACCES;
                                }
                        }
                        else
                        {
                                maxprot |= VM_PROT_WRITE;
                        }
                        vnp = vp.get_raw();
                }
        }

        // error = vm_kmmap(map, &addr, size, prot, maxprot, flags, vnp, pos);

        if (error)
        {
                return error;
        }

        return addr;
}

int vm_mprotect(pthread_t a_procp, vm_address_t a_addr, size_t a_len, unsigned long a_prot)
{
        vm_offset_t addr;
        vm_size_t size;
        vm_prot_t prot;

#ifdef DEBUG
        if (mmapdebug & MDB_FOLLOW)
                printf("mprotect(%d): addr %x len %x prot %d\n", p->p_pid, SCARG(uap, addr), SCARG(uap, len), SCARG(uap, prot));
#endif

        addr = a_addr;
        if ((addr & PAGE_MASK) || a_len < 0)
        {
                return -EINVAL;
        }

        size = (vm_size_t)a_len;
        prot = a_prot & VM_PROT_ALL;

        switch (vm_map_protect(a_procp->p_thread->task->map, addr, addr + size, prot, FALSE))
        {
        case KERN_SUCCESS:
                return (0);
        case KERN_PROTECTION_FAILURE:
                return -EACCES;
        }
        return -EINVAL;
}

int vm_munmap(pthread_t a_procp, vm_address_t a_addr, size_t a_len)
{
        vm_offset_t addr;
        vm_size_t size;
        vm_map_t map;

#ifdef DEBUG
        if (mmapdebug & MDB_FOLLOW)
                printf("munmap(%d): addr %x len %x\n", p->p_pid, SCARG(uap, addr), SCARG(uap, len));
#endif

        addr = (vm_offset_t)a_addr;

        if ((addr & PAGE_MASK) || a_len < 0)
        {
                return -EINVAL;
        }

        size = (vm_size_t)round_page(a_len);
        if (size == 0)
        {
                return (0);
        }

        /*
         * Check for illegal addresses.  Watch out for address wrap...
         * Note that VM_*_ADDRESS are not constants due to casts (argh).
         */
        if (VM_MAX_ADDRESS > 0 && addr + size >= VM_MAX_ADDRESS)
        {
                return -EINVAL;
        }

        if (VM_MIN_ADDRESS > 0 && addr < VM_MIN_ADDRESS)
        {
                return -EINVAL;
        }

        if (addr > addr + size)
                return -EINVAL;

        map = a_procp->p_thread->task->map;
        /*
         * Make sure entire range is allocated.
         * XXX this seemed overly restrictive, so we relaxed it.
         */
#if 0
	if (!vm_map_check_protection(map, addr, addr + size, VM_PROT_NONE))
		return -EINVAL;
#endif
        /* returns nothing but KERN_SUCCESS anyway */
        vm_map_remove(map, addr, addr + size);

        return (0);
}