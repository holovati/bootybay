extern "C" {
#include <mach/ucred.h>
#include <mach/vm/pmap.h>
#include <mach/vm_param.h>
}
#include <mach/errno.h>
#include <mach/init.h>
#include <mach/mount.h>
#include <mach/proc.h>
#include <mach/vm/vm_object.h>
#include <mach/vm/vm_pager.h>
#include <mach/vnode.h>
#include <mach/vnode_pager.h>
#include <mach/zalloc.h>
#include <strings.h>

using vm_pager_zone_t    = zone<vm_pager, 32, 1>::type;
using vnode_pager_zone_t = zone<vn_pager, 32, 1>::type;

static vm_pager_zone_t s_vm_pager_zone;
static vnode_pager_zone_t s_vnode_pager_zone;

static struct pagerlst vnode_pager_list; /* list of managed vnodes */

static vm_pager_t vnode_pager_alloc(void *, vm_size_t, vm_prot_t, vm_offset_t);
static void vnode_pager_cluster(vm_pager_t, vm_offset_t, vm_offset_t *, vm_offset_t *);

static void vnode_pager_dealloc(vm_pager_t);
static int vnode_pager_getpage(vm_pager_t, vm_page_t *, int, boolean_t);
static boolean_t vnode_pager_haspage(vm_pager_t, vm_offset_t);
static void vnode_pager_init(void);
static int vnode_pager_io(vn_pager_t, vm_page_t *, int, boolean_t, enum uio_rw);
static boolean_t vnode_pager_putpage(vm_pager_t, vm_page_t *, int, boolean_t);

static struct pagerops vnodepagerops = {.pgo_init     = vnode_pager_init,
                                        .pgo_alloc    = vnode_pager_alloc,
                                        .pgo_dealloc  = vnode_pager_dealloc,
                                        .pgo_getpages = vnode_pager_getpage,
                                        .pgo_putpages = vnode_pager_putpage,
                                        .pgo_haspage  = vnode_pager_haspage,
                                        .pgo_cluster  = vnode_pager_cluster};

static void vnode_pager_init()
{
#ifdef DEBUG
        if (vpagerdebug & VDB_FOLLOW)
                printf("vnode_pager_init()\n");
#endif
        log_trace("?");
}

static int bootstrap()
{
        vm_pager_zone_t::init(&s_vm_pager_zone, "vm pager");
        vnode_pager_zone_t::init(&s_vnode_pager_zone, "vm pager");

        TAILQ_INIT(&vnode_pager_list);
        return 0;
}

subsys_initcall(bootstrap);

/*
 * Allocate (or lookup) pager for a vnode.
 * Handle is a vnode pointer.
 */
extern vm_object_t vm_object_lookup(vm_pager_t);
extern void vm_object_enter(vm_object_t, vm_pager_t);

static vm_pager_t vnode_pager_alloc(void *handle, vm_size_t size, vm_prot_t prot, vm_offset_t foff)
{
        vm_pager_t pager;
        vn_pager_t vnp;
        vm_object_t object;
        vnode::attributes vattr;
        struct proc *p = proc::current(); /* XXX */

#ifdef DEBUG
        if (vpagerdebug & (VDB_FOLLOW | VDB_ALLOC))
                printf("vnode_pager_alloc(%x, %x, %x)\n", handle, size, prot);
#endif
        /*
         * Pageout to vnode, no can do yet.
         */
        if (handle == NULL)
                return (NULL);

        /*
         * Vnodes keep a pointer to any associated pager so no need to
         * lookup with vm_pager_lookup.
         */
        vnode::locked_ptr vp{(struct vnode *)handle};
        pager = (vm_pager_t)vp->v_un.vu_vmdata;
        if (pager == NULL)
        {
                /*
                 * Allocate pager structures
                 */
                // pager = (vm_pager_t)malloc(sizeof *pager /*, M_VMPAGER, M_WAITOK*/);
                pager = s_vm_pager_zone.alloc();
                if (pager == NULL)
                        return (NULL);

                // vnp = (vn_pager_t)malloc(sizeof *vnp /*, M_VMPGDATA, M_WAITOK*/);
                vnp = s_vnode_pager_zone.alloc();
                if (vnp == NULL)
                {
                        // free(pager /*, M_VMPAGER*/);
                        s_vm_pager_zone.free(pager);
                        return (NULL);
                }
                /*
                 * And an object of the appropriate size
                 */
                if (vp->getattr(&vattr, p->p_cred->pc_ucred, p) == 0)
                {
                        object = vm_object_allocate(round_page(vattr.va_size));
                        vm_object_enter(object, pager);
                        panic("VM_OBJECT");
                        // object->setpager(pager, 0, TRUE);
                }
                else
                {
                        // free(vnp /*,M_VMPGDATA*/);
                        // free(pager /*, M_VMPAGER*/);
                        s_vnode_pager_zone.free(vnp);
                        s_vm_pager_zone.free(pager);
                        return (NULL);
                }
                /*
                 * Hold a reference to the vnode and initialize pager data.
                 */
                vnode::refernce(vp.get_raw());
                vnp->vnp_flags = 0;
                vnp->vnp_vp    = vp.get_raw();
                vnp->vnp_size  = vattr.va_size;
                TAILQ_INSERT_TAIL(&vnode_pager_list, pager, pg_list);
                pager->pg_handle   = handle;
                pager->pg_type     = PG_VNODE;
                pager->pg_flags    = 0;
                pager->pg_ops      = &vnodepagerops;
                pager->pg_data     = vnp;
                vp->v_un.vu_vmdata = (vn_pager_t)(pager);
        }
        else
        {
                /*
                 * vm_object_lookup() will remove the object from the
                 * cache if found and also gain a reference to the object.
                 */
                object = vm_object_lookup(pager);
#ifdef DEBUG
                vnp = (vn_pager_t)pager->pg_data;
#endif
        }
#ifdef DEBUG
        if (vpagerdebug & VDB_ALLOC)
                printf("vnode_pager_setup: vp %x sz %x pager %x object %x\n", vp, vnp->vnp_size, pager, object);
#endif
        return (pager);
}

static void vnode_pager_dealloc(vm_pager_t pager)
{
        vn_pager_t vnp = (vn_pager_t)pager->pg_data;
        struct vnode *vp;
#ifdef NOTDEF
        struct proc *p = curproc; /* XXX */
#endif

#ifdef DEBUG
        if (vpagerdebug & VDB_FOLLOW)
                printf("vnode_pager_dealloc(%x)\n", pager);
#endif
        if (vp = vnp->vnp_vp)
        {
                vp->v_un.vu_vmdata = NULL;
                vp->v_flag &= ~VTEXT;
#if NOTDEF
                /* can hang if done at reboot on NFS FS */
                (void)VOP_FSYNC(vp, p->p_ucred, p);
#endif
                vnode::unreference(vp);
        }
        TAILQ_REMOVE(&vnode_pager_list, pager, pg_list);
        s_vnode_pager_zone.free(vnp);
        s_vm_pager_zone.free(pager);
}

static int vnode_pager_getpage(vm_pager_t pager, vm_page_t *mlist, int npages, boolean_t sync)
{
#ifdef DEBUG
        if (vpagerdebug & VDB_FOLLOW)
                printf("vnode_pager_getpage(%x, %x, %x, %x)\n", pager, mlist, npages, sync);
#endif
        return (vnode_pager_io((vn_pager_t)pager->pg_data, mlist, npages, sync, UIO_READ));
}

static boolean_t vnode_pager_putpage(vm_pager_t pager, vm_page_t *mlist, int npages, boolean_t sync)
{
        int err;

#ifdef DEBUG
        if (vpagerdebug & VDB_FOLLOW)
                printf("vnode_pager_putpage(%x, %x, %x, %x)\n", pager, mlist, npages, sync);
#endif
        if (pager == NULL)
                return (FALSE); /* ??? */
        err = vnode_pager_io((vn_pager_t)pager->pg_data, mlist, npages, sync, UIO_WRITE);
        /*
         * If the operation was successful, mark the pages clean.
         */
        if (err == VM_PAGER_OK)
        {
                while (npages--)
                {
                        (*mlist)->flags |= PG_CLEAN;
                        pmap_clear_modify((*mlist)->phys_addr);
                        mlist++;
                }
        }
        return (err);
}

static boolean_t vnode_pager_haspage(vm_pager_t pager, vm_offset_t offset)
{
        struct proc *p = proc::current(); /* XXX */
        vn_pager_t vnp = (vn_pager_t)pager->pg_data;
        size_t bn;
        int err;

/*
 * Offset beyond end of file, do not have the page
 * Lock the vnode first to make sure we have the most recent
 * version of the size.
 */
#if DAMIR
        vn_lock(vnp->vnp_vp, LK_EXCLUSIVE | LK_RETRY, p);
#else
        vnode::locked_ptr vp{vnp->vnp_vp};
#endif
        if (offset >= vnp->vnp_size)
        {
                return (FALSE);
        }

        /*
         * Read the index to find the disk block to read
         * from.  If there is no block, report that we don't
         * have this data.
         *
         * Assumes that the vnode has whole page or nothing.
         */
        err = vnp->vnp_vp->bmap(offset / vnp->vnp_vp->v_mount->mnt_stat.f_bsize, (struct vnode **)0, &bn, NULL);
        if (err)
        {
                return (TRUE);
        }
        return ((long)bn < 0 ? FALSE : TRUE);
}

static void vnode_pager_cluster(vm_pager_t pager, vm_offset_t offset, vm_offset_t *loffset, vm_offset_t *hoffset)
{
        vn_pager_t vnp = (vn_pager_t)pager->pg_data;
        vm_offset_t loff, hoff;

#ifdef DEBUG
        if (vpagerdebug & VDB_FOLLOW)
                printf("vnode_pager_cluster(%x, %x) ", pager, offset);
#endif
        loff = offset;
        if (loff >= vnp->vnp_size)
                panic("vnode_pager_cluster: bad offset");
        /*
         * XXX could use VOP_BMAP to get maxcontig value
         */
        hoff = loff + MAXBSIZE;
        if (hoff > round_page(vnp->vnp_size))
                hoff = round_page(vnp->vnp_size);

        *loffset = loff;
        *hoffset = hoff;
#ifdef DEBUG
        if (vpagerdebug & VDB_FOLLOW)
                printf("returns [%x-%x]\n", loff, hoff);
#endif
}

static int vnode_pager_io(vn_pager_t vnp, vm_page_t *mlist, int npages, boolean_t sync, enum uio_rw rw)
{
        struct uio auio;
        struct iovec aiov;
        vm_offset_t kva, foff;
        int error, size;
        struct proc *p = proc::current(); /* XXX */

        /* XXX */
        vm_page_t m;

        if (npages != 1)
        {
                panic("vnode_pager_io: cannot handle multiple pages");
        }

        m = *mlist;
        /* XXX */

        foff = m->offset + m->object->paging_offset;
        /*
         * Allocate a kernel virtual address and initialize so that
         * we can use VOP_READ/WRITE routines.
         */
        kva = vm_pager_map_pages(mlist, npages, sync);
        if (kva == 0)
        {
                return (VM_PAGER_AGAIN);
        }
        /*
         * After all of the potentially blocking operations have been
         * performed, we can do the size checks:
         *	read beyond EOF (returns error)
         *	short read
         */
#ifdef DAMIR
        vn_lock(vnp->vnp_vp, LK_EXCLUSIVE | LK_RETRY, p);
#else
        vnode::locked_ptr vp{vnp->vnp_vp};
#endif
        if (foff >= vnp->vnp_size)
        {
                vm_pager_unmap_pages(kva, npages);

                return (VM_PAGER_BAD);
        }
        if (foff + PAGE_SIZE > vnp->vnp_size)
        {
                size = vnp->vnp_size - foff;
        }
        else
        {
                size = PAGE_SIZE;
        }

        aiov.iov_base   = (void *)kva;
        aiov.iov_len    = size;
        auio.uio_iov    = &aiov;
        auio.uio_iovcnt = 1;
        auio.uio_offset = foff;
        auio.uio_segflg = UIO_SYSSPACE;
        auio.uio_rw     = rw;
        auio.uio_resid  = size;
        auio.uio_procp  = p;

        if (rw == UIO_READ)
        {
                error = vnp->vnp_vp->read(&auio);
        }
        else
        {
                error = vnp->vnp_vp->write(&auio);
        }

        if (!error)
        {
                int count = size - auio.uio_resid;

                if (count == 0)
                        error = -EINVAL;
                else if (count != PAGE_SIZE && rw == UIO_READ)
                        bzero((void *)(kva + count), PAGE_SIZE - count);
        }

        vm_pager_unmap_pages(kva, npages);
        return (error ? VM_PAGER_ERROR : VM_PAGER_OK);
}

boolean_t vnode_pager_uncache(struct vnode *vp)
{
        struct proc *p = proc::current(); /* XXX */
        vm_object_t object;
        boolean_t uncached;
        vm_pager_t pager;

        /*
         * Not a mapped vnode
         */
        if (vp->v_type != VREG || (pager = (vm_pager_t)vp->v_un.vu_vmdata) == NULL)
                return (TRUE);
#ifdef DEBUG
        if (!VOP_ISLOCKED(vp))
        {
                extern int (**nfsv2_vnodeop_p)();

                if (vp->v_op != nfsv2_vnodeop_p)
                        panic("vnode_pager_uncache: vnode not locked!");
        }
#endif
        /*
         * Must use vm_object_lookup() as it actually removes
         * the object from the cache list.
         */
        object = vm_object_lookup(pager);
        if (object)
        {
                uncached = (object->ref_count <= 1);
#if DAMIR
                VOP_UNLOCK(vp, 0, p);
                pager_cache(object, FALSE);
                vn_lock(vp, LK_EXCLUSIVE | LK_RETRY, p);
#else
                // vp->unlock();
#endif
        }
        else
                uncached = TRUE;
        return (uncached);
}

/*
 * (XXX)
 * Lets the VM system know about a change in size for a file.
 * If this vnode is mapped into some address space (i.e. we have a pager
 * for it) we adjust our own internal size and flush any cached pages in
 * the associated object that are affected by the size change.
 *
 * Note: this routine may be invoked as a result of a pager put
 * operation (possibly at object termination time), so we must be careful.
 */
void vnode_pager_setsize(struct vnode *vp, size_t nsize)
{
        vn_pager_t vnp;
        vm_object_t object;
        vm_pager_t pager;

        /*
         * Not a mapped vnode
         */
        if (vp == NULL || vp->v_type != VREG || vp->v_un.vu_vmdata == NULL)
                return;
        /*
         * Hasn't changed size
         */
        pager = (vm_pager_t)vp->v_un.vu_vmdata;
        vnp   = (vn_pager_t)pager->pg_data;
        if (nsize == vnp->vnp_size)
                return;
        /*
         * No object.
         * This can happen during object termination since
         * vm_object_page_clean is called after the object
         * has been removed from the hash table, and clean
         * may cause vnode write operations which can wind
         * up back here.
         */
        object = vm_object_lookup(pager);
        if (object == NULL)
                return;

#ifdef DEBUG
        if (vpagerdebug & (VDB_FOLLOW | VDB_SIZE))
                printf("vnode_pager_setsize: vp %x obj %x osz %d nsz %d\n", vp, object, vnp->vnp_size, nsize);
#endif
        /*
         * File has shrunk.
         * Toss any cached pages beyond the new EOF.
         */
        if (nsize < vnp->vnp_size)
        {
                object->lock();
                panic("VM_OBJECT");
                // object->page_remove((vm_offset_t)nsize, vnp->vnp_size);
                object->unlock();
        }
        vnp->vnp_size = (vm_offset_t)nsize;
        vm_object_deallocate(object);
}

void vnode_pager_umount(struct mount *mp)
{
        struct proc *p = proc::current(); /* XXX */
        vm_pager_t pager, npager;

        for (pager = vnode_pager_list.tqh_first; pager != NULL; pager = npager)
        {
                /*
                 * Save the next pointer now since uncaching may
                 * terminate the object and render pager invalid
                 */
                npager = pager->pg_list.tqe_next;
                struct vnode::locked_ptr vp
                {
                        ((vn_pager_t)pager->pg_data)->vnp_vp
                };
                if (mp == (struct mount *)0 || vp->v_mount == mp)
                {
#if DAMIR
                        vn_lock(vp, LK_EXCLUSIVE | LK_RETRY, p);
#else
#endif
                        vnode_pager_uncache(vp.get_raw());
                }
        }
}

struct pagerops const *vnode_pager_getops() { return &vnodepagerops; }
