#pragma once

struct process;
struct session;

struct pgrp;

#ifdef __cplusplus
#include <mach/intrusive/list.hh>

#ifdef SYSCALL_GENERATOR

#endif

/*
 * One structure allocated per process group.
 */
// LIST_ENTRY(pgrp) pg_hash;
// LIST_HEAD(pg_members, proc) pg_members;

struct pgrp
{
        using list_link = intrusive::list::link<pgrp>;

        /* Hash chain. */
        list_link pg_hash;

        /* Pointer to pgrp members. */
        process::list_link::head<&process::p_pglink> pg_members;

        /* Pointer to session. */
        struct session *pg_session;

        /* Pgrp id. */
        pid_t pg_id;

        /* # procs qualifying pgrp for job control */
        int pg_jobc;

        void signal(int signum, int checkctty);
};

#endif

#ifdef __cplusplus
extern "C"
{
#endif

        void proc_group_init(size_t maxproc, process_t initproc);

        struct pgrp *proc_group_find(pid_t pgid);

        int proc_group_enter(pthread_t p, pid_t pgid, int mksess);

        /*
         * remove process from process group
         */
        int proc_group_leave(pthread_t a_procp);

#ifdef __cplusplus
}
#endif