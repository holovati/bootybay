/*
 * Mach Operating System
 * Copyright (c) 1993-1989 Carnegie Mellon University
 * All Rights Reserved.
 *
 * Permission to use, copy, modify and distribute this software and its
 * documentation is hereby granted, provided that both the copyright
 * notice and this permission notice appear in all copies of the
 * software, derivative works or modified versions, and any portions
 * thereof, and that both notices appear in supporting documentation.
 *
 * CARNEGIE MELLON ALLOWS FREE USE OF THIS SOFTWARE IN ITS "AS IS"
 * CONDITION.  CARNEGIE MELLON DISCLAIMS ANY LIABILITY OF ANY KIND FOR
 * ANY DAMAGES WHATSOEVER RESULTING FROM THE USE OF THIS SOFTWARE.
 *
 * Carnegie Mellon requests users of this software to return to
 *
 *  Software Distribution Coordinator  or  Software.Distribution@CS.CMU.EDU
 *  School of Computer Science
 *  Carnegie Mellon University
 *  Pittsburgh PA 15213-3890
 *
 * any improvements or extensions that they make and grant Carnegie Mellon
 * the rights to redistribute these changes.
 */
/*
 * Device switch for i386 AT bus.
 */

// clang-format off
#include <mach/machine/vm_types.h>
#include <device/conf.h>
// clang-format on
extern vm_offset_t block_io_mmap();

extern int timeopen(), timeclose();
extern vm_offset_t timemmap();
#define timename "time"

extern int kdopen(), kdclose(), kdread(), kdwrite();
extern int kdgetstat(), kdsetstat(), kdportdeath();
extern vm_offset_t kdmmap();
#define kdname "kd"

#if NCOM > 0
extern int comopen(), comclose(), comread(), comwrite();
extern int comgetstat(), comsetstat(), comportdeath();
#  define comname "com"
#endif

extern int kbdopen(), kbdclose(), kbdread();
extern int kbdgetstat(), kbdsetstat();
#define kbdname "kbd"

extern int mouseopen(), mouseclose(), mouseread();
#define mousename "mouse"

extern int ioplopen(), ioplclose();
extern vm_offset_t ioplmmap();
#define ioplname "iopl"

/*
 * List of devices - console must be at slot 0
 */
struct dev_ops dev_name_list[] = {
    /*name,		open,		close,		read,
      write,	getstat,	setstat,	mmap,
      async_in,	reset,		port_death,	subdev,
      dev_info */

    /* We don't assign a console here, when we find one via
       cninit() we stick something appropriate here through the
       indirect list */
    {"cn",
     nulldev,
     nulldev,
     nulldev,
     nulldev,
     nulldev,
     nulldev,
     nulldev,
     nodev,
     nulldev,
     nulldev,
     0,
     nodev},
#if 0
    {kdname,
     kdopen,
     kdclose,
     kdread,
     kdwrite,
     kdgetstat,
     kdsetstat,
     kdmmap,
     nodev,
     nulldev,
     kdportdeath,
     0,
     nodev},
#endif
    {timename,
     timeopen,
     timeclose,
     nulldev,
     nulldev,
     nulldev,
     nulldev,
     timemmap,
     nodev,
     nulldev,
     nulldev,
     0,
     nodev},

#ifndef LINUX_DEV
#  if NHD > 0
    {hdname,
     hdopen,
     hdclose,
     hdread,
     hdwrite,
     hdgetstat,
     hdsetstat,
     nomap,
     nodev,
     nulldev,
     nulldev,
     1024,
     hddevinfo},
#  endif

#  if NAHA > 0
    {rzname,
     rz_open,
     rz_close,
     rz_read,
     rz_write,
     rz_get_status,
     rz_set_status,
     nomap,
     nodev,
     nulldev,
     nulldev,
     1024, /* 8 */
     rz_devinfo},

    {tzname,
     rz_open,
     rz_close,
     rz_read,
     rz_write,
     rz_get_status,
     rz_set_status,
     nomap,
     nodev,
     nulldev,
     nulldev,
     8,
     nodev},

    {cdname,
     cd_open,
     cd_close,
     cd_read,
     cd_write,
     nodev,
     nodev,
     nomap,
     nodev,
     nulldev,
     nulldev,
     8,
     nodev},

    {scname,
     rz_open,
     rz_close,
     rz_read,
     rz_write,
     rz_get_status,
     rz_set_status,
     nomap,
     nodev,
     nulldev,
     nulldev,
     8,
     nodev},

#  endif

#  if NFD > 0
    {fdname,
     fdopen,
     fdclose,
     fdread,
     fdwrite,
     fdgetstat,
     fdsetstat,
     nomap,
     nodev,
     nulldev,
     nulldev,
     64,
     fddevinfo},
#  endif

#  if NWT > 0
    {wtname,
     wtopen,
     wtclose,
     wtread,
     wtwrite,
     nulldev,
     nulldev,
     nomap,
     nodev,
     nulldev,
     nulldev,
     0,
     nodev},
#  endif

#  if NPC586 > 0
    {pc586name,
     pc586open,
     nulldev,
     nulldev,
     pc586output,
     pc586getstat,
     pc586setstat,
     nomap,
     pc586setinput,
     nulldev,
     nulldev,
     0,
     nodev},
#  endif

#  if NNE > 0
    {nename,
     neopen,
     nulldev,
     nulldev,
     neoutput,
     negetstat,
     nesetstat,
     nulldev,
#    ifdef FIPC
     nesetinput,
     nulldev,
     nefoutput,
     0,
#    else
     nesetinput,
     nulldev,
     nulldev,
     0,
#    endif
     nodev},
#  endif

#  if NAT3C501 > 0
    {at3c501name,
     at3c501open,
     nulldev,
     nulldev,
     at3c501output,
     at3c501getstat,
     at3c501setstat,
     nomap,
     at3c501setinput,
     nulldev,
     nulldev,
     0,
     nodev},
#  endif

#  if NNS8390 > 0
    {ns8390wdname,
     wd8003open,
     nulldev,
     nulldev,
     ns8390output,
     ns8390getstat,
     ns8390setstat,
     nomap,
     ns8390setinput,
     nulldev,
     nulldev,
     0,
     nodev},

    {ns8390elname,
     eliiopen,
     nulldev,
     nulldev,
     ns8390output,
     ns8390getstat,
     ns8390setstat,
     nomap,
     ns8390setinput,
     nulldev,
     nulldev,
     0,
     nodev},
#  endif

#  if NUL > 0
    {ulname,
     ulopen,
     nulldev,
     nulldev,
     uloutput,
     ulgetstat,
     ulsetstat,
     nulldev,
     ulsetinput,
     nulldev,
     nulldev,
     0,
     nodev},
#  endif

#  if NWD > 0
    {wdname,
     wdopen,
     nulldev,
     nulldev,
     wdoutput,
     wdgetstat,
     wdsetstat,
     nulldev,
     wdsetinput,
     nulldev,
     nulldev,
     0,
     nodev},
#  endif

#  if N0HPP > 0
    {hppname,
     hppopen,
     nulldev,
     nulldev,
     hppoutput,
     hppgetstat,
     hppsetstat,
     nulldev,
     hppsetinput,
     nulldev,
     nulldev,
     0,
     nodev},
#  endif

#  if NPAR > 0
    {parname,
     paropen,
     nulldev,
     nulldev,
     paroutput,
     pargetstat,
     parsetstat,
     nomap,
     parsetinput,
     nulldev,
     nulldev,
     0,
     nodev},
#  endif

#  if NDE6C > 0
    {de6cname,
     de6copen,
     nulldev,
     nulldev,
     de6coutput,
     de6cgetstat,
     de6csetstat,
     nomap,
     de6csetinput,
     nulldev,
     nulldev,
     0,
     nodev},
#  endif
#endif

#if DAMIR
#if NCOM > 0
    {comname,
     comopen,
     comclose,
     comread,
     comwrite,
     comgetstat,
     comsetstat,
     nomap,
     nodev,
     nulldev,
     comportdeath,
     0,
     nodev},
#endif
#endif

#ifndef LINUX_DEV
#  if NLPR > 0
    {lprname,
     lpropen,
     lprclose,
     lprread,
     lprwrite,
     lprgetstat,
     lprsetstat,
     nomap,
     nodev,
     nulldev,
     lprportdeath,
     0,
     nodev},
#  endif
#endif

#if NBLIT > 0
    {blitname,
     blitopen,
     blitclose,
     nodev,
     nodev,
     blit_get_stat,
     nodev,
     blitmmap,
     nodev,
     nodev,
     nodev,
     0,
     nodev},
#endif
#if 0
    {mousename,
     mouseopen,
     mouseclose,
     mouseread,
     nodev,
     nulldev,
     nulldev,
     nomap,
     nodev,
     nulldev,
     nulldev,
     0,
     nodev},

    {kbdname,
     kbdopen,
     kbdclose,
     kbdread,
     nodev,
     kbdgetstat,
     kbdsetstat,
     nomap,
     nodev,
     nulldev,
     nulldev,
     0,
     nodev},
#endif
    {ioplname,
     ioplopen,
     ioplclose,
     nodev,
     nodev,
     nodev,
     nodev,
     ioplmmap,
     nodev,
     nulldev,
     nulldev,
     0,
     nodev},

#if 0
#  if NHD > 0
	{ pchdname,     pchdopen,      	hdclose,	pchdread,
	  pchdwrite,	pchdgetstat,	pchdsetstat,	nomap,
	  nodev,	nulldev,	nulldev,	16,
	  hddevinfo },
#  endif
#endif

#if 0
#  if NHD > 0
        { hdname,       hdopen,         hdclose,        hdread,
          hdwrite,      hdgetstat,      hdsetstat,      nomap,
          nodev,        nulldev,        nulldev,        16,
          hddevinfo },
#  endif
#endif

};
int dev_name_count = sizeof(dev_name_list) / sizeof(dev_name_list[0]);

/*
 * Indirect list.
 */
struct dev_indirect dev_indirect_list[] = {

    /* console */
    {"console", &dev_name_list[0], 0}};
int dev_indirect_count =
    sizeof(dev_indirect_list) / sizeof(dev_indirect_list[0]);
