#include <mach/tmpsys.h>

#include <emerixx/memory_rw.h>

#include <rs64/version.h>

#include <sys/utsname.h>

static struct utsname const s_utsname = {"Emerixx", "(none)", GIT_COMMIT_HASH, GIT_BRANCH, "x86_64", "(none)"};

// ********************************************************************************************************************************
kern_return_t sys_uname(utsname *a_utsname)
// ********************************************************************************************************************************
{
        return copyout(&s_utsname, a_utsname, sizeof(s_utsname));
}
