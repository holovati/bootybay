#include <mach/tmpsys.h>

#include <kernel/time_value.h>
#include <mach/machine/spl.h>
#include <emerixx/memory_rw.h>

#include <kernel/thread.h>

#include <sys/resource.h>

#include <emerixx/process.h>

kern_return_t sys_getrusage(pthread_t a_thread, int who, struct rusage *a_rusage)
{
        rusage rusage_out = {};
        switch (who)
        {
        case RUSAGE_SELF: {
                // time_value_t user_time;
                // time_value_t system_time;
                spl_t opl = splsched();
                // thread_read_times(a_thread->pt_kthread, &user_time, &system_time);
                // pthread_read_times(a_thread, &rusage_out);
                splx(opl);
#if 0
                rusage_out.ru_utime.tv_sec  = user_time.seconds;
                rusage_out.ru_utime.tv_usec = user_time.microseconds;

                rusage_out.ru_stime.tv_sec  = system_time.seconds;
                rusage_out.ru_stime.tv_usec = system_time.microseconds;
#endif
                break;
        }
        case RUSAGE_CHILDREN:
                break;
        case RUSAGE_THREAD:
                break;

        default:
                return KERN_FAIL(EINVAL);
                break;
        }

        return emerixx::uio::copyout(&rusage_out, a_rusage);
}
