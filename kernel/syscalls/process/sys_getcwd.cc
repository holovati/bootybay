#include <mach/tmpsys.h>

#include <dirent.h>
#include <fcntl.h>
#include <sys/stat.h>

#include <aux/defer.hh>

#include <etl/algorithm.hh>
#include <etl/cstring.hh>

#include <emerixx/memory_rw.h>
#include <emerixx/vfs.h>

#include <emerixx/fsdata.hh>
#include <emerixx/process.h>

// ********************************************************************************************************************************
static kern_return_t getcwd_push_component(char *a_path_beg, char **a_path_end, char const *a_cnp, natural_t a_cnp_len)
// ********************************************************************************************************************************
{
        char *path_end = (*a_path_end) - a_cnp_len;

        if (path_end < a_path_beg)
        {
                return KERN_FAIL(ERANGE);
        }

        etl::memcpy8(path_end, a_cnp, a_cnp_len);

        *a_path_end = path_end;

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t getcwd_find_component_name(vfs_node_t a_vfsn,
                                                natural_t a_ino,
                                                char *a_buf,
                                                natural_t a_buf_len,
                                                char **a_cnp_out,
                                                natural_t *a_cnp_len_out)
// ********************************************************************************************************************************
{
        vfs_file_t f = nullptr;

        kern_return_t retval = vfs_file_open(a_vfsn, O_RDONLY | O_DIRECTORY, &f);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        defer(vfs_file_close(f));

        struct uio auio;
        struct iovec aiov;

        aiov.iov_base = a_buf;
        aiov.iov_len  = a_buf_len;

        auio.uio_iov    = &aiov;
        auio.uio_iovcnt = 1;
        auio.uio_rw     = UIO_READ;
        auio.uio_segflg = UIO_SYSSPACE;
        auio.uio_offset = 0;
        auio.uio_resid  = a_buf_len;

        for (boolean_t done = false; done == false;)
        {
                aiov.iov_base  = a_buf;
                aiov.iov_len   = a_buf_len;
                auio.uio_resid = a_buf_len;

                retval = vfs_file_getdents(f, &auio);

                if (KERN_STATUS_FAILURE(retval))
                {
                        break;
                }

                natural_t nbytes_read = a_buf_len - auio.uio_resid;

                if (nbytes_read == 0)
                {
                        // Nothing more here
                        retval = KERN_FAIL(ENOENT);
                        break;
                }

                for (natural_t pos = 0; pos < nbytes_read;)
                {
                        struct dirent *dent = (struct dirent *)(a_buf + pos);

                        if (dent->d_ino == a_ino)
                        {
                                *a_cnp_out = (a_buf + pos + offsetof(struct dirent, d_name));

                                for (*a_cnp_len_out = 0; *((*a_cnp_out) + (*a_cnp_len_out)); (*a_cnp_len_out)++)
                                        ;

                                done = true;
                                break;
                        }

                        pos += dent->d_reclen;
                }
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t sys_getcwd(pthread_t a_thread, char const *a_buffer, natural_t a_buffer_size)
// ********************************************************************************************************************************
{
        // Check if we can fit "/"
        if (a_buffer_size < 2)
        {
                return KERN_FAIL(ERANGE);
        }

        if (static_cast<integer_t>(a_buffer_size) < 1)
        {
                return KERN_FAIL(ENOMEM);
        }

        vfs_node_t vfsn_cwd = nullptr;

        kern_return_t retval = vfs_util_fd_to_vfsn(AT_FDCWD, &vfsn_cwd, false);
        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }
        defer(vfs_node_rel(vfsn_cwd));

        vfs_node_t vfsn_root = fs_context_getroot(uthread_self()->m_fsctx);
        defer(vfs_node_rel(vfsn_root));

        if (vfsn_root == vfsn_cwd)
        {
                retval = copyout(const_cast<char *>("/"), const_cast<char *>(a_buffer), sizeof("/"));

                if (KERN_STATUS_FAILURE(retval))
                {
                        return retval;
                }

                return sizeof("/");
        }
        // We are not at root, here we go.

        natural_t kbuf_len = 0;

        char *kbuf = vfs_pathbuf_alloc(&kbuf_len);
        defer(vfs_pathbuf_free(kbuf));

        if (a_buffer_size > kbuf_len)
        {
                a_buffer_size = kbuf_len;
        }

        kbuf_len = etl::min(kbuf_len, a_buffer_size);

        char *kbuf_end = kbuf + (kbuf_len - 1);

        natural_t dentbuf_len = 0;

        char *dentbuf = vfs_pathbuf_alloc(&dentbuf_len);
        defer(vfs_pathbuf_free(dentbuf));

        char *kbuf_cur = kbuf_end;
        getcwd_push_component(kbuf, &kbuf_cur, "", 1);

        vfs_node_t vfsn_cur = nullptr;
        vfs_node_ref(vfsn_cur = vfsn_cwd);

        while (vfsn_cur != vfsn_root)
        {
                struct stat statbuf;
                vfs_node_lock(vfsn_cur);
                retval = vfs_node_stat(vfsn_cur, &statbuf);
                vfs_node_ref(vfsn_cur);
                vfs_node_put(vfsn_cur);

                if (KERN_STATUS_FAILURE(retval))
                {
                        break;
                }

                vfs_node_t vfsn_next = nullptr;

                retval = vfs_node_lookup(vfsn_cur, "..", 2, &vfsn_next, false);
                if (KERN_STATUS_FAILURE(retval))
                {
                        break;
                }
                vfs_node_rel(vfsn_cur);

                vfsn_cur = vfsn_next;
                vfs_node_ref(vfsn_cur);
                vfs_node_put(vfsn_cur);

                vfsn_next = nullptr;

                char *cnp = nullptr;

                natural_t cnp_len = 0;

                retval = getcwd_find_component_name(vfsn_cur, statbuf.st_ino, dentbuf, dentbuf_len, &cnp, &cnp_len);

                if (KERN_STATUS_FAILURE(retval))
                {
                        break;
                }

                retval = getcwd_push_component(kbuf, &kbuf_cur, cnp, cnp_len);

                if (KERN_STATUS_FAILURE(retval))
                {
                        break;
                }

                retval = getcwd_push_component(kbuf, &kbuf_cur, "/", 1);

                if (KERN_STATUS_FAILURE(retval))
                {
                        break;
                }
        }
        vfs_node_rel(vfsn_cur);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        retval = copyout(kbuf_cur, const_cast<char *>(a_buffer), (natural_t)(kbuf_end - kbuf_cur));

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        retval = (kern_return_t)(kbuf_end - kbuf_cur);

        return retval;
}
