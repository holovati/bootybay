#include <mach/tmpsys.h>

/*
  brk() sets the end of the data segment to the value specified by
  addr, when that value is reasonable, the system has enough memory,
  and the process does not exceed its maximum data size (see
  setrlimit(2)).

  On success, brk() returns zero.  On error, -1 is returned, and errno
  is set to ENOMEM.
*/
vm_address_t sys_brk(pthread_t, vm_address_t) { return KERN_FAIL(ENOSYS); }
