#include <mach/tmpsys.h>

#include <emerixx/process.h>

pid_t sys_getpid(pthread_t a_thread) { return a_thread->m_pid; }