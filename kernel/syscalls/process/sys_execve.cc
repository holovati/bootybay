#include <elf.h>

#include <emerixx/vfs.h>
#include <emerixx/fsdata.hh>
#include <emerixx/process.h>

#include <emerixx/vfs/vfs_node_data.h>

#include <etl/algorithm.hh>
#include <etl/bit.hh>
#include <etl/cstring.hh>

#include <fcntl.h>
#include <sys/stat.h>

#include <kernel/vm/vm_fault.h>
#include <kernel/vm/vm_kern.h>
#include <kernel/vm/vm_map.h>
#include <kernel/vm/vm_page.h>
#include <kernel/vm/vm_page_map.h>
#include <kernel/vm/vm_param.h>
#include <kernel/vm/vm_prot.h>

#include <kernel/zalloc.h>

#include <aux/defer.hh>
#include <mach/tmpsys.h>
#include <mach/param.h>

#include <sched.h>
#include <string.h>

#define IS_ELF(ehdr)                                                                                                               \
        ((ehdr).e_ident[EI_MAG0] == ELFMAG0 && (ehdr).e_ident[EI_MAG1] == ELFMAG1 && (ehdr).e_ident[EI_MAG2] == ELFMAG2            \
         && (ehdr).e_ident[EI_MAG3] == ELFMAG3)

#define IS_SCRIPT(shhdr) ((shhdr.magic[0] == '#' && shhdr.magic[1] == '!'))

enum execop_t
{
        EXECOP_LOAD_PIMAGE,
        EXECOP_LOAD_INTERP,
        EXECOP_LOAD_FINISH
};

/* Elf to native */
static vm_prot_t eprot[] = { VM_PROT_NONE,
                             VM_PROT_EXECUTE,
                             VM_PROT_WRITE,
                             VM_PROT_WRITE | VM_PROT_EXECUTE,
                             VM_PROT_READ,
                             VM_PROT_READ | VM_PROT_EXECUTE,
                             VM_PROT_READ | VM_PROT_WRITE,
                             VM_PROT_READ | VM_PROT_WRITE | VM_PROT_EXECUTE };

static struct _elf_aux_vector
{
        Elf64_auxv_t at_phdr;
        Elf64_auxv_t at_execfd;
        Elf64_auxv_t at_phent;
        Elf64_auxv_t at_phnum;
        Elf64_auxv_t at_pagesz;
        Elf64_auxv_t at_base;
        Elf64_auxv_t at_flags;
        Elf64_auxv_t at_entry;
        Elf64_auxv_t at_notelf;
        Elf64_auxv_t at_uid;
        Elf64_auxv_t at_euid;
        Elf64_auxv_t at_gid;
        Elf64_auxv_t at_egid;
        Elf64_auxv_t at_platform;
        // Add the missing ones as needed
        Elf64_auxv_t at_null; // Terminator

} s_elf_aux_vector_template = {
  //
        {AT_PHDR,    { AT_NULL }}, //
        { AT_IGNORE, { AT_NULL }}, // at_execfd
        { AT_PHENT,  { AT_NULL }}, //
        { AT_PHNUM,  { AT_NULL }}, //
        { AT_PAGESZ, { AT_NULL }}, //
        { AT_IGNORE, { AT_NULL }}, // a_base
        { AT_IGNORE, { AT_NULL }}, // at_flags
        { AT_ENTRY,  { AT_NULL }}, //
        { AT_IGNORE, { AT_NULL }}, // at_notelf
        { AT_UID,    { AT_NULL }}, //
        { AT_EUID,   { AT_NULL }}, //
        { AT_GID,    { AT_NULL }}, //
        { AT_EGID,   { AT_NULL }}, //
        { AT_IGNORE, { AT_NULL }}, // at_platform
        { AT_NULL,   { AT_NULL }}  // terminator
  //
};

union execfile_hdr
{
        Elf64_Ehdr elf;
        struct
        {
                char magic[2];
                char args[510];
        } sh;
};

union stack_field_t
{
        char **spp;
        char *sp;
        void *ptr;
        _elf_aux_vector *eauxp;
        integer_t *intp;
        vm_address_t *vmap;
        natural_t val;
        Elf64_Phdr *ephdr;
        execfile_hdr *hdr;
};

static_assert(sizeof(stack_field_t) == sizeof(void *));

static_assert(IS_ALIGNED(USER_STACK_SIZE, PAGE_SIZE));

#define MAX_ARG_STRLEN (PAGE_SIZE * 5)

static vm_object_t elf_section_object_create(Elf64_Phdr *a_ephdr, vfs_node_t a_vfsn);

static inline kern_return_t ustrcpyin(char **a_uv, char **a_but, char **a_cur)
{
        kern_return_t retval;

        char *new_loc = *a_cur;

        *a_cur = ALIGN(stpncpyin(*a_cur, *a_uv, MAX_ARG_STRLEN, &retval) + 1, alignof(char *));

        if (retval == KERN_FAIL(ENAMETOOLONG))
        {
                return KERN_FAIL(E2BIG);
        }

        *a_but = (char *)round_page(*a_cur);

        *a_uv = new_loc;

        return retval;
}

static inline kern_return_t kstrcpyin(char **a_uv, char **a_but, char **a_cur)
{
        char *new_loc = *a_cur;

        *a_cur = ALIGN(stpncpy(*a_cur, *a_uv, MAX_ARG_STRLEN) + 1, alignof(char *));

        if ((new_loc + MAX_ARG_STRLEN) == *a_cur)
        {
                return KERN_FAIL(E2BIG);
        }

        *a_but = (char *)round_page(*a_cur);

        *a_uv = new_loc;

        return KERN_SUCCESS;
}

static inline kern_return_t uvecdatacpyin(char *a_valv[], char **but, char **cur)
{
        kern_return_t retval = KERN_SUCCESS;
        for (char **strval = a_valv; *strval; strval++)
        {
                retval = ustrcpyin(strval, but, cur);

                if (KERN_STATUS_FAILURE(retval))
                {
                        break;
                }
        }
        return retval;
}

// Rebase kernel adresses in a user stack vector to user addresses.
static inline void rebaseuvec(vm_address_t **a_vec, vm_address_t a_kustk, vm_address_t a_ustk, size_t a_ustksz)
{
        for (vm_address_t *addr = *a_vec; *addr; addr = *a_vec)
        {
                // Store the offset for the begining of the stack
                (*a_vec)++;
                *addr -= a_kustk;
                *addr += a_ustk - a_ustksz;
        }
}

kern_return_t sys_execveat(int a_dfd, char const *a_filename, char const *const a_argv[], char const *const a_envp[], int a_flags)
{
        process_t utask;
        pthread_t uthrd;

        uthrd = uthread_self_ex(&utask);
        defer(uthread_put_ex(uthrd, utask));

        // utask_suspend(utask);
        // defer(utask_resume(utask));

        stack_field_t stkbeg, stkend, stktop, stkbut, stkcur, stkvecend, stkstrbeg;
        // Alloc a new stack object

        vm_object_t stkobj = nullptr;

        kern_return_t retval = vm_object_create_unamed(USER_STACK_SIZE, &stkobj);

        if (retval != 0)
        {
                return KERN_FAIL(ENOMEM);
        }

        // Release object when done, if we successfully exec'ed the object will gain a extra ref and survive
        defer(vm_object_rel(stkobj));

        // Create a new stack object and map it in the kernel address space
        vm_map_entry vme = {};

        stkbeg.val = 0;

        retval = vm_map_find(kernel_map, stkobj, 0, &stkbeg.val, USER_STACK_SIZE, true, &vme);
        if (retval != KERN_SUCCESS)
        {
                return KERN_FAIL(ENOMEM);
        }
        // Release kernel mapping when done (Will get rid of the ref above)
        defer(vm_map_remove(kernel_map, stkbeg.val, stkend.val));
        // Initialize stack pointers
        stkend.val = (stkbeg.val + USER_STACK_SIZE);

        // Copyin the filepath provided into the temp buffer
        natural_t pathlen = (natural_t)(stpncpyin(stkbeg.sp, a_filename, PATH_MAX, &retval) - stkbeg.sp);
        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        stkcur.val = stktop.val = round_page(stkbeg.val + PATH_MAX);
        stkbut.val              = stktop.val + PAGE_SIZE * 4;

        // Extract argc pointer
        stack_field_t argc = stkcur;
        stkcur.intp++;

        // Reserve 31 pointers for script args.
        stack_field_t scrargv = stkcur;
        stkcur.spp += 31;

        // Copyin arg vector and extract pointer
        stack_field_t argv = stkcur;

        stkcur.spp = ptpncpyin(argv.spp, a_argv, stkbut.spp - stkcur.spp, &retval);
        if (KERN_STATUS_FAILURE(retval))
        {
                if (retval == KERN_FAIL(ENAMETOOLONG))
                {
                        retval = KERN_FAIL(E2BIG);
                }

                return retval;
        }
        *argc.intp = (stkcur.spp - argv.spp);
        if (*argc.intp > 0)
        {
                // Null terminator
                stkcur.spp += 1;
        }
        else
        {
                // Must be atleast one
                return KERN_FAIL(EINVAL);
        }

        // Copyin env vector
        stack_field_t envp = stkcur;
        if (a_envp)
        {
                stkcur.spp = ptpncpyin(envp.spp, a_envp, stkbut.spp - stkcur.spp, &retval);
                if (KERN_STATUS_FAILURE(retval))
                {
                        if (retval == KERN_FAIL(ENAMETOOLONG))
                        {
                                retval = KERN_FAIL(E2BIG);
                        }
                        return retval;
                }
        }
        integer_t envc = (stkcur.spp - envp.spp);
        if (envc > 0)
        {
                // Null terminator
                stkcur.spp += 1;
        }

        // Check if we have space for elf aux vector
        if ((stkcur.val + sizeof(_elf_aux_vector)) >= stkbut.val)
        {
                return KERN_FAIL(E2BIG);
        }

        // Save the end to the last entry of the env vector
        stack_field_t eaux = stkcur;
        stkcur.eauxp++;
        *eaux.eauxp = s_elf_aux_vector_template;

        // Record the end of stack vectors(including padding)
        // We might need to adjust them depending on if we are loading scripts.
        stkvecend = stkcur;

        // Move the stk pointer to the next page so we can start copying in strings
        stkstrbeg.val = stkcur.val = stkbut.val;

        // Allocate a new vm context
        vm_map_t vmctx = vm_map_create(pmap_create(0), VM_MIN_ADDRESS, VM_MAX_ADDRESS, true);
        defer(vm_map_deallocate(vmctx));

        // We need to collect the entry point during mapping
        vm_address_t regs[2] = {};

        // Will hold the stack protection extracted from the ELF files.
        vm_prot_t stkprot = VM_PROT_NONE;

        // Will hold the information about the interperter if there is one
        Elf64_Phdr interp_phdr = {};

        for (execop_t execop = EXECOP_LOAD_PIMAGE; execop != EXECOP_LOAD_FINISH;)
        {
                vfs_node_t vfsn;

                retval = vfs_util_simple_lookup_krn_path(stkbeg.sp, pathlen, a_dfd, &vfsn, true, true);

                if (KERN_STATUS_FAILURE(retval))
                {
                        return retval;
                }

                defer(vfs_node_put(vfsn));

                retval = vfs_node_read(vfsn, 0, stkbeg.ptr, sizeof(*stkbeg.hdr));

                if (KERN_STATUS_FAILURE(retval))
                {
                        return retval;
                }

                if (IS_SCRIPT(stkbeg.hdr->sh))
                {
                        // If the script arg vector reached argc. This means we processed too many scripts.
                        if ((scrargv.spp + 2) >= argv.spp)
                        {
                                return KERN_FAIL(ELOOP);
                        }

                        // Interpreters cannot be scripts
                        if (execop == EXECOP_LOAD_INTERP)
                        {
                                return KERN_FAIL(EIO);
                        }

                        // Make sure the string is terminated
                        stkbeg.hdr->sh.args[sizeof(stkbeg.hdr->sh.args) - 1] = '\0';

                        // Locate '\n'. If not present, bail.
                        char *shend = strchr(stkbeg.hdr->sh.args, '\n');

                        if (shend == nullptr)
                        {
                                return KERN_FAIL(ENOEXEC);
                        }

                        *shend = '\0';

                        while (shend[-1] == ' ')
                        {
                                shend--;
                                *shend = '\0';
                        }

                        char *interparg, *interpfn = strtok_r(stkbeg.hdr->sh.args, " ", &interparg);

                        if (interpfn == nullptr)
                        {
                                return KERN_FAIL(ENOEXEC);
                        }

                        retval = kstrcpyin(&interpfn, &stkbut.sp, &stkcur.sp);

                        if (KERN_STATUS_FAILURE(retval))
                        {
                                return retval;
                        }

                        *scrargv.spp = interpfn;
                        scrargv.spp++;
                        (*argc.intp)++;

                        if (interparg != nullptr)
                        {
                                interparg += strspn(interparg, " ");

                                retval = kstrcpyin(&interparg, &stkbut.sp, &stkcur.sp);

                                if (KERN_STATUS_FAILURE(retval))
                                {
                                        return retval;
                                }

                                *scrargv.spp = interparg;
                                scrargv.spp++;
                                (*argc.intp)++;
                        }

                        pathlen = (natural_t)(stpcpy(stkbeg.sp, interpfn) - stkbeg.sp);
                }
                else if (IS_ELF(stkbeg.hdr->elf))
                {
                        if (stkbeg.hdr->elf.e_phentsize != (sizeof(*stkbeg.ephdr)))
                        {
                                return KERN_FAIL(ENOEXEC);
                        }

                        // Too many headers, we need both the elf header and program headers
                        natural_t phentsz = stkbeg.hdr->elf.e_phnum * stkbeg.hdr->elf.e_phentsize;
                        if ((phentsz + sizeof((stkbeg.hdr->elf))) > PAGE_SIZE)
                        {
                                return KERN_FAIL(E2BIG);
                        }

                        stack_field_t phdr;
                        phdr.ptr = stkbeg.sp + sizeof((stkbeg.hdr->elf));
                        retval   = vfs_node_read(vfsn, stkbeg.hdr->elf.e_phoff, phdr.ptr, phentsz);

                        if (KERN_STATUS_FAILURE(retval))
                        {
                                return retval;
                        }

                        // ET_DYN is relocatable, so we put in the high end
                        vm_address_t mapaddr = VM_MAX_ADDRESS;
                        if (stkbeg.hdr->elf.e_type == ET_DYN)
                        {
                                /* XXX */
                                if (stkbeg.hdr->elf.e_entry < 0x40000000)
                                {
                                        for (decltype(stkbeg.hdr->elf.e_phnum) phidx = 0; phidx < stkbeg.hdr->elf.e_phnum; ++phidx)
                                        {
                                                if (phdr.ephdr[phidx].p_type == PT_LOAD)
                                                {
                                                        phdr.ephdr[phidx].p_vaddr += 0x40000000;
                                                        phdr.ephdr[phidx].p_paddr = phdr.ephdr[phidx].p_vaddr;
                                                        // Record first loadable section
                                                        if (phdr.ephdr[phidx].p_vaddr < mapaddr)
                                                        {
                                                                mapaddr = phdr.ephdr[phidx].p_vaddr;
                                                        }
                                                }
                                        }

                                        stkbeg.hdr->elf.e_entry += 0x40000000;
                                }
                        }
                        else if (stkbeg.hdr->elf.e_type == ET_EXEC)
                        {
                                for (decltype(stkbeg.hdr->elf.e_phnum) phidx = 0; phidx < stkbeg.hdr->elf.e_phnum; ++phidx)
                                {
                                        if (phdr.ephdr[phidx].p_type == PT_LOAD)
                                        {
                                                // Just grab the first section addr
                                                mapaddr = phdr.ephdr[phidx].p_vaddr;
                                                break;
                                        }
                                }
                        }
                        else
                        {
                                return KERN_FAIL(ENOEXEC);
                        }

                        // Set the ELF aux vectors relevan for this operation
                        if (execop == EXECOP_LOAD_PIMAGE)
                        {
                                eaux.eauxp->at_uid.a_un.a_val    = 0;
                                eaux.eauxp->at_euid.a_un.a_val   = 0;
                                eaux.eauxp->at_gid.a_un.a_val    = 0;
                                eaux.eauxp->at_egid.a_un.a_val   = 0;
                                eaux.eauxp->at_pagesz.a_un.a_val = PAGE_SIZE;
                                eaux.eauxp->at_phdr.a_un.a_val   = mapaddr + stkbeg.hdr->elf.e_phoff;
                                eaux.eauxp->at_phent.a_un.a_val  = stkbeg.hdr->elf.e_phentsize;
                                eaux.eauxp->at_phnum.a_un.a_val  = stkbeg.hdr->elf.e_phnum;
                                eaux.eauxp->at_entry.a_un.a_val  = stkbeg.hdr->elf.e_entry;
                        }
                        else if (execop == EXECOP_LOAD_INTERP)
                        {
                                eaux.eauxp->at_base.a_type     = AT_BASE;
                                eaux.eauxp->at_base.a_un.a_val = mapaddr;
                                execop                         = EXECOP_LOAD_FINISH;
                        }

                        // Use the entry point of whatever loaded last
                        /* XXX */
                        regs[0] = stkbeg.hdr->elf.e_entry;

                        for (Elf64_Half phidx = 0; phidx < stkbeg.hdr->elf.e_phnum; ++phidx)
                        {
                                switch (phdr.ephdr[phidx].p_type)
                                {
                                        case PT_LOAD:
                                        {
                                                /* Loadable segment. */
                                                vm_object_t elf_sec = elf_section_object_create(&phdr.ephdr[phidx], vfsn);
                                                defer(vm_object_rel(
                                                        elf_sec)); // When insterted in the vm map addiotional refrence is gained
                                                KASSERT(elf_sec != nullptr);

                                                vm_address_t secaddr = trunc_page(phdr.ephdr[phidx].p_vaddr);

                                                retval = vm_map_find(vmctx, elf_sec, 0, &secaddr, elf_sec->m_size, false, nullptr);

                                                KASSERT(retval == KERN_SUCCESS);

                                                vm_map_entry_t ent = nullptr;
                                                vm_map_lookup_entry(vmctx, secaddr, &ent);
                                                ent->protection = eprot[phdr.ephdr[phidx].p_flags & 7];
                                                ent->flags |= VM_MAP_ENTRY_NEEDS_COPY;

                                                continue;
                                        }
                                        case PT_GNU_STACK:
                                        {
                                                /* The p_flags member specifies the permissions on the segment
                                                 * containing the stack and is used to indicate wether the stack
                                                 * should be executable. The absense of this header indicates that the
                                                 * stack will be executable. */

                                                stkprot |= eprot[phdr.ephdr[phidx].p_flags & 7];
                                                continue;
                                        }
                                        case PT_INTERP:
                                        {
                                                /* Pathname of interpreter. */
                                                if (phdr.ephdr[phidx].p_filesz > PAGE_SIZE)
                                                {
                                                        return KERN_FAIL(E2BIG);
                                                }

                                                interp_phdr = phdr.ephdr[phidx];
                                                execop      = EXECOP_LOAD_INTERP;
                                                continue;
                                        }

                                        case PT_NULL:
                                        case PT_DYNAMIC:
                                        case PT_PHDR:
                                        case PT_NOTE:
                                        case PT_SHLIB:
                                        case PT_TLS:
                                        case PT_LOOS:
                                        case PT_GNU_EH_FRAME:
                                        case PT_GNU_RELRO:
                                        case PT_LOSUNW:
                                        case PT_SUNWSTACK:
                                        case PT_HIOS:
                                        case PT_LOPROC: /* First processor-specific type. */
                                        case PT_HIPROC: /* Last processor-specific type. */
                                        default:
                                                continue;
                                }
                        }

                        if (execop == EXECOP_LOAD_INTERP)
                        {
                                // We found a interpreter section during image load
                                // Read in the path
                                retval = vfs_node_read(vfsn, interp_phdr.p_offset, stkbeg.ptr, interp_phdr.p_filesz);
                                if (KERN_STATUS_FAILURE(retval))
                                {
                                        return retval;
                                }
                                pathlen = interp_phdr.p_filesz - 1;
                        }
                        else
                        {
                                execop = EXECOP_LOAD_FINISH;
                        }
                }
                else
                {
                        return KERN_FAIL(ENOEXEC);
                }
        }

        if (stkprot == VM_PROT_NONE) [[unlikley]]
        {
                // No stack protection found during mapping, set it to default
                stkprot = VM_PROT_DEFAULT;
        }

        if (scrargv.val < argv.val) [[likeley]]
        {
                // There is a hole between script args and user args.
                // Move the user args down
                size_t vecsz = stkvecend.sp - argv.sp;
                memmove(scrargv.spp, argv.spp, vecsz);
                argv.spp = argc.spp + 1;
                envp.spp = argv.spp + (*argc.intp) + 1;
                if (envc > 0)
                {
                        eaux.ptr = envp.spp + envc + 1;
                }
                else
                {
                        eaux = envp;
                }
                stkvecend.eauxp = eaux.eauxp + 1;
                size_t padsz    = stkstrbeg.val - stkvecend.val;
                etl::memset64(stkvecend.ptr, 0, padsz >> 3);
        }

        // Clear the tmp buffer we used for paths, elf headers etc.
        // It will be used as the top of the stack
        etl::memset64(stkbeg.ptr, 0, PATH_MAX >> 3);

        // Start copying in arg string replacing the old pointer with new ones
        retval = uvecdatacpyin(argv.spp, &stkbut.sp, &stkcur.sp);
        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        // Start copying in env string replacing the old pointer with new ones
        if (envc > 0)
        {
                retval = uvecdatacpyin(envp.spp, &stkbut.sp, &stkcur.sp);
                if (KERN_STATUS_FAILURE(retval))
                {
                        return retval;
                }
        }

        vm_offset_t stksz = stkbut.val - stkbeg.val;

        // Rebase argv to user addresses
        rebaseuvec(&argv.vmap, stkbeg.val, VM_USER_STACK_ADDRESS, stksz);

        // Rebase env to user addresses if needed
        if (envc > 0)
        {
                rebaseuvec(&envp.vmap, stkbeg.val, VM_USER_STACK_ADDRESS, stksz);
        }

        // Deallocate the kernel mapping of user stack object has a extra ref and will survive
        vm_map_remove(kernel_map, stkbeg.val, stkend.val);

        // Remember the  defer(vm_map_remove(kernel_map, stkbeg...... Make sure it fails.
        stkbeg.val = 0;
        stkend.val = 0;

        // Stack grows down, move the pages to high address(on x86 :)
        vm_object_lock(stkobj);
        for (vm_offset_t pgoff = 0; pgoff < stksz; pgoff += PAGE_SIZE)
        {
                vm_page_t pg;
                retval = vm_object_get_page(stkobj, pgoff, stkprot, &pg);
                if (retval != KERN_SUCCESS) [[unlikeley]]
                {
                        // Just in case, should never happen.
                        break;
                }
                vm_page_rename(stkobj->m_page_map, pg, stkobj->m_page_map, (stkobj->m_size - stksz) + pgoff);
        }
        vm_object_unlock(stkobj);

        if (retval != KERN_SUCCESS) [[unlikeley]]
        {
                return KERN_FAIL(ENOMEM);
        }

        // Map the stack object into the new user map and et the Stack pointer
        regs[1] = VM_USER_STACK_ADDRESS - USER_STACK_SIZE;
        retval  = vm_map_find(vmctx, stkobj, 0, &regs[1], USER_STACK_SIZE, false, nullptr);
        if (retval != KERN_SUCCESS)
        {
                return KERN_FAIL(ENOMEM);
        }
        vm_map_entry_t stkvme;
        vm_map_lookup_entry(vmctx, regs[1], &stkvme);
        stkvme->protection = stkprot;

        // We are pointing to the stack to, but we left our tmp buffer page, let the process
        // make use of it
        regs[1] += USER_STACK_SIZE - (stksz - PAGE_SIZE);

        // Replace the old context with the one we built
        retval = utask_replace_vm_context(utask, uthrd, vmctx, true);
        if (KERN_STATUS_FAILURE(retval))
        {
                // All that work wasted
                return retval;
        }

        utask->m_clone_flags &= ~CLONE_VM;

        if (utask->m_clone_flags & CLONE_VFORK)
        {
                utask_release_parent(utask);
                utask->m_clone_flags &= ~CLONE_VFORK;
        }

        utask_exec_done(utask, uthrd, regs, retval);

        return retval;
}

kern_return_t sys_execve(char const *a_filename, char const *const a_argv[], char const *const a_envp[])
{
        return sys_execveat(AT_FDCWD, a_filename, a_argv, a_envp, 0);
}

typedef struct elf_section_vm_obj
{
        vm_object_data m_vmo;
        struct vm_page_map_data m_page_map;
        vm_object_t m_vfsn_vmo;
        Elf64_Phdr m_phdr;
        natural_t m_pg_expect;
} *elf_section_vm_obj_t;

ZONE_DEFINE(elf_section_vm_obj, s_elfsec_vmo_zone, 0x1000, 0x80, ZONE_FIXED, 32);

static int nelfsecs;

// ********************************************************************************************************************************
static kern_return_t elf_section_get_page(vm_object_t a_vm_obj, vm_offset_t a_offset, vm_prot_t a_vm_prot, vm_page_t *a_vmp_out)
// ********************************************************************************************************************************
{
        KASSERT(vm_object_is_locked(a_vm_obj));

        elf_section_vm_obj_t elfsec_vmo = (elf_section_vm_obj_t)(a_vm_obj);

        // Check if we have the page at the given offset
        *a_vmp_out = vm_page_map_lookup(&elfsec_vmo->m_page_map, a_offset);

        // If we do, return it.
        if (*a_vmp_out)
        {
                // We don't set COW on our own pages
                (*a_vmp_out)->flags &= ~PG_COPYONWRITE;
                return KERN_SUCCESS;
        }

        // We don't have the page.

        // Is the offset within the vnode range
        kern_return_t result = KERN_FAILURE;
        Elf64_Phdr *phdr     = &elfsec_vmo->m_phdr;
        if (elfsec_vmo->m_pg_expect > 0)
        {
                if (a_offset < round_page(phdr->p_filesz + (phdr->p_offset & PAGE_MASK)))
                {
                        vm_offset_t copy_offset = 0;

                        if (a_offset == 0)
                        {
                                copy_offset = phdr->p_offset & PAGE_MASK;
                        }

                        vm_offset_t copy_size = etl::min((Elf64_Xword)(PAGE_SIZE)-copy_offset,
                                                         phdr->p_filesz + (phdr->p_offset & PAGE_MASK) - a_offset - copy_offset);

                        vm_object_lock(elfsec_vmo->m_vfsn_vmo);
                        defer(vm_object_unlock(elfsec_vmo->m_vfsn_vmo));

                        result = vm_object_get_page(elfsec_vmo->m_vfsn_vmo,
                                                    trunc_page(phdr->p_offset) + a_offset,
                                                    a_vm_prot,
                                                    a_vmp_out);

                        if (result == KERN_SUCCESS)
                        {
                                if (copy_offset != 0 || copy_size != PAGE_SIZE)
                                {
                                        // Lastly clone the page
                                        vm_page_t vfsn_page = *a_vmp_out;

                                        *a_vmp_out = vm_page_alloc(&elfsec_vmo->m_page_map, a_offset);

                                        KASSERT(*a_vmp_out != vfsn_page);

                                        (*a_vmp_out)->flags &= ~(PG_BUSY | PG_FAKE);

                                        (*a_vmp_out)->zero_fill();

                                        vm_page_copy(*a_vmp_out, vfsn_page, copy_offset, copy_size);

                                        elfsec_vmo->m_pg_expect--;
                                }
                        }
                }
                else
                {
                        KASSERT(a_offset < round_page(elfsec_vmo->m_vmo.m_size));
                        // The offset is not in the file, so we return a zero page and set it to COW.(a_offset is guaranteed to be
                        // within the range of this object)

                        *a_vmp_out = vm_page_alloc(&elfsec_vmo->m_page_map, a_offset);

                        (*a_vmp_out)->zero_fill();

                        (*a_vmp_out)->flags &= ~(PG_BUSY | PG_FAKE);

                        elfsec_vmo->m_pg_expect--;

                        result = KERN_SUCCESS;
                }
        }
        else
        {
                result = vm_object_get_page(elfsec_vmo->m_vfsn_vmo, trunc_page(phdr->p_offset) + a_offset, a_vm_prot, a_vmp_out);

                if (KERN_STATUS_SUCCESS(result) && (a_vm_prot & VM_PROT_WRITE))
                {
                        (*a_vmp_out)->flags |= PG_COPYONWRITE;
                }
        }

        KASSERT(result == KERN_SUCCESS);

        return result;
}

// ********************************************************************************************************************************
static kern_return_t elf_section_inactive(vm_object_t a_vm_obj)
// ********************************************************************************************************************************
{
        elf_section_vm_obj_t elfsec_vmo = (elf_section_vm_obj_t)(a_vm_obj);

        vm_object_rel(elfsec_vmo->m_vfsn_vmo);

        // No more references, deallocate the used pages and destroy the context
        vm_page_map_clear(&elfsec_vmo->m_page_map);

        s_elfsec_vmo_zone.free_delete(elfsec_vmo);

        nelfsecs--;

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static boolean_t elf_section_collapse(vm_object_t a_vm_obj,
                                      vm_object_t a_destination,
                                      vm_object_t *a_backing_object,
                                      vm_offset_t *a_backing_object_offset)
// ********************************************************************************************************************************
{
        elf_section_vm_obj_t elfsec_vmo = (elf_section_vm_obj_t)(a_vm_obj);

        boolean_t can_collapse = a_destination != nullptr && a_destination->m_page_map != nullptr && elfsec_vmo->m_vmo.m_usecnt == 1
                              && 0 == elfsec_vmo->m_pg_expect && a_destination->m_size == elfsec_vmo->m_vmo.m_size;

        if (can_collapse == true)
        {
                // The pages we do not transfer to the designation object will be freed when er use_count drops to zero.
                // The caller has the responsibilty to call unreference on us.
                vm_page_t vmp_next = nullptr;
                for (vm_page_t vmp = vm_page_map_first(&elfsec_vmo->m_page_map); vmp != nullptr; vmp = vmp_next)
                {
                        vmp_next = vm_page_map_next(vmp);
                        vm_page_rename(&elfsec_vmo->m_page_map, vmp, a_destination->m_page_map, vmp->offset);
                }

                if (a_backing_object)
                {
                        KASSERT(a_backing_object_offset != nullptr);
                        vm_object_ref((*a_backing_object = elfsec_vmo->m_vfsn_vmo));
                        *a_backing_object_offset = trunc_page(elfsec_vmo->m_phdr.p_offset);
                }
        }

        return can_collapse;
}

static struct vm_object_operations_data elf_section_vm_obj_ops = {
        //
        .get_page = elf_section_get_page,
        .inactive = elf_section_inactive,
        .collapse = elf_section_collapse,
        //
};

// ********************************************************************************************************************************
vm_object_t elf_section_object_create(Elf64_Phdr *a_ephdr, vfs_node_t a_vfsn)
// ********************************************************************************************************************************
{
        elf_section_vm_obj_t elfsec_vmo = s_elfsec_vmo_zone.alloc();

        nelfsecs++;

        vm_object_t vmo = &elfsec_vmo->m_vmo;

        vm_object_init(vmo);

        vmo->m_ops = &elf_section_vm_obj_ops;

        vm_object_ref(elfsec_vmo->m_vfsn_vmo = &a_vfsn->m_vmo);

        vm_page_map_init((vmo->m_page_map = &elfsec_vmo->m_page_map));

        elfsec_vmo->m_phdr = *a_ephdr;

        vmo->m_size = round_page(a_ephdr->p_memsz + (a_ephdr->p_vaddr & PAGE_MASK));

        elfsec_vmo->m_pg_expect = 0;

        // We need to fix the head
        if (a_ephdr->p_filesz < PAGE_SIZE)
        {
                elfsec_vmo->m_pg_expect += (a_ephdr->p_offset & PAGE_MASK) != 0 || (a_ephdr->p_filesz & PAGE_MASK);
        }
        else
        {
                // We need to fix the tail
                elfsec_vmo->m_pg_expect += (a_ephdr->p_offset & PAGE_MASK) != 0;
                elfsec_vmo->m_pg_expect += (a_ephdr->p_filesz & PAGE_MASK) != 0;
        }

        natural_t anon_size = round_page(vmo->m_size) - round_page(a_ephdr->p_filesz);

        if (anon_size > 0)
        {
                elfsec_vmo->m_pg_expect += (anon_size >> PAGE_SHIFT);
        }

        return vmo;
}
