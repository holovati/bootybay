#include <mach/tmpsys.h>

#include <kernel/thread_data.h>

#include <emerixx/process.h>

extern struct process *PER_TASK_VAR(__s_curproc);

pid_t sys_getppid(pthread_t a_thread)
{
        return (*PER_TASK_PTR(__s_curproc))->m_ppid;
}
