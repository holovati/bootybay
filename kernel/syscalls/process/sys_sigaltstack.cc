#include <mach/tmpsys.h>

#include <kernel/vm/vm_param.h>
#include <emerixx/memory_rw.h>

#include <emerixx/process.h>
// ********************************************************************************************************************************
kern_return_t sys_sigaltstack(pthread_t a_thread, const stack_t *a_ss, stack_t *a_oss)
// ********************************************************************************************************************************
{
        stack_t ss[2];
        kern_return_t retval = KERN_SUCCESS;

        if (a_ss != nullptr)
        {
                retval = emerixx::uio::copyin(a_ss, &ss[0]);
                a_ss   = &ss[0];
        }

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        retval = uthread_signalstack(a_thread, a_ss, &ss[1]);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        if (a_oss != nullptr)
        {

                retval = emerixx::uio::copyout(a_oss, &ss[1]);
        }

        return retval;
#if 0
        kern_return_t retval = KERN_SUCCESS;

        if (a_oss != nullptr)
        {
                retval = emerixx::uio::copyout(&(a_thread->pt_sigaltstack), a_oss);
        }

        if (retval != KERN_SUCCESS)
        {
                return retval;
        }

        if (a_ss != nullptr)
        {
                stack_t ss;

                if (a_thread->pt_sigaltstack.ss_flags & SS_ONSTACK)
                {
                        // An attempt was made to modify an active stack.
                        return KERN_FAIL(EPERM);
                }

                retval = emerixx::uio::copyin(a_ss, &ss);

                if (retval != KERN_SUCCESS)
                {
                        return retval;
                }

                // The ss argument is not a null pointer, and the ss_flags member pointed to by ss contains flags other than
                // SS_DISABLE.
                if ((ss.ss_sp != nullptr) && ((ss.ss_flags & ~SS_DISABLE) != 0))
                {
                        return KERN_FAIL(EINVAL);
                }

                // The size of the alternate stack area is less than MINSIGSTKSZ
                if (ss.ss_size < MINSIGSTKSZ)
                {
                        return KERN_FAIL(ENOMEM);
                }

                // Make sure the new stack is in userspace
                if ((reinterpret_cast<vm_address_t>(ss.ss_sp) + ss.ss_size) >= VM_MAX_ADDRESS)
                {
                        return KERN_FAIL(EINVAL);
                }

                // Only allow modification of SS_DISABLE
                ss.ss_flags = ss.ss_flags & ~SS_DISABLE;

                a_thread->pt_sigaltstack = ss;
        }

        return retval;
#endif
}
