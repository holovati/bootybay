#include <mach/tmpsys.h>

#include <sched.h>

extern kern_return_t linux_clone(
    pthread_t a_thread, natural_t a_flags, vm_address_t a_stack, pid_t *a_parent_tid, pid_t *a_child_tid, vm_address_t a_tls);

kern_return_t sys_fork(pthread_t a_thread) { return linux_clone(a_thread, SIGCHLD, 0, nullptr, nullptr, 0); }

kern_return_t sys_vfork(pthread_t a_thread)
{
        return linux_clone(a_thread, CLONE_VM | CLONE_VFORK | SIGCHLD, 0, nullptr, nullptr, 0);
}
