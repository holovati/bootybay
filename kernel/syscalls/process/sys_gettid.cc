#include <mach/tmpsys.h>

#include <emerixx/process.h>

tid_t sys_gettid(pthread_t a_thread) { return a_thread->m_tid; }