#include <mach/tmpsys.h>

#include <emerixx/memory_rw.h>

#include <emerixx/process.h>

kern_return_t sys_rt_sigprocmask(pthread_t a_thread, int a_how, sigset_t *a_nset, sigset_t *a_oset, size_t a_sigsetsize)
{
        if (a_sigsetsize != sizeof(sigset_t))
        {
                return KERN_FAIL(EINVAL);
        }

        kern_return_t retval = KERN_SUCCESS;

        sigset_t nset, oset;

        if (a_nset != nullptr)
        {
                retval = emerixx::uio::copyin(a_nset, &nset);
                if (retval != KERN_SUCCESS)
                {
                        return retval;
                }

                retval = uthread_sigprocmask(a_thread, a_how, &nset, a_oset ? &oset : nullptr);
        }
        else
        {
                retval = uthread_sigprocmask(a_thread, a_how, nullptr, &oset);
        }

        if (retval != KERN_SUCCESS)
        {
                return retval;
        }

        if (a_oset != nullptr)
        {
                retval = emerixx::uio::copyout(&oset, a_oset);
        }

        return retval;
}
