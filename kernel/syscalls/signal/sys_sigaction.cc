#include <mach/tmpsys.h>
#include <emerixx/memory_rw.h>

#include <emerixx/process.h>

kern_return_t sys_rt_sigaction(int a_signum, ksigaction_t const a_actp, ksigaction_t a_oactp, size_t a_sigsetsize)
{
        if (a_sigsetsize != sizeof(sigset_t))
        {
                return KERN_FAIL(EINVAL);
        }

        kern_return_t retval = KERN_SUCCESS;

        ksigaction actp, oactp;

        if (a_actp != nullptr)
        {
                retval = emerixx::uio::copyin(a_actp, &actp);
                if (retval != KERN_SUCCESS)
                {
                        return retval;
                }

                retval = uthread_sigaction(a_signum, &actp, a_oactp ? &oactp : nullptr);
        }
        else
        {
                retval = uthread_sigaction(a_signum, nullptr, &oactp);
        }

        if (retval != KERN_SUCCESS)
        {
                return retval;
        }

        if (a_oactp != nullptr)
        {
                retval = emerixx::uio::copyout(&oactp, a_oactp);
        }

        return retval;
}
