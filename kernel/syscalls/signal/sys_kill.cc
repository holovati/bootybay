#include <mach/tmpsys.h>

#include <emerixx/process.h>
#include <etl/cstdlib.hh>

kern_return_t sys_kill(pid_t a_pid, int a_signo)
{
        if ((a_signo < 1) || (a_signo > 64))
        {
                return KERN_FAIL(EINVAL);
        }

        if (a_pid > 0)
        {
                process_t p;

                kern_return_t retval = utask_find(a_pid, &p);

                if (KERN_STATUS_SUCCESS(retval))
                {
                        retval = utask_signal(p, a_signo);
                }

                return retval;
        }
        else if (a_pid < 0)
        {
                // "If pid is negative but not (pid_t)-1, sig will be sent to all processes whose process group ID is equal"to the
                // absolute value of pid and for which the process has permission to send a signal."
                a_pid            = etl::abs(a_pid);
                utask_group *grp = utask_group_find(a_pid);

                if (grp == nullptr)
                {
                        return KERN_FAIL(ESRCH);
                }

                return utask_group_signal(grp, a_signo);
        }
        else
        {
                utask_group *grp = utask_group_find(utask_self()->m_pgid);

                if (grp == nullptr)
                {
                        return KERN_FAIL(ESRCH);
                }

                return utask_group_signal(grp, a_signo);
        }
}
