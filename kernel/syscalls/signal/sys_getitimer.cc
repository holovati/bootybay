#include <emerixx/process.h>
#include <mach/tmpsys.h>
#include <sys/time.h>

#include <emerixx/memory_rw.h>

kern_return_t sys_getitimer(int a_which, struct itimerval *a_timer_out)
{
        // Check if the timer is supported.
        switch (a_which)
        {
        case ITIMER_REAL:
        case ITIMER_VIRTUAL:
        case ITIMER_PROF:
                break;
        default:
                return KERN_FAIL(EINVAL);
        }

        if (a_timer_out == nullptr)
        {
                return KERN_FAIL(EINVAL);
        }

        itimerspec timerspec_out;
        itimerval timerval_out;

        utask_getitimer(a_which, &timerspec_out);

        TIMESPEC_TO_TIMEVAL(&timerval_out.it_value, &timerspec_out.it_value);
        TIMESPEC_TO_TIMEVAL(&timerval_out.it_interval, &timerspec_out.it_interval);

        return emerixx::uio::copyout(&timerval_out, a_timer_out);
}
