#include <emerixx/process.h>
#include <mach/tmpsys.h>
#include <emerixx/memory_rw.h>
#include <sys/time.h>
#include <time.h>

// The setitimer() function shall set the timer specified by which to the value specified in the structure pointed to by value, and
// if ovalue is not a null pointer, store the previous value of the timer in the structure pointed to by ovalue.
kern_return_t sys_setitimer(int a_which, struct itimerval *a_timerval_in, struct itimerval *a_timerval_out)
{
        // Check if the timer is supported.
        switch (a_which)
        {
        case ITIMER_REAL:
        case ITIMER_VIRTUAL:
        case ITIMER_PROF:
                break;
        default:
                return KERN_FAIL(EINVAL);
        }

        if (a_timerval_in == nullptr)
        {
                return KERN_FAIL(EINVAL);
        }

        itimerval timerval_in;
        itimerval timerval_out;
        itimerspec timerspec_in;
        itimerspec timerspec_out;

        kern_return_t retval = emerixx::uio::copyin(a_timerval_in, &timerval_in);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        // Check if timer_in is in canonical form
        // (In canonical form, the number of microseconds is a non-negative integer less than 1000000 and the number of seconds is a
        // non-negative integer.)
        if ((timerval_in.it_value.tv_usec < 0) || (timerval_in.it_value.tv_usec > 1000000) || (timerval_in.it_value.tv_sec < 0))
        {
                return KERN_FAIL(EINVAL);
        }

        if ((timerval_in.it_interval.tv_usec < 0) || (timerval_in.it_interval.tv_usec > 1000000)
            || (timerval_in.it_interval.tv_sec < 0))
        {
                return KERN_FAIL(EINVAL);
        }

        TIMEVAL_TO_TIMESPEC(&timerval_in.it_value, &timerspec_in.it_value);
        TIMEVAL_TO_TIMESPEC(&timerval_in.it_interval, &timerspec_in.it_interval);

        retval = utask_setitimer(a_which, &timerspec_in, a_timerval_out != nullptr ? &timerspec_out : nullptr);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        if (a_timerval_out != nullptr)
        {
                TIMESPEC_TO_TIMEVAL(&timerval_out.it_value, &timerspec_out.it_value);
                TIMESPEC_TO_TIMEVAL(&timerval_out.it_interval, &timerspec_out.it_interval);
                retval = emerixx::uio::copyout(&timerval_out, a_timerval_out);
        }

        return retval;
}