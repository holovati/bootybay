#include <mach/tmpsys.h>

#include <emerixx/process.h>

#include <aux/defer.hh>
#include <mach/machine/fpu.h>
#include <mach/param.h>
#include <kernel/thread_data.h>
#include <emerixx/memory_rw.h>
#include <signal.h>

#include <strings.h>

typedef struct kucontext
{
        unsigned long uc_flags;
        struct __ucontext *uc_link;
        stack_t uc_stack;
        struct sigcontext uc_mcontext;
        sigset_t uc_sigmask;
} kucontext_t;

typedef struct rt_sigframe
{
        void (*restorer)(void);
        kucontext_t uc;
        siginfo_t info;
        struct _fpstate fpstate;
} *rt_sigframe_t;

kern_return_t sys_rt_sigreturn(pthread_t a_pthread)
{
        spl_t s = splhigh();
        defer(splx(s));
        using namespace emerixx::uio;
        pcb_t pcbp = current_thread()->pcb; // a_pthread->pt_kthread->pcb;

        // Grab the user stack pointer
        vm_address_t ursp = pcbp->iss.return_rsp;

        // Compensate rsp for the ret the signal handler executed
        ursp -= 8;

        // Grab a pointer to the signal frame created above
        rt_sigframe_t sigframe = (rt_sigframe_t)(ursp);

        // If we are on alt stack and returning from the first frame clear the SS_ONSTACK flag
        stack_t ss;
        uthread_signalstack(a_pthread, nullptr, &ss);

        if ((ss.ss_flags & SS_ONSTACK)
            && (reinterpret_cast<vm_address_t>(sigframe) == (reinterpret_cast<vm_address_t>(ss.ss_sp) + ss.ss_size)))
        {
                KASSERT((ss.ss_flags & SS_DISABLE) == 0);
                ss.ss_flags &= ~SS_ONSTACK;
                uthread_signalstack(a_pthread, &ss, nullptr);
        }

        // Restore the previous thread context(user space might have modified it)
        // We don't accept modifications cs
        {
                struct sigcontext sigctx;

                int error = emerixx::uio::copyin(&sigframe->uc.uc_mcontext, &sigctx);

                if (error)
                {
                        panic("SEGFAULT here");
                }

                pcbp->iss.cr2           = sigctx.cr2;
                pcbp->iss.rax           = sigctx.rax;
                pcbp->iss.rbx           = sigctx.rbx;
                pcbp->iss.rcx           = sigctx.rcx;
                pcbp->iss.rdx           = sigctx.rdx;
                pcbp->iss.rsi           = sigctx.rsi;
                pcbp->iss.rdi           = sigctx.rdi;
                pcbp->iss.rbp           = sigctx.rbp;
                pcbp->iss.r8            = sigctx.r8;
                pcbp->iss.r9            = sigctx.r9;
                pcbp->iss.r10           = sigctx.r10;
                pcbp->iss.r11           = sigctx.r11;
                pcbp->iss.r12           = sigctx.r12;
                pcbp->iss.r13           = sigctx.r13;
                pcbp->iss.r14           = sigctx.r14;
                pcbp->iss.r15           = sigctx.r15;
                pcbp->iss.trapnum       = sigctx.trapno;
                pcbp->iss.error_code    = sigctx.err;
                pcbp->iss.return_rip    = sigctx.rip;
                pcbp->iss.return_rflags = sigctx.eflags;
                pcbp->iss.return_rsp    = sigctx.rsp;
        }

        sigset_t sigmask = 0;
        if (emerixx::uio::copyin(&sigframe->uc.uc_sigmask, &sigmask))
        {
                panic("SEGFAULT here");
        }

        int signum = 0;
        {
                siginfo_t info;

                if (emerixx::uio::copyin(&sigframe->info, &info))
                {
                        panic("SEGFAULT here");
                }

                signum = info.si_signo;
        }

        {
                // FPU thing is a hack, se above
                if (emerixx::uio::copyin(&sigframe->fpstate, (struct _fpstate *)&pcbp->ifps))
                {
                        panic("SEGFAULT here");
                }

                // Load our old state(only if we are the owner of the FPU)
                thread_load_fpu(/*a_pthread->pt_kthread*/ current_thread());
        }

        // pcbp->iss.rax = a_pthread->process->on_signal_return(signum, sigmask, pcbp->iss.rax);

        uthread_sigprocmask(a_pthread, SIG_SETMASK, &sigmask, nullptr);
        // Sanity check( Is NSIG - 1???? )
        if (signum <= 0 || signum >= 64)
        {
                return -EINTR;
        }

        ksigaction sa; // = &a_pthread->pt_sighand->sh_sigaction[signum];

        uthread_sigaction(signum, nullptr, &sa);

        switch (pcbp->iss.rax)
        {
                case -ERESTART:
                case -EINTR:
                case -ERESTARTSYS:
                        /*
                        -ERESTARTSYS is interpreted as "shift the instruction pointer
                        back to syscall instruction" if SA_RESTART had been set when
                        we'd installed the handler and turned into -EINTR otherwise
                        */

                        if (sa.flags & SA_RESTART)
                        {
                                pcbp->iss.rax = -ERESTART;
                        }
                        else
                        {
                                pcbp->iss.rax = -EINTR;
                        }

                        break;
                case -ERESTARTNOINTR:
                {
                        /*
                        -ERESTARTNOINTR is unconditional "shift back" (fork(2) et.al.)
                         */
                        pcbp->iss.rax = -ERESTART;
                        break;
                }

                case -ERESTARTNOHAND:
                        /*
                        -ERESTARTNOHAND is "shift back" if no handler is set, -EINTR
                        otherwise (sigsuspend(2))
                        */
                        pcbp->iss.rax = -EINTR;
                        break;

                case -ERESTART_RESTARTBLOCK:
                        /*
                        -ERESTART_RESTARTBLOCK is -EINTR if there's a handler and
                        a horrible pile of hacks otherwise (timeout-related ones).
                        */
                        pcbp->iss.rax = -EINTR;
                        break;
                default:
                        break;
        }

        // If the previous syscall wants to be restarted decrement the ip by 2(size of
        // syscall opcode) and move the syscall number from error_code to rax.
        if (pcbp->iss.rax == static_cast<natural_t>(KERN_FAIL(ERESTART)))
        {
                pcbp->iss.return_rip -= 2;
                pcbp->iss.rax = pcbp->iss.error_code;
        }

        // There is a high propability that we will return with sysret, modify rcx
        // accordinly
        // pcbp->iss.rcx = pcbp->iss.return_rip; // Can't modify userspace without being SURE that sysenter and sysret are used

        // Syscall dispatch code overwrites rax with the return value
        return static_cast<kern_return_t>(pcbp->iss.rax);
}
