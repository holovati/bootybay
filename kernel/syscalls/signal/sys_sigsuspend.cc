#include <emerixx/process.h>
#include <mach/tmpsys.h>
#include <emerixx/memory_rw.h>

kern_return_t sys_rt_sigsuspend(sigset_t *a_sigset, size_t a_sigset_size)
{
        if (sizeof(sigset_t) != a_sigset_size)
        {
                return KERN_FAIL(EINVAL);
        }

        sigset_t ss;
        kern_return_t retval = emerixx::uio::copyin(a_sigset, &ss);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        return uthread_sigsuspend(&ss);
}