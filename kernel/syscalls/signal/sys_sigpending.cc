#include <emerixx/process.h>
#include <mach/tmpsys.h>
#include <emerixx/memory_rw.h>

kern_return_t sys_rt_sigpending(sigset_t *a_sigset, size_t a_sigset_size)
{
        if (a_sigset_size != sizeof(sigset_t))
        {
                return KERN_FAIL(EINVAL);
        }

        sigset_t sigpend;

        uthread_sigpending(&sigpend);

        return emerixx::uio::copyout(&sigpend, a_sigset);
}
