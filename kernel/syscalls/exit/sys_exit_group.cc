#include <emerixx/process.h>
#include <mach/tmpsys.h>

kern_return_t sys_exit_group(int a_exit_code)
{
        return process_exit(a_exit_code);
}
