#include <emerixx/process.h>
#include <mach/tmpsys.h>

kern_return_t sys_exit(pthread_t a_thread, int a_exit_code) { return uthread_exit(a_thread, a_exit_code); }
