#include <mach/tmpsys.h>

#include <limits.h>
#include <sys/resource.h>

#include <emerixx/vfs/vfs_file.h>
#include <kernel/vm/vm_param.h>
#include <emerixx/memory_rw.h>
#include <emerixx/vfs/vfs_file_data.h>

#include <emerixx/process.h>

#define PTHREAD_MAX 0x1000

int thread_local karl_jensem;

int thread_local karl_jensem_nonbss = 1;

kern_return_t sys_prlimit64(pthread_t a_thread,
                            pid_t a_pid,
                            unsigned int a_resource,
                            const struct rlimit64 *a_new_rlim,
                            struct rlimit64 *a_old_rlim)
{
        karl_jensem = 3;
        // karl_jensem_nonbss = 6;

        int _karl_jensem = karl_jensem + 1;

        int _karl_jensem_nonbss = karl_jensem_nonbss + 1;

        karl_jensem_nonbss = karl_jensem;

        _karl_jensem_nonbss = karl_jensem_nonbss;

        if (a_new_rlim)
        {
                log_warn("Not ready");
                return 0;
        }
        ////////

        struct rlimit64 olim;

        int err = -EINVAL;

        process_t p = nullptr; // uthread_to_utask(a_thread);

        if ((a_pid == 0) /* || (p->m_pid != a_pid)*/)
        {
                // p = a_thread->pt_process;
        }
        else
        {
                err = utask_find(a_pid, &p);
                if (KERN_STATUS_FAILURE(err))
                {
                        return err;
                }
        }

        switch (a_resource)
        {
                case RLIMIT_NPROC:
                {
                        olim.rlim_cur = (PTHREAD_MAX / 2);
                        olim.rlim_max = PTHREAD_MAX;
                        err           = 0;
                        break;
                }

                case RLIMIT_NOFILE:
                {
                        olim.rlim_cur = FILEMAX;
                        olim.rlim_max = FILEMAX;
                        err           = 0;
                        break;
                }

                case RLIMIT_CORE:
                {
                        olim.rlim_cur = RLIM64_INFINITY;
                        olim.rlim_max = RLIM64_INFINITY;
                        err           = 0;
                        break;
                }

                case RLIMIT_AS:
                {
                        olim.rlim_cur = VM_MAX_ADDRESS;
                        olim.rlim_max = VM_MAX_ADDRESS;
                        err           = 0;
                        break;
                }

                case RLIMIT_STACK:
                {
                        olim.rlim_cur = USER_STACK_SIZE;
                        olim.rlim_max = USER_STACK_SIZE;
                        err           = 0;
                        break;
                }

                case RLIMIT_RSS:
                {
                        olim.rlim_cur = 400;
                        olim.rlim_max = VM_MAX_ADDRESS;
                        err           = 0;
                        break;
                }

                default:
                        return -EINVAL;
                        break;
        }

        if (!err)
        {
                err = copyout(&olim, a_old_rlim, sizeof(struct rlimit64));
        }

        return err;
}
