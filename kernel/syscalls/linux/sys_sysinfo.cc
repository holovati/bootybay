#include <mach/tmpsys.h>

#include <kernel/vm/vm_param.h>
#include <emerixx/memory_rw.h>

#include <sys/sysinfo.h>

extern natural_t elapsed_ticks; /* number of ticks elapsed since bootup */
extern int hz;                  /* number of ticks per second */

int sys_sysinfo(struct sysinfo *a_sysinfo)
{
        struct sysinfo sinfo;

        /* Seconds since boot */
        sinfo.uptime = elapsed_ticks / hz;

        /* 1, 5, and 15 minute load averages */
        sinfo.loads[0] = 1;
        sinfo.loads[1] = 1;
        sinfo.loads[2] = 1;

        /* Available memory size */
        sinfo.freeram = 300000;

        /* Amount of shared memory */
        sinfo.sharedram = 0;

        /* Memory used by buffers */
        sinfo.bufferram = 0;

        /* Total swap space size */
        sinfo.totalswap = 0;

        /* Swap space still available */
        sinfo.freeswap = 0;

        /* Number of current processes */
        sinfo.procs = 300; // nprocs;

        /* Total high memory size */
        sinfo.totalhigh = 20000;

        /* Available high memory size */
        sinfo.freehigh = 20000;

        /* Memory unit size in bytes */
        sinfo.mem_unit = PAGE_SIZE;

        // bzero(&sinfo._f[0], sizeof(sinfo._f));

        return emerixx::uio::copyout(&sinfo, a_sysinfo);
}
