#include <mach/tmpsys.h>

#include <fcntl.h>

#include <emerixx/vfs.h>
#include <emerixx/fdesc.hh>
#include <emerixx/process.h>

// ********************************************************************************************************************************
kern_return_t sys_dup3(pthread_t a_thread, int a_oldfd, int a_newfd, int a_flags)
// ********************************************************************************************************************************
{
        if (((a_flags & ~O_CLOEXEC) != 0) || (a_oldfd == a_newfd))
        {
                return KERN_FAIL(EINVAL);
        }

        return fd_context_dup3(a_thread->m_fdctx, a_oldfd, a_newfd, a_flags);
}
