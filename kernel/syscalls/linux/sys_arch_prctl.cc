#include <mach/tmpsys.h>

#include <kernel/vm/vm_param.h>
#include <kernel/thread_data.h>

#include <emerixx/process.h>

/* User for prctl syscall */
#define ARCH_SET_GS 0x1001
#define ARCH_SET_FS 0x1002
#define ARCH_GET_FS 0x1003
#define ARCH_GET_GS 0x1004

#define ARCH_GET_CPUID 0x1011
#define ARCH_SET_CPUID 0x1012

#define ARCH_MAP_VDSO_64 0x2003

kern_return_t sys_arch_prctl(pthread_t a_pthread, int a_code, vm_address_t a_addr)
{
        // process_t p = process_find(a_pthread);

        // thread_t t = (thread_t)queue_first(&p->p_ktask->thread_list);

        struct amd64_saved_state *ustatep = &(current_thread()->pcb->iss);

        kern_return_t retval = KERN_SUCCESS;

        switch (a_code)
        {
                case ARCH_SET_GS:
                        panic("NI\n");
                        /* code */
                        break;

                case ARCH_SET_FS:
                {
                        if (a_addr < VM_MAX_ADDRESS)
                        {
                                ustatep->fsbase = a_addr;
                        }
                        else
                        {
                                retval = KERN_FAIL(EFAULT);
                        }
                        break;
                }
                case ARCH_GET_FS:
                        panic("NI\n");
                        /* code */
                        break;

                case ARCH_GET_CPUID:
                        panic("NI\n");
                        /* code */
                        break;

                case ARCH_SET_CPUID:
                        panic("NI\n");
                        /* code */
                        break;

                case ARCH_MAP_VDSO_64:
                        panic("NI\n");
                        /* code */
                        break;

                default:
                        retval = KERN_FAIL(EINVAL);
                        break;
        }

        return retval;
}
