#include <mach/tmpsys.h>

#include <aux/defer.hh>

#include <emerixx/vfs.h>
#include <emerixx/fdesc.hh>
#include <emerixx/process.h>
#include <emerixx/memory_rw.h>

ssize_t sys_splice(int a_fd_in, off_t *a_off_in, int a_fd_out, off_t *a_off_out, size_t a_len, unsigned a_flags)
{
        pthread_t uthrd = uthread_self();

        off_t offin = 0, offout = 0;
        vfs_file_t fin, fout;

        kern_return_t retval = fd_context_get_file(uthrd->m_fdctx, a_fd_in, &fin);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }
        defer(vfs_file_rel(fin));

        retval = fd_context_get_file(uthrd->m_fdctx, a_fd_out, &fout);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }
        defer(vfs_file_rel(fout));

        if (a_off_in != nullptr)
        {
                retval = emerixx::uio::copyin(a_off_in, &offin);

                if (KERN_STATUS_FAILURE(retval))
                {
                        return retval;
                }
        } 
        
        if (a_off_out != nullptr)
        {
                retval = emerixx::uio::copyin(a_off_out, &offout);
                if (KERN_STATUS_FAILURE(retval))
                {
                        return retval;
                }
        }
        
        return vfs_file_splice(fin, a_off_in ? &offin : nullptr, fout, a_off_out ? &offout : nullptr, a_len, a_flags);
}