#include <mach/tmpsys.h>
#include <emerixx/vfs/vfs_file.h>
#include <emerixx/fdesc.hh>
#include <emerixx/process.h>
#include <sys/file.h>

// ********************************************************************************************************************************
kern_return_t sys_flock(pthread_t a_thread, int a_fd, int a_operation)
// ********************************************************************************************************************************
{
        vfs_file_t f = nullptr;

        kern_return_t retval = fd_context_get_file(a_thread->m_fdctx, a_fd, &f);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        a_operation &= (LOCK_SH | LOCK_EX | LOCK_SH | LOCK_NB);

        if (a_operation & LOCK_UN && (a_operation != LOCK_UN))
        {
                // LOCK_UN is mutually exclusive with the other flags.
                retval = KERN_FAIL(EINVAL);
                goto out;
        }

        if ((a_operation & (LOCK_SH | LOCK_EX)) == (LOCK_SH | LOCK_EX))
        {
                // Cant have both exclusive and shared at the same time
                retval = KERN_FAIL(EINVAL);
                goto out;
        }

        retval = vfs_file_lock(f, a_operation);

out:
        vfs_file_rel(f);
        return retval;
}
