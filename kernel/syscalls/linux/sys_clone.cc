#include <mach/tmpsys.h>

#include <emerixx/memory_rw.h>

#include <emerixx/process.h>

extern kern_return_t linux_clone(
    pthread_t a_thread, natural_t a_flags, vm_address_t a_stack, pid_t *a_parent_tid, pid_t *a_child_tid, vm_address_t a_tls);

kern_return_t sys_clone(
    pthread_t a_thread, natural_t a_flags, vm_address_t a_stack, pid_t *a_parent_tid, pid_t *a_child_tid, vm_address_t a_tls)
{
        pid_t __parent_tid, *parent_tid = &__parent_tid;

        if (a_parent_tid == nullptr)
        {
                parent_tid = nullptr;
        }

        kern_return_t retval = linux_clone(a_thread, a_flags, a_stack, parent_tid, a_child_tid, a_tls);

        if (retval == KERN_SUCCESS)
        {
                retval = emerixx::uio::copyout(parent_tid, a_parent_tid);
        }

        return retval;
}
