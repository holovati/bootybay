#include <emerixx/process.h>
#include <mach/tmpsys.h>

tid_t sys_set_tid_address(pthread_t a_thread, pid_t *a_tid_address)
{
        a_thread->m_tid_address = a_tid_address;
        return a_thread->m_tid;
}