#include <mach/tmpsys.h>

#include <kernel/thread_data.h>
#include <emerixx/memory_rw.h>

#include "emerixx/sleep.h"
#include <emerixx/process.h>

#define FUTEX_WAIT        0
#define FUTEX_WAKE        1
#define FUTEX_FD          2
#define FUTEX_REQUEUE     3
#define FUTEX_CMP_REQUEUE 4
#define FUTEX_WAKE_OP     5
#define FUTEX_LOCK_PI     6
#define FUTEX_UNLOCK_PI   7
#define FUTEX_TRYLOCK_PI  8
#define FUTEX_WAIT_BITSET 9

#define FUTEX_PRIVATE 128

#define FUTEX_CLOCK_REALTIME 256

kern_return_t sys_futex(uint32_t *a_uaddr,
                        int a_futex_op,
                        uint32_t a_val,
                        const struct timespec *timeout, /* or: uint32_t val2 */
                        uint32_t *uaddr2 [[maybe_unused]],
                        uint32_t val3 [[maybe_unused]])
{
        kern_return_t retval;

        // The only works with a non preemptive and non smp kernel

        switch (a_futex_op)
        {
                case FUTEX_WAIT | FUTEX_PRIVATE:
                        uint32_t val;
                        retval = emerixx::uio::copyin(a_uaddr, &val);

                        if (retval != KERN_SUCCESS)
                        {
                                return retval;
                        }

                        if (val != a_val)
                        {
                                retval = KERN_FAIL(EWOULDBLOCK);
                        }
                        else
                        {
                                KASSERT(timeout == nullptr);
                                retval = uthread_sleep(a_uaddr);
                        }

                        break;

                case FUTEX_WAKE | FUTEX_PRIVATE:
                        /* code */
                        retval = thread_wakeup_prim(a_uaddr, a_val, THREAD_AWAKENED, true);
                        break;

                default:
                        retval = KERN_FAIL(ENOSYS);
                        break;
        }

        return retval;
}
