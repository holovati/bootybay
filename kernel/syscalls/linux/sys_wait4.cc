#include <mach/tmpsys.h>

#include <limits.h>
#include <sys/wait.h>

#include <etl/cstdlib.hh>

#include <kernel/thread_data.h>

#include <emerixx/process.h>
#include <emerixx/memory_rw.h>

extern struct process_internal *PER_TASK_VAR(__s_curproc);

pid_t sys_wait4(pid_t a_pid, int *a_stat_addr, int a_options, struct rusage *a_usage)
{
        if (a_options & ~(WUNTRACED | WCONTINUED | WNOHANG))
        {
                return KERN_FAIL(EINVAL);
        }

        if (a_pid == INT_MIN)
        {
                return KERN_FAIL(ESRCH);
        }

        kern_return_t retval;
        int stat = 0, *pstat = a_stat_addr ? &stat : nullptr;
        rusage ru = {}, *pru = a_usage ? &ru : nullptr;
        process_t p = (process_t)*PER_TASK_PTR(__s_curproc);

        if (a_pid < -1)
        {
                // Wait for any child process whose process group ID is equal to the absolute value of pid.
                retval = utask_wait_group_child(etl::abs(a_pid), pstat, a_options, pru);

                // retval = wait_grpid(p, std::abs(a_pid), a_options, &waitedp);
        }
        else if (a_pid == -1)
        {
                // Wait for any child process.
                retval = utask_wait_child(0, pstat, a_options, pru);
        }
        else if (a_pid == 0)
        {
                // Wait for any child process whose process group ID is equal to that of the calling process at the time of
                // the call.
                retval = utask_wait_group_child(p->m_pgid, pstat, a_options, pru);

                // retval = wait_grpid(p, p->m_pgid, a_options, &waitedp);
        }
        else if (a_pid > 0)
        {
                // Wait for the child whose process ID is equal to the value of pid.
                retval = utask_wait_child(a_pid, pstat, a_options, pru);
                // retval = wait_tid(p, a_pid, a_options, &waitedp);
        }
        else
        {
                retval = KERN_FAIL(EINVAL);
        }

        if (KERN_STATUS_SUCCESS(retval))
        {
                if (a_stat_addr)
                {
                        emerixx::uio::copyout(pstat, a_stat_addr);
                }

                if (a_usage)
                {
                        emerixx::uio::copyout(pru, a_usage);
                }
        }

        return retval;
}
