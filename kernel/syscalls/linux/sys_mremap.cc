#include <mach/tmpsys.h>

#include <kernel/vm/vm_map.h>
#include <kernel/vm/vm_page.h>
#include <kernel/vm/vm_object.h>
#include <kernel/thread_data.h>

#include <aux/defer.hh>
#include <mach/param.h>
#include <sys/mman.h>

#include <emerixx/process.h>

// Added in linux 5.7
#ifndef MREMAP_DONTUNMAP
#define MREMAP_DONTUNMAP 4
#endif

#define MREMAP_NOFLAGS 0

vm_address_t sys_mremap(pthread_t a_thread,
                        vm_address_t a_old_address,
                        size_t a_old_size,
                        size_t a_new_size,
                        natural_t a_flags,
                        vm_address_t a_new_address /* Only used with MREMAP_FIXED */)
{
        if (IS_ALIGNED(a_old_address, PAGE_SIZE) == false)
        {
                return KERN_FAIL(EINVAL);
        }

        if (a_new_size == 0)
        {
                return KERN_FAIL(EINVAL);
        }

        if (a_old_size == a_new_size)
        {
                return a_old_address;
        }

        kern_return_t retval = KERN_FAIL(EINVAL);

        vm_map_t vmm = current_task()->map;

        vm_map_lock(vmm);
        defer(vm_map_unlock(vmm));

        vm_map_entry_t vmme;

        if (vm_map_lookup_entry(vmm, a_old_address, &vmme) == false)
        {
                return KERN_FAIL(EFAULT);
        }

        if (((a_old_address + a_old_size) < vmme->links.start) || ((a_old_address + a_old_size) > vmme->links.end))
        {
                // Stay within the same entry
                return KERN_FAIL(EINVAL);
        }

        vm_object_t obj            = vmme->object.vm_object;
        vm_offset_t objoff         = vmme->offset;
        integer_t old_ent_flags    = vmme->flags;
        vm_prot_t old_ent_prot     = vmme->protection;
        vm_prot_t old_ent_max_prot = vmme->max_protection;

        // Make sure the object is locked throughout this whole process
        vm_object_lock(obj);
        defer(vm_object_unlock(obj));

        // Modifications to the vm_map entries might cause the objects ref counter to drop to zero causing a deallocation.
        // We bump to refcounter to be safe.
        vm_object_ref(obj);
        defer(vm_object_rel(obj));

        // TODO:(What about shrinking?)

        switch (a_flags)
        {
                case MREMAP_NOFLAGS:
                {
                        if (a_old_size == 0)
                        {
                                // Old size of zero requires MREMAP_MAYMOVE
                                retval = KERN_FAIL(EINVAL);
                                break;
                        }
                        KASSERT("Not ready");
                        break;
                }

                case MREMAP_MAYMOVE:
                {
                        // Things that don't work
                        if (!((obj->m_usecnt == 2) && (vmme->flags == 0)
                              && ((vmme->__pad == 0) || (vmme->__pad & MAP_ANONYMOUS) != 0) && (objoff == 0)))
                        {
                                panic("Look at this");
                        }

                        // Since a_new_address is only used with MREMAP_FIXED we are free to use it here for other purposes
                        a_new_address = a_old_address;

                        // Before doing anything find sufficient space for the new mapping
                        retval = vm_map_findspace(vmm, a_new_address, a_new_size, &a_new_address);

                        if (retval != KERN_SUCCESS)
                        {
                                retval = KERN_FAIL(ENOMEM);
                                break;
                        }

                        // Now that we are sure to be able to map the new region remove the old one
                        vm_map_delete(vmm, a_old_address, a_old_address + a_old_size);

                        // One of the inserts below must succeed. Bump the refcounter.
                        // vm_object_ref(obj); vm_map_insert does it!

                        // Try to insert the new entry at the same space as the old one.
                        // If there is no space for the new entry then use the address we found before.
                        retval = vm_map_insert(vmm, obj, objoff, a_old_address, a_old_address + a_new_size, nullptr);

                        if (retval == KERN_SUCCESS)
                        {
                                a_new_address = a_old_address;
                        }
                        else
                        {
                                retval = vm_map_insert(vmm, obj, objoff, a_new_address, a_new_address + a_new_size, nullptr);
                                KASSERT(retval == KERN_SUCCESS);
                        }

                        // Update flags and protection for the new entry
                        KASSERT(vm_map_lookup_entry(vmm, a_new_address, &vmme) == true);

                        vmme->protection     = old_ent_prot;
                        vmme->max_protection = old_ent_max_prot;
                        vmme->flags          = (u32)old_ent_flags;

                        if (a_new_size < a_old_size)
                        {
                                pmap_remove(vmm->pmap, vmme->offset + a_new_size, vmme->offset + a_old_size);
                                vm_page_map_remove_range(vmme->object.vm_object->m_page_map,
                                                         vmme->offset + a_new_size,
                                                         vmme->offset + a_old_size);
                        }
                        else
                        {
                                vm_object_set_size(obj, a_new_size);
                        }

                        obj->m_size = a_new_size;

                        break;
                }

                case MREMAP_MAYMOVE | MREMAP_FIXED:
                {
                        KASSERT("Not ready");

                        break;
                }

                case MREMAP_MAYMOVE | MREMAP_DONTUNMAP:
                {
                        // if MREMAP_DONTUNMAP is specified new_size and old_size must be equal.
                        if (a_old_size != a_new_size)
                        {
                                retval = KERN_FAIL(EINVAL);
                        }
                        KASSERT("Not ready");

                        break;
                }

                case MREMAP_MAYMOVE | MREMAP_FIXED | MREMAP_DONTUNMAP:
                {
                        // if MREMAP_DONTUNMAP is specified new_size and old_size must be equal.
                        if (a_old_size != a_new_size)
                        {
                                retval = KERN_FAIL(EINVAL);
                        }
                        KASSERT("Not ready");

                        break;
                }

                // Illegal flag combinations
                case MREMAP_FIXED | MREMAP_DONTUNMAP:
                        // If MREMAP_FIXED is specified, then MREMAP_MAYMOVE must also be specified.
                case MREMAP_DONTUNMAP:
                        // MREMAP_DONTUNMAP must be used in conjunction with MREMAP_MAYMOVE
                case MREMAP_FIXED:
                        // If MREMAP_FIXED is specified, then MREMAP_MAYMOVE must also be specified.
                default:
                        retval = KERN_FAIL(EINVAL);
                        break;
        }

        if (retval != KERN_SUCCESS)
        {
                a_new_address = retval;
        }

        return a_new_address;
}
