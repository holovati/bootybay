#include <emerixx/fdesc.hh>
#include <emerixx/network.h>
#include <emerixx/process.h>
#include <emerixx/sleep.h>
#include <kernel/vm/vm_kern.h>
#include <kernel/zalloc.h>
#include <mach/tmpsys.h>
#include <emerixx/memory_rw.h>
#include <emerixx/vfs/vfs_file.h>

#include <etl/algorithm.hh>

#include <aux/defer.hh>

#include <fcntl.h>

static kern_return_t sockaddr_copyin(sockaddr const *a_addr, socklen_t a_addrlen, sockaddr_storage *a_sau)
{
        if ((unsigned)a_addrlen > sizeof(struct sockaddr_storage))
        {
                return KERN_FAIL(EINVAL);
        }

        return copyin(a_addr, a_sau, a_addrlen);
}

kern_return_t sys_socket(int a_family, int a_type, int a_proto)
{
        int oflags = O_RDWR;

        if (a_type & SOCK_NONBLOCK)
        {
                oflags |= O_NONBLOCK;
        }

        if (a_type & SOCK_CLOEXEC)
        {
                oflags |= O_CLOEXEC;
        }

        kern_return_t retval = network_socket_create(a_family, a_type, a_proto, oflags);

        return retval;
}

#define GET_SOCKET_FILE(fd, f) fd_context_get_file(uthread_self()->m_fdctx, fd, f);

kern_return_t sys_connect(int a_fd, sockaddr const *a_sa, socklen_t a_sa_len)
{
        vfs_file_t f;

        kern_return_t retval = GET_SOCKET_FILE(a_fd, &f);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }
        defer(vfs_file_rel(f));

        sockaddr_storage ss = {};

        retval = sockaddr_copyin(a_sa, a_sa_len, &ss);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        retval = network_socket_connect(f, (sockaddr *)&ss, a_sa_len);

        return retval;
}

kern_return_t sys_accept4(int a_fd, sockaddr *a_peer, socklen_t *a_peer_addrlen, int a_flags)
{
        sockaddr_storage peer  = {};
        socklen_t peer_addrlen = 0;
        kern_return_t retval;

        if (a_peer_addrlen != nullptr)
        {
                retval = emerixx::uio::copyin(a_peer_addrlen, &peer_addrlen);

                if (KERN_STATUS_FAILURE(retval))
                {
                        return retval;
                }
        }

        if ((a_peer_addrlen != nullptr) && (peer_addrlen == 0))
        {
                return KERN_FAIL(EINVAL);
        }

        int oflags = O_RDWR;

        if ((a_flags & SOCK_NONBLOCK))
        {
                oflags |= O_NONBLOCK;
        }

        if (a_flags & SOCK_CLOEXEC)
        {
                oflags |= O_CLOEXEC;
        }

        vfs_file_t f;
        retval = GET_SOCKET_FILE(a_fd, &f);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }
        defer(vfs_file_rel(f));

        retval = network_socket_accept(f, (sockaddr *)&peer, &peer_addrlen, oflags);

        if (KERN_STATUS_SUCCESS(retval) && ((a_peer != nullptr) && (peer_addrlen > 0)))
        {
                emerixx::uio::copyout(peer_addrlen, a_peer_addrlen);
                copyout(&peer, a_peer, peer_addrlen);
        }

        return retval;
}

kern_return_t sys_accept(int a_fd, sockaddr *a_peer, socklen_t *a_peer_addrlen)
{
        return sys_accept4(a_fd, a_peer, a_peer_addrlen, 0);
}

kern_return_t sys_sendto(int a_fd, void *a_buffer, size_t a_buffer_len, int a_flags, sockaddr const *a_addr, socklen_t a_addrlen)
{
        iovec io_vector{ .iov_base = a_buffer, .iov_len = a_buffer_len };

        if (io_vector.iov_base == nullptr || io_vector.iov_len == 0)
        {
                return KERN_FAIL(EINVAL);
        }

        uio u{ .uio_iov    = &io_vector,
               .uio_iovcnt = 1,
               .uio_offset = 0,
               .uio_resid  = a_buffer_len,
               .uio_segflg = UIO_USERSPACE,
               .uio_rw     = UIO_WRITE };

        vfs_file_t f;
        kern_return_t retval = GET_SOCKET_FILE(a_fd, &f);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }
        defer(vfs_file_rel(f));

        if ((a_addr != nullptr))
        {
                sockaddr_storage sa = {};

                a_addrlen = etl::min(a_addrlen, (socklen_t)sizeof(sockaddr_storage));

                retval = copyin(a_addr, &sa, a_addrlen);

                if (KERN_STATUS_FAILURE(retval))
                {
                        return retval;
                }

                retval = network_socket_sendto(f, &u, a_flags, (struct sockaddr *)&sa, &a_addrlen);
        }
        else
        {
                // retval = so->m_nsops->sendto(so->m_nso, a_buffer, a_buffer_len, a_flags, &addr.soaddr, a_addrlen);
                retval = network_socket_sendto(f, &u, a_flags, nullptr, &a_addrlen);
        }

        return retval;
}

kern_return_t sys_recvfrom(int a_fd, void *a_buffer, size_t a_buffer_len, int a_flags, sockaddr *a_addr, socklen_t *a_addrlen)
{
        iovec io_vector{ .iov_base = a_buffer, .iov_len = a_buffer_len };
        uio u{ .uio_iov    = &io_vector,
               .uio_iovcnt = 1,
               .uio_offset = 0,
               .uio_resid  = a_buffer_len,
               .uio_segflg = UIO_USERSPACE,
               .uio_rw     = UIO_READ };

        vfs_file_t f;
        kern_return_t retval = GET_SOCKET_FILE(a_fd, &f);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }
        defer(vfs_file_rel(f));

        if ((a_addr != nullptr) && (a_addrlen != nullptr))
        {
                sockaddr_storage sa = {};
                socklen_t addrlen;

                retval = emerixx::uio::copyin(a_addrlen, &addrlen);

                if (KERN_STATUS_FAILURE(retval))
                {
                        return retval;
                }

                addrlen = etl::min(addrlen, (socklen_t)sizeof(sockaddr_storage));

                retval = network_socket_recvfrom(f, &u, a_flags, (sockaddr *)&sa, &addrlen);

                emerixx::uio::copyout((sockaddr *)&sa, a_addr);
                emerixx::uio::copyout(&addrlen, a_addrlen);
        }
        else
        {
                retval = network_socket_recvfrom(f, &u, a_flags, nullptr, nullptr);
        }

        if (KERN_STATUS_SUCCESS(retval))
        {
                retval = (kern_return_t)(a_buffer_len - u.uio_resid);
        }

        return retval;
}

kern_return_t sys_sendmsg(int a_fd, msghdr const *a_msg, int a_flags)
{
#if 0
        msghdr hdr;
        kern_return_t retval = so->m_nsops->sendmsg(so->m_nso, &hdr, a_flags);
        return retval;
#endif
        KASSERT(0);
        return KERN_FAIL(ENOSYS);
}

kern_return_t sys_recvmsg(int a_fd, msghdr const *a_msg, int a_flags)
{
#if 0
        msghdr hdr;
        kern_return_t retval = so->m_nsops->sendmsg(so->m_nso, &hdr, a_flags);
        return retval;
#endif
        KASSERT(0);
        return KERN_FAIL(ENOSYS);
}

kern_return_t sys_shutdown(int a_fd, int a_how)
{
        switch (a_how)
        {
                case SHUT_RD:
                case SHUT_WR:
                case SHUT_RDWR:
                        /* Okay */
                        break;
                default:
                        return KERN_FAIL(EINVAL);
        }

        vfs_file_t f;
        kern_return_t retval = GET_SOCKET_FILE(a_fd, &f);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }
        defer(vfs_file_rel(f));

        retval = network_socket_shutdown(f, a_how);

        return retval;
}

kern_return_t sys_bind(int a_fd, sockaddr *a_addr, socklen_t a_addrlen)
{
        vfs_file_t f;
        kern_return_t retval = GET_SOCKET_FILE(a_fd, &f);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }
        defer(vfs_file_rel(f));

        sockaddr_storage addr = {};

        retval = sockaddr_copyin(a_addr, a_addrlen, &addr);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        retval = network_socket_bind(f, (sockaddr *)&addr, a_addrlen);
        return retval;
}

kern_return_t sys_listen(int a_fd, int a_backlog)
{
        if (a_backlog > SOMAXCONN || a_backlog < 1)
        {
                a_backlog = SOMAXCONN;
        }

        vfs_file_t f;
        kern_return_t retval = GET_SOCKET_FILE(a_fd, &f);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }
        defer(vfs_file_rel(f));

        retval = network_socket_listen(f, a_backlog);
        return retval;
}

kern_return_t sys_getsockname(int a_fd, sockaddr *a_addr, socklen_t *a_addrlen)
{
        sockaddr_storage addr = {};
        socklen_t addrlen     = sizeof(addr);

        vfs_file_t f;
        kern_return_t retval = GET_SOCKET_FILE(a_fd, &f);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }
        defer(vfs_file_rel(f));

        retval = emerixx::uio::copyin(a_addrlen, &addrlen);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        addrlen = etl::min(addrlen, (socklen_t)sizeof(addr));

        retval = network_socket_getname(f, (sockaddr *)&addr, &addrlen);

        if (KERN_STATUS_SUCCESS(retval))
        {
                retval = emerixx::uio::copyout(addrlen, a_addrlen);

                if (KERN_STATUS_SUCCESS(retval))
                {
                        retval = copyout(&addr, a_addr, addrlen);
                }
        }

        return retval;
}

kern_return_t sys_getpeername(int a_fd, sockaddr *a_addr, socklen_t *a_addrlen)
{
        sockaddr_storage addr = {};
        socklen_t addrlen     = 0;

        kern_return_t retval = emerixx::uio::copyin(a_addrlen, &addrlen);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        vfs_file_t f;
        retval = GET_SOCKET_FILE(a_fd, &f);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }
        defer(vfs_file_rel(f));

        addrlen = etl::min(addrlen, (socklen_t)sizeof(addr));

        retval = network_socket_getpeername(f, (sockaddr *)&addr, &addrlen);

        if (KERN_STATUS_SUCCESS(retval))
        {
                retval = emerixx::uio::copyout(addrlen, a_addrlen);

                if (KERN_STATUS_SUCCESS(retval))
                {
                        retval = copyout(&addr, a_addr, addrlen);
                }
        }

        return retval;
}

kern_return_t sys_socketpair(int a_family, int a_type, int a_proto, int a_socket_vector[2])
{
        KASSERT(0);
        return KERN_FAIL(ENOSYS);
}

kern_return_t sys_setsockopt(int a_fd, int a_level, int a_opt_name, char const *a_opt_value, socklen_t a_opt_value_len)
{
        char buf[32];

        if (a_opt_value_len >= sizeof(buf))
        {
                return KERN_FAIL(EINVAL);
        }

        kern_return_t retval = copyin(a_opt_value, buf, a_opt_value_len);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        vfs_file_t f;
        retval = GET_SOCKET_FILE(a_fd, &f);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }
        defer(vfs_file_rel(f));

        retval = network_socket_setsockopt(f, a_level, a_opt_name, buf, a_opt_value_len);

        return retval;
}

kern_return_t sys_getsockopt(int a_fd, int a_level, int a_opt_name, char const *a_opt_value, socklen_t *a_opt_value_len)
{
        vfs_file_t f;
        kern_return_t retval = GET_SOCKET_FILE(a_fd, &f);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }
        defer(vfs_file_rel(f));

        char b[1];
        retval = network_socket_getsockopt(f, a_level, a_opt_name, b, a_opt_value_len);
        return retval;
}
