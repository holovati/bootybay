#include <mach/tmpsys.h>

#include <emerixx/vfs.h>

// ********************************************************************************************************************************
kern_return_t sys_sync(pthread_t a_thread)
// ********************************************************************************************************************************
{
        for(vfs_mount_t mnt = vfs_mount_first(); mnt != nullptr; mnt = vfs_mount_next(mnt))
        {
                vfs_mount_sync(mnt, true);
        }

        return KERN_SUCCESS;
}
