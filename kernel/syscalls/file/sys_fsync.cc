#include <mach/tmpsys.h>

#include <emerixx/vfs.h>

// ********************************************************************************************************************************
kern_return_t sys_fsync(pthread_t a_thread, int a_fd)
// ********************************************************************************************************************************
{
        vfs_node_t vfsn = nullptr;

        kern_return_t retval = vfs_util_fd_to_vfsn(a_fd, &vfsn, true);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        retval = vfs_node_fsync(vfsn);

        vfs_node_put(vfsn);

        return retval;
}
