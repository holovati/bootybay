#include <mach/tmpsys.h>

#include <fcntl.h>
#include <sys/stat.h>

#include <emerixx/vfs.h>
#include <emerixx/process.h>

// ********************************************************************************************************************************
kern_return_t sys_truncate(pthread_t a_proc, char const *a_path, off_t a_length)
// ********************************************************************************************************************************
{
        vfs_node_t vfsn = nullptr;

        kern_return_t retval = vfs_util_simple_lookup_usr_path(a_path, AT_FDCWD, &vfsn, true, true /*Lock it*/);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        retval = vfs_node_truncate(vfsn, a_length);

        vfs_node_put(vfsn);

        return retval;
}

// ********************************************************************************************************************************
kern_return_t sys_ftruncate(pthread_t a_thread, int a_fd, off_t a_length)
// ********************************************************************************************************************************
{
        vfs_node_t vfsn = nullptr;

        kern_return_t retval = vfs_util_fd_to_vfsn(a_fd, &vfsn, true /*Lock it*/);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        retval = vfs_node_truncate(vfsn, a_length);

        vfs_node_put(vfsn);

        return retval;
}
