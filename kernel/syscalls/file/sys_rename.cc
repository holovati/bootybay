#include <mach/tmpsys.h>

#include <fcntl.h>
#include <string.h>

#include <aux/defer.hh>

#include <etl/utility.hh>

#include <emerixx/vfs/vfs_node.h>
#include <emerixx/vfs/vfs_pathbuf.h>
#include <emerixx/vfs/vfs_util.h>

/*
 * Rename files.  Source and destination must either both be directories, or both not be directories.  If target is a directory, it
 * must be empty.
 */
int sys_renameat2(pthread_t a_proc, int a_old_dfd, char const *a_old_path, int a_new_dfd, char const *a_new_path, int a_flag)
{
        vfs_rename_lock_exclusive();
        defer(vfs_rename_lock_exclusive_done());

        KASSERT(a_flag == 0);

        char const *old_path;
        natural_t old_path_len;
        // Get the old path (target)
        kern_return_t retval = vfs_pathbuf_copyin(a_old_path, &old_path, &old_path_len);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        defer(vfs_pathbuf_free(old_path));

        char const *new_path;
        natural_t new_path_len;
        // Get the new path (destination)
        retval = vfs_pathbuf_copyin(a_new_path, &new_path, &new_path_len);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        defer(vfs_pathbuf_free(new_path));

        // The vfs node where we start looking for the vfs_node pointed to by old_path (target)
        vfs_node_t old_begin_vfsn;
        retval = vfs_util_lookup_first_node(old_path, a_old_dfd, &old_begin_vfsn, false /*Lookup will lock before usage*/);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        defer(vfs_node_rel(old_begin_vfsn));

        // The vfs node where we start looking for the vfs_node pointed to by new_path (destination)
        vfs_node_t new_begin_vfsn;
        retval = vfs_util_lookup_first_node(new_path, a_new_dfd, &new_begin_vfsn, false /*Lookup will lock before usage*/);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        defer(vfs_node_rel(new_begin_vfsn));

        vfs_node_t old_parent_vfsn   = nullptr;
        char const *old_child_name   = nullptr;
        natural_t old_child_name_len = 0;

        vfs_node_t new_parent_vfsn   = nullptr;
        char const *new_child_name   = nullptr;
        natural_t new_child_name_len = 0;

        retval = vfs_node_lookup_rename(old_begin_vfsn,
                                        old_path,
                                        old_path_len,
                                        new_begin_vfsn,
                                        new_path,
                                        new_path_len,
                                        true,
                                        &old_parent_vfsn,
                                        &old_child_name,
                                        &old_child_name_len,
                                        &new_parent_vfsn,
                                        &new_child_name,
                                        &new_child_name_len);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        retval = vfs_node_rename(old_parent_vfsn,
                                 old_child_name,
                                 old_child_name_len,
                                 new_parent_vfsn,
                                 new_child_name,
                                 new_child_name_len);

        if (new_parent_vfsn != old_parent_vfsn)
        {
                vfs_node_put(old_parent_vfsn);
        }

        vfs_node_put(new_parent_vfsn);

        return retval;
}

int sys_renameat(pthread_t a_proc, int a_old_dfd, char const *a_old_name, int a_new_dfd, char const *a_new_name)
{
        return sys_renameat2(a_proc, a_old_dfd, a_old_name, a_new_dfd, a_new_name, 0);
}

int sys_rename(pthread_t a_proc, char const *a_old_name, char const *a_new_name)
{
        return sys_renameat(a_proc, AT_FDCWD, a_old_name, AT_FDCWD, a_new_name);
}
