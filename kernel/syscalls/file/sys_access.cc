#include <mach/tmpsys.h>
#include <fcntl.h>
#include <emerixx/vfs.h>

// Added to linux 16 april 2020 as faccessat2 (https://lwn.net/Articles/817916/)
// ********************************************************************************************************************************
static kern_return_t sys_faccessat2(pthread_t a_proc, int a_dfd, char const *a_upath, mode_t a_mode, int a_flags)
// ********************************************************************************************************************************
{
        vfs_node_t vfsn;
        kern_return_t retval
                = vfs_util_simple_lookup_usr_path(a_upath, a_dfd, &vfsn, (a_flags & AT_SYMLINK_NOFOLLOW) == 0, true /*Want lock*/);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        /* Flags == 0 means only check for existence. */
        if (a_mode)
        {
                /*
                The mode specifies the accessibility check(s) to be performed,
                and is either the value F_OK, or a mask consisting of the bitwise
                OR of one or more of R_OK, W_OK, and X_OK.  F_OK tests for the
                existence of the file.  R_OK, W_OK, and X_OK test whether the
                file exists and grants read, write, and execute permissions,
                respectively.
                */
                retval = KERN_SUCCESS;
        }

        vfs_node_put(vfsn);

        return retval;
}

// ********************************************************************************************************************************
int sys_access(pthread_t a_proc, char const *a_path, mode_t a_mode)
// ********************************************************************************************************************************
{
        return sys_faccessat2(a_proc, AT_FDCWD, a_path, a_mode, 0 /* Will follow */);
}

// Non POSIX conforming Linux version
// ********************************************************************************************************************************
int sys_faccessat(pthread_t a_proc, int a_dfd, char const *a_path, mode_t a_mode)
// ********************************************************************************************************************************
{
        return sys_faccessat2(a_proc, a_dfd, a_path, a_mode, AT_SYMLINK_NOFOLLOW);
}
