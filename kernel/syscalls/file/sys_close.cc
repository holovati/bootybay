#include <emerixx/fdesc.hh>
#include <emerixx/process.h>
#include <mach/tmpsys.h>

// ********************************************************************************************************************************
kern_return_t sys_close(pthread_t a_thread, int a_fd)
// ********************************************************************************************************************************
{
        return fd_context_fdclose(a_thread->m_fdctx, a_fd);
}