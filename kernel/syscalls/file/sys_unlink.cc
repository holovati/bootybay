#include <mach/tmpsys.h>

#include <aux/defer.hh>

#include <fcntl.h>

#include <emerixx/vfs.h>

// ********************************************************************************************************************************
kern_return_t sys_unlikat(pthread_t a_proc, int a_dfd, char const *a_path, int a_flags)
// ********************************************************************************************************************************
{
        vfs_rename_lock_shared();
        defer(vfs_rename_lock_shared_done());

        char const *kpath    = nullptr;
        natural_t kpath_len  = 0;
        kern_return_t retval = vfs_pathbuf_copyin(a_path, &kpath, &kpath_len);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }
        defer(vfs_pathbuf_free(kpath));

        // Get the starting node
        vfs_node_t vfsn_start = nullptr;

        retval = vfs_util_lookup_first_node(kpath, a_dfd, &vfsn_start, false);
        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }
        defer(vfs_node_rel(vfsn_start));

        vfs_node_t vfsn_parent = nullptr, vfsn_child = nullptr;
        char const *a_child_name  = nullptr;
        natural_t a_child_namelen = 0;

        retval = vfs_node_lookup_delete(vfsn_start,
                                        kpath,
                                        kpath_len,
                                        false,
                                        &vfsn_parent,
                                        &vfsn_child,
                                        &a_child_name,
                                        &a_child_namelen);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        retval = vfs_node_unlink(vfsn_parent, vfsn_child, a_child_name, a_child_namelen, !!(a_flags & AT_REMOVEDIR));

        vfs_node_rel(vfsn_child);

        vfs_node_put(vfsn_parent);

        return retval;
}

// ********************************************************************************************************************************
kern_return_t sys_unlink(pthread_t a_proc, char const *a_path)
// ********************************************************************************************************************************
{
        return sys_unlikat(a_proc, AT_FDCWD, a_path, 0);
}

// ********************************************************************************************************************************
kern_return_t sys_rmdir(pthread_t a_proc, char const *a_path)
// ********************************************************************************************************************************
{
        return sys_unlikat(a_proc, AT_FDCWD, a_path, AT_REMOVEDIR);
}
