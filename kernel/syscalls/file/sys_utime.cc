#include <mach/tmpsys.h>

#include <fcntl.h>
#include <utime.h>
#include <sys/stat.h>

#include <emerixx/memory_rw.h>
#include <emerixx/vfs.h>

#include <kernel/mach_clock.h>

// ********************************************************************************************************************************
kern_return_t sys_utimensat(pthread_t a_thread, int a_dfd, char const *a_path, struct timespec *a_utimens, int a_flags)
// ********************************************************************************************************************************
{
        kern_return_t retval = 0;

        struct timespec tv[2];
        if (a_utimens == nullptr)
        {
                mach_get_timespec(tv);
                tv[1] = tv[0];
        }
        else
        {
                retval = copyin(a_utimens, tv, sizeof(tv));

                if (KERN_STATUS_SUCCESS(retval))
                {
                        return retval;
                }
        }

        // TODO: Check validity of userdata

        vfs_node_t vfsn = nullptr;

        if (a_path != nullptr)
        {
                retval = vfs_util_simple_lookup_usr_path(a_path,
                                                         a_dfd,
                                                         &vfsn,
                                                         (a_flags & AT_SYMLINK_NOFOLLOW) == 0,
                                                         true /*Lock it*/);
        }
        else
        {
                retval = vfs_util_fd_to_vfsn(a_dfd, &vfsn, true /*Lock it*/);
        }

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        retval = vfs_node_utimens(vfsn, tv);

        vfs_node_put(vfsn);

        return retval;
}

// ********************************************************************************************************************************
int sys_futimesat(pthread_t a_thread, int a_dfd, char const *a_path, struct timeval *a_utimes)
// ********************************************************************************************************************************
{
        if (a_utimes)
        {

                struct timeval tv[2];

                kern_return_t retval = copyin(a_utimes, tv, sizeof(tv));

                if (retval != KERN_SUCCESS)
                {
                        return retval;
                }

                struct timespec tp[2];

                tp[0].tv_sec  = tv[0].tv_sec;
                tp[0].tv_nsec = tv[0].tv_usec * 1000;

                tp[1].tv_sec  = tv[1].tv_sec;
                tp[1].tv_nsec = tv[1].tv_usec * 1000;

                return sys_utimensat(a_thread, a_dfd, a_path, tp, AT_SYMLINK_NOFOLLOW);
        }
        else
        {
                return sys_utimensat(a_thread, a_dfd, a_path, nullptr, AT_SYMLINK_NOFOLLOW);
        }
}

// ********************************************************************************************************************************
int sys_utimes(pthread_t a_thread, char const *a_path, struct timeval *a_utimes)
// ********************************************************************************************************************************
{
        return sys_futimesat(a_thread, AT_FDCWD, a_path, a_utimes);
}

// ********************************************************************************************************************************
int sys_utime(pthread_t a_thread, char const *a_path, struct utimbuf *a_time)
// ********************************************************************************************************************************
{
        if (a_time)
        {
                struct utimbuf tb;

                kern_return_t retval = emerixx::uio::copyin(a_time, &tb);

                if (retval != KERN_SUCCESS)
                {
                        return retval;
                }

                struct timespec tp[2] = {};

                tp[0].tv_sec = tb.actime;
                tp[1].tv_sec = tb.modtime;

                return sys_utimensat(a_thread, AT_FDCWD, a_path, tp, AT_SYMLINK_NOFOLLOW);
        }
        else
        {
                return sys_utimensat(a_thread, AT_FDCWD, a_path, nullptr, AT_SYMLINK_NOFOLLOW);
        }
}
