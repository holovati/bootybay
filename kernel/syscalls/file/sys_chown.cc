#include <mach/tmpsys.h>

#include <fcntl.h>

#include <emerixx/vfs.h>
// The fchown() function shall be equivalent to chown() except that the file whose owner and group are changed is specified by the
// file descriptor fildes.
// ********************************************************************************************************************************
kern_return_t sys_fchown(pthread_t a_thread, int a_fd, uid_t a_uid, gid_t a_gid)
// ********************************************************************************************************************************
{
        vfs_node_t vfsn = nullptr;

        kern_return_t retval = vfs_util_fd_to_vfsn(a_fd, &vfsn, true /*Want lock*/);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        retval = vfs_node_chown(vfsn, a_uid, a_gid);

        vfs_node_put(vfsn);

        return retval;
}

// The fchownat() function shall be equivalent to the chown() and lchown() functions except in the case where path specifies a
// relative path. In this case the file to be changed is determined relative to the directory associated with the file descriptor fd
// instead of the current working directory. If the access mode of the open file description associated with the file descriptor is
// not O_SEARCH, the function shall check whether directory searches are permitted using the current permissions of the directory
// underlying the file descriptor. If the access mode is O_SEARCH, the function shall not perform the check.
// ********************************************************************************************************************************
kern_return_t sys_fchownat(pthread_t a_proc, int a_dfd, char const *a_path, uid_t a_uid, gid_t a_gid, int a_flag)
// ********************************************************************************************************************************
{
        vfs_node_t vfsn = nullptr;

        kern_return_t retval
                = vfs_util_simple_lookup_usr_path(a_path, a_dfd, &vfsn, (a_flag & AT_SYMLINK_NOFOLLOW) == 0, true /*Want lock*/);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        retval = vfs_node_chown(vfsn, a_uid, a_gid);

        vfs_node_put(vfsn);

        return retval;
}

// The lchown() function shall be equivalent to chown(), except in the case where the named file is a symbolic link. In this case,
// lchown() shall change the ownership of the symbolic link file itself, while chown() changes the ownership of the file or
// directory to which the symbolic link refers.
// ********************************************************************************************************************************
kern_return_t sys_lchown(pthread_t a_proc, char const *a_path, uid_t a_uid, gid_t a_gid)
// ********************************************************************************************************************************
{
        return sys_fchownat(a_proc, AT_FDCWD, a_path, a_uid, a_gid, AT_SYMLINK_NOFOLLOW);
}

// The chown() function shall change the user and group ownership of a file.
// ********************************************************************************************************************************
kern_return_t sys_chown(pthread_t a_proc, char const *a_path, uid_t a_uid, gid_t a_gid)
// ********************************************************************************************************************************
{
        return sys_fchownat(a_proc, AT_FDCWD, a_path, a_uid, a_gid, 0 /* Will follow symlink */);
}
