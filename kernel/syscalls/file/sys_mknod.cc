#include <mach/tmpsys.h>

#include <fcntl.h>
#include <sys/stat.h>

#include <aux/defer.hh>

#include <emerixx/vfs.h>
// ********************************************************************************************************************************
kern_return_t sys_mknodat(pthread_t a_proc, int a_dfd, char const *a_upath, mode_t a_mode, dev_t a_dev)
// ********************************************************************************************************************************
{
        vfs_rename_lock_shared();
        defer(vfs_rename_lock_shared_done());

        if ((a_mode & (S_IFCHR | S_IFBLK | S_IFIFO | S_IFREG)) == 0)
        {
                // This is not correct, it is possible to create files and special files
                // But we will stick with this for now
                return KERN_FAIL(EINVAL);
        }

        char const *kpath    = nullptr;
        natural_t kpath_len  = 0;
        kern_return_t retval = vfs_pathbuf_copyin(a_upath, &kpath, &kpath_len);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }
        defer(vfs_pathbuf_free(kpath));

        vfs_node_t vfsn_start = nullptr;

        retval = vfs_util_lookup_first_node(kpath, a_dfd, &vfsn_start, false /*Lookup will lock before usage*/);
        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }
        defer(vfs_node_rel(vfsn_start));

        vfs_node_t vfsn_parent   = nullptr;
        const char *child_name   = nullptr;
        natural_t child_name_len = 0;

        retval = vfs_node_lookup_create(vfsn_start,
                                        kpath,
                                        kpath_len,
                                        &vfsn_parent,
                                        &child_name,
                                        &child_name_len,
                                        nullptr,
                                        false,
                                        false);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        retval = vfs_node_mknod(vfsn_parent, child_name, child_name_len, a_mode, a_dev);

        vfs_node_put(vfsn_parent);

        return retval;
}

// ********************************************************************************************************************************
kern_return_t sys_mknod(pthread_t a_proc, char const *a_path, mode_t a_mode, dev_t a_dev)
// ********************************************************************************************************************************
{
        return sys_mknodat(a_proc, AT_FDCWD, a_path, a_mode, a_dev);
}
