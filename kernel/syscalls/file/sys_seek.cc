#include <emerixx/fdesc.hh>
#include <emerixx/vfs.h>
#include <emerixx/process.h>
#include <mach/tmpsys.h>
#include <unistd.h>

// ********************************************************************************************************************************
kern_return_t sys_lseek(pthread_t a_thread, int a_fd, off_t a_offset, int a_whence)
// ********************************************************************************************************************************
{
        vfs_file_t f = nullptr;

        kern_return_t retval = fd_context_get_file(a_thread->m_fdctx, a_fd, &f);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        retval = vfs_file_lseek(f, a_offset, a_whence);

        vfs_file_rel(f);

        return retval;
}