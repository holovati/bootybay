#include <mach/tmpsys.h>

#include <fcntl.h>
#include <sys/stat.h>

#include <emerixx/vfs.h>
#include <emerixx/process.h>

// This was introduced in linux on Thu, 10 Sep 2020 10:23:37 -0400 and not in our syscall descriptors yet
// ********************************************************************************************************************************
static int sys_fchmodat2(pthread_t a_proc, int a_dfd, char const *a_path, mode_t a_mode, int a_flags)
// ********************************************************************************************************************************
{
        vfs_node_t vfsn = nullptr;

        kern_return_t retval
                = vfs_util_simple_lookup_usr_path(a_path, a_dfd, &vfsn, (a_flags & AT_SYMLINK_NOFOLLOW) == 0, true /*Want lock*/);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        retval = vfs_node_chmod(vfsn, a_mode & ~S_IFMT);

        vfs_node_put(vfsn);

        return retval;
}

// Non POSIX conforming Linux version
// ********************************************************************************************************************************
int sys_fchmodat(pthread_t a_proc, int a_dfd, char const *a_path, mode_t a_mode)
// ********************************************************************************************************************************
{
        return sys_fchmodat2(a_proc, a_dfd, a_path, a_mode, 0 /* Will follow symlink*/);
}

// ********************************************************************************************************************************
int sys_fchmod(pthread_t a_thread, int a_fd, mode_t a_mode)
// ********************************************************************************************************************************
{
        vfs_node_t vfsn = nullptr;

        kern_return_t retval = vfs_util_fd_to_vfsn(a_fd, &vfsn, true /*Want lock*/);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        retval = vfs_node_chmod(vfsn, a_mode & ~S_IFMT);

        vfs_node_put(vfsn);

        return retval;
}

// ********************************************************************************************************************************
int sys_chmod(pthread_t a_proc, char const *a_path, mode_t a_mode)
// ********************************************************************************************************************************
{
        return sys_fchmodat2(a_proc, AT_FDCWD, a_path, a_mode, 0 /* Will follow symlink*/);
}
