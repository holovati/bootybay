#include <mach/tmpsys.h>

#include <fcntl.h>

#include <aux/defer.hh>

#include <etl/algorithm.hh>

#include <emerixx/fdesc.hh>
#include <emerixx/process.h>
#include <emerixx/vfs.h>
#include <emerixx/vfs/vfs_file_data.h>
#include <emerixx/memory_rw.h>

static ssize_t readv_internal(vfs_file_t a_file, iovec *a_iovec, size_t a_iovec_cnt)
{
        uio u{.uio_iov    = a_iovec,
              .uio_iovcnt = a_iovec_cnt,
              .uio_offset = 0,
              .uio_resid  = 0, // Calculated in the loop below
              .uio_segflg = UIO_USERSPACE,
              .uio_rw     = UIO_READ};

        for (size_t i = 0; i < u.uio_iovcnt; ++i)
        {
                u.uio_resid += a_iovec[i].iov_len;
        }

        size_t oldresid = u.uio_resid;

        ssize_t retval = vfs_file_read(a_file, &u);

        ssize_t nbytes = static_cast<ssize_t>(oldresid - u.uio_resid);

#if 0
        if ((nbytes > 0) && ((retval == KERN_FAIL(ERESTART)) || (retval == KERN_FAIL(EINTR)) || (retval == KERN_FAIL(EWOULDBLOCK))))
        {
                retval = KERN_SUCCESS;
        }
#endif
        return nbytes ? nbytes : retval;
}

static kern_return_t readv_getfile_internal(pthread_t a_thread, int a_fd, vfs_file_t *a_file_out)
{
        kern_return_t retval = fd_context_get_file(a_thread->m_fdctx, a_fd, a_file_out);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        if ((*a_file_out)->m_oflags & O_WRONLY)
        {
                return KERN_FAIL(EBADF);
        }

        return KERN_SUCCESS;
}

ssize_t sys_readv(pthread_t a_proc, int a_fd, iovec const *a_iovec, size_t a_iovec_cnt)
{
        if (a_iovec_cnt > UIO_MAXIOV)
        {
                return KERN_FAIL(EINVAL);
        }

        vfs_file_t f;
        ssize_t retval = readv_getfile_internal(a_proc, a_fd, &f);

        if (retval != KERN_SUCCESS)
        {
                return retval;
        }
        defer(vfs_file_rel(f));

        iovec io_vector[UIO_SMALLIOV];
        size_t iovcnt;

        ssize_t nbytes = 0;
        while (a_iovec_cnt > 0)
        {
                iovcnt = etl::min(a_iovec_cnt, static_cast<size_t>(UIO_SMALLIOV));

                retval = copyin(a_iovec, io_vector, static_cast<vm_size_t>(sizeof(iovec) * iovcnt));

                if (retval != KERN_SUCCESS)
                {
                        break;
                }

                retval = readv_internal(f, io_vector, iovcnt);

                if (retval <= 0)
                {
                        break;
                }

                nbytes += retval;

                a_iovec_cnt -= iovcnt;
                a_iovec += iovcnt;
        }

        return nbytes ? nbytes : retval;
}

ssize_t sys_read(pthread_t a_proc, int a_fd, void *a_buf, size_t a_nbyte)
{
        vfs_file_t f;

        ssize_t retval = readv_getfile_internal(a_proc, a_fd, &f);

        if (retval != KERN_SUCCESS)
        {
                return retval;
        }
        defer(vfs_file_rel(f));

        iovec io_vector{.iov_base = a_buf, .iov_len = a_nbyte};

        return readv_internal(f, &io_vector, 1);
}
