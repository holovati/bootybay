#include <mach/tmpsys.h>

#include <sys/stat.h>
#include <fcntl.h>
#include <strings.h>

#include <emerixx/vfs.h>
#include <emerixx/fdesc.hh>
#include <emerixx/process.h>
#include <emerixx/memory_rw.h>

// ********************************************************************************************************************************
kern_return_t sys_fstat(pthread_t a_thread, int a_fd, struct stat *a_statbuf)
// ********************************************************************************************************************************
{
        vfs_node_t vfsn;
        kern_return_t retval = vfs_util_fd_to_vfsn(a_fd, &vfsn, true /*want lock*/);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        struct stat statbuf = {};

        retval = vfs_node_stat(vfsn, &statbuf);
        if (KERN_STATUS_SUCCESS(retval))
        {
                retval = emerixx::uio::copyout(&statbuf, a_statbuf);
        }

        vfs_node_put(vfsn);

        return retval;
}

// ********************************************************************************************************************************
kern_return_t sys_newfstatat(pthread_t a_thread, int a_dfd, char const *a_path, struct stat *a_statbuf, int a_flag)
// ********************************************************************************************************************************
{
        if (a_flag & AT_EMPTY_PATH)
        {
                return sys_fstat(a_thread, a_dfd, a_statbuf);
        }

        vfs_node_t vfsn = nullptr;

        kern_return_t retval
                = vfs_util_simple_lookup_usr_path(a_path, a_dfd, &vfsn, (a_flag & AT_SYMLINK_NOFOLLOW) == 0, true /* want lock */);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        struct stat statbuf = {};

        retval = vfs_node_stat(vfsn, &statbuf);
        if (KERN_STATUS_SUCCESS(retval))
        {
                retval = emerixx::uio::copyout(&statbuf, a_statbuf);
        }

        vfs_node_put(vfsn);

        return retval;
}

// ********************************************************************************************************************************
kern_return_t sys_stat(pthread_t a_proc, char const *a_path, struct stat *a_statbuf)
// ********************************************************************************************************************************
{
        return sys_newfstatat(a_proc, AT_FDCWD, a_path, a_statbuf, 0 /* Will follow symlink */);
}

// ********************************************************************************************************************************
kern_return_t sys_lstat(pthread_t a_proc, char const *a_path, struct stat *a_statbuf)
// ********************************************************************************************************************************
{
        return sys_newfstatat(a_proc, AT_FDCWD, a_path, a_statbuf, AT_SYMLINK_NOFOLLOW);
}
