#include <mach/tmpsys.h>

#include <fcntl.h>
#include <limits.h>

#include <aux/defer.hh>

#include <emerixx/vfs.h>
// ********************************************************************************************************************************
kern_return_t sys_symlinkat(pthread_t a_thread, char const *a_path1, int a_dfd, char const *a_path2 /*The symlink file itself*/)
// ********************************************************************************************************************************
{
        vfs_rename_lock_shared();
        defer(vfs_rename_lock_shared_done());

        // Get the target path
        char const *targetpath   = nullptr;
        natural_t targetpath_len = 0;
        kern_return_t retval     = vfs_pathbuf_copyin(a_path1, &targetpath, &targetpath_len);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }
        defer(vfs_pathbuf_free(targetpath));

        if (targetpath_len >= _POSIX_SYMLINK_MAX)
        {
                return KERN_FAIL(ENAMETOOLONG);
        }

        // Get the link path
        char const *lnkpath   = nullptr;
        natural_t lnkpath_len = 0;

        retval = vfs_pathbuf_copyin(a_path2, &lnkpath, &lnkpath_len);
        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }
        defer(vfs_pathbuf_free(lnkpath));

        // Get the starting node
        vfs_node_t vfsn_start = nullptr;

        retval = vfs_util_lookup_first_node(lnkpath, a_dfd, &vfsn_start, false /*want lock, will be locked during lookup*/);
        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }
        defer(vfs_node_rel(vfsn_start));

        // Do a lookup, if child exists we fail
        vfs_node_t vfsn_parent   = nullptr;
        const char *child_name   = nullptr;
        natural_t child_name_len = 0;

        retval = vfs_node_lookup_create(vfsn_start,
                                        lnkpath,
                                        lnkpath_len,
                                        &vfsn_parent,
                                        &child_name,
                                        &child_name_len,
                                        nullptr,
                                        false,
                                        false);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        // Attempt to crate a symlink
        retval = vfs_node_symlink(vfsn_parent, child_name, child_name_len, targetpath, targetpath_len);

        vfs_node_put(vfsn_parent);

        return retval;
}

// ********************************************************************************************************************************
kern_return_t sys_symlink(pthread_t a_proc, char const *a_path1, char const *a_path2)
// ********************************************************************************************************************************
{
        return sys_symlinkat(a_proc, a_path1, AT_FDCWD, a_path2);
}
