#include <mach/tmpsys.h>

#include <fcntl.h>
#include <sys/statfs.h>

#include <emerixx/vfs/vfs_node_data.h>
#include <emerixx/vfs.h>
#include <emerixx/memory_rw.h>

// ********************************************************************************************************************************
static kern_return_t statfs_internal(pthread_t a_thread, vfs_node_t a_vfsn, struct statfs *a_buf_out)
// ********************************************************************************************************************************
{
        struct statfs sp = {};

        kern_return_t retval = vfs_mount_stat(a_vfsn->m_mount, &sp);

        if (KERN_STATUS_SUCCESS(retval))
        {
                retval = emerixx::uio::copyout(sp, a_buf_out);
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t sys_statfs(pthread_t a_thread, char const *a_path, struct statfs *a_buf_out)
// ********************************************************************************************************************************
{
        vfs_node_t vfsn = nullptr;

        kern_return_t retval = vfs_util_simple_lookup_usr_path(a_path, AT_FDCWD, &vfsn, true, true);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        retval = statfs_internal(a_thread, vfsn, a_buf_out);

        vfs_node_put(vfsn);

        return retval;
}

// ********************************************************************************************************************************
kern_return_t sys_fstatfs(pthread_t a_thread, int a_fd, struct statfs *a_buf_out)
// ********************************************************************************************************************************
{
        vfs_node_t vfsn = nullptr;

        kern_return_t retval = vfs_util_fd_to_vfsn(a_fd, &vfsn, true);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        retval = statfs_internal(a_thread, vfsn, a_buf_out);

        vfs_node_put(vfsn);

        return retval;
}
