#include <mach/tmpsys.h>

#include <fcntl.h>

#include <emerixx/vfs.h>
#include <emerixx/fsdata.hh>
#include <emerixx/process.h>

// ********************************************************************************************************************************
kern_return_t sys_chdir(pthread_t a_thread, char const *a_path)
// ********************************************************************************************************************************
{
        vfs_node_t vfsn = nullptr;

        kern_return_t retval = vfs_util_simple_lookup_usr_path(a_path, AT_FDCWD, &vfsn, false, false);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        retval = fs_context_chdir(a_thread->m_fsctx, vfsn);

        vfs_node_rel(vfsn);

        return retval;
}

// ********************************************************************************************************************************
kern_return_t sys_fchdir(pthread_t a_thread, int a_dfd)
// ********************************************************************************************************************************
{
        vfs_node_t vfsn;
        kern_return_t retval = vfs_util_fd_to_vfsn(a_dfd, &vfsn, false);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        retval = fs_context_chdir(a_thread->m_fsctx, vfsn);

        vfs_node_rel(vfsn);

        return retval;
}
