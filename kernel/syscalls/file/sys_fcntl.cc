#include <mach/tmpsys.h>

#include <emerixx/vfs.h>
#include <emerixx/fdesc.hh>
#include <emerixx/process.h>

// ********************************************************************************************************************************
kern_return_t sys_fcntl(pthread_t a_thread, int a_fd, unsigned a_cmd, unsigned long a_arg)
// ********************************************************************************************************************************
{
        return fd_context_fcntl(a_thread->m_fdctx, a_fd, a_cmd, a_arg);
}
