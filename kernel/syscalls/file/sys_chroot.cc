#include <mach/tmpsys.h>

#include <fcntl.h>

#include <emerixx/fsdata.hh>
#include <emerixx/process.h>
#include <emerixx/vfs.h>
// ********************************************************************************************************************************
kern_return_t sys_chroot(pthread_t a_thread, char const *a_path)
// ********************************************************************************************************************************
{
        /*
        if ((error = suser(a_proc->process->p_cred->pc_ucred, &a_proc->process->p_acflag)))
        {
                return error;
        }
        */
        vfs_node_t vfsn = nullptr;

        kern_return_t retval = vfs_util_simple_lookup_usr_path(a_path, AT_FDCWD, &vfsn, true, false);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        retval = fs_context_chroot(a_thread->m_fsctx, vfsn);

        vfs_node_rel(vfsn);

        return retval;
}
