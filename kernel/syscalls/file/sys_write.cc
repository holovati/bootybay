#include <mach/tmpsys.h>

#include <aux/defer.hh>

#include <emerixx/vfs.h>
#include <emerixx/memory_rw.h>
#include <emerixx/fdesc.hh>
#include <emerixx/process.h>

ssize_t sys_writev(pthread_t a_thread, int a_fd, iovec const *iovp, int iovcnt)
{
        uio auio;
        iovec *iov;
        iovec *needfree;
        iovec aiov[UIO_SMALLIOV];

        size_t iovlen;
        vfs_file_t fp;

        kern_return_t retval = fd_context_get_file(a_thread->m_fdctx, a_fd, &fp);

        if (retval != KERN_SUCCESS)
        {
                return retval;
        }
        defer(vfs_file_rel(fp));

        /* note: can't use iovlen until iovcnt is validated */
        iovlen = iovcnt * sizeof(struct iovec);
        if (iovcnt > UIO_SMALLIOV)
        {
                if (iovcnt > UIO_MAXIOV)
                {
                        return KERN_FAIL(EINVAL);
                }
                // MALLOC(iov, struct iovec *, iovlen, M_IOV, M_WAITOK);
                /// iov      = (struct iovec *)malloc(sizeof(struct iovec) * iovlen);
                // needfree = iov;
                panic("picnic");
        }
        else
        {
                iov      = aiov;
                needfree = NULL;
        }

        auio.uio_iov    = iov;
        auio.uio_iovcnt = iovcnt;
        auio.uio_offset = 0;
        auio.uio_rw     = UIO_WRITE;
        auio.uio_segflg = UIO_USERSPACE;

        ssize_t cnt = 0;

        retval = copyin(iovp, iov, iovlen);

        if (retval != KERN_SUCCESS)
        {
                goto done;
        }

        auio.uio_resid = 0;

        for (long i = 0; i < iovcnt; i++)
        {

                auio.uio_resid += iov->iov_len;

                iov++;
        }

        if (auio.uio_resid == 0)
        {
                retval = KERN_FAIL(EINVAL);
                goto done;
        }

        cnt = auio.uio_resid;

        if (cnt > 0 && (retval = vfs_file_write(fp, &auio)))
        {
                if (auio.uio_resid != (size_t)cnt
                    && (retval == KERN_FAIL(ERESTART) || retval == KERN_FAIL(EINTR) || retval == KERN_FAIL(EWOULDBLOCK)))
                {
                        retval = KERN_SUCCESS;
                }

                if (retval == -EPIPE)
                {
                        // log_debug("EPIPE");
                        raise(SIGPIPE);
                        //  p->signal(SIGPIPE);
                }
        }

        cnt -= auio.uio_resid;

        // Read the description for the linux writev
        //*retval = cnt;
done:
        if (needfree)
        {
                // FREE(needfree, M_IOV);
                panic("picnic");
        }

        if (KERN_STATUS_SUCCESS(retval))
        {
                retval = (kern_return_t)cnt;
        }

        // return (error);

        return retval;
}

ssize_t sys_write(pthread_t a_thread, int a_fd, char const *buf, size_t nbyte)
{
        if (buf == nullptr)
        {
                return KERN_FAIL(EFAULT);
        }

        if (nbyte == 0)
        {
                return KERN_FAIL(EINVAL);
        }

        uio auio;
        iovec aiov;

        vfs_file_t fp;
        kern_return_t retval = fd_context_get_file(a_thread->m_fdctx, a_fd, &fp);

        if (retval != KERN_SUCCESS)
        {
                return retval;
        }
        defer(vfs_file_rel(fp));

        aiov.iov_base   = const_cast<char *>(buf);
        aiov.iov_len    = nbyte;
        auio.uio_iov    = &aiov;
        auio.uio_iovcnt = 1;
        auio.uio_offset = 0;
        auio.uio_resid  = nbyte;
        auio.uio_rw     = UIO_WRITE;
        auio.uio_segflg = UIO_USERSPACE;

        ssize_t cnt = nbyte;

        retval = vfs_file_write(fp, &auio);

        if (retval != KERN_SUCCESS)
        {
                if (auio.uio_resid != (size_t)cnt
                    && (retval == KERN_FAIL(ERESTART) || retval == KERN_FAIL(EINTR) || retval == KERN_FAIL(EWOULDBLOCK)))
                {
                        retval = KERN_SUCCESS;
                }

                if (retval == KERN_FAIL(EPIPE))
                {
                        raise(SIGPIPE);
                }
        }

        if (KERN_STATUS_SUCCESS(retval))
        {
                retval = (kern_return_t)(cnt - auio.uio_resid);
        }

        return retval;
}
