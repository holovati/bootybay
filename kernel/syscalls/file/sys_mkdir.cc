#include <mach/tmpsys.h>

#include <fcntl.h>
#include <sys/stat.h>

#include <aux/defer.hh>

#include <emerixx/vfs.h>
#define ACCESSPERMS (S_IRWXU | S_IRWXG | S_IRWXO)                               /* 0777 */
#define ALLPERMS    (S_ISUID | S_ISGID | S_ISVTX | S_IRWXU | S_IRWXG | S_IRWXO) /* 07777 */

// ********************************************************************************************************************************
kern_return_t sys_mkdirat(pthread_t a_thread, int a_dfd, char const *a_upath, mode_t a_mode)
// ********************************************************************************************************************************
{
        vfs_rename_lock_shared();
        defer(vfs_rename_lock_shared_done());

        a_mode = a_mode & ~S_IFMT;

        char const *kpath    = nullptr;
        natural_t kpath_len  = 0;
        kern_return_t retval = vfs_pathbuf_copyin(a_upath, &kpath, &kpath_len);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }
        defer(vfs_pathbuf_free(kpath));

        vfs_node_t vfsn_start = nullptr;

        retval = vfs_util_lookup_first_node(kpath, a_dfd, &vfsn_start, false /*Want lock, lookup will lock*/);
        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }
        defer(vfs_node_rel(vfsn_start));

        vfs_node_t vfsn_parent   = nullptr;
        const char *child_name   = nullptr;
        natural_t child_name_len = 0;

        retval = vfs_node_lookup_create(vfsn_start,
                                        kpath,
                                        kpath_len,
                                        &vfsn_parent,
                                        &child_name,
                                        &child_name_len,
                                        nullptr,
                                        false,
                                        true);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        retval = vfs_node_mkdir(vfsn_parent, child_name, child_name_len, a_mode);

        vfs_node_put(vfsn_parent);

        return retval;
}

// ********************************************************************************************************************************
kern_return_t sys_mkdir(pthread_t a_proc, char const *a_path, mode_t a_mode)
// ********************************************************************************************************************************
{
        return sys_mkdirat(a_proc, AT_FDCWD, a_path, a_mode);
}
