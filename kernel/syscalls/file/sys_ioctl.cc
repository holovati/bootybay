#include <mach/tmpsys.h>

#include <fcntl.h>
#include <sys/ioctl.h>

#include <emerixx/fdesc.hh>
#include <emerixx/ioctl_req.h>
#include <emerixx/vfs/vfs_file.h>
#include <emerixx/process.h>

// ********************************************************************************************************************************
kern_return_t sys_ioctl(pthread_t a_thread, int a_fd, int a_cmd, natural_t a_arg)
// ********************************************************************************************************************************
{
        // Intercept "ioctls" for fcntl
        switch (a_cmd)
        {
                case FIONCLEX:
                        return fd_context_fcntl(a_thread->m_fdctx, a_fd, F_SETFD, 0);
                case FIOCLEX:
                        return fd_context_fcntl(a_thread->m_fdctx, a_fd, F_SETFD, FD_CLOEXEC);
                default:
                        [[fallthrough]];
        }

        vfs_file_t vfsf = nullptr;

        kern_return_t retval = fd_context_get_file(a_thread->m_fdctx, a_fd, &vfsf);

        if (KERN_STATUS_SUCCESS(retval))
        {
                struct ioctl_req_data ioctl_req;
                retval = ioctl_req_create(((integer_t)a_cmd) & 0xFFFFFFFF, (void *)(a_arg), true, &ioctl_req);
                if (KERN_STATUS_SUCCESS(retval))
                {
                        retval = vfs_file_ioctl(vfsf, &ioctl_req);
                }

                vfs_file_rel(vfsf);
        }

        return retval;
}
