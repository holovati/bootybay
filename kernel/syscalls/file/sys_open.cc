#include <mach/tmpsys.h>

#include <fcntl.h>
#include <sys/stat.h>

#include <aux/defer.hh>

#include <emerixx/vfs.h>
#include <emerixx/vfs/vfs_node_data.h>
#include <emerixx/process.h>
#include <emerixx/fdesc.hh>

// ********************************************************************************************************************************
static kern_return_t vfs_node_to_fd(vfs_node_t a_vfsn, int a_oflags)
// ********************************************************************************************************************************
{
        vfs_file_t vfsf = nullptr;

        kern_return_t retval = vfs_file_open(a_vfsn, a_oflags, &vfsf);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        retval = fd_context_fdalloc(uthread_self(), vfsf);

        if (KERN_STATUS_FAILURE(retval))
        {
                vfs_file_close(vfsf);
                return retval;
        }

        vfs_file_rel(vfsf); // Will gain a extra ref when we allocate a fd

        return retval;
}

// ********************************************************************************************************************************
static kern_return_t open_existing_vfsn(vfs_node_t a_vfsn, int a_oflags)
// ********************************************************************************************************************************
{
        if ((a_oflags & O_NOFOLLOW) && (S_ISLNK(a_vfsn->m_mode) == true))
        {
                return KERN_FAIL(ELOOP);
        }

        if ((a_oflags & (O_WRONLY | O_RDWR)) && (S_ISDIR(a_vfsn->m_mode) == true))
        {
                return KERN_FAIL(EISDIR);
        }

        if ((a_oflags & O_DIRECTORY) && (S_ISDIR(a_vfsn->m_mode) == false))
        {
                return KERN_FAIL(ENOTDIR);
        }

        if ((a_oflags & O_TRUNC) && (S_ISREG(a_vfsn->m_mode) == true))
        {
                kern_return_t retval = vfs_node_truncate(a_vfsn, 0);

                if (KERN_STATUS_FAILURE(retval))
                {
                        return retval;
                }
        }

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t create_file(pthread_t a_thread, unsigned int a_dirfd, const char *a_upath, int a_flags, mode_t a_mode)
// ********************************************************************************************************************************
{
        if (a_mode & S_IFMT)
        {
                // No type setting, regular is implicit.
                return KERN_FAIL(EINVAL);
        }

        char const *kpath    = nullptr;
        natural_t kpath_len  = 0;
        kern_return_t retval = vfs_pathbuf_copyin(a_upath, &kpath, &kpath_len);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }
        defer(vfs_pathbuf_free(kpath));

        vfs_node_t vfsn_start = nullptr;
        retval                = vfs_util_lookup_first_node(kpath, a_dirfd, &vfsn_start, false /* want lock*/);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }
        defer(vfs_node_rel(vfsn_start));

        vfs_node_t vfsn_parent   = nullptr;
        vfs_node_t vfsn_child    = nullptr;
        const char *child_name   = nullptr;
        natural_t child_name_len = 0;

        retval = vfs_node_lookup_create(vfsn_start,
                                        kpath,
                                        kpath_len,
                                        &vfsn_parent,
                                        &child_name,
                                        &child_name_len,
                                        &vfsn_child,
                                        (a_flags & O_NOFOLLOW) == 0,
                                        false);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        if (vfsn_child != nullptr)
        {
                KASSERT(vfsn_parent == nullptr);

                retval = open_existing_vfsn(vfsn_child, a_flags);

                if (KERN_STATUS_SUCCESS(retval))
                {
                        retval = vfs_node_to_fd(vfsn_child, a_flags);
                }

                vfs_node_put(vfsn_child);
        }
        else
        {
                retval = vfs_node_create(vfsn_parent, child_name, child_name_len, a_mode | S_IFREG, &vfsn_child);

                if (KERN_STATUS_SUCCESS(retval))
                {
                        retval = vfs_node_to_fd(vfsn_child, a_flags);
                        vfs_node_rel(vfsn_child);
                }

                vfs_node_put(vfsn_parent);
        }

        return retval;
}

// ********************************************************************************************************************************
static kern_return_t open_file(pthread_t a_thread, unsigned int a_dirfd, const char *a_fname, int a_flags)
// ********************************************************************************************************************************
{
        vfs_node_t vfsn = nullptr;

        kern_return_t retval
                = vfs_util_simple_lookup_usr_path(a_fname, a_dirfd, &vfsn, (a_flags & O_NOFOLLOW) == 0, true /*Want lock*/);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        retval = open_existing_vfsn(vfsn, a_flags);

        if (KERN_STATUS_FAILURE(retval))
        {
                vfs_node_put(vfsn);
                return retval;
        }

        retval = vfs_node_to_fd(vfsn, a_flags);

        vfs_node_put(vfsn);

        return retval;
}

// ********************************************************************************************************************************
kern_return_t sys_openat(pthread_t a_thread, unsigned int a_dirfd, const char *a_fname, int a_flags, mode_t a_mode)
// ********************************************************************************************************************************
{
        vfs_rename_lock_shared();
        defer(vfs_rename_lock_shared_done());

        if (a_flags & O_PATH)
        {
                a_flags &= O_CLOEXEC | O_DIRECTORY | O_NOFOLLOW | O_PATH;
        }

        kern_return_t retval;

        if (a_flags & O_CREAT)
        {
                retval = create_file(a_thread, a_dirfd, a_fname, a_flags, a_mode);
        }
        else
        {
                retval = open_file(a_thread, a_dirfd, a_fname, a_flags);
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t sys_open(pthread_t a_procp, char const *a_pathp, int flags, mode_t a_mode)
// ********************************************************************************************************************************
{
        return sys_openat(a_procp, AT_FDCWD, a_pathp, flags, a_mode);
}

// The creat() function shall behave as if it is implemented as follows:
// ********************************************************************************************************************************
kern_return_t sys_creat(pthread_t a_proc, char const *a_path, mode_t a_mode)
// ********************************************************************************************************************************
{
        return sys_open(a_proc, a_path, O_WRONLY | O_CREAT | O_TRUNC, a_mode);
}
