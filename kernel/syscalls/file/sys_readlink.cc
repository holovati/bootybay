#include <fcntl.h>
#include <string.h>

#include <aux/defer.hh>

#include <mach/tmpsys.h>

#include <emerixx/vfs.h>

#include <emerixx/memory_rw.h>

kern_return_t sys_readlinkat(pthread_t a_proc, int a_dfd, char const *a_path, char *a_buf, int a_bufsize)
{
        vfs_node_t vfsn = nullptr;

        kern_return_t retval = vfs_util_simple_lookup_usr_path(a_path, a_dfd, &vfsn, false, true /* Want lock*/);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        defer(vfs_node_put(vfsn));

        natural_t pathbuf_len = 0;
        char *pathbuf         = vfs_pathbuf_alloc(&pathbuf_len);
        defer(vfs_pathbuf_free(pathbuf));

        retval = vfs_node_readlink(vfsn, pathbuf, &pathbuf_len);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        if (pathbuf_len > (natural_t)a_bufsize)
        {
                return KERN_FAIL(ENAMETOOLONG);
        }

        retval = copyout(pathbuf, a_buf, pathbuf_len);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        return (kern_return_t)(pathbuf_len);
}

kern_return_t sys_readlink(pthread_t a_proc, char const *a_path, char *a_buf, int a_bufsize)
{
        return sys_readlinkat(a_proc, AT_FDCWD, a_path, a_buf, a_bufsize);
}
