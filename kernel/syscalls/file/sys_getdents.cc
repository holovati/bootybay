#include <mach/tmpsys.h>

#include <fcntl.h>

#include <emerixx/vfs.h>
#include <emerixx/vfs/vfs_file_data.h>
#include <emerixx/memory_rw.h>
#include <emerixx/fdesc.hh>
#include <emerixx/process.h>


// ********************************************************************************************************************************
kern_return_t sys_getdents64(pthread_t a_thread, int a_fd, struct linux_dirent64 *a_buf, size_t a_count)
// ********************************************************************************************************************************
{
        vfs_file_t fp;

        kern_return_t retval = fd_context_get_file(a_thread->m_fdctx, a_fd, &fp);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        struct uio auio;
        if (fp->m_oflags & O_WRONLY)
        {
                retval = KERN_FAIL(EBADF);
        }
        else
        {
                struct iovec aiov;

                aiov.iov_base   = a_buf;
                aiov.iov_len    = a_count;
                auio.uio_iov    = &aiov;
                auio.uio_iovcnt = 1;
                auio.uio_rw     = UIO_READ;
                auio.uio_segflg = UIO_USERSPACE;
                auio.uio_resid  = a_count;

                retval = vfs_file_getdents(fp, &auio);
        }
        
        vfs_file_rel(fp);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        return (kern_return_t)(a_count - auio.uio_resid);
}