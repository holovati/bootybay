#include <mach/tmpsys.h>

#include <aux/defer.hh>

#include <fcntl.h>
#include <sys/stat.h>

#include <emerixx/vfs.h>
// ********************************************************************************************************************************
kern_return_t sys_linkat(pthread_t a_proc, int a_path_dfd, char const *a_path, int a_lnk_dfd, char const *a_lnk_path, int a_flags)
// ********************************************************************************************************************************
{
        vfs_rename_lock_shared();
        defer(vfs_rename_lock_shared_done());

        // Get the target vnode
        vfs_node_t vfsn_target = nullptr;

        kern_return_t retval = vfs_util_simple_lookup_usr_path(a_path,
                                                               a_path_dfd,
                                                               &vfsn_target,
                                                               !!(a_flags & AT_SYMLINK_FOLLOW),
                                                               false /* no need to lock the target*/);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }
        defer(vfs_node_rel(vfsn_target));

        // Get the link path
        char const *lnkpath   = nullptr;
        natural_t lnkpath_len = 0;
        retval                = vfs_pathbuf_copyin(a_lnk_path, &lnkpath, &lnkpath_len);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }
        defer(vfs_pathbuf_free(lnkpath));

        // Get the starting node
        vfs_node_t vfsn_start = nullptr;

        retval = vfs_util_lookup_first_node(lnkpath, a_lnk_dfd, &vfsn_start, false /* Lookup will lock the node before usage */);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }
        defer(vfs_node_rel(vfsn_start));

        // Do a lookup, if child exists we fail
        vfs_node_t vfsn_parent   = nullptr;
        const char *child_name   = nullptr;
        natural_t child_name_len = 0;

        retval = vfs_node_lookup_create(vfsn_start,
                                        lnkpath,
                                        lnkpath_len,
                                        &vfsn_parent,
                                        &child_name,
                                        &child_name_len,
                                        nullptr,
                                        false,
                                        false);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        if (vfsn_parent == vfsn_target)
        {
                retval = KERN_FAIL(EPERM);
        }
        else
        {
                retval = vfs_node_link(vfsn_parent, vfsn_target, child_name, child_name_len);
        }

        vfs_node_put(vfsn_parent);

        return retval;
}

// ********************************************************************************************************************************
int sys_link(pthread_t a_proc, char const *a_path, char const *a_link_path)
// ********************************************************************************************************************************
{
        return sys_linkat(a_proc, AT_FDCWD, a_path, AT_FDCWD, a_link_path, 0 /* Do not follow symlink */);
}
