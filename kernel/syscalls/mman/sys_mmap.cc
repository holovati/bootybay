#include <mach/tmpsys.h>

#include <mach/param.h>

#include <sys/mman.h>

#include <aux/defer.hh>

#include <kernel/vm/vm_map.h>
#include <kernel/vm/vm_object.h>
#include <kernel/vm/vm_param.h>
#include <kernel/vm/vm_prot.h>
#include <kernel/thread_data.h>

#include <emerixx/fdesc.hh>
#include <emerixx/process.h>
#include <emerixx/vfs/vfs_file.h>

// Make sure our prot flags are the same as userspace
static_assert(PROT_READ == VM_PROT_READ && PROT_WRITE == VM_PROT_WRITE && PROT_EXEC == VM_PROT_EXECUTE && PROT_NONE == VM_PROT_NONE,
              "User/Kernel protection flags mismatch");

vm_address_t sys_mmap(pthread_t a_thread, vm_address_t a_addr, size_t a_len, int a_prot, int a_flags, int a_fd, off_t a_pgoff)
{
        a_len  = round_page(a_len);
        a_addr = trunc_page(a_addr);

        if ((a_flags & (MAP_SHARED | MAP_PRIVATE)) == (MAP_SHARED | MAP_PRIVATE))
        {
                return KERN_FAIL(EINVAL);
        }

        if (!a_addr)
        {
                a_addr = 0x40000000;
        }

        // The value of len is zero.
        if (!a_len)
        {
                return KERN_FAIL(EINVAL);
        }

        if ((a_addr + round_page(a_len)) >= VM_MAX_ADDRESS)
        {
                return KERN_FAIL(EINVAL);
        }

        if (!page_aligned(a_pgoff))
        {
                return KERN_FAIL(EINVAL);
        }

        vm_object_t vmobj   = nullptr;
        vm_map_t map        = current_task()->map;
        uint32_t vmentflags = 0;

        if (a_flags & MAP_SHARED)
        {
                vmentflags |= VM_MAP_ENTRY_IS_SHARED;
        }
        else
        {
                vmentflags |= VM_MAP_ENTRY_NEEDS_COPY;
        }

        if ((a_fd == -1) || (a_flags & MAP_ANON))
        {
                // KASSERT((a_flags & MAP_SHARED) == 0); // Not ready
                a_pgoff = 0;
                kern_return_t retval
                        = vm_map_find(map, vmobj, (vm_offset_t)a_pgoff, &a_addr, a_len, (a_flags & MAP_FIXED) == 0, nullptr);

                if ((retval == KERN_NO_SPACE) && (a_flags & MAP_FIXED))
                {
                        retval = vm_map_delete(map, a_addr, a_addr + a_len);
                        KASSERT(retval == KERN_SUCCESS);
                        retval = vm_map_find(map, vmobj, (vm_offset_t)a_pgoff, &a_addr, a_len, (a_flags & MAP_FIXED) == 0, nullptr);
                }

                KASSERT(retval == KERN_SUCCESS);
                retval = vm_map_protect(map, a_addr, a_addr + a_len, a_prot, false);
                KASSERT(retval == KERN_SUCCESS);
                vm_map_entry *ent;
                KASSERT(vm_map_lookup_entry(map, a_addr, &ent) == true);
                ent->__pad = a_flags;
                ent->flags |= vmentflags;

                return a_addr;
        }

        vfs_file_t f = nullptr;

        kern_return_t retval = fd_context_get_file(a_thread->m_fdctx, a_fd, &f);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        defer(vfs_file_rel(f));

        retval = vfs_file_mmap(f, &vmobj);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        defer(vm_object_rel(vmobj));

        // KASSERT((a_flags & MAP_SHARED) == 0); // Not ready

        retval = vm_map_find(map, vmobj, (vm_offset_t)a_pgoff, &a_addr, a_len, (a_flags & MAP_FIXED) == 0, nullptr);
        if ((retval == KERN_NO_SPACE) && (a_flags & MAP_FIXED))
        {
                retval = vm_map_delete(map, a_addr, a_addr + a_len);
                KASSERT(retval == KERN_SUCCESS);
                retval = vm_map_find(map, vmobj, (vm_offset_t)a_pgoff, &a_addr, a_len, (a_flags & MAP_FIXED) == 0, nullptr);
        }
        KASSERT(retval == KERN_SUCCESS);
        retval = vm_map_protect(map, a_addr, a_addr + a_len, a_prot, false);
        KASSERT(retval == KERN_SUCCESS);

        vm_map_entry *ent;
        KASSERT(vm_map_lookup_entry(map, a_addr, &ent) == true);
        ent->__pad = a_flags;
        ent->flags |= vmentflags;

        return a_addr;

        panic("Naynay");
        if (a_fd != -1)
        {
                panic("ad");
#if 0
                vnode::ptr vnptr = filep->m_vnode;

                // The fildes argument refers to a file whose type is not supported by mmap
                if (vnptr->is_memory_mappable() == false)
                {
                        // Not mappable
                        return KERN_FAIL(ENODEV);
                }
                // The file is a regular file and the value of off plus len exceeds the offset maximum established in the open file
                // description associated with fildes.
                // if (!((a_pgoff + a_len) <= file_object->m_size))
                //{
                //        return -EOVERFLOW;
                //}

                (vmobj = vnptr.get_raw())->reference();

                if (a_flags & MAP_SHARED)
                {
                        vmentflags |= VM_MAP_ENTRY_IS_SHARED;
                }
                else
                {
                        vmentflags |= VM_MAP_ENTRY_NEEDS_COPY;
                }
#endif
        }
        else
        {
                panic("adf");
#if 0
                KASSERT((a_flags & MAP_SHARED) == 0); // Not ready

                (vmobj = vm_object::get_zero_object())->reference();
                a_pgoff = 0;
#endif
                vmentflags |= VM_MAP_ENTRY_NEEDS_COPY;
        }

        // In case we fail, a new ref is added in case of success
        panic("ads");
        ;
        // defer(vmobj->unreference());

        kern_return_t result = vm_map_find(map, vmobj, (vm_offset_t)a_pgoff, &a_addr, a_len, (a_flags & MAP_FIXED) == 0, nullptr);

        if ((result == KERN_NO_SPACE) && (a_flags & MAP_FIXED))
        {
                result = vm_map_delete(map, a_addr, a_addr + a_len);
                KASSERT(result == KERN_SUCCESS);
                result = vm_map_find(map, vmobj, (vm_offset_t)a_pgoff, &a_addr, a_len, (a_flags & MAP_FIXED) == 0, nullptr);
        }

        if (result != KERN_SUCCESS)
        {
                return KERN_FAIL(EINVAL);
        }

#if 0
        // MAP_SHARED and MAP_PRIVATE describe the disposition of write references to the memory object. If MAP_SHARED is specified,
        // write references shall change the underlying object. If MAP_PRIVATE is specified, modifications to the mapped data by the
        // calling process shall be visible only to the calling process and shall not change the underlying object. It is
        // unspecified whether modifications to the underlying object done after the MAP_PRIVATE mapping is established are visible
        // through the MAP_PRIVATE mapping. Either MAP_SHARED or MAP_PRIVATE can be specified, but not both. The mapping type is
        // retained across fork().
        if (vnptr.is_null() == false)
        {
                KASSERT((a_flags & MAP_SHARED) == 0); // Not ready, should be easy to implement

                if (vnptr->m_size < a_len)
                {
                        vm_address_t sig_bus_addr_start = a_addr + vnptr->m_size;
                        vm_address_t sig_bus_addr_end   = sig_bus_addr_start + (a_len - vnptr->m_size);
                        size_t sig_bus_block_size       = sig_bus_addr_end - sig_bus_addr_start;

                        result = vm_map_delete(map, sig_bus_addr_start, sig_bus_addr_end);
                        KASSERT(result == KERN_SUCCESS);

                        result = vm_map_find(
                            map, sigbus_object::instance(), 0, &sig_bus_addr_start, sig_bus_block_size, false, nullptr);
                        KASSERT(result == KERN_SUCCESS);

                        sigbus_object::instance()->reference();
                }

                vm_map_entry *ent;
                KASSERT(vm_map_lookup_entry(map, a_addr, &ent));
                if ((a_flags & MAP_PRIVATE) && (a_prot & VM_PROT_WRITE))
                {
                        ent->flags |= VM_MAP_ENTRY_NEEDS_COPY;
                }
        }

        result = vm_map_protect(map, trunc_page(a_addr), round_page(a_addr + a_len), a_prot, false);

        KASSERT(result == KERN_SUCCESS);

        // Experiment. save the mmap flags
        vm_map_entry *ent;
        KASSERT(vm_map_lookup_entry(map, a_addr, &ent) == true);
        ent->__pad = a_flags;
        ent->flags |= vmentflags;
        // Everything ok, add a ref to the object
        //vmobj->reference();
#endif
        return a_addr;
}

kern_return_t sys_munmap(pthread_t a_thread, vm_address_t a_addr, size_t a_len)
{
        // The len argument is 0.
        if (a_len == 0)
        {
                return -EINVAL;
        }

        // Dont attempt to unmap 0
        if (!a_addr)
        {
                return KERN_SUCCESS;
        }

        // The addr argument is not a multiple of the page size as returned by sysconf().
        if (IS_ALIGNED(a_addr, PAGE_SIZE) == false)
        {
                return -EINVAL;
        }

        // Addresses in the range [addr,addr+len) are outside the valid range for the address space of a process.
        if (((a_addr + round_page(a_len)) < VM_MAX_ADDRESS) == false)
        {
                return -EINVAL;
        }

        vm_map_delete(current_task()->map, a_addr, a_addr + a_len);

        return 0;
}
