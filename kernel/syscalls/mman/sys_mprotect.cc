#include <mach/tmpsys.h>

#include <kernel/vm/vm_map.h>
#include <kernel/vm/vm_param.h>
#include <kernel/vm/vm_prot.h>
#include <kernel/thread_data.h>

#include <emerixx/process.h>

int sys_mprotect(pthread_t a_thread, vm_address_t a_addr, size_t a_len, vm_prot_t a_prot)
{
        if ((a_addr & PAGE_MASK) || (a_len == 0))
        {
                return KERN_FAIL(EINVAL);
        }

        switch (vm_map_protect(current_task()->map, a_addr, a_addr + a_len, a_prot & VM_PROT_ALL, FALSE))
        {
                case KERN_SUCCESS:
                        return (0);
                case KERN_PROTECTION_FAILURE:
                        return KERN_FAIL(EACCES);
                default:
                        return KERN_FAIL(EINVAL);
        }
        return KERN_FAIL(EINVAL);
}
