#include <mach/tmpsys.h>

#include <sys/mman.h>

kern_return_t sys_madvise(pthread_t a_thread, vm_address_t a_start, size_t a_len, int a_type)
{
        int err = -1;
        switch (a_type)
        {
        case MADV_NORMAL: {
                /* No special treatment.  This is the default. */
                err = 0;
                break;
        }

        case MADV_RANDOM: {
                /*
                 *Expect page references in random order.  (Hence, read ahead may be less
                 * useful than normally.)
                 */
                panic("madvise ni\n");
                break;
        }

        case MADV_SEQUENTIAL: {
                panic("madvise ni\n");
                break;
        }

        case MADV_WILLNEED: {
                panic("madvise ni\n");
                break;
        }

        case MADV_DONTNEED: {
                /*
                  Do not expect access in the near future.  (For the time being,
                  the application is finished with the given range, so the
                  kernel can free resources associated with it.)

                  After a successful MADV_DONTNEED operation, the semantics of
                  memory access in the specified region are changed: subsequent
                  accesses of pages in the range will succeed, but will result
                  in either repopulating the memory contents from the up-to-date
                  contents of the underlying mapped file (for shared file
                  mappings, shared anonymous mappings, and shmem-based
                  techniques such as System V shared memory segments) or zero-
                  fill-on-demand pages for anonymous private mappings.

                  Note that, when applied to shared mappings, MADV_DONTNEED
                  might not lead to immediate freeing of the pages in the range.
                  The kernel is free to delay freeing the pages until an
                  appropriate moment.  The resident set size (RSS) of the
                  calling process will be immediately reduced however.

                  MADV_DONTNEED cannot be applied to locked pages, Huge TLB
                  pages, or VM_PFNMAP pages.  (Pages marked with the kernel-
                  internal VM_PFNMAP flag are special memory areas that are not
                  managed by the virtual memory subsystem.  Such pages are
                  typically created by device drivers that map the pages into
                  user space.)
                */

                /* Okay for now */
                err = 0;
                break;
        }

        case MADV_FREE: {
                panic("madvise ni\n");
                break;
        }

        case MADV_REMOVE: {
                panic("madvise ni\n");
                break;
        }

        case MADV_DONTFORK: {
                panic("madvise ni\n");
                break;
        }

        case MADV_DOFORK: {
                panic("madvise ni\n");
                break;
        }

        case MADV_MERGEABLE: {
                panic("madvise ni\n");
                break;
        }

        case MADV_UNMERGEABLE: {
                panic("madvise ni\n");
                break;
        }

        case MADV_HUGEPAGE: {
                panic("madvise ni\n");
                break;
        }

        case MADV_NOHUGEPAGE: {
                panic("madvise ni\n");
                break;
        }

        case MADV_DONTDUMP: {
                panic("madvise ni\n");
                break;
        }

        case MADV_DODUMP: {
                panic("madvise ni\n");
                break;
        }

        case MADV_WIPEONFORK: {
                panic("madvise ni\n");
                break;
        }

        case MADV_KEEPONFORK: {
                panic("madvise ni\n");
                break;
        }

        case MADV_HWPOISON: {
                panic("madvise ni\n");
                break;
        }

        case MADV_SOFT_OFFLINE: {
                panic("madvise ni\n");
                break;
        }

        default: {
                /* Set errno to EINVAL */
                break;
        }
        }
        return err;
}
