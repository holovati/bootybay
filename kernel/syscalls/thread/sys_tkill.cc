#include <emerixx/process.h>
#include <mach/tmpsys.h>

kern_return_t sys_tkill(pid_t a_tid, int a_sig)
{
        if ((a_tid < 1) || (a_sig < 1) || (a_sig > 64))
        {
                return KERN_FAIL(EINVAL);
        }

        pthread_t uthrd;
        kern_return_t retval = uthread_find(a_tid, &uthrd);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        return uthread_signal(uthrd, a_sig);
}

kern_return_t sys_tgkill(pid_t a_pid, pid_t a_tid, int a_sig)
{
        if ((a_pid < 1) || (a_sig < 1) || (a_sig > 64))
        {
                return KERN_FAIL(EINVAL);
        }

        pthread_t uthrd;
        kern_return_t retval = uthread_find(a_tid, &uthrd);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        if (uthrd->m_pid != a_pid)
        {
                return KERN_FAIL(ESRCH);
        }

        return uthread_signal(uthrd, a_sig);
}
