#include <mach/tmpsys.h>

#include <kernel/sched.h>
#include <kernel/thread_data.h>

kern_return_t sys_sched_yield(pthread_t)
{
        thread_block(nullptr);
        return KERN_SUCCESS;
}