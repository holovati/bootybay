#include <mach/tmpsys.h>
#include <fcntl.h>

#include <etl/algorithm.hh>

#include <emerixx/fs/pipefs/pipefs.h>
#include <emerixx/vfs/vfs_file.h>
#include <emerixx/memory_rw.h>

#include <emerixx/fdesc.hh>
#include <emerixx/process.h>

// ********************************************************************************************************************************
kern_return_t sys_pipe(pthread_t a_thread, int *a_fildes)
// ********************************************************************************************************************************
{
        return sys_pipe2(a_thread, a_fildes, 0);
}

// ********************************************************************************************************************************
kern_return_t sys_pipe2(pthread_t a_thread, int *a_fildes, int a_oflags)
// ********************************************************************************************************************************
{
        vfs_file_t vfsf[2];
        etl::clear(vfsf);

        a_oflags &= (O_CLOEXEC | O_DIRECT | O_NONBLOCK);

        kern_return_t retval = pipefs_pipe2(a_oflags, vfsf);

        if (KERN_STATUS_SUCCESS(retval))
        {
                int retfd[2];

                retfd[0] = fd_context_fdalloc(a_thread, vfsf[0]);

                if (KERN_STATUS_FAILURE(retfd[0]))
                {
                        goto out;
                }

                retfd[1] = fd_context_fdalloc(a_thread, vfsf[1]);

                if (KERN_STATUS_FAILURE(retfd[1]))
                {
                        fd_context_fdclose(a_thread->m_fdctx, retfd[0]);
                        goto out;
                }

                retval = copyout(retfd, a_fildes, sizeof(retfd));

                if (KERN_STATUS_FAILURE(retval))
                {
                        fd_context_fdclose(a_thread->m_fdctx, retfd[0]);
                        fd_context_fdclose(a_thread->m_fdctx, retfd[1]);
                }
        out:
                vfs_file_rel(vfsf[0]);
                vfs_file_rel(vfsf[1]);
        }

        return retval;
}