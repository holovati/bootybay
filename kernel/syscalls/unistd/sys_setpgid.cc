#include <mach/tmpsys.h>

#include <kernel/thread_data.h>

#include <emerixx/process.h>

extern struct process *PER_TASK_VAR(__s_curproc);

pid_t sys_setpgid(pthread_t a_thread, pid_t a_pid, pid_t a_pgid)
{
        if (a_pid < 0 || a_pgid < 0)
        {
                return KERN_FAIL(EINVAL);
        }

        if (a_pid == 0)
        {
                a_pid = a_thread->m_pid;
        }

        process_t utask;
        kern_return_t retval = utask_find(a_pid, &utask); // Check if inferior

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        if (a_pgid == 0)
        {
                if (a_thread->m_pid == a_pid)
                {
                        a_pgid = utask->m_pgid;
                }
                else
                {
                        a_pgid = (*PER_TASK_PTR(__s_curproc))->m_pgid;
                }
        }

        if (utask->m_pgid == a_pgid)
        {
                return KERN_SUCCESS;
        }

        return utask_set_group_id(utask, a_pgid);
}
