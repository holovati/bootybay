#include <emerixx/fdesc.hh>
#include <emerixx/process.h>
#include <mach/tmpsys.h>

//
kern_return_t sys_dup2(pthread_t a_thread, int a_oldfd, int a_newfd)
//
{
        return fd_context_dup2(a_thread->m_fdctx, a_oldfd, a_newfd);
}
