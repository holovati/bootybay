#include <mach/tmpsys.h>

#include <emerixx/process.h>

pid_t sys_getpgid(pthread_t a_pthread, pid_t a_pid)
{
        process_t p;

        if (a_pid == 0)
        {
                a_pid = a_pthread->m_pid;
        }

        kern_return_t retval = utask_find(a_pid, &p);

        if (KERN_STATUS_SUCCESS(retval))
        {
                retval = p->m_pgid;
        }

        return retval;
}

pid_t sys_getpgrp(pthread_t a_pthread) { return sys_getpgid(a_pthread, a_pthread->m_pid); }
