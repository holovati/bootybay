#include <emerixx/process.h>
#include <mach/tmpsys.h>
#include <sys/sysmacros.h>

pid_t sys_setsid(pthread_t a_pthread)
{
        // panic("brah");
        process_t utask = utask_self();

        if ((utask->m_sid == utask->m_pid) && (utask->m_sid == utask->m_pgid))
        {
                return utask->m_pid;
        }

        KASSERT(utask->m_sid != utask->m_pid); // We don't support session leaders leaving yet.

        utask->m_sid  = utask->m_pid;
        utask->m_ctty = makedev(0xFF, 0xFF);
        utask_set_group_id(utask, utask->m_pid);

        return utask->m_sid;
#if 0
        return KERN_SUCCESS;

        process_t a_procp = a_pthread->process;

        if (a_procp->p_pgrp->pg_id == a_procp->p_pid || proc_group_find((pid_t)a_procp->p_pid))
        {
                return -EPERM;
        }
        else
        {
                proc_group_enter(a_procp, (pid_t)a_procp->p_pid, 1);
                return (int)a_procp->p_pid;
        }
#endif
}
