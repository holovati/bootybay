#include <emerixx/fdesc.hh>
#include <emerixx/process.h>
#include <mach/tmpsys.h>

// ********************************************************************************************************************************
kern_return_t sys_dup(pthread_t a_thread, int a_fd)
// ********************************************************************************************************************************
{
        return fd_context_dup(a_thread->m_fdctx, a_fd);
}
