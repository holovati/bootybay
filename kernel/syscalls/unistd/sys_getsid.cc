#include <mach/tmpsys.h>
#include <pthread.h>
#include <unistd.h>
#include <emerixx/process.h>

pid_t sys_getsid(pthread_t a_uthread, pid_t a_pid)
{
        if (a_pid < 0)
        {
                return KERN_FAIL(EINVAL);
        }
        process_t p;
        if (a_pid == 0)
        {
                p = utask_self();
        }
        else
        {
                kern_return_t retval = utask_find(a_pid, &p);
                if (KERN_STATUS_FAILURE(retval))
                {
                        return retval;
                }
        }

        // What if there is no leader
        // KASSERT(p->m_sid != nullptr);

        return p->m_sid;
}
