#include <mach/tmpsys.h>
#include <emerixx/memory_rw.h>

int sys_getgroups(pthread_t a_procp, int a_gidsetsize, gid_t *a_gidset)
{

        if (a_gidsetsize < 0)
        {
                return KERN_FAIL(EINVAL);
        }

        if (a_gidsetsize == 0)
        {
                return 1;
        }

        kern_return_t retval = emerixx::uio::copyout(0u, a_gidset);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        return 1;

#if 0
        struct pcred *pc = a_procp->process->p_cred;
        size_t ngrp;
        int error;

        if ((ngrp = (size_t)a_gidsetsize) == 0)
        {
                return pc->pc_ucred->cr_ngroups;
        }

        if (ngrp < pc->pc_ucred->cr_ngroups)
        {
                return -EINVAL;
        }

        ngrp = pc->pc_ucred->cr_ngroups;

        error = copyout(pc->pc_ucred->cr_groups, a_gidset, ngrp * sizeof(gid_t));
        if (error)
        {
                return (error);
        }

        return ngrp;
#endif
}
