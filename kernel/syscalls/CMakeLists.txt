
add_library(syscalls OBJECT
    file/sys_open.cc
    file/sys_fstat.cc
    file/sys_chown.cc
    file/sys_chmod.cc
    file/sys_unlink.cc
    file/sys_readlink.cc
    file/sys_access.cc
    file/sys_symlink.cc
    file/sys_chdir.cc
    file/sys_chroot.cc
    file/sys_mkdir.cc
    file/sys_rename.cc
    file/sys_mknod.cc
    file/sys_utime.cc
    file/sys_truncate.cc
    file/sys_link.cc
    file/sys_read.cc
    file/sys_write.cc
    file/sys_statfs.cc
    file/sys_ioctl.cc
    file/sys_fcntl.cc
    file/sys_getdents.cc
    file/sys_close.cc
    file/sys_sync.cc
    file/sys_fsync.cc
    file/sys_seek.cc

    stat/sys_umask.cc

    fcntl/sys_fadvise.cc

    unistd/sys_pipe.cc
    unistd/sys_getuid.cc
    unistd/sys_geteuid.cc
    unistd/sys_getgid.cc
    unistd/sys_getegid.cc
    unistd/sys_setuid.cc
    unistd/sys_setgid.cc
    unistd/sys_getpgid.cc
    unistd/sys_setpgid.cc
    unistd/sys_getgroups.cc
    unistd/sys_setsid.cc
    unistd/sys_getsid.cc
    unistd/sys_dup.cc
    unistd/sys_dup2.cc

    process/sys_getcwd.cc
    process/sys_execve.cc
    process/sys_brk.cc
    process/sys_fork.cc
    process/sys_getpid.cc
    process/sys_getppid.cc
    process/sys_gettid.cc
    process/sys_sigaltstack.cc

    mman/sys_madvise.cc
    mman/sys_mlock.cc
    mman/sys_mmap.cc
    mman/sys_mprotect.cc
    mman/sys_msync.cc

    network/sys_socket.cc

    misc/sys_getrandom.cc
    misc/sys_uname.cc
    misc/sys_getrusage.cc

    select/sys_poll.cc
    select/sys_select.cc

    signal/sys_getitimer.cc
    signal/sys_setitimer.cc
    signal/sys_sigaction.cc
    signal/sys_sigprocmask.cc
    signal/sys_kill.cc
    signal/sys_sigreturn.cc
    signal/sys_sigsuspend.cc
    signal/sys_sigpending.cc

    sched/sys_sched_yield.cc
    sched/sys_sched_getaffinity.cc

    linux/sys_clone.cc
    linux/sys_mremap.cc
    linux/sys_membarrier.cc
    linux/sys_futex.cc
    linux/sys_wait4.cc
    linux/sys_arch_prctl.cc
    linux/sys_set_tid_address.cc
    linux/sys_sysinfo.cc
    linux/sys_prlimit.cc
    linux/sys_flock.cc
    linux/sys_setgroups.cc
    linux/sys_dup3.cc
    linux/sys_splice.cc

    exit/sys_exit_group.cc
    exit/sys_exit.cc

    time/sys_clock_gettime.cc
    time/sys_timer_gettime.cc
    time/sys_nanosleep.cc

    thread/sys_tkill.cc
)

target_include_directories(syscalls
    PRIVATE
    "${CMAKE_CURRENT_SOURCE_DIR}"
    SYSTEM $<TARGET_PROPERTY:fs,INTERFACE_INCLUDE_DIRECTORIES>
    SYSTEM $<TARGET_PROPERTY:etl,INTERFACE_INCLUDE_DIRECTORIES>
    SYSTEM $<TARGET_PROPERTY:aux,INTERFACE_INCLUDE_DIRECTORIES>
    SYSTEM $<TARGET_PROPERTY:net,INTERFACE_INCLUDE_DIRECTORIES>
    SYSTEM $<TARGET_PROPERTY:emerixx,INTERFACE_INCLUDE_DIRECTORIES>
)
