#include <mach/tmpsys.h>

#include <emerixx/fsdata.hh>
#include <emerixx/process.h>

mode_t sys_umask(pthread_t a_thread, mode_t a_newmask) { return fs_context_umask(a_thread->m_fsctx, a_newmask); }
