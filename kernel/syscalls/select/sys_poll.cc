#include <mach/tmpsys.h>

#include <aux/defer.hh>

#include <kernel/zalloc.h>

#include <emerixx/vfs/vfs_file.h>
#include <emerixx/vfs/vfs_file_data.h>
#include <emerixx/poll.h>
#include <emerixx/memory_rw.h>

struct pollfd_storage_data
{
        pollfd pfds[0x100];
};

ZONE_DEFINE(pollfd_storage_data, s_pollfd_storage, 0x100, 0x100 / 2, ZONE_EXHAUSTIBLE, 0x20);

kern_return_t sys_ppoll(pollfd *a_fds, nfds_t a_nfds, const timespec *a_ts, const sigset_t *a_sigmask, size_t a_sigsetsize)
{
        if (a_nfds > FILEMAX)
        {
                return KERN_FAIL(EINVAL);
        }

        if (a_nfds >= 10)
        {
                return KERN_FAIL(ENOMEM);
        }

        if (a_sigsetsize != sizeof(sigset_t))
        {
                return KERN_FAIL(EINVAL);
        }

        kern_return_t retval;

        sigset_t tmpsigmask, *sigmask = nullptr;
        if (a_sigmask)
        {
                retval = copyin(a_sigmask, &sigmask, a_sigsetsize);

                if (retval != KERN_SUCCESS)
                {
                        return retval;
                }

                sigmask = &tmpsigmask;
        }

        timespec tmpts, *ts = nullptr;
        if (a_ts != nullptr)
        {
                // TODO: Validate ts
                retval = emerixx::uio::copyin(a_ts, &tmpts);

                if (retval != KERN_SUCCESS)
                {
                        return retval;
                }

                ts = &tmpts;
        }

        pollfd *fds = (pollfd *)s_pollfd_storage.alloc();
        defer(s_pollfd_storage.free((pollfd_storage_data *)fds));

        retval = copyin(a_fds, fds, sizeof(pollfd) * a_nfds);

        if (retval != KERN_SUCCESS)
        {
                return retval;
        }

        retval = ppoll(fds, a_nfds, ts, sigmask);

        copyout(fds, a_fds, sizeof(pollfd) * a_nfds);

        return retval;
}

kern_return_t sys_poll(pollfd *a_fds, nfds_t a_nfds, int a_timeout)
{
        if (a_nfds > FILEMAX)
        {
                return KERN_FAIL(EINVAL);
        }

        if (a_nfds >= 10)
        {
                return KERN_FAIL(ENOMEM);
        }

        pollfd *fds = (pollfd *)s_pollfd_storage.alloc();
        defer(s_pollfd_storage.free((pollfd_storage_data *)fds));

        kern_return_t retval = copyin(a_fds, fds, sizeof(pollfd) * a_nfds);

        if (retval != KERN_SUCCESS)
        {
                return retval;
        }

        retval = poll(fds, a_nfds, a_timeout);

        copyout(fds, a_fds, sizeof(pollfd) * a_nfds);

        return retval;
}
