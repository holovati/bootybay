#include <mach/tmpsys.h>

#include <aux/time.h>
#include <emerixx/memory_rw.h>
#include <sys/select.h>

#define ARG_PTR(nm) __tmp##nm, *nm = &__tmp##nm

#define VALIDATE_NFDS(nfds)                                                                                                        \
        {                                                                                                                          \
                if (a_nfds < 0 || a_nfds > FD_SETSIZE)                                                                             \
                {                                                                                                                  \
                        return KERN_FAIL(EINVAL);                                                                                  \
                }                                                                                                                  \
        }

struct pselect6_data
{
        const sigset_t *ss; /* Pointer to signal set */
        size_t ss_len;      /* Size (in bytes) of object pointed to by 'ss' */
};

template <typename T> //
static kern_return_t arg_copyin(T const *a_utv, T **a_ktv)
{
        kern_return_t retval = KERN_SUCCESS;
        if (a_utv != nullptr)
        {
                retval = emerixx::uio::copyin(a_utv, *a_ktv);
        }
        else
        {
                *a_ktv = nullptr;
        }
        return retval;
}

static kern_return_t fd_set_copyin(fd_set **a_ufds, fd_set **a_kfds)
{
        kern_return_t retval = KERN_SUCCESS;
        for (size_t i = 0; i < 3; ++i)
        {
                retval = arg_copyin(a_ufds[i], &a_kfds[i]);

                if (retval != KERN_SUCCESS)
                {
                        break;
                }
        }
        return retval;
}

static kern_return_t fd_set_copyout(fd_set **a_ufds, fd_set **a_kfds)
{
        kern_return_t retval = KERN_SUCCESS;

        for (size_t i = 0; i < 3; ++i)
        {
                if (a_ufds[i] != nullptr)
                {
                        retval = emerixx::uio::copyout(a_kfds[i], a_ufds[i]);

                        if (retval != KERN_SUCCESS)
                        {
                                break;
                        }
                }
        }

        return retval;
}

kern_return_t sys_pselect6(int a_nfds, fd_set *a_rfds, fd_set *a_wfds, fd_set *a_efds, timespec *a_ts, const pselect6_data *a_mask)
{
        VALIDATE_NFDS(a_nfds);

        pselect6_data ARG_PTR(mask);
        kern_return_t retval = arg_copyin(a_mask, &mask);

        if (retval != KERN_SUCCESS)
        {
                return retval;
        }

        sigset_t ARG_PTR(sigmask);
        if (mask != nullptr)
        {
                retval = arg_copyin(mask->ss, &sigmask);

                if (retval != KERN_SUCCESS)
                {
                        return retval;
                }

                if ((sigmask != nullptr) && (mask->ss_len != sizeof(sigset_t)))
                {
                        return KERN_FAIL(EINVAL);
                }
        }

        timespec ARG_PTR(ts);
        retval = arg_copyin(a_ts, &ts);

        if (retval != KERN_SUCCESS)
        {
                return retval;
        }

        if (ts != nullptr && timespec_valid(ts) == false)
        {
                return KERN_FAIL(EINVAL);
        }

        fd_set fds[3];
        fd_set *ufds[3] = {a_rfds, a_wfds, a_efds};
        fd_set *kfds[3] = {&fds[0], &fds[1], &fds[2]};

        retval = fd_set_copyin(ufds, kfds);

        if (retval != KERN_SUCCESS)
        {
                return retval;
        }

        retval = pselect(a_nfds, kfds[0], kfds[1], kfds[2], ts, sigmask);

        if (retval < KERN_SUCCESS)
        {
                return retval;
        }

        kern_return_t nbits = retval;

        retval = fd_set_copyout(ufds, kfds);

        if (retval == KERN_SUCCESS)
        {
                retval = nbits;
        }

        return retval;
}

kern_return_t sys_select(int a_nfds, fd_set *a_rfds, fd_set *a_wfds, fd_set *a_efds, timeval *a_tv)
{
        VALIDATE_NFDS(a_nfds);

        timeval ARG_PTR(tv);
        kern_return_t retval = arg_copyin(a_tv, &tv); // TODO: Remember to validate tv

        if (retval < KERN_SUCCESS)
        {
                return retval;
        }

        fd_set fds[3];
        fd_set *ufds[3] = {a_rfds, a_wfds, a_efds};
        fd_set *kfds[3] = {&fds[0], &fds[1], &fds[2]};

        retval = fd_set_copyin(ufds, kfds);

        if (retval != KERN_SUCCESS)
        {
                return retval;
        }

        retval = select(a_nfds, kfds[0], kfds[1], kfds[2], tv);

        if (retval < KERN_SUCCESS)
        {
                return retval;
        }

        kern_return_t nbits = retval;

        retval = fd_set_copyout(ufds, kfds);

        if (retval == KERN_SUCCESS)
        {
                retval = nbits;
        }

        return retval;
}
