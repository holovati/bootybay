#include <mach/tmpsys.h>

#include <aux/time.h>
#include <kernel/mach_clock.h>
#include <kernel/time_value.h>
#include <emerixx/memory_rw.h>

// ********************************************************************************************************************************
kern_return_t sys_clock_gettime(pthread_t, clockid_t a_clock, struct timespec *a_timespec)
// ********************************************************************************************************************************
{
        kern_return_t retval = KERN_FAIL(EINVAL);
        timespec tp          = {};

        switch (a_clock)
        {

        case CLOCK_REALTIME: {
                mach_get_timespec(&tp);
                retval = KERN_SUCCESS;
                break;
        }
        case CLOCK_MONOTONIC: {
                mach_get_timespec(&tp);
                retval = KERN_SUCCESS;
                break;
        }
        case CLOCK_PROCESS_CPUTIME_ID: {
                log_debug("Not ready");
                break;
        }
        case CLOCK_THREAD_CPUTIME_ID: {
                log_debug("Not ready");

                break;
        }
        case CLOCK_MONOTONIC_RAW: {
                log_debug("Not ready");

                break;
        }
        case CLOCK_REALTIME_COARSE: {
                log_debug("Not ready");

                break;
        }
        case CLOCK_MONOTONIC_COARSE: {
                log_debug("Not ready");

                break;
        }
        case CLOCK_BOOTTIME: {
                log_debug("Not ready");

                break;
        }
        case CLOCK_REALTIME_ALARM: {
                log_debug("Not ready");

                break;
        }
        case CLOCK_BOOTTIME_ALARM: {
                log_debug("Not ready");

                break;
        }
        case CLOCK_SGI_CYCLE: {
                log_debug("Not ready");

                break;
        }
        case CLOCK_TAI: {
                log_debug("Not ready");

                break;
        }

        default:
                break;
        }

        if (retval == KERN_SUCCESS)
        {
                retval = emerixx::uio::copyout(&tp, a_timespec);
        }

        return retval;
}
