#include <aux/time.h>
#include <mach/tmpsys.h>
//

#include <emerixx/process.h>
#include <emerixx/sleep.h>
#include <kernel/mach_clock.h>
#include <kernel/time_out.h>

#include <emerixx/memory_rw.h>
#include <strings.h>

static inline void timespec_normalize(struct timespec *r, const struct timespec *a)
{
        if (a->tv_nsec >= NSEC_PER_SEC || a->tv_nsec <= -NSEC_PER_SEC)
        {
                r->tv_sec  = a->tv_sec + a->tv_nsec / NSEC_PER_SEC;
                r->tv_nsec = a->tv_nsec % NSEC_PER_SEC;
        }
        else
        {
                *r = *a;
        }

        if (r->tv_sec > 0 && r->tv_nsec < 0)
        {
                r->tv_sec -= 1;
                r->tv_nsec += NSEC_PER_SEC;
        }
        else if (r->tv_sec < 0 && r->tv_nsec > 0)
        {
                r->tv_sec += 1;
                r->tv_nsec -= NSEC_PER_SEC;
        }
}

kern_return_t sys_nanosleep(pthread_t a_proc, struct timespec const *a_rqtp, struct timespec *a_rmtp)
{
        struct timespec rqtp;

        if (a_rqtp == nullptr)
        {
                return KERN_FAIL(EINVAL);
        }

        kern_return_t retval = emerixx::uio::copyin(a_rqtp, &rqtp);

        if (retval != KERN_SUCCESS)
        {
                return retval;
        }

        if ((rqtp.tv_nsec < 0) || (rqtp.tv_nsec > NSEC_PER_SEC))
        {
                return KERN_FAIL(EINVAL);
        }

        timespec_normalize(&rqtp, &rqtp);

        u64 msec = static_cast<u64>(timespec_to_msec(&rqtp));

        i32 secsleep = static_cast<i32>((msec * static_cast<u64>(hz)) / 1000u);

        if (secsleep == 0)
        {
                secsleep = 1;
        }

        struct timespec sleep_start;
        mach_get_timespec(&sleep_start);
        retval = uthread_sleep_timeout((emerixx_wait_event_t)sys_nanosleep, /*PUSER | PCATCH 8, "nanosleep",*/ secsleep, nullptr);

        if (retval == KERN_FAIL(EINTR))
        {
                // See proc::on_signal_return. A signal aborted the sleep, a restart might block forver.
                // ERESTART_RESTARTBLOCK will return EINTR to userspace.
                retval = KERN_FAIL(ERESTART_RESTARTBLOCK);

                if (a_rmtp != nullptr)
                {
                        struct timespec sleep_end;
                        struct timespec sleep_time;

                        mach_get_timespec(&sleep_end);

                        timespec_sub(&sleep_time, &sleep_end, &sleep_start);

                        timespec_sub(&rqtp, &rqtp, &sleep_time);

                        kern_return_t e = emerixx::uio::copyout(rqtp, a_rmtp);

                        if (e != KERN_SUCCESS)
                        {
                                retval = e;
                        }
                }
        }
        else
        {
                retval = 0;
        }

        return retval;
}
