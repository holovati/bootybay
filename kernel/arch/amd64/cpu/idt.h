#pragma once

/* IDT for AMD64 */
#define IDTSZ 0x100 /*(0x20 + 0x20 + 0x10)*/

struct idt_gate {
  u32 offset_low : 16; /* offset 0..15 */
  u32 selector : 16;
  u32 ist : 8;
  u32 type : 4;
  u32 access : 4;
  u32 offset_mid : 16; /* offset 16..31 */
  u32 offset_high;     /* offset 32..63 */
  u32 mbz;             /* Unused in long mode*/
};

extern struct pseudo_descriptor const __idt_descriptor;