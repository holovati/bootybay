#include <emerixx/chrdev.h>
#include <emerixx/vfs/vfs_file_data.h>
#include <emerixx/vfs/vfs_node.h>

#include <aux/init.h>

#include <fcntl.h>
#include <sys/sysmacros.h>

#include <emerixx/memory_rw.h>

#include "pio.h"

// A driver to write IO ports from userspace. Used for debugging

#define PIO_CTX(file) ((((pio_ctx_t)(&(file)->m_private))))

#pragma pack(1)
typedef struct pio_ctx_data
{
        uint32_t m_offset;
        uint32_t m_xferchunk;
} *pio_ctx_t;
#pragma pack(0)

static_assert(sizeof(pio_ctx_data) <= sizeof(vfs_private_t));

static chrdev_data s_piodev;

// ********************************************************************************************************************************
static kern_return_t pio_open(vfs_node_t, vfs_file_t a_file)
// ********************************************************************************************************************************
{
        pio_ctx_t ctx = PIO_CTX(a_file);

        *ctx = { .m_offset = 0, .m_xferchunk = 1 };

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t pio_read(vfs_file_t a_file, struct uio *a_uio)
// ********************************************************************************************************************************
{
        union
        {
                uint8_t byte;
                uint16_t word;
                uint32_t dword;
        };

        pio_ctx_t ctx = PIO_CTX(a_file);

        if (a_uio->uio_resid < ctx->m_xferchunk)
        {
                return KERN_FAIL(EINVAL);
        }

        switch (ctx->m_xferchunk)
        {
                case 1:
                        byte = inb((int)ctx->m_offset);
                        break;
                case 2:
                        word = inw((int)ctx->m_offset);
                        break;
                case 4:
                        dword = inl((int)ctx->m_offset);
                        break;
                default:
                        for (uint32_t i = 0; i < ctx->m_xferchunk; ++i)
                        {
                                byte                 = inb((int)(ctx->m_offset + i));
                                kern_return_t retval = uiomove(&byte, a_uio->uio_resid, a_uio);
                                if (KERN_STATUS_FAILURE(retval))
                                {
                                        return retval;
                                }
                        }

                        return KERN_SUCCESS;
        }

        return uiomove(&byte, ctx->m_xferchunk, a_uio);
}

// ********************************************************************************************************************************
static kern_return_t pio_write(vfs_file_t a_file, struct uio *a_uio)
// ********************************************************************************************************************************
{
        union
        {
                uint8_t byte;
                uint16_t word;
                uint32_t dword;
        };

        pio_ctx_t ctx = PIO_CTX(a_file);

        if (a_uio->uio_resid < ctx->m_xferchunk)
        {
                return KERN_FAIL(EINVAL);
        }

        switch (ctx->m_xferchunk)
        {
                case 1:
                {
                        kern_return_t retval = uiomove(&byte, sizeof(byte), a_uio);

                        if (KERN_STATUS_FAILURE(retval))
                        {
                                return retval;
                        }

                        outb((int)ctx->m_offset, byte);
                }
                break;
                case 2:
                {
                        kern_return_t retval = uiomove(&word, sizeof(word), a_uio);

                        if (KERN_STATUS_FAILURE(retval))
                        {
                                return retval;
                        }

                        outw((int)ctx->m_offset, word);
                }
                break;
                case 4:
                {
                        kern_return_t retval = uiomove(&dword, sizeof(dword), a_uio);

                        if (KERN_STATUS_FAILURE(retval))
                        {
                                return retval;
                        }

                        outl((int)ctx->m_offset, dword);
                }
                break;
                default:
                {
                        for (uint32_t i = 0; i < ctx->m_xferchunk; ++i)
                        {
                                kern_return_t retval = uiomove(&byte, sizeof(byte), a_uio);

                                if (KERN_STATUS_FAILURE(retval))
                                {
                                        return retval;
                                }

                                outb((int)(ctx->m_offset + i), byte);
                        }
                }
                break;
        }

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t pio_lseek(vfs_file_t a_file, off_t a_offset, int a_whence)
// ********************************************************************************************************************************
{
        pio_ctx_t ctx = PIO_CTX(a_file);

        switch (a_whence)
        {
                case SEEK_CUR:
                        ctx->m_offset += (uint32_t)a_offset;
                        break;
                case SEEK_END:
                        ctx->m_offset = (uint32_t)UINTPTR_MAX;
                        break;
                case SEEK_SET:
                        ctx->m_offset = (uint32_t)a_offset;
                        break;
                default:
                        return KERN_FAIL(EINVAL);
        }

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t pio_close(vfs_file_t)
// ********************************************************************************************************************************
{
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t pio_getdents(vfs_file_t, struct uio *uio)
// ********************************************************************************************************************************
{
        panic("No impl");
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t pio_mmap(vfs_file_t, vm_object_t *)
// ********************************************************************************************************************************
{
        panic("No impl");
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t pio_poll(vfs_file_t, struct pollfd **a_pfd)
// ********************************************************************************************************************************
{
        panic("No impl");
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t pio_splice(vfs_file_t, off_t *a_off, vfs_file_t a_file_out, off_t *a_off_out, size_t a_len, unsigned a_flags)
// ********************************************************************************************************************************
{
        panic("No impl");
        return KERN_SUCCESS;
}

static struct vfs_file_operations_data s_pio_ops = {
        //
        .fopen     = pio_open,                 //
        .fread     = pio_read,                 //
        .fwrite    = pio_write,                //
        .flseek    = pio_lseek,                //
        .fioctl    = vfs_file_op_ioctl_enotty, //
        .fclose    = pio_close,                //
        .fgetdents = pio_getdents,             //
        .fmmap     = pio_mmap,                 //
        .fpoll     = pio_poll,                 //
        .fsplice   = pio_splice                //
        //
};

// ********************************************************************************************************************************
static kern_return_t pio_device_init()
// ********************************************************************************************************************************
{
        kern_return_t retval = driver_chrdev_alloc(&s_piodev, "pio", &s_pio_ops, makedev(100, 0), 1);

        return retval;
}

device_initcall(pio_device_init);
