#pragma once

#include <mach/machine/locore.h>

#ifdef __cplusplus
extern "C" {
#endif

NORETURN USED void locore_exit_fast_syscall(struct amd64_saved_state *a_saved_state);

NORETURN USED void locore_exit_iret(struct amd64_saved_state *a_saved_state);

NORETURN USED void locore_kern_trap(struct amd64_saved_state *a_saved_state);

NORETURN USED void locore_kern_interrupt(struct amd64_saved_state *a_saved_state);

NORETURN USED void locore_user_interrupt(struct amd64_saved_state *a_saved_state, vm_address_t a_thread_stack);

NORETURN USED void locore_user_trap(struct amd64_saved_state *a_saved_state, vm_address_t a_thread_stack);

void locore_interrupt_common(struct amd64_saved_state *a_saved_state, bool kernel_mode);

#ifdef __cplusplus
}
#endif
