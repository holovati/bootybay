#include <etl/bit.hh>

#include <mach/machine/locore.h>
#include <mach/machine/macro_help.h>
#include <mach/machine/vm_param.h>
#include <kernel/thread_data.h>
#include <kernel/env.h>

#include "../cpu/idt.h"
#include "eflags.h"
#include "gdt.h"
#include "msr.h"
#include "proc_reg.h"
#include "seg.h"
#include "trapno.h"

// Provided by the linker, our interrupt, nmi, mc stacks grow downwards from here
extern vm_address_t __executable_start[];
#define INTERRUPT_STACK ((vm_address_t)(__executable_start))
#define NMI_STACK       (INTERRUPT_STACK - PGBYTES)
#define MC_STACK        (NMI_STACK - PGBYTES)
#define KSTACK_END      (MC_STACK - PGBYTES)

// TSS
#define TSS_IST_DISABLE 0
#define TSS_IST_1       1
#define TSS_IST_2       2
#define TSS_IST_3       3
#define TSS_IST_4       4
#define TSS_IST_5       5
#define TSS_IST_6       6
#define TSS_IST_7       7

#pragma pack(1)
struct tss
{
        u32 resv0;
        u64 rsp[3];
        u64 resv1;
        u64 ist[TSS_IST_7];
        u64 resv2;
        u32 io_map_address;
};
#pragma pack()

template <unsigned ...i>
using u32bit = etl::bit<u32, i...>;

// GDT
/*
 * AMD64 segmentation.
 */
#define DESC_DPL_KERNEL  (GDT_PRIV_KERNEL << 13)
#define DESC_DPL_USER    (GDT_PRIV_USER << 13)
#define DESC_PRESENT     (u32bit<15>::value)
#define DESC_GRANULARITY (u32bit<23>::value)

#define DESC_SEGMENT (u32bit<12>::value)
/* AMD manual says the writable bit is ignored on data in long mode.
 * But I get a GP without it */
#define DESC_SEGMENT_DATA_WRITEABLE (u32bit<9>::value)
#define DESC_SEGMENT_CODE           (u32bit<11>::value)
#define DESC_SEGMENT_CODE_LONG      (u32bit<21>::value)

#define DESC_SYS_TYPE_TSS (0x09 << 8)

/*
  STACKLAYOUT BEFORE MACRO:
  Return SS         + 48
  Return RSP        + 40
  Return RFLAGS     + 32
  Return CS         + 24
  Return RIP        + 16
  ERROR CODE        + 8
  ERROR NUMBER      <-- RSP
*/
#define ASM_SAVE_REGS_TO(__reg)                                                                                                    \
        asm volatile("push r15");                                                                                                  \
        asm volatile("push r14");                                                                                                  \
        asm volatile("push r13");                                                                                                  \
        asm volatile("push r12");                                                                                                  \
        asm volatile("push r11");                                                                                                  \
        asm volatile("push r10");                                                                                                  \
        asm volatile("push r9");                                                                                                   \
        asm volatile("push r8");                                                                                                   \
        asm volatile("push rbp");                                                                                                  \
        asm volatile("push rdi");                                                                                                  \
        asm volatile("push rsi");                                                                                                  \
        asm volatile("push rdx");                                                                                                  \
        asm volatile("push rcx");                                                                                                  \
        asm volatile("push rbx");                                                                                                  \
        asm volatile("push rax");                                                                                                  \
                                                                                                                                   \
        /* Push cr2 */                                                                                                             \
        asm volatile("mov rcx, cr2");                                                                                              \
        asm volatile("push rcx");                                                                                                  \
                                                                                                                                   \
        /* Save FSBASE */                                                                                                          \
        asm volatile("xor rax, rax");                                                                                              \
        asm volatile("xor rdx, rdx");                                                                                              \
        asm volatile("mov rcx, %0" : : "i"(MSR_FS_BASE) :); /* FSBASE */                                                           \
        asm volatile("rdmsr");                                                                                                     \
        asm volatile("shl rdx, 32");                                                                                               \
        asm volatile("or rdx, rax");                                                                                               \
        asm volatile("push rdx");                                                                                                  \
                                                                                                                                   \
        /* Set the stack pointer to be the first argument */                                                                       \
        asm volatile("mov " #__reg ", rsp")

#define ASM_JUMP_TO_HANDLER(handler)                                                                                               \
        asm volatile("push 0\n popfq");                                                                                            \
        asm volatile("and rsp, 0xFFFFFFFFFFFFFFF0");                                                                               \
        asm volatile("xor rbp, rbp");                                                                                              \
        asm volatile("push 0");                                                                                                    \
        asm volatile("jmp " #handler);                                                                                             \
        UNREACHABLE()

#define ASM_KERNEL_INIT_STACK()                                                                                                    \
        asm volatile("mov rdx, rsp");                                                                                              \
        asm volatile("and rdx, %0" : : "i"(~(PGBYTES - 1)));                                                                       \
        asm volatile("add rdx, 0x7FE03000");                                                                                       \
        asm volatile("cmp rdx, 0x3001");                                                                                           \
        asm volatile("lea rdx,  __executable_start" : :);                                                                          \
        asm volatile("cmovnb rsp, rdx");

#define ASM_USER_INIT_STACK()                                                                                                      \
        /* Get our kernel stack */                                                                                                 \
        asm volatile("mov rsp, QWORD PTR [active_threads]");                                                                       \
        asm volatile("mov rsp, QWORD PTR [rsp + %0]" : : "i"(offsetof(struct thread, kernel_stack)) :);                            \
                                                                                                                                   \
        /* Pass the kernel stack address as second paramter to locore_user_trap*/                                                  \
        asm volatile("mov rsi, rsp");                                                                                              \
                                                                                                                                   \
        /* set the stack top */                                                                                                    \
        asm volatile("add rsp, %0" : : "i"(KERNEL_STACK_SIZE) :);                                                                  \
                                                                                                                                   \
        /* Create room for kernel state */                                                                                         \
        asm volatile("sub rsp, %0" : : "i"(sizeof(struct amd64_saved_state)) :);                                                   \
                                                                                                                                   \
        /* Create exception link */                                                                                                \
        /* struct amd64_exception_link { */                                                                                        \
        asm volatile("push 0");   /* natural_t pad;*/                                                                              \
        asm volatile("push rdi"); /* struct amd64_saved_state *saved_state;*/                                                      \
        /* }; */

extern "C" {
// Kernel faults are almost always fatal(except legal page faults)/
// We save the thread state to the current stack and switch to the interrupt
// stack(if not on it already)
NAKED USED static void kernel_trap_internal()
{
        ASM_SAVE_REGS_TO(rdi);

        ASM_KERNEL_INIT_STACK();

        ASM_JUMP_TO_HANDLER(locore_kern_trap);
}

NAKED USED static void kernel_interrupt_internal()
{
        ASM_SAVE_REGS_TO(rdi);

        ASM_KERNEL_INIT_STACK();

        ASM_JUMP_TO_HANDLER(locore_kern_interrupt);
}

NAKED USED static void user_trap_internal()
{
        asm volatile("swapgs");

        ASM_SAVE_REGS_TO(rdi);

        ASM_USER_INIT_STACK();

        ASM_JUMP_TO_HANDLER(locore_user_trap);
}

NAKED USED static void user_interrupt_internal()
{
        asm volatile("swapgs");

        ASM_SAVE_REGS_TO(rdi);

        ASM_USER_INIT_STACK();

        ASM_JUMP_TO_HANDLER(locore_user_interrupt);
}

// After the trap number and error code have been added to the trap
// context we land here.
NAKED static void trap_common()
{
        /* User traps are treated as "normal" kernel entries and are always recoverble
         * from the kernel standpoint. Kernel traps are almost always fatal to the
         * kernel(except for legal page faults) */
        asm volatile("cmp QWORD PTR [rsp + 24], %0" : : "i"(GDT_USER_CS_SELECTOR));
        asm volatile("je user_trap_internal");

        /* A Kernel trap */
        asm volatile("jmp kernel_trap_internal");

        UNREACHABLE();
}

// After the interrupt number and error code have been added to the interrupt
// context we land here.
NAKED USED static void interrupt_common()
{
        /* User interrupts are a little bit different because of ast checking */
        asm volatile("cmp QWORD PTR [rsp + 24], %0" : : "i"(GDT_USER_CS_SELECTOR));
        asm volatile("je user_interrupt_internal");

        /* A Kernel interrupt */
        asm volatile("jmp kernel_interrupt_internal");

        UNREACHABLE();
}
// Entry point for user SYSCALL instruction
// We treat SYSCALL entries as user traps, all this function does is setup the
// stack for a jump to user_trap_internal.

static natural_t PER_CPU_VAR(trsp);

NAKED static void syscall_entry()
{
        /* Load pcb_t -> ss into rsp */
        // We need access to our temp rsp var
        asm volatile("swapgs");

        asm volatile("mov gs:[%0], rsp" : : "i"(PER_CPU_OFF(trsp)) :);

        asm volatile("mov rsp, QWORD PTR [active_threads]");
        asm volatile("mov rsp, QWORD PTR [rsp + %0]" : : "i"(offsetof(struct thread, pcb)) :);
        asm volatile("add rsp, %0" : : "i"(offsetof(struct pcb, iss) + sizeof(struct amd64_saved_state)) :);
        /* Save the user state to pcb->ss */

        /* set return ss, even though sysret takes care of it*/
        asm volatile("push %0" : : "i"(GDT_USER_DS_SELECTOR) :);

        /* save user rsp */
        asm volatile("push gs:[%0]" : : "i"(PER_CPU_OFF(trsp)) :);

        /* save user rflags */
        asm volatile("push r11");

        /* set return cs, even though sysret takes care of it*/
        asm volatile("push %0" : : "i"(GDT_USER_CS_SELECTOR) :);

        /* save user rip */
        asm volatile("push rcx");

        /* Save the syscall number in the error_code field, needed  when restarting
         * system calls after handling a signal */
        asm volatile("push rax");

        /* Set that trap number for SYSCALL instruction */
        asm volatile("push %0" : : "i"(TRAP_FAST_SYSCALL) :);

        // user_trap_internal expects user gs
        asm volatile("swapgs");

        asm volatile("jmp user_trap_internal");
}
} // extern "C"

// clang-format off
<%
# tepmlate metadata
f = open(_in_folder + "/../data/exceptions.py", "r")
exceptions = eval(f.read())
f.close()
%>


<% exception_entry_names = [] %>
%for exception in exceptions:
<%
exception_entry_name = ""
exception_descrition = exception["description"]
exception_vector = str(exception["vector"])
if len(exception["mnemonic"]) > 0:
    exception_entry_name = exception["mnemonic"].replace("#","").lower()
elif exception["type"] == "reserved":
    exception_entry_name = "reserved"+exception_vector
else:
    if "nmi" in exception["description"].lower():
        exception_entry_name = "nmi"
    else:
        exception_entry_name = "?"

exception_entry_names.append("exception_entry_{0}".format(exception_entry_name))
%>
/*
    **** ${exception["description"]} ****

    ${exception["source"]}
*/
NAKED static void ${exception_entry_names[-1]}() {
%if exception["error_code"] != True:
        /* ${exception["description"]} does not push a error code, so we push a zero to match the stack layout of exceptions with a error code */
        asm volatile("push 0");

%endif
        /* Push the vector number */
        asm volatile("push ${exception_vector}");

        /* Save the general purpose registers */
        asm volatile ("jmp %0" : :"i"(trap_common) : );
        UNREACHABLE();
}
%endfor
%for i in range(len(exceptions), 32):

<% exception_entry_names.append("exception_entry_reserved{0}".format(i)) %>
NAKED static void ${exception_entry_names[-1]}() {
        /* Reserved exceptions do not push a error code, so we push a zero to match the stack layout of exceptions with a error code */
        asm volatile("push 0");

        /* Push the vector number */
        asm volatile("push ${exception_vector}");

        /* Jump to the common handler */
        asm volatile ("jmp trap_common");
        UNREACHABLE();
}
%endfor

%for i in range(0x00, 0x100 -0x20):
NAKED static void ${"irq{0}_entry".format(i)}()
{
        /* Interrupts do not push error codes, we push a zero to make it compatible
         * with amd64_saved_state */
        asm volatile("push 0");

        /* Push the vector number */
        asm volatile("push ${i}");

        /* Jump to the common handler */
        asm volatile("jmp interrupt_common");
        UNREACHABLE();
}
% endfor

#pragma GCC diagnostic ignored "-Wnarrowing"
#pragma GCC diagnostic ignored "-Winline"

constexpr struct idt_gate create_gate(vm_address_t offset, u8 selector, u8 type, u8 access, u8 ist)
{
        idt_gate gate{};

        gate.offset_low  = ((offset)) & 0xffff;
        gate.selector    = selector;
        gate.ist         = ist & 3;
        gate.type        = type;
        gate.access      = access | SEG_ACC_P;
        gate.offset_mid  = ((offset) >> 16) & 0xffff;
        gate.offset_high = ((offset) >> 32) & 0xffffffff;
        gate.mbz         = 0;

        return gate;
}


#pragma GCC diagnostic ignored "-Wnarrowing"
SECTION(cpu.idt.vector)
static const struct idt_gate idt_vector[] = {
%for i in range(0, len(exception_entry_names)):
%if i == 2:
        // nmi
        create_gate((vm_offset_t)${exception_entry_names[i]}, GDT_KERNEL_CS, SEG_TYPE_INTR_GATE, SEG_ACC_PL_K, TSS_IST_1),
%elif i == 18:
        // #MC
        create_gate((vm_offset_t)${exception_entry_names[i]}, GDT_KERNEL_CS, SEG_TYPE_INTR_GATE, SEG_ACC_PL_K, TSS_IST_2),
%elif i == 8:
        // double fault. Use MC stack for printing info before panic.
        create_gate((vm_offset_t)${exception_entry_names[i]}, GDT_KERNEL_CS, SEG_TYPE_INTR_GATE, SEG_ACC_PL_K, TSS_IST_2),
%else:
        create_gate((vm_offset_t)${exception_entry_names[i]}, GDT_KERNEL_CS, SEG_TYPE_INTR_GATE, SEG_ACC_PL_K, TSS_IST_DISABLE),
%endif
%endfor
        // Fill in irq entries
%for i in range(0x20, 0x100):
        create_gate((vm_offset_t)${"irq{0}_entry".format(i-0x20)}, GDT_KERNEL_CS, SEG_TYPE_INTR_GATE, SEG_ACC_PL_K, TSS_IST_DISABLE),
% endfor
};
#pragma GCC diagnostic pop

// clang-format on

SECTION(cpu.idt.descriptor)
constexpr static struct pseudo_descriptor idt_descriptor = {.limit = sizeof(idt_vector) - 1, .base = (vm_address_t)idt_vector};

// AMD manual. p.155 Figure 6-1
SECTION(cpu.msr.star)
constexpr static msr_t star = (msr_t)(((((u64)GDT_KERNEL_DS) << 48) | GDT_PRIV_KERNEL) | (((u64)GDT_KERNEL_CS) << 32));

// Set syscall entry point
SECTION(cpu.msr.lstar)
static msr_t lstar = (vm_address_t)syscall_entry;

SECTION(cpu.tss.vector)
constexpr static struct tss ktss __attribute__((aligned(16))) = //
    {.resv0          = 0,
     .rsp            = {INTERRUPT_STACK, 0xDEADBEEFFEADFACE, 0xFEADFACEDEADBEEF},
     .resv1          = 0,
     .ist            = {NMI_STACK, MC_STACK, 0, 0, 0, 0, 0},
     .resv2          = 0,
     .io_map_address = 0};

constexpr static u32 gdt_tss0lo()
{
        u32 out = 0;
        /* Set limit 0..16 */
        out |= ((u16)(sizeof(struct tss)));
        /* Set base address 0..15 */
        out |= (u32)(((u16)((vm_offset_t)&ktss) << 16));
        return out;
}

constexpr static u32 gdt_tss0hi()
{
        u32 out = 0;
        /* Set limit 16..20 */
        out |= ((u32)(((sizeof(struct tss) >> 16) & 0xF) << 16));
        /* Set base address 24..31 */
        out |= (u32)((((u64)(&ktss) >> 24) & 0xFF) << 24);
        /* Set base address 16..23  and present bit*/
        out |= (((u8)((u64)(&ktss) >> 16)) | DESC_PRESENT);
        return out;
}

constexpr static u32 gdt_tss1lo() { return (u32)((u64)(&ktss) >> 32); }

SECTION(cpu.gdt.vector)
static struct
{
        u32 lo;
        u32 hi;
} gdt_vector[] = {
    {/* NULL SEGMENT */
     .lo = 0,
     .hi = 0},
    {/* KERNEL CODE SEGMENT */
     .lo = 0,
     .hi = (DESC_SEGMENT | DESC_DPL_KERNEL | DESC_PRESENT | DESC_SEGMENT_CODE | DESC_SEGMENT_CODE_LONG)},
    {/* KERNEL DATA SEGMENT */
     .lo = 0,
     .hi = (DESC_SEGMENT | DESC_DPL_KERNEL | DESC_PRESENT | DESC_SEGMENT_DATA_WRITEABLE)},
    {/* USER DATA SEGMENT */
     .lo = 0,
     .hi = (DESC_SEGMENT | DESC_DPL_USER | DESC_PRESENT | DESC_SEGMENT_DATA_WRITEABLE)},
    {/* USER CODE SEGMENT */
     .lo = 0,
     .hi = (DESC_SEGMENT | DESC_DPL_USER | DESC_PRESENT | DESC_SEGMENT_CODE | DESC_SEGMENT_CODE_LONG | u32bit<9>::value)},
    {/* KERNEL TSS LOW */
     .lo = gdt_tss0lo(),
     .hi = (gdt_tss0hi() | DESC_SYS_TYPE_TSS | DESC_DPL_KERNEL)},
    {/* KERNEL TSS HIGH */
     .lo = gdt_tss1lo(),
     .hi = DESC_PRESENT}
    /* GDT END */
};
#pragma GCC diagnostic pop

/* See AMD Programmers manual vol.2 pg 88 for details*/
SECTION(cpu.gdt.descriptor)
constexpr static struct pseudo_descriptor gdt_descriptor = {.limit = sizeof(gdt_vector) - 1, .base = (vm_address_t)(gdt_vector)};
