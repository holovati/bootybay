#include "locore/locore_internal.h"
#include "pic.h"
#include <mach/machine/pio.h>
#include <mach/machine/spl.h>
#include <mach/machine/thread.h>

#include <emerixx/interrupt.h>

extern irq_handler_t ivect2[];
extern void *ivect2_devs[];

extern int (*ivect[/*NINTR*/])(int unit, spl_t a_spl, bool kernel_mode, amd64_saved_state *ss); /* defined in pic_isa.c */

extern int intpri[/*NINTR*/]; /* defined in pic_isa.c */
extern int iunit[/*NINTR*/];  /* defined in pic.c */

/// TEMP
#define PIC1_CMD     0x20
#define PIC1_DATA    0x21
#define PIC2_CMD     0xA0
#define PIC2_DATA    0xA1
#define PIC_READ_IRR 0x0a /* OCW3 irq ready next CMD read */
#define PIC_READ_ISR 0x0b /* OCW3 irq service next CMD read */

/* Helper func */
static uint16_t __pic_get_irq_reg(int ocw3)
{
        /* OCW3 to PIC CMD to get the register values.  PIC2 is chained, and
         * represents IRQs 8-15.  PIC1 is IRQs 0-7, with 2 being the chain */
        outb(PIC1_CMD, ocw3);
        outb(PIC2_CMD, ocw3);
        return (uint16_t)(inb(PIC2_CMD) << 8) | inb(PIC1_CMD);
}

/* Returns the combined value of the cascaded PICs irq request register */
uint16_t pic_get_irr(void)
{
        return __pic_get_irq_reg(PIC_READ_IRR);
}

/* Returns the combined value of the cascaded PICs in-service register */
uint16_t pic_get_isr(void)
{
        return __pic_get_irq_reg(PIC_READ_ISR);
}

uint64_t master_spuries = 0;
uint64_t slave_spuries  = 0;

void locore_interrupt_common(struct amd64_saved_state *a_saved_state, bool kernel_mode)
{

        if (a_saved_state->trapnum == 7 || a_saved_state->trapnum == 15)
        {
                uint16_t isr = pic_get_isr();

                if ((a_saved_state->trapnum == 7) && (((isr & (1 << 7)) == 0)))
                {
                        master_spuries++;
                        locore_exit_iret(a_saved_state);
                        UNREACHABLE();
                }
                else if ((a_saved_state->trapnum == 15) && (((isr & (1 << 15)) == 0)))
                {
                        slave_spuries++;
                        /* ack interrupt to master */
                        outb(PIC_MASTER_ICW, NON_SPEC_EOI);
                        locore_exit_iret(a_saved_state);
                        UNREACHABLE();
                }
        }

        /* get new ipl and set it ( will enable interrupts )*/
        spl_t prev_ipl = splx(intpri[a_saved_state->trapnum /* << 2*/] /* irq * 4 */);

        /* get device unit number */
        int unit_number = iunit[a_saved_state->trapnum /* << 2*/];

        ivect[a_saved_state->trapnum /* << 2*/](unit_number, prev_ipl, kernel_mode, a_saved_state);

        if (ivect2[a_saved_state->trapnum])
        {
                ivect2[a_saved_state->trapnum](a_saved_state->trapnum, ivect2_devs[a_saved_state->trapnum]);
        }

        /* ack interrupt to master */
        outb(PIC_MASTER_ICW, NON_SPEC_EOI);

        if (a_saved_state->trapnum > 7)
        {
                /* ack interrupt to slave */
                outb(PIC_SLAVE_ICW, NON_SPEC_EOI);
        }

        /* restore previous ipl, without enabling interrupts */
        splx_cli(prev_ipl);
}
