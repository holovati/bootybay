#include "gdt.h"
#include "locore/locore_internal.h"
#include "trapno.h"

#include <kernel/ast.h>
#include <mach/machine/thread.h>

#include <kernel/vm/vm_fault.h>
#include <kernel/vm/vm_param.h>
#include <kernel/thread_data.h>

#include <emerixx/process.h>
#include <signal.h>

#include "proc_reg.h"
#include "msr.h"

// Auto generated
extern "C" void syscall_dispatch(struct amd64_saved_state *a_saved_state, thread_t a_thread);

extern "C" void fpexterrflt();

extern "C" void fpnoextflt(thread_t);

/*
 * Handle AST traps for i386.
 * Check for delayed floating-point exception from
 * AT-bus machines.
 */
static void i386_astintr(ast_t a_ast)
{
        splsched(); /* block interrupts to check reasons */

        if (a_ast & AST_I386_FP)
        {
                /*
                 * AST was for delayed floating-point exception -
                 * FP interrupt occured while in kernel.
                 * Turn off this AST reason and handle the FPU error.
                 */
                int mycpu = 0; /*cpu_number()*/
                ast_off(mycpu, AST_I386_FP);

                spl0();

                fpexterrflt();
        }
        else
        {
                /*
                 * Not an FPU trap.  Handle the AST.
                 * Interrupts are still blocked.
                 */
                ast_taken();
        }
}

static inline thread_t user_enter_common(vm_address_t a_thread_stack)
{
        active_stacks[/*current_cpu()*/ 0] = a_thread_stack;
        thread_t thread                    = current_thread();

        wrmsr(MSR_FS_BASE, thread->tls);

        return thread;
}

NORETURN
static thread_t user_exit_common(struct amd64_saved_state *a_saved_state, bool a_is_fastsyscall)
{
again:
        sti();

        uthread_system_exit();

        KASSERT(a_saved_state->return_cs == GDT_USER_CS_SELECTOR);

        if (ast_t ast = need_ast[/*cpu*/ 0]; ast)
        {
                i386_astintr(ast);
                goto again;
        }

        asm volatile("swapgs");

        if (a_is_fastsyscall)
        {
                locore_exit_fast_syscall(a_saved_state);
        }
        else
        {
                locore_exit_iret(a_saved_state);
        }

        UNREACHABLE();
}

void locore_user_interrupt(struct amd64_saved_state *a_saved_state, vm_address_t a_thread_stack)
{
        user_enter_common(a_thread_stack);

        locore_interrupt_common(a_saved_state, false);

        user_exit_common(a_saved_state, false);
}

void locore_user_trap(struct amd64_saved_state *a_saved_state, vm_address_t a_thread_stack)
{
        thread_t thread = user_enter_common(a_thread_stack);

        sti();

        switch (a_saved_state->trapnum)
        {
                case TRAP_DIVIDE_ERROR:
                {
                        uthread_signal(thread->pthread, SIGFPE);
                        log_debug("User trap divide error not implemented");
                        break;
                }

                case TRAP_DEBUG_EXCEPTION:
                {
                        panic("User trap debug exception not implemented");
                        break;
                }

                case TRAP_NMI_INTERRUPT:
                {
                        panic("User trap nmi not implemented");
                        break;
                }

                case TRAP_BREAKPOINT:
                {
                        panic("User trap breakpoint not implemented");
                        break;
                }

                case TRAP_OVERFLOW:
                {
                        panic("User trap overflow not implemented");
                        break;
                }

                case TRAP_BOUND_RANGE_EXCEEDED:
                {
                        panic("User trap bound range exceeded not implemented");
                        break;
                }

                case TRAP_INVALID_OPCODE:
                {
                        panic("User trap invalid opcode not implemented");
                        break;
                }

                case TRAP_DEVICE_NOT_AVAILABLE:
                {
                        fpnoextflt(thread);
                        break;
                }

                case TRAP_DOUBLE_FAULT:
                {
                        panic("User trap double fault not implemented");
                        break;
                }

                case TRAP_COPROCESSOR_SEGMENT_OVERRUN_RESERVED:
                {
                        panic("User trap coprocessor segment overrun not implemented");
                        break;
                }

                case TRAP_INVALID_TSS:
                {
                        panic("User trap invalid tss not implemented");
                        break;
                }

                case TRAP_SEGMENT_NOT_PRESENT:
                {
                        panic("User trap segment nor present not implemented");
                        break;
                }

                case TRAP_STACK_SEGMENT:
                {
                        panic("User trap stack segment not implemented");
                        break;
                }

                case TRAP_GENERAL_PROTECTION:
                {
                        // panic("User trap general protection not implemented");
                        /* Send a SEGFAULT signal and call it a day */
                        log_debug("Pid %d: User GP fault", thread->pthread->m_pid);
                        uthread_signal(thread->pthread, SIGSEGV);
                        break;
                }

                case TRAP_PAGE_FAULT:
                {
                        vm_prot_t prot = VM_PROT_READ;

                        if (a_saved_state->error_code & 2 /*T_PF_WRITE*/)
                        {
                                prot |= VM_PROT_WRITE;
                        }

                        vm_address_t fault_page_addr = trunc_page(a_saved_state->cr2);

                        kern_return_t kr = vm_fault(thread->task->map, fault_page_addr, prot, false);

                        if (kr != KERN_SUCCESS)
                        {
                                /* Send a SEGFAULT signal and call it a day */
                                log_debug("Pid %d, Could not map user page at address %p", thread->pthread->m_pid, fault_page_addr);

                                uthread_signal(thread->pthread, SIGSEGV);
                        }
                        break;
                }

                case TRAP_INTEL_RESERVED_DO_NOT_USE:
                {
                        panic("User reserved trap not implemented");
                        break;
                }

                case TRAP_X87_FPU_FLOATING_POINT_ERROR:
                {
                        panic("User trap x87 fpu error not implemented");
                        break;
                }

                case TRAP_ALIGNMENT_CHECK:
                {
                        panic("User trap alignment check not implemented");
                        break;
                }

                case TRAP_MACHINE_CHECK:
                {
                        panic("User trap machine check not implemented");
                        break;
                }

                case TRAP_SIMD_FLOATING_POINT_EXCEPTION:
                {
                        panic("User trap SIMD floating point exception not implemented");
                        break;
                }

                case TRAP_VIRTUALIZATION_EXCEPTION:
                {
                        panic("User trap virtualization exception not implemented");
                        break;
                }

                case TRAP_CONTROL_PROTECTION_EXCEPTION:
                {
                        panic("User trap control protection exception not implemented");
                        break;
                }

                case TRAP_FAST_SYSCALL:
                {
                        syscall_dispatch(a_saved_state, thread);
                        break;
                }

                default:
                {
                        panic("User trap number %d has no handler! this should never happen", a_saved_state->trapnum);
                }
        }

        cli();
        user_exit_common(a_saved_state, /*a_saved_state->trapnum == TRAP_FAST_SYSCALL*/ false);
}

// Used by the scheduler, must never return to kernel mode with this. verify usage!
void locore_exception_return()
{
        vm_offset_t ksaddr              = current_thread()->kernel_stack;
        struct amd64_exception_link *el = STACK_IEL(ksaddr);
        user_exit_common(el->saved_state, false);
}
