#include "locore/locore_internal.h"
#include "msr.h"

#define ASM_RESTORE_REGS_FROM(__reg)                                                                                               \
        asm volatile("cli");                                                                                                       \
                                                                                                                                   \
        /* Set the saved state as the active stack, so we can start popping shit of it*/                                           \
        asm volatile("mov rsp, " #__reg);                                                                                          \
                                                                                                                                   \
        /* Set FSBASE */                                                                                                           \
        asm volatile("mov rcx, %0" : : "i"(MSR_FS_BASE) :);                                                                        \
        asm volatile("mov rdx, QWORD PTR [rsp]");                                                                                  \
        asm volatile("shr rdx, 32");                                                                                               \
        asm volatile("pop rax");                                                                                                   \
        asm volatile("wrmsr");                                                                                                     \
                                                                                                                                   \
        /* Pop cr2 */                                                                                                              \
        asm volatile("add rsp, 8");                                                                                                \
                                                                                                                                   \
        /* Pop the rest of the thread registers */                                                                                 \
        asm volatile("pop rax");                                                                                                   \
        asm volatile("pop rbx");                                                                                                   \
        asm volatile("pop rcx");                                                                                                   \
        asm volatile("pop rdx");                                                                                                   \
        asm volatile("pop rsi");                                                                                                   \
        asm volatile("pop rdi");                                                                                                   \
        asm volatile("pop rbp");                                                                                                   \
        asm volatile("pop r8");                                                                                                    \
        asm volatile("pop r9");                                                                                                    \
        asm volatile("pop r10");                                                                                                   \
        asm volatile("pop r11");                                                                                                   \
        asm volatile("pop r12");                                                                                                   \
        asm volatile("pop r13");                                                                                                   \
        asm volatile("pop r14");                                                                                                   \
        asm volatile("pop r15")

NORETURN NAKED void locore_exit_fast_syscall(struct amd64_saved_state *)
{
        ASM_RESTORE_REGS_FROM(rdi);

        /* we just neeed to move the userstack back into rsp,
         * sysret ignores the following
         * trapnum;
         * error_code;
         * return_rip;(is in rcx)
         * return_cs; (sysret sets it to user)
         * return_rflags; (are in r11)
         * natural_t return_ss;(sysret sets it to user)
         */

        /* skip return ss */
        asm volatile("add rsp, 40");
        asm volatile("pop rsp");
        asm volatile("sysretq");

        UNREACHABLE();
}

NORETURN NAKED void locore_exit_iret(struct amd64_saved_state *)
{
        ASM_RESTORE_REGS_FROM(rdi);

        /* remove "error code" and vector number */
        asm volatile("add rsp, 16");

        asm volatile("iretq");

        UNREACHABLE();
}
