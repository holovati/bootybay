#include <kernel/vm/vm_fault.h>
#include <kernel/vm/vm_kern.h>
#include <kernel/vm/vm_param.h>
#include <kernel/vm/vm_prot.h>
#include <mach/framebuffer.h>
#include <mach/machine/thread.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "locore/locore_internal.h"
#define TRAP_NAMES_ARRAY s_trap_names
#include "trapno.h"

#include "proc_reg.h"

extern char g_ter_112n_psf_start[];
static uint32_t x = 0, y = 0;

char *g_symstart;
char *g_symend;

static void vdump_string(const char *fmt, va_list args)
{
        char out_buf[0x100];
        size_t slen;
        font_data f;
        uint32_t fgbg[] = {fb_make_rgb(0xFF, 0xFF, 0xFF), fb_make_rgb(0, 0, 0)};
        psf_extract_font_data(g_ter_112n_psf_start, &f);
        out_buf[slen = vsnprintf(out_buf, sizeof(out_buf), fmt, args)] = 0;
        fb_draw_string(out_buf, slen, &x, &y, &f, fgbg, 0, nullptr);
}

static void dump_string(const char *fmt, ...)
{
        va_list args;
        va_start(args, fmt);
        vdump_string(fmt, args);
        va_end(args);
}

static const char *get_symbol(vm_address_t a_va)
{
        if (a_va == 0)
        {
                return "nullptr";
        }

        for (char *ptr = g_symstart; ptr < g_symend;)
        {
                uint32_t lo_addr = *((uint32_t *)ptr);
                ptr += sizeof(uint32_t);

                uint32_t sz = *((uint32_t *)ptr);
                ptr += sizeof(uint32_t);

                uint32_t lo_va = ((uint32_t)(a_va & UINT32_MAX));

                if ((lo_va >= lo_addr) && (lo_va <= (lo_addr + sz)))
                {
                        return ptr;
                }

                ptr += (strlen(ptr) + 1);
        }

        return nullptr;
}

void locore_kern_trap(struct amd64_saved_state *a_saved_state)
{

        if (a_saved_state->trapnum == TRAP_PAGE_FAULT)
        {
                vm_prot_t prot = VM_PROT_READ;

                if (a_saved_state->error_code & 2 /*T_PF_WRITE*/)
                {
                        prot |= VM_PROT_WRITE;
                }

                vm_address_t fault_page_addr = trunc_page(a_saved_state->cr2);

                kern_return_t retval = vm_fault(kernel_map, fault_page_addr, prot, true);

                if (retval == KERN_SUCCESS)
                {
                        locore_exit_iret(a_saved_state);
                        UNREACHABLE();
                }
        }

        if (a_saved_state->trapnum == TRAP_BREAKPOINT)
        {
                dump_string("PANIC: %s:%d: %s", //
                            (char *)a_saved_state->rdi,
                            (int)(a_saved_state->rsi),
                            (char *)(a_saved_state->rdx));
        }

        dump_string("\nKERNEL TRAP: %s(%d)\n\n", s_trap_names[a_saved_state->trapnum], a_saved_state->trapnum);

        dump_string(
            "RAX=%p RBX=%p RCX=%p RDX=%p\n", a_saved_state->rax, a_saved_state->rbx, a_saved_state->rcx, a_saved_state->rdx);

        dump_string(
            "RSI=%p RDI=%p RBP=%p RSP=%p\n", a_saved_state->rsi, a_saved_state->rdi, a_saved_state->rbp, a_saved_state->return_rsp);

        dump_string("R8 =%p R9 =%p R10=%p R11=%p\n", a_saved_state->r8, a_saved_state->r9, a_saved_state->r10, a_saved_state->r11);

        dump_string(
            "R12=%p R13=%p R14=%p R15=%p\n", a_saved_state->r12, a_saved_state->r13, a_saved_state->r14, a_saved_state->r15);

        dump_string("RIP=%p RFL=%p CR2=%p ERR=%p\n",
                    a_saved_state->return_rip,
                    a_saved_state->return_rflags,
                    a_saved_state->cr2,
                    a_saved_state->error_code);

        dump_string("\nCallstack\n");

        char const *sym = get_symbol(a_saved_state->return_rip);

        if (sym != nullptr)
        {
                dump_string("-> %04x:%p:%s\n", 0, a_saved_state->return_rsp, sym);
        }
        else
        {
                dump_string("-> %04x:%p:%p\n", 0, a_saved_state->return_rsp, a_saved_state->return_rip);
        }

        for (vm_address_t prev_rbp_ptr = a_saved_state->rbp, callnum = 1;
             prev_rbp_ptr != 0 && ((vm_address_t *)(prev_rbp_ptr))[1] != 0;
             prev_rbp_ptr = *((vm_address_t *)prev_rbp_ptr), callnum++)
        {
                vm_address_t func_addr = ((vm_address_t *)(prev_rbp_ptr))[1];
                sym                    = get_symbol(func_addr);

                if (sym != nullptr)
                {
                        dump_string("   %04x:%p:%s\n", callnum, prev_rbp_ptr, sym);
                }
                else
                {
                        dump_string("   %04x:%p:%p\n", callnum, prev_rbp_ptr, func_addr);
                }
        }

        dump_string("Happy bug hunting! :)\n");

        while (1)
        {
                cli();
                locore_cpu_idle();
        }

        UNREACHABLE();
}

void locore_kern_interrupt(struct amd64_saved_state *a_saved_state)
{
        locore_interrupt_common(a_saved_state, true);

        locore_exit_iret(a_saved_state);

        UNREACHABLE();
}