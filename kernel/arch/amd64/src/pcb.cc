/*
 * Mach Operating System
 * Copyright (c) 1991,1990 Carnegie Mellon University
 * All Rights Reserved.
 *
 * Permission to use, copy, modify and distribute this software and its
 * documentation is hereby granted, provided that both the copyright
 * notice and this permission notice appear in all copies of the
 * software, derivative works or modified versions, and any portions
 * thereof, and that both notices appear in supporting documentation.
 *
 * CARNEGIE MELLON ALLOWS FREE USE OF THIS SOFTWARE IN ITS "AS IS"
 * CONDITION.  CARNEGIE MELLON DISCLAIMS ANY LIABILITY OF ANY KIND FOR
 * ANY DAMAGES WHATSOEVER RESULTING FROM THE USE OF THIS SOFTWARE.
 *
 * Carnegie Mellon requests users of this software to return to
 *
 *  Software Distribution Coordinator  or  Software.Distribution@CS.CMU.EDU
 *  School of Computer Science
 *  Carnegie Mellon University
 *  Pittsburgh PA 15213-3890
 *
 * any improvements or extensions that they make and grant Carnegie Mellon
 * the rights to redistribute these changes.
 */
#include <kernel/kern_return.h>
#include <kernel/sched_prim.h>
#include <kernel/vm/pmap.h>
#include <kernel/vm/vm_param.h>
#include <mach/machine/fpu.h>
#include <mach/machine/thread.h>
#include <kernel/thread_data.h>
#include <kernel/thread_status.h>
#include <strings.h>

#include "eflags.h"
#include "gdt.h"
#include "ktss.h"
#include "proc_reg.h"
#include "msr.h"

#if NCPUS > 1 && DAMIR
#include <i386/mp_desc.h>
#endif

#include <kernel/vm/vm_kern.h>
#include <kernel/vm/vm_map.h>
#include <kernel/zalloc.h>

extern thread_t Switch_context();
extern void Thread_continue();

// extern iopb_tss_t iopb_create();
// extern void iopb_destroy();
// extern void user_ldt_free();

// zone_t pcb_zone;
using pcb_zone_t = zone<pcb, THREAD_MAX, THREAD_CHUNK>::type;

static pcb_zone_t pcb_zone;

static_assert(sizeof(struct fpsave_state) == 512, "Wrong fp_state size");

vm_offset_t kernel_stack[NCPUS]; /* top of active_stack */

/*
 *	stack_attach:
 *
 *	Attach a kernel stack to a thread.
 */

void stack_attach(thread_t thread, vm_offset_t stack, void *continuation)
{
        // counter(if (++c_stacks_current > c_stacks_max) c_stacks_max = c_stacks_current);

        thread->kernel_stack = stack;

        /*
         *	We want to run continuation, giving it as an argument
         *	the return value from Load_context/Switch_context.
         *	Thread_continue takes care of the mismatch between
         *	the argument-passing/return-value conventions.
         *	This function will not return normally,
         *	so we don`t have to worry about a return address.
         */
        /* Return to thread continue */
        STACK_IKS(stack)->k_rip = (vm_offset_t)locore_thread_continue;
        /* The stack starts after AMD64 Exception link*/
        STACK_IKS(stack)->k_rsp = (vm_offset_t)STACK_IEL(stack);
        /* First arg to Thread_continue */
        STACK_IKS(stack)->k_rbx = (vm_offset_t)continuation;

        /*
         *	Point top of kernel stack to user`s registers.
         */
        STACK_IEL(stack)->saved_state = &thread->pcb->iss;
}

/*
 *	stack_detach:
 *
 *	Detaches a kernel stack from a thread, returning the old stack.
 */

vm_offset_t stack_detach(thread_t thread)
{
        vm_offset_t stack;

        // counter(if (--c_stacks_current < c_stacks_min) c_stacks_min = c_stacks_current);

        stack                = thread->kernel_stack;
        thread->kernel_stack = 0;

        return stack;
}

#define SA_SIZE_MULTIPLE_OF(type, s) static_assert((sizeof(type) % (s)) == 0, #type " must be a multiple of " #s "")

SA_SIZE_MULTIPLE_OF(struct pcb, 16);
SA_SIZE_MULTIPLE_OF(struct amd64_saved_state, 16);

static void switch_ktss(pcb_t pcb)
{
        // int mycpu = cpu_number();
        {
                // iopb_tss_t tss = pcb->ims.io_tss;
                vm_offset_t pcb_stack_top;

                /*
                 *	Save a pointer to the top of the "kernel" stack -
                 *	actually the place in the PCB where a trap into
                 *	kernel mode will push the registers.
                 *	The location depends on V8086 mode.  If we are
                 *	not in V8086 mode, then a trap into the kernel
                 *	won`t save the v86 segments, so we leave room.
                 */
                pcb_stack_top = ((vm_offset_t)(&pcb->iss)) + sizeof(struct amd64_saved_state);
                tss_set_kstack(pcb_stack_top);
                // curr_ktss(mycpu)->rsp[0] = pcb_stack_top;
        }
#if DAMIR
        {
                pcb_stack_top = (pcb->iss.efl & EFL_VM) ? (int)(&pcb->iss + 1) : (int)(&pcb->iss.v86_segs);

                if (tss == 0)
                {
                        /*
                         *	No per-thread IO permissions.
                         *	Use standard kernel TSS.
                         */
                        if (!(gdt_desc_p(mycpu, KERNEL_TSS)->type & SEG_TYPE_TSS_BUSY))
                        {
                                set_tr(KERNEL_TSS);
                        }
                        curr_ktss(mycpu)->esp0 = pcb_stack_top;
                }
                else
                {
                        /*
                         * Set the IO permissions.  Use this thread`s TSS.
                         */
                        *gdt_desc_p(mycpu, USER_TSS) = *(struct real_descriptor *)tss->iopb_desc;
                        tss->tss.esp0                = pcb_stack_top;
                        set_tr(USER_TSS);
                        gdt_desc_p(mycpu, KERNEL_TSS)->access &= ~SEG_TYPE_TSS_BUSY;
                }
        }

        {
                user_ldt_t ldt = pcb->ims.ldt;
                /*
                 * Set the thread`s LDT.
                 */
                if (ldt == 0)
                {
                        /*
                         * Use system LDT.
                         */
                        set_ldt(KERNEL_LDT);
                }
                else
                {
                        /*
                         * Thread has its own LDT.
                         */
                        *gdt_desc_p(mycpu, USER_LDT) = ldt->desc;
                        set_ldt(USER_LDT);
                }
        }

#endif
        /*
         * Load the floating-point context, if necessary.
         */

        fpu_load_context(pcb);
}

/*
 *	stack_handoff:
 *
 *	Move the current thread's kernel stack to the new thread.
 */

void stack_handoff(thread_t old_thread, thread_t new_thread)
{
        int mycpu = cpu_number();
        vm_offset_t stack;

        /*
         *	Save FP registers if in use.
         */

        fpu_save_context(old_thread);
        /*
         *	Switch address maps if switching tasks.
         */
        {
                task_t old_task, new_task;

                if ((old_task = old_thread->task) != (new_task = new_thread->task))
                {
                        PMAP_DEACTIVATE_USER(vm_map_pmap(old_task->map), old_thread, mycpu);
                        PMAP_ACTIVATE_USER(vm_map_pmap(new_task->map), new, mycpu);
                }
        }

        /*
         *	Load the rest of the user state for the new thread
         */
        switch_ktss(new_thread->pcb);

        /*
         *	Switch to new thread
         */
        stack                    = current_stack();
        old_thread->kernel_stack = 0;
        new_thread->kernel_stack = stack;
        active_threads[mycpu]    = new_thread;
        wrmsr(MSR_FS_BASE, new_thread->tls);

        /*
         *	Switch exception link to point to new
         *	user registers.
         */
        STACK_IEL(stack)->saved_state = &new_thread->pcb->iss;
}

/*
 * Switch to the first thread on a CPU.
 */
void load_context(thread_t new_thread)
{
        switch_ktss(new_thread->pcb);
        locore_load_context(new_thread);
}

/*
 * Switch to a new thread.
 * Save the old thread`s kernel state or continuation,
 * and return it.
 */
thread_t switch_context(thread_t old_thread, void (*continuation)(), thread_t new_thread)
{
        /*
         *	Save FP registers if in use.
         */
        fpu_save_context(old);

        /*
         *	Switch address maps if switching tasks.
         */
        {
                task_t old_task, new_task;
                int mycpu = cpu_number();

                if ((old_task = old_thread->task) != (new_task = new_thread->task))
                {
                        PMAP_DEACTIVATE_USER(vm_map_pmap(old_task->map), old, mycpu);
                        PMAP_ACTIVATE_USER(vm_map_pmap(new_task->map), new, mycpu);
                }
        }

        /*
         *	Load the rest of the user state for the new thread
         */
        switch_ktss(new_thread->pcb);

        return locore_switch_context(old_thread, continuation, new_thread);
}

void pcb_module_init()
{
        // pcb_zone = zinit(sizeof(struct pcb),
        //                 THREAD_MAX * sizeof(struct pcb),
        //                 THREAD_CHUNK * sizeof(struct pcb),
        //                 0,
        //                 "i386 pcb state");

        pcb_zone_t::init(&pcb_zone, "i386 pcb state");

        fpu_module_init();
#if DAMIR
        iopb_init();
#endif
}

void pcb_init(thread_t thread)
{
        pcb_t pcb = pcb_zone.alloc();

        KASSERT(pcb != nullptr);

        // counter(if (++c_threads_current > c_threads_max) c_threads_max = c_threads_current);

        /*
         *	We can't let random values leak out to the user.
         */
        bzero((char *)pcb, sizeof *pcb);
        simple_lock_init(&pcb->lock);

        /*
         *	Guarantee that the bootstrapped thread will be in user
         *	mode.
         */
        pcb->iss.return_cs = GDT_USER_CS_SELECTOR;
        pcb->iss.return_ss = GDT_USER_DS_SELECTOR;
        // pcb->iss.ds = USER_DS; DAMIR
        // pcb->iss.es = USER_DS;
        // pcb->iss.fs = USER_DS;
        // pcb->iss.gs = USER_DS;

        pcb->iss.return_rflags = EFL_USER_SET;

        extern struct fpsave_state sss;

        pcb->ifps = sss;

        thread->pcb = pcb;
}

void pcb_terminate(thread_t thread)
{
        pcb_t pcb = thread->pcb;

        // counter(if (--c_threads_current < c_threads_min) c_threads_min = c_threads_current);

#if DAMIR
        if (pcb->ims.io_tss != 0)
        {
                iopb_destroy(pcb->ims.io_tss);
        }
        if (pcb->ims.ldt != 0)
        {
                user_ldt_free(pcb->ims.ldt);
        }

        if (pcb->ims.ifps != 0)
        {
                fp_free(pcb->ims.ifps);
        }
#endif

        // If we are the current FPU holder, release us
        fpu_free(&pcb->ifps);

        // zfree(pcb_zone, (vm_offset_t)pcb);
        pcb_zone.free(pcb);
        thread->pcb = 0;
}

/*
 *	pcb_collect:
 *
 *	Attempt to free excess pcb memory.
 */
#if 0
static void pcb_collect(thread_t /*thread*/) {}
#endif
/*
 *	thread_setstatus:
 *
 *	Set the status of the specified thread.
 */

/*
 *	thread_dup:
 *
 *	Duplicate the user's state of a thread.  This is only used to perform
 *	the Unix fork operation.
 */
void thread_dup(thread_t parent, thread_t child, natural_t **a_child_tls, natural_t **a_child_sp, natural_t **a_child_retval)
{

        *(child->pcb) = *(parent->pcb);

        if (a_child_tls != nullptr)
        {
                *a_child_tls = &child->pcb->iss.fsbase;
        }

        if (a_child_sp != nullptr)
        {
                *a_child_sp = &child->pcb->iss.return_rsp;
        }

        if (a_child_retval != nullptr)
        {
                *a_child_retval = &child->pcb->iss.rax;
        }
}

kern_return_t thread_setstatus(thread_t thread, enum THREAD_STATE_FLAVOR flavor, thread_state_t tstate, natural_t count)
{
        switch (flavor)
        {
                case THREAD_STATE_FLAVOR_GP_REGS:
                {
                        struct amd64_thread_state *state;
                        struct amd64_saved_state *saved_state;

                        if (count < THREAD_STATE_GP_REGS_SIZE)
                        {
                                return KERN_INVALID_ARGUMENT;
                        }

                        state       = (struct amd64_thread_state *)tstate;
                        saved_state = USER_REGS(thread);

                        /*
                         * General registers
                         */
                        saved_state->fsbase        = state->fsbase;
                        saved_state->cr2           = state->cr2;
                        saved_state->rax           = state->rax;
                        saved_state->rbx           = state->rbx;
                        saved_state->rcx           = state->rcx;
                        saved_state->rdx           = state->rdx;
                        saved_state->rsi           = state->rsi;
                        saved_state->rdi           = state->rdi;
                        saved_state->rbp           = state->rbp;
                        saved_state->r8            = state->r8;
                        saved_state->r9            = state->r9;
                        saved_state->r10           = state->r10;
                        saved_state->r11           = state->r11;
                        saved_state->r12           = state->r12;
                        saved_state->r13           = state->r13;
                        saved_state->r14           = state->r14;
                        saved_state->r15           = state->r15;
                        saved_state->return_rip    = state->rip;
                        saved_state->return_rsp    = state->rsp;
                        saved_state->return_rflags = state->rflags | EFL_USER_SET;

                        saved_state->trapnum = 0x80;

                        break;
                }

                default:
                        return KERN_INVALID_ARGUMENT;
        }

        return KERN_SUCCESS;

        panic("Not ready!");
#if DAMIR
        switch (flavor)
        {
                case i386_THREAD_STATE:
                case i386_REGS_SEGS_STATE:
                {
                        struct i386_thread_state *state;
                        struct i386_saved_state *saved_state;

                        if (count < i386_THREAD_STATE_COUNT)
                        {
                                return (KERN_INVALID_ARGUMENT);
                        }

                        if (flavor == i386_REGS_SEGS_STATE)
                        {
                                /*
                                 * Code and stack selectors must not be null,
                                 * and must have user protection levels.
                                 * Only the low 16 bits are valid.
                                 */
                                state->cs &= 0xffff;
                                state->ss &= 0xffff;
                                state->ds &= 0xffff;
                                state->es &= 0xffff;
                                state->fs &= 0xffff;
                                state->gs &= 0xffff;

                                if (state->cs == 0 || (state->cs & SEL_PL) != SEL_PL_U || state->ss == 0
                                    || (state->ss & SEL_PL) != SEL_PL_U)
                                {
                                        return KERN_INVALID_ARGUMENT;
                                }
                        }
                        // dami0791
                        state = (struct i386_thread_state *)tstate;
                        // 22KirsebærUbåd
                        // ZoologCirkus05
                        // Mod24Udsalg
                        // ØrePingvin72
                        saved_state = USER_REGS(thread);

                        /*
                         * General registers
                         */
                        saved_state->edi  = state->edi;
                        saved_state->esi  = state->esi;
                        saved_state->ebp  = state->ebp;
                        saved_state->uesp = state->uesp;
                        saved_state->ebx  = state->ebx;
                        saved_state->edx  = state->edx;
                        saved_state->ecx  = state->ecx;
                        saved_state->eax  = state->eax;
                        saved_state->eip  = state->eip;
                        saved_state->efl  = (state->efl & ~EFL_USER_CLEAR) | EFL_USER_SET;

                        /*
                         * Segment registers.  Set differently in V8086 mode.
                         */
                        if (state->efl & EFL_VM)
                        {
                                /*
                                 * Set V8086 mode segment registers.
                                 */
                                saved_state->cs              = state->cs & 0xffff;
                                saved_state->ss              = state->ss & 0xffff;
                                saved_state->v86_segs.v86_ds = state->ds & 0xffff;
                                saved_state->v86_segs.v86_es = state->es & 0xffff;
                                saved_state->v86_segs.v86_fs = state->fs & 0xffff;
                                saved_state->v86_segs.v86_gs = state->gs & 0xffff;

                                /*
                                 * Zero protected mode segment registers.
                                 */
                                saved_state->ds = 0;
                                saved_state->es = 0;
                                saved_state->fs = 0;
                                saved_state->gs = 0;

                                if (thread->pcb->ims.v86s.int_table)
                                {
                                        /*
                                         * Hardware assist on.
                                         */
                                        thread->pcb->ims.v86s.flags = state->efl & (EFL_TF | EFL_IF);
                                }
                        }
                        else if (flavor == i386_THREAD_STATE)
                        {
                                /*
                                 * 386 mode.  Set segment registers for flat
                                 * 32-bit address space.
                                 */
                                saved_state->cs = GDT_USER_CS;
                                saved_state->ss = GDT_USER_DS;
                                saved_state->ds = GDT_USER_DS;
                                saved_state->es = GDT_USER_DS;
                                saved_state->fs = GDT_USER_DS;
                                saved_state->gs = GDT_USER_DS;
                        }
                        else
                        {
                                /*
                                 * User setting segment registers.
                                 * Code and stack selectors have already been
                                 * checked.  Others will be reset by 'iret'
                                 * if they are not valid.
                                 */
                                saved_state->cs = state->cs;
                                saved_state->ss = state->ss;
                                saved_state->ds = state->ds;
                                saved_state->es = state->es;
                                saved_state->fs = state->fs;
                                saved_state->gs = state->gs;
                        }
                        break;
                }

                case i386_FLOAT_STATE:
                {
                        if (count < i386_FLOAT_STATE_COUNT)
                        {
                                return (KERN_INVALID_ARGUMENT);
                        }

                        return fpu_set_state(thread, (struct i386_float_state *)tstate);
                }

                /*
                 * Temporary - replace by i386_io_map
                 */
                case i386_ISA_PORT_MAP_STATE:
                {
                        struct i386_isa_port_map_state *state;
                        iopb_tss_t tss;

                        if (count < i386_ISA_PORT_MAP_STATE_COUNT)
                        {
                                return (KERN_INVALID_ARGUMENT);
                        }

#if 0
		/*
		 *	If the thread has no ktss yet,
		 *	we must allocate one.
		 */

		state = (struct i386_isa_port_map_state *) tstate;
		tss = thread->pcb->ims.io_tss;
		if (tss == 0) {
			tss = iopb_create();
			thread->pcb->ims.io_tss = tss;
		}

		bcopy((char *) state->pm,
		      (char *) tss->bitmap,
		      sizeof state->pm);
#endif
                        break;
                }

                case i386_V86_ASSIST_STATE:
                {
                        struct i386_v86_assist_state *state;
                        vm_offset_t int_table;
                        int int_count;

                        if (count < i386_V86_ASSIST_STATE_COUNT)
                        {
                                return KERN_INVALID_ARGUMENT;
                        }

                        state     = (struct i386_v86_assist_state *)tstate;
                        int_table = state->int_table;
                        int_count = state->int_count;

                        if (int_table >= VM_MAX_ADDRESS
                            || int_table + int_count * sizeof(struct v86_interrupt_table) > VM_MAX_ADDRESS)
                        {
                                return KERN_INVALID_ARGUMENT;
                        }

                        thread->pcb->ims.v86s.int_table = int_table;
                        thread->pcb->ims.v86s.int_count = int_count;

                        thread->pcb->ims.v86s.flags = USER_REGS(thread)->return_rflags & (EFL_TF | EFL_IF);

                        break;
                }

                default:
                        return (KERN_INVALID_ARGUMENT);
        }
#endif
        return (KERN_SUCCESS);
}

/*
 *	thread_getstatus:
 *
 *	Get the status of the specified thread.
 */

kern_return_t thread_getstatus(thread_t thread,
                               enum THREAD_STATE_FLAVOR flavor,
                               thread_state_t tstate, /* pointer to OUT array */
                               natural_t *count /* IN/OUT */)
{

        switch (flavor)
        {
                case THREAD_STATE_FLAVOR_GP_REGS:
                {
                        struct amd64_thread_state *state;
                        struct amd64_saved_state *saved_state;

                        if (*count < THREAD_STATE_GP_REGS_SIZE)
                        {
                                return KERN_INVALID_ARGUMENT;
                        }

                        state       = (struct amd64_thread_state *)tstate;
                        saved_state = USER_REGS(thread);

                        /*
                         * General registers
                         */
                        state->fsbase = saved_state->fsbase;
                        state->cr2    = saved_state->cr2;
                        state->rax    = saved_state->rax;
                        state->rbx    = saved_state->rbx;
                        state->rcx    = saved_state->rcx;
                        state->rdx    = saved_state->rdx;
                        state->rsi    = saved_state->rsi;
                        state->rdi    = saved_state->rdi;
                        state->rbp    = saved_state->rbp;
                        state->r8     = saved_state->r8;
                        state->r9     = saved_state->r9;
                        state->r10    = saved_state->r10;
                        state->r11    = saved_state->r11;
                        state->r12    = saved_state->r12;
                        state->r13    = saved_state->r13;
                        state->r14    = saved_state->r14;
                        state->r15    = saved_state->r15;
                        state->rip    = saved_state->return_rip;
                        state->rsp    = saved_state->return_rsp;
                        state->rflags = saved_state->return_rflags;

                        *count = THREAD_STATE_GP_REGS_SIZE;

                        break;
                }

                default:
                        return KERN_INVALID_ARGUMENT;
        }

        return KERN_SUCCESS;

        panic("Not ready!");
#if DAMIR
        switch (flavor)
        {
                case THREAD_STATE_FLAVOR_LIST:
                        if (*count < 4)
                        {
                                return (KERN_INVALID_ARGUMENT);
                        }
                        tstate[0] = i386_THREAD_STATE;
                        tstate[1] = i386_FLOAT_STATE;
                        tstate[2] = i386_ISA_PORT_MAP_STATE;
                        tstate[3] = i386_V86_ASSIST_STATE;
                        *count    = 4;
                        break;

                case i386_THREAD_STATE:
                case i386_REGS_SEGS_STATE:
                {
                        struct i386_thread_state *state;
                        struct i386_saved_state *saved_state;

                        if (*count < i386_THREAD_STATE_COUNT)
                        {
                                return (KERN_INVALID_ARGUMENT);
                        }

                        state       = (struct i386_thread_state *)tstate;
                        saved_state = USER_REGS(thread);

                        /*
                         * General registers.
                         */
                        state->edi  = saved_state->edi;
                        state->esi  = saved_state->esi;
                        state->ebp  = saved_state->ebp;
                        state->ebx  = saved_state->ebx;
                        state->edx  = saved_state->edx;
                        state->ecx  = saved_state->ecx;
                        state->eax  = saved_state->eax;
                        state->eip  = saved_state->eip;
                        state->efl  = saved_state->efl;
                        state->uesp = saved_state->uesp;

                        state->cs = saved_state->cs;
                        state->ss = saved_state->ss;
                        if (saved_state->efl & EFL_VM)
                        {
                                /*
                                 * V8086 mode.
                                 */
                                state->ds = saved_state->v86_segs.v86_ds & 0xffff;
                                state->es = saved_state->v86_segs.v86_es & 0xffff;
                                state->fs = saved_state->v86_segs.v86_fs & 0xffff;
                                state->gs = saved_state->v86_segs.v86_gs & 0xffff;

                                if (thread->pcb->ims.v86s.int_table)
                                {
                                        /*
                                         * Hardware assist on
                                         */
                                        if ((thread->pcb->ims.v86s.flags & (EFL_IF | V86_IF_PENDING)) == 0)
                                        {
                                                state->efl &= ~EFL_IF;
                                        }
                                }
                        }
                        else
                        {
                                /*
                                 * 386 mode.
                                 */
                                state->ds = saved_state->ds & 0xffff;
                                state->es = saved_state->es & 0xffff;
                                state->fs = saved_state->fs & 0xffff;
                                state->gs = saved_state->gs & 0xffff;
                        }
                        *count = i386_THREAD_STATE_COUNT;
                        break;
                }

                case i386_FLOAT_STATE:
                {
                        if (*count < i386_FLOAT_STATE_COUNT)
                        {
                                return (KERN_INVALID_ARGUMENT);
                        }

                        *count = i386_FLOAT_STATE_COUNT;
                        return fpu_get_state(thread, (struct i386_float_state *)tstate);
                }

                /*
                 * Temporary - replace by i386_io_map
                 */
                case i386_ISA_PORT_MAP_STATE:
                {
                        struct i386_isa_port_map_state *state;
                        iopb_tss_t tss;

                        if (*count < i386_ISA_PORT_MAP_STATE_COUNT)
                        {
                                return (KERN_INVALID_ARGUMENT);
                        }

                        state = (struct i386_isa_port_map_state *)tstate;
                        tss   = thread->pcb->ims.io_tss;

                        if (tss == 0)
                        {
                                int i;

                                /*
                                 *	The thread has no ktss, so no IO permissions.
                                 */

                                for (i = 0; i < sizeof state->pm; i++)
                                {
                                        state->pm[i] = 0xff;
                                }
                        }
                        else
                        {
                                /*
                                 *	The thread has its own ktss.
                                 */

                                bcopy((char *)tss->bitmap, (char *)state->pm, sizeof state->pm);
                        }

                        *count = i386_ISA_PORT_MAP_STATE_COUNT;
                        break;
                }

                case i386_V86_ASSIST_STATE:
                {
                        struct i386_v86_assist_state *state;

                        if (*count < i386_V86_ASSIST_STATE_COUNT)
                        {
                                return KERN_INVALID_ARGUMENT;
                        }

                        state            = (struct i386_v86_assist_state *)tstate;
                        state->int_table = thread->pcb->ims.v86s.int_table;
                        state->int_count = thread->pcb->ims.v86s.int_count;

                        *count = i386_V86_ASSIST_STATE_COUNT;
                        break;
                }

                default:
                        return (KERN_INVALID_ARGUMENT);
        }
#endif
        return (KERN_SUCCESS);
}

/*
 * Return prefered address of user stack.
 * Always returns low address.  If stack grows up,
 * the stack grows away from this address;
 * if stack grows down, the stack grows towards this
 * address.
 */
#if 0
static vm_offset_t user_stack_low(vm_size_t stack_size) { return (VM_MAX_ADDRESS - stack_size); }
#endif
void thread_clear_pcb(thread_t a_thread)
{
        a_thread->pcb->iss                = {};
        a_thread->pcb->ifps.fp_regs_state = {};
        a_thread->pcb->iss.return_cs      = GDT_USER_CS_SELECTOR;
        a_thread->pcb->iss.return_ss      = GDT_USER_DS_SELECTOR;
        a_thread->pcb->iss.return_rflags  = EFL_USER_SET;
}
/*
 * Allocate argument area and set registers for first user thread.
 */
vm_offset_t thread_set_user_regs(thread_t a_thread,
                                 vm_offset_t stack_base /* low address */,
                                 vm_offset_t stack_size,
                                 vm_offset_t entry,
                                 vm_size_t arg_size)
{
        vm_offset_t arg_addr;
        struct amd64_saved_state *saved_state;

        arg_size = arg_size + (((sizeof(natural_t) * 2) - 1) & ~((sizeof(natural_t) * 2) - 1));
        arg_addr = stack_base + stack_size - arg_size;

        saved_state = USER_REGS(a_thread);
        bzero(saved_state, sizeof(*saved_state));
        /*
         *	Guarantee that the bootstrapped thread will be in user
         *	mode.
         */
        saved_state->return_cs     = GDT_USER_CS_SELECTOR;
        saved_state->return_ss     = GDT_USER_DS_SELECTOR;
        saved_state->return_rflags = EFL_USER_SET;
        saved_state->return_rsp    = arg_addr;
        saved_state->return_rip    = entry;

        return (arg_addr);
}
