#pragma once

struct amd64_saved_state;

#ifdef __cplusplus
extern "C" {
#endif

void hardclock(int iunit /* 'unit' number */,
               int old_ipl /* old interrupt level */,
               int kernel_mode,
               struct amd64_saved_state *regs /* saved registers */);

#ifdef __cplusplus
}
#endif