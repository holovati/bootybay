
#include <kernel/vm/pmap.h>
#include <kernel/zalloc.h>

#include <emerixx/pci.h>

#include <mach/machine/bus/pci/pci_cfgreg.h>

ZONE_DEFINE(pci_dev, s_pci_devs, 0x20, 0x20, ZONE_EXHAUSTIBLE, 0x20);

int pci_register_driver(struct pci_driver *a_pci_driver)
{
        pci_dev *dev = nullptr;

        for (int bus = 0; bus <= PCI_BUSMAX; bus++)
        {
                for (int slot = 0; slot <= PCI_SLOTMAX; ++slot)
                {
                        for (int func = 0; func <= PCI_FUNCMAX; ++func)
                        {
                                for (pci_device_id const *dev_id = a_pci_driver->id_table; dev_id->vendor != 0; ++dev_id)
                                {
                                        pci_vendor_id_t ven_num
                                            = pci_cfgregread(bus, slot, func, PCIR_VENDOR, sizeof(ven_num)) & 0xFFFF;
                                        pci_product_id_t dev_num
                                            = pci_cfgregread(bus, slot, func, PCIR_DEVICE, sizeof(dev_num)) & 0xFFFF;

                                        if ((dev_id->vendor == ven_num) && (dev_id->device == dev_num))
                                        {
                                                if (dev == nullptr)
                                                {
                                                        dev = s_pci_devs.alloc();
                                                }

                                                dev->device         = dev_num;
                                                dev->vendor         = ven_num;
                                                dev->devfn          = PCI_DEVID(bus, PCI_DEVFN(slot, func));
                                                dev->irq            = 0xFF;
                                                dev->m_device.undef = nullptr;

                                                if (KERN_STATUS_SUCCESS(a_pci_driver->probe(dev, dev_id)))
                                                {
                                                        dev->driver = a_pci_driver;
                                                        dev         = nullptr;
                                                }
                                        }
                                }
                        }
                }
        }

        if (dev != nullptr)
        {
                s_pci_devs.free(dev);
        }

        return KERN_SUCCESS;
}

int pci_unregister_driver(void *d)
{
        //
        panic("Not ready");
}

int pci_read_config_dword(struct pci_dev *pdev, int reg, uint32_t *val)
{
        *val = pci_cfgregread(PCI_BUS_NUM(pdev->devfn), PCI_SLOT(pdev->devfn), PCI_FUNC(pdev->devfn), reg, sizeof(*val));
        return KERN_SUCCESS;
}

int pci_read_config_word(struct pci_dev *pdev, int reg, uint16_t *val)
{
        *val = (uint16_t)pci_cfgregread(PCI_BUS_NUM(pdev->devfn), PCI_SLOT(pdev->devfn), PCI_FUNC(pdev->devfn), reg, sizeof(*val));
        return KERN_SUCCESS;
}

int pci_read_config_byte(struct pci_dev *pdev, int reg, uint8_t *val)
{
        *val = (uint8_t)pci_cfgregread(PCI_BUS_NUM(pdev->devfn), PCI_SLOT(pdev->devfn), PCI_FUNC(pdev->devfn), reg, sizeof(*val));
        return KERN_SUCCESS;
}

int pci_write_config_dword(struct pci_dev *pdev, int reg, uint32_t val)
{
        pci_cfgregwrite(PCI_BUS_NUM(pdev->devfn), PCI_SLOT(pdev->devfn), PCI_FUNC(pdev->devfn), reg, (uint32_t)val, sizeof(val));
        return KERN_SUCCESS;
}

int pci_write_config_word(struct pci_dev *pdev, int reg, uint16_t val)
{
        pci_cfgregwrite(PCI_BUS_NUM(pdev->devfn), PCI_SLOT(pdev->devfn), PCI_FUNC(pdev->devfn), reg, (uint32_t)val, sizeof(val));
        return KERN_SUCCESS;
}

int pci_write_config_byte(struct pci_dev *pdev, int reg, uint8_t val)
{
        pci_cfgregwrite(PCI_BUS_NUM(pdev->devfn), PCI_SLOT(pdev->devfn), PCI_FUNC(pdev->devfn), reg, (uint32_t)val, sizeof(val));
        return KERN_SUCCESS;
}
#if 0
int pci_bus_read_config_word(struct pci_bus *bus, unsigned int devfn, int reg, u16 *val)
{
        // pcitag_t tag = pci_make_tag(bus->pc, bus->number, PCI_SLOT(devfn), PCI_FUNC(devfn));
        uint32_t v;

        // v    = pci_conf_read(bus->pc, tag, (reg & ~0x2));
        *val = (v >> ((reg & 0x2) * 8));
        return 0;
}

int pci_bus_read_config_byte(struct pci_bus *bus, unsigned int devfn, int reg, u8 *val)
{
        // pcitag_t tag = pci_make_tag(bus->pc, bus->number, PCI_SLOT(devfn), PCI_FUNC(devfn));
        uint32_t v;

        // v    = pci_conf_read(bus->pc, tag, (reg & ~0x3));
        *val = (v >> ((reg & 0x3) * 8));
        return 0;
}

int pci_bus_write_config_byte(struct pci_bus *bus, unsigned int devfn, int reg, u8 val)
{
        // pcitag_t tag = pci_make_tag(bus->pc, bus->number, PCI_SLOT(devfn), PCI_FUNC(devfn));
        uint32_t v;

        // v = pci_conf_read(bus->pc, tag, (reg & ~0x3));
        v &= ~(0xff << ((reg & 0x3) * 8));
        v |= (val << ((reg & 0x3) * 8));
        // pci_conf_write(bus->pc, tag, (reg & ~0x3), v);
        return 0;
}
#endif
int pci_enable_device(struct pci_dev *pdev)
{
        uint16_t cmdr;
        pci_read_config_word(pdev, PCIR_COMMAND, &cmdr);
        cmdr |= (PCIM_CMD_MEMEN | PCIM_CMD_PORTEN | PCIM_CMD_MWRICEN);
        pci_write_config_word(pdev, PCIR_COMMAND, cmdr);

        uint8_t irqline;
        pci_read_config_byte(pdev, PCIR_INTLINE, &irqline);

        pdev->irq = (int)(irqline);

        return KERN_SUCCESS;
}

void pci_set_master(struct pci_dev *pdev)
{
        uint16_t cmdr;
        pci_read_config_word(pdev, PCIR_COMMAND, &cmdr);
        cmdr |= PCIM_CMD_BUSMASTEREN;
        pci_write_config_word(pdev, PCIR_COMMAND, cmdr);
}

natural_t pci_resource_len(struct pci_dev *pdev, int bar)
{
        natural_t retval = 0;

        uint32_t barval;

        pci_read_config_dword(pdev, PCIR_BAR(bar), &barval);

        if ((barval != 0) && PCI_BAR_MEM(barval))
        {
                pci_write_config_dword(pdev, PCIR_BAR(bar), 0xFFFFFFFF);

                uint32_t tmp;
                pci_read_config_dword(pdev, PCIR_BAR(bar), &tmp);

                pci_write_config_dword(pdev, PCIR_BAR(bar), barval);

                retval = ~tmp + 1;
        }

        return retval;
}

void __iomem *pci_iomap(struct pci_dev *a_pci_dev, int a_bar, natural_t a_size)
{
        natural_t retval = 0;

        uint32_t barval;

        pci_read_config_dword(a_pci_dev, PCIR_BAR(a_bar), &barval);

        if ((barval != 0) && PCI_BAR_MEM(barval))
        {
                retval = (natural_t)pmap_mapdev_uncacheable((natural_t)(barval & PCIM_BAR_MEM_BASE), a_size);
        }

        return ((void __iomem *)retval);
}