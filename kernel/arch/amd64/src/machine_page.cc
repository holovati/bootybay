#include <aux/init.h>

#include <etl/bit.hh>

#include "machine_page.h"

static_assert(sizeof(machine_page) == 256);

#define NPVE 0x600

#define PAGE_EMB_PVE0_USED etl::bit<decltype(machine_page::m_tmp), 0>::value
#define PAGE_EMB_PVE1_USED etl::bit<decltype(machine_page::m_tmp), 1>::value

#define LRU_PVE_FIRST() (queue_containing_record(queue_first(&s_pve_lru), pv_entry_data, m_lru_link))
#define LRU_PVE_LAST()  (queue_containing_record(queue_last(&s_pve_lru), pv_entry_data, m_lru_link))

#define PAGE_PVE_FIRST(pg)    (queue_containing_record(queue_first(&((pg)->m_pve_list)), pv_entry_data, m_pg_link))
#define PAGE_PVE_LAST(pg)     (queue_containing_record(queue_last(&((pg)->m_pve_list)), pv_entry_data, m_pg_link))
#define PAGE_PVE_END(pg, pve) (queue_end(&(pg)->m_pve_list, &(pve)->m_pg_link))
#define PAGE_PVE_NEXT(pve)    (queue_containing_record(queue_next(&((pve)->m_pg_link)), pv_entry_data, m_pg_link))
#define PAGE_PVE_REMOVE(pve)  (remqueue(&(pve)->m_pg_link))

static queue_head_t s_pve_lru;

static pv_entry_data s_pve_storage[NPVE];

// ********************************************************************************************************************************
pv_entry_t pve_find(machine_page *m, pmap_t pmap, vm_address_t vma)
// ********************************************************************************************************************************
{
        for (pv_entry_t pve = PAGE_PVE_FIRST(m); PAGE_PVE_END(m, pve) == false; pve = PAGE_PVE_NEXT(pve))
        {
                if (pve->m_pmap == pmap && pve->m_vaddr == vma)
                {
                        return pve;
                }
        }

        return nullptr;
}

// ********************************************************************************************************************************
void pve_free(machine_page *m, pv_entry_t a_pve)
// ********************************************************************************************************************************
{
        if (&m->m_emb_pve[0] == a_pve)
        {
                m->m_tmp &= ~PAGE_EMB_PVE0_USED;
        }
        else if (&m->m_emb_pve[1] == a_pve)
        {
                m->m_tmp &= ~PAGE_EMB_PVE1_USED;
        }

        PAGE_PVE_REMOVE(a_pve);
        a_pve->m_pg_link = {};
        a_pve->m_pmap    = nullptr;
        a_pve->m_vaddr   = 0;
}

// ********************************************************************************************************************************
void pve_alloc(machine_page *m, pmap_t pmap, vm_address_t vma)
// ********************************************************************************************************************************
{
        pv_entry_t pve;

        if ((m->m_tmp & PAGE_EMB_PVE0_USED) == 0)
        {
                pve = &m->m_emb_pve[0];
                m->m_tmp |= PAGE_EMB_PVE0_USED;
        }
        else if ((m->m_tmp & PAGE_EMB_PVE1_USED) == 0)
        {
                pve = &m->m_emb_pve[1];
                m->m_tmp |= PAGE_EMB_PVE1_USED;
        }
        else
        {
                pve = LRU_PVE_LAST();

                if (pve->m_pmap != nullptr)
                {
                        pt_entry_t *pte = pmap_pte(pve->m_pmap, pve->m_vaddr, nullptr);

                        KASSERT(pte != nullptr);

                        m->m_pmap_attrubutes = ((*pte) & (INTEL_PTE_MOD | INTEL_PTE_REF));

                        if (*pte & INTEL_PTE_WIRED)
                        {
                                panic("pmap lru removing a wired page");
                        }

                        *pte = 0;

                        asm volatile("invlpg [%0]\n" : : "r"(pve->m_vaddr) : "memory");

                        remqueue(&pve->m_pg_link);
                }

                pve_touch(pve);
        }

        pve->m_pmap  = pmap;
        pve->m_vaddr = vma;
        enqueue_tail(&m->m_pve_list, &pve->m_pg_link);
}

// ********************************************************************************************************************************
void pve_enum(machine_page *m, pve_enum_cb_t a_cb, void *a_ud)
// ********************************************************************************************************************************
{
        for (pv_entry_t pve = PAGE_PVE_FIRST(m); PAGE_PVE_END(m, pve) == false;)
        {
                pv_entry_t pve_next = PAGE_PVE_NEXT(pve);
                a_cb(m, pve, a_ud);
                pve = pve_next;
        }
}

// ********************************************************************************************************************************
void pve_touch(pv_entry_t a_pve)
// ********************************************************************************************************************************
{
        if (((a_pve >= &s_pve_storage[0]) && (a_pve < &s_pve_storage[NPVE])) && (a_pve != LRU_PVE_FIRST()))
        {
                remqueue(&(a_pve)->m_lru_link);
                enqueue_head(&s_pve_lru, &(a_pve)->m_lru_link);
        }
}

// ********************************************************************************************************************************
static kern_return_t early_init()
// ********************************************************************************************************************************
{
        queue_init(&s_pve_lru);

        for (pv_entry_data &pve : s_pve_storage)
        {
                enqueue_tail(&s_pve_lru, &pve.m_lru_link);
        }

        return KERN_SUCCESS;
}

early_initcall(early_init);

static_assert(sizeof(machine_page) == 256);
