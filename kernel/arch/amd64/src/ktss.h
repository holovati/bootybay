#pragma once

static inline void tss_set_kstack(vm_address_t kstack)
{
        extern vm_address_t __tss_rsp0;

        if ((kstack & 15) != 0)
        {
                panic("tss stack not 16 byte aligned");
        }
        __tss_rsp0 = kstack;
}
