#pragma once

static inline u8 inb(int port)
{
        u8 value;
        asm volatile("in %0, %1\n" : "=a"(value) : "Nd"((u16)port) :);
        return value;
}

static inline u16 inw(int port)
{
        u16 value;
        asm volatile("in %0, %1\n" : "=a"(value) : "Nd"((u16)port) :);
        return value;
}

static inline u32 inl(int port)
{
        u32 value;
        asm volatile("in %0, %1\n" : "=a"(value) : "Nd"((u16)port) :);
        return value;
}

static inline void outb(int port, u8 value) { asm volatile("out %0, %1\n" : : "Nd"((u16)port), "a"(value) :); }

static inline void outw(int port, u16 value) { asm volatile("out %0, %1\n" : : "Nd"((u16)port), "a"(value) :); }

static inline void outl(int port, u32 value) { asm volatile("out %0, %1\n" : : "Nd"((u16)port), "a"(value) :); }
