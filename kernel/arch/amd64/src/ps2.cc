#include <aux/init.h>
#include <kernel/sched_prim.h>
#include <mach/machine/pio.h>
#include <mach/machine/ps2.h>
#include <mach/machine/spl.h>

#include "pic.h"

/*
 * ps2_device_input:
 *
 *	This function sends a byte to the PS2_PORT_READWRITE port, but
 *	first waits until the input/output data buffer is clear before
 *	sending the data.  Note that this byte can be either data or a
 *	keyboard/mouse command.
 *
 */
static void ps2_device_input(uint8_t a_input)
{
        while (ps2_ctrl_input_full())
        {
        }
        outb(PS2_PORT_READWRITE, a_input);
}

// ********************************************************************************************************************************
void ps2_ctrl_cmd(uint8_t a_cmd)
// ********************************************************************************************************************************
{
        while (inb(PS2_PORT_STATUS) & PS2_STATUS_INPUT_BUFFER_FULL)
        {
        }
        outb(PS2_PORT_COMMNAD, a_cmd);
}

// ********************************************************************************************************************************
int ps2_primary_output_full()
// ********************************************************************************************************************************
{
        uint8_t sts = inb(PS2_PORT_STATUS);
        return ((sts & PS2_STATUS_OUTPUT_BUFFER_FULL) != 0) && (sts & PS2_STATUS_AUXILARY_OUTPUT_BUFFER_FULL) == 0;
}

// ********************************************************************************************************************************
int ps2_aux_output_full()
// ********************************************************************************************************************************
{
        uint8_t sts = inb(PS2_PORT_STATUS);
        return ((sts & PS2_STATUS_OUTPUT_BUFFER_FULL) != 0) && (sts & PS2_STATUS_AUXILARY_OUTPUT_BUFFER_FULL) != 0;
}

// ********************************************************************************************************************************
int ps2_ctrl_input_full()
// ********************************************************************************************************************************
{
        return (inb(PS2_PORT_STATUS) & PS2_STATUS_INPUT_BUFFER_FULL) != 0;
}

// ********************************************************************************************************************************
void ps2_primary_cmd(uint8_t a_cmd)
// ********************************************************************************************************************************
{
        ps2_device_input(a_cmd);
        for (int spin = 1000; ps2_primary_output_full() == 0 && spin; spin--)
        {
        }
}

// ********************************************************************************************************************************
void ps2_aux_cmd(uint8_t a_cmd)
// ********************************************************************************************************************************
{
        ps2_ctrl_cmd(PS2_CONTROLLER_COMMAND_AUXILARY);
        ps2_device_input(a_cmd);
        for (int spin = 1000; ps2_aux_output_full() == 0 && spin; spin--)
        {
        }
}

// ********************************************************************************************************************************
uint8_t ps2_device_output()
// ********************************************************************************************************************************
{
        while ((inb(PS2_PORT_STATUS) & PS2_STATUS_OUTPUT_BUFFER_FULL) == 0)
        {
        }
        return inb(PS2_PORT_READWRITE);
}

// ********************************************************************************************************************************
void ps2_enable_primary()
// ********************************************************************************************************************************
{
        ps2_ctrl_cmd(PS2_CONTROLLER_COMMAND_DISABLE);
        while ((inb(PS2_PORT_STATUS) & PS2_STATUS_OUTPUT_BUFFER_FULL))
        {
                inb(PS2_PORT_READWRITE);
        }
        ps2_ctrl_cmd(PS2_CONTROLLER_COMMAND_READ);
        uint8_t config_byte = inb(PS2_PORT_READWRITE);
        config_byte |= (uint8_t)PS2_CONFIG_ENABLE_KBD_IRQ;
        config_byte &= (uint8_t)~PS2_CONFIG_SYSTEM_DISABLE_KBD;
        ps2_ctrl_cmd(PS2_CONTROLLER_COMMAND_WRITE);
        ps2_device_input(config_byte);
        ps2_ctrl_cmd(PS2_CONTROLLER_COMMAND_ENABLE);
}

// ********************************************************************************************************************************
void ps2_disable_primary()
// ********************************************************************************************************************************
{
        ps2_ctrl_cmd(PS2_CONTROLLER_COMMAND_DISABLE);
        while ((inb(PS2_PORT_STATUS) & PS2_STATUS_OUTPUT_BUFFER_FULL))
        {
                inb(PS2_PORT_READWRITE);
        }
        ps2_ctrl_cmd(PS2_CONTROLLER_COMMAND_READ);
        uint8_t config_byte = inb(PS2_PORT_READWRITE);
        config_byte &= (uint8_t)~PS2_CONFIG_ENABLE_KBD_IRQ;
        config_byte |= (uint8_t)PS2_CONFIG_SYSTEM_DISABLE_KBD;
        ps2_ctrl_cmd(PS2_CONTROLLER_COMMAND_WRITE);
        ps2_device_input(config_byte);
        ps2_ctrl_cmd(PS2_CONTROLLER_COMMAND_ENABLE);
}

// ********************************************************************************************************************************
void ps2_enable_auxilary()
// ********************************************************************************************************************************
{
        ps2_ctrl_cmd(PS2_CONTROLLER_COMMAND_DISABLE);
        while ((inb(PS2_PORT_STATUS) & PS2_STATUS_OUTPUT_BUFFER_FULL))
        {
                inb(PS2_PORT_READWRITE);
        }
        ps2_ctrl_cmd(PS2_CONTROLLER_COMMAND_READ);
        uint8_t config_byte = inb(PS2_PORT_READWRITE);
        config_byte |= (uint8_t)PS2_CONFIG_ENABLE_AUX_IRQ;
        config_byte &= (uint8_t)~PS2_CONFIG_SYSTEM_DISABLE_AUX;
        ps2_ctrl_cmd(PS2_CONTROLLER_COMMAND_WRITE);
        ps2_device_input(config_byte);
        ps2_ctrl_cmd(PS2_CONTROLLER_COMMAND_ENABLE);
}

// ********************************************************************************************************************************
void ps2_disable_auxilary()
// ********************************************************************************************************************************
{
        ps2_ctrl_cmd(PS2_CONTROLLER_COMMAND_DISABLE);
        while ((inb(PS2_PORT_STATUS) & PS2_STATUS_OUTPUT_BUFFER_FULL))
        {
                inb(PS2_PORT_READWRITE);
        }
        ps2_ctrl_cmd(PS2_CONTROLLER_COMMAND_READ);
        uint8_t config_byte = inb(PS2_PORT_READWRITE);
        config_byte &= (uint8_t)~PS2_CONFIG_ENABLE_AUX_IRQ;
        config_byte |= (uint8_t)PS2_CONFIG_SYSTEM_DISABLE_AUX;
        ps2_ctrl_cmd(PS2_CONTROLLER_COMMAND_WRITE);
        ps2_device_input(config_byte);
        ps2_ctrl_cmd(PS2_CONTROLLER_COMMAND_ENABLE);
}

// ********************************************************************************************************************************
static kern_return_t ps2_primary_irq()
// ********************************************************************************************************************************
{
        if (ps2_primary_output_full())
        {
                thread_wakeup_with_result((event_t)ps2_primary_output_full, THREAD_INTERRUPTED);
        }
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t ps2_aux_irq()
// ********************************************************************************************************************************
{
        if (ps2_aux_output_full())
        {
                thread_wakeup_with_result((event_t)ps2_aux_output_full, THREAD_INTERRUPTED);
        }
        return KERN_SUCCESS;
}

extern int (*ivect[])();
extern int intpri[];

// ********************************************************************************************************************************
static kern_return_t ps2_init_ctrl()
// ********************************************************************************************************************************
{
        /* Install handler for IRQ's (PS/2 devices) */
        ivect[1]  = ps2_primary_irq;
        intpri[1] = SPLTTY;

        ivect[12]  = ps2_aux_irq;
        intpri[12] = SPLTTY;

        form_pic_mask();

        /* Disable controller */
        ps2_ctrl_cmd(PS2_CONTROLLER_COMMAND_DISABLE);

        /* get rid of any garbage in output buffer */
        while ((inb(PS2_PORT_STATUS) & PS2_STATUS_OUTPUT_BUFFER_FULL))
        {
                inb(PS2_PORT_READWRITE);
        }

        /* ask for the ctlr command byte(config byte) */
        ps2_ctrl_cmd(PS2_CONTROLLER_COMMAND_READ);
        uint8_t config_byte = inb(PS2_PORT_READWRITE);

        /* Disable both primary and auxillary ports and their IRQ's, drivers can enable them later */
        config_byte &= (uint8_t) ~(PS2_CONFIG_ENABLE_KBD_IRQ | PS2_CONFIG_ENABLE_AUX_IRQ);
        config_byte |= (uint8_t)(PS2_CONFIG_SYSTEM_DISABLE_KBD | PS2_CONFIG_SYSTEM_DISABLE_AUX);

        /* write new ctlr command byte */
        ps2_ctrl_cmd(PS2_CONTROLLER_COMMAND_WRITE);
        ps2_device_input(config_byte);

        /* Enable controller */
        ps2_ctrl_cmd(PS2_CONTROLLER_COMMAND_ENABLE);

        return KERN_SUCCESS;
}

core_initcall(ps2_init_ctrl);