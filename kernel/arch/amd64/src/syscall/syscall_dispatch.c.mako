// clang-format off
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wincompatible-pointer-types"
#pragma GCC diagnostic ignored "-Wmissing-declarations"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wdiscarded-qualifiers"
#pragma GCC diagnostic ignored "-Wpointer-sign"

<%!
import json
from collections import namedtuple

def _json_object_hook(d):
    return namedtuple('X', d.keys())(*d.values())

def json2obj(data):
    #gitlab runner goes crazy if data is directly passed
    d = data
    return json.loads(d, object_hook=_json_object_hook)

class SyscallParam:
    def __init__(self, number, lnxtype, rstype, name, is_pointer, is_struct):
        self.number = number
        self.lnxtype = lnxtype
        self.rstype = rstype
        self.name = name
        self.is_pointer = is_pointer
        self.is_struct = is_struct
%>
<%
# syscall metadata
f = open(_in_folder + "/syscall_descriptors.json", "r")
syscalls = json2obj(f.read())[3]
f.close()

f = open(_out_folder + "/syscall_descriptors.json", "r")
rssyscallarrs = f.read().replace("}{", "}CUT{").split("CUT")
f.close()

# GCC has dumped all functions with SYSCALL(...) attibute, there might be duplicates
# Remove them here
tmpdict = {}
for rssyscallarr in rssyscallarrs:
    #print(rssyscallarr)
    for rssyscall in json2obj(rssyscallarr).syscalls:
        tmpdict[rssyscall.key] = rssyscall.value

rssyscalls = tmpdict

# Create header list removing duplicates
inc_headers = []
for rssyscall in rssyscalls.values():
  inc_headers.append(rssyscall.header)

inc_headers = mylist = list(set(inc_headers))

sorted(syscalls, key=lambda X: X.number)

arg_regs = ["rdi", "rsi", "rdx", "r10", "r8", "r9"]
%>

/* Forward declare linux structs */
<%
forward_decls = []
for syscall in syscalls:
  for parm in syscall.parameters:
    if parm.is_struct:
      forward_decls.append(parm.type.replace("*", "").replace("const", "").strip())
forward_decls = list(dict.fromkeys(forward_decls))
%>
% for fwd in forward_decls:
${fwd};
% endfor
#include <kernel/vm/vm_param.h>
#include <mach/machine/macro_help.h>
#include <kernel/thread_data.h>
#include <sys/types.h>

#include <signal.h>
#include <mqueue.h>

% for header in inc_headers:
#include "${header}"
% endfor

#define __s32 i32
#define __u32 u32
#define __old_kernel_stat stat
#define loff_t            off_t
#define ksigset_t sigset_t
typedef void (*psyscall_t)(pthread_t, struct amd64_saved_state *);

% for syscall in syscalls:
<%
# if linux does not implement a syscall skip it
if not syscall.implemented:
    continue
%>
static void ___sys_${syscall.name}(pthread_t pp, struct amd64_saved_state *ssp) {
% if syscall.name in rssyscalls:
<%
# Check if our system call takes a pthread_t parameter, if it does make sure the parameter list accounts for it.
# X(name='filename', type='const char *', attribute='__user', is_pointer=True, is_struct=False, is_posix_type=False, is_const=True)
# X(type='pthread_t', name='procp', fullname='proc', is_pointer=True)

proc_param = None
rsparms = []
for i in range(0, len(rssyscalls[syscall.name].parameters)):
    rsparm = rssyscalls[syscall.name].parameters[i]
    if rsparm.type == 'pthread_t':
        proc_param = SyscallParam(i, 'pthread_t', rsparm.type, 'pp', rsparm.is_pointer, len(rsparm.fullname)>0)
        continue
    rsparms.append(SyscallParam(i, 'LnxNone' ,rsparm.type, 'pp', rsparm.is_pointer, len(rsparm.fullname)>0))

args = []
for i in range(0, len(syscall.parameters)):
    lnxparm = syscall.parameters[i]
    args.append(SyscallParam(i, lnxparm.type, rsparms[i].rstype, lnxparm.name, lnxparm.is_pointer, lnxparm.is_struct))

if proc_param != None:
    args = args[:proc_param.number] + [proc_param] + args[proc_param.number:]
%>
  // ${syscall.parameters}
% for i in range(0, len(syscall.parameters)):
  ${syscall.parameters[i].type} ${syscall.parameters[i].name} = (${syscall.parameters[i].type})((${syscall.parameters[i].type})ssp->${arg_regs[i]});
% endfor

  //if (proc_get_pid(pp) > 1)
  //log_trace("PID %d Enter: ${syscall.name}", proc_get_pid(pp));
  ssp->rax = (natural_t)${rssyscalls[syscall.name].name}(${', '.join([o.name for o in args])});
  //if (proc_get_pid(pp) > 1)
  //log_trace("PID %d Leave: ${syscall.name} -> %08X", proc_get_pid(pp), ssp->rax);

% else:
  #if 0
  ${syscall}
  #endif

  log_error("PID %d: ${syscall.name} not implemented", 0, ssp);
  ssp->rax = (natural_t)KERN_FAIL(ENOSYS);
% endif
}
% endfor

static void ___sys_nosys(pthread_t pp, struct amd64_saved_state *ssp) {
  ssp->rax = (natural_t)KERN_FAIL(ENOSYS);
}

/* */
static psyscall_t syscallsw[] = {
% for syscall in syscalls:
% if syscall.implemented:
  ___sys_${syscall.name},
% else:
  ___sys_nosys,
% endif
% endfor
};

/* We return the saved state to the asm code for convinience */
void syscall_dispatch(struct amd64_saved_state *ssp, thread_t a_thread) {
  if (!(ssp->rax < ${len(syscalls)})) {
      ssp->rax = (natural_t)(KERN_FAIL(ENOSYS));
  } else {
      syscallsw[ssp->rax](a_thread->pthread, ssp);
  }
}

#pragma GCC diagnostic pop