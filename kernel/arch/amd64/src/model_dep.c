/*
 * Mach Operating System
 * Copyright (c) 1991,1990,1989, 1988 Carnegie Mellon University
 * All Rights Reserved.
 *
 * Permission to use, copy, modify and distribute this software and its
 * documentation is hereby granted, provided that both the copyright
 * notice and this permission notice appear in all copies of the
 * software, derivative works or modified versions, and any portions
 * thereof, and that both notices appear in supporting documentation.
 *
 * CARNEGIE MELLON ALLOWS FREE USE OF THIS SOFTWARE IN ITS "AS IS"
 * CONDITION.  CARNEGIE MELLON DISCLAIMS ANY LIABILITY OF ANY KIND FOR
 * ANY DAMAGES WHATSOEVER RESULTING FROM THE USE OF THIS SOFTWARE.
 *
 * Carnegie Mellon requests users of this software to return to
 *
 *  Software Distribution Coordinator  or  Software.Distribution@CS.CMU.EDU
 *  School of Computer Science
 *  Carnegie Mellon University
 *  Pittsburgh PA 15213-3890
 *
 * any improvements or extensions that they make and grant Carnegie Mellon
 * the rights to redistribute these changes.
 */
/*
 *	File:	model_dep.c
 *	Author:	Avadis Tevanian, Jr., Michael Wayne Young
 *
 *	Copyright (C) 1986, Avadis Tevanian, Jr., Michael Wayne Young
 *
 *	Basic initialization for I386 - ISA bus machines.
 */
#include <aux/init.h>
#include <cpuid.h>
#include <kernel/machine.h>
#include <kernel/time_value.h>
#include <kernel/vm/vm_param.h>
#include <kernel/vm/vm_prot.h>
#include <kernel/time_out.h>
#include <kernel/env.h>
#include <mach/machine/fpu.h>
#include <mach/machine/model_dep.h>
#include <mach/machine/multiboot2.h>
#include <mach/machine/pmap.h>
#include <mach/machine/spl.h>
#include <mach/machine/vm_param.h>
#include <machine/vm/vm_param.h>
#include <mach/machine/pio.h>
#include <string.h>

#include "../cpu/idt.h"
#include "eflags.h"
#include "gdt.h"
#include "msr.h"
#include "proc_reg.h"
#include "rtc.h"
#include "pit.h"

extern void syslog_init();
extern void init_fpu();
extern void picinit();
extern void gdt_init();
extern void idt_init();
extern void ktss_init();
extern void exceptions_init();

struct multiboot_tag_framebuffer g_mbfb;

static struct modeldep_bootdata_private
{
        struct modeldep_bootdata public_data;
        /* Memory map passed to us by the bootloader */
        struct multiboot_mmap_entry const *mb2_mem_map;
        /* Entries in the memory map */
        size_t const mb2_mem_map_count;

        /* The current mmap entry used for stealing pages from */
        size_t cur_mb2_mem_map_index;
        size_t cur_mb2_mem_map_offset;

        /* Virtual address of physical memory, for the kvtophys/phystokv macros.  */
        vm_address_t phys_mem_va;

        integer_t early_boot_page_idx;
        pt_entry_t *early_boot_pt;

} s_bootdata;

static void mb2_iter_pages(int mb2_type, modeldep_iter_pages_cb cb, void *user_data)
{
        for (size_t i = 0; i < s_bootdata.mb2_mem_map_count; ++i)
        {
                if (s_bootdata.mb2_mem_map[i].type == mb2_type)
                {
                        vm_offset_t region_start = round_page(s_bootdata.mb2_mem_map[i].addr);
                        vm_offset_t region_end   = trunc_page(region_start + s_bootdata.mb2_mem_map[i].len);
                        for (vm_offset_t cur_pg = region_start; cur_pg < region_end; cur_pg += PGBYTES)
                        {
                                if (!cb(cur_pg, region_start, region_end, user_data))
                                {
                                        return;
                                }
                        }
                }
        }
}

void modeldep_iter_all_pages(modeldep_iter_pages_cb cb, void *user_data)
{
        mb2_iter_pages(MULTIBOOT_MEMORY_AVAILABLE, cb, user_data);
        mb2_iter_pages(MULTIBOOT_MEMORY_RESERVED, cb, user_data);
        mb2_iter_pages(MULTIBOOT_MEMORY_ACPI_RECLAIMABLE, cb, user_data);
        mb2_iter_pages(MULTIBOOT_MEMORY_NVS, cb, user_data);
}

void modeldep_iter_avail_pages(modeldep_iter_pages_cb cb, void *user_data)
{
        mb2_iter_pages(MULTIBOOT_MEMORY_AVAILABLE, cb, user_data);
}

extern time_value_t g_time;

extern int (*ivect[])();
extern int iunit[];
extern int intpri[];


/* Calculate a kernel virtual address from a physical address.  */
#define phystokv(pa) ((vm_offset_t)(pa) + s_bootdata.phys_mem_va)

/* Same, but in reverse.
   This only works for the region of kernel virtual addresses
   that directly map physical addresses.  */
#define kvtophys(va) ((vm_offset_t)(va)-phys_mem_va)

/* This variable contains the kernel virtual address
   corresponding to linear address 0.
   In the absence of paging,
   linear addresses are always the same as physical addresses.  */
#ifndef linear_base_va
#define linear_base_va phys_mem_va
#endif
#undef lintokv
#undef kvtolin
/* Convert between linear and kernel virtual addresses.  */
#define lintokv(la) ((vm_offset_t)(la) + linear_base_va)
#define kvtolin(va) ((vm_offset_t)(va)-linear_base_va)

/* Location of the kernel's symbol table.
   Both of these are 0 if none is available.  */
#if MACH_KDB
static vm_offset_t kern_sym_start, kern_sym_end;
#else
#define kern_sym_start 0
#define kern_sym_end   0
#endif

/* Command line supplied to kernel.  */
char *kernel_cmdline = "";

/* This is used for memory initialization:
   it gets bumped up through physical memory
   that exists and is not occupied by boot gunk.
   It is not necessarily page-aligned.  */
static vm_offset_t avail_next = 0x1000; /* XX end of BIOS data area */

extern void setup_main();

void inittodr(); /* forward */

/* XX interrupt stack pointer and highwater mark, for locore.S.  */
vm_offset_t int_stack_top, int_stack_high;

void thread_bootstrap_return(void) { panic("%s called\n", __PRETTY_FUNCTION__); }

int cpu_number() { return 0; }

int cnputc()
{
        panic("%s called\n", __PRETTY_FUNCTION__);
        return 0;
}

int switch_to_shutdown_context()
{
        panic("%s called\n", __PRETTY_FUNCTION__);
        return 0;
}

int exception()
{
        panic("%s called\n", __PRETTY_FUNCTION__);
        return 0;
}

/*
 * Find devices.  The system is alive.
 */
#include <mach/machine/lock.h>
void machine_init()
{
        /*
         * Initialize the console.
         */
        syslog_init(); // cninit();

        /*
         * Set up to use floating point.
         */
        init_fpu();

        /*
         * Find the devices
         */
        // probeio();

        /*
         * Get the time
         */
        inittodr();

        /*
         * Tell the BIOS not to clear and test memory.
         */
        //*(unsigned short *)phystokv(0x472) = 0x1234;

        /*
         * Unmap page 0 to trap NULL references.
         */
        // pmap_unmap_page_zero();
}

/*
 * Halt the system or reboot.
 */
void halt_all_cpus(boolean_t reboot)
{
        if (reboot)
        {
                //  kdreboot();
        }
        else
        {
                // rebootflag = 1;
                log_info("In tight loop: hit ctl-alt-del to reboot");
                spl0();
        }
        for (;;)
                continue;
}

void exit(int rc) { halt_all_cpus(0); }

void db_reset_cpu() { halt_all_cpus(1); }

/*
 * Compute physical memory size and other parameters.
 */
static boolean_t mem_size_init(vm_address_t region_addr, vm_address_t region_start, vm_address_t region_end, void *user_data)
{
        struct modeldep_bootdata *ctx = (struct modeldep_bootdata *)user_data;

        if (ctx->phys_last_addr < region_end)
        {
                ctx->phys_last_addr = region_end;
        }

        if (ctx->phys_first_addr > region_start)
        {
                ctx->phys_first_addr = region_start;
        }
        ctx->phys_page_count++;

        return TRUE;
}

extern void do_initcalls(int lvl);

/*
 * Basic PC VM initialization.
 * Turns on paging and changes the kernel segments to use high linear
 * addresses.
 */
void modeldep_init()
{
        int_stack_high = s_bootdata.public_data.kernel_image_va_addr_start;
        int_stack_top  = int_stack_high - PGBYTES;

        /*
         * Initialize the PIC prior to any possible call to an spl.
         */
        picinit();

        do_initcalls(INITCALL_CORE);

        /*
         * Find memory size parameters.
         */
        s_bootdata.public_data.phys_first_addr = UINTPTR_MAX;
        s_bootdata.public_data.phys_last_addr  = 0;

        modeldep_iter_avail_pages(mem_size_init, &s_bootdata.public_data);

        /*
         DH:
           Physical memory wo;; start after the kernel image.
           We will free usable pages below the kernel image
           at a later stage.
       */

        for (size_t i = 0; i < s_bootdata.mb2_mem_map_count; ++i)
        {
                if (s_bootdata.mb2_mem_map[i].type == MULTIBOOT_MEMORY_AVAILABLE)
                {
                        vm_offset_t region_start = s_bootdata.mb2_mem_map[i].addr;
                        vm_offset_t region_end   = region_start + s_bootdata.mb2_mem_map[i].len;
                        vm_offset_t kern_img_end = s_bootdata.public_data.kernel_image_phys_addr_end;

                        /*
                          DH:
                            Initialize current multiboot2 mmap entry used for page stealing
                        */
                        if (kern_img_end >= region_start && kern_img_end < region_end)
                        {
                                s_bootdata.cur_mb2_mem_map_index  = i;
                                s_bootdata.cur_mb2_mem_map_offset = kern_img_end - region_start;
                        }
                }
        }

        /*
         *	Initialize kernel physical map, mapping the
         *	region from loadpt to avail_start.
         *	Kernel virtual address starts at VM_KERNEL_MIN_ADDRESS.
         *	XXX make the BIOS page (page 0) read-only.
         */
        pmap_bootstrap(&s_bootdata.public_data);

        /* Set phys_mem_va to point to the beging of 1:1 mapping of physical
         * memory */
        s_bootdata.phys_mem_va = pml4enum2lin(0x100);

        /* Update the pointer to multiboot2 memory map entries to use the new
         * mapping
         */
        s_bootdata.mb2_mem_map = (const struct multiboot_mmap_entry *)phystokv(((vm_offset_t)s_bootdata.mb2_mem_map) + 0x80000000);

        /* DH: Paging and long mode is enabled by the bootloader, a new mapping
         * table has been built and activated in pmap_bootstrap */
}

boolean_t is_1gb_aligned(vm_offset_t address) { return ((address & (0x40000000 - 1)) == 0); }

boolean_t is_2mb_aligned(vm_offset_t address) { return ((address & (0x200000 - 1)) == 0); }

boolean_t cpu_feature_page1gb()
{
        u32 eax, ebx, ecx, edx = 0;
        __get_cpuid(0x80000001, &eax, &ebx, &ecx, &edx);
        return (edx & (1 << 26)) != 0;
}

vm_offset_t early_boot_map_page(vm_offset_t phys_page_addr)
{
        vm_offset_t tmp_page_addr = s_bootdata.phys_mem_va + (PAGE_SIZE * (s_bootdata.early_boot_page_idx++ % 4));

        pt_entry_t *tmp_page_pte = &s_bootdata.early_boot_pt[ptenum(tmp_page_addr)];

        *tmp_page_pte = (((*tmp_page_pte) & ~INTEL_PTE_PFN) | phys_page_addr);

        asm volatile("invlpg [%0]\n" : : "r"(tmp_page_addr) : "memory");

        return tmp_page_addr;
}

/*
 *	C boot entrypoint - called by boot_entry in boothdr.S.
 *	Running in 32-bit flat mode, but without paging yet.
 */
extern vm_offset_t __executable_start;

extern void crt_bootstrap();

extern void do_earlyinitcalls(void);

static void *PER_CPU_VAR(percpu_selfref);

USED void c_boot_entry(struct multiboot_mmap_entry *mb2mmap,
                       size_t mb2_mmap_count,
                       struct multiboot_tag_framebuffer *a_mbfb,
                       char *a_symstart,
                       char *a_symend)
{
        extern char *g_symstart;
        extern char *g_symend;

        g_symstart = a_symstart;
        g_symend   = a_symend;
        
        // Load the per cpu section before init calls
        // TODO: Make copies of the section(s) for each cpu before using this one
        __percpu_percpu_selfref_data_boot = &__percpu_percpu_selfref_data_boot;
        wrmsr(MSR_KERNEL_GS_BASE, (msrval_t)(__percpu_percpu_selfref_data_boot));
        asm volatile ("swapgs");

        // Copy the framebuffer information, and rebase phys addr to 1:1 mapping
        g_mbfb = *a_mbfb;
        do_earlyinitcalls();

        // Make a local copy of the mmap entries
        {
                size_t mb2_size                             = sizeof(struct multiboot_mmap_entry) * mb2_mmap_count;
                struct multiboot_mmap_entry *local_mb2_mmap = __builtin_alloca(mb2_size);
                memcpy(local_mb2_mmap, mb2mmap, mb2_size);
                mb2mmap = local_mb2_mmap;
        }

        // Finish setting up the CPU
        set_cr4(get_cr4() | CR4_OSFXSR);
        // Introduced in ivybridge(Too new) see https://lwn.net/Articles/769355/
        // set_cr4(get_cr4() | CR4_FSGSBASE);
        set_cr0(get_cr0() & ~CR0_EM);

        set_cr0(get_cr0() | CR0_WP);

        // Enable syscall sysret
        wrmsr(MSR_EFER, rdmsr(MSR_EFER) | MSR_EFER_SCE);

        // AMD manual. p.155 Figure 6-1
        wrmsr(MSR_STAR, __msr_star);

        // Set syscall entry point
        wrmsr(MSR_LSTAR, __msr_lstar);

        // Set syscall flags mask (we enter with interrupts disabled)
        wrmsr(MSR_SF_MASK, EFL_USER_CLEAR);

        /* Stash the boot_image_info pointer.  */
        s_bootdata.mb2_mem_map                     = mb2mmap;
        *((size_t *)&s_bootdata.mb2_mem_map_count) = mb2_mmap_count;

        s_bootdata.public_data.kernel_image_phys_addr_start = ((vm_offset_t)&__executable_start) + 0x80000000;

        s_bootdata.public_data.kernel_image_phys_addr_end = ((vm_offset_t) /*&_end*/ round_page(a_symend)) + 0x80000000;

        s_bootdata.public_data.kernel_image_va_addr_start = ((vm_offset_t)&__executable_start);

        s_bootdata.public_data.kernel_image_va_addr_end = ((vm_offset_t) /*&_end*/ round_page(a_symend));

#ifdef ENABLE_MEM_POISON
        s_bootdata.public_data.early_poison = 1;
#endif
        /*
        DH:
          PA 0 - kernel_image_end is 1:1 mapped starting at VA 0xFFFFFFFF80000000
          by the bootloader
        */
        s_bootdata.phys_mem_va = 0xFFFFFFFF80000000;

        /* Get PML4 table */
        s_bootdata.early_boot_pt = (pt_entry_t *)phystokv(get_cr3());

        /* Get PDP table for phys_mem_va */
        s_bootdata.early_boot_pt
            = (pt_entry_t *)phystokv(s_bootdata.early_boot_pt[lin2pml4enum(s_bootdata.phys_mem_va)] & INTEL_PML4E_PFN);

        /* Get PD table for phys_mem_va */
        s_bootdata.early_boot_pt
            = (pt_entry_t *)phystokv(s_bootdata.early_boot_pt[lin2pdpenum(s_bootdata.phys_mem_va)] & INTEL_PDPE_PFN);

        /* Get page table for phys_mem_va */
        s_bootdata.early_boot_pt
            = (pt_entry_t *)phystokv(s_bootdata.early_boot_pt[lin2pdenum(s_bootdata.phys_mem_va)] & INTEL_PDE_PFN);

        /*
        DH:
          Initialize data for early_boot_map_page
        */
        s_bootdata.early_boot_page_idx = 0;

#if DAMIR
        /* Find the kernel command line, if there is one.  */
        if (boot_info->flags & MULTIBOOT_CMDLINE)
                kernel_cmdline = (char *)phystokv(boot_info->cmdline);
#endif
        /*
         * Do basic VM initialization
         */
        modeldep_init();

        machine_slot[0].is_cpu      = TRUE;
        machine_slot[0].running     = TRUE;
        machine_slot[0].cpu_type    = CPU_TYPE_I386;
        machine_slot[0].cpu_subtype = CPU_SUBTYPE_AT386;

        /*
         * Start the system.
         */

        setup_main(&s_bootdata.public_data);
}

#include <kernel/time_value.h>
#include <kernel/vm/pmap.h>
#include <kernel/vm/vm_prot.h>

vm_offset_t timemmap(dev_t dev, vm_offset_t off, vm_prot_t prot)
{
        extern mapped_time_value_t *mtime;

        if (prot & VM_PROT_WRITE)
                return (-1);

        return (amd64_btop(pmap_extract(pmap_kernel(), (vm_offset_t)mtime->microseconds)));
}

void startrtclock() 
{
        // Init the PIT
        intpri[0] = SPLHI;
        form_pic_mask();

        uint16_t divisor = (uint16_t)(CLKNUM / hz);

        outb(PITCTL_PORT, PIT_C0 | PIT_SQUAREMODE | PIT_READMODE);
        outb(PITCTR0_PORT, (uint8_t)((divisor >> 0) & 0xFF));
        outb(PITCTR0_PORT, (uint8_t)((divisor >> 8) & 0xFF));
}

void inittodr()
{
        time_value_t new_time;

        new_time.seconds      = 0;
        new_time.microseconds = 0;

        readtodc(&new_time.seconds);

        {
                spl_t s = splhigh();
                g_time  = new_time;
                splx(s);
        }
}

void resettodr() { writetodc(); }

/* Always returns page-aligned regions.  */
boolean_t init_alloc_aligned(vm_size_t size, vm_offset_t *addrp)
{
        struct multiboot_mmap_entry const *mmape = &s_bootdata.mb2_mem_map[s_bootdata.cur_mb2_mem_map_index];
        size                                     = round_page(size);

        if ((s_bootdata.cur_mb2_mem_map_offset + size) < mmape->len)
        {
                *addrp = mmape->addr + s_bootdata.cur_mb2_mem_map_offset;

                /* Skip first physical page */
                if (*addrp == 0)
                {
                        s_bootdata.cur_mb2_mem_map_offset += size;
                        return init_alloc_aligned(size, addrp);
                }

                /* if the found adress is in the range it means we wrapped, bail out */
                /* Read the comment in pmap_bootstrap to know why (PAGE_SIZE*3)) */
                if (*addrp >= (s_bootdata.public_data.kernel_image_phys_addr_start - (PAGE_SIZE * 3))
                    && *addrp < s_bootdata.public_data.kernel_image_phys_addr_end)
                {
                        return FALSE;
                }
                s_bootdata.cur_mb2_mem_map_offset += size;
#ifdef ENABLE_MEM_POISON
                if (s_bootdata.public_data.early_poison)
                {
                        vm_address_t tmp_page_addr = early_boot_map_page(*addrp);
                        // Poison
                        memset((void *)tmp_page_addr, 0xEB, PAGE_SIZE);
                }
#endif
                return TRUE;
        }

        s_bootdata.cur_mb2_mem_map_index++;

        /* Find next available entry*/
        for (size_t i = s_bootdata.cur_mb2_mem_map_index; i < s_bootdata.mb2_mem_map_count; ++i)
        {
                if (s_bootdata.mb2_mem_map[i].type == MULTIBOOT_MEMORY_AVAILABLE)
                {
                        s_bootdata.cur_mb2_mem_map_index  = i;
                        s_bootdata.cur_mb2_mem_map_offset = 0;
                        return init_alloc_aligned(size, addrp);
                }
        }

        /* Still nothing wrap around and map before kernel image*/
        for (size_t i = 0; i < s_bootdata.mb2_mem_map_count; ++i)
        {
                if (s_bootdata.mb2_mem_map[i].type == MULTIBOOT_MEMORY_AVAILABLE)
                {
                        s_bootdata.cur_mb2_mem_map_index  = i;
                        s_bootdata.cur_mb2_mem_map_offset = 0;
                        return init_alloc_aligned(size, addrp);
                }
        }

        return FALSE;
}

boolean_t pmap_next_page(vm_offset_t *addrp) { return init_alloc_aligned(PAGE_SIZE, addrp); }

/* Grab a physical page:
   the standard memory allocation mechanism
   during system initialization.  */
vm_offset_t pmap_grab_page()
{
        vm_offset_t addr;
        if (!pmap_next_page(&addr))
        {
                panic("Not enough memory to initialize Mach");
        }
        return addr;
}

// Entry point from the bootloader
NAKED NORETURN void _start(void)
{
        // Just in case
        asm volatile("cli");

        // The bootloader has passed us the following structure

        /*
        struct system_memmap {
            u32 count;
            u32 descriptor_ptr; // pointer to Multiboot MEMRORY_INFO
        };
        */
        // We need to convert descriptor_ptr to a kernel space address and pass it on
        // in rdi along with count in rsi

        // Get pointer
        asm volatile("xor rax, rax");
        asm volatile("mov eax, [rdi + 4]");
        // Translate to kernel vm space
        asm volatile("or rax, %0" : : "i"(VM_BIOSMEM_MIN_ADDRESS) :);

        // Translate multiboot info struct pointer to kernel vm address
        asm volatile("or rdx, %0" : : "i"(VM_BIOSMEM_MIN_ADDRESS) :);

        // Get the count
        asm volatile("xor rsi, rsi");
        asm volatile("mov esi, [rdi]");

        // Put the descriptor pointer back in rdi
        asm volatile("mov rdi, rax");

        asm volatile("push rdi");
        asm volatile("push rsi");
        asm volatile("push rdx");
        asm volatile("call crt_bootstrap");
        asm volatile("pop rdx");
        asm volatile("pop rsi");
        asm volatile("pop rdi");

        // symbol start and end are on the stack
        asm volatile("pop rcx"); // symstart
        asm volatile("or rcx, %0" : : "i"(VM_BIOSMEM_MIN_ADDRESS) :);

        asm volatile("pop r8"); // symend
        asm volatile("or r8, %0" : : "i"(VM_BIOSMEM_MIN_ADDRESS) :);

        // Laod the GDT
        asm volatile("lgdt __gdt_descriptor");
        // Load the IDT
        asm volatile("lidt __idt_descriptor");
        // Set Tss segment
        asm volatile("ltr %0" : : "A"((u16)GDT_KERNEL_TSS) :);

        // Make a iret to C code
        /* Reload all the segment registers from the new GDT. */
        asm volatile("push %0" ::"i"(GDT_KERNEL_DS_SELECTOR));
        asm volatile("lea rax, __executable_start \n\t push rax");
        asm volatile("pushfq");
        asm volatile("push %0" ::"i"(GDT_KERNEL_CS_SELECTOR));
        asm volatile("lea rax, c_boot_entry\n\t push rax");
        asm volatile("iretq");

        UNREACHABLE();
}