#include <mach/tmpsys.h>

#include <cpuid.h>
#include <emerixx/process.h>

#include <kernel/ast.h>
#include <kernel/cpu_number.h>
#include <mach/machine/fpu.h>
#include <mach/machine/locore.h>
#include <mach/machine/macro_help.h>
#include <mach/machine/spl.h>
#include <mach/machine/thread.h>

#include <kernel/vm/pmap.h>
#include <kernel/vm/vm_fault.h>
#include <kernel/vm/vm_map.h>
#include <kernel/vm/vm_param.h>
#include <kernel/thread_data.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>

#include "gdt.h"
#include "msr.h"
#include "pic.h"
#include "pio.h"
#include "proc_reg.h"
#include "trapno.h"

static int cpu_flags;

#define LOAD_KERNEL_STATE_REG(kernel_state_ptr, reg)                                                                               \
        asm volatile("mov " #reg ",[" #kernel_state_ptr "+ %0]" : : "i"(offsetof(struct amd64_kernel_state, k_##reg)) :)

void __fatal_panic(char const *function, int line, char const *fmt, ...)
{
        char out_buf[0x80];
        va_list args;
        va_start(args, fmt);
        out_buf[vsnprintf(out_buf, sizeof(out_buf), fmt, args)] = 0;
        va_end(args);

        asm volatile("mov rdi, %0" : : "r"(function) :);
        asm volatile("mov esi, %0" : : "r"(line) :);
        asm volatile("mov rdx, %0" : : "r"(out_buf) :);

        asm volatile("int 3");
        UNREACHABLE();
}

/*
 * Halt a cpu.
 */

void locore_cpu_halt(void)
{

        sys_sync(current_thread()->pthread);
        asm volatile("cli");
        while (1)
        {

                asm volatile("hlt");
        }
}

void locore_cpu_idle() { asm volatile("hlt"); }

#define false 0

kern_return_t copyout(const void *kaddr, void *uaddr, size_t len)
{
        thread_t thread = current_thread();
        vm_map_t map    = thread->task->map;
        pmap_t pmap     = vm_map_pmap(map);

        kern_return_t res = KERN_SUCCESS;

        char *ucur = (char *)uaddr;

        if (ucur >= (char *)VM_USER_STACK_ADDRESS)
        {
                return KERN_FAIL(EFAULT);
        }

        char *uend = ucur + len;
        if (uend >= (char *)VM_USER_STACK_ADDRESS)
        {
                return KERN_FAIL(EFAULT);
        }

        char *kcur = (char *)kaddr;

        vm_address_t curpg;

        vm_map_lock(map);
        while (ucur < uend)
        {
                curpg = trunc_page(ucur);

                pt_entry_t *pte = pmap_pte(pmap, curpg, NULL);

                /* Check if the pte for cur_upage is mapped */
                if ((pte != NULL) && (*pte & INTEL_PTE_VALID) && (*pte & INTEL_PTE_WRITE))
                {
                        len = (size_t)(round_page(ucur + 1) - (vm_offset_t)ucur);
                        len = len <= (size_t)(uend - ucur) ? len : (size_t)(uend - ucur);

                        if (cpu_flags & 1)
                        {
                                int nosmap = get_cr4();
                                nosmap &= ~CR4_SMAP;
                                set_cr4(nosmap);
                                memcpy(ucur, kcur, len);
                                set_cr4(nosmap | CR4_SMAP);
                        }
                        else
                        {
                                memcpy(ucur, kcur, len);
                        }
                        ucur += len;
                        kcur += len;
                }
                else
                {

                        /* Try to fault in the page */
                        vm_map_unlock(map);
                        res = vm_fault(map, curpg, VM_PROT_READ | VM_PROT_WRITE, FALSE);
                        vm_map_lock(map);

                        if (res != KERN_SUCCESS)
                        {
                                res = KERN_FAIL(EFAULT);
                                break;
                        }
                }
        }
        vm_map_unlock(map);
        return res;
}

kern_return_t copyin(const void *uaddr, void *kaddr, size_t len)
{
        thread_t thread = current_thread();
        vm_map_t map    = thread->task->map;
        pmap_t pmap     = vm_map_pmap(map);

        kern_return_t res = KERN_SUCCESS;

        char *ucur = (char *)uaddr;

        if (ucur >= (char *)VM_USER_STACK_ADDRESS)
        {
                return KERN_FAIL(EFAULT);
        }

        char *uend = ucur + len;
        if (uend >= (char *)VM_USER_STACK_ADDRESS)
        {
                return KERN_FAIL(EFAULT);
        }

        char *kcur = (char *)kaddr;

        vm_address_t curpg;

        vm_map_lock(map);
        while (ucur < uend)
        {

                curpg = trunc_page(ucur);

                pt_entry_t *pte = pmap_pte(pmap, curpg, NULL);

                /* Check if the pte for cur_upage is mapped */
                if ((pte != NULL) && (*pte & INTEL_PTE_VALID))
                {
                        len = (size_t)(round_page(ucur + 1) - (vm_offset_t)ucur);
                        len = len <= (size_t)(uend - ucur) ? len : (size_t)(uend - ucur);

                        /* all good, do the copy. */
                        if (cpu_flags & 1)
                        {
                                int nosmap = get_cr4();
                                nosmap &= ~CR4_SMAP;
                                set_cr4(nosmap);
                                memcpy(kcur, ucur, len);
                                set_cr4(nosmap | CR4_SMAP);
                        }
                        else
                        {
                                memcpy(kcur, ucur, len);
                        }
                        ucur += len;
                        kcur += len;
                }
                else
                {

                        vm_map_unlock(map);
                        res = vm_fault(map, curpg, VM_PROT_READ, FALSE);
                        vm_map_lock(map);

                        if (res != KERN_SUCCESS)
                        {
                                res = KERN_FAIL(EFAULT);
                                break;
                        }
                }
        }
        vm_map_unlock(map);

        /* The process must not get swapped out during this operation */
        return res;
}

char *stpncpyin(char *a_kdest, char const *a_usrc, size_t a_len, kern_return_t *a_kret)
{
        thread_t thread = current_thread();
        vm_map_t map    = thread->task->map;
        pmap_t pmap     = vm_map_pmap(map);

        char *retval = a_kdest;
        char *e      = a_kdest + a_len;
        while (retval < e)
        {
                vm_map_lock(map);
                pt_entry_t *pte = pmap_pte(pmap, trunc_page(a_usrc), NULL);

                /* Check if the pte for cur_upage is mapped */
                if ((pte != NULL) && (*pte & INTEL_PTE_VALID))
                {
                        size_t un = (size_t)(round_page(a_usrc + 1) - (vm_offset_t)a_usrc);
                        size_t kn = (size_t)(e - retval);
                        size_t cn = kn < un ? kn : un;
                        char *tc;
                        /* all good, do the copy. */
                        if (cpu_flags & 1)
                        {
                                int nosmap = get_cr4() & ~CR4_SMAP;
                                set_cr4(nosmap);
                                tc = stpncpy(retval, a_usrc, cn);
                                set_cr4(nosmap | CR4_SMAP);
                        }
                        else
                        {
                                tc = stpncpy(retval, a_usrc, cn);
                        }

                        vm_map_unlock(map);

                        if (tc < (retval + cn))
                        {
                                *a_kret = KERN_SUCCESS;
                                return tc;
                        }

                        a_usrc += cn;
                        retval = tc;
                }
                else
                {

                        /* Try to fault in the page */
                        vm_map_unlock(map);

                        kern_return_t res = vm_fault(map, trunc_page(a_usrc), VM_PROT_READ, FALSE);

                        if (res != KERN_SUCCESS)
                        {
                                *a_kret = KERN_FAIL(EFAULT);
                                return retval;
                        }
                }
        }

        *a_kret = KERN_FAIL(ENAMETOOLONG);

        return retval;
}

char **ptpncpyin(char *a_kdest[], char const *const a_usrc[], size_t a_count, kern_return_t *a_kret)
{
        if (a_count == 0)
        {
                *a_kret = KERN_FAIL(ENAMETOOLONG);
                return a_kdest;
        }

        thread_t thread = current_thread();
        vm_map_t map    = thread->task->map;
        pmap_t pmap     = vm_map_pmap(map);

        char const **retval = a_kdest;
        char const **e      = &a_kdest[a_count];

        while (retval < e)
        {
                vm_map_lock(map);
                pt_entry_t *pte = pmap_pte(pmap, trunc_page(a_usrc), NULL);

                /* Check if the pte for cur_upage is mapped */
                if ((pte != NULL) && (*pte & INTEL_PTE_VALID))
                {
                        size_t un       = (size_t)(((char const *const *)round_page(a_usrc + 1)) - a_usrc);
                        size_t kn       = (size_t)(e - retval);
                        size_t cn       = kn < un ? kn : un;
                        char const **tc = retval;
                        /* all good, do the copy. */
                        if (cpu_flags & 1)
                        {
                                int nosmap = get_cr4() & ~CR4_SMAP;
                                set_cr4(nosmap);

                                for (size_t i = 0; i < cn; ++i)
                                {
                                        if ((*tc = a_usrc[i]) == 0)
                                        {
                                                break;
                                        }
                                        tc++;
                                }

                                set_cr4(nosmap | CR4_SMAP);
                        }
                        else
                        {
                                for (size_t i = 0; i < cn; ++i)
                                {
                                        if ((*tc = a_usrc[i]) == 0)
                                        {
                                                break;
                                        }
                                        tc++;
                                }
                        }

                        vm_map_unlock(map);

                        if (tc < (retval + cn))
                        {
                                *a_kret = KERN_SUCCESS;
                                return tc;
                        }

                        a_usrc += cn;
                        retval = tc;
                }
                else
                {

                        /* Try to fault in the page */
                        vm_map_unlock(map);

                        kern_return_t res = vm_fault(map, trunc_page(a_usrc), VM_PROT_READ, FALSE);

                        if (res != KERN_SUCCESS)
                        {
                                *a_kret = KERN_FAIL(EFAULT);
                                return retval;
                        }
                }
        }

        *a_kret = KERN_FAIL(ENAMETOOLONG);

        return retval;
}

NAKED
void locore_call_continuation(continuation_t cont)
{
        asm volatile("or rsp, %0" /* rewind stack */
                     :
                     : "i"(KERNEL_STACK_SIZE - 1)
                     :);

        asm volatile("sub rsp, %0"
                     : /* Create room for the usual structures, -1 because of or
                          above(0x...fffff) */
                     : "i"(-1 + (sizeof(struct amd64_kernel_state) + sizeof(struct amd64_exception_link)))
                     :);

        asm volatile("xor rbp, rbp"); /* zero frame pointer */

        asm volatile("jmp %0" : : "r"(cont) :); /* goto continuation */

        UNREACHABLE();
}

extern vm_offset_t kernel_stack[/*NCPUS*/];
void locore_load_context(thread_t thread)
{
        int mycpu                        = cpu_number();
        vm_offset_t kernel_stack_address = thread->kernel_stack;
        /* Right after exception link */
        struct amd64_kernel_state *kernel_state = STACK_IKS(kernel_stack_address);

        /* store stack address */
        active_stacks[mycpu] = kernel_stack_address;

        /* store stack top */
        kernel_stack[mycpu] = kernel_stack_address
                              + (KERNEL_STACK_SIZE - sizeof(struct amd64_kernel_state) - (sizeof(struct amd64_exception_link)));

        asm volatile("mov rcx, %0" : : "R"(kernel_state) :);

        LOAD_KERNEL_STATE_REG(rcx, rsp); /* Switch stack */
        LOAD_KERNEL_STATE_REG(rcx, r15);
        LOAD_KERNEL_STATE_REG(rcx, r14);
        LOAD_KERNEL_STATE_REG(rcx, r13);
        LOAD_KERNEL_STATE_REG(rcx, r12);
        LOAD_KERNEL_STATE_REG(rcx, rbp);
        LOAD_KERNEL_STATE_REG(rcx, rbx);

        asm volatile("xor rax, rax");
        asm volatile("jmp [rcx + %0]" : : "i"(offsetof(struct amd64_kernel_state, k_rip)) :);
        UNREACHABLE();
}

NAKED
void locore_thread_continue(void)
{
        // asm volatile("mov rdi, rbx");
        asm volatile("mov rdi, rax");
        asm volatile("xor rbp, rbp");
        asm volatile("call rbx");

        UNREACHABLE();
}

static thread_t switch_context_internal(thread_t old_thread, void *continuation, thread_t new_thread, struct amd64_kernel_state *ks)
{
        int mycpu                                   = cpu_number();
        vm_offset_t active_stack_address            = active_stacks[mycpu];
        struct amd64_kernel_state *old_kernel_state = STACK_IKS(active_stack_address);
        *old_kernel_state                           = *ks;

        old_thread->kernel_stack = active_stack_address;
        old_thread->swap_func    = continuation;

        vm_offset_t new_kernel_stack_address        = new_thread->kernel_stack;
        struct amd64_kernel_state *new_kernel_state = STACK_IKS(new_kernel_stack_address);

        /* store stack address */
        active_stacks[mycpu] = new_kernel_stack_address;

        /* store stack top (where is this used)*/
        kernel_stack[mycpu] = new_kernel_stack_address
                              + (KERNEL_STACK_SIZE - sizeof(struct amd64_kernel_state) - (sizeof(struct amd64_exception_link)));
        active_threads[mycpu] = new_thread;
        wrmsr(MSR_FS_BASE, new_thread->tls);

        asm volatile("mov rcx, %0" : : "R"(new_kernel_state) :);
        asm volatile("mov rax, %0" : : "R"(old_thread) :);

        LOAD_KERNEL_STATE_REG(rcx, rsp); /* Switch stack */
        LOAD_KERNEL_STATE_REG(rcx, r15);
        LOAD_KERNEL_STATE_REG(rcx, r14);
        LOAD_KERNEL_STATE_REG(rcx, r13);
        LOAD_KERNEL_STATE_REG(rcx, r12);
        LOAD_KERNEL_STATE_REG(rcx, rbp);
        LOAD_KERNEL_STATE_REG(rcx, rbx);

        // if (new_kernel_state->k_rip < 0xDEADBEEF) {
        //  printf("dk\n");
        //}

        asm volatile("jmp [rcx + %0]" : : "i"(offsetof(struct amd64_kernel_state, k_rip)) :);
        UNREACHABLE();
}

/*
 *	This really only has to save registers
 *	when there is no explicit continuation.
 */
NAKED
thread_t locore_switch_context(thread_t old_thread, continuation_t continuation, thread_t new_thread)
{
        /* k_rip is already pushed by the cpu */
        /* Save the return rsp(without the rip pushed by the CPU) */

        asm volatile("mov rax, rsp");
        asm volatile("add rax, 8");
        asm volatile("push rax");

        asm volatile("push r15");
        asm volatile("push r14");
        asm volatile("push r13");
        asm volatile("push r12");
        asm volatile("push rbp");
        asm volatile("push rbx");

        asm volatile("mov rcx, rsp");

        asm volatile("call %0" : : "i"(switch_context_internal) :);
        asm volatile("ret");
        UNREACHABLE();
}

/* Bootstrap code for pid 1, we only forward what we got on the stack to
 * execve, if execve fails, we call exit */
SECTION(icodetext) NAKED static void pid1_bs(void)
{
        /* SYSCALL ARGS  RDI, RSI, RDX, R10, R8 and R9 */
        
        // Create stdin
        asm volatile("pop rdi"); /* holds /dev/tty or similar */
        asm volatile("xor rsi, rsi"); // no flags for open
        asm volatile("xor rdx, rdx"); // no mode for open
        asm volatile("mov rax, 2"); // open
        asm volatile("syscall");
        // TODO: Check for failure
        // Create stdout
        asm volatile("xor rdi, rdi"); /* dup fd 0 */
        asm volatile("mov rax, 32"); // dup
        asm volatile("syscall");
        // Create stderr
        asm volatile("xor rdi, rdi"); /* dup fd 0 */
        asm volatile("mov rax, 32"); // dup
        asm volatile("syscall");

        /* Pathname as first arg */
        asm volatile("mov rdi, QWORD PTR [rsp + 8]"); /* Skip argc */
        /* Args begin */
        asm volatile("mov rsi, rsp"); /* Skip argc and pathname */
        asm volatile("add rsi, 8");

        /* Find the end of args */
        asm volatile("mov rdx, rsi");
        asm volatile("loop_arg:");
        asm volatile("add rdx, 8");
        asm volatile("cmp QWORD PTR [rdx - 8], 0");
        asm volatile("jne loop_arg");

        asm volatile("mov rax, 59"); // EXECVE
        asm volatile("syscall");

        /* if get here execve failed, just call exit */
        /* Maybe print a message ? */
        asm volatile("mov rax, 60"); // EXIT
}

// Temp
#include <aux/init.h>
void do_initcalls(int lvl);

void do_earlyinitcalls(void);

extern initcall_t __initcall_start[];
extern initcall_t __initcall0_start[];
extern initcall_t __initcall1_start[];
extern initcall_t __initcall2_start[];
extern initcall_t __initcall3_start[];
extern initcall_t __initcall4_start[];
extern initcall_t __initcall5_start[];
extern initcall_t __initcall6_start[];
extern initcall_t __initcall7_start[];
extern initcall_t __initcall_end[];

initcall_t *initcall_levels[] = {
    __initcall0_start,
    __initcall1_start,
    __initcall2_start,
    __initcall3_start,
    __initcall4_start,
    __initcall5_start,
    __initcall6_start,
    __initcall7_start,
    __initcall_end,
};

const char *initcall_level_names[] = {"pure", "core", "postcore", "arch", "subsys", "fs", "device", "late"};

void do_initcalls(int lvl)
{
        size_t n_calls = (size_t)(((size_t)initcall_levels[lvl + 1] - (size_t)initcall_levels[lvl]) / sizeof(uintptr_t));
        for (size_t i = 0; i < n_calls; ++i)
        {
                if (initcall_levels[lvl][i]())
                {
                        panic("%s(%p) failed", initcall_level_names[lvl], initcall_levels[lvl][i]);
                }
        }
}

void do_earlyinitcalls(void)
{
        size_t n_calls = (size_t)(((size_t)__initcall0_start - (size_t)__initcall_start) / sizeof(uintptr_t));
        for (size_t i = 0; i < n_calls; ++i)
        {
                KASSERT((__initcall_start[i]()) == 0);
        }
}

void locore_init_user(void)
{
        cpu_flags = 0;

        unsigned int eax = 0, ebx = 0, ecx = 0, edx = 0;
        __get_cpuid_count(7, 0, &eax, &ebx, &ecx, &edx);

        if (ebx & (1 << 20))
        {
                set_cr4(get_cr4() | CR4_SMAP);
                cpu_flags = 1;
        }

        /*Initialize higher half first */
        do_initcalls(INITCALL_SUBSYS);
        do_initcalls(INITCALL_DEVICE);
        do_initcalls(INITCALL_FS);
        do_initcalls(INITCALL_LATE);
        log_set_quiet(FALSE);

        // Map icode
        u8 *icd     = ICODE_DATA;
        size_t icsz = ICODE_SIZE;

        vm_map_t umap = current_task()->map;

        vm_offset_t uicode = 0x00400000;

        kern_return_t result = vm_map_find(umap, NULL, 0, &uicode, round_page(ICODE_SIZE), FALSE, NULL);

        KASSERT(result == KERN_SUCCESS);

        int err = copyout(ICODE_DATA, (void *)uicode, ICODE_SIZE);

        if (err)
        {
                panic("Could not copyout icode");
        }

        result = vm_map_protect(umap, uicode, uicode + round_page(ICODE_SIZE), VM_PROT_READ | VM_PROT_EXECUTE, FALSE);

        KASSERT(result == KERN_SUCCESS);

        vm_address_t pid1_ssize    = USER_STACK_SIZE / PGBYTES;
        vm_address_t pid1_maxsaddr = VM_USER_STACK_ADDRESS - USER_STACK_SIZE;

        result = vm_map_find(umap, NULL, 0, &pid1_maxsaddr, USER_STACK_SIZE, FALSE, NULL);

        KASSERT(result == KERN_SUCCESS);

        result = vm_map_protect(umap, (pid1_maxsaddr), (pid1_maxsaddr) + USER_STACK_SIZE, VM_PROT_READ | VM_PROT_WRITE, FALSE);

        KASSERT(result == KERN_SUCCESS);

//#define INIT "/sbin/init"
#define INIT "/sbin/sash"

        // int copyout(const void *kaddr, void *uaddr, vm_size_t len) {
#define ALIGN_STACK(p) ((p) & ~((typeof(p))0xF))
        /* Setup a small stack for icode */
        /* start with arg(s) */
        vm_address_t ustack = VM_USER_STACK_ADDRESS - 8;
        /* http://articles.manugarg.com/aboutelfauxiliaryvectors.html */
        ustack -= sizeof(INIT);
        char *initnmp = (char *)ustack;
        copyout(INIT, (void *)ustack, sizeof(INIT));


#define CONSOLE "/dev/console"

        ustack -= sizeof(CONSOLE);
        char *ttypath = (char *)ustack;
        copyout(CONSOLE, (void *)ustack, sizeof(CONSOLE));

        ustack -= sizeof("-a");
        char *initargp = (char *)ustack;
        copyout("-a", (void *)ustack, sizeof("-a"));

        ustack -= sizeof("SHELL=/sbin/sash");
        char *envp = (char *)ustack;
        copyout("SHELL=/sbin/sash", (void *)ustack, sizeof("SHELL=/sbin/sash"));

        ustack = ALIGN_STACK(ustack);

        /* env terminator and envp */

        ustack -= 8;
        ustack -= 8;
        copyout(&envp, (void *)ustack, sizeof(envp));
        ustack -= 8;

        /* arg terminator and args */
        ustack -= 8;
        copyout(&initargp, (void *)ustack, sizeof(initargp));
        ustack -= 8;
        copyout(&initnmp, (void *)ustack, sizeof(initnmp));

        ustack -= 8;
        size_t initargc = 2;
        copyout(&initargc, (void *)ustack, sizeof(initargc));

        ustack -= 8;
        copyout(&ttypath, (void *)ustack, sizeof(ttypath));

        size_t stack_sz = VM_MAX_ADDRESS - ustack;

        ustack = trunc_page(VM_MAX_ADDRESS - 1);

        thread_set_user_regs(current_thread(), pid1_maxsaddr, USER_STACK_SIZE, uicode, stack_sz);

        log_set_level(LOG_PANIC);

        log_info("Entering user mode");

        // After scheduler runs us again we will be propelled to usermode
        thread_block(locore_exception_return);

        UNREACHABLE();
}
