#pragma once

#include <kernel/lock.h>
#include <kernel/vm/pmap.h>
#include <kernel/vm/vm_page.h>

typedef struct pv_entry_data
{
        queue_chain_t m_pg_link;
        queue_chain_t m_lru_link;
        pmap_t m_pmap;
        vm_address_t m_vaddr;
} *pv_entry_t;

struct machine_page
{
        struct vm_page m_vm_page;
        natural_t m_pmap_attrubutes;
        natural_t m_tmp;
        simple_lock_data_t m_slock;
        queue_head_t m_pve_list;

        struct pv_entry_data m_emb_pve[2];

        natural_t m_pad[5];
};

typedef void (*pve_enum_cb_t)(machine_page *, pv_entry_t, void *);

pv_entry_t pve_find(machine_page *m, pmap_t pmap, vm_address_t vma);

void pve_free(machine_page *m, pv_entry_t a_pve);

void pve_alloc(machine_page *m, pmap_t pmap, vm_address_t vma);

void pve_enum(machine_page *m, pve_enum_cb_t a_cb, void *a_ud);

void pve_touch(pv_entry_t a_pve);