#include "pic.h"
#include "pio.h"
#include <kernel/mach_clock.h>
#include <mach/machine/spl.h>
#include <mach/machine/vm_types.h>

#include "proc_reg.h"

extern spl_t curr_ipl;
extern int pic_mask[/*NSPL*/];
extern int curr_pic_mask;

#define NAKED           __attribute__((naked))
#define Entry(new, old) extern spl_t(new)(void) __attribute__((alias(#old)))

/*
 * Set IPL to the specified value.
 *
 * NOTE: Normally we would not have to enable interrupts
 * here.  Linux drivers, however, use cli()/sti(), so we must
 * guard against the case where a Mach routine which
 * has done an spl() calls a Linux routine that returns
 * with interrupts disabled.  A subsequent splx() can,
 * potentially, return with interrupts disabled.
 */
#define SETIPL(level)                                                                                                              \
        movl $(level), % edx;                                                                                                      \
        cmpl EXT(curr_ipl), % edx;                                                                                                 \
        jne spl;                                                                                                                   \
        sti;                                                                                                                       \
        movl % edx, % eax;                                                                                                         \
        ret

static integer_t softclkpending;

/*
 * Program PICs with mask in %eax.
 */
#define SETMASK()                                                                                                                  \
        cmpl EXT(curr_pic_mask), % eax;                                                                                            \
        je 9f;                                                                                                                     \
        outb % al, $(PIC_MASTER_OCW);                                                                                              \
        movl % eax, EXT(curr_pic_mask);                                                                                            \
        movb % ah, % al;                                                                                                           \
        outb % al, $(PIC_SLAVE_OCW);                                                                                               \
        9:

static inline void pic_set_mask(int mask)
{
        if (curr_pic_mask != mask)
        {
                outb(PIC_MASTER_OCW, mask);
                curr_pic_mask = mask;
                mask          = mask >> 8;
                outb(PIC_SLAVE_OCW, mask);
        }
}

Entry(splsoftclock, spl1);
spl_t spl1(void) { return splx(SPL1); }

spl_t spl2(void) { return splx(SPL2); }

spl_t spl3(void) { return splx(SPL3); }

Entry(splnet, spl4);
Entry(splhdw, spl4);
spl_t spl4() { return splx(SPL4); }

Entry(splbio, spl5);
Entry(spldcm, spl5);
spl_t spl5() { return splx(SPL5); }

Entry(spltty, spl6);
Entry(splimp, spl6);
Entry(splvm, spl6);
spl_t spl6() { return splx(SPL6); }

Entry(splclock, spl7);
Entry(splsched, spl7);
Entry(splhigh, spl7);
Entry(splhi, spl7);
spl_t spl7(void) { return splx(SPL7); }

/*
 * Like splx() but returns with interrupts disabled and does
 * not return the previous ipl.  This should only be called
 * when returning from an interrupt.
 */
#if 0
.align	TEXT_ALIGN
	.globl	splx_cli
splx_cli:
	movl	4(%esp),%edx		/* get ipl */
	cli				/* disable interrupts */
	testl	%edx,%edx		/* spl0? */
	jnz	2f			/* no, skip */
	cmpl	$0,softclkpending	/* softclock pending? */
	je	1f			/* no, skip */
	movl	$0,softclkpending	/* clear flag */
	call	EXT(spl1)		/* block further interrupts */
	call	EXT(softclock)		/* go handle interrupt */
	cli				/* disable interrupts */
1:
	xorl	%edx,%edx		/* edx = ipl 0 */
2:
	cmpl	EXT(curr_ipl),%edx	/* same ipl as current? */
	je	1f			/* yes, all done */
	movl	%edx,EXT(curr_ipl)	/* set ipl */
	movl	EXT(pic_mask)(,%edx,4),%eax
					/* get PIC mask */
	SETMASK()			/* program PICs with new mask */
1:
	ret
#endif
void splx_cli(spl_t ipl)
{
        cli();
        if (ipl == 0)
        {
                if (softclkpending)
                {
                        softclkpending = 0;
                        spl1();
                        softclock();
                        cli();
                }
        }

        if (ipl != curr_ipl)
        {
                curr_ipl = ipl;
                int pm   = pic_mask[ipl];
                pic_set_mask(pm);
        }
}

spl_t splx(spl_t level)
{
        if (level == 0)
        {
                return spl0();
        }

        if (level == curr_ipl)
        {
                sti();
                return curr_ipl;
        }

        /* get PIC mask */
        int pm = pic_mask[level];

        /* disable interrupts */
        cli();

        /* set ipl */
        spl_t prev_ipl = curr_ipl;
        curr_ipl       = level;

        /* program PICs with new mask */
        pic_set_mask(pm);
        /* enable interrupts */
        sti();

        /* return previous ipl */
        return prev_ipl;

#if 0
	movl	4(%esp),%edx		/* get ipl */
	testl	%edx,%edx		/* spl0? */
	jz	EXT(spl0)		/* yes, handle specially */
	cmpl	EXT(curr_ipl),%edx	/* same ipl as current? */
	jne	spl			/* no */
	sti				/* ensure interrupts are enabled */
	movl	%edx,%eax		/* return previous ipl */
	ret


/*
 * NOTE: This routine must *not* use %ecx, otherwise
 * the interrupt code will break.
 */
	.align	TEXT_ALIGN
	.globl	spl
spl:
	movl	EXT(pic_mask)(,%edx,4),%eax
					/* get PIC mask */
	cli				/* disable interrupts */
	xchgl	EXT(curr_ipl),%edx	/* set ipl */
	SETMASK()			/* program PICs with new mask */
	sti				/* enable interrupts */
	movl	%edx,%eax		/* return previous ipl */
	ret
#endif
}

spl_t sploff(void)
{
#if 0
	pushfl
	popl	%eax
	cli
	ret
#endif
        spl_t rflags = (spl_t)get_rflags();
        cli();
        return rflags;
}

spl_t splon(spl_t rflags)
{
#if 0
	pushl	4(%esp)
	popfl
	ret
#endif
        set_rflags(rflags);
        return rflags;
}

void setsoftclock(void) { ++softclkpending; }
#if 0
movl	EXT(curr_ipl),%eax	/* save current ipl */
	pushl	%eax
	cli				/* disable interrupts */
	cmpl	$0,softclkpending	/* softclock pending? */
	je	1f			/* no, skip */
	movl	$0,softclkpending	/* clear flag */
	call	EXT(spl1)		/* block further interrupts */
	call	EXT(softclock)		/* go handle interrupt */

	cli				/* disable interrupts */
1:
	cmpl	$(SPL0),EXT(curr_ipl)	/* are we at spl0? */
	je	1f			/* yes, all done */
	movl	$(SPL0),EXT(curr_ipl)	/* set ipl */
	movl	EXT(pic_mask)+SPL0*4,%eax
					/* get PIC mask */
	SETMASK()			/* program PICs with new mask */
1:
	sti				/* enable interrupts */
	popl	%eax			/* return previous mask */
	ret
#endif
spl_t spl0(void)
{
        /* save current ipl */
        spl_t ipl = curr_ipl;

        /* disable interrupts */
        cli();

        /* softclock pending? */
        if (softclkpending)
        {
                /* clear flag */
                softclkpending = 0;
                /* block further interrupts */
                spl1();
                /* go handle interrupt */
                setsoftclock();
                /* disable interrupts */
                cli();
        }

        /* are we at spl0? */
        if (curr_ipl != SPL0)
        {
                curr_ipl = SPL0;
                int pm   = pic_mask[SPL0];
                pic_set_mask(pm);
        }

        /* enable interrupts */
        sti();
        /* return previous mask */
        return ipl;
}
