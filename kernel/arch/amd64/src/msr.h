#pragma once

#define MSR_APIC_BASE      0x0000001Bull
#define MSR_SYSENTER_CS    0x00000174ull
#define MSR_SYSENTER_ESP   0x00000175ull
#define MSR_SYSENTER_EIP   0x00000176ull
#define MSR_MTRR_DEFTYPE   0x000002FFull
#define MSR_EFER           0xC0000080ull
#define MSR_STAR           0xC0000081ull
#define MSR_LSTAR          0xC0000082ull
#define MSR_CSTAR          0xC0000083ull
#define MSR_SF_MASK        0xC0000084ull
#define MSR_FS_BASE        0xC0000100ull
#define MSR_GS_BASE        0xC0000101ull
#define MSR_KERNEL_GS_BASE 0xC0000102ull
#define MSR_TSC_AUX        0xC0000103ull
#define MSR_TSC_RATIO      0xC0000104ull

#define MSR_EFER_SCE   (1ULL << 0)  /* System Call Extensions R/W */
#define MSR_EFER_LME   (1ULL << 8)  /* Long Mode Enable R/W */
#define MSR_EFER_LMA   (1ULL << 10) /* Long Mode Active R/W */
#define MSR_EFER_NXE   (1ULL << 11) /* No-Execute Enable R/W */
#define MSR_EFER_SVME  (1ULL << 12) /* Secure Virtual Machine Enable R/W */
#define MSR_EFER_LMSLE (1ULL << 13) /* Long Mode Segment Limit Enable R/W */
#define MSR_EFER_FFXSR (1ULL << 14) /* Fast FXSAVE/FXRSTOR R/W */
#define MSR_EFER_TCE   (1ULL << 15) /* Translation Cache Extension R/W */

typedef u64 msr_t;
typedef u64 msrval_t;

extern msrval_t __msr_star;
extern msrval_t __msr_lstar;

static inline void wrmsr(msr_t msr, msrval_t val) {
  u32 hi, lo;

  lo = (u32)(val & 0xFFFFFFFF);
  hi = (u32)(val >> 32);

  asm volatile("wrmsr" : : "a"(lo), "d"(hi), "c"(msr) :);
}

static inline msrval_t rdmsr(msr_t msr) {
  u32 hi, lo;
  msrval_t res;

  asm volatile("rdmsr" : "=a"(lo), "=d"(hi) : "c"(msr) :);

  res = hi;

  res <<= 32;

  res |= lo;

  return res;
}