/*
 * Mach Operating System
 * Copyright (c) 1991,1990 Carnegie Mellon University
 * Copyright (c) 1991 IBM Corporation
 * All Rights Reserved.
 *
 * Permission to use, copy, modify and distribute this software and its
 * documentation is hereby granted, provided that both the copyright
 * notice and this permission notice appear in all copies of the
 * software, derivative works or modified versions, and any portions
 * thereof, and that both notices appear in supporting documentation,
 * and that the name IBM not be used in advertising or publicity
 * pertaining to distribution of the software without specific, written
 * prior permission.
 *
 * CARNEGIE MELLON AND IBM ALLOW FREE USE OF THIS SOFTWARE IN ITS "AS IS"
 * CONDITION.  CARNEGIE MELLON AND IBM DISCLAIM ANY LIABILITY OF ANY KIND FOR
 * ANY DAMAGES WHATSOEVER RESULTING FROM THE USE OF THIS SOFTWARE.
 *
 * Carnegie Mellon requests users of this software to return to
 *
 *  Software Distribution Coordinator  or  Software.Distribution@CS.CMU.EDU
 *  School of Computer Science
 *  Carnegie Mellon University
 *  Pittsburgh PA 15213-3890
 *
 * any improvements or extensions that they make and grant Carnegie Mellon
 * the rights to redistribute these changes.
 */
/*
 * Clock interrupt.
 */
#include <kernel/time_out.h>
#include <kernel/mach_clock.h>
#include <mach/machine/eflags.h>
#include <mach/machine/locore.h>
#include <mach/machine/thread.h>

#include "gdt.h"
#ifdef SYMMETRY
#include <sqt/intctl.h>
#endif
#include <mach/machine/spl.h>

#include "hardclock.h"

void hardclock(int iunit /* 'unit' number */,
               int old_ipl /* old interrupt level */,
               int kernel_mode /* return address in interrupt handler */,
               struct amd64_saved_state *regs /* saved registers */)
{
        if (kernel_mode /*return_to_iret*/)
        {
                /*
                 * Interrupt from interrupt stack.
                 */
                clock_interrupt(tick,             /* usec per tick */
                                FALSE,            /* kernel mode */
                                old_ipl == SPL0); /* not SPL0 */
        }
        else
        {
                /*
                 * Interrupt from user mode or from thread stack.
                 */

                clock_interrupt(tick, /* usec per tick */
                                TRUE,
                                old_ipl == SPL0 /* base priority */

                );
        }
}
