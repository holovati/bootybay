#include <ctype.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <sys/sysmacros.h>
#include <sys/ttydefaults.h>

#include <libtsm.h>

#include <aux/init.h>

#include <etl/algorithm.hh>
#include <etl/bit.hh>

#include <kernel/thread_data.h>
#include <mach/machine/ps2.h>
#include <mach/framebuffer.h>

#include <kernel/time_out.h>

#include <emerixx/drivers/tty/tty_device_data.h>

#include "keymap.hh"
#include "scancodes.h"

#define VT_OUTPUT_PENDING etl::bit<decltype(vt_data::flags), 0>::value

#define N_VTES 4

static void vte_write(struct tsm_vte *vte, const char *buf, size_t len, void *a_data);

static int vte_draw(struct tsm_screen *con,
                    uint64_t id,
                    const uint32_t *ch,
                    size_t len,
                    unsigned int width,
                    unsigned int posx,
                    unsigned int posy,
                    const struct tsm_screen_attr *attr,
                    tsm_age_t age,
                    void *data);

static struct vt_data
{
        natural_t flags;
        tty_device_t tty;
        struct tsm_screen *screen;
        struct tsm_vte *vte;
        tsm_age_t age;
        tsm_age_t __pad;
} s_vt_context[N_VTES];

typedef struct vt_data *vt_t;

static natural_t s_active_vte;

/* Input mod masks */
#define INMMASK_SHIFT_DOWN etl::bit<u32, 0>::value
#define INMMASK_CTRL_DOWN  etl::bit<u32, 1>::value
#define INMMASK_ALT_DOWN   etl::bit<u32, 2>::value
#define INMMASK_CAPS_LOCK  etl::bit<u32, 3>::value
#define INMMASK_NUM_LOCK   etl::bit<u32, 4>::value
static u32 immask;
static u32 tsm_mod;

natural_t s_last_redraw_tick, s_update_interval_tick;

extern struct psfhdr g_ter_116n_psf_start;
extern struct psfhdr g_ter_116b_psf_start;

extern char g_ter_112n_psf_start[];
static integer_t s_overlay_timeout;
static uint32_t s_overlay_xy[2];
static uint32_t s_overlay_fgbg[2];
static font_data s_overlay_font;

static font_data s_fonts[2]; // Normal and Bold
static uint32_t s_color_palette[TSM_COLOR_NUM];
static thread_t s_render_thread;
static fb_backbuffer_data s_backbuffer;

/* special codes */
#define K_UP     0x80 /* OR'd in if key below is released */
#define K_EXTEND 0xe0 /* marker for "extended" sequence */
#define K_ACKSC  0xfa /* ack for keyboard command */
#define K_RESEND 0xfe /* request to resend keybd cmd */

#define KSTATE_BUFMB etl::bit<uint32_t, 0>::value /* Buffer multi byte */

static u32 kstate;

static u8 kmbbuf[8];
static size_t kmbbuf_len;

// PS2 Keyboard scanset 1
static u32 scset1[]{
        KEY_INV,        KEY_ESC,     KEY_1,         KEY_2,          KEY_3,     KEY_4,        KEY_5,          KEY_6,
        KEY_7,          KEY_8,       KEY_9,         KEY_0,          KEY_MINUS, KEY_EQUAL,    KEY_BACKSPACE,  KEY_TAB,
        KEY_Q,          KEY_W,       KEY_E,         KEY_R,          KEY_T,     KEY_Y,        KEY_U,          KEY_I,
        KEY_O,          KEY_P,       KEY_LEFTBRACE, KEY_RIGHTBRACE, KEY_ENTER, KEY_LEFTCTRL, KEY_A,          KEY_S,
        KEY_D,          KEY_F,       KEY_G,         KEY_H,          KEY_J,     KEY_K,        KEY_L,          KEY_SEMICOLON,
        KEY_APOSTROPHE, KEY_GRAVE,   KEY_LEFTSHIFT, KEY_BACKSLASH,  KEY_Z,     KEY_X,        KEY_C,          KEY_V,
        KEY_B,          KEY_N,       KEY_M,         KEY_COMMA,      KEY_DOT,   KEY_SLASH,    KEY_RIGHTSHIFT, KEY_KPASTERISK,
        KEY_LEFTALT,    KEY_SPACE,   KEY_CAPSLOCK,  KEY_F1,         KEY_F2,    KEY_F3,       KEY_F4,         KEY_F5,
        KEY_F6,         KEY_F7,      KEY_F8,        KEY_F9,         KEY_F10,   KEY_NUMLOCK,  KEY_SCROLLLOCK, KEY_KP7,
        KEY_KP8,        KEY_KP9,     KEY_KPMINUS,   KEY_KP4,        KEY_KP5,   KEY_KP6,      KEY_KPPLUS,     KEY_KP1,
        KEY_KP2,        KEY_KP3,     KEY_KP0,       KEY_KPDOT,      KEY_INV,   KEY_INV,      KEY_INV,        KEY_F11,
        KEY_F12,        KEY_KPEQUAL,
};

#define STATE_EX BIT0

#define SCSET_SIZE(set) (sizeof((set)) / sizeof((set)[0]))

#define KEYHANDLE(k)                                                                                                               \
        do                                                                                                                         \
        {                                                                                                                          \
                if (keyup)                                                                                                         \
                        con_key_up(k);                                                                                             \
                else                                                                                                               \
                        con_key_down(k);                                                                                           \
        }                                                                                                                          \
        while (0)

// ********************************************************************************************************************************
static void con_key_up(u32 key)
// ********************************************************************************************************************************
{
        if (key == KEY_LEFTSHIFT || key == KEY_RIGHTSHIFT)
        {
                immask &= ~INMMASK_SHIFT_DOWN;
                tsm_mod &= ~TSM_SHIFT_MASK;
                return;
        }

        if (key == KEY_LEFTCTRL || key == KEY_RIGHTCTRL)
        {
                immask &= ~INMMASK_CTRL_DOWN;
                tsm_mod &= ~TSM_CONTROL_MASK;
                return;
        }

        if (key == KEY_LEFTALT || key == KEY_RIGHTALT)
        {
                immask &= ~INMMASK_ALT_DOWN;
                tsm_mod &= ~TSM_ALT_MASK;
                return;
        }
}

// ********************************************************************************************************************************
static void con_key_down(u32 key)
// ********************************************************************************************************************************
{
        if (key == KEY_LEFTSHIFT || key == KEY_RIGHTSHIFT)
        {
                immask |= INMMASK_SHIFT_DOWN;
                tsm_mod |= TSM_SHIFT_MASK;
                return;
        }

        if (key == KEY_LEFTCTRL || key == KEY_RIGHTCTRL)
        {
                immask |= INMMASK_CTRL_DOWN;
                tsm_mod |= TSM_CONTROL_MASK;
                return;
        }

        if (key == KEY_LEFTALT || key == KEY_RIGHTALT)
        {
                immask |= INMMASK_ALT_DOWN;
                tsm_mod |= TSM_ALT_MASK;
                return;
        }

        if (key == KEY_CAPSLOCK)
        {
                if (immask & INMMASK_CAPS_LOCK)
                {
                        immask &= ~INMMASK_CAPS_LOCK;
                        tsm_mod &= ~TSM_LOCK_MASK;
                }
                else
                {
                        immask |= INMMASK_CAPS_LOCK;
                        tsm_mod |= TSM_LOCK_MASK;
                }
                return;
        }

        if (key == KEY_NUMLOCK)
        {
                if (immask & INMMASK_NUM_LOCK)
                {
                        immask &= ~INMMASK_NUM_LOCK;
                }
                else
                {
                        immask |= INMMASK_NUM_LOCK;
                }
                return;
        }

        if (!(key < KEYMAP_LEN(keymap_us)))
        {
                return;
        }

        u32 const *sym = &(keymap_us[key][0]);
        u32 osym;

        if (sym[1] >= XKB_KEY_KP_Space && sym[1] <= XKB_KEY_KP_9)
        {
                osym = sym[(immask & INMMASK_NUM_LOCK) != 0];
        }
        else
        {
                osym = sym[(immask & INMMASK_SHIFT_DOWN) != 0];
        }

        if ((immask & INMMASK_CAPS_LOCK) && isalpha(osym) && islower(osym))
        {
                osym = sym[1];
        }

        if (((tsm_mod & (TSM_CONTROL_MASK | TSM_ALT_MASK)) == (TSM_CONTROL_MASK | TSM_ALT_MASK)))
        {
                switch (key)
                {
                        case KEY_PAGEUP:
                        {
                                s_active_vte = (s_active_vte + 1) % N_VTES;
                        }
                        break;

                        case KEY_PAGEDOWN:
                        {
                                s_active_vte = (s_active_vte - 1) % N_VTES;
                        }
                        break;

                        default:
                                goto tsm_handler;
                }

                s_overlay_timeout = (hz / s_update_interval_tick) * 3;

                s_vt_context[s_active_vte].age = 0;

                s_vt_context[s_active_vte].age
                        = tsm_screen_draw(s_vt_context[s_active_vte].screen, vte_draw, &s_vt_context[s_active_vte]);
        }
        else
        {
        tsm_handler:
                tsm_vte_handle_keyboard(s_vt_context[s_active_vte].vte, osym, XKB_KEY_NoSymbol, tsm_mod, osym);
        }
}

// ********************************************************************************************************************************
static void kintr()
// ********************************************************************************************************************************
{
        u8 scancode = ps2_device_output();
        int keyup   = 0;
        ;

        if (kstate & KSTATE_BUFMB)
        {
                if (kmbbuf[kmbbuf_len] == 0xE0)
                {
                        /* Check if this is a key release */
                        keyup = (scancode & K_UP) != 0;
                        if (keyup)
                        {
                                scancode = (uint8_t)(scancode & ~K_UP);
                        }

                        switch (scancode)
                        {
                                case 0x10: /* (multimedia) previous */
                                        KEYHANDLE(KEY_PREVIOUSSONG);
                                        break;
                                case 0x19: /* (multimedia) next track) */
                                        KEYHANDLE(KEY_NEXTSONG);
                                        break;
                                case 0x1C: /* (keypad) enter */
                                        KEYHANDLE(KEY_KPENTER);
                                        break;
                                case 0x1D: /* right control */
                                        KEYHANDLE(KEY_RIGHTCTRL);
                                        break;
                                case 0x20: /* (multimedia) mute */
                                        KEYHANDLE(KEY_MUTE);
                                        break;
                                case 0x21: /* (multimedia) calculator */
                                        KEYHANDLE(KEY_CALC);
                                        break;
                                case 0x22: /* (multimedia) play */
                                        KEYHANDLE(KEY_PLAYPAUSE);
                                        break;
                                case 0x2E: /* (multimedia) volume down */
                                        KEYHANDLE(KEY_VOLUMEDOWN);
                                        break;
                                case 0x30: /* (multimedia) volume up */
                                        KEYHANDLE(KEY_VOLUMEUP);
                                        break;
                                case 0x32: /* (multimedia) WWW home */
                                        KEYHANDLE(KEY_WWW);
                                        break;
                                case 0x35: /* (keypad) / */
                                        KEYHANDLE(KEY_KPSLASH);
                                        break;
                                case 0x38: /* right alt (or altGr) */
                                        KEYHANDLE(KEY_RIGHTALT);
                                        break;
                                case 0x47: /* Home */
                                        KEYHANDLE(KEY_HOME);
                                        break;
                                case 0x48: /* Cursor up */
                                        KEYHANDLE(KEY_UP);
                                        break;
                                case 0x49: /* Page up */
                                        KEYHANDLE(KEY_PAGEUP);
                                        break;
                                case 0x4B: /* cursor left */
                                        KEYHANDLE(KEY_LEFT);
                                        break;
                                case 0x4D: /* cursor right */
                                        KEYHANDLE(KEY_RIGHT);
                                        break;
                                case 0x4F: /* end */
                                        KEYHANDLE(KEY_END);
                                        break;
                                case 0x50: /* cursor down */
                                        KEYHANDLE(KEY_DOWN);
                                        break;
                                case 0x51: /* Page down */
                                        KEYHANDLE(KEY_PAGEDOWN);
                                        break;
                                case 0x52: /* insert */
                                        KEYHANDLE(KEY_INSERT);
                                        break;
                                case 0x53: /* delete */
                                        KEYHANDLE(KEY_DELETE);
                                        break;
                                case 0x5B: /* left GUI(meta) */
                                        KEYHANDLE(KEY_LEFTMETA);
                                        break;
                                case 0x5C: /* right GUI(meta) */
                                        KEYHANDLE(KEY_RIGHTMETA);
                                        break;
                                case 0x5D: /* apps */
                                        KEYHANDLE(KEY_COMPOSE);
                                        break;
                                case 0x5E: /* Power (ACPI) */
                                        KEYHANDLE(KEY_POWER);
                                        break;
                                case 0x5F: /* Sleep (ACPI) */
                                        KEYHANDLE(KEY_SLEEP);
                                        break;
                                case 0x63: /* Wake (ACPI) */
                                        KEYHANDLE(KEY_WAKEUP);
                                        break;
                                case 0x65: /* WWW search */
                                        KEYHANDLE(KEY_SEARCH);
                                        break;
                                case 0x66: /* WWW favorites */
                                        KEYHANDLE(KEY_BOOKMARKS);
                                        break;
                                case 0x67: /* WWW refresh */
                                        KEYHANDLE(KEY_REFRESH);
                                        break;
                                case 0x68: /* WWW stop */
                                        KEYHANDLE(KEY_STOP);
                                        break;
                                case 0x69: /* WWW forward */
                                        KEYHANDLE(KEY_FORWARD);
                                        break;
                                case 0x6A: /* WWW back */
                                        KEYHANDLE(KEY_BACK);
                                        break;
                                case 0x6B: /* My computer ??? */
                                        KEYHANDLE(KEY_INV);
                                        break;
                                case 0x6C: /* Email */
                                        KEYHANDLE(KEY_MAIL);
                                        break;
                                case 0x6D: /* Select */
                                        KEYHANDLE(KEY_INV);
                                        break;
                                default:
                                        KEYHANDLE(KEY_INV);
                                        break;
                        }
                        kmbbuf_len = 0;
                        kstate &= ~KSTATE_BUFMB;
                        goto waitkey;
                }
        }

        switch (scancode)
        {
                case K_EXTEND:
                {
                        /* A multibyte sequence */
                        kmbbuf[kmbbuf_len] = scancode;
                        kstate |= KSTATE_BUFMB;
                        goto waitkey;
                }

                case K_RESEND:
                {
                        /* Keyboard wants controller to repeat last command it sent */
                        while (1)
                        {
                                panic("shait");
                        }
                        break;
                }
                case K_ACKSC:
                {
                        /* Command acknowledged (ACK) */
                        // khandleack(&kd_ack);
                        goto waitkey;
                }
                default:
                        break;
        }

        /* Check if this is a key release */
        keyup = (scancode & K_UP) != 0;
        if (keyup)
        {
                scancode = (uint8_t)(scancode & ~K_UP);
        }

        if (scancode < SCSET_SIZE(scset1))
        {
                KEYHANDLE(scset1[scancode]);
                goto waitkey;
        }

        // keyup++;

waitkey:
        return;
}

// ********************************************************************************************************************************
static int vte_draw(struct tsm_screen *con,
                    uint64_t id,
                    const uint32_t *ch,
                    size_t len,
                    unsigned int width,
                    unsigned int posx,
                    unsigned int posy,
                    const struct tsm_screen_attr *attr,
                    tsm_age_t age,
                    void *data)
// ********************************************************************************************************************************
{
        vt_t vt = (vt_t)data;

        if (age && (vt->age > 0) && (age <= vt->age))
        {
                return 0;
        }

        font_t f;

        if (attr->bold)
        {
                f = &s_fonts[1];
        }
        else
        {
                f = &s_fonts[0];
        }

        uint32_t glyph;
        if (len == 0)
        {
                glyph = ' ';
                len   = 1;
        }
        else
        {
                glyph = *ch & (f->count - 1);
        }

        int fbflags = 0;
        if (attr->underline)
        {
                fbflags |= FB_DRAW_GLYPH_UNDERLINE;
        }

        if (attr->inverse)
        {
                fbflags |= FB_DRAW_GLYPH_INVERSE_COLORS;
        }

        uint32_t fgbg[2] = { attr->fccode < 0 ? fb_make_rgb(attr->fr, attr->fg, attr->fb) : s_color_palette[attr->fccode],
                             attr->bccode < 0 ? fb_make_rgb(attr->br, attr->bg, attr->bb) : s_color_palette[attr->bccode] };

        fb_draw_glyph(glyph, posx * f->width, posy * f->height, f, fgbg, fbflags, /*&s_backbuffer*/ nullptr);

        return 0;
}

// ********************************************************************************************************************************
static void vte_write(struct tsm_vte *vte, const char *buf, size_t len, void *a_data)
// ********************************************************************************************************************************
{
        vt_t vt = (vt_t)(a_data);
        tty_device_lock(vt->tty);
        tty_device_input(vt->tty, buf, len);
        tty_device_unlock(vt->tty);
}

// ********************************************************************************************************************************
static void draw_active_vt_overlay()
// ********************************************************************************************************************************
{
        char out_buf[] = { 'T', 'T', 'Y', ' ', (char)('0' + (s_active_vte & 0x7)) };

        uint32_t x = s_overlay_xy[0], y = s_overlay_xy[1];
        fb_draw_string(out_buf,
                       ARRAY_SIZE(out_buf),
                       &x,
                       &y,
                       &s_overlay_font,
                       s_overlay_fgbg,
                       0,
                       /*&s_backbuffer*/ nullptr);
}

// ********************************************************************************************************************************
static void console_backbuffer_render()
// ********************************************************************************************************************************
{
again:
        if (ps2_primary_output_full())
        {
                kintr();
        }

        for (natural_t i = 0; i < N_VTES; ++i)
        {
                vt_t vt = &s_vt_context[i];

                if (vt->flags & VT_OUTPUT_PENDING)
                {
                        if (tty_device_lock_try(vt->tty) == true)
                        {
                                vt->flags &= ~VT_OUTPUT_PENDING;
                                tty_device_output_start(vt->tty, vt);
                                tty_device_unlock(vt->tty);
                        }
                }
        }

        natural_t tick_delta = (elapsed_ticks - s_last_redraw_tick);

        if (tick_delta >= s_update_interval_tick)
        {
                tick_delta = 0;

                s_last_redraw_tick = elapsed_ticks;

                s_vt_context[s_active_vte].age
                        = tsm_screen_draw(s_vt_context[s_active_vte].screen, vte_draw, &s_vt_context[s_active_vte]);

                if (s_overlay_timeout > 0)
                {
                        draw_active_vt_overlay();
                        s_overlay_timeout -= (integer_t)s_update_interval_tick;
                        if (s_overlay_timeout <= 0)
                        {
                                s_vt_context[s_active_vte].age = 0;
                        }
                }
        }

        // fb_render_backbuffer(&s_backbuffer);
        spl_t s = spltty();
        if (ps2_primary_output_full())
        {
                splx(s);
                goto again;
        }
        else
        {
                assert_wait((event_t)ps2_primary_output_full, true);
                thread_set_timeout((int)(s_update_interval_tick - tick_delta));
                splx(s);
                thread_block(console_backbuffer_render);
        }
        UNREACHABLE();
}

// ********************************************************************************************************************************
static kern_return_t tty_initialize(tty_device_t a_tty)
// ********************************************************************************************************************************
{
        vt_t vt = (vt_t)a_tty->m_private;

        vt->tty = a_tty;

        struct termios *tio = &a_tty->m_tio;
        struct winsize *ws  = &a_tty->m_ws;

        tio->c_iflag = TTYDEF_IFLAG;
        tio->c_oflag = TTYDEF_OFLAG;
        tio->c_cflag = TTYDEF_CFLAG;
        tio->c_lflag = TTYDEF_LFLAG;
        tio->c_line  = N_TTY;

        /* Set controls chars to match what libtsm produces */
        tio->c_cc[VINTR]    = '\x03'; /* Ctrl-C */
        tio->c_cc[VQUIT]    = '\x1c'; /* Ctrl-\ */
        tio->c_cc[VERASE]   = '\b';   /* Backspace */
        tio->c_cc[VKILL]    = '\x15'; /* Ctrl-U */
        tio->c_cc[VEOF]     = '\x04'; /* Ctrl-D */
        tio->c_cc[VTIME]    = CTIME;
        tio->c_cc[VMIN]     = CMIN;
        tio->c_cc[VSWTC]    = 0;
        tio->c_cc[VSTART]   = '\x11'; /* Ctrl-Q */
        tio->c_cc[VSTOP]    = '\x13'; /* Ctrl-S */
        tio->c_cc[VSUSP]    = '\x1a'; /* Ctrl-Z */
        tio->c_cc[VEOL]     = CEOL;
        tio->c_cc[VREPRINT] = '\x12'; /* Ctrl-R */
        tio->c_cc[VDISCARD] = '\x0f'; /* Ctrl-O */
        tio->c_cc[VWERASE]  = '\x17'; /* Ctrl-W */
        tio->c_cc[VLNEXT]   = '\x16'; /* Ctrl-V */
        tio->c_cc[VEOL2]    = CEOL;

        tio->__c_ispeed = tio->__c_ospeed = TTYDEF_SPEED;

        ws->ws_row    = (unsigned short)(s_backbuffer.height / s_fonts->height);
        ws->ws_col    = (unsigned short)(s_backbuffer.width / s_fonts->width);
        ws->ws_xpixel = (unsigned short)(s_backbuffer.width);
        ws->ws_ypixel = (unsigned short)(s_backbuffer.height);

        kern_return_t retval = tsm_screen_new(&vt->screen, nullptr, nullptr);

        if (KERN_STATUS_SUCCESS(retval))
        {
                retval = tsm_screen_resize(vt->screen, ws->ws_col, ws->ws_row);

                if (KERN_STATUS_SUCCESS(retval))
                {
                        retval = tsm_vte_new(&vt->vte, vt->screen, vte_write, vt, nullptr, nullptr);

                        if (KERN_STATUS_SUCCESS(retval))
                        {
                                tsm_screen_erase_screen(vt->screen, false);
                        }
                        else
                        {
                                tsm_screen_unref(vt->screen);
                        }
                }
                else
                {
                        tsm_screen_unref(vt->screen);
                }
        }

        return retval;
}

// ********************************************************************************************************************************
static void tty_user_closed(tty_device_t a_tty)
// ********************************************************************************************************************************
{
}

// ********************************************************************************************************************************
static kern_return_t tty_output_pending(tty_device_t a_tty)
// ********************************************************************************************************************************
{
        vt_t vt = (vt_t)a_tty->m_private;

        vt->flags |= VT_OUTPUT_PENDING;

        clear_wait(s_render_thread, THREAD_AWAKENED, false);
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t tty_input_processed(tty_device_t a_tty)
// ********************************************************************************************************************************
{
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t tty_recv_data(tty_device_t a_tty, void *a_data, natural_t a_data_len, natural_t *a_data_recv, void *)
// ********************************************************************************************************************************
{
        vt_t vt = (vt_t)(a_tty->m_private);

        if (a_tty->m_flags & TTY_DEVICE_FLAG_OUTPUT_SUSPENDED)
        {
                *a_data_recv = 0;
        }
        else
        {
                tty_device_unlock(a_tty);
                tsm_vte_input(vt->vte, (char *)a_data, a_data_len);
                tty_device_lock(a_tty);
                *a_data_recv = a_data_len;
        }

        return KERN_SUCCESS;
}

static struct tty_device_ops_data s_tty_ops = {
        .initialize      = tty_initialize,      //
        .user_closed     = tty_user_closed,     //
        .input_processed = tty_input_processed, //
        .output_pending  = tty_output_pending,  //
        .output_receive  = tty_recv_data        //
};

// ********************************************************************************************************************************
static kern_return_t console_init()
// ********************************************************************************************************************************
{
        // poll_device_ctx_init(&contty.t_pollfds);

        immask = 0;

        // Create a thread for back buffer rendering
        fb_alloc_backbuffer(&s_backbuffer);

        s_render_thread = kernel_thread(kernel_task, console_backbuffer_render, nullptr);

        psf_extract_font_data(&g_ter_116n_psf_start, &s_fonts[0]);

        psf_extract_font_data(&g_ter_116b_psf_start, &s_fonts[1]);

        s_color_palette[TSM_COLOR_BLACK]         = fb_make_rgb(0x2E, 0x34, 0x36);
        s_color_palette[TSM_COLOR_RED]           = fb_make_rgb(0xCC, 0x00, 0x00);
        s_color_palette[TSM_COLOR_GREEN]         = fb_make_rgb(0x4E, 0x9A, 0x06);
        s_color_palette[TSM_COLOR_YELLOW]        = fb_make_rgb(0xC4, 0xA0, 0x00);
        s_color_palette[TSM_COLOR_BLUE]          = fb_make_rgb(0x34, 0x65, 0xA4);
        s_color_palette[TSM_COLOR_MAGENTA]       = fb_make_rgb(0x75, 0x50, 0x7B);
        s_color_palette[TSM_COLOR_CYAN]          = fb_make_rgb(0x06, 0x98, 0x9A);
        s_color_palette[TSM_COLOR_LIGHT_GREY]    = fb_make_rgb(0xD3, 0xD7, 0xCF);
        s_color_palette[TSM_COLOR_DARK_GREY]     = fb_make_rgb(0x55, 0x57, 0x53);
        s_color_palette[TSM_COLOR_LIGHT_RED]     = fb_make_rgb(0xEF, 0x29, 0x29);
        s_color_palette[TSM_COLOR_LIGHT_GREEN]   = fb_make_rgb(0x8A, 0xE2, 0x34);
        s_color_palette[TSM_COLOR_LIGHT_YELLOW]  = fb_make_rgb(0xFC, 0xE9, 0x4F);
        s_color_palette[TSM_COLOR_LIGHT_BLUE]    = fb_make_rgb(0x72, 0x9F, 0xCF);
        s_color_palette[TSM_COLOR_LIGHT_MAGENTA] = fb_make_rgb(0xAD, 0x7F, 0xA8);
        s_color_palette[TSM_COLOR_LIGHT_CYAN]    = fb_make_rgb(0x34, 0xE2, 0xE2);
        s_color_palette[TSM_COLOR_WHITE]         = fb_make_rgb(0xEE, 0xEE, 0xEC);

        s_color_palette[TSM_COLOR_FOREGROUND] = fb_make_rgb(0xFF, 0xFF, 0xFF);
        s_color_palette[TSM_COLOR_BACKGROUND] = fb_make_rgb(0x0d, 0x11, 0x17);

        s_active_vte           = 0;
        s_last_redraw_tick     = elapsed_ticks;
        s_update_interval_tick = hz / 25;

        psf_extract_font_data(g_ter_112n_psf_start, &s_overlay_font);
        s_overlay_fgbg[0] = fb_make_rgb(0x2F, 0x81, 0xF7);
        s_overlay_fgbg[1] = fb_make_rgb(0x0d, 0x11, 0x17);

        s_overlay_timeout = 0;

        uint32_t overlay_margin = s_overlay_font.height * 2;

        s_overlay_xy[0] = (uint32_t)(s_backbuffer.width - (s_overlay_font.width * (sizeof("tty X") - 1)) - (overlay_margin));
        s_overlay_xy[1] = (uint32_t)(overlay_margin);

        for (natural_t i = 0; i < N_VTES; ++i)
        {
                kern_return_t retval = tty_device_register("tty", makedev(0, i), &s_tty_ops, &s_vt_context[i]);
                KASSERT(KERN_STATUS_SUCCESS(retval));
        }

        spl_t s = spltty();
        ps2_enable_primary();
        while (ps2_primary_output_full())
        {
                ps2_device_output();
        }
        splx(s);

        return KERN_SUCCESS;
}

device_initcall(console_init);
