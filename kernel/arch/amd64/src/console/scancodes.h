#pragma once

/* Based on https://source.android.com/devices/input/keyboard-devices.html */

#define KEY_INV        0x0000 /* Invalid scancode */
#define KEY_ESC        0x0001 /* 0x07 0x0029 Keyboard ESCAPE */
#define KEY_1          0x0002 /* 0x07 0x001e Keyboard 1 and ! */
#define KEY_2          0x0003 /* 0x07 0x001f Keyboard 2 and @ */
#define KEY_3          0x0004 /* 0x07 0x0020 Keyboard 3 and # */
#define KEY_4          0x0005 /* 0x07 0x0021 Keyboard 4 and $ */
#define KEY_5          0x0006 /* 0x07 0x0022 Keyboard 5 and % */
#define KEY_6          0x0007 /* 0x07 0x0023 Keyboard 6 and ^ */
#define KEY_7          0x0008 /* 0x07 0x0024 Keyboard 7 and & */
#define KEY_8          0x0009 /* 0x07 0x0025 Keyboard 8 and * */
#define KEY_9          0x000a /* 0x07 0x0026 Keyboard 9 and ( */
#define KEY_0          0x000b /* 0x07 0x0027 Keyboard 0 and ) */
#define KEY_MINUS      0x000c /* 0x07 0x002d Keyboard - and _ */
#define KEY_EQUAL      0x000d /* 0x07 0x002e Keyboard = and + */
#define KEY_BACKSPACE  0x000e /* 0x07 0x002a Keyboard DELETE (Backspace) */
#define KEY_TAB        0x000f /* 0x07 0x002b Keyboard Tab */
#define KEY_Q          0x0010 /* 0x07 0x0014 Keyboard q and Q */
#define KEY_W          0x0011 /* 0x07 0x001a Keyboard w and W */
#define KEY_E          0x0012 /* 0x07 0x0008 Keyboard e and E */
#define KEY_R          0x0013 /* 0x07 0x0015 Keyboard r and R */
#define KEY_T          0x0014 /* 0x07 0x0017 Keyboard t and T */
#define KEY_Y          0x0015 /* 0x07 0x001c Keyboard y and Y */
#define KEY_U          0x0016 /* 0x07 0x0018 Keyboard u and U */
#define KEY_I          0x0017 /* 0x07 0x000c Keyboard i and I */
#define KEY_O          0x0018 /* 0x07 0x0012 Keyboard o and O */
#define KEY_P          0x0019 /* 0x07 0x0013 Keyboard p and P */
#define KEY_LEFTBRACE  0x001a /* 0x07 0x002f Keyboard [ and { */
#define KEY_RIGHTBRACE 0x001b /* 0x07 0x0030 Keyboard ] and } */
#define KEY_ENTER      0x001c /* 0x07 0x0028 Keyboard Return (ENTER) */
#define KEY_LEFTCTRL   0x001d /* 0x07 0x00e0 Keyboard Left Control */
#define KEY_A          0x001e /* 0x07 0x0004 Keyboard a and A */
#define KEY_S          0x001f /* 0x07 0x0016 Keyboard s and S */
#define KEY_D          0x0020 /* 0x07 0x0007 Keyboard d and D */
#define KEY_F          0x0021 /* 0x07 0x0009 Keyboard f and F */
#define KEY_G          0x0022 /* 0x07 0x000a Keyboard g and G */
#define KEY_H          0x0023 /* 0x07 0x000b Keyboard h and H */
#define KEY_J          0x0024 /* 0x07 0x000d Keyboard j and J */
#define KEY_K          0x0025 /* 0x07 0x000e Keyboard k and K */
#define KEY_L          0x0026 /* 0x07 0x000f Keyboard l and L */
#define KEY_SEMICOLON  0x0027 /* 0x07 0x0033 Keyboard ; and : */
#define KEY_APOSTROPHE 0x0028 /* 0x07 0x0034 Keyboard ' and " */
#define KEY_GRAVE      0x0029 /* 0x07 0x0035 Keyboard ` and ~ */
#define KEY_LEFTSHIFT  0x002a /* 0x07 0x00e1 Keyboard Left Shift */
#define KEY_BACKSLASH  0x002b /* 0x07 0x0031 Keyboard \ and | */
#define KEY_BACKSLASH  0x002b /* 0x07 0x0032 Keyboard Non-US # and ~ */
#define KEY_Z          0x002c /* 0x07 0x001d Keyboard z and Z */
#define KEY_X          0x002d /* 0x07 0x001b Keyboard x and X */
#define KEY_C          0x002e /* 0x07 0x0006 Keyboard c and C */
#define KEY_V          0x002f /* 0x07 0x0019 Keyboard v and V */
#define KEY_B          0x0030 /* 0x07 0x0005 Keyboard b and B */
#define KEY_N          0x0031 /* 0x07 0x0011 Keyboard n and N */
#define KEY_M          0x0032 /* 0x07 0x0010 Keyboard m and M */
#define KEY_COMMA      0x0033 /* 0x07 0x0036 Keyboard , and < */
#define KEY_DOT        0x0034 /* 0x07 0x0037 Keyboard . and > */
#define KEY_SLASH      0x0035 /* 0x07 0x0038 Keyboard / and ? */
#define KEY_RIGHTSHIFT 0x0036 /* 0x07 0x00e5 Keyboard Right Shift */
#define KEY_KPASTERISK 0x0037 /* 0x07 0x0055 Keypad * */
#define KEY_LEFTALT    0x0038 /* 0x07 0x00e2 Keyboard Left Alt */
#define KEY_SPACE      0x0039 /* 0x07 0x002c Keyboard Spacebar */
#define KEY_CAPSLOCK   0x003a /* 0x07 0x0039 Keyboard Caps Lock */
#define KEY_F1         0x003b /* 0x07 0x003a Keyboard F1 */
#define KEY_F2         0x003c /* 0x07 0x003b Keyboard F2 */
#define KEY_F3         0x003d /* 0x07 0x003c Keyboard F3 */
#define KEY_F4         0x003e /* 0x07 0x003d Keyboard F4 */
#define KEY_F5         0x003f /* 0x07 0x003e Keyboard F5 */
#define KEY_F6         0x0040 /* 0x07 0x003f Keyboard F6 */
#define KEY_F7         0x0041 /* 0x07 0x0040 Keyboard F7 */
#define KEY_F8         0x0042 /* 0x07 0x0041 Keyboard F8 */
#define KEY_F9         0x0043 /* 0x07 0x0042 Keyboard F9 */
#define KEY_F10        0x0044 /* 0x07 0x0043 Keyboard F10 */
#define KEY_NUMLOCK    0x0045 /* 0x07 0x0053 Keyboard Num Lock and Clear */
#define KEY_SCROLLLOCK 0x0046 /* 0x07 0x0047 Keyboard Scroll Lock */
#define KEY_KP7        0x0047 /* 0x07 0x005f Keypad 7 and Home */
#define KEY_KP8        0x0048 /* 0x07 0x0060 Keypad 8 and Up Arrow */
#define KEY_KP9        0x0049 /* 0x07 0x0061 Keypad 9 and Page Up */
#define KEY_KPMINUS    0x004a /* 0x07 0x0056 Keypad - */
#define KEY_KP4        0x004b /* 0x07 0x005c Keypad 4 and Left Arrow */
#define KEY_KP5        0x004c /* 0x07 0x005d Keypad 5 */
#define KEY_KP6        0x004d /* 0x07 0x005e Keypad 6 and Right Arrow */
#define KEY_KPPLUS     0x004e /* 0x07 0x0057 Keypad + */
#define KEY_KP1        0x004f /* 0x07 0x0059 Keypad 1 and End */
#define KEY_KP2        0x0050 /* 0x07 0x005a Keypad 2 and Down Arrow */
#define KEY_KP3        0x0051 /* 0x07 0x005b Keypad 3 and PageDn */
#define KEY_KP0        0x0052 /* 0x07 0x0062 Keypad 0 and Insert */
#define KEY_KPDOT      0x0053 /* 0x07 0x0063 Keypad . and Delete */
//
//
#define KEY_102ND 0x0056 /* 0x07 0x0064 Keyboard Non-US \ and | */
#define KEY_F11   0x0057 /* 0x07 0x0044 Keyboard F11 */
#define KEY_F12   0x0058 /* 0x07 0x0045 Keyboard F12 */
//
//
//
//
//
//
//
#define KEY_KPENTER   0x0060 /* 0x07 0x0058 Keypad ENTER */
#define KEY_RIGHTCTRL 0x0061 /* 0x07 0x00e4 Keyboard Right Control */
#define KEY_KPSLASH   0x0062 /* 0x07 0x0054 Keypad / */
#define KEY_SYSRQ     0x0063 /* 0x07 0x0046 Keyboard Print Screen */
#define KEY_RIGHTALT  0x0064 /* 0x07 0x00e6 Keyboard Right Alt */
#define KEY_LINEFEED  0x0065
#define KEY_HOME      0x0066 /* 0x07 0x004a Keyboard Home */
#define KEY_UP        0x0067 /* 0x07 0x0052 Keyboard Up Arrow */
#define KEY_PAGEUP    0x0068 /* 0x07 0x004b Keyboard Page Up */
#define KEY_LEFT      0x0069 /* 0x07 0x0050 Keyboard Left Arrow */
#define KEY_RIGHT     0x006a /* 0x07 0x004f Keyboard Right Arrow */
#define KEY_END       0x006b /* 0x07 0x004d Keyboard End */
#define KEY_DOWN      0x006c /* 0x07 0x0051 Keyboard Down Arrow */
#define KEY_PAGEDOWN  0x006d /* 0x07 0x004e Keyboard Page Down */
#define KEY_INSERT    0x006e /* 0x07 0x0049 Keyboard Insert */
#define KEY_DELETE    0x006f /* 0x07 0x004c Keyboard Delete Forward */
//
#define KEY_MUTE       0x0071 /* 0x07 0x00ef */
#define KEY_VOLUMEDOWN 0x0072 /* 0x07 0x00ee */
#define KEY_VOLUMEUP   0x0073 /* 0x07 0x00ed */
#define KEY_POWER      0x0074 /* 0x07 0x0066 Keyboard Power */
#define KEY_KPEQUAL    0x0075 /* 0x07 0x0067 Keypad = */
#define KEY_PAUSE      0x0077 /* 0x07 0x0048 Keyboard Pause */
//
//
//
//
//
#define KEY_LEFTMETA     0x007d /* 0x07 0x00e3 Keyboard Left GUI */
#define KEY_RIGHTMETA    0x007e /* 0x07 0x00e7 Keyboard Right GUI */
#define KEY_COMPOSE      0x007f /* 0x07 0x0065 Keyboard Application */
#define KEY_STOP         0x0080 /* 0x07 0x00f3 */
#define KEY_FIND         0x0088 /* 0x07 0x00f4 */
#define KEY_CALC         0x008c /* 0x07 0x00fb */
#define KEY_WAKEUP       0x008f /* 0x01 0x0083 System Wake Up */
#define KEY_WWW          0x0096 /* 0x07 0x00f0 */
#define KEY_MAIL         0x009b /* 0x0c 0x018a AL Email Reader */
#define KEY_BOOKMARKS    0x009c /* 0x0c 0x022a AC Bookmarks */
#define KEY_BACK         0x009e /* 0x07 0x00f1 */
#define KEY_FORWARD      0x009f /* 0x07 0x00f2 */
#define KEY_EJECTCD      0x00a1 /* 0x07 0x00ec */
#define KEY_NEXTSONG     0x00a3 /* 0x07 0x00eb */
#define KEY_PLAYPAUSE    0x00a4 /* 0x07 0x00e8 */
#define KEY_PREVIOUSSONG 0x00a5 /* 0x07 0x00ea */
#define KEY_STOPCD       0x00a6 /* 0x07 0x00e9 */
#define KEY_HOMEPAGE     0x00ac /* 0x0c 0x0223 AC Home */
#define KEY_REFRESH      0x00ad /* 0x0c 0x0227 AC Refresh */
#define KEY_SCROLLUP     0x00b1 /* 0x07 0x00f5 */
#define KEY_SCROLLDOWN   0x00b2 /* 0x07 0x00f6 */
#define KEY_KPLEFTPAREN  0x00b3 /* 0x07 0x00b6 Keypad ( */
#define KEY_KPRIGHTPAREN 0x00b4 /* 0x07 0x00b7 Keypad ) */
#define KEY_F13          0x00b7 /* 0x07 0x0068 Keyboard F13 */
#define KEY_F14          0x00b8 /* 0x07 0x0069 Keyboard F14 */
#define KEY_F15          0x00b9 /* 0x07 0x006a Keyboard F15 */
#define KEY_F16          0x00ba /* 0x07 0x006b Keyboard F16 */
#define KEY_F17          0x00bb /* 0x07 0x006c Keyboard F17 */
#define KEY_F18          0x00bc /* 0x07 0x006d Keyboard F18 */
#define KEY_F19          0x00bd /* 0x07 0x006e Keyboard F19 */
#define KEY_F20          0x00be /* 0x07 0x006f Keyboard F20 */
#define KEY_F21          0x00bf /* 0x07 0x0070 Keyboard F21 */
#define KEY_F22          0x00c0 /* 0x07 0x0071 Keyboard F22 */
#define KEY_F23          0x00c1 /* 0x07 0x0072 Keyboard F23 */
#define KEY_F24          0x00c2 /* 0x07 0x0073 Keyboard F24 */
#define KEY_SEARCH       0x00d9 /* 0x0c 0x0221 AC Search */
#define KEY_SLEEP        0x00f8 /* 0x07 0x008e */
#define KEY_SELECT       0x0161
#define KEY_CLEAR        0x0163
