#pragma once

#define GDT_PRIV_KERNEL 0
#define GDT_PRIV_USER   3

#define GDT_NULL       0x00 /* null selector */
#define GDT_KERNEL_CS  0x08 /* kernel code */
#define GDT_KERNEL_DS  0x10 /* kernel data */
#define GDT_USER_DS    0x18 /* user data */
#define GDT_USER_CS    0x20 /* user code */
#define GDT_KERNEL_TSS 0x28 /* master TSS (uniprocessor) */

#define GDT_KERNEL_CS_SELECTOR (GDT_KERNEL_CS | GDT_PRIV_KERNEL)
#define GDT_KERNEL_DS_SELECTOR (GDT_KERNEL_DS | GDT_PRIV_KERNEL)

#define GDT_USER_CS_SELECTOR (GDT_USER_CS | GDT_PRIV_USER)
#define GDT_USER_DS_SELECTOR (GDT_USER_DS | GDT_PRIV_USER)

extern struct pseudo_descriptor __gdt_descriptor;