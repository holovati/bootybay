#pragma once
// clang-format off
<%
# tepmlate metadata
f = open(_in_folder + "/data/exceptions.py", "r")
exceptions = eval(f.read())
f.close()
%>

% for exception in exceptions:
#define ${"TRAP_{}".format(exception["description"].upper().replace(" ", "_").replace("(", "").replace(")", "").replace(".","").replace("-","_"))} ${exception["vector"]}
% endfor

#define ${"TRAP_FAST_SYSCALL {}".format(len(exceptions) + 0)}

#if defined(TRAP_NAMES_ARRAY)
static char const *TRAP_NAMES_ARRAY[] =
{
% for exception in exceptions:
        "${"{}".format(exception["description"].upper().replace(" ", "_").replace("(", "").replace(")", "").replace(".","").replace("-","_"))}",
% endfor
        NULL
};
#endif