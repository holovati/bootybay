/*
 * Mach Operating System
 * Copyright (c) 1991,1990,1989 Carnegie Mellon University
 * All Rights Reserved.
 *
 * Permission to use, copy, modify and distribute this software and its
 * documentation is hereby granted, provided that both the copyright
 * notice and this permission notice appear in all copies of the
 * software, derivative works or modified versions, and any portions
 * thereof, and that both notices appear in supporting documentation.
 *
 * CARNEGIE MELLON ALLOWS FREE USE OF THIS SOFTWARE IN ITS "AS IS"
 * CONDITION.  CARNEGIE MELLON DISCLAIMS ANY LIABILITY OF ANY KIND FOR
 * ANY DAMAGES WHATSOEVER RESULTING FROM THE USE OF THIS SOFTWARE.
 *
 * Carnegie Mellon requests users of this software to return to
 *
 *  Software Distribution Coordinator  or  Software.Distribution@CS.CMU.EDU
 *  School of Computer Science
 *  Carnegie Mellon University
 *  Pittsburgh PA 15213-3890
 *
 * any improvements or extensions that they make and grant Carnegie Mellon
 * the rights to redistribute these changes.
 */
#include <etl/cstring.hh>
#include <kernel/vm/vm_kern.h>
#include <kernel/vm/vm_map.h>
#include <kernel/vm/vm_param.h>
#include <kernel/vm/vm_prot.h>
#include <mach/machine/pmap.h>
#include <mach/machine/vm_param.h>
#include <string.h>
#include <strings.h>

/*
 *	pmap_zero_page zeros the specified (machine independent) page.
 */
void pmap_zero_page(vm_offset_t p)
{
        // KASSERT(p != vm_page_fictitious_addr);
        etl::memset64((void *)phystokv(p), 0, PAGE_SIZE >> 3);
}

/*
 *	pmap_copy_page copies the specified (machine independent) pages.
 */
void pmap_copy_page(vm_offset_t a_dest, vm_offset_t a_source, vm_offset_t a_offset, size_t a_dest_size)
{
        // KASSERT(src != vm_page_fictitious_addr);
        // KASSERT(dst != vm_page_fictitious_addr);

        KASSERT((a_offset + a_dest_size) <= PAGE_SIZE);

        void *dest = (void *)(phystokv(a_dest) + a_offset);
        void *src  = (void *)(phystokv(a_source) + a_offset);

        if ((a_dest_size & (sizeof(uint64_t) - 1)) == 0) [[likley]]
        {
                etl::memcpy64(dest, src, a_dest_size >> 3);
        }
        else if ((a_dest_size & (sizeof(uint32_t) - 1)) == 0)
        {
                etl::memcpy32(dest, src, a_dest_size >> 2);
        }
        else if ((a_dest_size & (sizeof(uint16_t) - 1)) == 0)
        {
                etl::memcpy16(dest, src, a_dest_size >> 1);
        }
        else
        {
                etl::memcpy8(dest, src, a_dest_size);
        }
}
#if 0
/*
 *	copy_to_phys(src_addr_v, dst_addr_p, count)
 *
 *	Copy virtual memory to physical memory
 */
void copy_to_phys(vm_offset_t src_addr_v, vm_offset_t dst_addr_p, int count)
{
        assert(dst_addr_p != vm_page_fictitious_addr);
        bcopy((void *)src_addr_v, (void *)phystokv(dst_addr_p), count);
}

/*
 *	copy_from_phys(src_addr_p, dst_addr_v, count)
 *
 *	Copy physical memory to virtual memory.  The virtual memory
 *	is assumed to be present (e.g. the buffer pool).
 */
void copy_from_phys(vm_offset_t src_addr_p, vm_offset_t dst_addr_v, int count)
{
        assert(src_addr_p != vm_page_fictitious_addr);
        bcopy((void *)phystokv(src_addr_p), (void *)dst_addr_v, count);
}
#endif
/*
 *	kvtophys(addr)
 *
 *	Convert a kernel virtual address to a physical address
 */
vm_offset_t kvtophys(vm_offset_t addr)
{
        extern vm_offset_t kernel_virtual_start;
        vm_offset_t phys_mem_va = pmap_get_dmap_addr();
        if (addr < kernel_virtual_start)
        {
                return (addr - phys_mem_va);
        }

        pt_entry_t *valid_pte;
        pt_entry_t *pte;
        if ((pte = pmap_pte(kernel_pmap, addr, &valid_pte)) == PT_ENTRY_NULL)
        {
                return 0;
        }
        return i386_trunc_page(*pte) | (addr & INTEL_OFFMASK);
}
