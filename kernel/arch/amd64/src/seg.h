#pragma once
/*
 * Mach Operating System
 * Copyright (c) 1991,1990 Carnegie Mellon University
 * Copyright (c) 1991 IBM Corporation
 * All Rights Reserved.
 *
 * Permission to use, copy, modify and distribute this software and its
 * documentation is hereby granted, provided that both the copyright
 * notice and this permission notice appear in all copies of the
 * software, derivative works or modified versions, and any portions
 * thereof, and that both notices appear in supporting documentation,
 * and that the name IBM not be used in advertising or publicity
 * pertaining to distribution of the software without specific, written
 * prior permission.
 *
 * CARNEGIE MELLON AND IBM ALLOW FREE USE OF THIS SOFTWARE IN ITS "AS IS"
 * CONDITION.  CARNEGIE MELLON AND IBM DISCLAIM ANY LIABILITY OF ANY KIND FOR
 * ANY DAMAGES WHATSOEVER RESULTING FROM THE USE OF THIS SOFTWARE.
 *
 * Carnegie Mellon requests users of this software to return to
 *
 *  Software Distribution Coordinator  or  Software.Distribution@CS.CMU.EDU
 *  School of Computer Science
 *  Carnegie Mellon University
 *  Pittsburgh PA 15213-3890
 *
 * any improvements or extensions that they make and grant Carnegie Mellon
 * the rights to redistribute these changes.
 */

#define SEG_TYPE_LDT       0x02 /* LDT */
#define SEG_TYPE_TSS       0x09 /* task segment */
#define SEG_TYPE_TSS_BUSY  0x0b /* task busy */
#define SEG_TYPE_CALL_GATE 0x0c /* call gate */
#define SEG_TYPE_INTR_GATE 0x0e /* interrupt gate */
#define SEG_TYPE_TRAP_GATE 0x0f /* trap gate */

/* For use in the type nibble of real descriptor, if creating a code segment */
#define SEG_TYPE_CODE_A 0x1 /* code, accessed */
#define SEG_TYPE_CODE_R 0x2 /* code, readable */
#define SEG_TYPE_CODE_C 0x4 /* code, conforming */
#define SEG_TYPE_CODE   0x8 /* code */

/* For use in the type nibble of real descriptor, if creating a code segment */
#define SEG_TYPE_DATA_A 0x1 /* data, accessed */
#define SEG_TYPE_DATA_W 0x2 /* data, writable */
#define SEG_TYPE_DATA_E 0x4 /* data, expand down */

/* For use in the access nibble */
#define SEG_ACC_SYSTEM 0x0 /* system descriptor(S bit) ldt,tss,gate */
#define SEG_ACC_USER   0x1 /* user descriptor(S bit) code, data*/
#define SEG_ACC_PL     0x6 /* access rights: */
#define SEG_ACC_PL_K   0x0 /* kernel access only */
#define SEG_ACC_PL_U   0x6 /* user access */
#define SEG_ACC_P      0x8 /* segment present */

/* For use in the size field of real descriptor */
#define SEG_SIZE_L 0x2 /* long mode */
#define SEG_SIZE_G 0x8 /* granularity */

/*
 * Components of a selector
 */
#define SEL_LDT  0x04 /* local selector */
#define SEL_PL   0x03 /* privilege level: */
#define SEL_PL_K 0x00 /* kernel selector */
#define SEL_PL_U 0x03 /* user selector */

/*
 * Convert selector to descriptor table index.
 */
#define sel_idx(sel) ((sel) >> 3)

/*
 * Real segment descriptor.
 */
struct real_descriptor
{
        u32 limit_low : 16; /* limit 0..15 */
        u32 base_low : 16;  /* base  0..15 */
        u32 base_med : 8;   /* base  16..23 */
        u32 type : 4;       /* type nibble */
        u32 access : 4;     /* access nibble */
        u32 limit_high : 4; /* limit 16..19 */
        u32 size : 4;       /* granularity, D/B, long */
        u32 base_high : 8;  /* base 24..32 */
        u32 base_high_long; /* base 32..64 */
        u32 mbz;            /* Unused in long mode*/
};

/* Fill a segment descriptor.  */
static inline void fill_descriptor(struct real_descriptor *desc, vm_offset_t base, vm_size_t limit, u8 type, u8 access, u8 sizebits)
{
        if (limit > 0xfffff)
        {
                limit >>= 12;
                sizebits |= SEG_SIZE_G;
        }

        desc->limit_low      = limit & 0xffff;
        desc->base_low       = base & 0xffff;
        desc->base_med       = (base >> 16) & 0xff;
        desc->type           = type & 15;
        desc->access         = access | SEG_ACC_P;
        desc->limit_high     = limit >> 16;
        desc->size           = sizebits;
        desc->base_high      = (base >> 24) & 0xff;
        desc->base_high_long = (base >> 32);
        desc->mbz            = 0;
}