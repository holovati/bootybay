#include <limits.h>
#include <string.h>

#include <aux/graphics.h>
#include <aux/init.h>

#include <kernel/vm/vm_kern.h>
#include <kernel/vm/vm_param.h>

#include <mach/framebuffer.h>
#include <mach/machine/multiboot2.h>

#include <etl/algorithm.hh>
#include <etl/cstring.hh>
#include <etl/queue.h>
#include <etl/hash.hh>

typedef queue_head_t glyph_lru_data, *glyph_lru_t;
typedef queue_chain_t glyph_lru_link_data, *glyph_lru_link_t;

typedef queue_head_t glyph_bucket_data, *glyph_bucket_t;
typedef queue_chain_t glyph_bucket_link_data, *glyph_bucket_link_t;

typedef struct glyph_hash_entry_data
{
        glyph_bucket_link_data m_bucket_link;
        glyph_lru_link_data m_lru_link;
        uint32_t m_glyph;
        int32_t m_flags;
        uint32_t m_fgbg_color[2];
        uint32_t m_font_bitmap;
        uint32_t __pad;
        uint32_t m_bitmap[32][32];
} *glyph_hash_entry_t;

static struct glyph_hash_entry_data s_glyph_hash_entry_storage[0x100];
// 64 K, a lot but no collisions in most cases
static glyph_bucket_data s_glyph_buckets[ARRAY_SIZE(s_glyph_hash_entry_storage) * 16];
static glyph_lru_data s_glyph_lru;

extern struct multiboot_tag_framebuffer g_mbfb;

static fb_fix_screeninfo s_finfo;
static fb_var_screeninfo s_vinfo;
static fb_backbuffer_data s_hwbuffer;

struct plot_cache_bitmap_data
{
        uint32_t *bitmap;
        uint32_t color;
        uint32_t pitch;
};
// ********************************************************************************************************************************
static void plot_cache_bitmap(uint32_t a_xpos, uint32_t a_ypos, void *a_data)
// ********************************************************************************************************************************
{
        struct plot_cache_bitmap_data *pcbd = reinterpret_cast<struct plot_cache_bitmap_data *>(a_data);

        (&pcbd->bitmap[a_ypos * pcbd->pitch])[a_xpos] = pcbd->color;
}

struct fgbg_data
{
        fb_backbuffer_data *bbd;
        uint32_t color;
        uint32_t pad;
};
struct plot_pixel_data
{
        struct fgbg_data *fgd;
        struct fgbg_data *bgd;
        struct fgbg_data fgbg[2];
};
// ********************************************************************************************************************************
static void plot_pixel(uint32_t a_xpos, uint32_t a_ypos, void *a_data)
// ********************************************************************************************************************************
{
        fgbg_data *data = reinterpret_cast<fgbg_data *>(a_data);
        *reinterpret_cast<uint32_t *>(data->bbd->memory + a_ypos * data->bbd->bpitch + a_xpos * data->bbd->bytespp) = data->color;
        data->bbd->dirty++;
}

// ********************************************************************************************************************************
static void draw_glyph_cached(uint32_t a_glyph,
                              uint32_t a_xpos,
                              uint32_t a_ypos,
                              font_t a_font,
                              uint32_t *a_fgbg_color,
                              int a_flags,
                              fb_backbuffer_t a_backbuffer)
// ********************************************************************************************************************************
{
        uint32_t bitmap_bits = ((vm_address_t)(a_font->bitmap + (a_glyph * a_font->length)) & 0xFFFFFFFF);

        uint8_t hashbuf[] = { (uint8_t)((a_flags & 0xFF)),
                              (uint8_t)((a_fgbg_color[1] >> 8) & 0xFF),
                              (uint8_t)((a_fgbg_color[1] >> 16) & 0xFF),
                              (uint8_t)((a_fgbg_color[0] >> 8) & 0xFF),
                              (uint8_t)((a_fgbg_color[0] >> 16) & 0xFF),
                              (uint8_t)((a_glyph >> 0) & 0xFF),
                              (uint8_t)(((bitmap_bits >> 0) & 0xFF)),
                              (uint8_t)(((bitmap_bits >> 8) & 0xFF)) };

        natural_t hash = etl::hash::djb2::hash_buffer(hashbuf, sizeof(hashbuf));

        glyph_bucket_t bucket = &s_glyph_buckets[hash & (ARRAY_SIZE(s_glyph_buckets) - 1)];

        glyph_hash_entry_t glyph_entry = nullptr;

        for (glyph_bucket_link_t entry = queue_first(bucket); queue_end(bucket, entry) == false; entry = queue_next(entry))
        {
                glyph_hash_entry_t tmp = queue_containing_record(entry, struct glyph_hash_entry_data, m_bucket_link);

                if (tmp->m_glyph != a_glyph)
                {
                        continue;
                }

                if (tmp->m_flags != a_flags)
                {
                        continue;
                }

                if ((tmp->m_fgbg_color[0] != a_fgbg_color[0]) || (tmp->m_fgbg_color[1] != a_fgbg_color[1]))
                {
                        continue;
                }

                if (tmp->m_font_bitmap != bitmap_bits)
                {
                        continue;
                }

                glyph_entry = tmp;

                break;
        }

        if (glyph_entry != nullptr)
        {
                // Touch the LRU
                remqueue(&glyph_entry->m_lru_link);
                enqueue_head(&s_glyph_lru, &glyph_entry->m_lru_link);
        }
        else
        {
                // Grab the least recently used entry removing it from the LRU list
                glyph_entry = queue_containing_record(queue_last(&s_glyph_lru), struct glyph_hash_entry_data, m_lru_link);
                // Remove it from it's hash bucket
                remqueue(&glyph_entry->m_bucket_link);
                remqueue(&glyph_entry->m_lru_link);

                // Now re-initialize the new entry

                // Add to the correct bucket
                enqueue_head(bucket, &glyph_entry->m_bucket_link);

                // Add to LRU
                enqueue_head(&s_glyph_lru, &glyph_entry->m_lru_link);

                // Init the actual entry
                glyph_entry->m_glyph         = a_glyph;
                glyph_entry->m_flags         = a_flags;
                glyph_entry->m_fgbg_color[0] = a_fgbg_color[0];
                glyph_entry->m_fgbg_color[1] = a_fgbg_color[1];
                glyph_entry->m_font_bitmap   = bitmap_bits;

                // Render the glyph
                if (a_flags & FB_DRAW_GLYPH_INVERSE_COLORS)
                {
                        a_fgbg_color[0] ^= fb_make_rgb(0xFF, 0xFF, 0xFF);
                        a_fgbg_color[1] ^= fb_make_rgb(0xFF, 0xFF, 0xFF);
                }

                struct plot_cache_bitmap_data pcbd[2];

                pcbd[0].color  = a_fgbg_color[0];
                pcbd[0].bitmap = (uint32_t *)glyph_entry->m_bitmap;
                pcbd[0].pitch  = ARRAY_SIZE(glyph_entry->m_bitmap[0]);

                pcbd[1].bitmap = (uint32_t *)glyph_entry->m_bitmap;
                pcbd[1].color  = a_fgbg_color[1];
                pcbd[1].pitch  = ARRAY_SIZE(glyph_entry->m_bitmap[0]);

                void *ptrs[2] = { &pcbd[0], &pcbd[1] };

                graphics_draw_psf_glyph(a_glyph, //
                                        0,
                                        0,
                                        a_font->bitmap,
                                        a_font->width,
                                        a_font->height,
                                        a_font->length,
                                        plot_cache_bitmap,
                                        ptrs);

                if (a_flags & FB_DRAW_GLYPH_UNDERLINE)
                {
                        graphics_line_to(0, //
                                         0 + a_font->height - 3,
                                         0 + a_font->width,
                                         0 + a_font->height - 3,
                                         plot_cache_bitmap,
                                         &pcbd[0]);
                }
        }

        vm_address_t x_offset = (a_xpos * a_backbuffer->bytespp);

        if ((a_font->width & (sizeof(uint64_t) - 1)) == 0) [[likeley]]
        {
                for (uint32_t y = 0; y < a_font->height; ++y)
                {
                        vm_address_t y_offset = (a_ypos + y) * a_backbuffer->bpitch;
                        etl::memcpy64((uint64_t *)(a_backbuffer->memory + y_offset + x_offset),
                                      glyph_entry->m_bitmap[y],
                                      a_font->width >> 1);
                }
        }
        else
        {
                for (uint32_t y = 0; y < a_font->height; ++y)
                {
                        vm_address_t y_offset = (a_ypos + y) * a_backbuffer->bpitch;
                        etl::memcpy32((uint32_t *)(a_backbuffer->memory + y_offset + x_offset),
                                      glyph_entry->m_bitmap[y],
                                      a_font->width);
                }
        }
        a_backbuffer->dirty++;
}

// ********************************************************************************************************************************
uint32_t fb_make_rgb(uint32_t a_red, uint32_t a_green, uint32_t a_blue)
// ********************************************************************************************************************************
{
        return ((((uint32_t)(a_red)) & (UINT32_MAX >> ((CHAR_BIT * sizeof(uint32_t)) - g_mbfb.framebuffer_red_mask_size)))
                        << g_mbfb.framebuffer_red_field_position
                | (((uint32_t)(a_green)) & (UINT32_MAX >> ((CHAR_BIT * sizeof(uint32_t)) - g_mbfb.framebuffer_green_mask_size)))
                          << g_mbfb.framebuffer_green_field_position
                | (((uint32_t)(a_blue)) & (UINT32_MAX >> ((CHAR_BIT * sizeof(uint32_t)) - g_mbfb.framebuffer_blue_mask_size)))
                          << g_mbfb.framebuffer_blue_field_position);
}

// ********************************************************************************************************************************
void fb_draw_glyph(uint32_t a_glyph,
                   uint32_t a_xpos,
                   uint32_t a_ypos,
                   font_t a_font,
                   uint32_t *a_fgbg_color,
                   int a_flags,
                   fb_backbuffer_t a_backbuffer)
// ********************************************************************************************************************************
{
        fb_backbuffer_t buffer = a_backbuffer ? a_backbuffer : &s_hwbuffer;

        if (a_glyph >= a_font->count)
        {
                a_glyph = '?';
        }

        if ((a_font->length * sizeof(uint32_t)) <= sizeof(glyph_hash_entry_data::m_bitmap))
        {
                draw_glyph_cached(a_glyph, a_xpos, a_ypos, a_font, a_fgbg_color, a_flags, buffer);
        }
        else
        {
                plot_pixel_data ppd;
                ppd.fgd = &ppd.fgbg[0];
                ppd.bgd = &ppd.fgbg[1];

                ppd.fgbg[0] = { .bbd = buffer, .color = a_fgbg_color[0], .pad = 0 };
                ppd.fgbg[1] = { .bbd = buffer, .color = a_fgbg_color[1], .pad = 0 };

                if (a_flags & FB_DRAW_GLYPH_INVERSE_COLORS)
                {
                        ppd.fgbg[0].color ^= fb_make_rgb(0xFF, 0xFF, 0xFF);
                        ppd.fgbg[1].color ^= fb_make_rgb(0xFF, 0xFF, 0xFF);
                }

                graphics_draw_psf_glyph(a_glyph,
                                        a_xpos,
                                        a_ypos,
                                        a_font->bitmap,
                                        a_font->width,
                                        a_font->height,
                                        a_font->length,
                                        plot_pixel,
                                        &ppd);

                if (a_flags & FB_DRAW_GLYPH_UNDERLINE)
                {
                        graphics_line_to(a_xpos,
                                         a_ypos + a_font->height - 3,
                                         a_xpos + a_font->width,
                                         a_ypos + a_font->height - 3,
                                         plot_pixel,
                                         &ppd.fgbg[0]);
                }
        }
}

// ********************************************************************************************************************************
void fb_draw_string(const char *a_str,
                    size_t a_strlen,
                    uint32_t *a_xpos,
                    uint32_t *a_ypos,
                    font_t a_font,
                    uint32_t *a_fgbg_color,
                    int a_flags,
                    fb_backbuffer_t a_backbuffer)
// ********************************************************************************************************************************
{
        for (size_t i = 0; i < a_strlen; ++i)
        {
                uint32_t c = a_str[i];
                if (c == '\n')
                {
                        *a_ypos += a_font->height;
                        *a_xpos = 0;
                        continue;
                }

                fb_draw_glyph(c, *a_xpos, *a_ypos, a_font, a_fgbg_color, a_flags, a_backbuffer);

                if ((*a_xpos + a_font->width) >= (a_backbuffer ? a_backbuffer->width : s_hwbuffer.width))
                {
                        *a_xpos = 0;
                        *a_ypos += a_font->height;
                }
                else
                {
                        *a_xpos += a_font->width;
                }
        }
}

// ********************************************************************************************************************************
void fb_render_backbuffer(fb_backbuffer_t a_backbuffer)
// ********************************************************************************************************************************
{
        if (a_backbuffer->dirty)
        {
                uint8_t *vmem = reinterpret_cast<uint8_t *>(s_hwbuffer.memory);
                uint8_t *bmem = reinterpret_cast<uint8_t *>(a_backbuffer->memory);
                for (uint32_t y = 0; y < s_hwbuffer.height; ++y)
                {
                        etl::memcpy64(vmem, bmem, a_backbuffer->qpitch);
                        vmem += s_hwbuffer.bpitch;
                        bmem += s_hwbuffer.bpitch;
                }
                a_backbuffer->dirty = 0;
        }
}

// ********************************************************************************************************************************
kern_return_t fb_alloc_backbuffer(fb_backbuffer_t a_backbuffer)
// ********************************************************************************************************************************
{
        size_t bbuffer_size  = round_page((g_mbfb.common.framebuffer_height * g_mbfb.common.framebuffer_width)
                                         * (g_mbfb.common.framebuffer_bpp / CHAR_BIT));
        kern_return_t retval = kmem_alloc_wired(kernel_map, &a_backbuffer->memory, bbuffer_size);

        if (retval == KERN_SUCCESS)
        {
                a_backbuffer->bytespp = g_mbfb.common.framebuffer_bpp / CHAR_BIT;
                a_backbuffer->bpitch  = (g_mbfb.common.framebuffer_width * (a_backbuffer->bytespp));
                a_backbuffer->qpitch  = (a_backbuffer->bpitch) / sizeof(uint64_t);
                a_backbuffer->dirty   = 0;
                a_backbuffer->width   = g_mbfb.common.framebuffer_width;
                a_backbuffer->height  = g_mbfb.common.framebuffer_height;
        }

        return retval;
}

// ********************************************************************************************************************************
void fb_free_backbuffer(fb_backbuffer_t a_backbuffer)
// ********************************************************************************************************************************
{
        kmem_free(kernel_map, a_backbuffer->memory, a_backbuffer->bpitch);
}

// ********************************************************************************************************************************
kern_return_t fb_get_variable_screeninfo(struct fb_var_screeninfo *a_vsi)
// ********************************************************************************************************************************
{
        *a_vsi = s_vinfo;
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t fb_get_fixed_screeninfo(struct fb_fix_screeninfo *a_fsi)
// ********************************************************************************************************************************
{
        *a_fsi = s_finfo;
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t early_init()
// ********************************************************************************************************************************
{
        for (glyph_bucket_data &bucket : s_glyph_buckets)
        {
                queue_init(&bucket);
        }

        queue_init(&s_glyph_lru);

        for (glyph_hash_entry_data &entry : s_glyph_hash_entry_storage)
        {
                entry = {};
                enqueue_tail(&s_glyph_lru, &entry.m_lru_link);
                enqueue_tail(&s_glyph_buckets[0], &entry.m_bucket_link);
        }

        s_hwbuffer.memory  = g_mbfb.common.framebuffer_addr + VM_MIN_KERNEL_ADDRESS;
        s_hwbuffer.bytespp = g_mbfb.common.framebuffer_bpp / CHAR_BIT;
        s_hwbuffer.bpitch  = (g_mbfb.common.framebuffer_width * (s_hwbuffer.bytespp));
        s_hwbuffer.qpitch  = (s_hwbuffer.width) / sizeof(uint64_t);
        s_hwbuffer.dirty   = 0;
        s_hwbuffer.width   = g_mbfb.common.framebuffer_width;
        s_hwbuffer.height  = g_mbfb.common.framebuffer_height;

        // Fixed info
        etl::memcpy8(s_finfo.id, "Emerixx VESA", sizeof("Emerixx VESA"));

        s_finfo.smem_start = g_mbfb.common.framebuffer_addr;
        s_finfo.smem_len   = (uint32_t)round_page(g_mbfb.common.framebuffer_height * g_mbfb.common.framebuffer_pitch);

        s_finfo.type     = FB_TYPE_PACKED_PIXELS;
        s_finfo.type_aux = 0;

        s_finfo.visual = FB_VISUAL_TRUECOLOR;

        s_finfo.xpanstep  = 0;
        s_finfo.ypanstep  = 0;
        s_finfo.ywrapstep = 0;

        s_finfo.line_length = g_mbfb.common.framebuffer_pitch;

        s_finfo.accel        = 0;
        s_finfo.capabilities = 0;

        // Variable info
        s_vinfo.xres = g_mbfb.common.framebuffer_width;
        s_vinfo.yres = g_mbfb.common.framebuffer_height;

        s_vinfo.xres_virtual = s_vinfo.xres;
        s_vinfo.yres_virtual = s_vinfo.yres;

        s_vinfo.xoffset = 0;
        s_vinfo.yoffset = 0;

        s_vinfo.bits_per_pixel = g_mbfb.common.framebuffer_bpp;

        s_vinfo.grayscale = 0;

        s_vinfo.red.offset    = g_mbfb.framebuffer_red_field_position;
        s_vinfo.red.length    = g_mbfb.framebuffer_red_mask_size;
        s_vinfo.red.msb_right = 0;

        s_vinfo.green.offset    = g_mbfb.framebuffer_green_field_position;
        s_vinfo.green.length    = g_mbfb.framebuffer_green_mask_size;
        s_vinfo.green.msb_right = 0;

        s_vinfo.blue.offset    = g_mbfb.framebuffer_blue_field_position;
        s_vinfo.blue.length    = g_mbfb.framebuffer_blue_mask_size;
        s_vinfo.blue.msb_right = 0;

        // ??
        s_vinfo.transp.offset    = 0;
        s_vinfo.transp.length    = 0;
        s_vinfo.transp.msb_right = 0;

        s_vinfo.nonstd = 0;

        s_vinfo.activate = FB_ACTIVATE_NOW;

        s_vinfo.height = 0;
        s_vinfo.width  = 0;

        s_vinfo.accel_flags = FB_ACCEL_NONE;

        s_vinfo.pixclock     = 0;
        s_vinfo.left_margin  = 0;
        s_vinfo.right_margin = 0;
        s_vinfo.upper_margin = 0;
        s_vinfo.lower_margin = 0;
        s_vinfo.hsync_len    = 0;
        s_vinfo.vsync_len    = 0;
        s_vinfo.sync         = 0;
        s_vinfo.vmode        = 0;
        s_vinfo.rotate       = 0;
        s_vinfo.colorspace   = 0;

        return KERN_SUCCESS;
}

early_initcall(early_init);
