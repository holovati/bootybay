#pragma once
#include <mach/machine/vm_types.h>

/*
 * CR0
 */

#define CR0_PE (1ULL << 0)  /* Protection Enabled                             R/W */
#define CR0_MP (1ULL << 1)  /* Monitor Coprocessor                            R/W */
#define CR0_EM (1ULL << 2)  /* Emulation                                      R/W */
#define CR0_TS (1ULL << 3)  /* Task Switched                                  R/W */
#define CR0_ET (1ULL << 4)  /* Extension Type                                 R   */
#define CR0_NE (1ULL << 5)  /* Numeric Error                                  R/W */
#define CR0_WP (1ULL << 16) /* Write Protect                                  R/W */
#define CR0_AM (1ULL << 18) /* Alignment Mask                                 R/W */
#define CR0_NW (1ULL << 29) /* Not Writethrough                               R/W */
#define CR0_CD (1ULL << 30) /* Cache Disable                                  R/W */
#define CR0_PG (1ULL << 31) /* Paging                                         R/W */

#define CR4_VME        (1ULL << 0)  /* Virtual-8086 Mode Extensions                    R/W */
#define CR4_PVI        (1ULL << 1)  /* Protected-Mode Virtual Interrupts               R/W */
#define CR4_TSD        (1ULL << 2)  /* Time Stamp Disable                              R/W */
#define CR4_DE         (1ULL << 3)  /* Debugging Extensions                            R/W */
#define CR4_PSE        (1ULL << 4)  /* Page Size Extensions                            R/W */
#define CR4_PAE        (1ULL << 5)  /* Physical-Address Extension                      R/W */
#define CR4_MCE        (1ULL << 6)  /* Machine Check Enable                            R/W */
#define CR4_PGE        (1ULL << 7)  /* Page-Global Enable                              R/W */
#define CR4_PCE        (1ULL << 8)  /* Performance-Monitoring Counter Enable           R/W */
#define CR4_OSFXSR     (1ULL << 9)  /* Operating System FXSAVE/FXRSTOR Support         R/W */
#define CR4_OSXMMEXCPT (1ULL << 10) /* Operating System Unmasked Exception Support     R/W */
#define CR4_FSGSBASE   (1ULL << 16) /* RDFSBASE, RDGSBASE, WRFSBASE, WRGSBASE instr    R/W */
#define CR4_OSXSAVE    (1ULL << 18) /* XSAVE and Processor Extended States Enable Bit  R/W */
#define CR4_SMAP       (1ULL << 21) /* Supervisor-Mode Access Prevention R/W */

static inline void cli(void) { asm volatile("cli"); }
static inline void sti(void) { asm volatile("sti"); }
static inline void cld(void) { asm volatile("cld"); }
// static inline void std(void) { asm volatile("std"); }
static inline void clts(void) { asm volatile("clts"); }

static inline integer_t get_rflags(void)
{
        integer_t rflags;
        asm volatile("pushfq; pop %0" : "=r"(rflags));
        return rflags;
}

static inline void set_rflags(integer_t value) { asm volatile("push %0; popfq" : : "r"(value)); }

static inline integer_t get_cr0(void)
{
        integer_t result;
        asm volatile("mov %0, cr0" : "=r"(result));
        return result;
}

static inline void set_cr0(integer_t value) { asm volatile("mov cr0, %0" : : "r"(value)); }

static inline integer_t get_cr2(void)
{
        integer_t result;
        asm volatile("mov %0, cr2" : "=r"(result));
        return result;
}

static inline vm_offset_t get_cr3(void)
{
        vm_offset_t result;
        asm volatile("mov %0, cr3" : "=r"(result) : :);
        return result;
}

static inline void set_cr3(vm_offset_t value) { asm volatile("mov cr3, %0" : : "r"(value)); }

static inline void set_cr4(u64 cr4) { asm volatile("mov cr4, %0\n" : : "r"(cr4) : "memory"); }

static inline u64 get_cr4()
{
        u64 ret;
        asm volatile("mov %0, cr4 \n" : "=r"(ret) : :);
        return ret;
}

static inline void clear_ts(void) { asm volatile("clts"); }

static inline void set_ts(void) { set_cr0(get_cr0() | CR0_TS); }

static inline u16 get_tr(void)
{
        u16 seg;
        asm volatile("str %0" : "=rm"(seg));
        return seg;
}

static inline void set_tr(u16 value) { asm volatile("ltr %0" : : "rm"(value)); }

static inline u16 get_ldt(void)
{
        u16 seg;
        asm volatile("sldt %0" : "=rm"(seg));
        return seg;
}

/* Format of a "pseudo-descriptor", used for loading the IDT and GDT.  */
struct pseudo_descriptor
{
        u16 limit;
        vm_offset_t base;
} __attribute__((packed));

/* Load the processor's IDT or LDT pointers.  */
static inline void set_idt(struct pseudo_descriptor *pdesc) { asm volatile("lidt %0" : : "m"(*pdesc) :); }

/* Load the processor's IDT or LDT pointers.  */
static inline void set_gdt(struct pseudo_descriptor *pdesc) { asm volatile("lgdt %0" : : "m"(*pdesc) :); }

static inline void set_ldt(u16 seg) { asm volatile("lldt %0" : : "rm"(seg)); }
/* This doesn't set a processor register,
   but it's often used immediately after setting one,
   to flush the instruction queue.  */
static inline void flush_instr_queue(void) { asm volatile("wbinvd"); }
