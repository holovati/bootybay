/*
 * Mach Operating System
 * Copyright (c) 1991,1990,1989,1988 Carnegie Mellon University
 * All Rights Reserved.
 *
 * Permission to use, copy, modify and distribute this software and its
 * documentation is hereby granted, provided that both the copyright
 * notice and this permission notice appear in all copies of the
 * software, derivative works or modified versions, and any portions
 * thereof, and that both notices appear in supporting documentation.
 *
 * CARNEGIE MELLON ALLOWS FREE USE OF THIS SOFTWARE IN ITS "AS IS"
 * CONDITION.  CARNEGIE MELLON DISCLAIMS ANY LIABILITY OF ANY KIND FOR
 * ANY DAMAGES WHATSOEVER RESULTING FROM THE USE OF THIS SOFTWARE.
 *
 * Carnegie Mellon requests users of this software to return to
 *
 *  Software Distribution Coordinator  or  Software.Distribution@CS.CMU.EDU
 *  School of Computer Science
 *  Carnegie Mellon University
 *  Pittsburgh PA 15213-3890
 *
 * any improvements or extensions that they make and grant Carnegie Mellon
 * the rights to redistribute these changes.
 */
/*
 *	File:	pmap.c
 *	Author:	Avadis Tevanian, Jr., Michael Wayne Young
 *	(These guys wrote the Vax version)
 *
 *	Physical Map management code for Intel i386, i486, and i860.
 *
 *	Manages physical address maps.
 *
 *	In addition to hardware address maps, this
 *	module is called upon to provide software-use-only
 *	maps which may or may not be stored in the same
 *	form as hardware maps.  These pseudo-maps are
 *	used to store intermediate results from copy
 *	operations to and from address spaces.
 *
 *	Since the information managed by this module is
 *	also stored by the logical address mapping module,
 *	this module may throw away valid virtual-to-physical
 *	mappings at almost any time.  However, invalidations
 *	of virtual-to-physical mappings must be done as
 *	requested.
 *
 *	In order to cope with hardware architectures which
 *	make virtual-to-physical map invalidates expensive,
 *	this module may delay invalidate or reduced protection
 *	operations until such time as they are actually
 *	necessary.  This module is given full information as
 *	to which processors are currently using which maps,
 *	and to when physical maps must be made correct.
 */

#define RBTREE_IMPLEMENTATION
#include <etl/rbtree.hh>

#include <etl/algorithm.hh>

#include <kernel/cpu_number.h>

#include <kernel/vm/vm_map.h>
#include <kernel/vm/vm_param.h>
#include <kernel/vm/vm_prot.h>
#include <kernel/vm/vm_page_map.h>

#include <kernel/zalloc.h>
#include <mach/machine/model_dep.h>
#include <mach/machine/multiboot2.h>
#include <mach/machine/spl.h>
#include <mach/machine/thread.h>
#include <kernel/thread_data.h>
#include <string.h>

#include <kernel/vm/vm_meter.h>

#include <aux/defer.hh>

#include "machine_page.h"
#include "proc_reg.h"

/*
 *	Amount of virtual memory mapped by one
 *	page-directory-pointer entry.
 */
#define PML4E_MAPPED_SIZE (pml4enum2lin(1))

/*
 *	Amount of virtual memory mapped by one
 *	page-directory-pointer entry.
 */
#define PDPE_MAPPED_SIZE (pdpenum2lin(1))

/*
 *	Amount of virtual memory mapped by one
 *	page-directory entry.
 */
#define PDE_MAPPED_SIZE (pdenum2lin(1))

static machine_page *s_machine_page_array;

/*
 *	Resident pages that represent real memory
 *	are allocated from a free list.
 */
static queue_head_t s_freeq;
static simple_lock_data_t s_vm_page_queue_free_lock;

static void machine_page_init(machine_page *a_machine_page, vm_address_t a_phys_addr, vm_page_io_descr_t a_io_descr)
{
        // a_machine_page->object     = nullptr;
        a_machine_page->m_vm_page.offset     = 0;
        a_machine_page->m_vm_page.wire_count = 0;
        a_machine_page->m_vm_page.flags      = 0;
        a_machine_page->m_vm_page.phys_addr  = a_phys_addr;
        a_machine_page->m_vm_page.m_io_descr = a_io_descr;
        a_machine_page->m_tmp                = 0;

        queue_init(&a_machine_page->m_pve_list);

        simple_lock_init(&a_machine_page->m_slock);

        if (a_io_descr != nullptr) // Private pages are not born with a io descriptor
        {
                etl::clear(a_machine_page->m_vm_page.m_io_descr->m_lba);
        }

        etl::clear(a_machine_page->m_emb_pve);

        etl::clear(a_machine_page->m_pad);

        a_machine_page->m_pmap_attrubutes = 0;
}

struct machine_page_private
{
        machine_page m_mp;
        rbtnode_data m_rblink;
};

typedef RBTREE_UNIQUE_KEY(machine_page_private, m_rblink) machine_page_private_map;

static machine_page_private_map s_mppm;

RBTREE_INSERT_LESS(machine_page_private_map, machine_page_private)
// ********************************************************************************************************************************
(machine_page_private &lhs, machine_page_private &rhs)
// ********************************************************************************************************************************
{
        return lhs.m_mp.m_vm_page.phys_addr < rhs.m_mp.m_vm_page.phys_addr;
}

RBTREE_FIND_LESS(machine_page_private_map, vm_address_t const &)
// ********************************************************************************************************************************
(machine_page_private &lhs, vm_address_t const &rhs)
// ********************************************************************************************************************************
{
        return lhs.m_mp.m_vm_page.phys_addr < rhs;
}

RBTREE_FIND_LESS(machine_page_private_map, vm_address_t const &)
// ********************************************************************************************************************************
(vm_address_t const &lhs, machine_page_private &rhs)
// ********************************************************************************************************************************
{
        return lhs < rhs.m_mp.m_vm_page.phys_addr;
}

ZONE_DEFINE(machine_page_private, s_priv_page_zone, 0x1000, 0x100, ZONE_EXHAUSTIBLE, 0x10);

static machine_page *machine_page_get(vm_address_t a_physical_address)
{
        machine_page_private *mp = s_mppm.find(a_physical_address);
        if (mp == nullptr) [[likeley]]
        {
                return &s_machine_page_array[a_physical_address >> PGSHIFT];
        }
        return &mp->m_mp;
}

// Used in vm_object
vm_page_t vm_page_zero = nullptr;

static void machine_page_bootstrap(size_t a_phys_page_count, vm_page_io_descr_t a_io_desc_arr)
{
        /*
         *	Initialize the page frames.
         */

        /* Just in case */
        cnt             = {};
        cnt.v_page_size = PGBYTES;

        queue_init(&s_freeq);

        simple_lock_init(&s_vm_page_queue_free_lock);

        vm_address_t phys_addr;
        while (pmap_next_page(&phys_addr))
        {
                machine_page *m = machine_page_get(phys_addr);
                machine_page_init(m, phys_addr, a_io_desc_arr++);

#ifdef ENABLE_MEM_POISON
                vm_address_t dmap_phys_addr = phys_addr + pmap_get_dmap_addr();
                memset((void *)dmap_phys_addr, 0xEA, PAGE_SIZE);
#endif

                enqueue_head(&s_freeq, &m->m_vm_page.m_qlink);

                cnt.v_free_count++;

                a_phys_page_count--; // What is left we already used
        }

        // Used in vm_object for zero object
        vm_page_zero = pmap_alloc_page();
        vm_page_zero->zero_fill();
        vm_page_zero->flags |= PG_COPYONWRITE;
}

void pmap_free_page(vm_page_t a_vmp)
{
        KASSERT(a_vmp != vm_page_zero);
        int spl = splimp();
        if ((a_vmp->flags & PG_FICTITIOUS) == 0)
        {
                machine_page *m = (machine_page *)a_vmp;
                if (!queue_empty(&m->m_pve_list))
                {
                        panic("Freeing page with pv entries");
                }

                simple_lock(&s_vm_page_queue_free_lock);
                enqueue_head(&s_freeq, &a_vmp->m_qlink);
                cnt.v_free_count++;
                thread_wakeup(&cnt.v_free_count);
                simple_unlock(&s_vm_page_queue_free_lock);
        }
        else if (a_vmp->flags & PG_PRIVATE)
        {
                machine_page_private *mpp = (machine_page_private *)(a_vmp);
                KASSERT(queue_empty(&mpp->m_mp.m_pve_list));
                s_mppm.remove(mpp);
                s_priv_page_zone.free(mpp);
        }
        else
        {
                panic("page origin unknown");
        }
        splx(spl);
}

vm_page_t pmap_alloc_private_page(vm_address_t a_phys_address)
{
        machine_page_private *mpp = s_priv_page_zone.alloc();
        machine_page_init(&mpp->m_mp, a_phys_address, nullptr);
        mpp->m_mp.m_vm_page.flags |= PG_PRIVATE;
        s_mppm.insert(mpp);
        return &mpp->m_mp.m_vm_page;
}

/*
 *	pmap_alloc_page:
 *
 *	Remove a page from the free list.
 *	Returns VM_PAGE_NULL if the free list is too small.
 */

vm_page_t pmap_alloc_page()
{
        simple_lock(&s_vm_page_queue_free_lock);

        /*
         *	Only let privileged threads (involved in pageout)
         *	dip into the reserved pool.
         */

        if ((cnt.v_free_count < cnt.v_free_min) && !current_thread()->vm_privilege)
        {
                simple_unlock(&s_vm_page_queue_free_lock);
                log_error("Returning null");
                return nullptr;
        }

        KASSERT(queue_empty(&s_freeq) == false);

        --cnt.v_free_count;

        machine_page *mpp = (machine_page *)queue_containing_record(dequeue_head(&s_freeq), vm_page, m_qlink);

        simple_unlock(&s_vm_page_queue_free_lock);

        /*
         *	Decide if we should poke the pageout daemon.
         *	We do this if the free count is less than the low
         *	water mark, or if the free count is less than the high
         *	water mark (but above the low water mark) and the inactive
         *	count is less than its target.
         *
         *	We don't have the counts locked ... if they change a little,
         *	it doesn't really matter.
         */

        if ((cnt.v_free_count < cnt.v_free_min)
            || ((cnt.v_free_count < cnt.v_free_target) && (cnt.v_inactive_count < cnt.v_inactive_target)))
        {
                // thread_wakeup((event_t)&vm_page_free_wanted);
                // UNUSED(mem);
        }
        // pmap_zero_page(mpp->phys_addr); // XXX
        return &mpp->m_vm_page;
}

void pmap_wait_free_page()
{
        spl_t s = splimp();
        simple_lock(&s_vm_page_queue_free_lock);
        while (cnt.v_free_count < cnt.v_free_min)
        {
                assert_wait(&cnt.v_free_count, false);
                simple_unlock(&s_vm_page_queue_free_lock);
                thread_block(nullptr);
        }
        simple_unlock(&s_vm_page_queue_free_lock);
        splx(s);
}

vm_page_t pmap_get_managed_page(vm_address_t a_physical_address)
{
        return &machine_page_get(a_physical_address)->m_vm_page;
}

/*
 *	Global data structures.
 */

/*
 *	List of cpus that are actively using mapped memory.  Any
 *	pmap update operation must wait for all cpus in this list.
 *	Update operations must still be queued to cpus not in this
 *	list.
 */
cpu_set cpus_active;

/*
 *	List of cpus that are idle, but still operating, and will want
 *	to see any kernel pmap updates when they become active.
 */
cpu_set cpus_idle;

/*
 *	Quick test for pmap update requests.
 */
volatile boolean_t cpu_update_needed[NCPUS];

/*
 *	Private data structures.
 */

/* We grab thease from modeldep_bootdata */
static vm_address_t phys_first_addr;
static vm_address_t phys_last_addr;
static size_t s_npages;

static vm_address_t dmap_address;

// static vm_size_t pv_head_table_size(size_t npages);

#define WRITE_PTE(pte_p, pte_entry) *(pte_p) = (pte_entry);

struct alignas(PAGE_SIZE) pmap_dirbase
{
        pt_entry_t ptes[NPTES];
};

using pmap_zone_t = zone<pmap, 512, 4096>::type;
// using pv_list_zone_t      = zone<pv_entry, 10000, 4096>::type;
using pmap_dirbase_zone_t = zone<pmap_dirbase, THREAD_MAX, THREAD_CHUNK>::type;

static pmap_zone_t pmap_zone;
// static pv_list_zone_t pv_list_zone;
static pmap_dirbase_zone_t s_dirbase_zone;

/*
 *	Each entry in the pv_head_table is locked by a bit in the
 *	pv_lock_table.  The lock bits are accessed by the physical
 *	address of the page they lock.
 */

/*
 *	Range of kernel virtual addresses available for kernel memory mapping.
 *	Does not include the virtual addresses used to map physical memory 1-1.
 *	Initialized by pmap_bootstrap.
 */
vm_offset_t kernel_virtual_start;
vm_offset_t kernel_virtual_end;

/*
 *	We allocate page table pages directly from the VM system
 *	through this object.  It maps physical memory.
 */
static struct vm_page_map_data s_pmap_page_map;
static struct lock_data s_pmap_page_map_lock;

/*
 *	Locking and TLB invalidation
 */

/*
 *	Locking Protocols:
 *
 *	There are two structures in the pmap module that need locking:
 *	the pmaps themselves, and the per-page pv_lists (which are locked
 *	by locking the pv_lock_table entry that corresponds to the pv_head
 *	for the list in question.)  Most routines want to lock a pmap and
 *	then do operations in it that require pv_list locking -- however
 *	pmap_remove_all and pmap_copy_on_write operate on a physical page
 *	basis and want to do the locking in the reverse order, i.e. lock
 *	a pv_list and then go through all the pmaps referenced by that list.
 *	To protect against deadlock between these two cases, the pmap_lock
 *	is used.  There are three different locking protocols as a result:
 *
 *  1.  pmap operations only (pmap_extract, pmap_access, ...)  Lock only
 *		the pmap.
 *
 *  2.  pmap-based operations (pmap_enter, pmap_remove, ...)  Get a read
 *		lock on the pmap_lock (shared read), then lock the pmap
 *		and finally the pv_lists as needed [i.e. pmap lock before
 *		pv_list lock.]
 *
 *  3.  pv_list-based operations (pmap_remove_all, pmap_copy_on_write, ...)
 *		Get a write lock on the pmap_lock (exclusive write); this
 *		also guaranteees exclusive access to the pv_lists.  Lock the
 *		pmaps as needed.
 *
 *	At no time may any routine hold more than one pmap lock or more than
 *	one pv_list lock.  Because interrupt level routines can allocate
 *	mbufs and cause pmap_enter's, the pmap_lock and the lock on the
 *	kernel_pmap can only be held at splvm.
 */

/*
 *	We raise the interrupt level to splvm, to block interprocessor
 *	interrupts during pmap operations.  We must take the CPU out of
 *	the cpus_active set while interrupts are blocked.
 */
#define SPLVM(spl)                                                                                                                 \
        {                                                                                                                          \
                spl = splvm();                                                                                                     \
                i_bit_clear(cpu_number(), &cpus_active);                                                                           \
        }

#define SPLX(spl)                                                                                                                  \
        {                                                                                                                          \
                i_bit_set(cpu_number(), &cpus_active);                                                                             \
                splx(spl);                                                                                                         \
        }

#define SPLVM_SCOPE()                                                                                                              \
        spl_t DEFER_2(__spl, __LINE__);                                                                                            \
        SPLVM(DEFER_2(__spl, __LINE__));                                                                                           \
        defer(SPLX(DEFER_2(__spl, __LINE__)))

static lock_data_t pmap_system_lock;

#define PMAP_SYS_LOCK()                                                                                                            \
        SPLVM_SCOPE();                                                                                                             \
        LOCK_WRITE_SCOPE(&pmap_system_lock)

#define PMAP_LOCK(pmap_ptr)                                                                                                        \
        PMAP_SYS_LOCK();                                                                                                           \
        SIMPLE_LOCK_SCOPE(&(pmap_ptr)->lock)

static void pmap_remove_range(pmap_t pmap, vm_offset_t va, pt_entry_t *spte, pt_entry_t *epte); /* forward */
void signal_cpus(cpu_set use_list, pmap_t pmap, vm_offset_t start, vm_offset_t end);            /* forward */

static void pmap_update_tlbs(pmap_t pmap, vm_offset_t s, vm_offset_t e)
{
        cpu_set cpu_mask = 1 << cpu_number();
        cpu_set users;

        /* Since the pmap is locked, other updates are locked */
        /* out, and any pmap_activate has finished. */

        /* find other cpus using the pmap */
        users = (pmap)->cpus_using & ~cpu_mask;
        if (users)
        {
                /* signal them, and wait for them to finish */
                /* using the pmap */
                signal_cpus(users, (pmap), (s), (e));
                while ((pmap)->cpus_using & cpus_active & ~cpu_mask)
                {
                        continue;
                }
        }

        /* invalidate our own TLB if pmap is in use */
        if ((pmap)->cpus_using & cpu_mask)
        {
                pmap_invalidate_tlb((s), (e));
        }
}

#define MAX_TBIS_SIZE 32 /* > this -> TBIA */ /* XXX */

/*
 *	Structures to keep track of pending TLB invalidations
 */

#define UPDATE_LIST_SIZE 4

struct pmap_update_item
{
        pmap_t pmap;       /* pmap to invalidate */
        vm_offset_t start; /* start address to invalidate */
        vm_offset_t end;   /* end address to invalidate */
};

typedef struct pmap_update_item *pmap_update_item_t;

/*
 *	List of pmap updates.  If the list overflows,
 *	the last entry is changed to invalidate all.
 */
struct pmap_update_list
{
        simple_lock_data_t lock;
        integer_t count;
        struct pmap_update_item item[UPDATE_LIST_SIZE];
};
typedef struct pmap_update_list *pmap_update_list_t;

struct pmap_update_list cpu_update_list[NCPUS];

/*
 *	Other useful macros.
 */
#define current_pmap()         (vm_map_pmap(current_thread()->task->map))
#define pmap_in_use(pmap, cpu) (((pmap)->cpus_using & (1 << (cpu))) != 0)

struct pmap kernel_pmap_store;
pmap_t kernel_pmap;

unsigned int inuse_ptepages_count = 0; /* debugging */

extern char end;

/*
 * Pointer to the basic page directory for the kernel.
 * Initialized by pmap_bootstrap().
 */
pt_entry_t *kernel_page_dir;

static inline pt_entry_t *pmap_pde(pt_entry_t *pd, vm_offset_t addr)
{
        panic("Should not be used");
        return &pd[lin2pdenum(addr)];
}

/*
 *	Given an offset and a map, compute the address of the
 *	pte.  If the address is invalid with respect to the map
 *	then PT_ENTRY_NULL is returned (and the map may need to grow).
 *
 *	This is only used internally.
 */
pt_entry_t *pmap_pte(pmap_t pmap, vm_offset_t addr, pt_entry_t **valid_pte_out)
{
        /*
          Levels:
            PML4-E/PDP = 3
            PDP-E/PD = 2
            PD-E/PT = 1
            PT-E/PAGE = 0
        */

        pt_entry_t *dummy;
        if (valid_pte_out == NULL)
        {
                valid_pte_out = &dummy;
        }

        pt_entry_t *pte = NULL;
        if (pmap->dirbase)
        { /* We really don't need this check */
                *valid_pte_out = &pmap->dirbase[lin2pml4enum(addr)];
                if ((**valid_pte_out & INTEL_PML4E_VALID))
                {
                        *valid_pte_out = &((pt_entry_t *)pml4etokv(**valid_pte_out))[lin2pdpenum(addr)];

                        if ((**valid_pte_out & INTEL_PDPE_VALID))
                        {
                                *valid_pte_out = &((pt_entry_t *)pdpetokv(**valid_pte_out))[lin2pdenum(addr)];

                                if ((**valid_pte_out & INTEL_PDE_VALID))
                                {
                                        *valid_pte_out = &((pt_entry_t *)pdetokv(**valid_pte_out))[ptenum(addr)];
                                        pte            = *valid_pte_out;
                                }
                        }
                }
        }

        return pte;
}

static void pve_modified_callback(machine_page *, pv_entry_t e, void *ud)
{
        boolean_t *modified = (boolean_t *)(ud);

        pt_entry_t *pte = pmap_pte(e->m_pmap, e->m_vaddr, nullptr);

        KASSERT(pte != nullptr); // It really should be here

        *modified |= !!(*pte & INTEL_PTE_MOD);
}

boolean_t pmap_vm_page_modified(vm_address_t paddr)
{
        boolean_t modified                = false;
        struct machine_page *machine_page = machine_page_get(paddr);
        pve_enum(machine_page, pve_modified_callback, &modified);
        return modified;
}

void pmap_flush_tlb(void)
{
        set_cr3(get_cr3());
}

static inline void pmap_invalidate_page(vm_offset_t address)
{
        asm volatile("invlpg [%0]\n" : : "r"(address) : "memory");
}

void pmap_invalidate_tlb(vm_offset_t a_start, vm_offset_t a_end)
{
        for (vm_offset_t address = a_start; address < a_end; address += PGBYTES)
        {
                pmap_invalidate_page(address);
        }
}
#if 0
/*
 *	Map memory at initialization.  The physical addresses being
 *	mapped are not managed and are never unmapped.
 *
 *	For now, VM is already on, we only need to map the
 *	specified memory.
 */
vm_offset_t pmap_map(vm_offset_t start_va, vm_offset_t start_pa, vm_offset_t end_pa, vm_prot_t prot, pt_entry_t mode)
{
        for (vm_offset_t va = start_va, pa = start_pa; pa < end_pa; va += PAGE_SIZE, pa += PAGE_SIZE)
        {
                pmap_enter(kernel_pmap, va, pa, prot, FALSE);
                pt_entry_t *pte = pmap_pte(kernel_pmap, va, NULL);
                *pte |= mode;
        }

        return (start_va);
}
#endif

vm_offset_t pmap_page_table_page_alloc();

/* Map a set of physical memory pages into the direct map
 * address space. Return a pointer to where it is mapped. This
 * routine is intended to be used for mapping device memory,
 * NOT real memory.
 */
void *pmap_mapdev_uncacheable(vm_offset_t pa, size_t size)
{
        vm_address_t start_va = trunc_page(pmap_get_dmap_addr() + pa);
        vm_address_t end_va   = round_page(start_va + size);

        for (vm_address_t va = start_va; va < end_va; va += PAGE_SIZE)
        {
                pt_entry_t *valid_pte = nullptr, *pte = nullptr;

                while (!(pte = pmap_pte(kernel_pmap, va, &valid_pte)))
                {
                        *valid_pte = pmap_page_table_page_alloc() | INTEL_PTE_VALID | INTEL_PTE_WRITE;
                }

                *pte = (va - pmap_get_dmap_addr()) | INTEL_PTE_NCACHE | INTEL_PTE_WRITE | INTEL_PTE_VALID;

                pmap_invalidate_page(va);
        }

        return (void *)start_va;
}

static bool bootstrap_map_page(vm_address_t pml4_pa, vm_address_t vaddr, vm_address_t *paddr)
{
        pt_entry_t *pml4 = (pt_entry_t *)early_boot_map_page(pml4_pa);

        /* Get the entry for the page directory pointer table */
        pt_entry_t *pml4e = &pml4[lin2pml4enum(vaddr)];

        /* If the PML4 entry for page directory pointer table is not valid for
         * va_current, grab some storage for it */
        if (((*pml4e) & INTEL_PML4E_VALID) == 0)
        {
                /* Entry not valid, we need to zero the new physical page befor usage.
                Unlike qemu, real hardware does not return cleared pages */
                vm_offset_t phys_page_pa = pmap_grab_page();
                void *phys_page_va       = (void *)early_boot_map_page(phys_page_pa);
                bzero(phys_page_va, PAGE_SIZE);
                *pml4e = phys_page_pa | INTEL_PML4E_VALID | INTEL_PML4E_WRITE;
        }

        /* Map the page directory pointer table */
        pt_entry_t *pdp = (pt_entry_t *)early_boot_map_page((*pml4e) & INTEL_PML4E_PFN);

        /* Get the page directory pointer entry for va_current */
        pt_entry_t *pdpe = &pdp[lin2pdpenum(vaddr)];

        if (((*pdpe) & INTEL_PDPE_VALID) == 0)
        {
                vm_offset_t phys_page_pa = pmap_grab_page();
                void *phys_page_va       = (void *)early_boot_map_page(phys_page_pa);
                bzero(phys_page_va, PAGE_SIZE);

                *pdpe = phys_page_pa | INTEL_PDPE_VALID | INTEL_PDPE_WRITE;
        }

        /* Map the page directory */
        pt_entry_t *pd = (pt_entry_t *)early_boot_map_page((*pdpe) & INTEL_PDPE_PFN);

        pt_entry_t *pde = &pd[lin2pdenum(vaddr)];

        if (((*pde) & INTEL_PDE_VALID) == 0)
        {
                vm_offset_t phys_page_pa = pmap_grab_page();
                void *phys_page_va       = (void *)early_boot_map_page(phys_page_pa);
                bzero(phys_page_va, PAGE_SIZE);

                *pde = phys_page_pa | INTEL_PDE_VALID | INTEL_PDE_WRITE;
        }

        /* Map the page table */
        pt_entry_t *pt = (pt_entry_t *)early_boot_map_page((*pde) & INTEL_PDE_PFN);

        /* Get the page table entry for va_current */
        pt_entry_t *pte = &pt[ptenum(vaddr)];

        if (*pte & INTEL_PTE_VALID)
        {
                /* Page already mapped */
                *paddr = *pte & INTEL_PTE_PFN;
                return false;
        }

        bool new_mapping = false;
        /* Enter a 4Kbyte page */
        if (*paddr == 0)
        {
                *paddr      = pmap_grab_page();
                new_mapping = true;
        }

        *pte = (*paddr | INTEL_PTE_VALID | INTEL_PTE_GLOBAL | INTEL_PTE_WRITE);

        return new_mapping;
}

struct vm_page_arr_map_ctx
{
        vm_offset_t pml4_pa;
        size_t page_count;
        vm_address_t vm_page_array_end; // inclusive!!!!
};

static boolean_t map_vm_page_array_page(vm_address_t phys_addr, vm_address_t region_start, vm_address_t region_end, void *user_data)
{
        struct vm_page_arr_map_ctx *data = (struct vm_page_arr_map_ctx *)user_data;
        machine_page *page               = machine_page_get(phys_addr);

        vm_address_t vm_page_array_page_addr = trunc_page(page);

        vm_address_t p = 0;
        bool new_map   = bootstrap_map_page(data->pml4_pa, vm_page_array_page_addr, &p);

        data->page_count++;

        data->vm_page_array_end = etl::max(data->vm_page_array_end, vm_page_array_page_addr);

        if (new_map)
        {
                // vm_page::buckets = emerixx::add_ptr<vm_page::bucket_t>((vm_page::bucket_t *)vm_page_array_page_addr, PAGE_SIZE);
        }

        return TRUE;
}

extern struct multiboot_tag_framebuffer g_mbfb;

/* Directly map a region */
struct dmap_region_ctx
{
        vm_address_t pml4_pa;
        vm_address_t ignore_below_pa;
        vm_size_t huge_page_mapped;
        boolean_t cpu_1gb_page_support;
};
static boolean_t dmap_region(vm_address_t region_addr, vm_address_t region_start, vm_address_t region_end, void *user_data)
{
        struct dmap_region_ctx *ctx = (struct dmap_region_ctx *)user_data;

        /* Should we ignore a range */
        if (region_addr < ctx->ignore_below_pa)
        {
                return TRUE;
        }

        /* If we have mapped a huge page skip the mapped size */
        if (ctx->huge_page_mapped)
        {
                ctx->huge_page_mapped -= PGBYTES;
                return TRUE;
        }

        /* Start the mapping process */
        vm_offset_t dmap_va = dmap_address + region_addr;

        /* Map the PML4 table */
        pt_entry_t *pml4 = (pt_entry_t *)early_boot_map_page(ctx->pml4_pa);

        /* Get the entry for the page directory pointer table */
        pt_entry_t *pml4e = &pml4[lin2pml4enum(dmap_va)];

        /* If the PML4 entry for page directory pointer table is not valid for
         * va_current, grab some storage for it */
        if (((*pml4e) & INTEL_PML4E_VALID) == 0)
        {
                /* XXX Enable no execute when it's enabled in the EFFER MSR */
                /* Entry not valid, we need to zero the new physical page befor usage.
                Unlike qemu, real hardware does not return cleared pages */
                vm_offset_t phys_page_pa = pmap_grab_page();
                void *phys_page_va       = (void *)early_boot_map_page(phys_page_pa);
                bzero(phys_page_va, PAGE_SIZE);

                *pml4e = phys_page_pa | INTEL_PML4E_VALID | INTEL_PML4E_WRITE /* | INTEL_PML4E_NO_EXECUTE */;
        }

        /* Map the page directory pointer table */
        pt_entry_t *pdp = (pt_entry_t *)early_boot_map_page((*pml4e) & INTEL_PML4E_PFN);

        /* Get the page directory pointer entry for va_current */
        pt_entry_t *pdpe = &pdp[lin2pdpenum(dmap_va)];

        if (ctx->cpu_1gb_page_support &&                 /* Do we support 1gb pages */
            is_1gb_aligned(region_addr)                  /*Is the region adress 1gb aligned? */
            && (region_addr + 0x40000000) <= region_end) /* Does it fit? */
        {
                *pdpe                 = (region_addr | INTEL_PDPE_VALID | INTEL_PDPE_1GBYTE_PAGE | INTEL_PDPE_1GBYTE_PAGE_GLOBAL
                         | INTEL_PDPE_WRITE);
                ctx->huge_page_mapped = 0x40000000 - PGBYTES;
                return TRUE;
        }

        /* No success mapping a 1gbyte page. Allocate a page directory if needed
         */
        if (((*pdpe) & INTEL_PDPE_VALID) == 0)
        {
                /* Entry not valid, we need to zero the new physical page before usage.
                    Unlike qemu, real hardware does not return cleared pages */
                vm_offset_t phys_page_pa = pmap_grab_page();
                void *phys_page_va       = (void *)early_boot_map_page(phys_page_pa);
                bzero(phys_page_va, PAGE_SIZE);

                *pdpe = phys_page_pa | INTEL_PDPE_VALID | INTEL_PDPE_WRITE;
        }

        /* Map the page directory */
        pt_entry_t *pd = (pt_entry_t *)early_boot_map_page((*pdpe) & INTEL_PDPE_PFN);

        pt_entry_t *pde = &pd[lin2pdenum(dmap_va)];

        /* Try to map the physical address as a 2MByte page */
        if (is_2mb_aligned(region_addr) && (region_addr + 0x200000) <= region_end)
        {
                *pde = (region_addr | INTEL_PDE_VALID | INTEL_PDE_2MBYTE_PAGE | INTEL_PDE_2MBYTE_PAGE_GLOBAL | INTEL_PDE_WRITE);
                ctx->huge_page_mapped = 0x200000 - PGBYTES;
                return TRUE;
        }

        /* No success mapping a 2Mbyte page allocate a page table if needed */
        if (((*pde) & INTEL_PDE_VALID) == 0)
        {
                /* Entry not valid, we need to zero the new physical page before usage. Unlike qemu, real hardware does not return
                 * cleared pages */
                vm_offset_t phys_page_pa = pmap_grab_page();
                void *phys_page_va       = (void *)early_boot_map_page(phys_page_pa);
                bzero(phys_page_va, PAGE_SIZE);

                *pde = phys_page_pa | INTEL_PDE_VALID | INTEL_PDE_WRITE;
        }

        /* Map the page table */
        pt_entry_t *pt = (pt_entry_t *)early_boot_map_page((*pde) & INTEL_PDE_PFN);

        /* Get the page table entry for va_current */
        pt_entry_t *pte = &pt[ptenum(dmap_va)];

        /* Enter a 4Kbyte page */
        *pte = region_addr | INTEL_PTE_VALID | INTEL_PTE_GLOBAL | INTEL_PTE_WRITE;

        return TRUE;
}

/*
 *	Bootstrap the system enough to run with virtual memory.
 *	Allocate the kernel page directory and page tables,
 *	and direct-map all physical memory.
 *	Called with mapping off.
 */
void pmap_bootstrap(struct modeldep_bootdata *bd)
{
        vm_page_map_init(&s_pmap_page_map);
        lock_init(&s_pmap_page_map_lock, true);

        s_mppm.init();
        /*
         * Mapping is turned off; we must reference only physical addresses.
         * The load image of the system is to be mapped 1-1 physical = virtual.
         */

        /*
         *	Set ptes_per_vm_page for general use.
         */
#if 0
	ptes_per_vm_page = PAGE_SIZE / INTEL_PGBYTES;
#endif

        /*
         *	The kernel's pmap is statically allocated so we don't
         *	have to use pmap_create, which is unlikely to work
         *	correctly at this part of the boot sequence.
         */

        kernel_pmap = &kernel_pmap_store;

        lock_init(&pmap_system_lock, FALSE); /* NOT a sleep lock */

        simple_lock_init(&kernel_pmap->lock);

        kernel_pmap->ref_count = 1;

        phys_first_addr = bd->phys_first_addr;
        phys_last_addr  = bd->phys_last_addr;
        s_npages        = bd->phys_page_count;

        dmap_address = pml4enum2lin(0x100);

        /*
         * Create a new PM4L Table for boot pmap
         */
        vm_offset_t pml4_pa = pmap_grab_page();
        {
                struct pmap_dirbase *pml4 = (struct pmap_dirbase *)early_boot_map_page(pml4_pa);
                etl::clear(pml4->ptes);
        }

        /* Map all physical memory 1:1 starting at 0xFFFF800000000000 */
        dmap_region_ctx dmap_ctx{ pml4_pa, 0, 0, cpu_feature_page1gb() };
        /* Lets map the first meg by hand */
        /*E820 does not always describe every region */
        for (vm_offset_t cur_pa = 0; cur_pa < 0x100000; cur_pa += PGBYTES)
        {
                dmap_region(cur_pa, 0, 0x100000, &dmap_ctx);
        }

        dmap_ctx.ignore_below_pa = 0x100000;

        for (vm_offset_t cur_pa = g_mbfb.common.framebuffer_addr;
             cur_pa
             < round_page(g_mbfb.common.framebuffer_addr + g_mbfb.common.framebuffer_height * g_mbfb.common.framebuffer_pitch);
             cur_pa += PGBYTES)
        {
                dmap_region(cur_pa,
                            g_mbfb.common.framebuffer_addr,
                            round_page(g_mbfb.common.framebuffer_addr
                                       + g_mbfb.common.framebuffer_height * g_mbfb.common.framebuffer_pitch),
                            &dmap_ctx);
        }

        /* Map everthing elese */
        modeldep_iter_all_pages(dmap_region, &dmap_ctx);

        /* Kernel image is always loaded in the range -2GByte - 0, we can add
         * 0x80000000(2GByte) to get the physical address */

        /* Since we are running on a stack allocated by the bootloader below the
         * kernel image, we have to map in a single page below the kernel image to use
         * as a stack until we setup a new one.
         *
         * We map 3 pages:
         *  kernel_image_begin_va                 : interrupt stack
         *  kernel_image_begin_va - PAGE_SIZE     : nmi
         *  kernel_image_begin_va - PAGE_SIZE * 2 : mc
         *  */

        for (vm_offset_t va_current = bd->kernel_image_va_addr_start - (PGBYTES * 3); va_current < bd->kernel_image_va_addr_end;
             va_current += PAGE_SIZE)
        {
                vm_address_t kern_img_pa = va_current + 0x80000000;
                bootstrap_map_page(pml4_pa, va_current, &kern_img_pa);
        }

        size_t phys_page_count        = 0;
        vm_page_io_descr_t vmp_io_arr = nullptr;
        {
                pt_entry_t *pml4 = (pt_entry_t *)early_boot_map_page(pml4_pa);

                // Find the next available PML4E after physcal 1:1 mapping
                for (size_t i = 0x100; i < NPML4ES; ++i)
                {
                        if ((pml4[i] & INTEL_PML4E_VALID) == 0)
                        {
                                // Use 1 PMLE for the vm_page array and vm_page buckets(excessive I
                                // know)

                                s_machine_page_array = (machine_page *)pml4enum2lin(i);
                                struct vm_page_arr_map_ctx data
                                {
                                        pml4_pa, 0, 0
                                };

                                modeldep_iter_avail_pages(map_vm_page_array_page, &data);

                                // Map IO descriptors for all free pages
                                vm_address_t vm_page_io_descr_arr_start = data.vm_page_array_end + PAGE_SIZE;
                                vm_address_t vm_page_io_descr_arr_end
                                        = vm_page_io_descr_arr_start
                                        + round_page((sizeof(struct vm_page_io_descr) * data.page_count));
                                for (vm_address_t cur = vm_page_io_descr_arr_start; cur < vm_page_io_descr_arr_end;
                                     cur += PAGE_SIZE)
                                {
                                        vm_address_t p = 0;
                                        bootstrap_map_page(pml4_pa, cur, &p);
                                }

                                /*Use the next PML4E for the kernel virtual space, this gives us 512 GiB
                                 * to work with */
                                kernel_virtual_start = pml4enum2lin(i + 1);
                                kernel_virtual_end   = pml4enum2lin(i + 2);

                                phys_page_count = data.page_count;

                                vmp_io_arr = (vm_page_io_descr_t)vm_page_io_descr_arr_start;
                                break;
                        }
                }
        }

        /* Active the new page table. */
        set_cr3(pml4_pa);

        kernel_pmap->dirbase = kernel_page_dir = (pt_entry_t *)(pml4enum2lin(0x100) + pml4_pa);

// Initialize page array
#ifdef ENABLE_MEM_POISON
        bd->early_poison = 0;
#endif
        machine_page_bootstrap(phys_page_count, vmp_io_arr);
}

/*
 *	Initialize the pmap module.
 *	Called by vm_init, to initialize any structures that the pmap
 *	system needs to map virtual memory.
 */
void pmap_init(struct modeldep_bootdata *bd)
{
        /*
         *	Create the zone of physical maps,
         *	and of the physical-to-virtual entries.
         */
        pmap_zone_t::init(&pmap_zone, "pmap");

        pmap_dirbase_zone_t::init(&s_dirbase_zone, "pmap dirbase");

        /*
         *	Set up the pmap request lists
         */
        for (int i = 0; i < NCPUS; i++)
        {
                pmap_update_list_t up = &cpu_update_list[i];

                simple_lock_init(&up->lock);
                up->count = 0;
        }
}

vm_address_t pmap_get_dmap_addr()
{
        return dmap_address;
}

boolean_t pmap_is_valid_paddr(vm_address_t paddr)
{
        return paddr >= (phys_first_addr && paddr < phys_last_addr) || s_mppm.find(paddr) != nullptr;
}

vm_size_t pmap_free_pages()
{
        return s_npages;
}

#define valid_page(x) (/*pmap_initialized &&*/ pmap_is_valid_paddr(x))

/*
 *	Routine:	pmap_page_table_page_alloc
 *
 *	Allocates a new physical page to be used as a page-table page.
 *
 *	Must be called with the pmap system and the pmap unlocked,
 *	since these must be unlocked to use vm_page_grab.
 */
vm_offset_t pmap_page_table_page_alloc()
{

        /*
         *	Allocate a VM page for the level 2 page table entries.
         */

        spl_t s = splimp();
        simple_lock(&s_vm_page_queue_free_lock);
        while (queue_empty(&s_freeq) == true)
        {
                assert_wait(&cnt.v_free_count, false);
                simple_unlock(&s_vm_page_queue_free_lock);
                thread_block(nullptr);
        }
        machine_page *mpp = (machine_page *)queue_containing_record(dequeue_head(&s_freeq), vm_page, m_qlink);
        simple_unlock(&s_vm_page_queue_free_lock);
        splx(s);

        /*
         *	Map the page to its physical address so that it
         *	can be found later.
         */
        vm_offset_t pa = mpp->m_vm_page.phys_addr;
        lock_write(&s_pmap_page_map_lock);
        vm_page_map_insert(&s_pmap_page_map, &mpp->m_vm_page, pa);
        mpp->m_vm_page.wire();
        inuse_ptepages_count++;
        cnt.v_free_count--;
        lock_write_done(&s_pmap_page_map_lock);

        /*
         *	Zero the page.
         */
        bzero((void *)phystokv(pa), PAGE_SIZE);

#if i860
        /*
         *	Mark the page table page(s) non-cacheable.
         */
        {
                int i = ptes_per_vm_page;
                pt_entry_t *pdp;

                panic("DAMIR <- CHeck this, pmap_pte is wrong");
                pdp = pmap_pte(kernel_pmap, pa);
                do
                {
                        *pdp |= INTEL_PTE_NCACHE;
                        pdp++;
                }
                while (--i > 0);
        }
#endif
        return pa;
}

/*
 *	Deallocate a page-table page.
 *	The page-table page must have all mappings removed,
 *	and be removed from its page directory.
 */
static void pmap_page_table_page_dealloc(vm_offset_t pa)
{
        lock_write(&s_pmap_page_map_lock);
        vm_page_t m = vm_page_map_lookup(&s_pmap_page_map, pa);
        vm_page_free(&s_pmap_page_map, m);
        inuse_ptepages_count--;
        lock_write_done(&s_pmap_page_map_lock);
}

/*
 *	Create and return a physical map.
 *
 *	If the size specified for the map
 *	is zero, the map is an actual physical
 *	map, and may be referenced by the
 *	hardware.
 *
 *	If the size specified is non-zero,
 *	the map will be used in software only, and
 *	is bounded by that size.
 */
pmap_t pmap_create(vm_size_t size)
{
        pmap_t p;
        pmap_statistics_t stats;

        /*
         *	A software use-only map doesn't even need a map.
         */

        if (size != 0)
        {
                return nullptr;
        }

        /*
         *	Allocate a pmap struct from the pmap_zone.  Then allocate
         *	the page descriptor table from the pd_zone.
         */

        // p = (pmap_t)zalloc(pmap_zone);
        p = pmap_zone.alloc();

        if (p == nullptr)
        {
                panic("pmap_create");
        }

        // if (kmem_alloc_wired(kernel_map, (vm_offset_t *)&p->dirbase, INTEL_PGBYTES)
        //    != KERN_SUCCESS)
        //  panic("pmap_create");

        p->dirbase = s_dirbase_zone.alloc()->ptes;

        bcopy(kernel_page_dir, p->dirbase, PAGE_SIZE);

        // vm_address_t phy = kvtophys((vm_offset_t)p->dirbase);

        p->ref_count = 1;

        simple_lock_init(&p->lock);
        p->cpus_using = 0;

        /*
         *	Initialize statistics.
         */

        stats                 = &p->stats;
        stats->resident_count = 0;
        stats->wired_count    = 0;

        return (p);
}

// Free all ptes recursively
static void pmap_free_ptes(pt_entry_t *pte_base, int level)
{
        // We should really only check up to level 3(PDP) as the ptes(lvl 4) should
        // all be freed by this point. We keep checking all 4 levels for debugging
        // purposes
        if (level < 4)
        {
                for (uint32_t i = 0; i < NPTES; ++i)
                {
                        if (pte_base[i] & INTEL_PTE_VALID)
                        {
                                if (level == 3)
                                {
                                        log_error("All ptes(lvl 4) should be freed before pmap descruction");
                                        continue;
                                }
                                vm_offset_t pa = pte_to_pa(pte_base[i]);

                                pmap_free_ptes((pt_entry_t *)(dmap_address + pa), level + 1);

                                lock_write(&s_pmap_page_map_lock);
                                vm_page_t m = vm_page_map_lookup(&s_pmap_page_map, pa);

                                if (m == nullptr)
                                {
                                        panic("pmap_destroy: pte page not in object");
                                }

                                vm_page_free(&s_pmap_page_map, m);

                                inuse_ptepages_count--;

                                lock_write_done(&s_pmap_page_map_lock);
                        }
                }
        }
}

/*
 *	Retire the given physical map from service.
 *	Should only be called if the map contains
 *	no valid mappings.
 */

void pmap_destroy(pmap_t p)
{
        // pt_entry_t *pdep;
        // vm_offset_t pa;
        // vm_page_t m;

        if (p == nullptr)
        {
                return;
        }

        spl_t s;
        SPLVM(s);
        simple_lock(&p->lock);
        integer_t c = --p->ref_count;
        simple_unlock(&p->lock);
        SPLX(s);

        if (c != 0)
        {
                return; /* still in use */
        }

        /*
         *	Free the memory maps, then the
         *	pmap structure.
         */
#if 0
  for (pdep = p->dirbase;
       pdep < &p->dirbase[lin2pdenum(LINEAR_MIN_KERNEL_ADDRESS)];
       pdep += ptes_per_vm_page) {
    if (*pdep & INTEL_PTE_VALID) {
      pa = pte_to_pa(*pdep);
      pmap_object->lock();
      m = vm_page_lookup(pmap_object, pa);
      if (m == VM_PAGE_NULL)
        panic("pmap_destroy: pte page not in object");
      vm_page_lock_queues();
      vm_page_free(m);
      inuse_ptepages_count--;
      vm_page_unlock_queues();
      pmap_object->unlock();
    }
  }
#endif

        // Zero out the kernel PML4E's so we don't unmap kernel pages
        bzero(&p->dirbase[NPML4ES / 2], sizeof(pt_entry_t) * (NPML4ES / 2));
        // Free all ptes
        pmap_free_ptes(p->dirbase, 0);
        // Free the PML4
        // kmem_free(kernel_map, (vm_offset_t)p->dirbase, INTEL_PGBYTES);
        s_dirbase_zone.free((pmap_dirbase *)p->dirbase);

        // Free the pmap itself
        // zfree(pmap_zone, (vm_offset_t)p);
        pmap_zone.free(p);
}

/*
 *	Add a reference to the specified pmap.
 */

void pmap_reference(pmap_t p)
{
        int s;
        if (p != nullptr)
        {
                SPLVM(s);
                simple_lock(&p->lock);
                p->ref_count++;
                simple_unlock(&p->lock);
                SPLX(s);
        }
}

/*
 *	Remove a range of hardware page-table entries.
 *	The entries given are the first (inclusive)
 *	and last (exclusive) entries for the VM pages.
 *	The virtual address is the va for the first pte.
 *
 *	The pmap must be locked.
 *	If the pmap is not the kernel pmap, the range must lie
 *	entirely within one pte-page.  This is NOT checked.
 *	Assumes that the pte-page exists.
 */

void pmap_remove_range(pmap_t pmap, vm_offset_t va, pt_entry_t *spte, pt_entry_t *epte)
{
        int num_removed, num_unwired;
        vm_offset_t pa;

#if DEBUG_PTE_PAGE
        if (pmap != kernel_pmap)
        {
                ptep_check(get_pte_page(spte));
        }
#endif
        num_removed = 0;
        num_unwired = 0;

        for (pt_entry_t *cpte = spte; cpte < epte; cpte++, va += PAGE_SIZE)
        {
                if (*cpte == 0)
                {
                        continue;
                }

                pa = pte_to_pa(*cpte);

                num_removed++;

                if (*cpte & INTEL_PTE_WIRED)
                {
                        num_unwired++;
                }

                if (!valid_page(pa))
                {
                        /*
                         *	Outside range of managed physical memory.
                         *	Just remove the mappings.
                         */
                        pt_entry_t *lpte = cpte;
                        *lpte            = 0;
                        lpte++;

                        pmap_invalidate_page(va); // ??? SMP
                        continue;
                }

                machine_page *m = machine_page_get(pa);

                SIMPLE_LOCK_SCOPE(&m->m_slock);

                /*
                 *	Get the modify and reference bits.
                 */
                pt_entry_t *lpte = cpte;

                m->m_pmap_attrubutes = ((*lpte) & (INTEL_PTE_MOD | INTEL_PTE_REF));
                *lpte++              = 0;
                pmap_invalidate_page(va); // ??? SMP

                /*
                 *	Remove the mapping from the pvlist for this physical page.
                 */
                pv_entry_t pve = pve_find(m, pmap, va);

                KASSERT(pve != nullptr);

                pve_free(m, pve);
        }

        /*
         *	Update the counts
         */
        pmap->stats.resident_count -= num_removed;
        pmap->stats.wired_count -= num_unwired;
}

/*
 *	Remove the given range of addresses
 *	from the specified map.
 *
 *	It is assumed that the start and end are properly
 *	rounded to the hardware page size.
 */

void pmap_remove(pmap_t a_map, vm_offset_t a_start, vm_offset_t a_end)
{
        if (a_map == nullptr)
        {
                return;
        }

        PMAP_LOCK(a_map);

        /*
         *	Invalidate the translation buffer first
         */
        pmap_update_tlbs(a_map, a_start, a_end);

        // pde = pmap_pde(map, start);

        for (vm_offset_t va = a_start; va < a_end;)
        {
                pt_entry_t *pml4e = &a_map->dirbase[lin2pml4enum(va)];

                if ((*pml4e & INTEL_PML4E_VALID) == 0)
                {
                        va = ROUNDDOWN(va, PML4E_MAPPED_SIZE) + PML4E_MAPPED_SIZE;
                        continue;
                }

                pt_entry_t *pdpe = &((pt_entry_t *)pml4etokv(*pml4e))[lin2pdpenum(va)];

                if ((*pdpe & INTEL_PDPE_VALID) == 0)
                {
                        va = ROUNDDOWN(va, PDPE_MAPPED_SIZE) + PDPE_MAPPED_SIZE;
                        continue;
                }

                pt_entry_t *pde = &((pt_entry_t *)pdpetokv(*pdpe))[lin2pdenum(va)];

                if ((*pde & INTEL_PDE_VALID) == 0)
                {
                        va = ROUNDDOWN(va, PDE_MAPPED_SIZE) + PDE_MAPPED_SIZE;
                        continue;
                }
                pt_entry_t *spte, *epte;
                vm_offset_t l;

                l = etl::min(((va + PDE_MAPPED_SIZE) & ~(PDE_MAPPED_SIZE - 1)), a_end);

                spte = &((pt_entry_t *)pdetokv(*pde))[ptenum(va)];
                epte = (&((pt_entry_t *)pdetokv(*pde))[ptenum(l - 1)]) + 1;
                pmap_remove_range(a_map, va, spte, epte);
                va = l;
        }
}

/*
 *	Routine:	pmap_page_protect
 *
 *	Function:
 *		Lower the permission for all mappings to a given
 *		page.
 */

static void pmap_page_protect_cb(machine_page *m, pv_entry_t pve, void *ud)
{
        pmap_t pmap        = pve->m_pmap;
        vm_address_t vaddr = pve->m_vaddr;
        bool remove        = *((bool *)(ud));

        pt_entry_t *pte = pmap_pte(pmap, vaddr, nullptr);

        KASSERT(pte);

        /*
         * Consistency checks.
         */
        KASSERT(*pte & INTEL_PTE_VALID);
        // KASSERT(pte_to_phys(*pte) == phys);

        /*
         * Remove the mapping if new protection is NONE or if write-protecting a kernel mapping.
         */
        if (remove || (pve->m_pmap == kernel_pmap))
        {
                /*
                 * Remove the mapping, collecting any modify bits.
                 */
                if (*pte & INTEL_PTE_WIRED)
                {
                        if (pve->m_pmap == kernel_pmap)
                        {
                                return;
                        }
                        // panic("pmap_remove_all removing a wired page from kernel map");
                        else
                        {
                                panic("pmap_remove_all removing a wired page from user map");
                        }
                }

                m->m_pmap_attrubutes = (*pte) & (INTEL_PTE_MOD | INTEL_PTE_REF);

                *pte++ = 0;

                pve->m_pmap->stats.resident_count--;

                /*
                 * Remove the pv_entry.
                 */
                pve_free(m, pve);
        }
        else
        {
                /*
                 * Write-protect.
                 */
                *pte &= ~INTEL_PTE_WRITE;
        }

        pmap_update_tlbs(pmap, vaddr, vaddr + PAGE_SIZE);

        pve_touch(pve);
}

void pmap_page_protect(vm_offset_t phys, vm_prot_t prot)
{
        // KASSERT(phys != vm_page_fictitious_addr);
        if (!valid_page(phys))
        {
                /*
                 *	Not a managed page.
                 */
                return;
        }

        /*
         * Determine the new protection.
         */
        boolean_t remove = FALSE;
        switch (prot)
        {
                case VM_PROT_READ:
                case VM_PROT_READ | VM_PROT_EXECUTE:
                        break;
                case VM_PROT_ALL:
                        return; /* nothing to do */
                default:
                        remove = TRUE;
                        break;
        }

        /*
         *	Lock the pmap system first, since we will be changing
         *	several pmaps.
         */
        PMAP_SYS_LOCK();

        machine_page *m = machine_page_get(phys);
        /*
         * Walk down PV list, changing or removing all mappings.
         * We do not have to lock the pv_list because we have
         * the entire pmap system locked.
         */

        /*
         * Lock the pmap to block pmap_extract and similar routines.
         */
        SIMPLE_LOCK_SCOPE(&m->m_slock);

        pve_enum(m, pmap_page_protect_cb, &remove);
}

/*
 *	Insert the given physical page (p) at
 *	the specified virtual address (v) in the
 *	target physical map with the protection requested.
 *
 *	If specified, the page will be wired down, meaning
 *	that the related pte can not be reclaimed.
 *
 *	NB:  This is the only routine which MAY NOT lazy-evaluate
 *	or lose information.  That is, this routine must actually
 *	insert this page into the given map NOW.
 */
void pmap_enter(pmap_t pmap, vm_offset_t v, vm_page_t a_vmp, vm_prot_t prot, boolean_t wired)
{
        // KASSERT(pa != vm_page_fictitious_addr);

        if (pmap == nullptr)
        {
                return;
        }

        if (pmap == kernel_pmap && (prot & VM_PROT_WRITE) == 0 && !wired /* hack for io_wire */)
        {
                /*
                 *	Because the 386 ignores write protection in kernel mode,
                 *	we cannot enter a read-only kernel mapping, and must
                 *	remove an existing mapping if changing it.
                 *
                 *  XXX should be #if'd for i386
                 */
                PMAP_LOCK(pmap);

                pt_entry_t *last_valid_pte;
                pt_entry_t *pte = pmap_pte(pmap, v, &last_valid_pte);
                if (pte != PT_ENTRY_NULL && *pte != 0)
                {
                        /*
                         *	Invalidate the translation buffer,
                         *	then remove the mapping.
                         */
                        pmap_update_tlbs(pmap, v, v + PAGE_SIZE);
                        pmap_remove_range(pmap, v, pte, pte + 1);
                }
                return;
        }

        /*
         *	Must allocate a new pvlist entry while we're unlocked;
         *	zalloc may cause pageout (which will lock the pmap system).
         *	If we determine we need a pvlist entry, we will unlock
         *	and allocate one.  Then we will retry, throughing away
         *	the allocated entry later (if we no longer need it).
         */
        machine_page *m = (machine_page *)(a_vmp);
        vm_address_t pa = a_vmp->phys_addr;

        // We don't start doing any real work before we have a spare pt
        vm_address_t spare_pt = 0;
        for (bool done = false; !done;)
        { // Retry

                // Grab a page in case we need it in order to enter the adress,
                // We can only do this if the pmap is unlocked
                if (!spare_pt)
                {
                        spare_pt = pmap_page_table_page_alloc();
                }

                // Lock the pmap and try to get the pte for the virtual adress
                // If this fails we use the spare, and repeat
                PMAP_LOCK(pmap);

                pt_entry_t *last_valid_pte;
                pt_entry_t *pte = pmap_pte(pmap, v, &last_valid_pte);

                // No pte path for virtual adress, use the spare and repeat
                if (!pte)
                {
                        /*
                         * Enter the new page table page in the page directory.
                         */
                        *last_valid_pte = spare_pt | INTEL_PTE_VALID | INTEL_PTE_WRITE | INTEL_PTE_USER;

                        /*
                         * Flush the data cache.
                         */
                        // flush();

                        // Repeat
                        spare_pt = 0;
                        continue;
                }

                /*
                 *	Special case if the physical page is already mapped
                 *	at this address.
                 */
                vm_offset_t old_pa     = pte_to_pa(*pte);
                pt_entry_t pt_template = 0;
                if (*pte && old_pa == pa)
                {
                        /*
                         *	May be changing its wired attribute or protection
                         */

                        if (wired && !(*pte & INTEL_PTE_WIRED))
                        {
                                pmap->stats.wired_count++;
                        }
                        else if (!wired && (*pte & INTEL_PTE_WIRED))
                        {
                                pmap->stats.wired_count--;
                        }

                        pt_template = pa_to_pte(pa) | INTEL_PTE_VALID;

                        if (pmap != kernel_pmap)
                        {
                                pt_template |= INTEL_PTE_USER;
                        }

                        if (prot & VM_PROT_WRITE)
                        {
                                pt_template |= INTEL_PTE_WRITE;
                        }

                        if (wired)
                        {
                                pt_template |= INTEL_PTE_WIRED;
                        }

                        pmap_update_tlbs(pmap, v, v + PAGE_SIZE);

                        if (*pte & INTEL_PTE_MOD)
                        {
                                pt_template |= INTEL_PTE_MOD;
                        }

                        WRITE_PTE(pte, pt_template)

                        pmap_invalidate_page(v);

                        pte++;

                        pte_increment_pa(pt_template);
                }
                else
                {
                        /*
                         *	Remove old mapping from the PV list if necessary.
                         */
                        if (*pte)
                        {
                                /*
                                 *	Invalidate the translation buffer,
                                 *	then remove the mapping.
                                 */
                                pmap_update_tlbs(pmap, v, v + PAGE_SIZE);

                                /*
                                 *	Don't free the pte page if removing last
                                 *	mapping - we will immediately replace it.
                                 */
                                pmap_remove_range(pmap, v, pte, pte + 1);
                        }

                        if (valid_page(pa))
                        {
                                SIMPLE_LOCK_SCOPE(&m->m_slock);
                                pve_alloc(m, pmap, v);
                        }

                        /*
                         *	And count the mapping.
                         */
                        pmap->stats.resident_count++;

                        if (wired)
                        {
                                pmap->stats.wired_count++;
                        }
                        /*
                         *	Build a pt_template to speed up entering -
                         *	only the pfn changes.
                         */
                        pt_template = pa_to_pte(pa) | INTEL_PTE_VALID;
                        if (pmap != kernel_pmap)
                        {
                                pt_template |= INTEL_PTE_USER;
                        }
                        if (prot & VM_PROT_WRITE)
                        {
                                pt_template |= INTEL_PTE_WRITE;
                        }
                        if (wired)
                        {
                                pt_template |= INTEL_PTE_WIRED;
                        }

                        WRITE_PTE(pte, pt_template)
                        pmap_invalidate_page(v);
                        pte++;
                        pte_increment_pa(pt_template);
                }

                done = true;
        }

        // Free the spare we allocated
        pmap_page_table_page_dealloc(spare_pt);
}

/*
 *	Routine:	pmap_change_wiring
 *	Function:	Change the wiring attribute for a map/virtual-address
 *			pair.
 *	In/out conditions:
 *			The mapping must already exist in the pmap.
 */
void pmap_change_wiring(pmap_t map, vm_offset_t v, boolean_t wired)
{
        /*
         *	We must grab the pmap system lock because we may
         *	change a pte_page queue.
         */
        PMAP_LOCK(map);

        pt_entry_t *pte = pmap_pte(map, v, nullptr);

        KASSERT(pte); // pmap_change_wiring: pte missing

        if (wired && !(*pte & INTEL_PTE_WIRED))
        {
                /*
                 *	wiring down mapping
                 */
                map->stats.wired_count++;
                *pte++ |= INTEL_PTE_WIRED;
        }
        else if (!wired && (*pte & INTEL_PTE_WIRED))
        {
                /*
                 *	unwiring mapping
                 */
                map->stats.wired_count--;
                *pte &= ~INTEL_PTE_WIRED;
        }
}

/*
 *	Routine:	pmap_extract
 *	Function:
 *		Extract the physical page address associated
 *		with the given map/virtual_address pair.
 */

vm_offset_t pmap_extract(pmap_t pmap, vm_offset_t va)
{
        pt_entry_t *pte;
        pt_entry_t *last_valid_pte;
        vm_offset_t pa;
        int spl;

        SPLVM(spl);
        simple_lock(&pmap->lock);
        if ((pte = pmap_pte(pmap, va, &last_valid_pte)) == PT_ENTRY_NULL)
        {
                pa = (vm_offset_t)0;
        }
        else if (!(*pte & INTEL_PTE_VALID))
        {
                pa = (vm_offset_t)0;
        }
        else
        {
                pa = pte_to_pa(*pte) + (va & INTEL_OFFMASK);
        }
        simple_unlock(&pmap->lock);
        SPLX(spl);
        return (pa);
}

/*
 *	Copy the range specified by src_addr/len
 *	from the source map to the range dst_addr/len
 *	in the destination map.
 *
 *	This routine is only advisory and need not do anything.
 */
#if 0
void pmap_copy(dst_pmap, src_pmap, dst_addr, len, src_addr)
	pmap_t		dst_pmap;
	pmap_t		src_pmap;
	vm_offset_t	dst_addr;
	vm_size_t	len;
	vm_offset_t	src_addr;
{
#ifdef lint
	dst_pmap++; src_pmap++; dst_addr++; len++; src_addr++;
#endif
}
#endif

/*
 *	Routine:	pmap_collect
 *	Function:
 *		Garbage collects the physical map system for
 *		pages which are no longer used.
 *		Success need not be guaranteed -- that is, there
 *		may well be pages which are not referenced, but
 *		others may be collected.
 *	Usage:
 *		Called by the pageout daemon when pages are scarce.
 */
void pmap_collect(pmap_t p)
{
        panic("pmap_collect not ready for service");
        pt_entry_t *pdp, *ptp;
        pt_entry_t *eptp;
        vm_offset_t pa;
        int wired;

        if (p == nullptr)
        {
                return;
        }

        if (p == kernel_pmap)
        {
                return;
        }

        /*
         *	Garbage collect map.
         */

        PMAP_LOCK(p);
        pmap_update_tlbs(p, VM_MIN_ADDRESS, VM_MAX_ADDRESS);

        for (pdp = p->dirbase; pdp < &p->dirbase[lin2pdenum(LINEAR_MIN_KERNEL_ADDRESS)]; ++pdp)
        {
                if (*pdp & INTEL_PTE_VALID)
                {
                        pa   = pte_to_pa(*pdp);
                        ptp  = (pt_entry_t *)phystokv(pa);
                        eptp = ptp + NPTES;

                        /*
                         * If the pte page has any wired mappings, we cannot
                         * free it.
                         */
                        wired = 0;
                        {
                                pt_entry_t *ptep;
                                for (ptep = ptp; ptep < eptp; ptep++)
                                {
                                        if (*ptep & INTEL_PTE_WIRED)
                                        {
                                                wired = 1;
                                                break;
                                        }
                                }
                        }
                        if (!wired)
                        {
                                /*
                                 * Remove the virtual addresses mapped by this pte page.
                                 */
                                { /*XXX big hack*/
                                        vm_offset_t va = pdenum2lin(pdp - p->dirbase);
                                        if (p == kernel_pmap)
                                        {
                                                va = lintokv(va);
                                        }
                                        pmap_remove_range(p, va, ptp, eptp);
                                }

                                /*
                                 * Invalidate the page directory pointer.
                                 */
                                {
                                        pt_entry_t *pdep = pdp;
                                        *pdep++          = 0;
                                }

                                // PMAP_READ_UNLOCK(p, spl);

                                /*
                                 * And free the pte page itself.
                                 */
                                {
                                        ;

                                        lock_write(&s_pmap_page_map_lock);
                                        vm_page_t m = vm_page_map_lookup(&s_pmap_page_map, pa);
                                        if (m == nullptr)
                                        {
                                                panic("pmap_collect: pte page not in object");
                                        }
                                        vm_page_free(&s_pmap_page_map, m);
                                        inuse_ptepages_count--;
                                        lock_write_done(&s_pmap_page_map_lock);
                                }

                                // PMAP_READ_LOCK(p, spl);
                        }
                }
        }
}

/*
 *	Routine:	pmap_activate
 *	Function:
 *		Binds the given physical map to the given
 *		processor, and returns a hardware map description.
 */
#if 0
void pmap_activate(my_pmap, th, my_cpu)
pmap_t	my_pmap;
	thread_t	th;
	int		my_cpu;
{
	PMAP_ACTIVATE(my_pmap, th, my_cpu);
}
#endif

/*
 *	Routine:	pmap_deactivate
 *	Function:
 *		Indicates that the given physical map is no longer
 *		in use on the specified processor.  (This is a macro
 *		in pmap.h)
 */
#if 0
void pmap_deactivate(pmap, th, which_cpu)
	pmap_t		pmap;
	thread_t	th;
	int		which_cpu;
{
#ifdef lint
	pmap++; th++; which_cpu++;
#endif
	PMAP_DEACTIVATE(pmap, th, which_cpu);
}
#endif

/*
 *	Routine:	pmap_kernel
 *	Function:
 *		Returns the physical map handle for the kernel.
 */
#if 0
pmap_t pmap_kernel()
{
    	return (kernel_pmap);
}
#endif

/*
 *	pmap_zero_page zeros the specified (machine independent) page.
 *	See machine/phys.c or machine/phys.s for implementation.
 */
#if 0
pmap_zero_page(phys)
vm_offset_t	phys;
{
int	i;

	assert(phys != vm_page_fictitious_addr);
	i = PAGE_SIZE / INTEL_PGBYTES;
	phys = intel_pfn(phys);

	while (i--)
		zero_phys(phys++);
}
#endif

/*
 *	pmap_copy_page copies the specified (machine independent) page.
 *	See machine/phys.c or machine/phys.s for implementation.
 */
#if 0
pmap_copy_page(src, dst)
	vm_offset_t	src, dst;
{
	int	i;

	assert(src != vm_page_fictitious_addr);
	assert(dst != vm_page_fictitious_addr);
	i = PAGE_SIZE / INTEL_PGBYTES;

	while (i--) {
		copy_phys(intel_pfn(src), intel_pfn(dst));
		src += INTEL_PGBYTES;
		dst += INTEL_PGBYTES;
	}
}
#endif

/*
 *	Routine:	pmap_pageable
 *	Function:
 *		Make the specified pages (by pmap, offset)
 *		pageable (or not) as requested.
 *
 *		A page which is not pageable may not take
 *		a fault; therefore, its page table entry
 *		must remain valid for the duration.
 *
 *		This routine is merely advisory; pmap_enter
 *		will specify that these pages are to be wired
 *		down (or not) as appropriate.
 */
void pmap_pageable(pmap_t a_pmap, vm_offset_t a_start, vm_offset_t a_end, boolean_t a_pageable)
{
#ifdef lint
        pmap++;
        start++;
        end++;
        pageable++;
#endif
}

/*
 *	Clear specified attribute bits.
 */

static void phys_attribute_clear_cb(machine_page *m, pv_entry_t pve, void *ud)
{
        integer_t bits = (integer_t)(*((int *)ud));

        /*
         * Lock the pmap to block pmap_extract and similar routines.
         */
        SIMPLE_LOCK_SCOPE(&pve->m_pmap->lock);

        pt_entry_t *pte = pmap_pte(pve->m_pmap, pve->m_vaddr, nullptr);

        KASSERT(pte);

        /*
         * Invalidate TLBs for all CPUs using this mapping.
         */
        pmap_update_tlbs(pve->m_pmap, pve->m_vaddr, pve->m_vaddr + PAGE_SIZE);

        /*
         * Clear modify or reference bits.
         */
        *pte &= ~bits;

        pve_touch(pve);
}

static void phys_attribute_clear(vm_offset_t phys, int bits)
{
        // KASSERT(phys != vm_page_fictitious_addr);

        if (!valid_page(phys))
        {
                /*
                 *	Not a managed page.
                 */
                return;
        }

        /*
         *	Lock the pmap system first, since we will be changing
         *	several pmaps.
         */
        PMAP_SYS_LOCK();

        machine_page *m = machine_page_get(phys);

        /*
         * Walk down PV list, clearing all modify or reference bits.
         * We do not have to lock the pv_list because we have
         * the entire pmap system locked.
         */
        pve_enum(m, phys_attribute_clear_cb, &bits);

        m->m_pmap_attrubutes &= ~bits;
}

/*
 *	Check specified attribute bits.
 */
struct phys_attribute_cb_ctx
{
        integer_t bits;
        integer_t test;
};

static void phys_attribute_test_cb(machine_page *m, pv_entry_t pve, void *ud)
{
        phys_attribute_cb_ctx *ctx = (phys_attribute_cb_ctx *)(ud);

        /*
         * Lock the pmap to block pmap_extract and similar routines.
         */
        SIMPLE_LOCK_SCOPE(&pve->m_pmap->lock);

        pt_entry_t *pte = pmap_pte(pve->m_pmap, pve->m_vaddr, nullptr);

        /*
         * Consistency checks.
         */
        KASSERT(*pte & INTEL_PTE_VALID);

        pve_touch(pve);

        /*
         * Check modify or reference bits.
         */
        if (*pte & ctx->bits)
        {
                ctx->test |= TRUE;
        }
}

static boolean_t phys_attribute_test(vm_offset_t phys, int bits)
{
        // KASSERT(phys != vm_page_fictitious_addr);
        if (!valid_page(phys))
        {
                /*
                 *	Not a managed page.
                 */
                return (FALSE);
        }

        /*
         *	Lock the pmap system first, since we will be checking
         *	several pmaps.
         */

        PMAP_SYS_LOCK();

        machine_page *m = machine_page_get(phys);

        if (m->m_pmap_attrubutes & bits)
        {
                return (TRUE);
        }

        /*
         * Walk down PV list, checking all mappings.
         * We do not have to lock the pv_list because we have
         * the entire pmap system locked.
         */
        phys_attribute_cb_ctx ctx = { .bits = bits, .test = FALSE };
        pve_enum(m, phys_attribute_test_cb, &ctx);

        return ctx.test;
}

vm_address_t pmap_get_phys_addr(vm_page_t a_vm_page)
{
        return (a_vm_page)->phys_addr;
}

/*
 *	Clear the modify bits on the specified physical page.
 */

void pmap_clear_modify(vm_offset_t phys)
{
        phys_attribute_clear(phys, INTEL_PTE_MOD);
}

/*
 *	pmap_is_modified:
 *
 *	Return whether or not the specified physical page is modified
 *	by any physical maps.
 */

boolean_t pmap_is_modified(vm_offset_t phys)
{
        return (phys_attribute_test(phys, INTEL_PTE_MOD));
}

/*
 *	pmap_clear_reference:
 *
 *	Clear the reference bit on the specified physical page.
 */

void pmap_clear_reference(vm_offset_t phys)
{
        phys_attribute_clear(phys, INTEL_PTE_REF);
}

/*
 *	pmap_is_referenced:
 *
 *	Return whether or not the specified physical page is referenced
 *	by any physical maps.
 */

boolean_t pmap_is_referenced(vm_offset_t phys)
{
        return (phys_attribute_test(phys, INTEL_PTE_REF));
}

/*
 *	    TLB Coherence Code (TLB "shootdown" code)
 *
 * Threads that belong to the same task share the same address space and
 * hence share a pmap.  However, they  may run on distinct cpus and thus
 * have distinct TLBs that cache page table entries. In order to guarantee
 * the TLBs are consistent, whenever a pmap is changed, all threads that
 * are active in that pmap must have their TLB updated. To keep track of
 * this information, the set of cpus that are currently using a pmap is
 * maintained within each pmap structure (cpus_using). Pmap_activate() and
 * pmap_deactivate add and remove, respectively, a cpu from this set.
 * Since the TLBs are not addressable over the bus, each processor must
 * flush its own TLB; a processor that needs to invalidate another TLB
 * needs to interrupt the processor that owns that TLB to signal the
 * update.
 *
 * Whenever a pmap is updated, the lock on that pmap is locked, and all
 * cpus using the pmap are signaled to invalidate. All threads that need
 * to activate a pmap must wait for the lock to clear to await any updates
 * in progress before using the pmap. They must ACQUIRE the lock to add
 * their cpu to the cpus_using set. An implicit assumption made
 * throughout the TLB code is that all kernel code that runs at or higher
 * than splvm blocks out update interrupts, and that such code does not
 * touch pageable pages.
 *
 * A shootdown interrupt serves another function besides signaling a
 * processor to invalidate. The interrupt routine (pmap_update_interrupt)
 * waits for the both the pmap lock (and the kernel pmap lock) to clear,
 * preventing user code from making implicit pmap updates while the
 * sending processor is performing its update. (This could happen via a
 * user data write reference that turns on the modify bit in the page
 * table). It must wait for any kernel updates that may have started
 * concurrently with a user pmap update because the IPC code
 * changes mappings.
 * Spinning on the VALUES of the locks is sufficient (rather than
 * having to acquire the locks) because any updates that occur subsequent
 * to finding the lock unlocked will be signaled via another interrupt.
 * (This assumes the interrupt is cleared before the low level interrupt code
 * calls pmap_update_interrupt()).
 *
 * The signaling processor must wait for any implicit updates in progress
 * to terminate before continuing with its update. Thus it must wait for an
 * acknowledgement of the interrupt from each processor for which such
 * references could be made. For maintaining this information, a set
 * cpus_active is used. A cpu is in this set if and only if it can
 * use a pmap. When pmap_update_interrupt() is entered, a cpu is removed from
 * this set; when all such cpus are removed, it is safe to update.
 *
 * Before attempting to acquire the update lock on a pmap, a cpu (A) must
 * be at least at the priority of the interprocessor interrupt
 * (splip<=splvm). Otherwise, A could grab a lock and be interrupted by a
 * kernel update; it would spin forever in pmap_update_interrupt() trying
 * to acquire the user pmap lock it had already acquired. Furthermore A
 * must remove itself from cpus_active.  Otherwise, another cpu holding
 * the lock (B) could be in the process of sending an update signal to A,
 * and thus be waiting for A to remove itself from cpus_active. If A is
 * spinning on the lock at priority this will never happen and a deadlock
 * will result.
 */

/*
 *	Signal another CPU that it must flush its TLB
 */
void signal_cpus(cpu_set a_use_list, pmap_t a_pmap, vm_offset_t a_start, vm_offset_t a_end)
{
        integer_t which_cpu, j;
        pmap_update_list_t update_list_p;

        while ((which_cpu = __builtin_ffsl(a_use_list)) != 0)
        {
                which_cpu -= 1; /* convert to 0 origin */

                update_list_p = &cpu_update_list[which_cpu];
                simple_lock(&update_list_p->lock);

                j = update_list_p->count;
                if (j >= UPDATE_LIST_SIZE)
                {
                        /*
                         *	list overflowed.  Change last item to
                         *	indicate overflow.
                         */
                        update_list_p->item[UPDATE_LIST_SIZE - 1].pmap  = kernel_pmap;
                        update_list_p->item[UPDATE_LIST_SIZE - 1].start = VM_MIN_ADDRESS;
                        update_list_p->item[UPDATE_LIST_SIZE - 1].end   = VM_MAX_KERNEL_ADDRESS;
                }
                else
                {
                        update_list_p->item[j].pmap  = a_pmap;
                        update_list_p->item[j].start = a_start;
                        update_list_p->item[j].end   = a_end;
                        update_list_p->count         = j + 1;
                }
                cpu_update_needed[which_cpu] = TRUE;
                simple_unlock(&update_list_p->lock);
#ifdef DAMIR
                if ((cpus_idle & (1 << which_cpu)) == 0)
                {
                        interrupt_processor(which_cpu);
                }
#endif
                a_use_list &= ~(1 << which_cpu);
        }
}

void process_pmap_updates(pmap_t my_pmap)
{
        int my_cpu = cpu_number();
        pmap_update_list_t update_list_p;
        int j;
        pmap_t pmap;

        update_list_p = &cpu_update_list[my_cpu];
        simple_lock(&update_list_p->lock);

        for (j = 0; j < update_list_p->count; j++)
        {
                pmap = update_list_p->item[j].pmap;
                if (pmap == my_pmap || pmap == kernel_pmap)
                {
                        pmap_invalidate_tlb(update_list_p->item[j].start, update_list_p->item[j].end);
                }
        }
        update_list_p->count      = 0;
        cpu_update_needed[my_cpu] = FALSE;
        simple_unlock(&update_list_p->lock);
}

/*
 *	Interrupt routine for TBIA requested from other processor.
 */
void pmap_update_interrupt()
{
        int my_cpu;
        pmap_t my_pmap;
        int s;

        my_cpu = cpu_number();

        /*
         *	Exit now if we're idle.  We'll pick up the update request
         *	when we go active, and we must not put ourselves back in
         *	the active set because we'll never process the interrupt
         *	while we're idle (thus hanging the system).
         */
        if (cpus_idle & (1 << my_cpu))
        {
                return;
        }

        if (current_thread() == THREAD_NULL)
        {
                my_pmap = kernel_pmap;
        }
        else
        {
                my_pmap = current_pmap();
                if (!pmap_in_use(my_pmap, my_cpu))
                {
                        my_pmap = kernel_pmap;
                }
        }

        /*
         *	Raise spl to splvm (above splip) to block out pmap_extract
         *	from IO code (which would put this cpu back in the active
         *	set).
         */
        s = splvm();

        do
        {
                /*
                 *	Indicate that we're not using either user or kernel
                 *	pmap.
                 */
                i_bit_clear(my_cpu, &cpus_active);

                /*
                 *	Wait for any pmap updates in progress, on either user
                 *	or kernel pmap.
                 */
                while (*(volatile int *)&my_pmap->lock.lock_data || *(volatile int *)&kernel_pmap->lock.lock_data)
                {
                        continue;
                }

                process_pmap_updates(my_pmap);

                i_bit_set(my_cpu, &cpus_active);
        }
        while (cpu_update_needed[my_cpu]);

        splx(s);
}

#if 0
/* Unmap page 0 to trap NULL references.  */
static void pmap_unmap_page_zero()
{
        pt_entry_t *pte;
        pt_entry_t *last_valid_pte;
        pte = pmap_pte(kernel_pmap, 0, &last_valid_pte);
        KASSERT(pte);
        *pte = 0;
        asm volatile("mov rax, cr3; mov cr3, rax" ::: "rax");
}
#endif
