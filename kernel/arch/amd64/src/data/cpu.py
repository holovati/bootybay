
{
    # AMD64 Saved state
    "saved_state": [
        {
            'ctype': 'natural_t',
            'name': 'rax'
        },
        {
            'ctype': 'natural_t',
            'name': 'rbx'
        },
        {
            'ctype': 'natural_t',
            'name': 'rcx'
        },
        {
            'ctype': 'natural_t',
            'name': 'rdx'
        },
        {
            'ctype': 'natural_t',
            'name': 'rsi'
        },
        {
            'ctype': 'natural_t',
            'name': 'rdi'
        },
        {
            'ctype': 'natural_t',
            'name': 'rbp'
        },
        {
            'ctype': 'natural_t',
            'name': 'r8'
        },
        {
            'ctype': 'natural_t',
            'name': 'r9'
        },
        {
            'ctype': 'natural_t',
            'name': 'r10'
        },
        {
            'ctype': 'natural_t',
            'name': 'r11'
        },
        {
            'ctype': 'natural_t',
            'name': 'r12'
        },
        {
            'ctype': 'natural_t',
            'name': 'r13'
        },
        {
            'ctype': 'natural_t',
            'name': 'r14'
        },
        {
            'ctype': 'natural_t',
            'name': 'r15'
        },
        {
            'ctype': 'natural_t',
            'name': 'trapnum'
        },
        {
            'ctype': 'natural_t',
            'name': 'error_code'
        },
        {
            'ctype': 'natural_t',
            'name': 'return_rip'
        },
        {
            'ctype': 'natural_t',
            'name': 'return_cs'
        },
        {
            'ctype': 'natural_t',
            'name': 'return_rflags'

        },
        {
            'ctype': 'natural_t',
            'name': 'return_rsp'

        },
        {
            'ctype': 'natural_t',
            'name': 'return_ss'
        }
    ]
}
