/*
 * Mach Operating System
 * Copyright (c) 1991,1990,1989 Carnegie Mellon University
 * All Rights Reserved.
 *
 * Permission to use, copy, modify and distribute this software and its
 * documentation is hereby granted, provided that both the copyright
 * notice and this permission notice appear in all copies of the
 * software, derivative works or modified versions, and any portions
 * thereof, and that both notices appear in supporting documentation.
 *
 * CARNEGIE MELLON ALLOWS FREE USE OF THIS SOFTWARE IN ITS "AS IS"
 * CONDITION.  CARNEGIE MELLON DISCLAIMS ANY LIABILITY OF ANY KIND FOR
 * ANY DAMAGES WHATSOEVER RESULTING FROM THE USE OF THIS SOFTWARE.
 *
 * Carnegie Mellon requests users of this software to return to
 *
 *  Software Distribution Coordinator  or  Software.Distribution@CS.CMU.EDU
 *  School of Computer Science
 *  Carnegie Mellon University
 *  Pittsburgh PA 15213-3890
 *
 * any improvements or extensions that they make and grant Carnegie Mellon
 * the rights to redistribute these changes.
 */
/*
Copyright (c) 1988,1989 Prime Computer, Inc.  Natick, MA 01760
All Rights Reserved.

Permission to use, copy, modify, and distribute this
software and its documentation for any purpose and
without fee is hereby granted, provided that the above
copyright notice appears in all copies and that both the
copyright notice and this permission notice appear in
supporting documentation, and that the name of Prime
Computer, Inc. not be used in advertising or publicity
pertaining to distribution of the software without
specific, written prior permission.

THIS SOFTWARE IS PROVIDED "AS IS", AND PRIME COMPUTER,
INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS
SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  IN
NO EVENT SHALL PRIME COMPUTER, INC.  BE LIABLE FOR ANY
SPECIAL, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY
DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
PROFITS, WHETHER IN ACTION OF CONTRACT, NEGLIGENCE, OR
OTHER TORTIOUS ACTION, ARISING OUR OF OR IN CONNECTION
WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#include "pic.h"

#include <mach/machine/spl.h>

#include <emerixx/interrupt.h>
#include <emerixx/pci.h>

#include "pio.h"

spl_t curr_ipl;
int pic_mask[NSPL];
int curr_pic_mask;

int iunit[NINTR] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};

int nintr = NINTR;
int npics = NPICS;

int master_icw, master_ocw, slaves_icw, slaves_ocw;

u16 PICM_ICW1, PICM_OCW1, PICS_ICW1, PICS_OCW1;
u16 PICM_ICW2, PICM_OCW2, PICS_ICW2, PICS_OCW2;
u16 PICM_ICW3, PICM_OCW3, PICS_ICW3, PICS_OCW3;
u16 PICM_ICW4, PICS_ICW4;

/*
** picinit() - This routine
**		* Establishes a table of interrupt vectors
**		* Establishes a table of interrupt priority levels
**		* Establishes a table of interrupt masks to be put
**			in the PICs.
**		* Establishes location of PICs in the system
**		* Initialises them
**
**	At this stage the interrupt functionality of this system should be
**	coplete.
**
*/

/*
** 1. First we form a table of PIC masks - rather then calling form_pic_mask()
**	each time there is a change of interrupt level - we will form a table
**	of pic masks, as there are only 7 interrupt priority levels.
**
** 2. The next thing we must do is to determine which of the PIC interrupt
**	request lines have to be masked out, this is done by calling
**	form_pic_mask() with a (int_lev) of zero, this will find all the
**	interrupt lines that have priority 0, (ie to be ignored).
**	Then we split this up for the master/slave PICs.
**
** 2. Initialise the PICs , master first, then the slave.
**	All the register field definitions are described in pic_jh.h, also
**	the settings of these fields for the various registers are selected.
**
*/

void picinit(void)
{
        u16 i;

        asm volatile("cli");

        /*
        ** 1. Form pic mask table
        */
#if 0
	printf (" Let the console driver screw up this line ! \n");
#endif

        form_pic_mask();

        /*
        ** 1a. Select current SPL.
        */

        curr_ipl      = SPLHI;
        curr_pic_mask = pic_mask[SPLHI];

        /*
        ** 2. Generate addresses to each PIC port.
        */

        master_icw = PIC_MASTER_ICW;
        master_ocw = PIC_MASTER_OCW;
        slaves_icw = PIC_SLAVE_ICW;
        slaves_ocw = PIC_SLAVE_OCW;

        /*
        ** 3. Select options for each ICW and each OCW for each PIC.
        */

        PICM_ICW1 = (ICW_TEMPLATE | EDGE_TRIGGER | ADDR_INTRVL8 | CASCADE_MODE | ICW4__NEEDED);

        PICS_ICW1 = (ICW_TEMPLATE | EDGE_TRIGGER | ADDR_INTRVL8 | CASCADE_MODE | ICW4__NEEDED);

        PICM_ICW2 = PICM_VECTBASE;
        PICS_ICW2 = PICS_VECTBASE;

        PICM_ICW3 = (SLAVE_ON_IR2);
        PICS_ICW3 = (I_AM_SLAVE_2);

        PICM_ICW4 = (SNF_MODE_DIS | NONBUFD_MODE | NRML_EOI_MOD | I8086_EMM_MOD);
        PICS_ICW4 = (SNF_MODE_DIS | NONBUFD_MODE | NRML_EOI_MOD | I8086_EMM_MOD);

        PICM_OCW1 = (curr_pic_mask & 0x00FF);
        PICS_OCW1 = ((curr_pic_mask & 0xFF00) >> 8);

        PICM_OCW2 = NON_SPEC_EOI;
        PICS_OCW2 = NON_SPEC_EOI;

        PICM_OCW3 = (OCW_TEMPLATE | READ_NEXT_RD | READ_IR_ONRD);
        PICS_OCW3 = (OCW_TEMPLATE | READ_NEXT_RD | READ_IR_ONRD);

        /*
        ** 4.	Initialise master - send commands to master PIC
        */

        outb(master_icw, PICM_ICW1);
        outb(master_ocw, PICM_ICW2);
        outb(master_ocw, PICM_ICW3);
        outb(master_ocw, PICM_ICW4);

        outb(master_ocw, PICM_MASK);
        outb(master_icw, PICM_OCW3);

        /*
        ** 5.	Initialise slave - send commands to slave PIC
        */

        outb(slaves_icw, PICS_ICW1);
        outb(slaves_ocw, PICS_ICW2);
        outb(slaves_ocw, PICS_ICW3);
        outb(slaves_ocw, PICS_ICW4);

        outb(slaves_ocw, PICS_OCW1);
        outb(slaves_icw, PICS_OCW3);

        /*
        ** 6. Initialise interrupts
        */
        outb(master_ocw, PICM_OCW1);

#if 0
	printf(" spl set to %x \n", curr_pic_mask);
#endif
}

/*
** form_pic_mask(int_lvl)
**
**	For a given interrupt priority level (int_lvl), this routine goes out
** and scans through the interrupt level table, and forms a mask based on the
** entries it finds there that have the same or lower interrupt priority level
** as (int_lvl). It returns a 16-bit mask which will have to be split up between
** the 2 pics.
**
*/

#define SLAVEMASK (0xFFFF ^ SLAVE_ON_IR2)
#define SLAVEACTV 0xFF00

extern int (*ivect[])();
extern int intpri[];

void form_pic_mask(void)
{
        for (u32 spl = SPL0; spl < NSPL; spl++)
        {
                u32 mask = 0;
                for (u32 intr = 0x00, bit = 0x01; intr < NINTR; intr++, bit <<= 1)
                {
                        u32 intspl = intpri[intr];
                        if (intspl <= spl)
                        {
                                mask |= bit;
                        }
                }

                if ((mask & SLAVEACTV) != SLAVEACTV)
                {
                        // mask &= SLAVEMASK;
                        // Mask off the slave pin on the master
                        mask &= ~(1 << 2);
                }

                pic_mask[spl] = mask;
                //KASSERT(mask & (1 << 15));
        }
}

int intnull(int unit_dev)
{
        log_warn("intnull(%d)", unit_dev);
        return IRQ_NONE;
}

int prtnull_count = 0;
int prtnull(int unit)
{
        ++prtnull_count;
        return IRQ_NONE;
}

extern int hardclock();
extern int fpintr();

/*
US  444336  (A6 55 TFSI Allroad) (S6 ~500000+++)
UK  513249  (S6)
SE  521000  (S6)
SR  476278  (A6 50 TDI) (S6 ~570000+++)
DE  584000  (S6)
SK  593775  (S6)
BIH 599856  (S6)
SI  532611  (A6 50 TDI) (S6 ~600000+++)
CZ  605840  (S6)
BE  622213  (S6)
IT  631444  (S6)
ES  641383  (S6)
FR  655664  (S6)
AT  679982  (S6)
PL  684742  (S6)
HR  659532  (A6 50 TDI) (S6 700000+++)
NO  755809  (S6)
FI  845035  (S6)
DK  1165307 (S6)
*/

int (*ivect[NINTR])() = {
    /* 00 */ hardclock, /* always */
    /* 01 */ intnull /*kdintr*/,
    /* 02 */ intnull,
    /* 03 */ intnull, /* lnpoll, comintr, ... */

    /* 04 */ intnull, /* comintr, ... */
    /* 05 */ intnull, /* comintr, wtintr, ... */
    /* 06 */ intnull, /* fdintr, ... */
    /* 07 */ prtnull, /* qdintr, ... */

    /* 08 */ intnull,
    /* 09 */ intnull, /* ether */
    /* 10 */ intnull,
    /* 11 */ intnull,

    /* 12 */ intnull,
    /* 13 */ fpintr,  /* always */
    /* 14 */ intnull, /* hdintr, ... */
    /* 15 */ intnull, /* ??? */
};

int intpri[NINTR] = {
    /* 00 */ 0,
    /* 01 */ SPL6,
    /* 02 */ 0,
    /* 03 */ 0,
    /* 04 */ 0,
    /* 05 */ 0,
    /* 06 */ 0,
    /* 07 */ 0,
    /* 08 */ 0,
    /* 09 */ 0,
    /* 10 */ 0,
    /* 11 */ 0,
    /* 12 */ 0,
    /* 13 */ SPL1,
    /* 14 */ 0,
    /* 15 */ 0,
};

irq_handler_t ivect2[NINTR] = {
    /* 00 */ NULL,
    /* 01 */ NULL,
    /* 02 */ NULL,
    /* 03 */ NULL,
    /* 04 */ NULL,
    /* 05 */ NULL,
    /* 06 */ NULL,
    /* 07 */ NULL,
    /* 08 */ NULL,
    /* 09 */ NULL,
    /* 10 */ NULL,
    /* 11 */ NULL,
    /* 12 */ NULL,
    /* 13 */ NULL,
    /* 14 */ NULL,
    /* 15 */ NULL
    //
};

void *ivect2_devs[NINTR] = {
    /* 00 */ NULL,
    /* 01 */ NULL,
    /* 02 */ NULL,
    /* 03 */ NULL,
    /* 04 */ NULL,
    /* 05 */ NULL,
    /* 06 */ NULL,
    /* 07 */ NULL,
    /* 08 */ NULL,
    /* 09 */ NULL,
    /* 10 */ NULL,
    /* 11 */ NULL,
    /* 12 */ NULL,
    /* 13 */ NULL,
    /* 14 */ NULL,
    /* 15 */ NULL
    //
};

// Should not be here, the old mach is not geared towards dynamic device detection, pci devices, irq sharing etc.
// This is basically a hack to get the ball rolling.
int request_irq(int a_irq, irq_handler_t a_handler, unsigned long a_flags, const char *a_name, void *a_dev)
{
        struct pci_dev *pdev = (struct pci_dev *)a_dev;

        if (pdev->irq > 15)
        {
                return KERN_FAIL(EINVAL);
        }

        pci_class_t devclass;
        pci_read_config_byte(a_dev, PCIR_CLASS, &devclass);

        asm volatile("cli");
        int oldpri = intpri[a_irq];

        switch (devclass)
        {
        case PCIC_NETWORK:
                KASSERT(ivect2[a_irq] == NULL);
                ivect2[a_irq]      = a_handler;
                ivect2_devs[a_irq] = a_dev;
                intpri[a_irq]      = SPLBIO;
                break;

        case PCIC_STORAGE:
                KASSERT(ivect[a_irq] == intnull);
                ivect[a_irq]  = a_handler;
                intpri[a_irq] = SPLBIO;
                break;

        default:
                panic("What has it got in its pocketses?");
                break;
        }

        if (oldpri != intpri[a_irq])
        {
                form_pic_mask();
        }

        asm volatile("sti");
        return KERN_SUCCESS;
}
