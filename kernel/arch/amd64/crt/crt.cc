#include <assert.h>
#include <mach/machine/macro_help.h>
#include <stdio.h>
#include <string.h>

extern "C"
{
void crt_bootstrap();
void crt_teardown();

extern void (*__preinit_array_start[])();
extern void (*__preinit_array_end[])();

extern void (*__init_array_start[])();
extern void (*__init_array_end[])();

extern void (*__fini_array_start[])(void);
extern void (*__fini_array_end[])(void);

extern void _init(void);
extern void _fini(void);

extern void __cxa_pure_virtual();

void __cxa_pure_virtual()
{
        assert(0);
}

extern void __cxa_finalize(void *f);

static void pre(void)
{
        const size_t size = (size_t)(__preinit_array_end - __preinit_array_start);
        size_t i;
        for (i = 0; i < size; i++)
        {
                assert(0);
                (*__preinit_array_start[i])();
        }
}

static void ctor(void)
{
        const size_t size = (size_t)(__init_array_end - __init_array_start);
        for (size_t i = 0; i < size; i++)
        {
                (*__init_array_start[i])();
        }
}

static void dtor(void)
{
        size_t i = (size_t)(__fini_array_end - __fini_array_start);
        while (i-- > 0)
        {
                (*__fini_array_start[i])();
                assert(0);
        }
        //_fini ();
}
}
extern char __bss_start[];
extern char __bss_end[];
void crt_bootstrap()
{
        memset(&__bss_start, 0, (natural_t)(__bss_end - __bss_start)); // Should be done by the bootloader
        pre();
        ctor();
}

void crt_teardown()
{
        dtor();
        __cxa_finalize(0);
}
