#ifndef VMPARAM_H
#define VMPARAM_H

#include <mach/machine/vm_param.h>

#ifdef PAGE_SIZE
#undef PAGE_SIZE
#endif
#define PAGE_SIZE ((size_t)0x1000)

#define VM_HIGHER_HALF (0xFFFF800000000000)

#define DMAP_MIN_ADDRESS VM_HIGHER_HALF

#define PHYS_TO_DMAP(x) ((typeof(x))(((vm_offset_t)(x)) | (DMAP_MIN_ADDRESS)))

#define PT_SELF_REFRENCE_IDX     (UINTMAX_C(0x1ED))
#define PT_SELF_REFRENCE_PTR     ((vm_offset_t)((UINTMAX_C(0xFFFF) << 48) | (PT_SELF_REFRENCE_IDX << 39)))
#define PT_SELF_REFRENCE_END_PTR ((UINTMAX_C(0xFFFF) << 48) | ((PT_SELF_REFRENCE_IDX + 1) << 39))

#define PT_SELF_REFRENCE_ARRAY ((page_table_entry(*)[0x200][0x200][0x200])(PT_SELF_REFRENCE_PTR))

#define PT4_IDX(p) ((((vm_offset_t)p) >> 39) & 0x1FF) /*512 gb per entry */
#define PT3_IDX(p) ((((vm_offset_t)p) >> 30) & 0x1FF) /*1 gb per entry (512 gb full)*/
#define PT2_IDX(p) ((((vm_offset_t)p) >> 21) & 0x1FF) /*2 mb per entry (1gb full) */
#define PT1_IDX(p) ((((vm_offset_t)p) >> 12) & 0x1FF) /*4096 bytes per entry (2mb full)*/

#define PT4_PTR(va)                                                                                                                \
        ((page_table_entry *)&(PT_SELF_REFRENCE_ARRAY[PT_SELF_REFRENCE_IDX][PT_SELF_REFRENCE_IDX][PT_SELF_REFRENCE_IDX][0]))
#define PT3_PTR(va) ((page_table_entry *)&(PT_SELF_REFRENCE_ARRAY[PT_SELF_REFRENCE_IDX][PT_SELF_REFRENCE_IDX][PT4_IDX((va))][0]))
#define PT2_PTR(va) ((page_table_entry *)&(PT_SELF_REFRENCE_ARRAY[PT_SELF_REFRENCE_IDX][PT4_IDX((va))][PT3_IDX((va))][0]))
#define PT1_PTR(va) ((page_table_entry *)&(PT_SELF_REFRENCE_ARRAY[PT4_IDX((va))][PT3_IDX((va))][PT2_IDX((va))][0]))

#define PT4_ENTRY(va) ((PT4_PTR((va))[PT4_IDX((va))]))
#define PT3_ENTRY(va) ((PT3_PTR((va))[PT3_IDX((va))]))
#define PT2_ENTRY(va) ((PT2_PTR((va))[PT2_IDX((va))]))
#define PT1_ENTRY(va) ((PT1_PTR((va))[PT1_IDX((va))]))

#define IS_PAGE_ALIGNED(x) (((((vm_offset_t)(x))) & (PAGE_SIZE - 1)) == 0)

#define PAGE_ROUND_DOWN(x) ROUND_DOWN((x), PAGE_SIZE)
#define PAGE_ROUND_UP(x)   ROUND_UP((x), PAGE_SIZE)
#define PAGE_ALIGN(x)      PAGE_ROUND_DOWN((x))

#define USRTEXT      0
#define USRSTACK     0xFDBFE000
#define BTOPUSRSTACK (0xFDC00 - (UPAGES)) /* btop(USRSTACK) */
#define LOWPAGES     0
#define HIGHPAGES    UPAGES

/*
 * Virtual memory related constants, all in bytes
 */
#define MAXTSIZ (6 * 1024 * 1024) /* max text size */
#ifndef DFLDSIZ
#define DFLDSIZ (6 * 1024 * 1024) /* initial data size limit */
#endif
#ifndef MAXDSIZ
#define MAXDSIZ (32 * 1024 * 1024) /* max data size */
#endif
#ifndef DFLSSIZ
#define DFLSSIZ (512 * 1024) /* initial stack size limit */
#endif
#ifndef MAXSSIZ
#define MAXSSIZ MAXDSIZ /* max stack size */
#endif

/*
 * Default sizes of swap allocation chunks (see dmap.h).
 * The actual values may be changed in vminit() based on MAXDSIZ.
 * With MAXDSIZ of 16Mb and NDMAP of 38, dmmax will be 1024.
 */
#define DMMIN  32   /* smallest swap allocation */
#define DMMAX  4096 /* largest potential swap allocation */
#define DMTEXT 1024 /* swap allocation for text */

/*
 * Sizes of the system and user portions of the system page table.
 */
#define SYSPTSIZE (2 * NPTEPG)
#define USRPTSIZE (2 * NPTEPG)

/*
 * Size of User Raw I/O map
 */
#define USRIOSIZE 300

/*
 * The size of the clock loop.
 */
#define LOOPPAGES (maxfree - firstfree)
#define MAXSLP    20
#define SAFERSS                                                                                                                    \
        8 /* nominal ``small'' resident set size                                                                                   \
             protected against replacement */

#define DISKRPM 60

#define KLMAX  (4 / CLSIZE)
#define KLSEQL (2 / CLSIZE) /* in klust if vadvise(VA_SEQL) */
#define KLIN   (4 / CLSIZE) /* default data/stack in klust */
#define KLTXT  (4 / CLSIZE) /* default text in klust */
#define KLOUT  (4 / CLSIZE)

#define KLSDIST       3 /* klusters advance/retard for seq. fifo */
#define LOTSFREE      (512 * 1024)
#define LOTSFREEFRACT 4
#define DESFREE       (200 * 1024)
#define DESFREEFRACT  8

#define HANDSPREAD (2 * 1024 * 1024)

#define RATETOSCHEDPAGING 4

#define LOTSOFMEM 2

/* user/kernel map constants */
#define VM_USER_MIN_ADDRESS ((vm_offset_t)0)
#define VM_USER_MAX_ADDRESS ((vm_offset_t)0x0000800000000000)

#define VM_BIOSMEM_MIN_ADDRESS ((vm_offset_t)0xFFFFFFFF80000000)
#define VM_BIOSMEM_MAX_ADDRESS ((vm_offset_t)0xFFFFFFFF80100000)

#define VM_BOOTSTACK_MIN_ADDRESS   ((vm_offset_t)0xFFFFFFFF80100000)
#define VM_BOOTSTACK_MAX_ADDRESS   ((vm_offset_t)0xFFFFFFFF80200000)
#define VM_SMALL_BOOTSTACK_ADDRESS (VM_BOOTSTACK_MAX_ADDRESS - (PAGE_SIZE * 0x10))

#define VM_KERNEL_IMAGE_BASE_ADDRESS ((vm_offset_t)0xFFFFFFFF80200000)
#ifndef LINKER
extern vm_address_t _end;
#endif
#define VM_KERNEL_IMAGE_END_ADDRESS ((vm_offset_t)&_end)
#define VM_KERNEL_IMAGE_SIZE        (VM_KERNEL_IMAGE_END_ADDRESS - VM_KERNEL_IMAGE_BASE_ADDRESS)

#define VM_KERNEL_MIN_ADDRESS ((vm_offset_t)0xFFFFFF8000000000)
#define VM_KERNEL_MAX_ADDRESS ((vm_offset_t)VM_BIOSMEM_MIN_ADDRESS)

#define BOOT_VTOP(addr) ((addr)-VM_BIOSMEM_MIN_ADDRESS)

//-----------------------------------------------------------------------------
#define UPT_MIN_ADDRESS ((vm_offset_t)0xFDC00000)
#define UPT_MAX_ADDRESS ((vm_offset_t)0xFDFF7000)
#define VM_MAX_ADDRESS  UPT_MAX_ADDRESS
#define UPDT            VM_MIN_KERNEL_ADDRESS
#define KPT_MIN_ADDRESS ((vm_offset_t)0xFDFF8000)
#define KPT_MAX_ADDRESS ((vm_offset_t)0xFDFFF000)

/* virtual sizes (bytes) for various kernel submaps */
#define VM_MBUF_SIZE (NMBCLUSTERS * MCLBYTES)
#define VM_KMEM_SIZE (NKMEMCLUSTERS * CLBYTES)
#define VM_PHYS_SIZE (USRIOSIZE * CLBYTES)

/* # of kernel PT pages (initial only, can grow dynamically) */
#define VM_KERNEL_PT_PAGES ((vm_size_t)2) /* XXX: SYSPTSIZE */

/* pcb base */
#define pcbb(p) ((u_int)(p)->p_addr)

#endif /* VMPARAM_H */
