#pragma once

#define __PER_CPU_PTR(off, out)                                                                                                    \
        do                                                                                                                         \
        {                                                                                                                          \
                asm volatile("mov rax, gs:[0] \n\t"                                                                                \
                             "add rax, %1     \n\t"                                                                                \
                             "mov [%0], rax   \n\t"                                                                                \
                             : "=m"(out)                                                                                           \
                             : "i"(off)                                                                                            \
                             : "rax");                                                                                             \
        }                                                                                                                          \
        while (0)
