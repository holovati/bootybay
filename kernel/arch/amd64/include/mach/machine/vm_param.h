/*
 * Mach Operating System
 * Copyright (c) 1991,1990,1989,1988 Carnegie Mellon University
 * All Rights Reserved.
 *
 * Permission to use, copy, modify and distribute this software and its
 * documentation is hereby granted, provided that both the copyright
 * notice and this permission notice appear in all copies of the
 * software, derivative works or modified versions, and any portions
 * thereof, and that both notices appear in supporting documentation.
 *
 * CARNEGIE MELLON ALLOWS FREE USE OF THIS SOFTWARE IN ITS "AS IS"
 * CONDITION.  CARNEGIE MELLON DISCLAIMS ANY LIABILITY OF ANY KIND FOR
 * ANY DAMAGES WHATSOEVER RESULTING FROM THE USE OF THIS SOFTWARE.
 *
 * Carnegie Mellon requests users of this software to return to
 *
 *  Software Distribution Coordinator  or  Software.Distribution@CS.CMU.EDU
 *  School of Computer Science
 *  Carnegie Mellon University
 *  Pittsburgh PA 15213-3890
 *
 * any improvements or extensions that they make and grant Carnegie Mellon
 * the rights to redistribute these changes.
 */
/*
 *	File:	vm_param.h
 *	Author:	Avadis Tevanian, Jr.
 *	Date:	1985
 *
 *	I386 machine dependent virtual memory parameters.
 *	Most of the declarations are preceeded by I386_ (or i386_)
 *	which is OK because only I386 specific code will be using
 *	them.
 */

#pragma once

#include <mach/machine/vm_types.h>

#define BYTE_SIZE 8 /* byte size in bits */

#define PGBYTES 4096 /* bytes per 80386 page */
#define PGSHIFT 12   /* number of bits to shift for pages */

/* Virtual page size is the same as real page size - 4K is big enough.  */
#define PAGE_SHIFT PGSHIFT

/*
 *	Convert bytes to pages and convert pages to bytes.
 *	No rounding is used.
 */

#define amd64_btop(x) (((natural_t)(x)) >> PGSHIFT)
#define amd64_ptob(x) (((natural_t)(x)) << PGSHIFT)

/*
 *	Round off or truncate to the nearest page.  These will work
 *	for either addresses or counts.  (i.e. 1 byte rounds to 1 page
 *	bytes.)
 */

#define i386_round_page(x) ((((unsigned)(x)) + PGBYTES - 1) & ~(PGBYTES - 1))
#define i386_trunc_page(x) (((unsigned)(x)) & ~(PGBYTES - 1))

/* User address spaces are 3GB each,
   starting at virtual and linear address 0.  */
#define VM_MIN_ADDRESS        ((vm_offset_t)0)
#define VM_MAX_ADDRESS        ((vm_offset_t)0x0000800000000000)
/* User stack begins right below the kernel DMAP */
#define VM_USER_STACK_ADDRESS (VM_MAX_ADDRESS)
#define USER_STACK_SIZE       ((size_t)(512 * 4096) * 2)

/* The kernel address space is 1GB, starting at virtual address 0.  */
#define VM_MIN_KERNEL_ADDRESS ((vm_offset_t)0xFFFF800000000000)
#define VM_MAX_KERNEL_ADDRESS ((vm_offset_t)0xFFFFFFFFFFFFFFFF)

/* The kernel virtual address space is actually located
   at high linear addresses.
   This is the kernel address range in linear addresses.  */
#define LINEAR_MIN_KERNEL_ADDRESS ((vm_offset_t)0xFFFF800000000000)
#define LINEAR_MAX_KERNEL_ADDRESS ((vm_offset_t)0xFFFFFFFFFFFFFFFF)

#define KERNEL_STACK_SIZE (1 * PGBYTES)
#define INTSTACK_SIZE     (1 * PGBYTES)
#define STACK_ALIGNMENT   (1 << 4)

/* interrupt stack size */

/*
 * Virtual memory related constants, all in bytes
 */
#define MAXTSIZ (6 * 1024 * 1024) /* max text size */
#ifndef DFLDSIZ
#define DFLDSIZ (6 * 1024 * 1024) /* initial data size limit */
#endif
#ifndef MAXDSIZ
#define MAXDSIZ (32 * 1024 * 1024) /* max data size */
#endif
#ifndef DFLSSIZ
#define DFLSSIZ (512 * 1024) /* initial stack size limit */
#endif
#ifndef MAXSSIZ
#define MAXSSIZ MAXDSIZ /* max stack size */
#endif

/*
 * Constants related to network buffer management.
 * MCLBYTES must be no larger than CLBYTES (the software page size), and,
 * on machines that exchange pages of input or output buffers with mbuf
 * clusters (MAPPED_MBUFS), MCLBYTES must also be an integral multiple
 * of the hardware page size.
 */
#define MSIZE    128 /* size of an mbuf */
#define MCLBYTES 1024
#define MCLSHIFT 10
#define MCLOFSET (MCLBYTES - 1)
#ifndef NMBCLUSTERS
#ifdef GATEWAY
#define NMBCLUSTERS 512 /* map size, max cluster allocation */
#else
#define NMBCLUSTERS 256 /* map size, max cluster allocation */
#endif
#endif

/*
 *	Conversion between 80386 pages and VM pages
 */

#define trunc_i386_to_vm(p) (atop(trunc_page(i386_ptob(p))))
#define round_i386_to_vm(p) (atop(round_page(i386_ptob(p))))
#define vm_to_i386(p)       (i386_btop(ptoa(p)))

/*
 *	Physical memory is direct-mapped to virtual memory
 *	starting at virtual address phys_mem_va.
 */

vm_address_t pmap_get_dmap_addr();
vm_address_t kvtophys(vm_address_t addr);
static inline vm_offset_t phystokv(vm_offset_t pa)
{
        return pa + pmap_get_dmap_addr();
}

/*
 *	Kernel virtual memory is actually at 0xc0000000 in linear addresses.
 */
#define kvtolin(a) ((vm_offset_t)(a) + LINEAR_MIN_KERNEL_ADDRESS)
#define lintokv(a) ((vm_offset_t)(a)-LINEAR_MIN_KERNEL_ADDRESS)
