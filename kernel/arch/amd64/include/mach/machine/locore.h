#pragma once
#include <kernel/kern_types.h>
#include <mach/machine/macro_help.h>
struct amd64_saved_state;

#define ICODE_DATA ((u8 *)icode)
#define ICODE_SIZE ((size_t)(szicode))
extern u8 icode[];       /* Pointer to bootstrap icode */
extern size_t szicode[]; /* Size of the icode */

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Halt a cpu.
 */
// void locore_cpu_halt(void);

kern_return_t copyout(const void *kaddr, void *uaddr, size_t len);

kern_return_t copyin(const void *uaddr, void *kaddr, size_t len);

char *stpncpyin(char *a_kdest, char const *a_usrc, size_t a_len, kern_return_t *a_kret);
char **ptpncpyin(char *a_kdest[], char const *const a_usrc[], size_t a_count, kern_return_t *a_kret);

/* Entry for all traps */
void locore_trap_entry(struct amd64_saved_state *regs);

void locore_call_continuation(continuation_t contr);

void locore_load_context(thread_t thread);

void locore_thread_continue(void);

thread_t locore_switch_context(thread_t old_thread, continuation_t continuation, thread_t new_thread);

void locore_kernel_trap(struct amd64_saved_state *regs);

void locore_cpu_idle();

void locore_exception_return(void);

void locore_init_user(void);

#ifdef __cplusplus
}
#endif
