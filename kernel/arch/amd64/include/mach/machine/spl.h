#pragma once

#define SPL0 0
#define SPL1 1
#define SPL2 2
#define SPL3 3
#define SPL4 4
#define SPL5 5
#define SPL6 6
#define SPL7 7

#define SPLPP  5
#define SPLBIO 5
#define SPLTTY 6
#define SPLNI  6
#define SPLHI  7
#define IPLHI  SPLHI

#define NSPL (SPL7 + 1)

/*
 *	This file defines the interrupt priority levels used by
 *	machine-dependent code.
 */

typedef int spl_t;

#ifdef __cplusplus
extern "C" {
#endif

spl_t splsoftclock();
spl_t spl1(void);
spl_t spl2(void);
spl_t spl3(void);
spl_t splnet(void);
spl_t splhdw(void);
spl_t spl4(void);
spl_t splbio(void);
spl_t spldcm(void);
spl_t spl5(void);
spl_t spltty(void);
spl_t splimp(void);
spl_t splvm(void);
spl_t spl6(void);
spl_t splclock(void);
spl_t splsched(void);
spl_t splhigh(void);
spl_t splhi(void);
spl_t spl7();
spl_t splx(spl_t level);
void splx_cli(spl_t level);
spl_t sploff();
spl_t splon(spl_t rflags);
void setsoftclock(void);
spl_t spl0(void);

#ifdef __cplusplus
}
#endif