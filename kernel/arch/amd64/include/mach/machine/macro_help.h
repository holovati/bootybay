#pragma once

#ifndef NAKED
#define NAKED __attribute__((naked))
#endif

#ifndef NORETURN
#define NORETURN __attribute__((noreturn))
#endif

#ifndef UNREACHABLE
#define UNREACHABLE() __builtin_unreachable()
#endif

#ifndef USED
#define USED __attribute__((used))
#endif

#ifndef UNUSED
#define UNUSED __attribute__((unused))
#endif

#ifndef FORCEINLINE
#define FORCEINLINE __attribute__((always_inline))
#endif

#ifndef SECTION
#define SECTION(name) __attribute__((used, section("." #name)))
#endif