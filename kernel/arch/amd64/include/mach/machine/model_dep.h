#pragma once

// Contains data passed between bootstrap/initcode
struct modeldep_bootdata
{
        vm_address_t phys_first_addr;
        vm_address_t phys_last_addr;
        size_t phys_page_count;

        /* Physcall address range used by the kernel image */
        vm_address_t kernel_image_phys_addr_start;
        vm_address_t kernel_image_phys_addr_end;

        /* Virtual address range used by the kernel image */
        vm_address_t kernel_image_va_addr_start;
        vm_address_t kernel_image_va_addr_end;
#ifdef ENABLE_MEM_POISON
        int early_poison;
#endif
};

#ifdef __cplusplus
extern "C" {
#endif

void pmap_bootstrap(struct modeldep_bootdata *bd);

boolean_t pmap_valid_page(vm_offset_t x);

vm_offset_t early_boot_map_page(vm_offset_t phys_page_addr);

vm_offset_t pmap_grab_page();

boolean_t cpu_feature_page1gb();

boolean_t is_1gb_aligned(vm_offset_t address);

boolean_t is_2mb_aligned(vm_offset_t address);

typedef boolean_t (*modeldep_iter_pages_cb)(vm_address_t phys_addr,
                                            vm_address_t region_start,
                                            vm_address_t region_end,
                                            void *user_data);

void modeldep_iter_all_pages(modeldep_iter_pages_cb cb, void *user_data);

void modeldep_iter_avail_pages(modeldep_iter_pages_cb cb, void *user_data);
#ifdef __cplusplus
}
#endif