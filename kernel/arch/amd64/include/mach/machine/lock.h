/*
 * Mach Operating System
 * Copyright (c) 1991,1990 Carnegie Mellon University
 * All Rights Reserved.
 *
 * Permission to use, copy, modify and distribute this software and its
 * documentation is hereby granted, provided that both the copyright
 * notice and this permission notice appear in all copies of the
 * software, derivative works or modified versions, and any portions
 * thereof, and that both notices appear in supporting documentation.
 *
 * CARNEGIE MELLON ALLOWS FREE USE OF THIS SOFTWARE IN ITS "AS IS"
 * CONDITION.  CARNEGIE MELLON DISCLAIMS ANY LIABILITY OF ANY KIND FOR
 * ANY DAMAGES WHATSOEVER RESULTING FROM THE USE OF THIS SOFTWARE.
 *
 * Carnegie Mellon requests users of this software to return to
 *
 *  Software Distribution Coordinator  or  Software.Distribution@CS.CMU.EDU
 *  School of Computer Science
 *  Carnegie Mellon University
 *  Pittsburgh PA 15213-3890
 *
 * any improvements or extensions that they make and grant Carnegie Mellon
 * the rights to redistribute these changes.
 */
/*
 * Machine-dependent simple locks for the i386.
 */
#ifndef _I386_LOCK_H_
#define _I386_LOCK_H_
/*
 *	All of the locking routines are built from calls on
 *	a locked-exchange operation.  Values of the lock are
 *	0 for unlocked, 1 for locked.
 */

static u32 _simple_lock_xchg_(simple_lock_t lock, u32 new_val)
{
        return __atomic_exchange_n(&(lock->lock_data), new_val, __ATOMIC_SEQ_CST);
}

#define simple_lock_init(l) ((l)->lock_data = 0)

static __attribute__((used)) void simple_lock(simple_lock_t lock)
{
        while (_simple_lock_xchg_(lock, 1))
        {
        }
}

#define simple_unlock(l) (_simple_lock_xchg_(l, 0))

#define simple_lock_try(l) (!_simple_lock_xchg_(l, 1))

/*
 *	General bit-lock routines.
 */

static inline void bit_lock(int bit, int volatile *addr)
{
        asm volatile("jmp	1f	\n\
		    0:	bt	%0, %1	\n\
			  jb	0b	\n\
		    1:	lock bts	%0, %1 \n\
        jb	0b"
                     :
                     : "m"(*addr), "r"(bit)
                     : "cc");
}

static inline void bit_unlock(int bit, integer_t volatile *addr)
{
        asm volatile("lock btr %0, %1" : : "m"(*addr), "r"(bit) : "cc");
        return;
}

/*
 *	Set or clear individual bits in a long word.
 *	The locked access is needed only to lock access
 *	to the word, not to individual bits.
 */
static inline void i_bit_set(natural_t bit, integer_t volatile *l)
{
        asm volatile("lock bts	%1, %0" : : "r"(bit), "m"(*(l)) : "cc");
}

static inline void i_bit_clear(natural_t bit, integer_t volatile *l)
{
        asm volatile("lock btr	%1, %0" : : "r"(bit), "m"(*(l)) : "cc");
}

extern void simple_lock_pause();

#define SIMPLE_LOCK_SCOPE(l)                                                                                                       \
        simple_lock(l);                                                                                                            \
        defer(simple_unlock((l)))

#endif
