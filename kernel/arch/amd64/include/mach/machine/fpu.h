#pragma once
/*
 * Mach Operating System
 * Copyright (c) 1991 Carnegie Mellon University
 * All Rights Reserved.
 *
 * Permission to use, copy, modify and distribute this software and its
 * documentation is hereby granted, provided that both the copyright
 * notice and this permission notice appear in all copies of the
 * software, derivative works or modified versions, and any portions
 * thereof, and that both notices appear in supporting documentation.
 *
 * CARNEGIE MELLON ALLOWS FREE USE OF THIS SOFTWARE IN ITS "AS IS"
 * CONDITION.  CARNEGIE MELLON DISCLAIMS ANY LIABILITY OF ANY KIND FOR
 * ANY DAMAGES WHATSOEVER RESULTING FROM THE USE OF THIS SOFTWARE.
 *
 * Carnegie Mellon requests users of this software to return to
 *
 *  Software Distribution Coordinator  or  Software.Distribution@CS.CMU.EDU
 *  School of Computer Science
 *  Carnegie Mellon University
 *  Pittsburgh PA 15213-3890
 *
 * any improvements or extensions that they make and grant Carnegie Mellon
 * the rights to redistribute these changes.
 */

/*
 * Macro definitions for routines to manipulate the
 * floating-point processor.
 */

#include <mach/machine/thread.h>

typedef struct thread *thread_t;

#ifdef __cplusplus
extern "C" {
#endif

void fpu_module_init();

void fp_state_alloc();

void enable_fpe(struct i386_fpsave_state *ifps);

void fpu_save(thread_t a_thread);

void fpu_free(struct fpsave_state *fps);

void thread_save_fpu(thread_t a_thread);

void thread_load_fpu(thread_t a_thread);

void fpnoextflt(thread_t a_thread);

/*
 * FPU instructions.
 */
#define fninit() asm volatile("fninit")

#define fnstcw(control) asm("fnstcw %0" : "=m"(*(unsigned short *)(control)))

#define fldcw(control) asm volatile("fldcw %0" : : "m"(*(unsigned short *)&(control)))

#define fnstsw()                                                                                                                   \
        ({                                                                                                                         \
                unsigned short _status__;                                                                                          \
                asm("fnstsw %0" : "=ma"(_status__));                                                                               \
                _status__;                                                                                                         \
        })

#define fnclex()      asm volatile("fnclex")
#define fnsave(state) asm volatile("fnsave %0" : "=m"(*state))
#define frstor(state) asm volatile("frstor %0" : : "m"(state))
#define fwait()       asm("fwait");

static inline void fxsave(struct fpsave_state *s) { asm volatile("fxsave [%0] " : : "r"(s) :); }
static inline void fxrstor(struct fpsave_state *s) { asm volatile("fxrstor [%0]" : : "r"(s) :); }

#define fpu_load_context(pcb)

#if 0
/*
 * Save thread`s FPU context.
 * If only one CPU, we just set the task-switched bit,
 * to keep the new thread from using the coprocessor.
 * If multiple CPUs, we save the entire state.
 */
#define fpu_save_context(thread)                                                                                                   \
        {                                                                                                                          \
                struct i386_fpsave_state *ifps;                                                                                    \
                ifps = (thread)->pcb->ims.ifps;                                                                                    \
                if (ifps != 0 && !ifps->fp_valid)                                                                                  \
                {                                                                                                                  \
                        /* registers are in FPU - save to memory */                                                                \
                        ifps->fp_valid = TRUE;                                                                                     \
                        fnsave(&ifps->fp_save_state);                                                                              \
                        set_ts();                                                                                                  \
                }                                                                                                                  \
        }

#else
#endif

#define fpu_save_context(thread)                                                                                                   \
        {                                                                                                                          \
                set_ts();                                                                                                          \
        }

#ifdef __cplusplus
}
#endif

extern int fp_kind;
