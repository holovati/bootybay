#pragma once

/*
 * Common I/O ports.
 */
#define PS2_PORT_TMR0      0x40 /* timer 0, 1, or 2 value (r/w) */
#define PS2_PORT_TMR1      0x41
#define PS2_PORT_TMR2      0x42
#define PS2_PORT_TMRCTL    0x43 /* timer control (write-only) */
#define PS2_PORT_READWRITE 0x60 /* keyboard data & cmds (read/write) */
#define PS2_PORT_STATUS    0x64 /* status (read-only) */
#define PS2_PORT_COMMNAD   0x64 /* ctlr command (write-only) */

/*
 * Bit definitions for PS2_PORT_STATUS port.
 */
#define PS2_STATUS_OUTPUT_BUFFER_FULL          0x01 /* output buffer full */
#define PS2_STATUS_INPUT_BUFFER_FULL           0x02 /* input buffer full */
#define PS2_STATUS_SYSTEM_FLAG                 0x04 /* "System Flag" */
#define PS2_STATUS_COMMAND_DATA                0x08 /* 1 = input buf has cmd, 0 = data */
#define PS2_STATUS_KEYBOARD_INHIBITED          0x10 /* 0 if keyboard inhibited */
#define PS2_STATUS_AUXILARY_OUTPUT_BUFFER_FULL 0x20 /* AUX device data is in the output buffer */

/*
 * PS2 commands (sent to PS2_PORT_COMMAND port).
 */
#define PS2_CONTROLLER_COMMAND_READ             0x20 /* read controller command byte */
#define PS2_CONTROLLER_COMMAND_WRITE            0x60 /* write controller command byte */
#define PS2_CONTROLLER_COMMAND_TEST             0xAB /* test interface */
#define PS2_CONTROLLER_COMMAND_DUMP             0xAC /* diagnostic dump */
#define PS2_CONTROLLER_COMMAND_DISABLE          0xAD /* disable keyboard */
#define PS2_CONTROLLER_COMMAND_ENABLE           0xAE /* enable keyboard */
#define PS2_CONTROLLER_COMMAND_READ_KEYBOARD_ID 0xC4 /* read keyboard ID */
#define PS2_CONTROLLER_COMMAND_AUXILARY         0xD4 /* Next device command is sent to the aux device */
#define PS2_CONTROLLER_COMMAND_ECHO             0xEE /* used for diagnostic testing */

/*
 * Bit definitions for controller config/command byte get/set with PS2_CONTROLLER_COMMAND_READ/WRITE.
 */
#define PS2_CONFIG_ENABLE_KBD_IRQ       0x01
#define PS2_CONFIG_ENABLE_AUX_IRQ       0x02
#define PS2_CONFIG_SYSTEM_FLAG          0x04
#define PS2_CONFIG_SYSTEM_RESERVED3     0x08
#define PS2_CONFIG_SYSTEM_DISABLE_KBD   0x10
#define PS2_CONFIG_SYSTEM_DISABLE_AUX   0x20
#define PS2_CONFIG_SYSTEM_KBD_TRANSLATE 0x40
#define PS2_CONFIG_SYSTEM_KBD_RESERVED7 0x80

/*
 * Commands (send to PS2_PORT_READWRITE).
 */
#define PS2_READWRITE_SET_KEYBOARD_LEDS 0xed /* set status LEDs (caps lock, etc.) */

#define PS2_MOUSE_COMMAND_GET_DEVICE_ID          0xF2
#define PS2_MOUSE_COMMAND_SET_SAMPLE_RATE        0xF3
#define PS2_MOUSE_COMMAND_ENABLE_DATA_REPORTING  0xF4
#define PS2_MOUSE_COMMAND_DISABLE_DATA_REPORTING 0xF5
#define PS2_MOUSE_COMMAND_RESET                  0xFF

/*
 * ps2_controller_command:
 *
 *	This function sends a command byte to the keyboard command
 *	port, but first waits until the input/output data buffer is
 *	clear before sending the data.
 *
 */
void ps2_ctrl_cmd(uint8_t a_cmd);

uint8_t ps2_device_output();

int ps2_ctrl_input_full();

void ps2_enable_primary();

void ps2_disable_primary();

void ps2_enable_auxilary();

void ps2_disable_auxilary();

int ps2_primary_output_full();

int ps2_aux_output_full();

void ps2_primary_cmd(uint8_t a_cmd);

void ps2_aux_cmd(uint8_t a_cmd);
