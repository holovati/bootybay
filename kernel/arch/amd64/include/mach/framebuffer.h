#pragma once

#include <aux/psf.h>

#include <emerixx/fb.h>

#define FB_DRAW_GLYPH_UNDERLINE      0x01
#define FB_DRAW_GLYPH_INVERSE_COLORS 0x02

typedef struct fb_backbuffer_data
{
        vm_address_t memory; /* Memory address of the buffer */
        size_t bpitch;       /* Pitch in uint8_t's */
        size_t qpitch;       /* Pitch in uint64_t's */
        size_t bytespp;      /* Size of each pixel in bytes */
        size_t dirty;        /* Is the buffer dirty */
        size_t width;        /* Width in pixels */
        size_t height;       /* Height in pixels */
} *fb_backbuffer_t;

uint32_t fb_make_rgb(uint32_t a_red, uint32_t a_green, uint32_t a_blue);

void fb_draw_glyph(uint32_t a_glyph,
                   uint32_t a_xpos,
                   uint32_t a_ypos,
                   font_t a_font,
                   uint32_t *a_fgbg_color,
                   int a_flags,
                   fb_backbuffer_t a_backbuffer);

void fb_draw_string(const char *a_str,
                    size_t a_strlen,
                    uint32_t *a_xpos,
                    uint32_t *a_ypos,
                    font_t a_font,
                    uint32_t *a_fgbg_color,
                    int a_flags,
                    fb_backbuffer_t a_backbuffer);

void fb_render_backbuffer(fb_backbuffer_t a_backbuffer);

kern_return_t fb_alloc_backbuffer(fb_backbuffer_t a_backbuffer);

void fb_free_backbuffer(fb_backbuffer_t a_backbuffer);

kern_return_t fb_get_variable_screeninfo(struct fb_var_screeninfo *a_fsi);

kern_return_t fb_get_fixed_screeninfo(struct fb_fix_screeninfo *a_fsi);
