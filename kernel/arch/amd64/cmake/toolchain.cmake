set(CMAKE_SYSTEM_NAME Generic)

set(CMAKE_CROSSCOMPILING TRUE)

set(CMAKE_C_COMPILER
    x86_64-rs64-musl-gcc
    CACHE STRING "gcc")
set(CMAKE_ASM_COMPILER
    x86_64-rs64-musl-gcc
    CACHE STRING "as")
set(CMAKE_CXX_COMPILER
    x86_64-rs64-musl-g++
    CACHE STRING "g++")

# set(CMAKE_AR
# x86_64-rs64-ar
# CACHE STRING "Archiver")
set(CMAKE_LINKER
    x86_64-rs64-musl-gcc
    CACHE STRING "Linker")

set(CMAKE_CXX_LINK_EXECUTABLE
    "<CMAKE_LINKER> <CMAKE_CXX_LINK_FLAGS> <LINK_FLAGS> <OBJECTS> -o <TARGET> <LINK_LIBRARIES>"
)

set(CMAKE_EXE_LINKER_FLAGS
    "-nostdlib -static -nostartfiles -nodefaultlibs -Wl,--gc-sections -ffreestanding -Wl,-z,max-page-size=4096 -Wl,-z,common-page-size=4096 -Wl,--build-id=none -mno-red-zone -Wl,--gc-sections"
)

message(${_CMAKE_TOOLCHAIN_PREFIX} .. ${_CMAKE_TOOLCHAIN_LOCATION})

set(RS64_TARGET_ARCH "amd64")

# ##############################################################################
# Exported arch specific compile commands
# ##############################################################################
set(ARCH_COMPILE_OPTIONS
    -masm=intel
    -mcmodel=kernel
    -fno-stack-protector
    -fno-unwind-tables
    -mno-red-zone
    -mno-sse
    -mno-mmx
    -mno-sse2
    -mno-3dnow
    -mno-80387
    -fno-asynchronous-unwind-tables
    -fno-pic
    -fno-pie
    -ffreestanding
    CACHE STRING "")
