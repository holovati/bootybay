include(${CMAKE_SOURCE_DIR}/tools/cmake/rs64_compile_options.cmake)
add_definitions(-DMACH_KERNEL -DKERNEL -Di386 -DCONTINUATIONS)

add_subdirectory(src/syscall)
add_subdirectory(src/console)

# ##############################################################################
# Compile options
# ##############################################################################
# set_rs64_compile_options()

# set(ARCH_LINK_OPTIONS "-Wl,--script=${CMAKE_BINARY_DIR}/link.ld" CACHE STRING
# "")

# ##############################################################################
# Copy and unpack the disk file to build dir
# ##############################################################################
configure_file(${PROJECT_SOURCE_DIR}/amd64/rsdisk.img.gz
  ${CMAKE_BINARY_DIR}/rsdisk.img.gz COPYONLY)

if(EXISTS ${CMAKE_BINARY_DIR}/rsdisk.img)
  file(REMOVE ${CMAKE_BINARY_DIR}/rsdisk.img)
endif()

message(STATUS "Unpacking ${CMAKE_BINARY_DIR}/rsdisk.img.gz")

execute_process(COMMAND gunzip ${CMAKE_BINARY_DIR}/rsdisk.img.gz)

set(MCOPY_RSDISK_FILE "${CMAKE_BINARY_DIR}/rsdisk.img")
set(MCOPY_RSDISK_PART "1")

configure_file(${PROJECT_SOURCE_DIR}/amd64/cmake/mtools-conf.in
  ${CMAKE_BINARY_DIR}/mtools-conf)

# ##############################################################################
# Add include path acpica
# ##############################################################################

# ##############################################################################
# Compile all c and c++ files
# ##############################################################################
set(libc_src crt/crt.cc crt/icxxabi.cc crt/crti.s crt/crtn.s)

set(c_srcs

  # src/fpe_linkage.c
  src/fpu.c
  src/hardclock.cc
  src/locore/locore_interrupt.cc
  src/locore/locore_entry_kern.cc
  src/locore/locore_entry_user.cc
  src/locore/locore_exit_asm.cc
  src/locore.c
  src/pcb.cc
  src/phys.cc
  src/pic.c

  # src/pit.cc
  src/spl.cc
  src/ps2.cc
  src/rtc.cc
  src/pmap.cc
  src/machine_page.cc
  src/pio.cc

  # src/read_fault.c
  src/framebuffer.cc
  src/model_dep.c)

set(pci_bus_srcs src/bus/pci/pci_cfgreg.c src/bus/pci/pci.cc)

# ##############################################################################
# Export libarch
# ##############################################################################
add_library(arch OBJECT ${c_srcs} ${pci_bus_srcs} ${libc_src})

target_include_directories(arch
  PUBLIC
  $<TARGET_PROPERTY:aux,INTERFACE_INCLUDE_DIRECTORIES>
  "${PROJECT_SOURCE_DIR}/include"
  PRIVATE
  "${PROJECT_SOURCE_DIR}/amd64/src"
  SYSTEM $<TARGET_PROPERTY:emerixx,INTERFACE_INCLUDE_DIRECTORIES>
  SYSTEM $<TARGET_PROPERTY:uapi,INTERFACE_INCLUDE_DIRECTORIES>
  SYSTEM $<TARGET_PROPERTY:drivers,INTERFACE_INCLUDE_DIRECTORIES>
  SYSTEM $<TARGET_PROPERTY:etl,INTERFACE_INCLUDE_DIRECTORIES>
  SYSTEM $<TARGET_PROPERTY:aux,INTERFACE_INCLUDE_DIRECTORIES>
)

# target_render_mako_files(arch src/exception_entry.c.mako)
target_render_mako_files(arch src/locore/locore_entry_asm.cc.mako)
target_render_mako_files(arch src/trapno.h.mako)

# ##############################################################################
# Preprocess linker file and export it
# ##############################################################################
add_custom_command(
  OUTPUT ${PROJECT_BINARY_DIR}/link.ld
  COMMAND
  ${CMAKE_C_COMPILER} -C -P -xc -E ${PROJECT_SOURCE_DIR}/amd64/link.lds
  -I$<JOIN:$<TARGET_PROPERTY:arch,INCLUDE_DIRECTORIES>,$<SEMICOLON>-I>
  -I$<JOIN:$<TARGET_PROPERTY:linker_script,INCLUDE_DIRECTORIES>,$<SEMICOLON>-I>
  -I${PROJECT_BINARY_DIR}/amd64/mako
  -o ${CMAKE_BINARY_DIR}/link.ld
  COMMAND_EXPAND_LISTS
  COMMENT "Generating linker script"
)

add_custom_target(linker_script DEPENDS ${PROJECT_BINARY_DIR}/link.ld)

add_dependencies(arch linker_script)

# add_dependencies(arch ${CMAKE_CURRENT_BINARY_DIR}/link.lds)

# ##############################################################################
# Called for the kernel target
# ##############################################################################
function(arch_target_build target)
  set(EXE_PATH ${CMAKE_BINARY_DIR}/stripped/${target}${CMAKE_EXECUTABLE_SUFFIX})
  set(IMG_PATH ${CMAKE_BINARY_DIR}/rsdisk.img)

  find_program(MCOPY_PRESENT "mcopy")

  if(MCOPY_PRESENT)
    message(
      STATUS
      "mcopy found, ${target}${CMAKE_EXECUTABLE_SUFFIX} will be copied to ${IMG_PATH}"
    )

    add_custom_command(
      TARGET ${target}
      POST_BUILD
      COMMAND ${CMAKE_COMMAND} -E env "MTOOLSRC=${CMAKE_BINARY_DIR}/mtools-conf"
      mcopy "-bspo" "-i" "${IMG_PATH}" "${EXE_PATH}" "C:"
      COMMENT "Copying ${EXE_PATH} to ${IMG_PATH}")

  else()
    message(WARNING "mcopy not found, bootable image will not be generated")
  endif()
endfunction()
