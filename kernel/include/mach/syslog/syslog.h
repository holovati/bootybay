#pragma once
/**
 * Copyright (c) 2020 rxi
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the MIT license. See `log.c` for details.
 */
/**
 * Copyright (c) 2020 Damir Holovati
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the MIT license. See `log.c` for details.
 *
 * Modified for kernel use
 */
#include <stdarg.h>

#define LOG_USE_COLOR
#define LOG_VERSION "0.1.0"

extern void locore_cpu_halt(void);

typedef struct
{
        va_list ap;
        const char *fmt;
        const char *file;
        void *udata;
        int line;
        int level;
} log_Event;

typedef void (*log_LogFn)(log_Event *ev);
typedef void (*log_LockFn)(boolean_t lock, void *udata);

enum
{
        LOG_TRACE,
        LOG_DEBUG,
        LOG_INFO,
        LOG_WARN,
        LOG_ERROR,
        LOG_PANIC
};

// Defined by the build system (based on CMakeLists.txt)
#ifndef __FILENAME__
#define __FILENAME__ __FILE__
#endif

#define log_trace(...) log_log(LOG_TRACE, __FILENAME__, __LINE__, __VA_ARGS__)
#define log_debug(...) log_log(LOG_DEBUG, __FILENAME__, __LINE__, __VA_ARGS__)
#define log_info(...)  log_log(LOG_INFO, __FILENAME__, __LINE__, __VA_ARGS__)
#define log_warn(...)  log_log(LOG_WARN, __FILENAME__, __LINE__, __VA_ARGS__)
#define log_error(...) log_log(LOG_ERROR, __FILENAME__, __LINE__, __VA_ARGS__)

void log_init();

const char *log_level_string(int level);
void log_set_lock(log_LockFn fn, void *udata);
void log_set_level(int level);
void log_set_quiet(boolean_t enable);
int log_add_callback(log_LogFn fn, void *udata, int level);
#ifndef KERNEL
int log_add_fp(vfs_file_t tp, int level);
#endif
void log_log(int level, const char *file, int line, const char *fmt, ...);