#pragma once

#if !defined(__NEED_vm_page)
#define __NEED_vm_page
#endif

#if !defined(__NEED_vfs_file)
#define __NEED_vfs_file
#endif

#if !defined(__NEED_ioctl_req)
#define __NEED_ioctl_req
#endif

#include <emerixx/vfs/vfs_types.h>
#include <kernel/vm/vm_types.h>

struct uio;
struct tty;
struct buf;
struct pollfd;
/*
 * Declaration of block device
 * switch. Each entry (row) is
 * the only link between the
 * main unix code and the driver.
 * The initialization of the
 * device switches is in the
 * file conf.c.
 */

extern struct bdevsw
{
        int (*d_open)(dev_t dev, int rw, int type);
        int (*d_close)(dev_t dev);
        int (*d_strategy)(struct buf *);
        int (*d_strategy2)(dev_t, void *);
        int (*d_strategy3)(dev_t, vm_page_t a_vmp);
        int (*d_ioctl)(dev_t dev, ioctl_req_t cmd, int fflag);
} bdevsw[];
