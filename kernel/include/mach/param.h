#pragma once
#include <sys/param.h>

/*
 * Compatibility definitions for disk IO.
 */

/*
 * Disk devices do all IO in 512-byte blocks.
 */
#define DEV_BSIZE  512
#define DEV_BSHIFT 9 /* log2(DEV_BSIZE) */
#define DEV_BMASK  (DEV_BSIZE - 1)
#define MAXPHYS    (64 * 1024) /* max raw I/O transfer size */
#define MAXBSIZE   MAXPHYS

#define IS_ALIGNED(x, y)      (((((natural_t)(x))) & ((natural_t)(y)-1)) == 0)
#define ALIGN(x, a)           ((typeof(x))((((long)(x) + (long)(a)-1)) & (~((long)(a)-1))))
#define ROUNDDOWN(val, round) ((typeof(val))((size_t)(val) & (size_t)(typeof(val))~((round)-1)))
#define ROUNDUP(val, round)   ((typeof(val))(((size_t)(val) + (round)-1) & (size_t)(typeof(val))~((round)-1)))
