#ifndef TYPES_H
#define TYPES_H
#ifdef __cplusplus
extern "C"
{
#endif

#ifdef SYSCALL_GENERATOR
#define SYSCALL(___x)    __attribute__((linux_syscall(#___x)))
#define SYSCALL_INSTANCE /* __attribute__((syscall_instance)) */
#define EXCEPTION(x)     /* ARCH_EXCEPTION(x) */
#else
#define SYSCALL(x)       /* SYSCALL x */
#define SYSCALL_INSTANCE /* SYSCALL INSTANCE */
#define EXCEPTION(x)     /* ARCH_EXCEPTION(x) */
#endif

#define __NEED_pid_t
#define __NEED_uid_t
#define __NEED_gid_t
#define __NEED_off_t
#define __NEED_mode_t
#define __NEED_ino_t
#define __NEED_dev_t
#define __NEED_suseconds_t
#define __NEED_struct_timespec
#define __NEED_struct_timeval
#define __NEED_time_t
#define __NEED_socklen_t

#define __DEFINED_pthread_t
#define __DEFINED_sigset_t

#include <bits/alltypes.h>
#include <stddef.h>
#include <stdint.h>

typedef int64_t sigset_t;
typedef struct pthread *pthread_t, *pthread_t;
typedef struct process *process_t;
typedef pid_t tid_t;

#define __user /**/
typedef __uint128_t u128;
typedef uint64_t u64;
typedef uint32_t u32;
typedef uint16_t u16;
typedef uint8_t u8;

typedef __int128_t i128;
typedef int64_t i64;
typedef int32_t i32;
typedef int16_t i16;
typedef int8_t i8;

typedef uintptr_t vm_offset_t;
typedef intptr_t vm_size_t;

typedef unsigned short umode_t;

typedef u32 compat_size_t;
typedef i32 compat_ssize_t;
typedef i32 compat_clock_t;
typedef i32 compat_pid_t;
typedef u32 compat_ino_t;
typedef i32 compat_off_t;
typedef i64 compat_loff_t;
typedef i32 compat_daddr_t;
typedef i32 compat_timer_t;
typedef i32 compat_key_t;
typedef i16 compat_short_t;
typedef i32 compat_int_t;
typedef i32 compat_long_t;
typedef u16 compat_ushort_t;
typedef u32 compat_uint_t;
typedef u32 compat_ulong_t;
typedef u32 compat_uptr_t;
typedef u32 compat_aio_context_t;

typedef unsigned int qid_t;

typedef struct __user_cap_header_struct
{
        u32 version;
        int pid;
} __user *cap_user_header_t;

typedef struct __user_cap_data_struct
{
        u32 effective;
        u32 permitted;
        u32 inheritable;
} __user *cap_user_data_t;

typedef long __kernel_old_time_t;

typedef unsigned long aio_context_t;

typedef i32 key_serial_t;
typedef u32 key_perm_t;

#define __bitwise /**/

typedef int __bitwise rwf_t;

#ifndef RS64_INIT
#define RS64_INIT /*INIT_SECTION_IN_ELF*/
#endif

#include <kernel/std_types.h>
#include <mach/syslog/syslog.h>

//

#ifdef __cplusplus
extern "C"
{
#endif
__attribute__((used)) __attribute__((noreturn)) void __fatal_panic(char const *function, int line, char const *fmt, ...);
#ifdef __cplusplus
}
#endif

#define panic(...)                                                                                                                 \
        do                                                                                                                         \
        {                                                                                                                          \
                __fatal_panic(__FILENAME__, __LINE__, __VA_ARGS__);                                                                \
                /*log_fatal(__VA_ARGS__); */                                                                                       \
                /*locore_cpu_halt();*/                                                                                             \
        }                                                                                                                          \
        while (0)

#include <kernel/assert.h>
#ifdef __cplusplus
}
#endif

#define RBTNODE_SIZE_TYPE    size_t
#define RBTNODE_UINTPTR_TYPE vm_address_t
#define RBTNODE_INLINE       __attribute__((always_inline))

#endif /* TYPES_H */
