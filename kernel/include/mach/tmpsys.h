#pragma once

#include <poll.h>
#include <signal.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <time.h>

typedef integer_t ssize_t;

typedef struct ksigaction *ksigaction_t;

#define __kernel_old_timeval timeval
#define __kernel_timespec    timespec

#ifdef __cplusplus
extern "C"
{
#endif

struct iovec;

SYSCALL(set_tid_address)
tid_t sys_set_tid_address(pthread_t a_thread, tid_t *a_tid_address);

SYSCALL(ioctl)
kern_return_t sys_ioctl(pthread_t a_thread, int fd, int cmd, natural_t arg);

SYSCALL(writev)
ssize_t sys_writev(pthread_t a_thread, int a_fd, struct iovec const *iovp, int iovcnt);

SYSCALL(write)
ssize_t sys_write(pthread_t a_thread, int a_fd, char const *buf, size_t nbyte);

SYSCALL(read)
ssize_t sys_read(pthread_t a_thread, int fd, void *buf, size_t n_bytes);

SYSCALL(readv)
ssize_t sys_readv(pthread_t a_thread, int fd, struct iovec const *iovp, size_t iovcnt);

SYSCALL(rt_sigprocmask)
kern_return_t sys_rt_sigprocmask(pthread_t a_thread, int a_how, sigset_t *a_nset, sigset_t *a_oset, size_t a_sigsetsize);

SYSCALL(rt_sigaction)
kern_return_t sys_rt_sigaction(int a_signum,
                               ksigaction_t const a_actp,
                               ksigaction_t a_oactp,
                               size_t a_sigsetsize);

SYSCALL(rt_sigsuspend)
kern_return_t sys_rt_sigsuspend(sigset_t *a_sigset, size_t a_sigset_size);

SYSCALL(rt_sigpending)
kern_return_t sys_rt_sigpending(sigset_t *a_sigset, size_t a_sigset_size);

SYSCALL(rt_sigreturn)
kern_return_t sys_rt_sigreturn(pthread_t a_thread);

SYSCALL(fstat)
kern_return_t sys_fstat(pthread_t a_thread, int a_fd, struct stat *a_statbuf);

SYSCALL(newfstatat)
kern_return_t sys_newfstatat(pthread_t a_thread, int dfd, char const *filename, struct stat *statbuf, int flag);

SYSCALL(openat)
kern_return_t sys_openat(pthread_t a_thread, unsigned int a_dirfd, const char *a_fname, int a_flags, mode_t a_mode);

SYSCALL(fcntl)
kern_return_t sys_fcntl(pthread_t a_thread, int a_fd, unsigned a_cmd, unsigned long a_arg);

SYSCALL(getdents64)
kern_return_t sys_getdents64(pthread_t a_thread, int a_fd, struct linux_dirent64 *a_buf, size_t a_count);

SYSCALL(close)
kern_return_t sys_close(pthread_t a_thread, int a_fd);

SYSCALL(fork)
kern_return_t sys_fork(pthread_t a_thread);

SYSCALL(vfork)
kern_return_t sys_vfork(pthread_t a_thread);

SYSCALL(clone)
kern_return_t sys_clone(pthread_t a_thread,
                        unsigned long a_clone_flags,
                        unsigned long a_newsp,
                        int *a_parent_tid,
                        pid_t *a_child_tid,
                        vm_address_t a_tls);

SYSCALL(getpid)
pid_t sys_getpid(pthread_t a_thread);

SYSCALL(gettid)
tid_t sys_gettid(pthread_t a_thread);

SYSCALL(getppid)
pid_t sys_getppid(pthread_t a_thread);

SYSCALL(futex)
kern_return_t sys_futex(uint32_t *a_uaddr,
                        int futex_op,
                        uint32_t val,
                        const struct timespec *timeout, /* or: uint32_t val2 */
                        uint32_t *uaddr2,
                        uint32_t val3);

SYSCALL(wait4)
pid_t sys_wait4(pid_t pid, int *stat_addr, int options, struct rusage *usage);

SYSCALL(arch_prctl)
kern_return_t sys_arch_prctl(pthread_t a_thread, int a_code, vm_address_t a_addr);

SYSCALL(readlink)
kern_return_t sys_readlink(pthread_t a_thread, char const *a_pathp, char *a_buf, int a_count);

SYSCALL(readlinkat)
kern_return_t sys_readlinkat(pthread_t a_thread, int a_dfd, char const *a_path, char *a_buf, int a_bufsize);

SYSCALL(open)
kern_return_t sys_open(pthread_t a_thread, char const *a_pathp, int flags, mode_t mode);

SYSCALL(stat)
kern_return_t sys_stat(pthread_t a_thread, char const *a_pathp, struct stat *a_statp);

SYSCALL(lstat)
kern_return_t sys_lstat(pthread_t a_thread, char const *a_pathp, struct stat *a_statp);

SYSCALL(chdir)
kern_return_t sys_chdir(pthread_t a_thread, char const *a_pathp);

SYSCALL(access)
kern_return_t sys_access(pthread_t a_thread, char const *a_pathp, mode_t a_mode);

SYSCALL(faccessat)
kern_return_t sys_faccessat(pthread_t a_thread, int dfd, char const *a_pathp, mode_t a_mode);

SYSCALL(sync)
kern_return_t sys_sync(pthread_t a_thread);

SYSCALL(mkdir)
kern_return_t sys_mkdir(pthread_t a_thread, char const *a_pathp, mode_t a_mode);

SYSCALL(mkdirat)
kern_return_t sys_mkdirat(pthread_t a_thread, int a_dfd, char const *a_pathp, mode_t a_mode);

SYSCALL(rmdir)
kern_return_t sys_rmdir(pthread_t a_thread, char const *a_pathp);

SYSCALL(creat)
kern_return_t sys_creat(pthread_t a_thread, char const *a_pathp, mode_t a_mode);

SYSCALL(chmod)
kern_return_t sys_chmod(pthread_t a_thread, char const *a_path, mode_t a_mode);

SYSCALL(fchmod)
kern_return_t sys_fchmod(pthread_t a_thread, int a_fd, mode_t a_mode);

SYSCALL(fchmodat)
kern_return_t sys_fchmodat(pthread_t a_thread, int a_dfd, char const *a_path, mode_t a_mode);

SYSCALL(mknod)
kern_return_t sys_mknod(pthread_t a_thread, char const *a_pathp, mode_t a_mode, dev_t a_dev);

SYSCALL(mknodat)
kern_return_t sys_mknodat(pthread_t a_thread, int a_dfd, char const *a_pathp, mode_t a_mode, dev_t a_dev);

SYSCALL(chown)
kern_return_t sys_chown(pthread_t a_thread, char const *a_pathp, uid_t a_uid, gid_t a_gid);

SYSCALL(lchown)
kern_return_t sys_lchown(pthread_t a_thread, char const *a_pathp, uid_t a_uid, gid_t a_gid);

SYSCALL(fchownat)
kern_return_t sys_fchownat(pthread_t a_thread, int a_dfd, char const *a_path, uid_t a_uid, gid_t a_gid, int a_flag);

SYSCALL(renameat2)
kern_return_t sys_renameat2(pthread_t a_thread,
                            int a_old_dfd,
                            char const *a_old_path,
                            int a_new_dfd,
                            char const *a_new_name,
                            int a_flag);

SYSCALL(renameat)
kern_return_t sys_renameat(pthread_t a_thread, int a_old_dfd, char const *a_old_name, int a_new_dfd, char const *a_new_name);

SYSCALL(rename)
kern_return_t sys_rename(pthread_t a_thread, char const *a_fromp, char const *a_top);

SYSCALL(umask)
mode_t sys_umask(pthread_t a_thread, mode_t a_newmask);

SYSCALL(chroot)
kern_return_t sys_chroot(pthread_t a_thread, char const *a_pathp);

SYSCALL(truncate)
kern_return_t sys_truncate(pthread_t a_thread, char const *a_pathp, off_t a_length);

SYSCALL(ftruncate)
kern_return_t sys_ftruncate(pthread_t a_thread, int a_fd, off_t a_length);

SYSCALL(link)
kern_return_t sys_link(pthread_t a_thread, char const *a_pathp, char const *a_linkp);

SYSCALL(linkat)
kern_return_t sys_linkat(pthread_t a_thread,
                         int a_path_dfd,
                         char const *a_path,
                         int a_link_dfd,
                         char const *a_link_path,
                         int a_flags);

SYSCALL(symlink)
kern_return_t sys_symlink(pthread_t a_thread, char const *a_path, char const *a_linkp);

SYSCALL(symlinkat)
kern_return_t sys_symlinkat(pthread_t a_thread, char const *a_path, int a_dfd, char const *a_linkp);

SYSCALL(unlink)
kern_return_t sys_unlink(pthread_t a_thread, char const *a_path);

SYSCALL(statfs)
kern_return_t sys_statfs(pthread_t a_thread, char const *a_pathp, struct statfs *a_buf);

SYSCALL(fstatfs)
kern_return_t sys_fstatfs(pthread_t a_thread, int a_fd, struct statfs *a_buf);

SYSCALL(utimensat)
kern_return_t sys_utimensat(pthread_t a_thread, int a_dfd, char const *a_path, struct timespec *a_utimens, int a_flags);

SYSCALL(futimesat)
kern_return_t sys_futimesat(pthread_t a_thread, int a_dfd, char const *a_path, struct timeval *a_utimes);

SYSCALL(utimes)
kern_return_t sys_utimes(pthread_t a_thread, char const *a_path, struct timeval *a_utimes);

SYSCALL(utime)
kern_return_t sys_utime(pthread_t a_thread, char const *a_path, struct utimbuf *a_time);

SYSCALL(getcwd)
kern_return_t sys_getcwd(pthread_t a_proc, char const *a_buffer, size_t a_buffer_size);

SYSCALL(fsync)
kern_return_t sys_fsync(pthread_t a_thread, int a_fd);

SYSCALL(fchown)
kern_return_t sys_fchown(pthread_t a_thread, int a_fd, uid_t a_uid, gid_t a_gid);

SYSCALL(lseek)
kern_return_t sys_lseek(pthread_t a_thread, int a_fd, off_t a_offset, int a_whence);

SYSCALL(fchdir)
kern_return_t sys_fchdir(pthread_t a_thread, int a_dfd);

SYSCALL(fadvise64)
kern_return_t sys_fadvise64(pthread_t a_thread, int a_fd, off_t a_offset, size_t a_len, int a_advice);

SYSCALL(unlinkat)
kern_return_t sys_unlikat(pthread_t a_thread, int dfd, char const *path, int flags);

SYSCALL(dup)
kern_return_t sys_dup(pthread_t a_thread, int a_fd);

SYSCALL(dup2)
kern_return_t sys_dup2(pthread_t a_thread, int a_oldfd, int a_newfd);

SYSCALL(dup3)
kern_return_t sys_dup3(pthread_t a_thread, int a_oldfd, int a_newfd, int a_flags);

SYSCALL(flock)
kern_return_t sys_flock(pthread_t p, int fd, int cmd);

struct old_utsname;

struct utsname;
SYSCALL(uname)
kern_return_t sys_uname(struct utsname *name);

SYSCALL(poll)
kern_return_t sys_poll(struct pollfd *a_fds, nfds_t a_nfds, int timeout);

SYSCALL(ppoll)
kern_return_t sys_ppoll(struct pollfd *a_fds,
                        nfds_t a_nfds,
                        const struct timespec *a_ts,
                        const sigset_t *a_sigmask,
                        size_t a_sigsetsize);

SYSCALL(splice)
ssize_t sys_splice(int a_fd_in, off_t *a_off_in, int a_fd_out, off_t *a_off_out, size_t a_len, unsigned a_flags);

SYSCALL(getpgrp)
kern_return_t sys_getpgrp(pthread_t a_procp);

SYSCALL(getgroups)
int sys_getgroups(pthread_t a_thread, int a_gidsetsize, gid_t *a_gidset);

SYSCALL(setsid)
pid_t sys_setsid(pthread_t a_thread);

SYSCALL(getsid)
pid_t sys_getsid(pthread_t a_thread, pid_t a_pid);

SYSCALL(setpgid)
pid_t sys_setpgid(pthread_t a_thread, pid_t a_pid, pid_t a_pgid);

SYSCALL(getpgid)
pid_t sys_getpgid(pthread_t a_thread, pid_t a_pid);

SYSCALL(setgroups)
kern_return_t sys_setgroups(pthread_t a_procp, int a_gidsetsize, gid_t *a_gidsetp);

SYSCALL(execve)
kern_return_t sys_execve(char const *filename, char const *const argv[], char const *const envp[]);

SYSCALL(execveat)
kern_return_t sys_execveat(int a_dfd, char const *filename, char const *const argv[], char const *const envp[], int a_flags);

SYSCALL(sigaltstack)
kern_return_t sys_sigaltstack(pthread_t a_procp, const stack_t *a_ss, stack_t *a_oss);

SYSCALL(exit)
kern_return_t sys_exit(pthread_t a_thread, int a_exit_code);

SYSCALL(exit_group)
kern_return_t sys_exit_group(int a_exit_code);

SYSCALL(kill)
kern_return_t sys_kill(pid_t a_pid, int a_signo);

SYSCALL(tkill)
kern_return_t sys_tkill(pid_t a_tid, int a_sig);

SYSCALL(tgkill)
kern_return_t sys_tgkill(pid_t a_pid, pid_t a_tid, int a_sig);

SYSCALL(prlimit64)
kern_return_t sys_prlimit64(pthread_t a_thread,
                            pid_t a_pid,
                            unsigned int resource,
                            const struct rlimit *a_new_rlim,
                            struct rlimit *a_old_rlim);

SYSCALL(getrusage)
kern_return_t sys_getrusage(pthread_t a_thread, int a_who, struct rusage *a_rusage);

SYSCALL(sysinfo)
kern_return_t sys_sysinfo(struct sysinfo *a_sysinfo);

SYSCALL(setitimer)
kern_return_t sys_setitimer(int a_which, struct itimerval *a_timer_in, struct itimerval *a_timer_out);

SYSCALL(getitimer)
kern_return_t sys_getitimer(int a_which, struct itimerval *a_timer_out);

SYSCALL(getrandom)
natural_t sys_getrandom(pthread_t a_proc, char *a_buffer, size_t a_count, int a_flags);

SYSCALL(sched_getaffinity)
kern_return_t sys_sched_getaffinity(pthread_t a_thread, pid_t a_pid, size_t a_len, unsigned long *a_user_ptr_mask);

SYSCALL(sched_yield)
kern_return_t sys_sched_yield(pthread_t a_proc);

SYSCALL(clock_gettime)
kern_return_t sys_clock_gettime(pthread_t a_procp, clockid_t a_clock, struct timespec *tp);

SYSCALL(nanosleep)
kern_return_t sys_nanosleep(pthread_t a_procp, struct timespec const *a_rqtp, struct timespec *a_rmtp);

SYSCALL(timer_gettime)
kern_return_t sys_timer_gettime(pthread_t a_procp, timer_t timer_id, struct itimerspec *setting);

SYSCALL(getuid) uid_t sys_getuid(pthread_t a_thread);

SYSCALL(geteuid)
uid_t sys_geteuid(pthread_t a_thread);

SYSCALL(getgid)
gid_t sys_getgid(pthread_t a_thread);

/*
 * Get effective group ID.  The "egid" is groups[0], and could be obtained
 * via getgroups.  This syscall exists because it is somewhat painful to do
 * correctly in a library function.
 */
SYSCALL(getegid)
gid_t sys_getegid(pthread_t a_thread);

SYSCALL(setuid)
kern_return_t sys_setuid(pthread_t a_thread, uid_t a_uid);

SYSCALL(setgid)
kern_return_t sys_setgid(pthread_t a_procp, gid_t a_gid);

SYSCALL(pipe)
kern_return_t sys_pipe(pthread_t a_thread, int *a_fildes);

SYSCALL(pipe2)
kern_return_t sys_pipe2(pthread_t a_thread, int *a_fildes, int a_flags);

SYSCALL(membarrier)
kern_return_t sys_membarrier(pthread_t a_proc, int a_cmd, unsigned int flags);

SYSCALL(select)
kern_return_t sys_select(int a_count, fd_set *a_rfds, fd_set *a_wfds, fd_set *a_efts, struct timeval *a_tv);

struct pselect6_data;
SYSCALL(pselect6)
kern_return_t sys_pselect6(int a_count,
                           fd_set *a_rfds,
                           fd_set *a_wfds,
                           fd_set *a_efts,
                           struct timespec *a_ts,
                           const struct pselect6_data *a_mask);

SYSCALL(brk)
vm_address_t sys_brk(pthread_t a_procp, vm_address_t a_uaddr);

SYSCALL(madvise)
int sys_madvise(pthread_t a_procp, vm_address_t a_ustart, size_t a_length, int a_behavior);

SYSCALL(msync)
int sys_msync(pthread_t a_procp, vm_address_t a_address, size_t a_length, int a_flags);

SYSCALL(mmap)
vm_address_t sys_mmap(pthread_t a_procp, vm_address_t a_addr, size_t a_len, int a_prot, int a_flags, int a_fd, off_t a_pgoff);

SYSCALL(mprotect)
int sys_mprotect(pthread_t a_procp, vm_address_t a_addr, size_t a_len, int a_prot);

SYSCALL(munmap)
int sys_munmap(pthread_t a_procp, vm_address_t a_addr, size_t a_len);

SYSCALL(mremap)
vm_address_t sys_mremap(pthread_t a_proc,
                        vm_address_t a_old_address,
                        size_t a_old_size,
                        size_t new_size,
                        natural_t a_flags,
                        vm_address_t a_new_address);

SYSCALL(mlock) int sys_mlock(pthread_t, vm_address_t a_addr, size_t a_len);

SYSCALL(mlockall)
int sys_mlockall(pthread_t, int a_flags);

SYSCALL(munlock)
int sys_munlock(pthread_t, vm_address_t a_addr, size_t a_len);

SYSCALL(munlockall)
int sys_munlockall(pthread_t);

SYSCALL(socket)
kern_return_t sys_socket(int a_family, int a_type, int a_proto);

SYSCALL(connect)
kern_return_t sys_connect(int a_fd, struct sockaddr const *a_addr, socklen_t a_addrlen);

SYSCALL(accept)
kern_return_t sys_accept(int a_fd, struct sockaddr *a_peer, socklen_t *a_peer_addrlen);

SYSCALL(accept4)
kern_return_t sys_accept4(int a_fd, struct sockaddr *a_peer, socklen_t *a_peer_addrlen, int a_flags);

SYSCALL(sendto)
kern_return_t sys_sendto(int a_fd,
                         void *a_buffer,
                         size_t a_buffer_len,
                         int a_flags,
                         struct sockaddr const *a_addr,
                         socklen_t a_addrlen);

SYSCALL(recvfrom)
kern_return_t sys_recvfrom(int a_fd,
                           void *a_buffer,
                           size_t a_buffer_len,
                           int a_flags,
                           struct sockaddr *a_addr,
                           socklen_t *a_addrlen);

SYSCALL(sendmsg)
kern_return_t sys_sendmsg(int a_fd, struct msghdr const *a_msg, int a_flags);

SYSCALL(recvmsg)
kern_return_t sys_recvmsg(int a_fd, struct msghdr const *a_msg, int a_flags);

SYSCALL(shutdown)
kern_return_t sys_shutdown(int a_fd, int a_how);

SYSCALL(bind)
kern_return_t sys_bind(int a_fd, struct sockaddr *a_addr, socklen_t a_addrlen);

SYSCALL(listen)
kern_return_t sys_listen(int a_fd, int a_backlog);

SYSCALL(getsockname)
kern_return_t sys_getsockname(int a_fd, struct sockaddr *a_addr, socklen_t *a_addrlen);

SYSCALL(getpeername)
kern_return_t sys_getpeername(int a_fd, struct sockaddr *a_addr, socklen_t *a_addrlen);

SYSCALL(socketpair)
kern_return_t sys_socketpair(int a_family, int a_type, int a_proto, int a_socket_vector[2]);

SYSCALL(setsockopt)
kern_return_t sys_setsockopt(int a_fd, int a_level, int a_opt_name, char const *a_opt_value, socklen_t a_opt_value_len);

SYSCALL(getsockopt)
kern_return_t sys_getsockopt(int a_fd, int a_level, int a_opt_name, char const *a_opt_value, socklen_t *a_opt_value_len);

#ifdef __cplusplus
}
#endif
