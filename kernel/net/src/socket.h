#pragma once
#include <sys/socket.h>

#if !defined(__NEED_vfs_node)
#define __NEED_vfs_node
#endif

#include <emerixx/vfs/vfs_types.h>

#include "domain.h"

typedef struct socket *socket_t;

#define SOCKET_CANACCEPT BIT0
#define SOCKET_NONBLOCK  BIT1
#define SOCKET_CANREAD   BIT2
#define SOCKET_CANWRITE  BIT3
#define SOCKET_HANGUP    BIT4

struct socket
{
        net_proto_t m_proto;
        void *m_proto_private;
        sockaddr *m_sa;
        sockaddr *m_peer_sa;
        integer_t m_flags;
        natural_t m_socket_id;
};

#include <emerixx/memory_rw.h>

struct socket_operations_data
{
        kern_return_t (*bind)(socket_t a_so, struct sockaddr *a_addr, socklen_t a_addrlen);

        kern_return_t (*listen)(socket_t a_so, integer_t a_oflags, integer_t a_backlog);

        kern_return_t (*accept)(socket_t a_so, integer_t a_so_oflags, socket_t a_newso);

        kern_return_t (*connect)(socket_t a_so, integer_t a_oflags, struct sockaddr *a_addr, socklen_t a_addrlen);

        kern_return_t (*sendto)(socket_t a_so,
                                integer_t a_so_oflags,
                                memory_rdwr_t a_mrw,
                                int a_flags,
                                struct sockaddr *a_addr,
                                socklen_t *a_addrlen);

        kern_return_t (*recvfrom)(socket_t a_so,
                                  integer_t a_so_oflags,
                                  memory_rdwr_t a_mrw,
                                  int a_flags,
                                  struct sockaddr *a_addr,
                                  socklen_t *a_addrlen);

        integer_t (*poll)(socket_t a_so, integer_t a_events);

        kern_return_t (*close)(socket_t a_so);

        kern_return_t (*shutdown)(socket_t a_so, integer_t a_how);
};

vfs_node_t socket_to_vfsn(socket_t a_socket);

void socket_ref(socket_t a_socket);

void socket_rel(socket_t a_socket);

void socket_lock(socket_t a_socket);

void socket_unlock(socket_t a_socket);

boolean_t socket_lock_try(socket_t a_socket);

socket_t socket_find(natural_t a_socket_id);

void socket_poll_thread_nudge();
