#define RBTNODE_FUNCTION_DEFINITIONS

#include "domain.h"

#include <aux/init.h>

#define DOMAIN_LOOKUP(domno) (rbtnode_find<integer_t, net_domain_t>((domno), s_rb_root))
#define DOMAIN_INSERT(dom)   (rbtnode_insert<net_domain_t>((dom), &s_rb_root, &s_rb_sentinel))
#define DOMAIN_FIRST()       (rbt_first<net_domain_t>(s_rb_root))
#define DOMAIN_NEXT(dom)     (rbt_next<net_domain_t>((dom)))

static rbtnode_data s_rb_sentinel;
static rbtnode_t s_rb_root;

template <>
// ********************************************************************************************************************************
net_domain_t rbtnode_to_type<net_domain_t>(rbtnode_t a_node)
// ********************************************************************************************************************************
{
        return rbtnode_type(a_node, net_domain_data, m_link);
}

template <>
// ********************************************************************************************************************************
rbtnode_t type_to_rbtnode<net_domain_t>(net_domain_t a_domain)
// ********************************************************************************************************************************
{
        return &a_domain->m_link;
}

template <>
// ********************************************************************************************************************************
bool rbtnode_is_less<integer_t, net_domain_t>(integer_t a_lhs, net_domain_t a_rhs)
// ********************************************************************************************************************************
{
        return a_lhs < a_rhs->m_domain;
}

template <>
// ********************************************************************************************************************************
bool rbtnode_is_less<net_domain_t, integer_t>(net_domain_t a_lhs, integer_t a_rhs)
// ********************************************************************************************************************************
{
        return a_lhs->m_domain < a_rhs;
}

template <>
// ********************************************************************************************************************************
bool rbtnode_is_less<net_domain_t, net_domain_t>(net_domain_t a_lhs, net_domain_t a_rhs)
// ********************************************************************************************************************************
{
        return a_lhs->m_domain < a_rhs->m_domain;
}

// ********************************************************************************************************************************
kern_return_t domain_register(net_domain_t a_domain)
// ********************************************************************************************************************************
{
        for (net_proto_t p = a_domain->m_proto_beg; p < a_domain->m_proto_end; p++)
        {
                p->m_domain = a_domain;
        }

        KASSERT(DOMAIN_INSERT(a_domain) == a_domain);

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t domain_find_proto(integer_t a_dom, integer_t a_type, integer_t a_proto, net_proto_t *a_proto_out)
// ********************************************************************************************************************************
{
        net_domain_t dom = DOMAIN_LOOKUP(a_dom);

        if (dom == nullptr)
        {
                return KERN_FAIL(EAFNOSUPPORT);
        }

        for (net_proto_t p = dom->m_proto_beg; p < dom->m_proto_end; p++)
        {
                if ((a_type == p->m_type) && (a_proto == p->m_protocol))
                {
                        *a_proto_out = p;
                        return KERN_SUCCESS;
                }
        }

        return KERN_FAIL(EPROTONOSUPPORT);
}

// ********************************************************************************************************************************
void domain_do_timeouts(natural_t a_ms_elapsed)
// ********************************************************************************************************************************
{
        for (net_domain_t dom = DOMAIN_FIRST(); dom != nullptr; dom = DOMAIN_NEXT(dom))
        {
                for (net_proto_t p = dom->m_proto_beg; p < dom->m_proto_end; p++)
                {
#if 0
                        if (p->m_timeout)
                        {
                                p->m_timeout(a_ms_elapsed);
                        }
#endif
                }
        }
}

// ********************************************************************************************************************************
static kern_return_t domain_init()
// ********************************************************************************************************************************
{
        s_rb_root = rbtnode_init_sentinel(&s_rb_sentinel);

        return KERN_SUCCESS;
}

early_initcall(domain_init);
