#include <lwip/netif.h>
#include <lwip/etharp.h>

#include <aux/init.h>

#include <etl/queue.h>
#include <etl/algorithm.hh>

#include <emerixx/net/iface.h>
#include <emerixx/net/iface_data.h>

#define IFACE_INTERNAL(p) ((struct iface_data_internal *)(p))

enum iface_chain_links : natural_t
{
        IFACE_LIST_ALL_IFACE = 0,
        IFACE_LIST_FREE_LIST = IFACE_LIST_ALL_IFACE,
        IFACE_LIST_LAST
};

struct iface_data_internal
{
        struct iface_data m_iface;
        struct netif m_lwip_netif;
        queue_chain_t m_links[IFACE_LIST_LAST];
        natural_t m_private_size;
        natural_t m_private_align;
        int8_t m_private_data[0];
};

static struct
{
        iface_data_internal m_iface;
        int8_t m_private_storage[0x400 - sizeof(m_iface)];
} s_iface_storage[0x10];

static queue_head_t s_iface_lists[IFACE_LIST_LAST];

#define iface_private_data_storage_size(align)                                                                                     \
        ((sizeof(s_iface_storage[0].m_private_storage))                                                                            \
         - (natural_t)(etl::round_up(s_iface_storage[0].m_private_storage, align) - s_iface_storage[0].m_private_storage))

// ********************************************************************************************************************************
iface_t iface_alloc(natural_t a_private_size, natural_t a_private_align)
// ********************************************************************************************************************************
{
        if (iface_private_data_storage_size(a_private_align) < a_private_size)
        {
                return nullptr;
        }

        if (queue_empty(&s_iface_lists[IFACE_LIST_FREE_LIST]))
        {
                return nullptr;
        }

        queue_entry_t qentry = dequeue_head(&s_iface_lists[IFACE_LIST_FREE_LIST]);

        struct iface_data_internal *iface
                = queue_containing_record(qentry, struct iface_data_internal, m_links[IFACE_LIST_FREE_LIST]);

        *iface = {};

        iface->m_private_size  = a_private_size;
        iface->m_private_align = a_private_align;

        iface->m_iface.m_mac_addr = iface->m_lwip_netif.hwaddr;

        iface->m_lwip_netif.hwaddr_len = sizeof(iface->m_lwip_netif.hwaddr);

        enqueue_tail(&s_iface_lists[IFACE_LIST_ALL_IFACE], &iface->m_links[IFACE_LIST_ALL_IFACE]);

        return &iface->m_iface;
}

// ********************************************************************************************************************************
static err_t lwip_netif_init_internal(struct netif *a_netif)
// ********************************************************************************************************************************
{
        extern err_t netif_output(struct netif * a_netif, struct pbuf * p);

        a_netif->linkoutput = netif_output;
        a_netif->output     = etharp_output;
        // a_netif->output_ip6 = ethip6_output;
        a_netif->mtu   = 1500; // ETHERNET_MTU;
        a_netif->flags = NETIF_FLAG_BROADCAST | NETIF_FLAG_ETHARP | NETIF_FLAG_ETHERNET | NETIF_FLAG_IGMP | NETIF_FLAG_MLD6;
        // MIB2_INIT_NETIF(netif, snmp_ifType_ethernet_csmacd, 100000000);

        // uint8_t mac[] = { 0x52, 0x54, 0x00, 0x12, 0x34, 0x56 };

        // SMEMCPY(a_netif->hwaddr, mac, ETH_HWADDR_LEN);
        // a_netif->hwaddr_len = ETH_HWADDR_LEN;

        return ERR_OK;
}

// ********************************************************************************************************************************
void iface_add(iface_t a_iface)
// ********************************************************************************************************************************
{
        struct netif *nif = &IFACE_INTERNAL(a_iface)->m_lwip_netif;

        ip4_addr_t ipaddr, netmask, gw;
        IP4_ADDR(&gw, 10, 1, 1, 1);
        IP4_ADDR(&ipaddr, 10, 1, 1, 2);
        IP4_ADDR(&netmask, 255, 255, 255, 0);

        netif_add(nif, &ipaddr, &netmask, &gw, nullptr, lwip_netif_init_internal, netif_input);
        nif->name[0] = 'e';
        nif->name[1] = '0';
        // netif_create_ip6_linklocal_address(&netif, 1);
        // netif_set_status_callback(&netif, netif_status_callback);
        netif_set_default(nif);
        netif_set_up(nif);
        netif_set_link_up(nif);
}

// ********************************************************************************************************************************
iface_private_t iface_private(iface_t a_iface)
// ********************************************************************************************************************************
{
        return etl::round_up(IFACE_INTERNAL(a_iface)->m_private_data, IFACE_INTERNAL(a_iface)->m_private_align);
}

// ********************************************************************************************************************************
struct netif *iface_lwip_netif(iface_t a_iface)
// ********************************************************************************************************************************
{
        return &IFACE_INTERNAL(a_iface)->m_lwip_netif;
}

// ********************************************************************************************************************************
iface_t lwip_netif_iface(struct netif *a_netif)
// ********************************************************************************************************************************
{
        return &queue_containing_record(a_netif, struct iface_data_internal, m_lwip_netif)->m_iface;
}

// ********************************************************************************************************************************
static kern_return_t iface_subsys_init()
// ********************************************************************************************************************************
{
        for (queue_head_t &q : s_iface_lists)
        {
                queue_init(&q);
        }

        for (auto &e : s_iface_storage)
        {
                enqueue_tail(&s_iface_lists[IFACE_LIST_FREE_LIST], &e.m_iface.m_links[IFACE_LIST_FREE_LIST]);
        }

        return KERN_SUCCESS;
}

subsys_initcall(iface_subsys_init);
