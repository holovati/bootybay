#include <lwip/pbuf.h>

#include <aux/init.h>

#include <etl/queue.h>

#include <kernel/vm/pmap.h>
#include <kernel/vm/vm_kern.h>

#include <emerixx/net/iface_buffer.h>
#include <emerixx/net/iface_buffer_data.h>

#define N_NETDEVICE_BUFFERS (0x1000000 / PAGE_SIZE)

struct iface_buffer_data_internal
{
        iface_buffer_data m_iface_buffer;
        struct pbuf_custom m_lwip_pbuf;
        queue_chain_t m_freelist_link;
};

static queue_head_t s_iface_buffer_freelist;

static int s_bufs_in_use;

// ********************************************************************************************************************************
static void iface_buffer_free_pbuf_internal(struct pbuf *a_pbuf)
// ********************************************************************************************************************************
{
        struct iface_buffer_data_internal *ifbuf = queue_containing_record(a_pbuf, struct iface_buffer_data_internal, m_lwip_pbuf);

        s_bufs_in_use--;

        enqueue_tail(&s_iface_buffer_freelist, &ifbuf->m_freelist_link);
}

// ********************************************************************************************************************************
kern_return_t iface_buffer_alloc(iface_t a_iface, iface_buffer_t *a_iface_buffer_out)
// ********************************************************************************************************************************
{
        if (queue_empty(&s_iface_buffer_freelist))
        {
                return KERN_FAIL(ENOBUFS);
        }

        queue_entry_t qentry = dequeue_head(&s_iface_buffer_freelist);

        struct iface_buffer_data_internal *ifbuf
                = queue_containing_record(qentry, struct iface_buffer_data_internal, m_freelist_link);

        ifbuf->m_iface_buffer.m_iface = a_iface;

        pbuf_alloced_custom(PBUF_RAW,
                            0,
                            PBUF_RAM,
                            &ifbuf->m_lwip_pbuf,
                            (void *)ifbuf->m_iface_buffer.m_virt_addr,
                            ifbuf->m_iface_buffer.m_buffer_size);
        // p->ref = 1;

        ifbuf->m_iface_buffer.m_packet_size = 0; // = p->tot_len = p->len = 0;

        ifbuf->m_iface_buffer.m_data_offset = 0;

        *a_iface_buffer_out = &ifbuf->m_iface_buffer;

        s_bufs_in_use++;

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
void iface_buffer_free(iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        s_bufs_in_use--;
        enqueue_tail(&s_iface_buffer_freelist, &((struct iface_buffer_data_internal *)(a_iface_buffer))->m_freelist_link);
}

// ********************************************************************************************************************************
struct pbuf *iface_buffer_lwip_pbuf(iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        return &((struct iface_buffer_data_internal *)(a_iface_buffer))->m_lwip_pbuf.pbuf;
}

// ********************************************************************************************************************************
iface_buffer_t iface_buffer_from_lwip_pbuf(struct pbuf *a_pbuf)
// ********************************************************************************************************************************
{
        return &queue_containing_record(a_pbuf, struct iface_buffer_data_internal, m_lwip_pbuf)->m_iface_buffer;
}

int ___nbufs;
// ********************************************************************************************************************************
static kern_return_t iface_buffer_subsys_init()
// ********************************************************************************************************************************
{
        s_bufs_in_use = 0;

        queue_init(&s_iface_buffer_freelist);

        struct iface_buffer_data_internal *ndbufs = nullptr;

        kmem_alloc_wired(kernel_map, (vm_address_t *)&ndbufs, sizeof(*ndbufs) * N_NETDEVICE_BUFFERS);

        KASSERT(ndbufs != nullptr);

        vm_address_t ndbuf_data = 0;

        kmem_alloc_wired(kernel_map, &ndbuf_data, PAGE_SIZE * N_NETDEVICE_BUFFERS);

        KASSERT(ndbuf_data != 0);

        ___nbufs = 0;
        for (uint32_t i = 0; i < N_NETDEVICE_BUFFERS; ++i)
        {
                ndbufs[i].m_iface_buffer.m_virt_addr   = ndbuf_data + (i * PAGE_SIZE);
                ndbufs[i].m_iface_buffer.m_phys_addr   = pmap_extract(kernel_pmap, ndbufs[i].m_iface_buffer.m_virt_addr);
                ndbufs[i].m_iface_buffer.m_buffer_size = PAGE_SIZE;
                ndbufs[i].m_iface_buffer.m_packet_size = 0;

                ndbufs[i].m_lwip_pbuf.custom_free_function = iface_buffer_free_pbuf_internal;

                struct pbuf *p = &ndbufs[i].m_lwip_pbuf.pbuf;

                p->next          = nullptr;
                p->payload       = (void *)ndbufs[i].m_iface_buffer.m_virt_addr;
                p->tot_len       = 0;
                p->len           = 0;
                p->type_internal = (u8_t)PBUF_RAM;
                p->flags         = PBUF_FLAG_IS_CUSTOM;
                p->ref           = 1;
                p->if_idx        = 0;

                // queue_init(&ndbufs[i].ifbuf.m_dev_link);
                // ENQUEUE_NETDEVICE_BUFFER(&s_netdev_buf_freelist, &ndbufs[i].ifbuf);

                enqueue_tail(&s_iface_buffer_freelist, &ndbufs[i].m_freelist_link);

                ___nbufs++;
        }

        return KERN_SUCCESS;
}

subsys_initcall(iface_buffer_subsys_init);
