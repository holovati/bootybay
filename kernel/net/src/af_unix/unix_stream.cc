#include <string.h>

#include <fcntl.h>
#include <poll.h>
#include <sys/un.h>
#include <sys/stat.h>

#include <etl/algorithm.hh>

#include <emerixx/vfs/vfs_util.h>
#include <emerixx/vfs/vfs_node.h>
#include <emerixx/pipe.h>

#include "domain.h"
#include "socket.h"

#define BUFFER_SIZE (1 << 21) // 2 MiB

#define SOCKET_TO_UNIX_STREAM(so) ((unix_stream_t)((so)->m_proto_private))

typedef struct unix_stream_data *unix_stream_t;

enum unix_socket_state : integer_t
{
        UNIX_SOCKET_STATE_CLOSED = 0,
        UNIX_SOCKET_STATE_BOUND,
        UNIX_SOCKET_STATE_LISTENING,
        UNIX_SOCKET_STATE_ESTABLISHED,
};

struct unix_stream_data
{
        enum unix_socket_state m_state;

        socket_t m_socket;

        union
        {
                struct
                {
                        pipe_t read;
                        pipe_t write;
                } m_channel;

                pipe_t m_channel_array[2];
        };

        union
        {
                struct
                {
                } closed;

                struct
                {
                        vfs_node_t bind_vfsn;
                } bound;

                struct
                {
                } listening;

                struct
                {
                } established;

        } m_state_ctx;
};

struct unix_stream_connection_req
{
        natural_t socket_id;
        pipe_t read;
        pipe_t write;
        natural_t pad; // Must be power of 2
};

static_assert(etl::is_power_of_two(sizeof(struct unix_stream_connection_req)));

// ********************************************************************************************************************************
static inline void unix_stream_change_state(unix_stream_t a_uns, enum unix_socket_state a_state)
// ********************************************************************************************************************************
{
        a_uns->m_state = a_state;
}

// ********************************************************************************************************************************
static kern_return_t unix_stream_find_listen_socket(struct sockaddr_un *a_sun, socket_t *a_sock_out)
// ********************************************************************************************************************************
{
        char const *path  = a_sun->sun_path;
        natural_t pathlen = strnlen(a_sun->sun_path, ARRAY_SIZE(a_sun->sun_path));

        vfs_node_t vfsn      = nullptr;
        kern_return_t retval = vfs_util_simple_lookup_krn_path_at_cwd(path, pathlen, &vfsn, true, true /* Want lock */);

        if (KERN_STATUS_FAILURE(retval))
        {
                if (retval == KERN_FAIL(ENOENT))
                {
                        retval = KERN_FAIL(ECONNREFUSED);
                }
                return retval;
        }

        natural_t socket_id = 0;

        retval = vfs_node_socket_id_get(vfsn, &socket_id);

        if (KERN_STATUS_FAILURE(retval))
        {
                vfs_node_put(vfsn);
                return retval;
        }

        socket_t listensock = socket_find(socket_id);

        if (listensock == nullptr)
        {
                vfs_node_put(vfsn);
                return KERN_FAIL(ECONNREFUSED);
        }
        else
        {
                *a_sock_out = listensock;
        }

        vfs_node_put(vfsn);

        return retval;
}

// ********************************************************************************************************************************
static kern_return_t unix_stream_socket_create(socket_t a_so)
// ********************************************************************************************************************************
{
        unix_stream_t uns = SOCKET_TO_UNIX_STREAM(a_so);

        uns->m_socket = a_so;

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static void unix_stream_socket_destroy(socket_t a_so)
// ********************************************************************************************************************************
{
        unix_stream_t uns = SOCKET_TO_UNIX_STREAM(a_so);

        KASSERT(uns->m_state == UNIX_SOCKET_STATE_CLOSED);

        for (pipe_t pipe : uns->m_channel_array)
        {
                if (pipe != nullptr)
                {
                        panic("Should be gone!");
                }
        }
}

// ********************************************************************************************************************************
static kern_return_t unix_stream_bind(socket_t a_so, struct sockaddr *a_addr, socklen_t a_addrlen)
// ********************************************************************************************************************************
{
        unix_stream_t uns = SOCKET_TO_UNIX_STREAM(a_so);

        if (uns->m_state != UNIX_SOCKET_STATE_CLOSED)
        {
                return KERN_FAIL(EINVAL);
        }

        struct sockaddr_un *sun = (struct sockaddr_un *)(a_addr);

        char const *path   = sun->sun_path;
        natural_t path_len = strnlen(sun->sun_path, ARRAY_SIZE(sun->sun_path));

        kern_return_t retval
                = vfs_util_create_file_simple(path, path_len, S_IFSOCK | S_IRUSR | S_IWUSR, &uns->m_state_ctx.bound.bind_vfsn);

        if (KERN_STATUS_FAILURE(retval))
        {
                if (retval == KERN_FAIL(EEXIST))
                {
                        retval = KERN_FAIL(EADDRINUSE);
                }
                return retval;
        }

        retval = vfs_node_socket_id_set(uns->m_state_ctx.bound.bind_vfsn, a_so->m_socket_id);

        if (KERN_STATUS_SUCCESS(retval))
        {
                unix_stream_change_state(uns, UNIX_SOCKET_STATE_BOUND);
                vfs_node_unlock(uns->m_state_ctx.bound.bind_vfsn);
        }
        else
        {
                vfs_node_put(uns->m_state_ctx.bound.bind_vfsn);
                uns->m_state_ctx.bound.bind_vfsn = nullptr;
        }

        return retval;
}

// ********************************************************************************************************************************
static kern_return_t unix_stream_listen(socket_t a_so, integer_t a_oflags, integer_t a_maxconn)
// ********************************************************************************************************************************
{
        unix_stream_t uns = SOCKET_TO_UNIX_STREAM(a_so);

        kern_return_t retval;
        if (uns->m_state == UNIX_SOCKET_STATE_BOUND)
        {
                // Create the read pipe for incoming connections. connecting socket ids get written here.
                retval = pipe_create(etl::round_next_pow2(a_maxconn - 1) * sizeof(struct unix_stream_connection_req),
                                     &uns->m_channel.read);

                if (KERN_STATUS_SUCCESS(retval))
                {
                        unix_stream_change_state(uns, UNIX_SOCKET_STATE_LISTENING);
                }
        }
        else
        {
                // Must be bound to a address first
                retval = KERN_FAIL(EOPNOTSUPP);
        }

        return retval;
}

// ********************************************************************************************************************************
static kern_return_t unix_stream_accept(socket_t a_so, integer_t a_so_oflags, socket_t a_newso)
// ********************************************************************************************************************************
{
        unix_stream_t uns = SOCKET_TO_UNIX_STREAM(a_so);

        if (uns->m_state != UNIX_SOCKET_STATE_LISTENING)
        {
                return KERN_FAIL(EINVAL);
        }

        struct unix_stream_connection_req req = {};

        kern_return_t retval;
        do
        {
                retval = pipe_read(uns->m_channel.read, &req, sizeof(req), !(a_so_oflags & O_NONBLOCK));

                if (retval > 0)
                {
                        KASSERT(retval == sizeof(req)); // We got 1 element as we requested.
                        break;
                }
        }
        while (KERN_STATUS_SUCCESS(retval));

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        // Make sure the peer is still here
        socket_t peerso = socket_find(req.socket_id);

        if (peerso == nullptr)
        {
                // The peer is gone
                pipe_rel(req.read);
                pipe_rel(req.write);
                return KERN_FAIL(ECONNABORTED);
        }

        // Initialize the new socket no locking needed as no one knows about it.
        unix_stream_t new_uns = SOCKET_TO_UNIX_STREAM(a_newso);

        new_uns->m_channel.read  = req.write;
        new_uns->m_channel.write = req.read;

        unix_stream_change_state(new_uns, UNIX_SOCKET_STATE_ESTABLISHED);

        socket_rel(peerso);

        return KERN_SUCCESS;
}
// ********************************************************************************************************************************
static kern_return_t unix_stream_connect(socket_t a_so, integer_t a_oflags, struct sockaddr *a_addr, socklen_t a_addrlen)
// ********************************************************************************************************************************
{
        unix_stream_t uns = SOCKET_TO_UNIX_STREAM(a_so);

        if (uns->m_state != UNIX_SOCKET_STATE_CLOSED)
        {
                if (uns->m_state == UNIX_SOCKET_STATE_ESTABLISHED)
                {
                        return KERN_FAIL(EISCONN);
                }
                else
                {
                        return KERN_FAIL(EALREADY);
                }
        }

        socket_t listenso = nullptr;

        kern_return_t retval = unix_stream_find_listen_socket((struct sockaddr_un *)(a_addr), &listenso);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        vfs_node_t listenvfsn = socket_to_vfsn(listenso);
        vfs_node_lock(listenvfsn); // Nobody know about the connecting socket yet, so its ok to do this.

        unix_stream_t listenuns = SOCKET_TO_UNIX_STREAM(listenso);

        if (listenuns->m_state != UNIX_SOCKET_STATE_LISTENING)
        {
                // It misght have closed while we were grabbing the lock
                vfs_node_put(listenvfsn); // same as unlock + socket_rel(listenso);
                return KERN_FAIL(ECONNREFUSED);
        }

        for (pipe_t &pipe : uns->m_channel_array)
        {
                retval = pipe_create(BUFFER_SIZE, &pipe);
                if (KERN_STATUS_FAILURE(retval))
                {
                        break;
                }
        }

        if (KERN_STATUS_FAILURE(retval))
        {
        fail_exit:
                for (pipe_t &pipe : uns->m_channel_array)
                {
                        if (pipe != nullptr)
                        {
                                pipe_rel(pipe);
                        }
                }
                vfs_node_put(listenvfsn); // same as unlock + socket_rel(listenso);

                return retval;
        }

        struct unix_stream_connection_req req = {};

        req.socket_id = a_so->m_socket_id;
        pipe_ref((req.read = uns->m_channel.read));
        pipe_ref((req.write = uns->m_channel.write));

        retval = pipe_write(listenuns->m_channel.read, &req, sizeof(req), sizeof(req), !(a_oflags & O_NONBLOCK));

        if (KERN_STATUS_FAILURE(retval))
        {
                pipe_rel(req.read);
                pipe_rel(req.write);
                goto fail_exit;
        }
        else
        {
                KASSERT(retval == sizeof(req)); // We sent one
                retval = 0;
        }

        vfs_node_put(listenvfsn); // same as unlock + socket_rel(listenso);

        unix_stream_change_state(uns, UNIX_SOCKET_STATE_ESTABLISHED);

        return retval;
}

// ********************************************************************************************************************************
static kern_return_t unix_stream_sendto_recvfrom(socket_t a_so,
                                                 integer_t a_oflags,
                                                 memory_rdwr_t a_mrw,
                                                 int a_flags,
                                                 struct sockaddr *a_addr,
                                                 socklen_t *a_addrlen)
// ********************************************************************************************************************************
{
        unix_stream_t uns = SOCKET_TO_UNIX_STREAM(a_so);

        if (uns->m_state != UNIX_SOCKET_STATE_ESTABLISHED)
        {
                return KERN_FAIL(ENOTCONN);
        }

        boolean_t can_sleep = !(a_oflags & O_NONBLOCK);

        boolean_t is_write = a_mrw->uio_rw == UIO_WRITE;

        kern_return_t retval = KERN_SUCCESS;

        natural_t xfer_count;

        do
        {
                xfer_count = 0;

                retval = pipe_rw(uns->m_channel_array[is_write], a_mrw, 1, 0, can_sleep, &xfer_count);
        }
        while ((a_mrw->uio_resid > 0) && KERN_STATUS_SUCCESS(retval) && (xfer_count > 0));

        return retval;
}

// ********************************************************************************************************************************
static integer_t unix_stream_poll(socket_t a_so, integer_t a_events)
// ********************************************************************************************************************************
{
        unix_stream_t uns = SOCKET_TO_UNIX_STREAM(a_so);

        integer_t revents = 0;

        switch (uns->m_state)
        {
                case UNIX_SOCKET_STATE_CLOSED:
                {
                        break;
                }
                case UNIX_SOCKET_STATE_BOUND:
                {
                        break;
                }

                case UNIX_SOCKET_STATE_LISTENING:
                {
                        if (a_events & (POLLIN | POLLRDNORM))
                        {
                                if (pipe_read_available(uns->m_channel.read))
                                {
                                        revents |= (POLLIN | POLLRDNORM);
                                }
                        }
                        break;
                }

                case UNIX_SOCKET_STATE_ESTABLISHED:
                {
                        if (a_events & (POLLIN | POLLRDNORM))
                        {
                                if (pipe_read_available(uns->m_channel.read))
                                {
                                        revents |= (POLLIN | POLLRDNORM);
                                }
                        }

                        /* NOTE: POLLHUP and POLLOUT/POLLWRNORM are mutually exclusive */
                        if (pipe_read_closed(uns->m_channel.write)) //
                        {
                                // The reader on our write endpoint is gone, A.K.A hangup
                                revents |= POLLHUP;
                        }
                        else if (a_events & (POLLOUT | POLLWRNORM))
                        {
                                if (pipe_write_available(uns->m_channel.write))
                                {
                                        revents |= (POLLOUT | POLLWRNORM);
                                }
                        }
                        break;
                }

                default:
                        panic("Should never happen");
                        break;
        }

        return revents;
}

// ********************************************************************************************************************************
static kern_return_t unix_stream_close(socket_t a_so)
// ********************************************************************************************************************************
{
        unix_stream_t uns = SOCKET_TO_UNIX_STREAM(a_so);

        switch (uns->m_state)
        {
                case UNIX_SOCKET_STATE_LISTENING:
                {
                        // Shutdown the pipe
                        pipe_read_shutdown(uns->m_channel.read);
                        pipe_write_shutdown(uns->m_channel.read);

                        // Drain all incoming connections shuting down endpoints and releasing references.

                        struct unix_stream_connection_req req = {};

                        for (integer_t rdcnt = pipe_read(uns->m_channel.read, &req, sizeof(req), true); //
                             rdcnt > 0;
                             rdcnt = pipe_read(uns->m_channel.read, &req, sizeof(req), true))
                        {
                                // Nobody is going to be reading from their write endpoint
                                pipe_read_shutdown(req.write);

                                // Nobody is going to be writing to their read endpoint
                                pipe_write_shutdown(req.read);

                                // Unreference the endpoints
                                pipe_rel(req.read);
                                pipe_rel(req.write);
                        }

                        // Release our pipe
                        pipe_rel(uns->m_channel.read);
                        uns->m_channel.read = nullptr;
                        [[fallthrough]];
                }
                case UNIX_SOCKET_STATE_BOUND:
                {
                        vfs_node_rel(uns->m_state_ctx.bound.bind_vfsn);
                        uns->m_state_ctx.bound.bind_vfsn = nullptr;
                        break;
                }
                case UNIX_SOCKET_STATE_ESTABLISHED:
                {
                        pipe_read_shutdown(uns->m_channel.read);
                        pipe_write_shutdown(uns->m_channel.write);

                        pipe_rel(uns->m_channel.read);
                        pipe_rel(uns->m_channel.write);

                        uns->m_channel.read = uns->m_channel.write = nullptr;

                        break;
                }

                case UNIX_SOCKET_STATE_CLOSED:
                {
                        break;
                }

                default:
                        panic("How?");
                        break;
        }

        uns->m_state = UNIX_SOCKET_STATE_CLOSED;

        return KERN_SUCCESS;
}

static struct socket_operations_data s_unix_stream_socket_ops = {
        .bind     = unix_stream_bind,            //
        .listen   = unix_stream_listen,          //
        .accept   = unix_stream_accept,          //
        .connect  = unix_stream_connect,         //
        .sendto   = unix_stream_sendto_recvfrom, //
        .recvfrom = unix_stream_sendto_recvfrom, //
        .poll     = unix_stream_poll,            //
        .close    = unix_stream_close            //
};

static struct net_proto_data s_unix_stream_proto[] = {
        {
         .m_domain             = nullptr,
         .m_type               = SOCK_STREAM,
         .m_protocol           = 0,
         .m_private_data_size  = sizeof(struct unix_stream_data),
         .m_private_data_align = alignof(struct unix_stream_data),
         .m_socket_ops         = &s_unix_stream_socket_ops,
         .m_proto_init         = unix_stream_socket_create /* inet_tcp_init */,
         .m_proto_destroy      = unix_stream_socket_destroy /* inet_tcp_init */,
         }  //
};

static net_domain_data s_unix_domain = {
        //
        .m_domain         = AF_UNIX,
        .m_sockaddr_size  = sizeof(sockaddr_un),
        .m_sockaddr_align = alignof(sockaddr_un),
        .m_link           = {},
        .m_proto_beg      = s_unix_stream_proto,
        .m_proto_end      = &s_unix_stream_proto[ARRAY_SIZE(s_unix_stream_proto)]
        //
};

// ********************************************************************************************************************************
static kern_return_t unix_init()
// ********************************************************************************************************************************
{
        domain_register(&s_unix_domain);

        return KERN_SUCCESS;
}

protocol_initcall(unix_init);
