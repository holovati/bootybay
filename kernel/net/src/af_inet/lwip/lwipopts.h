#pragma once

#define NO_SYS 1

#define LWIP_SOCKET  0
#define LWIP_NETCONN 0

#define SYS_LIGHTWEIGHT_PROT 0

#define LWIP_HAVE_LOOPIF 1

#define LWIP_SUPPORT_CUSTOM_PBUF 1

#define LWIP_DONT_PROVIDE_BYTEORDER_FUNCTIONS 1

#define LWIP_CALLBACK_API 1

// #define LWIP_WND_SCALE 1

#define LWIP_TCP_PCB_NUM_EXT_ARGS 1

// #define TCP_LISTEN_BACKLOG 1

#if 0
#define MEM_SIZE         0x100000 * 4
#define MEMP_NUM_PBUF    0x1000
#define MEMP_NUM_TCP_SEG 0x1000
#define PBUF_POOL_SIZE   0x1000
#define MEMP_NUM_TCP_PCB 32
#define TCP_MSS          1460
#define TCP_WND          (4000 * TCP_MSS)
#define TCP_SND_SCALE    14
#define TCP_RCV_SCALE    14
#define TCP_OVERSIZE     TCP_MSS
#define TCP_SND_QUEUELEN 0x1000
#define TCP_RCV_QUEUELEN 0x1000
#else /* IPv4 stuff */

/* If the system is 64 bit */
#if defined __LP64__
#define MEM_ALIGNMENT 8
#else
#define MEM_ALIGNMENT 4
#endif

#define MEM_SIZE ((1024 * 1024) * 1) /* 1 MiB */

/* SLAAC support and other IPv6 stuff */
#define LWIP_IPV6_DUP_DETECT_ATTEMPTS 1
#define LWIP_IPV6_SEND_ROUTER_SOLICIT 1
#define LWIP_IPV6_AUTOCONFIG          1
#define LWIP_IPV6_FORWARD             1
#define MEMP_NUM_MLD6_GROUP           16
#define LWIP_IPV6_NUM_ADDRESSES       6
#define IPV6_FRAG_COPYHEADER          1

/* TCP tuning */
#define LWIP_TCP_KEEPALIVE 1
#define LWIP_SO_RCVTIMEO   1

#define TCP_QUEUE_OOSEQ 1

#define MEMP_NUM_TCP_SEG 0x2000
#define PBUF_POOL_SIZE   (MEMP_NUM_TCP_SEG * 2)
#define MEMP_NUM_PBUF    0x1000

#define TCP_MSS          1460
#define TCP_WND          0x1cFFF
#define LWIP_WND_SCALE   1
#define TCP_RCV_SCALE    4
#define TCP_SND_BUF      TCP_WND
#define TCP_SND_QUEUELEN 512
#define TCP_OVERSIZE     TCP_MSS

/* Throughput settings */
#define LWIP_CHECKSUM_ON_COPY 0

/* Disable stats */
#define LWIP_STATS         0
#define LWIP_STATS_DISPLAY 0

#endif
