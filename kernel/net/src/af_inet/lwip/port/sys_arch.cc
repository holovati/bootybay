#include <string.h>

#include <lwip/sys.h>

#include <lwip/pbuf.h>
#include <lwip/init.h>
#include <lwip/netif.h>
#include <lwip/etharp.h>

#include <aux/init.h>

#include <kernel/time_out.h>

#include <emerixx/net/ethernet.h>
#include <emerixx/net/iface.h>
#include <emerixx/net/iface_buffer.h>
#include <emerixx/net/iface_buffer_data.h>
#include <emerixx/net/net_core.h>

// ********************************************************************************************************************************
u32_t sys_now(void)
// ********************************************************************************************************************************
{
        return (elapsed_ticks / hz) * 1000;
}

// ********************************************************************************************************************************
void ethernet_input(iface_buffer_t a_iface_buffer) // Called by core to push a ethernet packet up the stack
// ********************************************************************************************************************************
{
        struct pbuf *pb = iface_buffer_lwip_pbuf(a_iface_buffer);

        pb->len = pb->tot_len = a_iface_buffer->m_packet_size;

        struct netif *nif = iface_lwip_netif(a_iface_buffer->m_iface);

        if (nif->input(pb, nif) != ERR_OK)
        {
                pbuf_free(pb);
        }
}

// ********************************************************************************************************************************
err_t netif_output(struct netif *a_netif, struct pbuf *p) // Called by the lwip stack to send a packet
// ********************************************************************************************************************************
{
        KASSERT(p->next == nullptr);

        LINK_STATS_INC(link.xmit);

        iface_buffer_t ifbuf;

        kern_return_t retval = iface_buffer_alloc(lwip_netif_iface(a_netif), &ifbuf);

        if (KERN_STATUS_FAILURE(retval))
        {
                return ERR_BUF;
        }

        pbuf_copy_partial(p, (void *)ifbuf->m_virt_addr, p->tot_len, 0);

        ifbuf->m_packet_size = p->tot_len;

        net_core_send(ifbuf);

        // iface_buffer_send_pbuf(p);

/* Update SNMP stats (only if you use SNMP) */
// MIB2_STATS_NETIF_ADD(a_netif, ifoutoctets, p->tot_len);
#if 0
        int unicast = ((p->payload[0] & 0x01) == 0);
        if (unicast)
        {
                MIB2_STATS_NETIF_INC(a_netif, ifoutucastpkts);
        }
        else
        {
                MIB2_STATS_NETIF_INC(a_netif, ifoutnucastpkts);
        }

        // lock_interrupts();
        pbuf_copy_partial(p, mac_send_buffer, p->tot_len, 0);
        /* Start MAC transmit here */
        // unlock_interrupts();
#endif
        return ERR_OK;
}

// ********************************************************************************************************************************
static kern_return_t lwip_subsys_init()
// ********************************************************************************************************************************
{
        /// sys_init();
        lwip_init();
        return KERN_SUCCESS;
}

subsys_initcall(lwip_subsys_init);
