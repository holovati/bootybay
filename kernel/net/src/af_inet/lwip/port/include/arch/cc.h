#pragma once

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpadded"
#pragma GCC diagnostic ignored "-Wpedantic"
#pragma GCC diagnostic ignored "-Wpacked"
#pragma GCC diagnostic ignored "-Wswitch-enum"
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wduplicated-branches"

#define LWIP_TIMEVAL_PRIVATE 0

#define LWIP_PLATFORM_ASSERT(msg) panic(msg)

#ifdef ETL_QUEUE
#include ETL_QUEUE
#else
#include <etl/queue.h>
#endif

#define LWIP_PBUF_CUSTOM_DATA queue_chain_t m_sock_link; u16_t m_offset;
#define LWIP_PBUF_CUSTOM_DATA_INIT(p) do { ((p)->m_offset = 0); } while(0)

// #define LWIP_PLATFORM_ERROR(msg) log_error(msg)
