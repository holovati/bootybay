#include "lwip_bridge.h"

#include "inet.h"

struct lwip_tcp_req
{
        struct tcp_pcb *pcb;
        err_t err;
};

// ********************************************************************************************************************************
struct tcp_pcb *lwip_tcp_new()
// ********************************************************************************************************************************
{
        struct arg_data : lwip_tcp_req
        {
        } arg{
                {nullptr, ERR_OK},
        };

        auto cb = [](void *a_arg) -> void {
                struct arg_data *parg = (struct arg_data *)(a_arg);

                parg->pcb = tcp_new();
        };

        inet_lwip_thread_execute(cb, &arg, nullptr);

        return arg.pcb;
}

// ********************************************************************************************************************************
struct tcp_pcb *lwip_tcp_new_ip_type(u8_t a_type)
// ********************************************************************************************************************************
{
        struct arg_data : lwip_tcp_req
        {
                u8_t type;
        } arg{
                {nullptr, ERR_OK},
                a_type
        };

        auto cb = [](void *a_arg) -> void {
                struct arg_data *parg = (struct arg_data *)(a_arg);

                parg->pcb = tcp_new_ip_type(parg->type);
        };

        inet_lwip_thread_execute(cb, &arg, nullptr);

        return arg.pcb;
}

// ********************************************************************************************************************************
void lwip_tcp_arg(struct tcp_pcb *pcb, void *a_a)
// ********************************************************************************************************************************
{
        struct arg_data : lwip_tcp_req
        {
                void *a;
        } arg{
                {pcb, ERR_OK},
                a_a
        };

        auto cb = [](void *a_arg) -> void {
                struct arg_data *parg = (struct arg_data *)(a_arg);

                tcp_arg(parg->pcb, parg->a);
        };

        inet_lwip_thread_execute(cb, &arg, nullptr);
}

// ********************************************************************************************************************************
void lwip_tcp_recved(struct tcp_pcb *pcb, u16_t len)
// ********************************************************************************************************************************
{
        struct arg_data : lwip_tcp_req
        {
                u16_t len;
        } arg{
                {pcb, ERR_OK},
                len
        };

        auto cb = [](void *a_arg) -> void {
                struct arg_data *parg = (struct arg_data *)(a_arg);

                tcp_recved(parg->pcb, parg->len);
        };

        inet_lwip_thread_execute(cb, &arg, nullptr);
}

// ********************************************************************************************************************************
err_t lwip_tcp_bind(struct tcp_pcb *pcb, const ip_addr_t *ipaddr, u16_t port)
// ********************************************************************************************************************************
{
        struct arg_data : lwip_tcp_req
        {
                const ip_addr_t *ipaddr;
                u16_t port;
        } arg{
                {pcb, ERR_OK},
                ipaddr,
                port
        };

        auto cb = [](void *a_arg) -> void {
                struct arg_data *parg = (struct arg_data *)(a_arg);

                parg->err = tcp_bind(parg->pcb, parg->ipaddr, parg->port);
        };

        inet_lwip_thread_execute(cb, &arg, nullptr);

        return arg.err;
}

// ********************************************************************************************************************************
void lwip_tcp_bind_netif(struct tcp_pcb *pcb, const struct netif *netif)
// ********************************************************************************************************************************
{
        struct arg_data : lwip_tcp_req
        {
                const struct netif *netif;
        } arg{
                {pcb, ERR_OK},
                netif
        };

        auto cb = [](void *a_arg) -> void {
                struct arg_data *parg = (struct arg_data *)(a_arg);

                tcp_bind_netif(parg->pcb, parg->netif);
        };

        inet_lwip_thread_execute(cb, &arg, nullptr);
}

// ********************************************************************************************************************************
err_t lwip_tcp_connect(struct tcp_pcb *pcb, const ip_addr_t *ipaddr, u16_t port, tcp_connected_fn connected)
// ********************************************************************************************************************************
{
        struct arg_data : lwip_tcp_req
        {
                const ip_addr_t *ipaddr;
                u16_t port;
                tcp_connected_fn connected;
        } arg{
                {pcb, ERR_OK},
                ipaddr,
                port,
                connected
        };

        auto cb = [](void *a_arg) -> void {
                struct arg_data *parg = (struct arg_data *)(a_arg);

                parg->err = tcp_connect(parg->pcb, parg->ipaddr, parg->port, parg->connected);
        };

        inet_lwip_thread_execute(cb, &arg, nullptr);

        return arg.err;
}

// ********************************************************************************************************************************
struct tcp_pcb *lwip_tcp_listen_with_backlog_and_err(struct tcp_pcb *pcb, u8_t backlog, err_t *err, tcp_accept_fn af)
// ********************************************************************************************************************************
{
        struct arg_data : lwip_tcp_req
        {
                struct tcp_pcb *ret;
                u8_t backlog;
                err_t *err;
                tcp_accept_fn af;

        } arg{
                {pcb, ERR_OK},
                nullptr,
                backlog,
                err,
                af
        };

        auto cb = [](void *a_arg) -> void {
                struct arg_data *parg = (struct arg_data *)(a_arg);

                struct tcp_ext_arg_callbacks nullcallbacks = {};

                tcp_ext_arg_set_callbacks(parg->pcb, 0, &nullcallbacks);

                parg->ret = tcp_listen_with_backlog_and_err(parg->pcb, parg->backlog, parg->err);

                if (parg->ret != nullptr)
                {
                        tcp_accept(parg->ret, parg->af);
                }
        };

        inet_lwip_thread_execute(cb, &arg, nullptr);

        return arg.ret;
}

// ********************************************************************************************************************************
struct tcp_pcb *lwip_tcp_listen_with_backlog(struct tcp_pcb *pcb, u8_t backlog)
// ********************************************************************************************************************************
{
        struct arg_data : lwip_tcp_req
        {
                struct tcp_pcb *ret;
                u8_t backlog;

        } arg{
                {pcb, ERR_OK},
                nullptr,
                backlog
        };

        auto cb = [](void *a_arg) -> void {
                struct arg_data *parg = (struct arg_data *)(a_arg);

                parg->ret = tcp_listen_with_backlog(parg->pcb, parg->backlog);
        };

        inet_lwip_thread_execute(cb, &arg, nullptr);

        return arg.ret;
}

// ********************************************************************************************************************************
void lwip_tcp_abort(struct tcp_pcb *pcb)
// ********************************************************************************************************************************
{
        struct arg_data : lwip_tcp_req
        {
        } arg{
                {pcb, ERR_OK}
        };

        auto cb = [](void *a_arg) -> void {
                struct arg_data *parg = (struct arg_data *)(a_arg);

                tcp_abort(parg->pcb);
        };

        inet_lwip_thread_execute(cb, &arg, nullptr);
}

// ********************************************************************************************************************************
err_t lwip_tcp_close(struct tcp_pcb *pcb)
// ********************************************************************************************************************************
{
        struct arg_data : lwip_tcp_req
        {
        } arg{
                {pcb, ERR_OK}
        };

        auto cb = [](void *a_arg) -> void {
                struct arg_data *parg = (struct arg_data *)(a_arg);

                parg->err = tcp_close(parg->pcb);
        };

        inet_lwip_thread_execute(cb, &arg, nullptr);

        return arg.err;
}

// ********************************************************************************************************************************
err_t lwip_tcp_shutdown(struct tcp_pcb *pcb, int shut_rx, int shut_tx)
// ********************************************************************************************************************************
{
        struct arg_data : lwip_tcp_req
        {
                int shut_rx;
                int shut_tx;
        } arg{
                {pcb, ERR_OK},
                shut_rx,
                shut_tx
        };

        auto cb = [](void *a_arg) -> void {
                struct arg_data *parg = (struct arg_data *)(a_arg);

                parg->err = tcp_shutdown(parg->pcb, parg->shut_rx, parg->shut_tx);
        };

        inet_lwip_thread_execute(cb, &arg, nullptr);

        return arg.err;
}

// ********************************************************************************************************************************
err_t lwip_tcp_write(struct tcp_pcb *pcb, const void *dataptr, u16_t len, u8_t apiflags)
// ********************************************************************************************************************************
{
        struct arg_data : lwip_tcp_req
        {
                const void *dataptr;
                u16_t len;
                u8_t apiflags;
        } arg{
                {pcb, ERR_OK},
                dataptr,
                len,
                apiflags
        };

        auto cb = [](void *a_arg) -> void {
                struct arg_data *parg = (struct arg_data *)(a_arg);

                parg->err = tcp_write(parg->pcb, parg->dataptr, parg->len, parg->apiflags);
        };

        inet_lwip_thread_execute(cb, &arg, nullptr);

        return arg.err;
}

// ********************************************************************************************************************************
void lwip_tcp_setprio(struct tcp_pcb *pcb, u8_t prio)
// ********************************************************************************************************************************
{
        struct arg_data : lwip_tcp_req
        {
                u8_t prio;
        } arg{
                {pcb, ERR_OK},
                prio
        };

        auto cb = [](void *a_arg) -> void {
                struct arg_data *parg = (struct arg_data *)(a_arg);

                tcp_setprio(parg->pcb, parg->prio);
        };

        inet_lwip_thread_execute(cb, &arg, nullptr);
}

// ********************************************************************************************************************************
err_t lwip_tcp_output(struct tcp_pcb *pcb)
// ********************************************************************************************************************************
{
        struct arg_data : lwip_tcp_req
        {
        } arg{
                {pcb, ERR_OK}
        };

        auto cb = [](void *a_arg) -> void {
                struct arg_data *parg = (struct arg_data *)(a_arg);

                parg->err = tcp_output(parg->pcb);
        };

        inet_lwip_thread_execute(cb, &arg, nullptr);

        return arg.err;
}

// ********************************************************************************************************************************
err_t lwip_tcp_tcp_get_tcp_addrinfo(struct tcp_pcb *pcb, int local, ip_addr_t *addr, u16_t *port)
// ********************************************************************************************************************************
{
        struct arg_data : lwip_tcp_req
        {
                int local;
                ip_addr_t *addr;
                u16_t *port;
        } arg{
                {pcb, ERR_OK},
                local,
                addr,
                port
        };

        auto cb = [](void *a_arg) -> void {
                struct arg_data *parg = (struct arg_data *)(a_arg);

                parg->err = tcp_tcp_get_tcp_addrinfo(parg->pcb, parg->local, parg->addr, parg->port);
        };

        inet_lwip_thread_execute(cb, &arg, nullptr);

        return arg.err;
}

// ********************************************************************************************************************************
kern_return_t lwip_err_to_kern_return(err_t a_err)
// ********************************************************************************************************************************
{
        kern_return_t retval;

        switch (a_err)
        {
                case ERR_OK:
                        /** No error, everything OK. */
                        retval = KERN_FAIL(KERN_SUCCESS);
                        break;
                case ERR_MEM:
                        /** Out of memory error.     */
                        retval = KERN_FAIL(ENOMEM);
                        break;
                case ERR_BUF:
                        /** Buffer error.            */
                        retval = KERN_FAIL(ENOBUFS);
                        break;
                case ERR_TIMEOUT:
                        /** Timeout.                 */
                        retval = KERN_FAIL(ETIMEDOUT);
                        break;
                case ERR_RTE:
                        /** Routing problem.         */
                        retval = KERN_FAIL(EHOSTUNREACH);
                        break;
                case ERR_INPROGRESS:
                        /** Operation in progress    */
                        retval = KERN_FAIL(EINPROGRESS);
                        break;
                case ERR_VAL:
                        /** Illegal value.           */
                        retval = KERN_FAIL(EINVAL);
                        break;
                case ERR_WOULDBLOCK:
                        /** Operation would block.   */
                        retval = KERN_FAIL(EWOULDBLOCK);
                        break;
                case ERR_USE:
                        /** Address in use.          */
                        retval = KERN_FAIL(EADDRINUSE);
                        break;
                case ERR_ALREADY:
                        /** Already connecting.      */
                        retval = KERN_FAIL(EALREADY);
                        break;
                case ERR_ISCONN:
                        /** Conn already established.*/
                        retval = KERN_FAIL(EISCONN);
                        break;
                case ERR_CONN:
                        /** Not connected.           */
                        retval = KERN_FAIL(ENOTCONN);
                        break;
                case ERR_IF:
                        /** Low-level netif error    */
                        retval = KERN_FAIL(ENXIO);
                        break;
                case ERR_ABRT:
                        /** Connection aborted.      */
                        retval = KERN_FAIL(ECONNABORTED);
                        break;
                case ERR_RST:
                        /** Connection reset.        */
                        retval = KERN_FAIL(ECONNRESET);
                        break;
                case ERR_CLSD:
                        /** Connection closed.       */
                        retval = KERN_FAIL(ENETDOWN); // ???
                        break;
                case ERR_ARG:
                        /** Illegal argument.        */
                        [[falltrough]];
                default:
                        retval = KERN_FAIL(EINVAL);
                        break;
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t lwip_tcp_recved_free(struct tcp_pcb *pcb, struct pbuf *p)
// ********************************************************************************************************************************
{
        struct arg_data : lwip_tcp_req
        {
                struct pbuf *buffer;
        } arg{
                {pcb, ERR_OK},
                p
        };

        inet_lwip_thread_execute(
                [](void *a_arg) {
                        struct arg_data *parg = (struct arg_data *)a_arg;
                        u16_t tot_recv_len    = (parg->buffer->len - (parg->buffer->len - parg->buffer->m_offset));
                        tcp_recved(parg->pcb, tot_recv_len);
                        pbuf_free(parg->buffer);
                },
                &arg,
                nullptr);

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t lwip_pbuf_free(struct pbuf *p)
// ********************************************************************************************************************************
{
        struct arg_data : lwip_tcp_req
        {
                struct pbuf *buffer;
        } arg{
                {nullptr, ERR_OK},
                p
        };

        inet_lwip_thread_execute(
                [](void *a_arg) {
                        struct arg_data *parg = (struct arg_data *)a_arg;
                        pbuf_free(parg->buffer);
                },
                &arg,
                nullptr);

        return KERN_SUCCESS;
}
