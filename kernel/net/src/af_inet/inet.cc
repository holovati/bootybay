#include <netinet/in.h>

#include <kernel/lock.h>
#include <kernel/sched_prim.h>
#include <kernel/thread.h>
#include <kernel/thread_data.h>

#include <emerixx/net/thread_hook_data.h>
#include <emerixx/net/net_core.h>

#include "socket.h"

#include "inet.h"

struct inet_lwip_execute_msg
{
        queue_chain_t m_msgq_link;
        void (*m_callback)(void *);
        void *m_user_data;
        thread_t m_wakeup_thread;
        void *m_condvar;
};

static struct lock_data s_msg_lock;

static queue_head_t s_msgq;

// ********************************************************************************************************************************
static void net_thread_entry(void *)
// ********************************************************************************************************************************
{
again:
        if (lock_try_write(&s_msg_lock) == false)
        {
                // Could not get lock, bail!
                return;
        }

        if (queue_empty(&s_msgq) == true)
        {
                // Nothing to do, bail!
                lock_write_done(&s_msg_lock);
                return;
        }

        queue_entry_t qentry = dequeue_head(&s_msgq);

        lock_write_done(&s_msg_lock);

        struct inet_lwip_execute_msg *msg = queue_containing_record(qentry, struct inet_lwip_execute_msg, m_msgq_link);

        msg->m_callback(msg->m_user_data);

        if (msg->m_condvar == nullptr)
        {
                // No condvar means we have to wakeup the calling thread
                clear_wait(msg->m_wakeup_thread, THREAD_AWAKENED, false);
        }

        // Check for more work
        goto again;
}

// ********************************************************************************************************************************
void inet_lwip_thread_execute(void (*a_callback)(void *), void *a_user_data, void *a_condvar)
// ********************************************************************************************************************************
{
        struct inet_lwip_execute_msg msg = { .m_msgq_link     = {},               //
                                             .m_callback      = a_callback,       //
                                             .m_user_data     = a_user_data,      //
                                             .m_wakeup_thread = current_thread(), //
                                             .m_condvar       = a_condvar };

        lock_write(&s_msg_lock);

        enqueue_tail(&s_msgq, &msg.m_msgq_link);

        assert_wait(a_condvar, false);

        lock_write_done(&s_msg_lock);

        net_core_thread_nudge();

        thread_block(nullptr);
}

struct net_proto_data g_inet_proto[INET_PROTO_LAST];

static net_domain_data s_inet_domain = {
        //
        .m_domain         = AF_INET,
        .m_sockaddr_size  = sizeof(sockaddr_in),
        .m_sockaddr_align = alignof(sockaddr_in),
        .m_link           = {},
        .m_proto_beg      = g_inet_proto,
        .m_proto_end      = &g_inet_proto[INET_PROTO_LAST]
        //
};

struct net_proto_data g_inet6_proto[INET_PROTO_LAST];

static net_domain_data s_inet6_domain = {
        //
        .m_domain         = AF_INET6,
        .m_sockaddr_size  = sizeof(sockaddr_in6),
        .m_sockaddr_align = alignof(sockaddr_in6),
        .m_link           = {},
        .m_proto_beg      = g_inet6_proto,
        .m_proto_end      = &g_inet6_proto[INET_PROTO_LAST]
        //
};

struct net_thread_hook_data s_net_core_thread_hook = {

        .m_all_hooks = {},              //
        .m_user_data = nullptr,         //
        .m_callback  = net_thread_entry //
};

// ********************************************************************************************************************************
static kern_return_t inet_protocol_init()
// ********************************************************************************************************************************
{
        lock_init(&s_msg_lock, true);

        queue_init(&s_msgq);

        domain_register(&s_inet_domain);

        domain_register(&s_inet6_domain);

        net_core_thread_hook_install(&s_net_core_thread_hook);

        return KERN_SUCCESS;
}

protocol_initcall(inet_protocol_init);
