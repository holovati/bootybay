#pragma once

#if defined(LWIP_HDR_TCP_PRIV_H) && !defined(LWIP_HDR_SOCKETS_PRIV_H) && !defined(LWIP_HDR_INET_CHKSUM_H)
#if 0
/* This section gets injected into tcp.c */
// #pragma GCC diagnostic push
// #pragma GCC diagnostic ignored "-Wmissing-declarations"

/* As initial send MSS, we use TCP_MSS but limit it to 536. */
#if TCP_MSS > 536
#define INITIAL_MSS 536
#else
#define INITIAL_MSS TCP_MSS
#endif

static void tcp_free_listen(struct tcp_pcb *pcb);

// static void tcp_remove_listener(struct tcp_pcb *list, struct tcp_pcb_listen *lpcb);

static void tcp_listen_closed(struct tcp_pcb *pcb);

static err_t tcp_close_shutdown(struct tcp_pcb *pcb, u8_t rst_on_unacked_data);

// static err_t tcp_accept_null(void *arg, struct tcp_pcb *pcb, err_t err);

static void tcp_kill_prio(u8_t prio);

static void tcp_kill_state(enum tcp_state state);

static void tcp_kill_timewait(void);

static void tcp_handle_closepend(void);

static void tcp_netif_ip_addr_changed_pcblist(const ip_addr_t *old_addr, struct tcp_pcb *pcb_list);

extern u8_t tcp_timer_ctr;

void tcb_pcb_init(struct tcp_pcb *pcb);

void tcb_pcb_init(struct tcp_pcb *pcb)
{
        /* zero out the whole pcb, so there is no need to initialize members to zero */
        memset(pcb, 0, sizeof(*pcb));
        pcb->prio    = TCP_PRIO_NORMAL;
        pcb->snd_buf = TCP_SND_BUF;
        /* Start with a window that does not need scaling. When window scaling is
           enabled and used, the window is enlarged when both sides agree on scaling. */
        pcb->rcv_wnd = pcb->rcv_ann_wnd = TCPWND_MIN16(TCP_WND);
        pcb->ttl                        = TCP_TTL;
        /* As initial send MSS, we use TCP_MSS but limit it to 536.
           The send MSS is updated when an MSS option is received. */
        pcb->mss = INITIAL_MSS;
        /* Set initial TCP's retransmission timeout to 3000 ms by default.
           This value could be configured in lwipopts */
        pcb->rto        = LWIP_TCP_RTO_TIME / TCP_SLOW_INTERVAL;
        pcb->sv         = LWIP_TCP_RTO_TIME / TCP_SLOW_INTERVAL;
        pcb->rtime      = -1;
        pcb->cwnd       = 1;
        pcb->tmr        = tcp_ticks;
        pcb->last_timer = tcp_timer_ctr;

        /* RFC 5681 recommends setting ssthresh arbitrarily high and gives an example
        of using the largest advertised receive window.  We've seen complications with
        receiving TCPs that use window scaling and/or window auto-tuning where the
        initial advertised window is very small and then grows rapidly once the
        connection is established. To avoid these complications, we set ssthresh to the
        largest effective cwnd (amount of in-flight data) that the sender can have. */
        pcb->ssthresh = TCP_SND_BUF;

#if LWIP_CALLBACK_API
        pcb->recv = tcp_recv_null;
#endif /* LWIP_CALLBACK_API */

        /* Init KEEPALIVE timer */
        pcb->keep_idle = TCP_KEEPIDLE_DEFAULT;

#if LWIP_TCP_KEEPALIVE
        pcb->keep_intvl = TCP_KEEPINTVL_DEFAULT;
        pcb->keep_cnt   = TCP_KEEPCNT_DEFAULT;
#endif /* LWIP_TCP_KEEPALIVE */
        pcb_tci_init(pcb);
}

/* Some hacks in order to bring our own pcb's and bypass the mempool */

static void memp_free_hook(int pool, void *p)
{
        if (pool != MEMP_TCP_PCB)
        {
                memp_free(pool, p);
        }
}

#define static /* We need some symbols to initialize our own pcb's*/

#define memp_free(...) memp_free_hook(__VA_ARGS__)
#endif
#endif
