#pragma once

#include "domain.h"

void inet_lwip_thread_execute(void (*a_callback)(void *), void *a_user_data, void *a_condvar);

enum inet_proto : natural_t
{
        INET_PROTO_TCP_IP_DEFAULT = 0,
        INET_PROTO_TCP_IP_STREAM,
        INET_PROTO_LAST
};

extern struct net_proto_data g_inet_proto[INET_PROTO_LAST];

extern struct net_proto_data g_inet6_proto[INET_PROTO_LAST];
