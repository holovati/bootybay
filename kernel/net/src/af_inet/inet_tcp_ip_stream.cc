#include <fcntl.h>

#include "lwip_bridge.h"

#include <netinet/in.h>

#include <aux/init.h>

#include <etl/algorithm.hh>
#include <etl/bit.hh>

#include <kernel/lock.h>

#include <emerixx/net/thread_hook_data.h>
#include <emerixx/net/iface_buffer.h>
#include <emerixx/net/iface_buffer_data.h>
#include <emerixx/poll.h>

#include <emerixx/sleep.h>

#include "socket.h"

#include "inet.h"

#define SOCK_TCP_CTX(so) ((tcp_ctx_t)((so)->m_proto_private))

#define SATOSIN(sa) ((struct sockaddr_in *)(sa))

#define TCP_SHARED_DATA_FLAG_IS_LISTEN_SOCK etl::bit<decltype(lwip_tcp_shared_data::m_flags), 0>::value
#define TCP_SHARED_DATA_FLAG_CONN_WAS_RESET etl::bit<decltype(lwip_tcp_shared_data::m_flags), 1>::value

/*Forward declarations*/
typedef struct tcp_ctx_data *tcp_ctx_t;
typedef struct lwip_tcp_shared_data *lwip_tcp_shared_t;

typedef etl::queue<struct lwip_tcp_shared_data> lwip_tcp_shared_data_list;
typedef etl::queue<struct lwip_tpcb_envelope> lwip_tpcb_envelope_list;

static err_t lwip_tcp_sent_callback(void *arg, struct tcp_pcb *tpcb, u16_t len);
static err_t lwip_tcp_recv_callback(void *arg, struct tcp_pcb *tpcb, struct pbuf *p, err_t err);
static err_t lwip_tcp_connected_callback(void *arg, struct tcp_pcb *tpcb, err_t err);
static err_t lwip_tcp_poll_callback(void *arg, struct tcp_pcb *tpcb);
static void lwip_tcp_err_callback(void *arg, err_t err);
static err_t tcp_passive_open_callback(u8_t id, struct tcp_pcb_listen *lpcb, struct tcp_pcb *cpcb);
static void lwip_tcp_pcb_destroyed_callback(u8_t id, void *data);

struct lwip_tcp_shared_data : lwip_tcp_shared_data_list::chain
{
        enum tcp_state *m_state_ptr;     // A ptr where lwip tcp can write its state, users only ever read it
        struct lock_data m_bufferq_lock; // To protect the buffer queue
        queue_head_t m_bufferq;          // pbufs that were received from lwip if not listen so
        integer_t m_refcount;            // Ref counter for this struct, will never get above 2 but for alignment we use integer_t
        integer_t m_flags;
};

// Used to "send" incoming connections from the listener, so they can be accepted.
struct lwip_tpcb_envelope : lwip_tpcb_envelope_list::chain
{
        struct tcp_pcb *tpcb; //  The new pcb
};

struct tcp_ctx_data
{
        struct tcp_pcb *tpcb; // tcp pcb, never used directly, only trough cross thread api
        struct lwip_tcp_shared_data *m_lwip_shared;
        enum tcp_state volatile const m_tcp_state; // Updated by the network stack.
};

static struct lwip_tpcb_envelope s_lwip_tpcb_envelope_storage[MEMP_NUM_TCP_PCB * 2];
static struct lwip_tcp_shared_data s_lwip_shared_storage[MEMP_NUM_TCP_PCB * 2]; // One per pcb
static lwip_tcp_shared_data_list s_lwip_shared_freelist;
static lwip_tpcb_envelope_list s_lwip_tpcb_envelope_freelist;

/** A table of callback functions that is invoked for ext arguments */
static struct tcp_ext_arg_callbacks s_tcp_extra_arg_cb = {
        /** @ref tcp_extarg_callback_pcb_destroyed_fn */
        .destroy = lwip_tcp_pcb_destroyed_callback,
        /** @ref tcp_extarg_callback_passive_open_fn */
        .passive_open = tcp_passive_open_callback
};

// ********************************************************************************************************************************
static void lwip_tcp_shared_update_state(struct tcp_pcb *tpcb) // Network thread only
// ********************************************************************************************************************************
{
        lwip_tcp_shared_t lts = ((lwip_tcp_shared_t)tcp_ext_arg_get(tpcb, 0));

        if ((lts->m_refcount > 1) && (lts->m_state_ptr != nullptr))
        {
                *(lts->m_state_ptr) = tpcb->state;
        }
}

// ********************************************************************************************************************************
static lwip_tcp_shared_t lwip_tcp_shared_alloc(enum tcp_state const volatile *a_state_ptr) // Network thread only
// ********************************************************************************************************************************
{
        lwip_tcp_shared_t lts = nullptr;
        if (s_lwip_shared_freelist.empty() == false)
        {
                lts = s_lwip_shared_freelist.dequeue();
                lock_init(&lts->m_bufferq_lock, true);
                lts->m_state_ptr = (enum tcp_state *)const_cast<enum tcp_state *>(a_state_ptr);
                queue_init(&lts->m_bufferq);
                lts->m_refcount = 1;
        }
        return lts;
}

// ********************************************************************************************************************************
static void lwip_tcp_shared_free(lwip_tcp_shared_t a_lts) // Network thread only
// ********************************************************************************************************************************
{
        if ((--(a_lts->m_refcount)) == 0)
        {
                while (queue_empty(&a_lts->m_bufferq) == false)
                {
                        queue_entry_t qe = dequeue_head(&a_lts->m_bufferq);

                        if (a_lts->m_flags & TCP_SHARED_DATA_FLAG_IS_LISTEN_SOCK)
                        {
                                struct lwip_tpcb_envelope *env = (struct lwip_tpcb_envelope *)qe;
                                KASSERT(env->tpcb != nullptr);
                                tcp_abort(env->tpcb);
                                s_lwip_tpcb_envelope_freelist.enqueue(env);
                        }
                        else
                        {
                                struct pbuf *p = queue_containing_record(qe, struct pbuf, m_sock_link);
                                pbuf_free(p);
                        }
                }

                *a_lts = {};

                s_lwip_shared_freelist.enqueue(a_lts);
        }
}

// ********************************************************************************************************************************
static void lwip_tcp_pcb_set_callbacks(struct tcp_pcb *a_tpcb)
// ********************************************************************************************************************************
{
        /* Function to be called when more send buffer space is available. */
        a_tpcb->sent = lwip_tcp_sent_callback;
        /* Function to be called when (in-sequence) data has arrived. */
        a_tpcb->recv = lwip_tcp_recv_callback;
        /* Function to be called when a connection has been set up. */
        a_tpcb->connected = lwip_tcp_connected_callback;
        /* Function which is called periodically. */
        a_tpcb->poll = lwip_tcp_poll_callback;
        /* Function to be called whenever a fatal error occurs. */
        a_tpcb->errf = lwip_tcp_err_callback;
}

/** Function prototype for deallocation of arguments. Called *just before* the
 * pcb is freed, so don't expect to be able to do anything with this pcb!
 *
 * @param id ext arg id (allocated via @ref tcp_ext_arg_alloc_id)
 * @param data pointer to the data (set via @ref tcp_ext_arg_set before)
 */
// ********************************************************************************************************************************
void lwip_tcp_pcb_destroyed_callback(u8_t id, void *data) // Runs in network thread
// ********************************************************************************************************************************
{
        lwip_tcp_shared_t lts = (lwip_tcp_shared_t)(data);
        lwip_tcp_shared_free(lts);
}

/** Function prototype to transition arguments from a listening pcb to an accepted pcb
 *
 * @param id ext arg id (allocated via @ref tcp_ext_arg_alloc_id)
 * @param lpcb the listening pcb accepting a connection
 * @param cpcb the newly allocated connection pcb
 * @return ERR_OK if OK, any error if connection should be dropped
 */
// ********************************************************************************************************************************
err_t tcp_passive_open_callback(u8_t id, struct tcp_pcb_listen *lpcb, struct tcp_pcb *cpcb) // Runs in network thread
// ********************************************************************************************************************************
{
        err_t err = ERR_OK;

        lwip_tcp_pcb_set_callbacks(cpcb);

        lwip_tcp_shared_t listen_lts = (lwip_tcp_shared_t)lpcb->callback_arg;

        if (s_lwip_tpcb_envelope_freelist.empty() == false)
        {
                lwip_tcp_shared_t new_lts = lwip_tcp_shared_alloc(nullptr);

                if (new_lts != nullptr)
                {
                        tcp_arg(cpcb, new_lts);

                        tcp_ext_arg_set(cpcb, id, new_lts);

                        tcp_ext_arg_set_callbacks(cpcb, id, &s_tcp_extra_arg_cb);

                        struct lwip_tpcb_envelope *envelope = s_lwip_tpcb_envelope_freelist.dequeue();

                        envelope->tpcb = cpcb;

                        enqueue_tail(&listen_lts->m_bufferq, envelope);

                        uthread_wakeup(&listen_lts->m_bufferq);

                        new_lts->m_refcount++;
                }
                else
                {
                        err = ERR_MEM;
                }
        }
        else
        {
                err = ERR_MEM;
        }

        return err;
}

// ********************************************************************************************************************************
static kern_return_t tcp_ip_stream_socket_create(socket_t a_so) // Runs in user thread
// ********************************************************************************************************************************
{
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static void tcp_ip_stream_socket_destroy(socket_t a_so) // Runs in user thread
// ********************************************************************************************************************************
{
        tcp_ctx_t tctx = SOCK_TCP_CTX(a_so);

        inet_lwip_thread_execute(
                [](void *arg) {
                        if (arg)
                        {
                                lwip_tcp_shared_free((lwip_tcp_shared_t)(arg));
                        }
                },
                tctx->m_lwip_shared,
                nullptr);
}

// ********************************************************************************************************************************
static kern_return_t tcp_ip_stream_bind(socket_t a_so, struct sockaddr *a_addr, socklen_t a_addrlen) // Runs in user thread
// ********************************************************************************************************************************
{
        struct sockaddr_in *sin = SATOSIN(a_addr);

        struct args
        {
                ip_addr_t ipa;
                u16_t port;
                tcp_ctx_t tctx;
                kern_return_t retval;
        } args = { .ipa    = IPADDR4_INIT(sin->sin_addr.s_addr),
                   .port   = PP_NTOHS(sin->sin_port),
                   .tctx   = SOCK_TCP_CTX(a_so),
                   .retval = KERN_SUCCESS };

        inet_lwip_thread_execute(
                [](void *arg) {
                        struct args *parg = (struct args *)(arg);

                        if (parg->tctx->tpcb != nullptr)
                        {
                                parg->retval = KERN_FAIL(EINVAL);
                                return;
                        }

                        lwip_tcp_shared_t lts = lwip_tcp_shared_alloc(&parg->tctx->m_tcp_state);

                        if (lts == nullptr)
                        {
                                parg->retval = KERN_FAIL(ENOMEM);
                                return;
                        }

                        struct tcp_pcb *tpcb = tcp_new();

                        if (tpcb == nullptr)
                        {
                                lwip_tcp_shared_free(lts);
                                parg->retval = KERN_FAIL(ENOMEM);
                                return;
                        }
                        lts->m_refcount++;

                        lwip_tcp_pcb_set_callbacks(tpcb);

                        tcp_ext_arg_set_callbacks(tpcb, 0, &s_tcp_extra_arg_cb);

                        tcp_ext_arg_set(tpcb, 0, lts);

                        tcp_arg(tpcb, lts); // Make tcp_pcb knows about this

                        parg->tctx->tpcb = tpcb;

                        parg->tctx->m_lwip_shared = lts;

                        err_t err = tcp_bind(tpcb, &parg->ipa, parg->port);

                        parg->retval = lwip_err_to_kern_return(err);

                        lwip_tcp_shared_update_state(tpcb);
                },
                &args,
                nullptr);

        return args.retval;
}

/** Called when the pcb receives a RST or is unexpectedly closed for any other reason.
 *
 * @note The corresponding pcb is already freed when this callback is called!
 *
 * @param arg Additional argument to pass to the callback function (@see tcp_arg())
 * @param err Error code to indicate why the pcb has been closed
 *            ERR_ABRT: aborted through tcp_abort or by a TCP timer
 *            ERR_RST: the connection was reset by the remote host
 */
// ********************************************************************************************************************************
void lwip_tcp_err_callback(void *arg, err_t err) // Runs in network thread
// ********************************************************************************************************************************
{
        lwip_tcp_shared_t lts = (lwip_tcp_shared_t)(arg);
        *lts->m_state_ptr     = CLOSED;
        lts->m_flags |= TCP_SHARED_DATA_FLAG_CONN_WAS_RESET;
        lwip_tcp_shared_free(lts);
}

// ********************************************************************************************************************************
/** Called when a new connection can be accepted on a listening pcb.
 *
 * @param arg Additional argument to pass to the callback function (@see tcp_arg())
 * @param newpcb The new connection pcb
 * @param err An error code if there has been an error accepting.
 *            Only return ERR_ABRT if you have called tcp_abort from within the
 *            callback function!
 */
// ********************************************************************************************************************************
static err_t lwip_tcp_accept_callback(void *arg, struct tcp_pcb *newpcb, err_t err) // Runs in network thread
// ********************************************************************************************************************************
{
        lwip_tcp_shared_update_state(newpcb);

        if (err == ERR_OK)
        {
        }

        return err;
}

// ********************************************************************************************************************************
static kern_return_t tcp_ip_stream_accept(socket_t a_so, integer_t a_so_oflags, socket_t a_newso) // Runs in user thread
// ********************************************************************************************************************************
{
        struct args
        {
                kern_return_t retval;
                tcp_ctx_t tctx;
                tcp_ctx_t newtctx;
                struct sockaddr_in *remote_sin;
                struct sockaddr_in *local_sin;
        } arg = { .retval     = KERN_SUCCESS,
                  .tctx       = SOCK_TCP_CTX(a_so),
                  .newtctx    = SOCK_TCP_CTX(a_newso),
                  .remote_sin = SATOSIN(a_so->m_peer_sa),
                  .local_sin  = SATOSIN(a_so->m_sa) };

again:
        inet_lwip_thread_execute(
                [](void *a_arg) {
                        struct args *parg = (struct args *)(a_arg);

                        if (parg->tctx->m_lwip_shared->m_refcount < 2)
                        {
                                // The pcb is gone for some reason
                                parg->retval = KERN_FAIL(EINVAL);
                                return;
                        }

                        if (parg->tctx->tpcb->state != LISTEN)
                        {
                                parg->retval = KERN_FAIL(EINVAL);
                                return;
                        }

                        if (queue_empty(&parg->tctx->m_lwip_shared->m_bufferq))
                        {
                                parg->retval = KERN_FAIL(EWOULDBLOCK);
                                return;
                        }

                        struct lwip_tpcb_envelope *envelope
                                = (struct lwip_tpcb_envelope *)dequeue_head(&parg->tctx->m_lwip_shared->m_bufferq);

                        parg->newtctx->tpcb = envelope->tpcb; // We gained a ref in the accept callback

                        parg->newtctx->m_lwip_shared = (lwip_tcp_shared_t)(parg->newtctx->tpcb->callback_arg);

                        parg->newtctx->m_lwip_shared->m_state_ptr = const_cast<tcp_state *>(&parg->newtctx->m_tcp_state);

                        lwip_tcp_shared_update_state(parg->newtctx->tpcb);

                        s_lwip_tpcb_envelope_freelist.enqueue(envelope);

                        ip_addr_t ip;
                        u16_t port;

                        tcp_tcp_get_tcp_addrinfo(parg->newtctx->tpcb, 0, &ip, &port);
                        parg->remote_sin->sin_family      = AF_INET;
                        parg->remote_sin->sin_addr.s_addr = ip.addr;
                        parg->remote_sin->sin_port        = (port);

                        tcp_tcp_get_tcp_addrinfo(parg->tctx->tpcb, 1, &ip, &port);
                        parg->local_sin->sin_family      = AF_INET;
                        parg->local_sin->sin_addr.s_addr = ip.addr;
                        parg->local_sin->sin_port        = (port);
                },
                &arg,
                nullptr);

        if (KERN_STATUS_FAILURE(arg.retval))
        {
                if ((arg.retval == KERN_FAIL(EWOULDBLOCK)) && !(a_so_oflags & O_NONBLOCK))
                {
                        arg.retval = uthread_sleep_timeout(&arg.tctx->m_lwip_shared->m_bufferq, 0, nullptr);

                        if (KERN_STATUS_SUCCESS(arg.retval))
                        {
                                goto again;
                        }
                }
        }

        return arg.retval;
}

// ********************************************************************************************************************************
static kern_return_t tcp_ip_stream_listen(socket_t a_so, integer_t a_oflags, integer_t a_maxconn) // Runs in user thread
// ********************************************************************************************************************************
{
        struct args
        {
                kern_return_t retval;
                tcp_ctx_t tctx;
                integer_t maxxconn;
        } arg = { .retval = KERN_SUCCESS, .tctx = SOCK_TCP_CTX(a_so), .maxxconn = a_maxconn };

        inet_lwip_thread_execute(
                [](void *a_arg) {
                        struct args *parg = (struct args *)(a_arg);

                        if (parg->tctx->m_lwip_shared->m_refcount < 2)
                        {
                                // The pcb is gone for some reason
                                parg->retval = KERN_FAIL(EINVAL);
                                return;
                        }

                        err_t err = ERR_OK;

                        struct tcp_ext_arg_callbacks nullcallbacks = {};
                        tcp_ext_arg_set_callbacks(parg->tctx->tpcb, 0, &nullcallbacks);

                        struct tcp_pcb *listentpcb = tcp_listen_with_backlog_and_err(parg->tctx->tpcb, parg->maxxconn, &err);

                        if (err == ERR_OK)
                        {
                                parg->tctx->m_lwip_shared->m_flags |= TCP_SHARED_DATA_FLAG_IS_LISTEN_SOCK;

                                tcp_accept(listentpcb, lwip_tcp_accept_callback);

                                tcp_ext_arg_set_callbacks(listentpcb, 0, &s_tcp_extra_arg_cb);

                                parg->tctx->tpcb = listentpcb;

                                lwip_tcp_shared_update_state(parg->tctx->tpcb);
                        }

                        parg->retval = lwip_err_to_kern_return(err);
                },
                &arg,
                nullptr);

        return arg.retval;
}

// ********************************************************************************************************************************
/** Called when a pcb is connected to the remote side after initiating a connection attempt by calling tcp_connect().
 *
 * @param arg Additional argument to pass to the callback function (@see tcp_arg())
 * @param tpcb The connection pcb which is connected
 * @param err An unused error code, always ERR_OK currently ;-) @todo!
 *            Only return ERR_ABRT if you have called tcp_abort from within the
 *            callback function!
 *
 * @note When a connection attempt fails, the error callback is currently called!
 */
// ********************************************************************************************************************************
err_t lwip_tcp_connected_callback(void *arg, struct tcp_pcb *tpcb, err_t err) // Runs in network thread
// ********************************************************************************************************************************
{
        lwip_tcp_shared_update_state(tpcb);
        return ERR_OK;
}

// ********************************************************************************************************************************
static kern_return_t tcp_ip_stream_connect(socket_t a_so, integer_t a_oflags, struct sockaddr *a_addr, socklen_t a_addrlen)
// ********************************************************************************************************************************
{
        tcp_ctx_t tctx = SOCK_TCP_CTX(a_so);

        struct sockaddr_in *sin = SATOSIN(a_addr);

        struct args
        {
                struct sockaddr_in *sin;
                ip_addr_t ipa;
                u16_t port;
                tcp_ctx_t tctx;
                kern_return_t retval;
        } arg = { .sin = sin, .ipa = IPADDR4_INIT(sin->sin_addr.s_addr), .tctx = tctx, .retval = KERN_SUCCESS };

        inet_lwip_thread_execute(
                [](void *a_arg) {
                        struct args *parg = (struct args *)(a_arg);

                        lwip_tcp_shared_t lts = lwip_tcp_shared_alloc(&parg->tctx->m_tcp_state);

                        if (lts == nullptr)
                        {
                                parg->retval = KERN_FAIL(ENOMEM);
                                return;
                        }

                        struct tcp_pcb *tpcb = tcp_new();

                        if (tpcb == nullptr)
                        {
                                lwip_tcp_shared_free(lts);
                                parg->retval = KERN_FAIL(ENOMEM);
                                return;
                        }

                        lts->m_refcount++;

                        lwip_tcp_pcb_set_callbacks(tpcb);

                        tcp_ext_arg_set_callbacks(tpcb, 0, &s_tcp_extra_arg_cb);

                        tcp_ext_arg_set(tpcb, 0, lts);

                        tcp_arg(tpcb, lts); // Make tcp_pcb knows about this

                        parg->tctx->tpcb = tpcb;

                        parg->tctx->m_lwip_shared = lts;

                        err_t err = tcp_connect(tpcb, &parg->ipa, parg->port, lwip_tcp_connected_callback);

                        parg->retval = lwip_err_to_kern_return(err);

                        lwip_tcp_shared_update_state(tpcb);
                },
                &arg,
                nullptr);

        return arg.retval;
}

/** Called when sent data has been acknowledged by the remote side.
 * Use it to free corresponding resources.
 * This also means that the pcb has now space available to send new data.
 *
 * @param arg Additional argument to pass to the callback function (@see tcp_arg())
 * @param tpcb The connection pcb for which data has been acknowledged
 * @param len The amount of bytes acknowledged
 * @return ERR_OK: try to send some data by calling tcp_output
 *            Only return ERR_ABRT if you have called tcp_abort from within the
 *            callback function!
 */
// ********************************************************************************************************************************
err_t lwip_tcp_sent_callback(void *arg, struct tcp_pcb *tpcb, u16_t len) // Runs in network thread
// ********************************************************************************************************************************
{
        lwip_tcp_shared_update_state(tpcb);
        tcp_output(tpcb);
        return ERR_OK;
}

// ********************************************************************************************************************************
static kern_return_t tcp_ip_stream_sendto(socket_t a_so,
                                          integer_t a_oflags,
                                          memory_rdwr_t a_mrw,
                                          int a_flags,
                                          struct sockaddr *a_addr,
                                          socklen_t *a6_addrlen) // Runs in user thread
// ********************************************************************************************************************************
{
        tcp_ctx_t tctx = SOCK_TCP_CTX(a_so);

        if (tctx->m_lwip_shared->m_flags & TCP_SHARED_DATA_FLAG_CONN_WAS_RESET)
        {
                return KERN_FAIL(ECONNRESET);
        }

        if (tctx->m_tcp_state != ESTABLISHED)
        {
                return KERN_FAIL(ENOTCONN);
        }

        natural_t sndmax = etl::min(tcp_sndbuf(tctx->tpcb), (u16_t)TCP_MSS);

        char buffer[TCP_MSS];

        natural_t xfer = a_mrw->uio_resid;

        kern_return_t retval = uiomove(buffer, sndmax, a_mrw);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        xfer = xfer - a_mrw->uio_resid;

        err_t err = lwip_tcp_write(tctx->tpcb, buffer, (u16_t)(xfer), TCP_WRITE_FLAG_COPY);

        retval = lwip_err_to_kern_return(err);

        lwip_tcp_output(tctx->tpcb);

        KASSERT(KERN_STATUS_SUCCESS(retval));

        return retval;
}

// ********************************************************************************************************************************
/** Called when data has been received.
 *
 * @param arg Additional argument to pass to the callback function (@see tcp_arg())
 * @param tpcb The connection pcb which received data
 * @param p The received data (or NULL when the connection has been closed!)
 * @param err An error code if there has been an error receiving
 *            Only return ERR_ABRT if you have called tcp_abort from within the
 *            callback function!
 */
// ********************************************************************************************************************************
err_t lwip_tcp_recv_callback(void *arg, struct tcp_pcb *tpcb, struct pbuf *p, err_t err) // Runs in network thread
// ********************************************************************************************************************************
{
        lwip_tcp_shared_t lts = (lwip_tcp_shared_t)(arg);

        lwip_tcp_shared_update_state(tpcb);

        if ((err == ERR_OK))
        {
                if (lock_try_write(&lts->m_bufferq_lock) == true)
                {
                        while (p != nullptr)
                        {
                                pbuf *next = p->next;
                                p->next    = nullptr;
                                p->tot_len = p->len;
                                enqueue_tail(&lts->m_bufferq, &p->m_sock_link);
                                p = next;
                        }
                        lock_done(&lts->m_bufferq_lock);

                        uthread_wakeup(&lts->m_bufferq);

                        socket_poll_thread_nudge();
                }
                else
                {
                        err = ERR_MEM;
                }
        }
        return err;
}

// ********************************************************************************************************************************
static kern_return_t tcp_ip_stream_recvfrom(socket_t a_so,
                                            integer_t a_oflags,
                                            memory_rdwr_t a_mrw,
                                            int a_flags,
                                            struct sockaddr *a_addr,
                                            socklen_t *a6_addrlen) // Runs in user thread
// ********************************************************************************************************************************
{
        tcp_ctx_t tctx = SOCK_TCP_CTX(a_so);

        kern_return_t retval = KERN_SUCCESS;

        natural_t totxfer = a_mrw->uio_resid;

        queue_head_t empty_buffers;
        queue_init(&empty_buffers);

        while (a_mrw->uio_resid > 0 && KERN_STATUS_SUCCESS(retval))
        {
                pbuf *p = nullptr;

                lock_read(&tctx->m_lwip_shared->m_bufferq_lock);
                if ((queue_empty(&tctx->m_lwip_shared->m_bufferq) == true))
                {
                        if (tctx->m_tcp_state > ESTABLISHED)
                        {
                                lock_done(&tctx->m_lwip_shared->m_bufferq_lock);
                                break;
                        }

                        if (tctx->m_lwip_shared->m_flags & TCP_SHARED_DATA_FLAG_CONN_WAS_RESET)
                        {
                                lock_done(&tctx->m_lwip_shared->m_bufferq_lock);
                                retval = KERN_FAIL(ECONNRESET);
                                break;
                        }

                        if (a_oflags & O_NONBLOCK)
                        {
                                retval = KERN_FAIL(EWOULDBLOCK);
                        }
                        else
                        {
                                retval = uthread_sleep_timeout(&tctx->m_lwip_shared->m_bufferq,
                                                               0,
                                                               &tctx->m_lwip_shared->m_bufferq_lock);
                        }
                }
                else
                {
                        p = queue_containing_record(queue_first(&tctx->m_lwip_shared->m_bufferq), struct pbuf, m_sock_link);
                }

                lock_done(&tctx->m_lwip_shared->m_bufferq_lock);

                if (p == nullptr)
                {
                        continue;
                }

                natural_t xfercnt = p->len - p->m_offset;

                if (xfercnt == 0)
                {
                        lock_write(&tctx->m_lwip_shared->m_bufferq_lock);
                        enqueue_tail(&empty_buffers, dequeue_head(&tctx->m_lwip_shared->m_bufferq));
                        lock_done(&tctx->m_lwip_shared->m_bufferq_lock);
                }
                else
                {
                        natural_t oresid = a_mrw->uio_resid;

                        retval = uiomove((((char *)p->payload)) + p->m_offset, xfercnt, a_mrw);

                        if (KERN_STATUS_FAILURE(retval))
                        {
                                break;
                        }

                        xfercnt = (oresid - a_mrw->uio_resid);

                        p->m_offset += (u16_t)(xfercnt);

                        if ((xfercnt > 0) & (p->flags & PBUF_FLAG_PUSH))
                        {
                                break;
                        }
                }
        }

        totxfer -= a_mrw->uio_resid;

        struct args
        {
                tcp_pcb *pcb;
                queue_head_t *q;
                u16_t receved;
        } arg = { .pcb = tctx->tpcb, .q = &empty_buffers, .receved = (u16_t)(totxfer) };

        inet_lwip_thread_execute(
                [](void *a_arg) {
                        struct args *parg = (struct args *)(a_arg);

                        tcp_recved(parg->pcb, parg->receved);

                        while (queue_empty(parg->q) == false)
                        {
                                pbuf_free(queue_containing_record(dequeue_head(parg->q), struct pbuf, m_sock_link));
                        }
                },
                &arg,
                nullptr);

        if (KERN_STATUS_FAILURE(retval) && (totxfer > 0))
        {
                retval = KERN_SUCCESS;
        }

        return retval;
}

// ********************************************************************************************************************************
/** Called periodically as specified by @see tcp_poll.
 *
 * @param arg Additional argument to pass to the callback function (@see tcp_arg())
 * @param tpcb tcp pcb
 * @return ERR_OK: try to send some data by calling tcp_output
 *            Only return ERR_ABRT if you have called tcp_abort from within the
 *            callback function!
 */
// ********************************************************************************************************************************
err_t lwip_tcp_poll_callback(void *arg, struct tcp_pcb *tpcb) // Runs in network thread
// ********************************************************************************************************************************
{
        lwip_tcp_shared_update_state(tpcb);
        lwip_tcp_shared_t lts = (lwip_tcp_shared_t)(arg);

        if (queue_empty(&lts->m_bufferq) == false)
        {
                uthread_wakeup(&lts->m_bufferq);
                socket_poll_thread_nudge();
        }

        switch (tpcb->state)
        {
                case CLOSED:
                case LISTEN:
                case SYN_SENT:
                case SYN_RCVD:
                        break;
                case ESTABLISHED:
                {
                }
                break;
                case FIN_WAIT_1:
                case FIN_WAIT_2:
                case CLOSE_WAIT:
                case CLOSING:
                case LAST_ACK:
                case TIME_WAIT:
                {
                }
                default:
                        break;
        }

        return ERR_OK;
}

// ********************************************************************************************************************************
static integer_t tcp_ip_stream_poll(socket_t a_so, integer_t a_events) // Runs in user or protocol thread
// ********************************************************************************************************************************
{
        tcp_ctx_t tctx = SOCK_TCP_CTX(a_so);

        integer_t revents = 0;

        if (a_events & (POLLIN | POLLRDNORM))
        {
                if (lock_try_read(&tctx->m_lwip_shared->m_bufferq_lock) == true)
                {
                        if (queue_empty(&tctx->m_lwip_shared->m_bufferq) == false)
                        {
                                revents |= (POLLIN | POLLRDNORM);
                        }

                        lock_done(&tctx->m_lwip_shared->m_bufferq_lock);
                }
        }

        /* NOTE: POLLHUP and POLLOUT/POLLWRNORM are mutually exclusive */
        if (tctx->m_tcp_state > ESTABLISHED)
        {
                // The reader on our write endpoint is gone, A.K.A hangup
                revents |= POLLHUP;
        }
        else if (a_events & (POLLOUT | POLLWRNORM))
        {
                if (tctx->m_tcp_state == ESTABLISHED) // Ok for now
                {
                        revents |= (POLLIN | POLLRDNORM);
                }
        }

        return revents;
}

// ********************************************************************************************************************************
static kern_return_t tcp_ip_stream_close(socket_t a_so) // Runs in user thread
// ********************************************************************************************************************************
{
        tcp_ctx_t tctx = SOCK_TCP_CTX(a_so);

        // lwip_tcp_output(tctx->tpcb);
        kern_return_t retval = KERN_SUCCESS;

        if ((tctx->tpcb != nullptr) && !(tctx->m_lwip_shared->m_flags & TCP_SHARED_DATA_FLAG_CONN_WAS_RESET))
        {
                err_t err = lwip_tcp_close(tctx->tpcb);

                retval = lwip_err_to_kern_return(err);
        }
        else
        {
                KASSERT(tctx->m_tcp_state == CLOSED);
        }

        KASSERT(KERN_STATUS_SUCCESS(retval));

        return retval;
}

// ********************************************************************************************************************************
static kern_return_t tcp_ip_stream_shutdown(socket_t a_so, integer_t a_how) // Runs in user thread
// ********************************************************************************************************************************
{
        tcp_ctx_t tctx = SOCK_TCP_CTX(a_so);

        // lwip_tcp_output(tctx->tpcb);

        err_t err = lwip_tcp_shutdown(tctx->tpcb, !!(a_how & SHUT_RD), !!(a_how & SHUT_WR));

        return lwip_err_to_kern_return(err);
}

static struct socket_operations_data s_inet_tcp_ip_stream_socket_ops = {
        .bind     = tcp_ip_stream_bind,     //
        .listen   = tcp_ip_stream_listen,   //
        .accept   = tcp_ip_stream_accept,   //
        .connect  = tcp_ip_stream_connect,  //
        .sendto   = tcp_ip_stream_sendto,   //
        .recvfrom = tcp_ip_stream_recvfrom, //
        .poll     = tcp_ip_stream_poll,     //
        .close    = tcp_ip_stream_close,    //
        .shutdown = tcp_ip_stream_shutdown  //
};

// ********************************************************************************************************************************
static kern_return_t tcp_ip_stream_init()
// ********************************************************************************************************************************
{
        g_inet_proto[INET_PROTO_TCP_IP_DEFAULT] = { .m_domain             = nullptr,
                                                    .m_type               = SOCK_STREAM,
                                                    .m_protocol           = 0,
                                                    .m_private_data_size  = sizeof(struct tcp_ctx_data),
                                                    .m_private_data_align = alignof(struct tcp_ctx_data),
                                                    .m_socket_ops         = &s_inet_tcp_ip_stream_socket_ops,
                                                    .m_proto_init         = tcp_ip_stream_socket_create,
                                                    .m_proto_destroy      = tcp_ip_stream_socket_destroy };

        g_inet_proto[INET_PROTO_TCP_IP_STREAM] = { .m_domain             = nullptr,
                                                   .m_type               = SOCK_STREAM,
                                                   .m_protocol           = IPPROTO_TCP,
                                                   .m_private_data_size  = sizeof(struct tcp_ctx_data),
                                                   .m_private_data_align = alignof(struct tcp_ctx_data),
                                                   .m_socket_ops         = &s_inet_tcp_ip_stream_socket_ops,
                                                   .m_proto_init         = tcp_ip_stream_socket_create,
                                                   .m_proto_destroy      = tcp_ip_stream_socket_destroy };
        s_lwip_shared_freelist.initialize();

        for (lwip_tcp_shared_data &lts : s_lwip_shared_storage)
        {
                s_lwip_shared_freelist.enqueue(&lts);
        }

        s_lwip_tpcb_envelope_freelist.initialize();

        for (lwip_tpcb_envelope &env : s_lwip_tpcb_envelope_storage)
        {
                s_lwip_tpcb_envelope_freelist.enqueue(&env);
        }

        return KERN_SUCCESS;
}

early_initcall(tcp_ip_stream_init);
