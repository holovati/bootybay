#pragma once

#include <lwip/tcp.h>

struct tcp_pcb *lwip_tcp_new(void);

struct tcp_pcb *lwip_tcp_new_ip_type(u8_t type);

void lwip_tcp_arg(struct tcp_pcb *pcb, void *arg);

void lwip_tcp_recved(struct tcp_pcb *pcb, u16_t len);

err_t lwip_tcp_bind(struct tcp_pcb *pcb, const ip_addr_t *ipaddr, u16_t port);

void lwip_tcp_bind_netif(struct tcp_pcb *pcb, const struct netif *netif);

err_t lwip_tcp_connect(struct tcp_pcb *pcb, const ip_addr_t *ipaddr, u16_t port, tcp_connected_fn connected);

struct tcp_pcb *lwip_tcp_listen_with_backlog_and_err(struct tcp_pcb *pcb, u8_t backlog, err_t *err, tcp_accept_fn af);

struct tcp_pcb *lwip_tcp_listen_with_backlog(struct tcp_pcb *pcb, u8_t backlog);

#define lwip_tcp_listen(pcb) lwip_tcp_listen_with_backlog(pcb, TCP_DEFAULT_LISTEN_BACKLOG)

void lwip_tcp_abort(struct tcp_pcb *pcb);

err_t lwip_tcp_close(struct tcp_pcb *pcb);

err_t lwip_tcp_shutdown(struct tcp_pcb *pcb, int shut_rx, int shut_tx);

err_t lwip_tcp_write(struct tcp_pcb *pcb, const void *dataptr, u16_t len, u8_t apiflags);

void lwip_tcp_setprio(struct tcp_pcb *pcb, u8_t prio);

err_t lwip_tcp_output(struct tcp_pcb *pcb);

err_t lwip_tcp_tcp_get_tcp_addrinfo(struct tcp_pcb *pcb, int local, ip_addr_t *addr, u16_t *port);

kern_return_t lwip_err_to_kern_return(err_t a_err);

kern_return_t lwip_tcp_recved_free(struct tcp_pcb *pcb, struct pbuf *p);

kern_return_t lwip_pbuf_free(struct pbuf *p);
