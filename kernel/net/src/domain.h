#pragma once

#include <aux/init.h>

#include <etl/rbtnode.h>

#define protocol_initcall(fn) __define_initcall(protocol, fn)

typedef struct net_domain_data *net_domain_t;

typedef struct net_proto_data *net_proto_t;

typedef struct socket_operations_data *socket_operations_t;

typedef kern_return_t (*proto_rw_cb_t)(struct socket *a_so, void *buf, natural_t a_buf_len, void *user_data);

typedef enum net_usr_req_op
{
        NET_USR_REQ_BIND,
        NET_USR_REQ_LISTEN,
        NET_USR_REQ_ACCEPT,
        NET_USR_REQ_SEND,
        NET_USR_REQ_RECV,
        NET_USR_REQ_CONNECT,
        NET_USR_REQ_CLOSE,
        NET_USR_REQ_SHUTDOWN,
        NET_USR_REQ_USER
} net_usr_req_op_t;

struct net_proto_data
{
        net_domain_t m_domain;
        integer_t m_type;
        integer_t m_protocol;
        natural_t m_private_data_size;
        natural_t m_private_data_align;
        socket_operations_t m_socket_ops;
        kern_return_t (*m_proto_init)(struct socket *);
        void (*m_proto_destroy)(struct socket *);
};

struct net_domain_data
{
        integer_t m_domain;
        natural_t m_sockaddr_size;
        natural_t m_sockaddr_align;
        rbtnode_data m_link;
        net_proto_t m_proto_beg;
        net_proto_t m_proto_end;
};

kern_return_t domain_register(net_domain_t a_proto);

kern_return_t domain_find_proto(integer_t a_dom, integer_t a_type, integer_t a_proto, net_proto_t *a_proto_out);

void domain_do_timeouts(natural_t a_ms_elapsed);
