#include <string.h>

#include <fcntl.h>

#include <sys/ioctl.h>
#include <sys/stat.h>

#include <aux/init.h>

#include <etl/algorithm.hh>
#include <etl/cstring.hh>

#include <kernel/sched_prim.h>
#include <kernel/zalloc.h>
#include <kernel/thread_data.h>

#include <emerixx/fdesc.hh>
#include <emerixx/network.h>
#include <emerixx/process.h>
#include <emerixx/sleep.h>
#include <emerixx/memory_rw.h>
#include <emerixx/ioctl_req.h>
#include <emerixx/vfs/vfs_node_data.h>
#include <emerixx/vfs/vfs_file_data.h>
#include <emerixx/vfs/vfs_file.h>
#include <emerixx/poll.h>

#include "domain.h"
#include "socket.h"

#define FILE_TO_SOCK(f) ((socket_t)((((vm_address_t)(f)->m_vfsn)) - offsetof(struct socket_storage, m_vfsn)))

#define VM_OBJECT_TO_SOCKET_STORAGE(vmo)                                                                                           \
        ((struct socket_storage *)((((vm_address_t)(vmo))) - offsetof(struct socket_storage, m_vfsn)))

#define SOCKET_MAXCONN 5

#define SOCKET_PRIV_DATA_SIZE_MAX (512 / sizeof(natural_t))

static queue_head_t s_allsockq;
static natural_t s_last_socket_id;
static natural_t s_nsockets;

static thread_t s_sock_poll_thread;

struct socket_storage
{
        struct socket m_so;
        struct vfs_node_data m_vfsn;
        queue_chain_t m_allsock_link;
        poll_device_context_t m_poll_context;
        natural_t m_priv[SOCKET_PRIV_DATA_SIZE_MAX];
};

ZONE_DEFINE(socket_storage, s_socket_zone, 0x100, 0x10, ZONE_FIXED, 0x10);

static lock_data_t s_globals_lock;

static kern_return_t socket_fopen(vfs_node_t a_vfsn, vfs_file_t a_vfsf)
{
        return KERN_SUCCESS;
}

static kern_return_t socket_fread(vfs_file_t a_so_file, struct uio *a_uio);
static kern_return_t socket_fwrite(vfs_file_t a_so_file, struct uio *a_uio);
static kern_return_t socket_flseek(vfs_file_t, off_t, int)
{
        return KERN_FAIL(ESPIPE);
}
static kern_return_t socket_fioctl(vfs_file_t a_so_file, ioctl_req_t a_cmd);
static kern_return_t socket_fclose(vfs_file_t a_so_file);
static kern_return_t socket_getdents(vfs_file_t, struct uio *)
{
        return KERN_FAIL(ENOTDIR);
}
static kern_return_t socket_fmmap(vfs_file_t a_so_file, vm_object_t *);
static kern_return_t socket_fpoll(vfs_file_t a_so_file, struct pollfd **a_pfd);
static kern_return_t socket_fsplice(vfs_file_t a_so_file,
                                    off_t *a_off,
                                    vfs_file_t a_file_out,
                                    off_t *a_off_out,
                                    size_t a_len,
                                    unsigned a_flags);

static struct vfs_file_operations_data s_socket_fops = {
        //
        .fopen     = socket_fopen,    //
        .fread     = socket_fread,    //
        .fwrite    = socket_fwrite,   //
        .flseek    = socket_flseek,   //
        .fioctl    = socket_fioctl,   //
        .fclose    = socket_fclose,   //
        .fgetdents = socket_getdents, //
        .fmmap     = socket_fmmap,    //
        .fpoll     = socket_fpoll,    //
        .fsplice   = socket_fsplice
        //
};

// ********************************************************************************************************************************
vfs_node_t socket_to_vfsn(socket_t a_socket)
// ********************************************************************************************************************************
{
        socket_storage *ss = (socket_storage *)(a_socket);
        return &ss->m_vfsn;
}

// ********************************************************************************************************************************
void socket_ref(socket_t a_socket)
// ********************************************************************************************************************************
{
        socket_storage *ss = (socket_storage *)(a_socket);
        vm_object_ref(&ss->m_vfsn.m_vmo);
}

// ********************************************************************************************************************************
void socket_rel(socket_t a_socket)
// ********************************************************************************************************************************
{
        socket_storage *ss = (socket_storage *)(a_socket);
        vm_object_rel(&ss->m_vfsn.m_vmo);
}

// ********************************************************************************************************************************
void socket_lock(socket_t a_socket)
// ********************************************************************************************************************************
{
        socket_storage *ss = (socket_storage *)(a_socket);
        vm_object_lock(&ss->m_vfsn.m_vmo);
}

// ********************************************************************************************************************************
void socket_unlock(socket_t a_socket)
// ********************************************************************************************************************************
{
        socket_storage *ss = (socket_storage *)(a_socket);
        vm_object_unlock(&ss->m_vfsn.m_vmo);
}

// ********************************************************************************************************************************
boolean_t socket_lock_try(socket_t a_socket)
// ********************************************************************************************************************************
{
        socket_storage *ss = (socket_storage *)(a_socket);
        return lock_try_write(&ss->m_vfsn.m_vmo.m_lock);
}

// ********************************************************************************************************************************
static NORETURN void socket_poll_loop()
// ********************************************************************************************************************************
{
        lock_try_read(&s_globals_lock);
        for (queue_entry_t qent = queue_first(&s_allsockq); queue_end(&s_allsockq, qent) == false; qent = queue_next(qent))
        {
                socket_storage *ss = queue_containing_record(qent, socket_storage, m_allsock_link);

                if (socket_lock_try(&ss->m_so))
                {
                        poll_event_t events = poll_enum_wanted_events(&ss->m_poll_context);
                        if (events != 0)
                        {
                                poll_event_t revents = (poll_event_t)ss->m_so.m_proto->m_socket_ops->poll(&ss->m_so, events);

                                if (revents & events)
                                {
                                        poll_wakeup(&ss->m_poll_context, revents);
                                }
                        }
                        socket_unlock(&ss->m_so);
                }
        }
        lock_read_done(&s_globals_lock);
        assert_wait((event_t)socket_poll_loop, true);
        thread_set_timeout(hz * 10);
        thread_block(socket_poll_loop);
        UNREACHABLE();
}

// ********************************************************************************************************************************
void socket_poll_thread_nudge()
// ********************************************************************************************************************************
{
        clear_wait(s_sock_poll_thread, THREAD_AWAKENED, false);
}

// ********************************************************************************************************************************
socket_t socket_find(natural_t a_socket_id)
// ********************************************************************************************************************************
{
        lock_read(&s_globals_lock);

        for (queue_entry_t qent = queue_first(&s_allsockq); queue_end(&s_allsockq, qent) == false; qent = queue_next(qent))
        {
                socket_storage *ss = queue_containing_record(qent, socket_storage, m_allsock_link);

                if (ss->m_so.m_socket_id == a_socket_id)
                {
                        vm_object_ref(&ss->m_vfsn.m_vmo);
                        lock_read_done(&s_globals_lock);
                        return &ss->m_so;
                }
        }

        lock_read_done(&s_globals_lock);
        return nullptr;
}

// ********************************************************************************************************************************
static kern_return_t socket_inactive(vm_object_t a_vm_obj)
// ********************************************************************************************************************************
{
        // This socket is dead and ready for reuse

        socket_storage *ss = VM_OBJECT_TO_SOCKET_STORAGE(a_vm_obj);

        ss->m_so.m_proto->m_proto_destroy(&ss->m_so);

        lock_write(&s_globals_lock);

        remqueue(&ss->m_allsock_link);

        s_socket_zone.free(ss);

        s_nsockets--;

        lock_write_done(&s_globals_lock);

        socket_poll_thread_nudge();

        return KERN_SUCCESS;
}

struct vm_object_operations_data s_socket_vmo_ops = {

        .inactive = socket_inactive
};

// ********************************************************************************************************************************
static kern_return_t network_socket_alloc(integer_t a_domain_no, integer_t a_type_no, integer_t a_proto_no, socket_t *a_socket_out)
// ********************************************************************************************************************************
{
        lock_write(&s_globals_lock);

        net_proto_t proto;

        kern_return_t retval = domain_find_proto(a_domain_no, a_type_no, a_proto_no, &proto);

        if (KERN_STATUS_FAILURE(retval))
        {
                lock_write_done(&s_globals_lock);
                return retval;
        }

        socket_storage *ss = s_socket_zone.alloc();

        vfs_node_init(&ss->m_vfsn);
        vm_object_lock(&ss->m_vfsn.m_vmo);

        ss->m_vfsn.m_mode = S_IFSOCK;

        ss->m_vfsn.m_vmo.m_ops = &s_socket_vmo_ops;

        ss->m_so.m_flags = 0;

        ss->m_so.m_socket_id = ++s_last_socket_id;

        ss->m_so.m_proto         = proto;
        ss->m_so.m_proto_private = (void *)ALIGN((vm_address_t)(ss->m_priv), proto->m_private_data_align);

        ss->m_so.m_sa = (sockaddr *)ALIGN(((vm_address_t)(ss->m_so.m_proto_private)) + proto->m_private_data_size,
                                          proto->m_domain->m_sockaddr_align);

        ss->m_so.m_peer_sa = (sockaddr *)ALIGN(((vm_address_t)(ss->m_so.m_sa)) + proto->m_domain->m_sockaddr_size,
                                               proto->m_domain->m_sockaddr_align);

        poll_device_ctx_init(&ss->m_poll_context);

        KASSERT((((vm_address_t)(ss->m_so.m_peer_sa)) + proto->m_domain->m_sockaddr_size)
                < ((vm_address_t)(&ss->m_priv[SOCKET_PRIV_DATA_SIZE_MAX])));

        etl::clear(ss->m_priv);

        retval = proto->m_proto_init(&ss->m_so);

        if (KERN_STATUS_FAILURE(retval))
        {
                s_socket_zone.free(ss);
                lock_write_done(&s_globals_lock);
                return retval;
        }

        enqueue_head(&s_allsockq, &ss->m_allsock_link);

        s_nsockets++;

        *a_socket_out = &ss->m_so;

        lock_write_done(&s_globals_lock);

        return retval;
}

// ********************************************************************************************************************************
static void network_socket_free(socket_t a_socket)
// ********************************************************************************************************************************
{
        socket_storage *ss = (socket_storage *)(a_socket);
        log_debug("Freeing socket %p", ss);
        s_socket_zone.free(ss);
}

// ********************************************************************************************************************************
kern_return_t network_socket_create(int a_domain_no, int a_type_no, int a_proto_no, int a_oflags)
// ********************************************************************************************************************************
{
        socket_t so;
        kern_return_t retval = network_socket_alloc(a_domain_no, a_type_no, a_proto_no, &so);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        socket_storage *ss = (socket_storage *)(so);

        ss->m_vfsn.m_fops = &s_socket_fops;

        vfs_file_t vfsf = nullptr;

        retval = vfs_file_open(&ss->m_vfsn, a_oflags, &vfsf);

        if (KERN_STATUS_FAILURE(retval))
        {
                panic("Free the socket");
                return retval;
        }

        retval = fd_context_fdalloc(uthread_self(), vfsf);

        if (KERN_STATUS_FAILURE(retval))
        {
                panic("Free the file and the socket");
                return retval;
        }

        vfs_file_rel(vfsf);

        vm_object_put(&ss->m_vfsn.m_vmo);

        socket_poll_thread_nudge();

        return retval;
}

// ********************************************************************************************************************************
kern_return_t network_socket_accept(vfs_file_t a_file, //
                                    struct sockaddr *a_peer,
                                    socklen_t *a_peer_addrlen,
                                    int a_oflags)
// ********************************************************************************************************************************
{

        socket_t so = FILE_TO_SOCK(a_file);

        socket_t newso;

        kern_return_t retval = network_socket_alloc(so->m_proto->m_domain->m_domain, //
                                                    so->m_proto->m_type,
                                                    so->m_proto->m_protocol,
                                                    &newso);

        if (KERN_STATUS_FAILURE(retval))
        {
                return retval;
        }

        socket_lock(so);

        retval = so->m_proto->m_socket_ops->accept(so, a_file->m_oflags, newso);

        vfs_file_t vfsf = nullptr;

        socket_storage *newss = (socket_storage *)(newso);

        if (KERN_STATUS_FAILURE(retval))
        {
                network_socket_free(newso);
                goto out;
        }

        newss->m_vfsn.m_fops = &s_socket_fops;

        retval = vfs_file_open(&newss->m_vfsn, a_oflags, &vfsf);

        if (KERN_STATUS_FAILURE(retval))
        {
                panic("Free the socket");
                return retval;
        }

        retval = fd_context_fdalloc(uthread_self(), vfsf);

        if (KERN_STATUS_FAILURE(retval))
        {
                panic("Free the file and the socket");
                return retval;
        }

        vfs_file_rel(vfsf);

        *a_peer_addrlen = (socklen_t)so->m_proto->m_domain->m_sockaddr_size;
        memcpy(a_peer, so->m_peer_sa, *a_peer_addrlen);

        vm_object_put(&newss->m_vfsn.m_vmo); // unlock and release the socket

out:
        socket_unlock(so);

        socket_poll_thread_nudge();

        return retval;
}

// ********************************************************************************************************************************
kern_return_t socket_fioctl(vfs_file_t a_so_file, ioctl_req_t a_cmd)
// ********************************************************************************************************************************
{
        kern_return_t retval = KERN_SUCCESS;
        panic("gonzo");
        return retval;
}

// ********************************************************************************************************************************
kern_return_t network_socket_recvfrom(vfs_file_t a_file,
                                      struct uio *a_uio,
                                      int a_flags,
                                      struct sockaddr *a_addr,
                                      socklen_t *a_addrlen)
// ********************************************************************************************************************************
{
        socket_t so = FILE_TO_SOCK(a_file);

        socket_lock(so);

        natural_t oresid = a_uio->uio_resid;

        kern_return_t retval = so->m_proto->m_socket_ops->recvfrom(so, a_file->m_oflags, a_uio, a_flags, a_addr, a_addrlen);

        if (KERN_STATUS_SUCCESS(retval))
        {
                retval = (kern_return_t)(oresid - a_uio->uio_resid);
        }

        socket_unlock(so);

        socket_poll_thread_nudge();

        return retval;
}

// ********************************************************************************************************************************
kern_return_t network_socket_sendto(vfs_file_t a_file,
                                    struct uio *a_uio,
                                    int a_flags,
                                    struct sockaddr *a_addr,
                                    socklen_t *a_addrlen)
// ********************************************************************************************************************************
{
        socket_t so = FILE_TO_SOCK(a_file);

        socket_lock(so);
        natural_t oresid = a_uio->uio_resid;

        kern_return_t retval = so->m_proto->m_socket_ops->sendto(so, a_file->m_oflags, a_uio, a_flags, a_addr, a_addrlen);

        if (KERN_STATUS_SUCCESS(retval))
        {
                retval = (kern_return_t)(oresid - a_uio->uio_resid);
        }

        socket_unlock(so);

        socket_poll_thread_nudge();

        return retval;
}

// ********************************************************************************************************************************
kern_return_t network_socket_getpeername(vfs_file_t a_file, struct sockaddr *a_addr, socklen_t *a_addrlen)
// ********************************************************************************************************************************
{
        socket_t so = FILE_TO_SOCK(a_file);

        *a_addrlen = (socklen_t)etl::min(so->m_proto->m_domain->m_sockaddr_size, (natural_t)*a_addrlen);

        memcpy(a_addr, so->m_peer_sa, *a_addrlen);

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t socket_fpoll(vfs_file_t a_file, struct pollfd **a_pfd)
// ********************************************************************************************************************************
{
        socket_t so = FILE_TO_SOCK(a_file);

        socket_lock(so);
        poll_event_t revents = (poll_event_t)so->m_proto->m_socket_ops->poll(so, (*a_pfd)->events);
        (*a_pfd)->revents |= ((poll_event_t)((*a_pfd)->events & revents));
        kern_return_t retval = poll_record(&((socket_storage *)(so))->m_poll_context, a_pfd);
        socket_unlock(so);

        return retval;
}

// ********************************************************************************************************************************
kern_return_t network_socket_connect(vfs_file_t a_file, struct sockaddr *a_addr, socklen_t a_addrlen)
// ********************************************************************************************************************************
{
        socket_t so = FILE_TO_SOCK(a_file);

        if (a_addrlen > so->m_proto->m_domain->m_sockaddr_size)
        {
                return KERN_FAIL(EINVAL);
        }

        if (so->m_proto->m_domain->m_domain != a_addr->sa_family)
        {
                return KERN_FAIL(EAFNOSUPPORT);
        }

        socket_lock(so);

        kern_return_t retval = so->m_proto->m_socket_ops->connect(so, a_file->m_oflags, a_addr, a_addrlen);

        if (KERN_STATUS_SUCCESS(retval))
        {
                memcpy(so->m_peer_sa, a_addr, a_addrlen);
        }

        socket_unlock(so);

        socket_poll_thread_nudge();

        return retval;
}

// ********************************************************************************************************************************
kern_return_t socket_fread(vfs_file_t a_file, struct uio *a_uio)
// ********************************************************************************************************************************
{
        socket_t so = FILE_TO_SOCK(a_file);

        socket_lock(so);
        kern_return_t retval = so->m_proto->m_socket_ops->recvfrom(so, a_file->m_oflags, a_uio, 0, nullptr, nullptr);
        socket_unlock(so);

        socket_poll_thread_nudge();

        return retval;

        // return network_socket_recvfrom(a_file, a_uio, 0, nullptr, nullptr);
}

// ********************************************************************************************************************************
kern_return_t socket_fwrite(vfs_file_t a_file, struct uio *a_uio)
// ********************************************************************************************************************************
{
        socket_t so = FILE_TO_SOCK(a_file);

        socket_lock(so);
        kern_return_t retval = so->m_proto->m_socket_ops->sendto(so, a_file->m_oflags, a_uio, 0, nullptr, nullptr);
        socket_unlock(so);

        socket_poll_thread_nudge();

        return retval;

        // return network_socket_sendto(a_file, a_uio, 0, nullptr, nullptr);
}

// ********************************************************************************************************************************
kern_return_t socket_fclose(vfs_file_t a_file)
// ********************************************************************************************************************************
{
        socket_t so = FILE_TO_SOCK(a_file);

        socket_lock(so);
        kern_return_t retval = so->m_proto->m_socket_ops->close(so);
        socket_unlock(so);

        socket_poll_thread_nudge();

        return retval;
}

// ********************************************************************************************************************************
kern_return_t socket_fmmap(vfs_file_t, vm_object_t *)
// ********************************************************************************************************************************
{
        panic("noop");
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t socket_fsplice(vfs_file_t, off_t *a_off, vfs_file_t a_file_out, off_t *a_off_out, size_t a_len, unsigned a_flags)
// ********************************************************************************************************************************
{
        panic("noop");
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t network_socket_sendmsg(vfs_file_t a_file, struct msghdr *a_msg, int a_flags)
// ********************************************************************************************************************************
{
        panic("noop");
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t network_socket_recvmsg(vfs_file_t a_file, struct msghdr *a_msg, int a_flags)
// ********************************************************************************************************************************
{
        panic("noop");
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t network_socket_shutdown(vfs_file_t a_file, int a_how)
// ********************************************************************************************************************************
{
        socket_t so = FILE_TO_SOCK(a_file);

        kern_return_t retval = so->m_proto->m_socket_ops->shutdown(so, a_how);

        return retval;
}

// ********************************************************************************************************************************
kern_return_t network_socket_bind(vfs_file_t a_file, struct sockaddr *a_addr, socklen_t a_addrlen)
// ********************************************************************************************************************************
{
        socket_t so = FILE_TO_SOCK(a_file);

        if (a_addrlen > so->m_proto->m_domain->m_sockaddr_size)
        {
                return KERN_FAIL(EINVAL);
        }

        if (a_addr->sa_family != ((sa_family_t)so->m_proto->m_domain->m_domain))
        {
                return KERN_FAIL(EINVAL);
        }

        socket_lock(so);
        kern_return_t retval = so->m_proto->m_socket_ops->bind(so, a_addr, a_addrlen);
        socket_unlock(so);

        if (KERN_STATUS_SUCCESS(retval))
        {
                memcpy(so->m_sa, a_addr, a_addrlen);
                socket_poll_thread_nudge();
        }

        return retval;
}

// ********************************************************************************************************************************
kern_return_t network_socket_listen(vfs_file_t a_file, int a_backlog)
// ********************************************************************************************************************************
{
        integer_t maxconn = etl::min(a_backlog, SOCKET_MAXCONN);

        socket_t so = FILE_TO_SOCK(a_file);

        socket_lock(so);
        kern_return_t retval = so->m_proto->m_socket_ops->listen(so, a_file->m_oflags, maxconn);
        socket_unlock(so);

        socket_poll_thread_nudge();

        return retval;
}

// ********************************************************************************************************************************
kern_return_t network_socket_getname(vfs_file_t a_file, struct sockaddr *a_addr, socklen_t *a_addrlen)
// ********************************************************************************************************************************
{
        socket_t so = FILE_TO_SOCK(a_file);

        socket_lock(so);

        if (so->m_proto->m_domain->m_sockaddr_size > *a_addrlen)
        {
                return KERN_FAIL(EINVAL);
        }

        *a_addrlen = (socklen_t)so->m_proto->m_domain->m_sockaddr_size;

        memcpy(a_addr, so->m_sa, *a_addrlen);

        socket_unlock(so);

        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t network_socket_setsockopt(vfs_file_t a_file,
                                        int a_level,
                                        int a_opt_name,
                                        char *a_opt_value,
                                        socklen_t a_opt_value_len)
// ********************************************************************************************************************************
{
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
kern_return_t network_socket_getsockopt(vfs_file_t a_file,
                                        int a_level,
                                        int a_opt_name,
                                        char *a_opt_value,
                                        socklen_t *a_opt_value_len)
// ********************************************************************************************************************************
{
        return KERN_SUCCESS;
}

// ********************************************************************************************************************************
static kern_return_t network_early_init()
// ********************************************************************************************************************************
{
        lock_init(&s_globals_lock, true);
        queue_init(&s_allsockq);
        s_last_socket_id = 0;
        return KERN_SUCCESS;
}

early_initcall(network_early_init);

// ********************************************************************************************************************************
static kern_return_t socket_subsys_init()
// ********************************************************************************************************************************
{
        s_sock_poll_thread = kernel_thread(kernel_task, socket_poll_loop, nullptr);
        thread_suspend(s_sock_poll_thread);
        s_sock_poll_thread->max_priority = BASEPRI_USER;
        s_sock_poll_thread->priority     = BASEPRI_USER;
        s_sock_poll_thread->sched_pri    = BASEPRI_USER;
        thread_resume(s_sock_poll_thread);

        return KERN_SUCCESS;
}

subsys_initcall(socket_subsys_init);
