#include <lwip/timeouts.h>

#include <aux/init.h>

#include <etl/queue.h>

#include <kernel/sched_prim.h>
#include <kernel/thread.h>

#include <kernel/thread_data.h>

#include <emerixx/net/iface_data.h>
#include <emerixx/net/iface_buffer_data.h>
#include <emerixx/net/thread_hook_data.h>

#include <emerixx/net/net_core.h>

static thread_t s_net_core_thread;

static queue_head_t s_thread_hooks;

static queue_head_t s_pollq;

static queue_head_t s_incoming;

static queue_head_t s_outgoing;

// ********************************************************************************************************************************
void net_core_send(iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        enqueue_tail(&s_outgoing, &a_iface_buffer->m_gp_link);
}

// ********************************************************************************************************************************
void net_core_receive(iface_buffer_t a_iface_buffer)
// ********************************************************************************************************************************
{
        enqueue_tail(&s_incoming, &a_iface_buffer->m_gp_link);
}

// ********************************************************************************************************************************
void net_core_rx_sched(iface_t a_iface)
// ********************************************************************************************************************************
{
        if ((a_iface->m_flags & IFACE_ON_RX_SCHED_LIST) == 0)
        {
                enqueue_tail(&s_pollq, &a_iface->m_sched_link);
                a_iface->m_flags |= IFACE_ON_RX_SCHED_LIST;
                clear_wait(s_net_core_thread, THREAD_INTERRUPTED, false);
        }
}

// ********************************************************************************************************************************
void net_core_thread_nudge()
// ********************************************************************************************************************************
{
        clear_wait(s_net_core_thread, THREAD_INTERRUPTED, false);
}

// ********************************************************************************************************************************
void net_core_thread_hook_install(net_thread_hook_t a_thread_hook)
// ********************************************************************************************************************************
{
        enqueue_tail(&s_thread_hooks, &a_thread_hook->m_all_hooks);
}

// ********************************************************************************************************************************
static NORETURN void net_core_thread_entry()
// ********************************************************************************************************************************
{
again:
        sys_check_timeouts();

        spl_t s = splbio();
        while (queue_empty(&s_pollq) == false)
        {
                queue_entry_t qentry = dequeue_head(&s_pollq);
                iface_t netdev       = queue_containing_record(qentry, struct iface_data, m_sched_link);
                netdev->m_flags &= ~IFACE_ON_RX_SCHED_LIST;
                splx(s);
                netdev->rx(netdev);
                s = splbio();
        }
        splx(s);

        while (queue_empty(&s_incoming) == false)
        {
                queue_entry_t qentry = dequeue_head(&s_incoming);

                iface_buffer_t ifbuf = queue_containing_record(qentry, struct iface_buffer_data, m_gp_link);

                ifbuf->m_iface->input(ifbuf);
        }

        while (queue_empty(&s_outgoing) == false)
        {
                queue_entry_t qentry = dequeue_head(&s_outgoing);

                iface_buffer_t ifbuf = queue_containing_record(qentry, struct iface_buffer_data, m_gp_link);

                ifbuf->m_iface->tx(ifbuf->m_iface, ifbuf);
        }

        for (queue_entry_t qentry = queue_first(&s_thread_hooks); !queue_end(&s_thread_hooks, qentry); qentry = queue_next(qentry))
        {
                net_thread_hook_t nth = queue_containing_record(qentry, struct net_thread_hook_data, m_all_hooks);

                nth->m_callback(nth->m_user_data);
        }

        if (queue_empty(&s_outgoing) == false || queue_empty(&s_incoming) == false)
        {
                goto again;
        }

        sys_check_timeouts();

        s = splbio();
        if (queue_empty(&s_pollq) == false)
        {
                splx(s);
                goto again;
        }

        assert_wait((event_t)net_core_thread_entry, true);
        splx(s);
        thread_set_timeout(hz / 20);
        thread_block(net_core_thread_entry);
        UNREACHABLE();
}

// ********************************************************************************************************************************
static kern_return_t net_core_init()
// ********************************************************************************************************************************
{
        // Initialize protocols
        extern initcall_t __initcall_pr_start[];
        extern initcall_t __initcall_pr_end[];
        size_t n_calls = (size_t)(((size_t)__initcall_pr_end - (size_t)__initcall_pr_start) / sizeof(uintptr_t));
        for (size_t i = 0; i < n_calls; ++i)
        {
                KASSERT((__initcall_pr_start[i]()) == 0);
        }

        s_net_core_thread = kernel_thread(kernel_task, net_core_thread_entry, nullptr);
        thread_suspend(s_net_core_thread);
        s_net_core_thread->max_priority = BASEPRI_USER;
        s_net_core_thread->priority     = BASEPRI_USER;
        s_net_core_thread->sched_pri    = BASEPRI_USER;
        thread_resume(s_net_core_thread);

        return KERN_SUCCESS;
}
subsys_initcall(net_core_init);

// ********************************************************************************************************************************
static kern_return_t net_core_early_init()
// ********************************************************************************************************************************
{
        queue_init(&s_pollq);

        queue_init(&s_thread_hooks);

        queue_init(&s_incoming);

        queue_init(&s_outgoing);

        return KERN_SUCCESS;
}

early_initcall(net_core_early_init);
