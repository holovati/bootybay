#pragma once

#ifndef __NEED_net_iface
#define __NEED_net_iface
#endif

#include <emerixx/net/types.h>

#include <etl/queue.h>

struct iface_buffer_data
{
        queue_chain_t m_gp_link;
        vm_address_t m_phys_addr;
        vm_address_t m_virt_addr;
        natural_t m_packet_size;
        natural_t m_buffer_size;
        natural_t m_data_offset;
        iface_t m_iface;
};
