#pragma once

#ifndef __NEED_net_iface_buffer
#define __NEED_net_iface_buffer
#endif

#include <emerixx/net/types.h>

void ethernet_input(iface_buffer_t a_iface_buffer);
