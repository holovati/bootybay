#if defined(__NEED_net_iface_buffer) && !defined(__DEFINED_net_iface_buffer)
#define __DEFINED_net_iface_buffer
typedef struct iface_buffer_data *iface_buffer_t;
#endif

#if defined(__NEED_iface_private) && !defined(__DEFINED_iface_private)
#define __DEFINED_iface_private
typedef void *iface_private_t;
#endif

#if defined(__NEED_net_iface) && !defined(__DEFINED_net_iface)
#define __DEFINED_net_iface
typedef struct iface_data *iface_t;
#endif

#if defined(__NEED_net_thread_hook_data) && !defined(__DEFINED_net_thread_hook_data)
#define __DEFINED_net_thread_hook_data
typedef struct net_thread_hook_data *net_thread_hook_t;
#endif
