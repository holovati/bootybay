#pragma once

#ifndef __NEED_net_iface
#define __NEED_net_iface
#endif

#ifndef __NEED_net_iface_buffer
#define __NEED_net_iface_buffer
#endif

#include <emerixx/net/types.h>

#include <etl/queue.h>
#include <etl/bit.hh>

#define IFACE_ON_RX_SCHED_LIST etl::bit<decltype(iface_data::m_flags), 0>::value

struct iface_data
{
        natural_t m_flags;
        queue_chain_t m_sched_link;
        uint8_t *m_mac_addr;
        void (*rx)(iface_t);
        void (*tx)(iface_t, iface_buffer_t);
        void (*input)(iface_buffer_t);
};
