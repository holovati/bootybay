#pragma once

#include <etl/queue.h>

struct net_thread_hook_data
{
        queue_chain_t m_all_hooks;
        void *m_user_data;
        void (*m_callback)(void *);
};
