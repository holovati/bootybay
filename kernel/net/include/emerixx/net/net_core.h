#pragma once

#ifndef __NEED_net_iface
#define __NEED_net_iface
#endif

#ifndef __NEED_net_iface_buffer
#define __NEED_net_iface_buffer
#endif

#ifndef __NEED_net_thread_hook_data
#define __NEED_net_thread_hook_data
#endif

#include <emerixx/net/types.h>

void net_core_send(iface_buffer_t a_iface_buffer);

void net_core_receive(iface_buffer_t a_iface_buffer);

void net_core_rx_sched(iface_t a_iface);

void net_core_thread_nudge();

void net_core_thread_hook_install(net_thread_hook_t a_thread_hook);
