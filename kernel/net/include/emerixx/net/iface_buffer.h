#pragma once

#ifndef __NEED_net_iface
#define __NEED_net_iface
#endif

#ifndef __NEED_net_iface_buffer
#define __NEED_net_iface_buffer
#endif

#include <emerixx/net/types.h>

kern_return_t iface_buffer_alloc(iface_t a_iface, iface_buffer_t *a_iface_buffer_out);

void iface_buffer_free(iface_buffer_t a_iface_buffer);

struct pbuf *iface_buffer_lwip_pbuf(iface_buffer_t a_iface_buffer);

iface_buffer_t iface_buffer_from_lwip_pbuf(struct pbuf *a_pbuf);
