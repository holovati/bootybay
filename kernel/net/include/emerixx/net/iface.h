#pragma once

#ifndef __NEED_net_iface
#define __NEED_net_iface
#endif

#ifndef __NEED_net_iface_buffer
#define __NEED_net_iface_buffer
#endif

#ifndef __NEED_iface_private
#define __NEED_iface_private
#endif

#include <emerixx/net/types.h>

iface_t iface_alloc(natural_t a_private_size, natural_t a_private_align);

void iface_add(iface_t a_iface);

iface_private_t iface_private(iface_t a_iface);

struct netif *iface_lwip_netif(iface_t a_iface);

iface_t lwip_netif_iface(struct netif *a_netif);
