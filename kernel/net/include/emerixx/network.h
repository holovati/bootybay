#pragma once

kern_return_t network_socket_create(int a_domain, //
                                    int a_type,
                                    int a_proto,
                                    int a_oflags);

kern_return_t network_socket_accept(vfs_file_t, //
                                    struct sockaddr *a_peer,
                                    socklen_t *a_peer_addrlen,
                                    int a_flags);

kern_return_t network_socket_getpeername(vfs_file_t, struct sockaddr *a_addr, socklen_t *a_addrlen);

kern_return_t network_socket_recvfrom(vfs_file_t, //
                                      struct uio *a_uio,
                                      int a_flags,
                                      struct sockaddr *a_addr,
                                      socklen_t *a_addrlen);

kern_return_t network_socket_sendto(vfs_file_t, //
                                    struct uio *a_uio,
                                    int a_flags,
                                    struct sockaddr *a_addr,
                                    socklen_t *a_addrlen);

kern_return_t network_socket_connect(vfs_file_t, struct sockaddr *a_addr, socklen_t a_addrlen);

kern_return_t network_socket_getname(vfs_file_t, sockaddr *a_addr, socklen_t *a_socklen);

kern_return_t network_socket_bind(vfs_file_t, sockaddr *a_addr, socklen_t a_socklen);

kern_return_t network_socket_shutdown(vfs_file_t, int a_how);

kern_return_t network_socket_listen(vfs_file_t, int a_backlog);

kern_return_t network_socket_sendmsg(vfs_file_t a_file, struct msghdr *a_msg, int a_flags);

kern_return_t network_socket_recvmsg(vfs_file_t a_file, struct msghdr *a_msg, int a_flags);

kern_return_t network_socket_setsockopt(vfs_file_t a_file, int a_level, int a_opt_name, char *a_opt_value, socklen_t a_opt_value_len);

kern_return_t network_socket_getsockopt(vfs_file_t a_file, int a_level, int a_opt_name, char *a_opt_value, socklen_t *a_opt_value_len);