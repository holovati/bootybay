#pragma once

#define count_of(x) (sizeof(x) / sizeof(x[0]))

namespace etl
{

// ********************************************************************************************************************************
static inline void *memcpy64(void *a_dest, void const *a_src, size_t a_count)
// ********************************************************************************************************************************
{
#if defined(__x86_64__)
        asm volatile("rep movsq"
                     : "=D"(a_dest), "=S"(a_src), "=c"(a_count)
                     : "D"(a_dest), "S"(a_src), "c"(a_count)
                     : /* No clobber */);
#else
        for (size_t i = 0; i < count; ++count)
        {
                reinterpret_cast<uint64_t *>(dest)[i] = reinterpret_cast<uint64_t *>(src)[i];
        }
#endif
        return const_cast<void *>(a_src);
}

// ********************************************************************************************************************************
static inline void *memcpy32(void *a_dest, void const *a_src, size_t a_count)
// ********************************************************************************************************************************
{
#if defined(__x86_64__)
        asm volatile("rep movsd"
                     : "=D"(a_dest), "=S"(a_src), "=c"(a_count)
                     : "D"(a_dest), "S"(a_src), "c"(a_count)
                     : /* No clobber */);
#else
        for (size_t i = 0; i < count; ++count)
        {
                reinterpret_cast<uint32_t *>(dest)[i] = reinterpret_cast<uint32_t *>(src)[i];
        }
#endif
        return const_cast<void *>(a_src);
}

// ********************************************************************************************************************************
static inline void *memcpy16(void *a_dest, void const *a_src, size_t a_count)
// ********************************************************************************************************************************
{
#if defined(__x86_64__)
        asm volatile("rep movsw"
                     : "=D"(a_dest), "=S"(a_src), "=c"(a_count)
                     : "D"(a_dest), "S"(a_src), "c"(a_count)
                     : /* No clobber */);
#else
        for (size_t i = 0; i < count; ++count)
        {
                reinterpret_cast<uint16_t *>(dest)[i] = reinterpret_cast<uint16_t *>(src)[i];
        }
#endif
        return const_cast<void *>(a_src);
}

// ********************************************************************************************************************************
static inline void *memcpy8(void *a_dest, void const *a_src, size_t a_count)
// ********************************************************************************************************************************
{
#if defined(__x86_64__)
        asm volatile("rep movsb"
                     : "=D"(a_dest), "=S"(a_src), "=c"(a_count)
                     : "D"(a_dest), "S"(a_src), "c"(a_count)
                     : /* No clobber */);
#else
        for (size_t i = 0; i < count; ++count)
        {
                reinterpret_cast<uint8_t *>(dest)[i] = reinterpret_cast<uint8_t *>(src)[i];
        }
#endif
        return const_cast<void *>(a_src);
}

// ********************************************************************************************************************************
static inline void *memset64(void *a_dest, integer_t a_val, size_t a_count)
// ********************************************************************************************************************************
{
#if defined(__x86_64__)
        asm volatile("rep stosq" : "=D"(a_dest), "=c"(a_count) : "D"(a_dest), "a"(a_val), "c"(a_count) : /* No clobber */);
#else
        for (size_t i = 0; i < count; ++count)
        {
                reinterpret_cast<uint64_t *>(dest)[i] = static_cast<uint64_t>(a_val);
        }
#endif
        return const_cast<void *>(a_dest);
}

// ********************************************************************************************************************************
static inline void *memset32(void *a_dest, integer_t a_val, size_t a_count)
// ********************************************************************************************************************************
{
#if defined(__x86_64__)
        asm volatile("rep stosd" : "=D"(a_dest), "=c"(a_count) : "D"(a_dest), "a"(a_val), "c"(a_count) : /* No clobber */);
#else
        for (size_t i = 0; i < count; ++count)
        {
                reinterpret_cast<uint32_t *>(dest)[i] = static_cast<uint32_t>(a_val);
        }
#endif
        return const_cast<void *>(a_dest);
}

// ********************************************************************************************************************************
static inline void *memset16(void *a_dest, integer_t a_val, size_t a_count)
// ********************************************************************************************************************************
{
#if defined(__x86_64__)
        asm volatile("rep stosw" : "=D"(a_dest), "=c"(a_count) : "D"(a_dest), "a"(a_val), "c"(a_count) : /* No clobber */);
#else
        for (size_t i = 0; i < count; ++count)
        {
                reinterpret_cast<uint16_t *>(dest)[i] = static_cast<uint16_t>(a_val);
        }
#endif
        return const_cast<void *>(a_dest);
}

// ********************************************************************************************************************************
static inline void *memset8(void *a_dest, integer_t a_val, size_t a_count)
// ********************************************************************************************************************************
{
#if defined(__x86_64__)
        asm volatile("rep stosb" : "=D"(a_dest), "=c"(a_count) : "D"(a_dest), "a"(a_val), "c"(a_count) : /* No clobber */);
#else
        for (size_t i = 0; i < count; ++count)
        {
                reinterpret_cast<uint8_t *>(dest)[i] = static_cast<uint8_t>(a_val);
        }
#endif
        return const_cast<void *>(a_dest);
}

} // namespace etl