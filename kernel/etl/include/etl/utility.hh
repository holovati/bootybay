#pragma once

namespace etl
{

template <typename T> struct remove_reference
{
        typedef T type;
};

template <typename T> struct remove_reference<T &>
{
        typedef T type;
};

template <typename T> struct remove_reference<T &&>
{
        typedef T type;
};

template <typename T>
// ********************************************************************************************************************************
constexpr typename etl::remove_reference<T>::type &&move(T &&a_obj)
// ********************************************************************************************************************************
{
        return static_cast<typename etl::remove_reference<T>::type &&>(a_obj);
}

} // namespace etl