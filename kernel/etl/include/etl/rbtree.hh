#pragma once
// clang-format off

#if defined(__cplusplus)
#if !defined(RBTNODE_FUNCTION_DEFINITIONS)
#define RBTNODE_FUNCTION_DEFINITIONS
#endif

#include "rbtnode.h"

template <typename T, rbtnode_data T::*FIELD, bool UNIQUE_KEY> //
struct rbtree;

#define __RBTREE(c, l, uniqkey) rbtree<c, &c::l, uniqkey>
#define RBTREE_UNIQUE_KEY(c, l) __RBTREE(c, l, true)
#define RBTREE_MULTI_KEY(c, l)  __RBTREE(c, l, false)

#if defined(RBTREE_IMPLEMENTATION)


template <typename T, typename U, rbtnode_data U::*FIELD, typename MAPOP> static inline bool operator < (T const &, U &);

template <typename T, typename U, rbtnode_data U::*FIELD, typename MAPOP> static inline bool operator < (U &, T const &);

template <typename T, typename U, rbtnode_data U::*FIELD, typename MAPOP> static inline bool operator < (U &, U &);


#define RBTREE_INSERT_LESS(map, cmptype)                                                                                           \
        template <> inline bool operator < <cmptype, map::object_type, map::F, map::map_insert>

#define RBTREE_FIND_LESS(map, cmptype) template <> inline bool operator < <cmptype, map::object_type, map::F, map::map_find>

template <typename T, rbtnode_data T::*FIELD, bool UNIQUE_KEY> //
struct rbtree
{
        using object_type                  = T;
        static constexpr decltype(FIELD) F = FIELD;

        template <bool B, class Z = void> //
        struct rbtree_enable_if
        {
        };

        template <class Z> //
        struct rbtree_enable_if<true, Z>
        {
                typedef Z type;
        };

        template <bool ENABLE> //
        using cond_ret_object_t = typename rbtree_enable_if<ENABLE, T>::type *;

        struct map_insert;
        struct map_find;

        rbtnode_data m_sentinel;
        rbtnode_t m_root;
        void init() { m_root = rbtnode_init_sentinel(&m_sentinel); }

        template <typename TKEY> //
        T *find(TKEY const &a_key)
        {
                bool key_is_less;
                T *object = to_object(m_root);
                for (rbtnode_t x = m_root; !rbtnode_is_sentinel(x);
                     object      = to_object((x = x->fields[!key_is_less])))
                {
                        key_is_less = is_less<decltype(a_key), map_find>(a_key, *object);
                        if (!key_is_less && !is_less<decltype(a_key), map_find>(*object, a_key))
                        {
                                return object;
                        }
                }
                return nullptr;
        }

        template <typename TKEY, size_t N> //
        T *find(TKEY const (&a_key)[N])
        {
                return find<decltype(&a_key[0])>(&a_key[0]);
        }

        template <typename TKEY> //
        T *find_after(TKEY const &a_key, T* a_root)
        {
                for (T *x = a_root; !rbtnode_is_sentinel(&(x->*F));
                     x = to_object((x->*F).fields[!is_less<decltype(a_key), map_find>(a_key, *x)]))
                {
                        if (is_equal<decltype(a_key), map_find>(a_key, *x))
                        {
                                return x;
                        }
                }
                return nullptr;
        }

        template <typename TKEY, bool UK = UNIQUE_KEY> //
        cond_ret_object_t<UK == true> insert(TKEY const &a_key, T *a_object)
        {
                rbtnode_t *x, y;
                bool key_is_less;
                for (x = &m_root, y = m_root; !rbtnode_is_sentinel(*x);
                     y = *x, x = &((*x)->fields[!key_is_less]))
                {
                        key_is_less = is_less<decltype(a_key), map_insert>(a_key, *to_object(*x));
                        if (!key_is_less && !is_less<decltype(a_key), map_insert>(*to_object(*x), a_key))
                        {
                                return to_object(*x);
                        }
                }
                rbtnode_insert(&(a_object->*FIELD), x, y, &m_root, &m_sentinel);
                return a_object;
        }

        template <typename TKEY, bool UK = UNIQUE_KEY> //
        cond_ret_object_t<UK == false> insert(TKEY const &a_key, T *a_object)
        {
                rbtnode_t *x, y;
                for (x = &m_root, y = m_root; !rbtnode_is_sentinel(*x);
                     y = *x, x = &((*x)->fields[!is_less<decltype(a_key), map_insert>(a_key, *to_object(*x))]))
                {
                        ;
                        ;
                }
                rbtnode_insert(&(a_object->*FIELD), x, y, &m_root, &m_sentinel);
                return a_object;
        }

        template <typename TKEY, RBTNODE_SIZE_TYPE N> //
        T *insert(TKEY const (&a_key)[N], T *a_object)
        {
                return insert<decltype(&a_key[0])>(&a_key[0], a_object);
        }

        T *insert(T *a_object) { return insert<decltype(*a_object)>(*a_object, const_cast<T *>(a_object)); }

        void remove(T *a_object) { rbtnode_delete(&(a_object->*FIELD), &m_root); }

        bool empty() { return rbtnode_is_sentinel(m_root); }

        T *first()
        {
                rbtnode_t n = rbtnode_min(m_root);
                T *object   = nullptr;
                if (!rbtnode_is_sentinel(n))
                {
                        object = to_object(n);
                }
                return object;
        }

        T *last()
        {
                rbtnode_t n = rbtnode_max(m_root);
                T *object   = nullptr;
                if (!rbtnode_is_sentinel(n))
                {
                        object = to_object(n);
                }
                return object;
        }

        static T *next(T *a_object)
        {
                rbtnode_t n = rbtnode_next(&(a_object->*FIELD));
                a_object    = nullptr;
                if (!rbtnode_is_sentinel(n))
                {
                        a_object = to_object(n);
                }
                return a_object;
        }

        static T *prev(T *a_object)
        {
                rbtnode_t n = rbtnode_prev(&(a_object->*FIELD));
                a_object    = nullptr;
                if (!rbtnode_is_sentinel(n))
                {
                        a_object = to_object(n);
                }
                return a_object;
        }

        static T *min(T *a_tree_root)
        {
                T *object = nullptr;
                rbtnode_t n;
                if (!rbtnode_is_sentinel((n = rbtnode_min(&(a_tree_root->*FIELD)))))
                {
                        object = to_object(n);
                }
                return object;
        }

        static T *max(T *a_tree_root)
        {
                T *object = nullptr;
                rbtnode_t n;
                if (!rbtnode_is_sentinel((n = rbtnode_max(&(a_tree_root->*FIELD)))))
                {
                        object = to_object(n);
                }
                return object;
        }

        static T *left(T *a_node)
        {
                T *object = nullptr;
                rbtnode_t n;
                if (!rbtnode_is_sentinel((n = rbtnode_left(&(a_node->*FIELD)))))
                {
                        object = to_object(n);
                }
                return object;
        }

        static T *right(T *a_node)
        {
                T *object = nullptr;
                rbtnode_t n;
                if (!rbtnode_is_sentinel((n = rbtnode_right(&(a_node->*FIELD)))))
                {
                        object = to_object(n);
                }
                return object;
        }

        static T *to_object(rbtnode_t const a_node)
        {
                return reinterpret_cast<T *>(reinterpret_cast<RBTNODE_UINTPTR_TYPE>(a_node)
                                             - reinterpret_cast<RBTNODE_UINTPTR_TYPE>(&(static_cast<T *>(nullptr)->*FIELD)));
        }

        template <typename K, typename MAPOP> static bool is_less(T &lhs, K &rhs)
        {
                return ::operator < <K, T, FIELD, MAPOP>(lhs, rhs);
        }

        template <typename K, typename MAPOP> static bool is_less(K &lhs, T &rhs)
        {
                return ::operator < <K, T, FIELD, MAPOP>(lhs, rhs);
        }

        template <typename K, typename MAPOP> static bool is_less(T &lhs, T &rhs)
        {
                return ::operator < <T, T, FIELD, MAPOP>(lhs, rhs);
        }

        template <typename K, typename MAPOP> static bool is_equal(K &lhs, T &rhs)
        {
                return !is_less<K, MAPOP>(lhs, rhs) && !is_less<K, MAPOP>(rhs, lhs);
        }

        template <typename K, typename MAPOP> static bool is_equal(T &lhs, K &rhs)
        {
                return !is_less<K, MAPOP>(lhs, rhs) && !is_less<K, MAPOP>(rhs, lhs);
        }

        template <typename K, typename MAPOP> static bool is_equal(T &lhs, T &rhs)
        {
                return !is_less<T, MAPOP>(lhs, rhs) && !is_less<T, MAPOP>(rhs, lhs);
        }
};

#endif // RBTREE_IMPLEMENTATION
#endif // __cplusplus
