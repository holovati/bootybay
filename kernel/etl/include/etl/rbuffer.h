#pragma once

#if !defined(RBUFFER_SIZE_TYPE)
#define RBUFFER_SIZE_TYPE unsigned long
#endif

#if !defined(RBUFFER_BOOL_TYPE)
#define RBUFFER_BOOL_TYPE int
#endif

#if !defined(RBUFFER_INLINE)
#define RBUFFER_INLINE inline
#endif

#if !defined(RBUFFER_STORAGE_CLASS)
#define RBUFFER_STORAGE_CLASS
#endif

typedef RBUFFER_SIZE_TYPE (*rbuffer_rw_cb)(RBUFFER_SIZE_TYPE a_pos, RBUFFER_SIZE_TYPE a_count, void *a_ud);

#if defined(__cplusplus)
extern "C" {
#endif

RBUFFER_STORAGE_CLASS RBUFFER_BOOL_TYPE rbuffer_empty(RBUFFER_SIZE_TYPE a_rpos, RBUFFER_SIZE_TYPE a_wpos);

RBUFFER_STORAGE_CLASS RBUFFER_BOOL_TYPE rbuffer_full(RBUFFER_SIZE_TYPE a_rpos, RBUFFER_SIZE_TYPE a_wpos, RBUFFER_SIZE_TYPE a_count);

RBUFFER_STORAGE_CLASS RBUFFER_SIZE_TYPE rbuffer_read_count(RBUFFER_SIZE_TYPE a_rpos, RBUFFER_SIZE_TYPE a_wpos);

RBUFFER_STORAGE_CLASS RBUFFER_SIZE_TYPE rbuffer_write_count(RBUFFER_SIZE_TYPE a_rpos,
                                                            RBUFFER_SIZE_TYPE a_wpos,
                                                            RBUFFER_SIZE_TYPE a_count);

RBUFFER_STORAGE_CLASS RBUFFER_SIZE_TYPE rbuffer_read(RBUFFER_SIZE_TYPE a_rpos, //
                                                     RBUFFER_SIZE_TYPE a_wpos,
                                                     RBUFFER_SIZE_TYPE a_count,
                                                     rbuffer_rw_cb a_read_cb,
                                                     void *a_ud);

RBUFFER_STORAGE_CLASS RBUFFER_SIZE_TYPE rbuffer_write(RBUFFER_SIZE_TYPE a_rpos, //
                                                      RBUFFER_SIZE_TYPE a_wpos,
                                                      RBUFFER_SIZE_TYPE a_count,
                                                      rbuffer_rw_cb a_write_cb,
                                                      void *a_ud);

#if defined(__cplusplus)
}
#endif

#if defined(__cplusplus) && defined(RBUFFER_IMPLEMENTATION)

static RBUFFER_INLINE RBUFFER_SIZE_TYPE rbuffer_read_count_internal(RBUFFER_SIZE_TYPE a_rpos,
                                                                    RBUFFER_SIZE_TYPE a_wpos,
                                                                    RBUFFER_SIZE_TYPE)
{
        return a_wpos - a_rpos;
}

static RBUFFER_INLINE RBUFFER_SIZE_TYPE rbuffer_write_count_internal(RBUFFER_SIZE_TYPE a_wpos,
                                                                     RBUFFER_SIZE_TYPE a_rpos,
                                                                     RBUFFER_SIZE_TYPE a_count)
{
        return a_count - rbuffer_read_count_internal(a_rpos, a_wpos, a_count);
}

typedef decltype(rbuffer_read_count_internal) avail_count_t;

template <avail_count_t avail_cnt>
static RBUFFER_INLINE void rbuffer_xfer_internal(RBUFFER_SIZE_TYPE a_pos[2],
                                                 RBUFFER_SIZE_TYPE a_count,
                                                 rbuffer_rw_cb a_cb,
                                                 void *a_ud)
{
        RBUFFER_SIZE_TYPE count_mask = (RBUFFER_SIZE_TYPE)(a_count - 1);

        for (RBUFFER_SIZE_TYPE avail_count = avail_cnt(a_pos[0], a_pos[1], a_count); //
             avail_count > 0;
             avail_count = avail_cnt(a_pos[0], a_pos[1], a_count))
        {
                RBUFFER_SIZE_TYPE masked_pos = (a_pos[0] & count_mask);

                if ((masked_pos + avail_count) >= a_count)
                {
                        avail_count = (RBUFFER_SIZE_TYPE)(a_count - masked_pos);
                }

                if (masked_pos = a_cb(masked_pos, avail_count, a_ud); masked_pos == 0)
                {
                        break;
                }

                a_pos[0] = (RBUFFER_SIZE_TYPE)(a_pos[0] + masked_pos);
        }
}

RBUFFER_BOOL_TYPE rbuffer_empty(RBUFFER_SIZE_TYPE a_rpos, RBUFFER_SIZE_TYPE a_wpos) { return a_wpos == a_rpos; }

RBUFFER_BOOL_TYPE rbuffer_full(RBUFFER_SIZE_TYPE a_rpos, RBUFFER_SIZE_TYPE a_wpos, RBUFFER_SIZE_TYPE a_count)
{
        return (a_wpos - a_rpos) == a_count;
}

RBUFFER_SIZE_TYPE rbuffer_read_count(RBUFFER_SIZE_TYPE a_rpos, RBUFFER_SIZE_TYPE a_wpos)
{
        return rbuffer_read_count_internal(a_rpos, a_wpos, 0);
}

RBUFFER_SIZE_TYPE rbuffer_write_count(RBUFFER_SIZE_TYPE a_rpos, RBUFFER_SIZE_TYPE a_wpos, RBUFFER_SIZE_TYPE a_count)
{
        return rbuffer_write_count_internal(a_wpos, a_rpos, a_count);
}

RBUFFER_SIZE_TYPE rbuffer_read(RBUFFER_SIZE_TYPE a_rpos, //
                               RBUFFER_SIZE_TYPE a_wpos,
                               RBUFFER_SIZE_TYPE a_count,
                               rbuffer_rw_cb a_read_cb,
                               void *a_ud)
{
        RBUFFER_SIZE_TYPE pos[] = {a_rpos, a_wpos};

        rbuffer_xfer_internal<rbuffer_read_count_internal>(pos, a_count, a_read_cb, a_ud);

        return pos[0];
}

RBUFFER_SIZE_TYPE rbuffer_write(RBUFFER_SIZE_TYPE a_rpos, //
                                RBUFFER_SIZE_TYPE a_wpos,
                                RBUFFER_SIZE_TYPE a_count,
                                rbuffer_rw_cb a_write_cb,
                                void *a_ud)
{
        RBUFFER_SIZE_TYPE pos[] = {a_wpos, a_rpos};

        rbuffer_xfer_internal<rbuffer_write_count_internal>(pos, a_count, a_write_cb, a_ud);

        return pos[0];
}

#endif
