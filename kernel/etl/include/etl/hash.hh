namespace etl
{
namespace hash
{
namespace djb2
{
// ********************************************************************************************************************************
static inline natural_t hash_buffer(void const *a_buffer, natural_t a_buffer_len, natural_t hash = 5381)
// ********************************************************************************************************************************
{
        for (natural_t i = 0; i < a_buffer_len; ++i)
        {
                hash = ((hash << 5) + hash) + (int)(static_cast<unsigned char const *>(a_buffer)[i]); /* hash * 33 + c */
        }

        return hash;
}

// ********************************************************************************************************************************
static inline natural_t hash_string(char const *a_string)
// ********************************************************************************************************************************
{
        natural_t hash = 5381;
        int       c;

        while ((c = *a_string++))
        {
                hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
        }

        return hash;
}

} // namespace djb2
} // namespace hash
} // namespace etl
