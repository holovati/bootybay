#pragma once

typedef struct rbtnode_data
{
        struct rbtnode_data *fields[3];
} * rbtnode_t;

#if defined(RBTNODE_FUNCTION_DEFINITIONS) || defined(RBTNODE_IMPLEMENTATION)

#if !defined(RBTNODE_INLINE)
#define RBTNODE_INLINE inline
#endif

#if !defined(RBTNODE_STORAGE_CLASS)
#define RBTNODE_STORAGE_CLASS
#endif

typedef int (*rbtnode_comp_cb_t)(rbtnode_t, rbtnode_t);

#if defined(__GNUC__)
#pragma GCC diagnostic ignored "-Wunused-function"
#endif

#if defined(__cplusplus)
extern "C" {
#endif

static RBTNODE_INLINE int rbtnode_is_sentinel(rbtnode_t a_node) { return ((**(rbtnode_t **)((a_node)->fields[0])) == a_node); }

RBTNODE_STORAGE_CLASS rbtnode_t rbtnode_init_sentinel(rbtnode_t a_sentinel);

RBTNODE_STORAGE_CLASS rbtnode_t rbtnode_next(rbtnode_t a_node);

RBTNODE_STORAGE_CLASS rbtnode_t rbtnode_prev(rbtnode_t a_node);

RBTNODE_STORAGE_CLASS rbtnode_t rbtnode_min(rbtnode_t a_node);

RBTNODE_STORAGE_CLASS rbtnode_t rbtnode_max(rbtnode_t a_node);

RBTNODE_STORAGE_CLASS rbtnode_t rbtnode_left(rbtnode_t a_node);

RBTNODE_STORAGE_CLASS rbtnode_t rbtnode_right(rbtnode_t a_node);

RBTNODE_STORAGE_CLASS void rbtnode_insert(rbtnode_t a_node, //
                                          rbtnode_t *a_leaf,
                                          rbtnode_t a_leaf_node,
                                          rbtnode_t *a_root,
                                          rbtnode_t a_sentinel);

RBTNODE_STORAGE_CLASS void rbtnode_delete(rbtnode_t a_node, rbtnode_t *a_root);

RBTNODE_STORAGE_CLASS rbtnode_t rbtnode_find(rbtnode_t a_node, rbtnode_t a_root, rbtnode_comp_cb_t a_less_fn);

RBTNODE_STORAGE_CLASS rbtnode_t *rbtnode_find_leaf(rbtnode_t a_node, //
                                                   rbtnode_t *a_leaf_parent_out,
                                                   rbtnode_t *a_root,
                                                   rbtnode_comp_cb_t a_less_fn);

RBTNODE_STORAGE_CLASS rbtnode_t *rbtnode_find_leaf_uniq(rbtnode_t a_node,
                                                        rbtnode_t *a_leaf_parent_out,
                                                        rbtnode_t *a_root,
                                                        rbtnode_comp_cb_t a_less_fn);

#define rbtnode_type(node, type, link) ((type *)((unsigned char *)(node)-offsetof(type, link)))

#if defined(__cplusplus)
}
#endif

#if defined(__cplusplus)

template <typename TYPE> [[maybe_unused]] static TYPE rbtnode_to_type(rbtnode_t);

template <typename TYPE> [[maybe_unused]] static rbtnode_t type_to_rbtnode(TYPE);

template <typename KEY_TYPE, typename VALUE_TYPE> [[maybe_unused]] static bool rbtnode_is_less(KEY_TYPE, VALUE_TYPE);

template <typename KEY_TYPE, typename VALUE_TYPE> [[maybe_unused]] static VALUE_TYPE rbtnode_find(KEY_TYPE a_val, rbtnode_t a_root)
{
        rbtnode_t x;
        bool key_is_less;
        for (x = a_root; !rbtnode_is_sentinel(x); x = x->fields[!key_is_less])
        {
                key_is_less = rbtnode_is_less<KEY_TYPE, VALUE_TYPE>(a_val, rbtnode_to_type<VALUE_TYPE>(x));
                if (!key_is_less && !rbtnode_is_less<VALUE_TYPE, KEY_TYPE>(rbtnode_to_type<VALUE_TYPE>(x), a_val))
                {
                        return rbtnode_to_type<VALUE_TYPE>(x);
                }
        }

        return nullptr;
}

template <typename VALUE_TYPE>
[[maybe_unused]] static VALUE_TYPE rbtnode_insert(VALUE_TYPE a_val, rbtnode_t *a_root, rbtnode_t a_sentinel)
{
        rbtnode_t *x, y;
        bool key_is_less;
        for (x = a_root, y = *a_root; !rbtnode_is_sentinel(*x); y = *x, x = &((*x)->fields[!key_is_less]))
        {
                key_is_less = rbtnode_is_less<VALUE_TYPE, VALUE_TYPE>(a_val, rbtnode_to_type<VALUE_TYPE>(*x));
                if (!key_is_less && !rbtnode_is_less<VALUE_TYPE, VALUE_TYPE>(rbtnode_to_type<VALUE_TYPE>(*x), a_val))
                {
                        return rbtnode_to_type<VALUE_TYPE>(*x);
                }
        }

        rbtnode_insert(type_to_rbtnode<VALUE_TYPE>(a_val), x, y, a_root, a_sentinel);

        return a_val;
}

template <typename VALUE_TYPE> [[maybe_unused]] static VALUE_TYPE rbt_first(rbtnode_t a_root)
{
        rbtnode_t n       = rbtnode_min(a_root);
        VALUE_TYPE object = nullptr;
        if (!rbtnode_is_sentinel(n))
        {
                object = rbtnode_to_type<VALUE_TYPE>(n);
        }
        return object;
}

template <typename VALUE_TYPE> [[maybe_unused]] static VALUE_TYPE rbt_last(rbtnode_t a_root)
{
        rbtnode_t n       = rbtnode_max(a_root);
        VALUE_TYPE object = nullptr;
        if (!rbtnode_is_sentinel(n))
        {
                object = rbtnode_to_type<VALUE_TYPE>(n);
        }
        return object;
}

template <typename VALUE_TYPE> [[maybe_unused]] static VALUE_TYPE rbt_next(VALUE_TYPE a_object)
{
        rbtnode_t n = rbtnode_next(type_to_rbtnode<VALUE_TYPE>(a_object));
        a_object    = nullptr;
        if (!rbtnode_is_sentinel(n))
        {
                a_object = rbtnode_to_type<VALUE_TYPE>(n);
        }
        return a_object;
}

template <typename VALUE_TYPE> [[maybe_unused]] static VALUE_TYPE rbt_prev(VALUE_TYPE a_object)
{
        rbtnode_t n = rbtnode_prev(type_to_rbtnode<VALUE_TYPE>(a_object));
        a_object    = nullptr;
        if (!rbtnode_is_sentinel(n))
        {
                a_object = rbtnode_to_type<VALUE_TYPE>(n);
        }
        return a_object;
}

#endif

#if defined(__GNUC__)
#pragma GCC diagnostic pop
#endif

#if defined(__cplusplus) && defined(RBTNODE_IMPLEMENTATION)

#if !defined(RBTNODE_SIZE_TYPE) || !defined(RBTNODE_UINTPTR_TYPE)
#error Define RBTNODE_SIZE_TYPE and/or RBTNODE_UINTPTR_TYPE
#endif

#if defined(_MSC_VER)
#pragma warning(push)
#pragma warning(disable : 4127)
#pragma warning(disable : 4505)
#endif

#define RBTNODE_COMPILE_DIRECTION_CHECK()                                                                                          \
        static_assert(direction == RBTNODE_DIRECTION_LEFT || direction == RBTNODE_DIRECTION_RIGHT,                                 \
                      "RBTNODE_DIRECTION_LEFT or RBTNODE_DIRECTION_RIGHT only.")

#if defined(left)
#undef left
#endif

#define left fields[RBTNODE_DIRECTION_LEFT]

#if defined(right)
#undef right
#endif

#define right fields[RBTNODE_DIRECTION_RIGHT]

enum RBTNODE_DIRECTION : RBTNODE_SIZE_TYPE
{
        RBTNODE_DIRECTION_LEFT = 0,
        RBTNODE_DIRECTION_RIGHT,
        RBTNODE_DIRECTION_PARENT
};

enum class RBTNODE_COLOR : RBTNODE_UINTPTR_TYPE
{
        RBTNODE_COLOR_RED = 0,
        RBTNODE_COLOR_BLACK,
};

static RBTNODE_INLINE RBTNODE_COLOR rbtnode_get_color(rbtnode_t a_node)
{
        return static_cast<RBTNODE_COLOR>(reinterpret_cast<RBTNODE_UINTPTR_TYPE>(a_node->fields[RBTNODE_DIRECTION_PARENT]) & 1);
}

static RBTNODE_INLINE void rbtnode_set_color(rbtnode_t a_node, RBTNODE_COLOR a_color)
{
        a_node->fields[RBTNODE_DIRECTION_PARENT] = reinterpret_cast<decltype(a_node)>(
            (reinterpret_cast<RBTNODE_UINTPTR_TYPE>(a_node->fields[RBTNODE_DIRECTION_PARENT]) & ~1)
            | static_cast<RBTNODE_UINTPTR_TYPE>(a_color));
}

static RBTNODE_INLINE rbtnode_t rbtnode_get_parent(rbtnode_t a_node)
{
        return reinterpret_cast<rbtnode_t>(reinterpret_cast<RBTNODE_UINTPTR_TYPE>(a_node->fields[RBTNODE_DIRECTION_PARENT]) & ~1);
}

static RBTNODE_INLINE void rbtnode_set_parent(rbtnode_t a_node, rbtnode_t a_parent)
{
        a_node->fields[RBTNODE_DIRECTION_PARENT] = reinterpret_cast<decltype(a_node)>(
            reinterpret_cast<RBTNODE_UINTPTR_TYPE>(a_parent)
            | (reinterpret_cast<RBTNODE_UINTPTR_TYPE>(a_node->fields[RBTNODE_DIRECTION_PARENT]) & 1));
}

static RBTNODE_INLINE bool rbtnode_is_black(rbtnode_t a_node)
{
        return (rbtnode_get_color(a_node) == RBTNODE_COLOR::RBTNODE_COLOR_BLACK);
}

static RBTNODE_INLINE bool rbtnode_is_red(rbtnode_t a_node) { return !rbtnode_is_black(a_node); }

static RBTNODE_INLINE void rbtnode_make_red(rbtnode_t a_node) { rbtnode_set_color(a_node, RBTNODE_COLOR::RBTNODE_COLOR_RED); }

static RBTNODE_INLINE void rbtnode_make_black(rbtnode_t a_node) { rbtnode_set_color(a_node, RBTNODE_COLOR::RBTNODE_COLOR_BLACK); }

static RBTNODE_INLINE bool rbtnode_is_left_child(rbtnode_t a_node, rbtnode_t a_parent) { return (a_node == a_parent->left); }

static RBTNODE_INLINE bool rbtnode_is_right_child(rbtnode_t a_node, rbtnode_t a_parent)
{
        return !rbtnode_is_left_child(a_node, a_parent);
}

template <RBTNODE_DIRECTION direction> //
static RBTNODE_INLINE void rbtnode_rotate(rbtnode_t a_node, rbtnode_t *const a_root)
{
        RBTNODE_COMPILE_DIRECTION_CHECK();
        rbtnode_t x = a_node->fields[!direction];
        rbtnode_t y = rbtnode_get_parent(a_node);

        if (*a_root == a_node)
        {
                *a_root = x;
        }
        else if (y->fields[direction] == a_node)
        {
                y->fields[direction] = x;
        }
        else
        {
                y->fields[!direction] = x;
        }

        rbtnode_set_parent(x, y);

        if (rbtnode_is_sentinel((a_node->fields[!direction] = x->fields[direction])) == false)
        {
                rbtnode_set_parent(a_node->fields[!direction], a_node);
        }

        x->fields[direction] = a_node;

        rbtnode_set_parent(a_node, x);
}

template <RBTNODE_DIRECTION direction>
static RBTNODE_INLINE rbtnode_t rbtnode_insert_black_uncle_fixup(rbtnode_t a_node, rbtnode_t *const a_root)
{
        RBTNODE_COMPILE_DIRECTION_CHECK();
        if (a_node == rbtnode_get_parent(a_node)->fields[!direction])
        {
                /* case 2 - move x up and rotate */
                rbtnode_rotate<direction>((a_node = rbtnode_get_parent(a_node)), a_root);
        }
        // case 3
        rbtnode_make_black((a_node = rbtnode_get_parent(a_node))); // color x's parent black
        rbtnode_make_red((a_node = rbtnode_get_parent(a_node)));   // color x's grandparent red and set y to it
        rbtnode_rotate<static_cast<decltype(direction)>(!direction)>(
            a_node, a_root); // Rotate right if x's parent is a left child otherwise rotate right
        return a_node;
}
#define rbtnode_insert_fixup_left  rbtnode_insert_black_uncle_fixup<RBTNODE_DIRECTION_LEFT>
#define rbtnode_insert_fixup_right rbtnode_insert_black_uncle_fixup<RBTNODE_DIRECTION_RIGHT>

template <RBTNODE_DIRECTION direction> //
static RBTNODE_INLINE rbtnode_t rbtnode_minmax(rbtnode_t a_subtree_root)
{
        RBTNODE_COMPILE_DIRECTION_CHECK();

        while (rbtnode_is_sentinel(a_subtree_root->fields[direction]) == false)
        {
                a_subtree_root = a_subtree_root->fields[direction];
        }

        return a_subtree_root;
}
#define _rbtnode_min rbtnode_minmax<RBTNODE_DIRECTION_LEFT>
#define _rbtnode_max rbtnode_minmax<RBTNODE_DIRECTION_RIGHT>

template <RBTNODE_DIRECTION direction> //
static RBTNODE_INLINE rbtnode_t rbtnode_prevnext(rbtnode_t a_node)
{
        RBTNODE_COMPILE_DIRECTION_CHECK();
        if (!rbtnode_is_sentinel(a_node->fields[direction]))
        {
                return rbtnode_minmax<static_cast<RBTNODE_DIRECTION>(!direction)>(a_node->fields[direction]);
        }

        rbtnode_t y = rbtnode_get_parent(a_node);

        while ((rbtnode_is_sentinel(y) == false) && (a_node == y->fields[direction]))
        {
                y = rbtnode_get_parent((a_node = y));
        }

        return y;
}
#define _rbtnode_prev rbtnode_prevnext<RBTNODE_DIRECTION_LEFT>
#define _rbtnode_next rbtnode_prevnext<RBTNODE_DIRECTION_RIGHT>

template <RBTNODE_DIRECTION direction>
static RBTNODE_INLINE rbtnode_t rbtnode_delete_fixup(rbtnode_t a_node, rbtnode_t a_parent, rbtnode_t *const a_root)
{
        RBTNODE_COMPILE_DIRECTION_CHECK();
        rbtnode_t sibling = a_parent->fields[!direction];

        if (rbtnode_is_red(sibling))
        {
                // case 1
                rbtnode_make_black(sibling);
                rbtnode_make_red(a_parent);
                rbtnode_rotate<direction>(a_parent, a_root);
                sibling = (a_parent = rbtnode_get_parent(a_node))->fields[!direction];
        }

        if (rbtnode_is_black(sibling->left) && rbtnode_is_black(sibling->right))
        {
                // case 2
                rbtnode_make_red(sibling);
                a_node = a_parent;
        }
        else
        {
                if (rbtnode_is_black(sibling->fields[!direction]))
                {
                        // case 3
                        rbtnode_make_black(sibling->fields[direction]);
                        rbtnode_make_red(sibling);
                        rbtnode_rotate<static_cast<RBTNODE_DIRECTION>(!direction)>(sibling, a_root);
                        sibling = (a_parent = rbtnode_get_parent(a_node))->fields[!direction];
                }
                // case 4
                rbtnode_set_color(sibling, rbtnode_get_color(a_parent));
                rbtnode_make_black(a_parent);
                rbtnode_make_black(sibling->fields[!direction]);
                rbtnode_rotate<direction>(a_parent, a_root);
                a_node = *a_root;
        }
        return a_node;
}
#define rbtnode_delete_fixup_left  rbtnode_delete_fixup<RBTNODE_DIRECTION_LEFT>
#define rbtnode_delete_fixup_right rbtnode_delete_fixup<RBTNODE_DIRECTION_RIGHT>

template <bool unique_key>
static RBTNODE_INLINE rbtnode_t *rbtnode_find_leaf_internal(rbtnode_t a_node,
                                                            rbtnode_t *a_leaf_parent_out,
                                                            rbtnode_t *a_root,
                                                            rbtnode_comp_cb_t a_less_fn)
{
        rbtnode_t *x;
        for (x = a_root, *a_leaf_parent_out = *a_root; !rbtnode_is_sentinel(*x);
             (*a_leaf_parent_out) = *x, x = &((*x)->fields[!a_less_fn(a_node, *x)]))
        {
                if (unique_key && (!a_less_fn(a_node, *x) && !a_less_fn(*x, a_node)))
                {
                        break;
                }
        }
        return x;
}
#define _rbtnode_find_leaf      rbtnode_find_leaf_internal<false>
#define _rbtnode_find_leaf_uniq rbtnode_find_leaf_internal<true>

static RBTNODE_INLINE void rbtnode_move(rbtnode_t a_dst, rbtnode_t a_src, rbtnode_t *a_root)
{
        rbtnode_t dst_parent;
        if (rbtnode_is_sentinel(dst_parent = rbtnode_get_parent(a_dst)))
        {
                *a_root = a_src;
        }
        else
        {
                dst_parent->fields[rbtnode_is_right_child(a_dst, dst_parent)] = a_src;
        }

        rbtnode_set_parent(a_src, dst_parent);
}

rbtnode_t rbtnode_init_sentinel(rbtnode_t a_sentinel)
{
        *a_sentinel = {{a_sentinel,
                        reinterpret_cast<rbtnode_t>(&a_sentinel->right),
                        reinterpret_cast<decltype(a_sentinel)>(RBTNODE_COLOR::RBTNODE_COLOR_BLACK)}};
        return a_sentinel;
}

rbtnode_t rbtnode_next(rbtnode_t a_node) { return _rbtnode_next(a_node); }

rbtnode_t rbtnode_prev(rbtnode_t a_node) { return _rbtnode_prev(a_node); }

rbtnode_t rbtnode_min(rbtnode_t a_node) { return _rbtnode_min(a_node); }

rbtnode_t rbtnode_max(rbtnode_t a_node) { return _rbtnode_max(a_node); }

rbtnode_t rbtnode_left(rbtnode_t a_node) { return a_node->fields[RBTNODE_DIRECTION_LEFT]; }

rbtnode_t rbtnode_right(rbtnode_t a_node) { return a_node->fields[RBTNODE_DIRECTION_RIGHT]; }

rbtnode_t rbtnode_find(rbtnode_t a_node, rbtnode_t a_root, rbtnode_comp_cb_t a_less_fn)
{
        rbtnode_t x;
        for (x = a_root; !rbtnode_is_sentinel(x); x = x->fields[!a_less_fn(a_node, x)])
        {
                if (!a_less_fn(a_node, x) && !a_less_fn(x, a_node))
                {
                        break;
                }
        }
        return x;
}

rbtnode_t *rbtnode_find_leaf(rbtnode_t a_node, //
                             rbtnode_t *a_leaf_parent_out,
                             rbtnode_t *a_root,
                             rbtnode_comp_cb_t a_less_fn)
{
        return _rbtnode_find_leaf(a_node, a_leaf_parent_out, a_root, a_less_fn);
}

rbtnode_t *rbtnode_find_leaf_uniq(rbtnode_t a_node, rbtnode_t *a_leaf_parent_out, rbtnode_t *a_root, rbtnode_comp_cb_t a_less_fn)
{
        return _rbtnode_find_leaf(a_node, a_leaf_parent_out, a_root, a_less_fn);
}

void rbtnode_insert(rbtnode_t a_node, rbtnode_t *a_leaf, rbtnode_t a_leaf_node, rbtnode_t *a_root, rbtnode_t a_sentinel)
{
        *a_leaf = &(*a_node = {{a_sentinel, a_sentinel, a_leaf_node}});

        rbtnode_t y, yp;
        bool parent_is_left_child;

        // Crawl back up, balancing the tree
        while (rbtnode_is_red((y = rbtnode_get_parent(a_node))))
        {
                // y contains x's parent
                parent_is_left_child = rbtnode_is_left_child(y, yp = rbtnode_get_parent(y)); // yp contains x's grandparent

                if (rbtnode_is_red((y = yp->fields[parent_is_left_child]))) // y contains x's uncle, check if it is red
                {
                        // Uncle is red
                        /* case 1 - change the colours and promote x to its grandparent*/
                        rbtnode_make_black(y);                                     // color x's uncle black
                        rbtnode_make_black((a_node = rbtnode_get_parent(a_node))); // color x's parent black and set x to it
                        rbtnode_make_red((a_node = rbtnode_get_parent(a_node)));   // color x's grandparent red and set x to it
                }
                else
                {
                        // Uncle is black
                        if (/*parent is left child*/ parent_is_left_child)
                        {
                                a_node = rbtnode_insert_fixup_left(a_node, a_root);
                        }
                        else /*parent is right child*/
                        {
                                a_node = rbtnode_insert_fixup_right(a_node, a_root);
                        }
                }
        }
        rbtnode_make_black(*a_root);
}

void rbtnode_delete(rbtnode_t a_node, rbtnode_t *a_root)
{
        rbtnode_t x;
        RBTNODE_COLOR orig_color = rbtnode_get_color(a_node);

        if (rbtnode_is_sentinel(a_node->left))
        {
                x = a_node->right;
                rbtnode_move(a_node, a_node->right, a_root);
        }
        else if (rbtnode_is_sentinel(a_node->right))
        {
                x = a_node->left;
                rbtnode_move(a_node, a_node->left, a_root);
        }
        else
        {
                rbtnode_t replacement = rbtnode_min(a_node->right);
                orig_color            = rbtnode_get_color(replacement);

                x = replacement->right;

                if (rbtnode_get_parent(replacement) == a_node)
                {
                        rbtnode_set_parent(x, replacement);
                }
                else
                {
                        rbtnode_move(replacement, replacement->right, a_root);
                        replacement->right = a_node->right;
                        rbtnode_set_parent(replacement->right, replacement);
                }
                rbtnode_move(a_node, replacement, a_root);
                replacement->left = a_node->left;
                rbtnode_set_parent(replacement->left, replacement);
                rbtnode_set_color(replacement, rbtnode_get_color(a_node));
        }

        if (orig_color == RBTNODE_COLOR::RBTNODE_COLOR_BLACK)
        {
                rbtnode_t parent;
                while (x != *a_root && rbtnode_is_black(x))
                {
                        if (rbtnode_is_left_child(x, (parent = rbtnode_get_parent(x))))
                        {
                                x = rbtnode_delete_fixup_left(x, parent, a_root);
                        }
                        else
                        {
                                x = rbtnode_delete_fixup_right(x, parent, a_root);
                        }
                }
                rbtnode_make_black(x);
        }
}

#undef left
#undef right

#if defined(_MSC_VER)
#pragma warning(pop)
#endif
#endif
#endif
